package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class Encampments extends StartWebDriver {
	static By encampmentsMenu = By.xpath("//span[contains(text(),'Encampments')]");
	
	public static String returnencampmentsPageHeader() {
		String encampmentsPageHeaderText = WebDriverTasks.waitForElement(encampmentsMenu).getText();
		System.out.println("The page header is:" +encampmentsPageHeaderText);
		return encampmentsPageHeaderText;
	}

}
