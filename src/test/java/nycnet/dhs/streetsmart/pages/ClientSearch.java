package nycnet.dhs.streetsmart.pages;

import org.apache.commons.exec.util.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;



public class ClientSearch extends StartWebDriver {
     //Duplicate Search grid locators
	 static By duplicateSearchPageHeader = By.xpath("//h4[@class='panel-title']/*[contains(text(), 'Duplicate Search')]");
	 
	 static By duplicateSearchFirstName = By.xpath("(//*[@id='first-name'])[1]");
	 static By duplicateSearchLastName = By.xpath("(//*[@id='last-name'])[1]");
	 static By duplicateSearchButton = By.id("cint-info-save");
	 static By duplicateSearchAddNewClient = By.xpath("(//*[@id='data-table_wrapper']//a[contains(text(),'Add New Client')])[1]");
	 static By expandClientIDDetailsDuplicateSearch = By.id("dubiconaccordion2");
	 //static By clientCategoryDropDownMenu = By.cssSelector(".form-control.ng-valid.ng-touched.ng-not-empty.ng-dirty.ng-valid-parse");
	 static By clientCategoryDropDownMenuOnDuplicateSearch = By.cssSelector("#clinetSearchPanel select[ng-model='ClientCategory']");
	 static By clientCategoryColumnOnDuplicateSearch = By.cssSelector("#clinetSearchPanel div[title='Client Category']");
	
	 static By clientCategoryValueongridOnDuplicateSearch = By.cssSelector("div[title='Engaged Unsheltered Prospect']");
	 static By duplicateSearchColumnPicker = By.xpath("(//*[@class='ui-grid-menu-button ng-scope'])[1]");
	 static By clientCategoryOnColumnPicker = By.xpath("//*[@class='ui-grid-menu-item ng-binding'][contains(text(), 'Client Category')]");
	 
	 static By duplicateSearchClientCategoryCaseload = By.xpath("(//*[@ng-model='ClientCategory']/*[@label='Caseload'])[1]");
	 static By duplicateSearchClientCategoryOtherProspect = By.xpath("(//*[@ng-model='ClientCategory']/*[@label='Other Prospect'])[1]");
	 static By duplicateSearchClientCategoryEnrollmentEnded = By.xpath("(//*[@ng-model='ClientCategory']/*[@label='Enrollment Ended'])[1]");
	 static By duplicateSearchClientCategoryEngagedUnshelteredProspect = By.xpath("(//*[@ng-model='ClientCategory']/*[@label='Engaged Unsheltered Prospect'])[1]");
	 static By duplicateSearchClientCategoryInactive = By.xpath("(//*[@ng-model='ClientCategory']/*[@label='Inactive'])[1]");
	 static By duplicateSearchClientCategoryEngagedUnshelteredProspectValueOnGrid = By.xpath("//*[@title='Engaged Unsheltered Prospect']");
	 
	 //Client Search Grid locators
	 static By expandClientIDDetailsClientSearch = By.id("iconaccordion2");
	 
	 static By clientCategoryDropDownMenuOnClientSearch = By.xpath("(//*[@ng-model='ClientCategory'])[2]");
	 static By clientSearchColumnPicker = By.xpath("(//*[@class='ui-grid-menu-button ng-scope'])[2]");
	 static By clientCategoryColumnOnClientSearchGrid = By.xpath("(//*[@id='clinetSearchPanel']//*[@title='Client Category'])[2]");
	 static By clientCategoryValueongridOnClientSearch = By.xpath("(//*[@id='clinetSearchPanel']//*[@title='Engaged Unsheltered Prospect'])[2]");
	 
	 static By clientSearchFirstName = By.xpath("(//*[@id='first-name'])[2]");
	 static By clientSearchLastName = By.xpath("(//*[@id='last-name'])[2]");
	 
	 static By clientSearchButton = By.id("client-info-save");
	 static By clientSearchAddNewClient = By.xpath("(//*[@id='data-table_wrapper']//a[contains(text(),'Add New Client')])[2]");
	 static By expandClientSearchButton = By.id("panel-body-collapse");
	 
	 static By clientCategory = By.xpath("//*[@ng-bind='Client.ClientEnrollment.EnrollmentReasonName']");
	 
	 //Add new client locators
	 static By engagedUnshelteredButton = By.id("EngagedUnshelteredButton");
	 static By prospectButton = By.id("nonDHSVeteranDataSaveButton");
	 static By enrolledButton = By.id("nonDHSVeteranDataCancelButton");
	 
	 //Merge locators
	 static By checkmarkOfFirstRecord = By.xpath("(//*[@class='ui-grid-cell-contents'])[2]");
	 static By checkmarkOfSecondRecord = By.xpath("(//*[@class='ui-grid-cell-contents'])[3]");
	 static By mergeButton = By.xpath("//*[@value='Merge']");
	 static By FirstPrimaryClient = By.xpath("(//*[@ng-click='OnPrimaryChange(mergeClient)'])[1]");
	 static By okConfirm = By.xpath("//button[@ng-click='ok()']");
	 static By confirmButton = By.xpath("//button[@ng-click='ok()'][.='Confirm']");
	 static By closeSecondaryClient = By.xpath("(//button[@ng-click='CloseEnrollmentCaseAndPlacement(clientObj.ClientId)'])[2]");
	 static By saveButton = By.xpath("//button[@ng-click='SaveClientWithOtherConflictedEntities()']");
	 static By saveButtonWhenChoosingPrimary = By.xpath("//button[@ng-click='ValidateClientDemographics()']");
	 static By firstClientStreetSmartIDOnMerge = By.xpath("(//input[@ng-click='OnPrimaryChange(mergeClient)']/following-sibling::label)[1]");

	
	 public static String returnDuplicateSearchPageHeader() {
		  String duplicateSearchPageHeaderText =  WebDriverTasks.waitForElement(duplicateSearchPageHeader).getText();
		  System.out.println("The page header is " + duplicateSearchPageHeaderText);
		  return duplicateSearchPageHeaderText;
	  }
	 
	 public static String returnClientCategory() {
		  String clientCategoryText =  WebDriverTasks.waitForElement(clientCategory).getText();
		  System.out.println("The client category is " + clientCategoryText);
		  return clientCategoryText;
	  }
	 
	 public static void inputTextinFirstNameField(String firstName) {
		 WebDriverTasks.waitForElement(duplicateSearchFirstName ).sendKeys(firstName);
	  }
	 
	 public static void inputTextinLastNameField(String lastName) {
		 WebDriverTasks.waitForElement(duplicateSearchLastName ).sendKeys(lastName);
	  }
	 
	 public static void clickOnexpandClientIDdetailsOnDuplicateSearch () {
		 WebDriverTasks.waitForElement(expandClientIDDetailsDuplicateSearch).click();
	 }
	 
	
	 public static void selectCategoryDropDownMenuOnDuplicateSearch (String clientcategory) {
		 WebDriverTasks.waitForElementAndSelect(clientCategoryDropDownMenuOnDuplicateSearch).selectByVisibleText(clientcategory);
	 }
	 
	 public static String returnclientCategoryColumnOnDuplicateSearch() {
		  String Text =  WebDriverTasks.waitForElement(clientCategoryColumnOnDuplicateSearch).getText();
		  System.out.println("The value of the column is  " + Text);
		  return Text;
	  }
	 
	 public static String returnduplicateSearchClientCategoryCaseload () {
		 return WebDriverTasks.waitForElement(duplicateSearchClientCategoryCaseload).getText();
		
	 }
	 
	 public static String returnduplicateSearchClientCategoryOtherProspect () {
		 return WebDriverTasks.waitForElement(duplicateSearchClientCategoryOtherProspect).getText();
		
	 }
	 
	 public static String returnduplicateSearchClientCategoryEnrollmentEnded () {
		 return WebDriverTasks.waitForElement(duplicateSearchClientCategoryEnrollmentEnded).getText();
		
	 }
	 
	 public static String returnduplicateSearchClientCategoryEngagedUnshelteredProspect () {
		 return WebDriverTasks.waitForElement(duplicateSearchClientCategoryEngagedUnshelteredProspect).getText();
	 }
	 
	 public static String returnduplicateSearchClientCategoryInactive () {
		 return WebDriverTasks.waitForElement(duplicateSearchClientCategoryInactive).getText();
		
	 }
	 
	 
	 public static boolean verifyIfclientCategoryColumnExistsOnDuplicateSearch() {
		 boolean clientCategoryIsPresent =  WebDriverTasks.verifyIfElementExists(clientCategoryColumnOnDuplicateSearch);
		  System.out.println("The presence of the column is  " + clientCategoryIsPresent);
		  return clientCategoryIsPresent;
	  }
	 
	 public static String returnclientCategoryValueongridOnDuplicateSearch() {
		  String Text =  WebDriverTasks.waitForElement(clientCategoryValueongridOnDuplicateSearch).getText();
		  System.out.println("The value of the cell on the grid  " + Text);
		  return Text;
	  }
	 
	 public static void clickOnColumnPickerOnDuplicateSearch () {
		 WebDriverTasks.waitForElement(duplicateSearchColumnPicker).click();
	 }
	 
	 public static String returnEngagedUnshelteredProspecClientCategoryOnDuplicateSearchGrid() {
		  String Text =  WebDriverTasks.waitForElement(duplicateSearchClientCategoryEngagedUnshelteredProspectValueOnGrid).getText();
		  System.out.println("The value of the column is  " + Text);
		  return Text;
	  }
	 
	 //Will work on both Client Search grid and Duplicate Search grid
	 public static void clickOnclientCategoryOnColumnPicker () {
		 WebDriverTasks.waitForElement(clientCategoryOnColumnPicker).click();
	 }
	 
	 public static void clickOnDuplicateSearchAddNewClient () {
		 WebDriverTasks.waitForElement(duplicateSearchAddNewClient).click();
	 }
	 
	 public static void clickOnDupliceSearchButton () {
		 WebDriverTasks.waitForElement(duplicateSearchButton).click();
	 }
	 
	 
	 //Client Search grid actions
	 
	 public static void clickOnexpandClientIDdetailsOnClientSearch () {
		 WebDriverTasks.waitForElement(expandClientIDDetailsClientSearch).click();
	 }
	 
	 public static void selectCategoryDropDownMenuOnClientSearch (String clientcategory) {
		 WebDriverTasks.waitForElementAndSelect(clientCategoryDropDownMenuOnClientSearch).selectByVisibleText(clientcategory);
	 }
	 
	 public static String returnclientCategoryColumnOnClientSearch() {
		  String Text =  WebDriverTasks.waitForElement(clientCategoryColumnOnClientSearchGrid).getText();
		  System.out.println("The value of the column is  " + Text);
		  return Text;
	  }
	 
	 public static boolean verifyIfclientCategoryColumnExistsOnClientSearch() {
		 boolean clientCategoryIsPresent =  WebDriverTasks.verifyIfElementExists(clientCategoryColumnOnClientSearchGrid);
		  System.out.println("The presence of the column is  " + clientCategoryIsPresent);
		  return clientCategoryIsPresent;
	  }
	 
	 public static String returnclientCategoryValueongridOnClientSearch() {
		  String Text =  WebDriverTasks.waitForElement(clientCategoryValueongridOnClientSearch).getText();
		  System.out.println("The value of the cell on the grid  " + Text);
		  return Text;
	  }
	 
	 public static void clickOnColumnPickerOnClientSearch () {
		 WebDriverTasks.waitForElement(clientSearchColumnPicker).click();
	 }
	 
	 //
	 public static void clickOnexpandClientIDdetailsOnClientSearchGrid () {
		 WebDriverTasks.waitForElement(expandClientIDDetailsClientSearch).click();
	 }
	 
	 public static void clickOnClientOnGrid (String streetsmartID) {
		 By streetSmartClient = By.linkText(streetsmartID);
		 WebDriverTasks.waitForElement(streetSmartClient).click();
	 }
	 
	 public static void expandClientSearchPanel () {
		 WebDriverTasks.waitForElement(expandClientSearchButton).click();
	 }
	 
	 public static void inputTextinClientSearchFirstNameField(String firstName) {
		 WebDriverTasks.waitForElement(clientSearchFirstName).sendKeys(firstName);
	  }
	 
	 public static void inputTextinClientSearchLastNameField(String lastName) {
		 WebDriverTasks.waitForElement(clientSearchLastName).sendKeys(lastName);
	  }
	 
	 public static void clickOnClientSearchButton () {
		 WebDriverTasks.waitForElement(clientSearchButton).click();
	 } 
	 
	 public static void scrollToClientSearchButton () {
		 WebDriverTasks.scrollToElement(clientSearchButton);
	 } 
	 
	 public static void clickOnClientSearchAddNewClient () {
		 WebDriverTasks.waitForElement(clientSearchAddNewClient).click();
	 }
	 
	 public static void clickOnEngagedUnshelteredButton () {
		 WebDriverTasks.waitForElement(engagedUnshelteredButton).click();
	 }
	 
	 public static void clickOnProspectButton () {
		 WebDriverTasks.waitForElement(prospectButton).click();
	 }
	 
	 public static void clickOnEnrolledButton () {
		 WebDriverTasks.waitForElement(enrolledButton).click();
	 }
	 
	//Merge Client actions
	 public static void selectFirstRecord () {
		 WebDriverTasks.waitForElement(checkmarkOfFirstRecord).click();
	 }
	 
	 public static void selectSecondRecord () {
		 WebDriverTasks.waitForElement(checkmarkOfSecondRecord).click();
	 }
	 
	 
	 public static void clickOnMergeButton () {
		 WebDriverTasks.waitForElement(mergeButton).click();
	 }
	 
	 public static void chooseFirstPrimaryClient () {
		 WebDriverTasks.waitForElement(FirstPrimaryClient).click();
	 }
	 
	 public static void clickConfirmButton () {
		 if (WebDriverTasks.verifyIfElementExists(confirmButton)) {
			 driver.findElement(confirmButton).click();
		 }
			 
		   // driver.findElement(confirmButton).click();
		// WebDriverTasks.waitForElement(confirmButton).click();
	 }
	 
	 public static void clickConfirmButtonJavascriptExecutor () {
		WebDriverTasks.specificWait(5000);
		 if (WebDriverTasks.verifyIfElementExists(confirmButton)) {
			 WebDriverTasks.clickOnElementJavascriptExcecutor(confirmButton);
		 }
	 }
	 
	 public static void clickOKConfirm () {
		 if (WebDriverTasks.verifyIfElementExists(okConfirm)) {
			 WebDriverTasks.waitForElement(okConfirm).click();
		 }
	 }
	 
	 public static void closeSecondaryClient () {
		 WebDriverTasks.waitForElement(closeSecondaryClient).click();
	 }
	 
	 public static void saveButtonWhenChoosingPrimary () {
		 WebDriverTasks.waitForElement(saveButtonWhenChoosingPrimary).click();
	 }
	 
	 public static void saveButton () {
		 WebDriverTasks.waitForElement(saveButton).click();
	 }
	 
	 public static String returnfirstClientStreetSmartIDOnMerge() {
		  String text =  WebDriverTasks.waitForElement(firstClientStreetSmartIDOnMerge).getText();
		  System.out.println("The StreetSmartID of the client is  " + text);
		  String textSplit[] = StringUtils.split(text, "-");
			String streetsmartID = textSplit[0];
			System.out.println("The StreetSmartID of the client is  " + textSplit[0]);
			return streetsmartID;
		 
	  }
	 
	}
	 
