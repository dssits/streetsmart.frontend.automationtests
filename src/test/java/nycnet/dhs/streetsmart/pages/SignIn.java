package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;



public class SignIn extends StartWebDriver {

	static By userNameInputBox = By.id("txtUserName");
	static By passwordInputBox = By.id("txtPassword");
	static By signInButton = By.xpath("//button[contains(text(), 'Sign me in')]");
	static By signInPageHeader = By.xpath("//*[@class='login-header']//*[contains(text(), 'StreetSmart: Please Sign In')]");
	static By provideUsernamePasswordHeader = By.id("spnErrorMessage");
	
	 public static void inputUserName(String username) {
		 WebDriverTasks.waitForElement(userNameInputBox).sendKeys(username);
	  }
	 
	 public static void passwordInputBox(String password) {
		 WebDriverTasks.waitForElement(passwordInputBox).sendKeys(password);
	  }
	 
	 public static void clickOnSignInButton() {
		 WebDriverTasks.waitForElement(signInButton).click();;
	  }
	 
	 public static String returnSignInPageHeader() {
		 String signInPageHeaderText = WebDriverTasks.waitForElement(signInPageHeader).getText();
		 System.out.println("The page header is " + signInPageHeaderText);
		 return signInPageHeaderText;
	  }
	 
	 public static String returnProvideUserNamePasswordHeader() {
		 String signInUserNamePageHeader = WebDriverTasks.waitForElement(provideUsernamePasswordHeader).getText();
		 System.out.println("The page header is " + signInUserNamePageHeader);
		 return signInUserNamePageHeader;
	  }
}
