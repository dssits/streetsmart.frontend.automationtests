package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class Common extends StartWebDriver {
	
	 static By StreetSmartID = By.linkText("C160911");
	 static By selectorForFirstRow = By.xpath("(//*[@class='ui-grid-cell-contents'])[2]");
	 static By selectorForThirdRow = By.xpath("(//*[@class='ui-grid-cell-contents'])[4]");
	 static By selectorForFifthRow = By.xpath("(//*[@class='ui-grid-cell-contents'])[6]");
	 static By exportToExcel = By.linkText("Excel");
	 //static By exportToExcel = By.xpath("//*[@class='pull-left col-sm-2 auto-width']");
	 //static By exportToExcel = By.cssSelector(".pull-left.col-sm-2.auto-width");
	 //static By exportToExcel = By.xpath("(//*[@class='caret'])[5]");
	 static By exportToExcelSelected = By.linkText("Selected Records");
	 static By exportToExcelFilteredRecords = By.linkText("Filtered Records");
	 static By exportToExcelAllRecords = By.linkText("All Records");
	 static By exportToExcelCurrentView = By.linkText("Current View");
	 static By exportToExcelMessage = By.xpath("//*[@class='toast-message']");
	 static By oKConfirmationButton = By.id("nonDHSVeteranDataSaveButton");
	 
		static By dateFilterYearlyButton = By.xpath("//button[contains(text(), 'Yearly')]");
		static By dateFilterYearlyMenu  = By.id("ClientEngagementGrid-Yearly");
		static By dateFilterYearlySelect  = By.xpath("//select[@ng-model='selectYear']");
		
		//Locators from Add New Client - Duplicate Search, Client Search. Add New Engagements - Duplicate Search. Client and Engagement search
		 static By engagedUnshelteredButton = By.id("EngagedUnshelteredButton");
		 static By prospectButton = By.id("nonDHSVeteranDataSaveButton");
		 static By enrolledButton = By.id("nonDHSVeteranDataCancelButton");
		 
		 //Locators for Advanced Filters
		 static By clickOnAdvancedFilter = By.linkText("Advanced Filtering");
		 static By clickOnChooseFilter = By.xpath("//button[@id='dropDown_Add Filter']//b[contains(@class,'caret')]");
		 static By clickOnRadioClientCategory = By.xpath("//input[@id='comboBoxClient Category']");
		
		 static By clickOnClientCategoryFilterButton = By.xpath("//div[@class='ng-scope']//b[@class='caret']");
		 static By clickOnClientCategoryFilterButton2 = By.xpath("(//div[@class='ng-scope']//b[@class='caret'])[2]");
		 static By clickOnRadioEngagedUnsheltered = By.xpath("//input[@id='comboBoxEngaged Unsheltered Prospect']");
		 static By clickOnRadioOtherProspect = By.xpath("//input[@id='comboBoxOther Prospect']");
		 static By clickOnRadioInactive = By.xpath("//input[@id='comboBoxInactive']");
		 static By clickOnRadioCaseload = By.xpath("//input[@id='comboBoxCaseload']");
		 static By clickOnApplyFilter = By.xpath("//button[@id='nonDHSVeteranDataSaveButton']");
		 
		 //Locators for column picker
		 static By ColumnPicker = By.xpath("(//*[@class='ui-grid-menu-button ng-scope'])[1]");
		 static By clientCategoryOnColumnPicker = By.xpath("//*[@class='ui-grid-menu-item ng-binding'][contains(text(), 'Client Category')]");
	
		 //Can be used to click on any linktext. Can also be used to click on dates on Calendar
		 public static void clickOnLinkText(String text) {
			 WebDriverTasks.waitForElement(By.linkText(text)).click();
		 }
		 
		 public static void clickOnPartialLinkText(String text) {
			 WebDriverTasks.waitForElement(By.partialLinkText(text)).click();
		 }
		 
		//Actions for All Engagements, My Engagements
		public static void clickOnDateFilterYearlyButton() {
			 WebDriverTasks.waitForElement(dateFilterYearlyButton).click();
		 }
		
		public static void clickOnDateFilterYearlyMenu() {
			 WebDriverTasks.waitForElement(dateFilterYearlyMenu).click();
		 }
		
		public static void selectYearonYearlyFilter(String year) {
			WebDriverTasks.waitForElementAndSelect(dateFilterYearlySelect).selectByVisibleText(year);
		 }
		
	 public static void selectFirstRowinClientListGrid() {
		 WebDriverTasks.waitForElement(selectorForFirstRow).click();
	 }
	 
	 public static void selectThirdRowinClientListGrid() {
		 WebDriverTasks.waitForElement(selectorForThirdRow).click();
	 }
	 
	 public static void selectFifthRowinClientListGrid() {
		 WebDriverTasks.waitForElement(selectorForFifthRow).click();
	 }
	 
	 public static void clickOnStreetSmartID() {
		 WebDriverTasks.waitForElement(StreetSmartID).click();
	 }
	 
	 public static void clickOnExportToExcel() {
		 WebDriverTasks.waitForElement(exportToExcel).click();
	 }
	 
	 public static void scrollToExportToExcel() {
		 WebDriverTasks.scrollToElement(exportToExcel);
	 }
	 
	 public static void clickOnExportToExcelSelectedRecords() {
		 WebDriverTasks.waitForElement(exportToExcelSelected).click();
	 }
	 
	 public static void clickOnExportToExcelFilteredRecords() {
		 WebDriverTasks.waitForElement(exportToExcelFilteredRecords).click();
	 }
	 
	 public static void clickOnExportToExcelCurrentView() {
		 WebDriverTasks.waitForElement(exportToExcelCurrentView).click();
	 }
	 
	 public static void clickOnExportToExcelAllRecords() {
		 WebDriverTasks.waitForElement(exportToExcelAllRecords).click();
	 }
	 
	 public static void clickOnOKConfirmationButton() {
		 WebDriverTasks.waitForElement(oKConfirmationButton).click();
	 }
	 
	 public static String verifyExportToExcelMessage() {
		 
		 String exportToExcelText =  WebDriverTasks.waitForElementButNotForAngular(exportToExcelMessage).getText();
		  System.out.println("The Export To Excel Message is " +  exportToExcelText);
		  return exportToExcelText;
	 }
	 
	 public static String verifyExportAllToExcelMessage() {
		 String exportToExcelText =  WebDriverTasks.waitForElementButNotForAngular(exportToExcelMessage).getText();
		  System.out.println("The Export To Excel Message is " +  exportToExcelText);  
		  return exportToExcelText;
	 }
	 
	 //Actions for Add New Client - Duplicate Search, Client Search. Add New Engagements - Duplicate Search. Client and Engagement search

	 
	 public static void clickOnEngagedUnshelteredButton () {
		 WebDriverTasks.waitForElement(engagedUnshelteredButton).click();
	 }
	 
	 public static String returnEngagedUnshelteredButtonText () {
		 String engagedUnshelteredButtonText = WebDriverTasks.waitForElement(engagedUnshelteredButton).getText();
		 return engagedUnshelteredButtonText;
	 }
	 
	 public static void clickOnProspectButton () {
		 WebDriverTasks.waitForElement(prospectButton).click();
	 }
	 
	 public static void clickOnEnrolledButton () {
		 WebDriverTasks.waitForElement(enrolledButton).click();
	 }
	 
	 //Actions for advanced filters
	 public static void clickOnApplyFilter() {
			WebDriverTasks.waitForElement(clickOnApplyFilter).click();
			WebDriverTasks.waitForAngular();
		}
		 
		 public static void clickOnRadioInactive() {
			 WebDriverTasks.waitForElement(clickOnRadioInactive).click();
			 System.out.println("Inactive option selected");
		 }
		 
		 public static void clickOnRadioEngagedUnsheltered() {
			 WebDriverTasks.waitForElement(clickOnRadioEngagedUnsheltered).click();
			 System.out.println("Client Category option selected");
		 }
		 
		 public static void clickOnRadioOtherProspect() {
			 WebDriverTasks.waitForElement(clickOnRadioOtherProspect).click();
			 System.out.println("Client Category option selected");
		 }
		 
		 public static void clickOnRadioCaseload() {
			 WebDriverTasks.waitForElement(clickOnRadioCaseload).click();
			 System.out.println("Client Category option selected");
		 }
		 
		 public static void clickOnClientCategoryFilterButton() {
			WebDriverTasks.waitForElement(clickOnClientCategoryFilterButton).click();
		 }
		 
		 public static void clickOnClientCategoryFilterButton2() {
				WebDriverTasks.waitForElement(clickOnClientCategoryFilterButton2).click();
			 }
		 
		 public static void clickOnRadioClientCategory() {
			 WebDriverTasks.waitForElement(clickOnRadioClientCategory).click();
		 }
		 
		 public static void clickOnChooseFiter() {
			 WebDriverTasks.waitForElement(clickOnChooseFilter).click();
		 }
		 public static void clickOnAdvancedFilter() {
			 WebDriverTasks.waitForElement(clickOnAdvancedFilter).click();
		 }
		 
		 //Actions for column picker
		 public static void clickOnColumnPicker () {
			 WebDriverTasks.waitForElement(ColumnPicker).click();
		 }
		 
		 public static void clickOnclientCategoryOnColumnPicker () {
			 WebDriverTasks.waitForElement(clientCategoryOnColumnPicker).click();
		 }
		 
		 

}
