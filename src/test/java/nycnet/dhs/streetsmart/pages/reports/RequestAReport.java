package nycnet.dhs.streetsmart.pages.reports;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class RequestAReport extends StartWebDriver {
    static By requestareportMenuItem = By.xpath("//a[contains(text(),'Request a Report')]");
    //Added By Venkat
    static By requestTitle = By.xpath("//input[@ng-model='CustomReportRequest.CustomReportRequestTitle']");
    static By submitRequestButton = By.xpath("//a[@ng-click='AddRequest()']");
    static By requestDetails = By.xpath("//textarea[@ng-model='CustomReportRequest.CustomReportRequestDetails']");
    static By saveRequestButton = By.xpath("//button[@ng-click='SaveCustomReportRequest()']");
    static By successMessage = By.xpath("//h2[text()='Submitted!']/following-sibling::p[text()='Your request has been submitted']");
    static By confirmSuccessPopup = By.xpath("//button[text()='OK']");
    static By savedRequestTitle = By.xpath("//div[@ng-bind='CustomReportRequest.CustomReportRequestTitle']");
    static By savedRequestDetail = By.xpath("//pre[@ng-bind='CustomReportRequest.CustomReportRequestDetails']");
    static By cancelRequestButton = By.xpath("//button[@ng-click='CancelViewRequestForm()']");
    static String actionButtonXpath = "//a[@title='%s']/ancestor::div[@role='row']//a[normalize-space()='Action']";
    static String radioOption = "//label[contains(normalize-space(),'%s')]/preceding-sibling::input[@type='radio']";
    static By changeStatusLink = By.xpath("//ul[contains(@style,'display: block;')]/li/a[normalize-space()='Change Status']");
    static By updateStatusButton = By.xpath("//button[@ng-click='UpdateRequestStatus()']");
    static String statusXpath = "//a[@title='%s']/ancestor::div[@role='gridcell']/following-sibling::div/div";

    public static String returnrequestareportPageHeader() {
        String requestareportPageHeaderText = WebDriverTasks.waitForElement(requestareportMenuItem).getText();
        System.out.println("The page header is:" + requestareportPageHeaderText);
        return requestareportPageHeaderText;
    }

    /**
     * This method will click on Submit Request button
     * Created By : Venkat
     */
    public static void clickSubmitRequestButton() {
        WebDriverTasks.waitForElement(submitRequestButton).click();
    }

    /**
     * This method will set title text in Title field
     *
     * @param title This is title string to be saved
     *              Created By : Venkat
     */
    public static void setTitle(String title) {
        WebDriverTasks.waitForElement(requestTitle).sendKeys(title);
    }

    /**
     * This method will set detail text in Detail field
     *
     * @param details This is detail string to be saved
     *                Created By : Venkat
     */
    public static void setDetails(String details) {
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(requestDetails).sendKeys(details);
    }

    /**
     * This method will click on Save button
     */
    public static void clickOnSaveRequestButton() {
        WebDriverTasks.waitForElement(saveRequestButton).click();
    }

    /**
     * This method will verify that success message is displayed on saving new request
     *
     * @return Boolean If success message is displayed then it will return True else False
     */
    public static boolean verifySuccessMessageDisplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(successMessage, 10);
    }

    /**
     * This method will click on Ok on success popup
     * Created By : Venkat
     */
    public static void confirmSuccessPopup() {
        WebDriverTasks.waitForElement(confirmSuccessPopup).click();
    }

    /**
     * This method will click on Request link for provided request title
     *
     * @param requestTitle This is target request title
     *                     Created By : Venkat
     */
    public static void clickOnRequestLink(String requestTitle) {
        WebDriverTasks.waitForAngular();
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(By.linkText(requestTitle)).click();
    }

    /**
     * This method will verify that expected  status title is displayed on request info page
     *
     * @param expectedTitle This is expected Title name
     * @return Boolean If expected and actual detail matches then it will return True else False
     * Created By : Venkat
     */
    public static boolean verifySavedTitle(String expectedTitle) {
        String actualTitle = WebDriverTasks.waitForElement(savedRequestTitle).getText();
        return expectedTitle.equals(actualTitle);
    }

    /**
     * This method will verify that expected  status detail is displayed on request info page
     *
     * @param expectedDetail This is expected detail
     * @return Boolean If expected and actual detail matches then it will return True else False
     * Created By : Venkat
     */
    public static boolean verifySavedDetail(String expectedDetail) {
        String actualTitle = WebDriverTasks.waitForElement(savedRequestDetail).getText();
        return expectedDetail.equals(actualTitle);
    }

    /**
     * This method will click on Cancel button to navigate back to data grid page
     * Created By : Venkat
     */
    public static void clickOnCancel() {
        WebDriverTasks.waitForElement(cancelRequestButton).click();
    }

    /**
     * This method will change status for provided request title.
     *
     * @param requestTitle This is target request title
     * @param status       This is target status
     *                     Created By : Venkat
     */
    public static void changeStatusTo(String requestTitle, String status) {
        By actionLink = By.xpath(String.format(actionButtonXpath, requestTitle));
        WebDriverTasks.waitForElement(actionLink).click();
        WebDriverTasks.waitForElement(changeStatusLink).click();
        By radioButton = By.xpath(String.format(radioOption, status));
        WebDriverTasks.waitForElement(radioButton).click();
        WebDriverTasks.waitForElement(updateStatusButton).click();
        WebDriverTasks.waitForElement(confirmSuccessPopup).click();
    }

    /**
     * This method verify if the request status is matching with provided status
     *
     * @param requestTitle   This is request title for which the status is to be compared
     * @param expectedStatus This is expected status
     * @return Boolean If the actual status matched with expected status then it will return True else False
     * Created By : Venkat
     */
    public static boolean verifyRequestStatus(String requestTitle, String expectedStatus) {
        By statusLocator = By.xpath(String.format(statusXpath, requestTitle));
        String actualStatus = WebDriverTasks.waitForElement(statusLocator).getText();
        return expectedStatus.equals(actualStatus);
    }

    /**
     * This method will verify if Action button for a Request is enabled or not
     *
     * @param requestTitle This is the request tile name
     * @return Boolean If button is disabled then it will return True else False
     * Created By : Venkat
     */
    public static boolean verifyActionButtonDisabled(String requestTitle) {
        By actionLink = By.xpath(String.format(actionButtonXpath, requestTitle));
        return Boolean.parseBoolean(WebDriverTasks.waitForElement(actionLink).getAttribute("disabled"));
    }
}
