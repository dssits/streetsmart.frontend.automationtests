package nycnet.dhs.streetsmart.pages.reports;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class PreviewReport extends StartWebDriver {

    static By createReport = By.xpath("//button[contains(text(),'Create Report')]");
    static By confirmButton = By.cssSelector("button[class='confirm']");
    static By saveReport = By.xpath("//*[contains(text(),'Yes, save it!')]");
    
    //Values on grid
    static By inactiveOnGrid = By.cssSelector("div[title='Inactive']");
    static By facilityNameBrooklynSkyHotelOnGrid = By.xpath("//*[@title='Brookyn Sky Hotel']");
    static By facilityNameBethelOnGrid = By.xpath("//*[@title='Bethel']");
    
    //Values on columns
    static By facilityNameColumnGrid = By.xpath("//*[@role='columnheader']//*[contains(text(),'Facility Name')]");
	static By facilityAddressColumnGrid = By.xpath("//*[@role='columnheader']//*[contains(text(),'Facility Address')]");
    
    public static void clickOnCreateReport() {
		 WebDriverTasks.waitForElement(createReport).click();
	 }
    
    public static void clickOnSaveReport() {
		 WebDriverTasks.waitForElement(saveReport).click();
	 }
    
    public static void clickOnConfirmButton() {
		 WebDriverTasks.waitForElement(confirmButton).click();
	 }
    
	  public static String returnInactiveOnGrid() {
		     String text = WebDriverTasks.waitForElement(inactiveOnGrid).getText();
		     System.out.println("The Client Category on the grid is " + text);
			 return text;
}
	  
	  public static String returnFacilityNameColumnOnGrid() {
		     String text = WebDriverTasks.waitForElement(facilityNameColumnGrid).getText();
		     System.out.println("The value on column " + text);
			 return text;
}

      public static String returnFacilityAddressColumnOnGrid() {
	     String text = WebDriverTasks.waitForElement(facilityAddressColumnGrid).getText();
	     System.out.println("The value on column " + text);
		 return text;
}
      
      public static String returnFacilityNameBrookynSkyHotelOnGrid() {
 	     String text = WebDriverTasks.waitForElement(facilityNameBrooklynSkyHotelOnGrid).getText();
 	     System.out.println("The value on column " + text);
 		 return text;
 }
      public static String returnFacilityNameBethelOnGrid() {
    	     String text = WebDriverTasks.waitForElement(facilityNameBethelOnGrid).getText();
    	     System.out.println("The value on column " + text);
    		 return text;
    }
   
    
}
