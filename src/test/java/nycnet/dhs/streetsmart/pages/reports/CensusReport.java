package nycnet.dhs.streetsmart.pages.reports;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CensusReport extends StartWebDriver {

    //Added by Venkat
    static By dataGrid = By.cssSelector("div.ui-grid-contents-wrapper");
    static By expandedDataview = By.cssSelector("div.ui-grid-render-container-body>div[role='rowgroup']>div.ui-grid-canvas>div.ui-grid-row>div.expandableRow");
    static By rowExpander = By.cssSelector("div.ui-grid-render-container-left>div[role='rowgroup']>div.ui-grid-canvas>div.ui-grid-row");

    /**
     * This method will verify that data grid is displayed on page
     * Created By : Venakt
     *
     * @return Boolean If data grid is displayed then it wil return True else False
     */
    public static boolean verifyDataGridIsDisplayed() {
        if (WebDriverTasks.verifyIfElementIsDisplayed(dataGrid, 10)) {
            WebDriverTasks.scrollToElement(dataGrid);
            return true;
        }
        return false;
    }

    /**
     * This method will verify that on clicking on Expand icon the data gid is expanded and records are displayed
     * Created By : Venakt
     *
     * @return If grid is expanded then it will return True else False
     */
    public static boolean expandAndVerifyRecord() {
        WebDriverTasks.waitForElement(rowExpander).click();
        WebDriverTasks.wait(5);
        return WebDriverTasks.verifyIfElementIsDisplayed(expandedDataview, 3);
    }

    /**
     * This method will verify that on clicking on Collapse icon the data gid is expanded and records are collapsed
     * Created By : Venakt
     *
     * @return If grid is collapsed then it will return True else False
     */
    public static boolean collapseAndVerifyRecord() {
        WebDriverTasks.waitForElement(rowExpander).click();
        WebDriverTasks.wait(5);
        return !WebDriverTasks.verifyIfElementIsDisplayed(expandedDataview, 3);
    }

}
