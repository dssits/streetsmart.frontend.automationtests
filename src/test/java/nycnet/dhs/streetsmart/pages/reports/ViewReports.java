package nycnet.dhs.streetsmart.pages.reports;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class ViewReports extends StartWebDriver {
    static By viewreportsMenuItem = By.xpath("//a[contains(text(),'View Reports')]");

    static By searchInputBox = By.id("customReportsSearchText");

    static By confirmButton = By.cssSelector("button[class='confirm']");

    static By editButton = By.xpath("//*[@ng-click='EditReport()']");

    //Values on View Report grid
    static By engagedUnshelteredProspectOnGrid = By.cssSelector("div[title='Engaged Unsheltered Prospect']");

    static By inactiveOnGrid = By.cssSelector("div[title='Inactive']");

    static By OtherProspectOnGrid = By.cssSelector("div[title='Other Prospect']");
    static By facilityNameBrooklynSkyHotelOnGrid = By.xpath("//*[@title='Brookyn Sky Hotel']");
    static By facilityNameBethelOnGrid = By.xpath("//*[@title='Bethel']");

    //Columns on View Report grid
    static By facilityNameColumnGrid = By.xpath("//*[@role='columnheader']//*[contains(text(),'Facility Name')]");
    static By facilityAddressColumnGrid = By.xpath("//*[@role='columnheader']//*[contains(text(),'Facility Address')]");

    //Export to Excel locators
    static By exportToExcelAllRecords = By.linkText("All");


    //Added By Venkat
    static By searchBox = By.id("customReportsSearchText");
    static By facilityNameCells = By.cssSelector("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(5)");
    static By facilityAddressCells = By.cssSelector("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(6)");
    static By legalFirstNameCells = By.cssSelector("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(2)");
    static By preferredFirstName = By.cssSelector("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(3)");
    static By placementDescriptionCells = By.cssSelector("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(4)");
    static By allColumns = By.xpath("//div[@role='columnheader']/div[@role='button']/span[contains(@class,'ui-grid-header-cell')]");
    static By exportToExcel = By.xpath("//a[@ng-show='UserHasAllExportPermission' and normalize-space()='Excel']");
    static By exportAll = By.xpath("//ul[not(@style='display: none;') and @style]//a[normalize-space()='All']");
    static By exportCurrentView = By.xpath("//ul[not(@style='display: none;') and @style]//a[normalize-space()='Current View']");
    static By exportCurrentViewConfirmationButton = By.xpath("//button[text()='OK']");
    static By recordsPagerDetails = By.xpath("//div[@class='ui-grid-pager-count']/span");
    static By recordsVisibleOnPage = By.cssSelector("div.ui-grid-render-container-body>div.ui-grid-viewport>div.ui-grid-canvas>div.ui-grid-row");
    static By goToLastPage = By.xpath("//button[@title='Page to last']");
    static By recordsPerPageDropDown = By.xpath("//select[@ng-model='grid.options.paginationPageSize']");
    static By searchForInputBox = By.id("customReportListSearchText");


    public static String returnviewreportsPageHeader() {
        String viewreportsPageHeaderText = WebDriverTasks.waitForElement(viewreportsMenuItem).getText();
        System.out.println("The page header is:" + viewreportsPageHeaderText);
        return viewreportsPageHeaderText;
    }

    public static void clickOnConfirmButton() {
        WebDriverTasks.waitForElement(confirmButton).click();
    }

    public static void clickOnEditButton() {
        WebDriverTasks.waitForElement(editButton).click();
    }

    public static void searchInputBox(String query) {
        WebDriverTasks.waitForElement(searchInputBox).sendKeys(query);
    }

    public static void submitInputBox() {
        WebDriverTasks.waitForElement(searchInputBox).sendKeys(Keys.ENTER);
    }

    public static void clearInputBox() {
        WebDriverTasks.waitForElement(searchInputBox).clear();
    }

    public static void clickByPartialLinkText(String linkName) {
        WebDriverTasks.waitForElement(By.partialLinkText(linkName)).click();
    }

    public static String returnEngagedUnshelteredProspectOnGrid() {
        String text = WebDriverTasks.waitForElement(engagedUnshelteredProspectOnGrid).getText();
        System.out.println("The Client Category on the grid is " + text);
        return text;
    }

    public static String returnInactiveOnGrid() {
        String text = WebDriverTasks.waitForElement(inactiveOnGrid).getText();
        System.out.println("The Client Category on the grid is " + text);
        return text;
    }

    public static String returnOtherProspectOnGrid() {
        String text = WebDriverTasks.waitForElement(OtherProspectOnGrid).getText();
        System.out.println("The Client Category on the grid is " + text);
        return text;
    }

    public static String returnFacilityNameColumnOnGrid() {
        String text = WebDriverTasks.waitForElement(facilityNameColumnGrid).getText();
        System.out.println("The value on column " + text);
        return text;
    }

    public static String returnFacilityAddressColumnOnGrid() {
        String text = WebDriverTasks.waitForElement(facilityAddressColumnGrid).getText();
        System.out.println("The value on column " + text);
        return text;
    }

    public static String returnFacilityNameBrookynSkyHotelOnGrid() {
        String text = WebDriverTasks.waitForElement(facilityNameBrooklynSkyHotelOnGrid).getText();
        System.out.println("The value on column " + text);
        return text;
    }

    public static String returnFacilityNameBethelOnGrid() {
        String text = WebDriverTasks.waitForElement(facilityNameBethelOnGrid).getText();
        System.out.println("The value on column " + text);
        return text;
    }

    //Actions for export to Excel
    public static void clickOnExportToExcelAllRecords() {
        WebDriverTasks.waitForElement(exportToExcelAllRecords).click();
    }

    /**
     * This mwthod will open report as per provided report name
     *
     * @param reportName This is report name.
     */
    public static void openReport(String reportName) {
        WebDriverTasks.waitForElement(By.linkText(reportName)).click();
    }

    /**
     * This will clear existing value in search box and put new provided value and hit enter
     *
     * @param value This is search value
     */
    public static void searchWithValue(String value) {
        WebDriverTasks.waitForElement(searchBox).clear();
        WebDriverTasks.waitForElement(searchBox).sendKeys(value);
        WebDriverTasks.waitForElement(searchBox).sendKeys(Keys.ENTER);
    }

    /**
     * This method will search value and verify it against the column name provided
     *
     * @param columnName This is target column name
     * @param value      This is the search value
     * @return If all the data in column matched expected data them it will return True else False
     */
    public static boolean searchAndVerifyFilteredDataForColumn(String columnName, String value) {
        By targetColumn = By.cssSelector(String.format("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(%d)", getColumnNo(columnName)));
        searchWithValue(value);
        WebDriverTasks.waitForAngular();
        List<WebElement> dataCells = WebDriverTasks.driver.findElements(targetColumn);
        for (WebElement cell : dataCells) {
            if (!(cell.getText().toLowerCase().matches("." + value.toLowerCase() + ".*") || cell.getText().toLowerCase().contains(value.toLowerCase())))
                return false;
        }
        return true;
    }

    /**
     * This method will return column number in data grid
     *
     * @param columnName This is column name
     * @return int This is column number in integer format(column number starts from 1).
     */
    public static int getColumnNo(String columnName) {
        WebDriverTasks.waitForAngular();
        List<WebElement> cols = WebDriverTasks.driver.findElements(allColumns);
        int i = 1;
        for (WebElement col : cols) {
            if (columnName.equals(col.getText()))
                return i;
            i++;
        }
        Assert.fail("Could not find any column with column name: " + columnName);
        return 0;
    }

    /**
     * This method will click on Excel and then All to export all the records in excel
     * Created By : Venkat
     */
    public static void exportAllRecordsToExcel() {
        WebDriverTasks.waitForElement(exportToExcel).click();
        WebDriverTasks.waitForElement(exportAll).click();
    }

    /**
     * This method will click on All and then Current view, after that it wil click on Ok for the confirmation popup
     * Created By : Venkat
     *
     * @throws InterruptedException
     */
    public static void exportCurrentViewToExcel() throws InterruptedException {
        WebDriverTasks.waitForElement(exportToExcel).click();
        WebDriverTasks.waitForElement(exportCurrentView).click();
        if (WebDriverTasks.verifyIfElementIsDisplayed(exportCurrentViewConfirmationButton)) {
            WebDriverTasks.wait(3);
            WebDriverTasks.waitForElement(exportCurrentViewConfirmationButton).click();
            WebDriverTasks.wait(1);
        }
        WebDriverTasks.waitForAngular();
    }

    /**
     * This method will get list of visible columns on data grid
     * Created By : Venkat
     *
     * @return List<String> This is list of column names
     */
    public static List<String> getVisibleColumns() {
        WebDriverTasks.waitForAngular();
        List<String> columnNames = new LinkedList<>();
        List<WebElement> visibleColumns = WebDriverTasks.driver.findElements(allColumns);
        for (WebElement col : visibleColumns) {
            columnNames.add(col.getText().trim());
        }
        return columnNames;
    }

    /**
     * This method will get number of records available for the report
     * Created By : Venkat
     *
     * @return int This is total records count
     */
    public static int getTotalRecordsCount() {
        if(!WebDriverTasks.verifyIfElementIsDisplayed(recordsPagerDetails,7))
            return 0;
        String pagerString = WebDriverTasks.waitForElement(recordsPagerDetails).getText().trim();
        String[] arr = pagerString.split("of");
        String totalRecords = arr[1].trim().replace(" items", "");
        return Integer.parseInt(totalRecords);
    }

    /**
     * This Method will return number of records displayed in data grid in application
     * Created By : Venkat
     *
     * @return int This is records count
     */
    public static int getVisibleRecordsCount() {
        WebDriverTasks.waitForAngular();
        return WebDriverTasks.driver.findElements(recordsVisibleOnPage).size();
    }

    /**
     * This method will verify that the columns displayed in data grid is reflected in exported excel by comparing lists of columns
     * Created By : Venkat
     *
     * @return boolean If the column count and column names matched then it will return True else False
     * @throws IOException
     * @throws InterruptedException
     */
    public static boolean verifyTableColumnsAreDisplayedInExportedExcel() throws IOException, InterruptedException {
        String exportedFileName = Helpers.returnMostRecentFileinDownloadFolder();
        String sheetName = "CustomReport";
        List<String> expectedColumns = getVisibleColumns();
        List<String> actualColumns = Helpers.getAllColumnNames(exportedFileName, sheetName, 0);
        if (actualColumns.size() != expectedColumns.size())
            return false;
        for (int i = 0; i < actualColumns.size(); i++) {
            if (!actualColumns.get(i).equalsIgnoreCase(expectedColumns.get(i)))
                return false;
        }
        return true;
    }

    /**
     * This method will return records/row count in exported excel, excluding column
     * Created By : Venkat
     *
     * @return int This is number of record/rows in excel
     * @throws IOException
     */
    public static int getRecordCountInExportedExcel() throws IOException {
        String exportedFileName = Helpers.returnMostRecentFileinDownloadFolder();
        String sheetName = "CustomReport";
        return Helpers.getRecordCountInExcel(exportedFileName, sheetName) - 1;
    }

    /**
     * This method will change the 'Records per page' value
     * Created By : Venkat
     *
     * @param recordsPerPage This is new records per page value
     */
    public static void changeRecordsPerPageTo(String recordsPerPage) {
        Select paginationDropdown = new Select(WebDriverTasks.waitForElement(recordsPerPageDropDown));
        paginationDropdown.selectByVisibleText(recordsPerPage);
    }

    /**
     * This Method will search for provided value
     * Created By : Venkat
     *
     * @param searchValue This is the value for which we want to search in data grid
     */
    public static void searchFor(String searchValue) {
        WebDriverTasks.waitForElement(searchForInputBox).click();
        WebDriverTasks.waitForElement(searchForInputBox).clear();
        WebDriverTasks.waitForElement(searchForInputBox).sendKeys(searchValue);
        WebDriverTasks.waitForElement(searchForInputBox).sendKeys(Keys.ENTER);
        WebDriverTasks.waitForAngular();
    }
}
