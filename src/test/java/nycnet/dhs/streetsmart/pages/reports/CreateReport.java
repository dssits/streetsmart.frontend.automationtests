package nycnet.dhs.streetsmart.pages.reports;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateReport extends StartWebDriver {
    static By createreportMenuItem = By.xpath("//a[contains(text(),'Create Report')]");

    static By edit = By.cssSelector("i[class='fa fa-pencil cursor-pointer f-s-15']");
    static By save = By.cssSelector("i[class='fa fa-check cursor-pointer f-s-15");
    static By reportName = By.cssSelector("input[ng-model='ReportName']");

    //Demographics section
    static By demographicsSection = By.xpath("(//*[@class='fa fa-plus-circle pull-right'])[1]");
    static By streetSmartIDdemographicsSection = By.id("StreetSmartId");
    static By clientCategoryMenuEngagedUnshelteredProspect = By.cssSelector("label[title='Engaged Unsheltered Prospect']");
    static By clientCategoryMenuInactive = By.cssSelector("label[title='Inactive']");

    //Enrolled section
    static By enrolledClientsSection = By.xpath("(//*[@class='fa fa-plus-circle pull-right'])[2]");
    static By clientCategoryOnEnrolledSection = By.id("ClientCategory");
    static By clientCategoryDropDownOnEnrolledSection = By.cssSelector("span[title='Client Category']");
    static By engagedUnshelteredProspectclientCategoryDropDownOnEnrolledSection = By.cssSelector("span[title='[ Client Category ] Engaged Unsheltered Prospect']");
    static By previewButton = By.xpath("//button[contains(text(),'Preview Report')]");
    static By facilityName = By.id("FacilityName");
    static By facilityNameDropDown = By.id("dropDown_Facility Name");
    static By facilityNameDropDownBrookynSky = By.id("dropDown_[ Facility Name ] Brookyn Sky Hotel");
    static By facilityAddress = By.id("FacilityAddress");

    static By facilityAddressBrookynSkyHotel = By.id("comboBoxBrookyn Sky Hotel");
    static By facilityAddressBethel = By.xpath("//*[@title='Bethel']");

    //Added By : venkat
    static By expandEnrolledClientsPane = By.xpath("//a[normalize-space()='Enrolled Clients']");
    static By clientCategoryCheckBox = By.id("ClientCategory");
    static By clientCategoryButton = By.id("dropDown_Client Category");
    static By engagedUnshelteredProspectOption = By.cssSelector("label[title='Engaged Unsheltered Prospect']>input");
    static By inactiveOption = By.cssSelector("label[title='Inactive']>input");
    static By expandDemographicsPane = By.xpath("//a[@data-toggle='collapse' and normalize-space()='Demographics']/i[@class='fa fa-plus-circle pull-right']");
    static By clientDOBCheckbox = By.xpath("//span[normalize-space()='Date Of Birth']/preceding-sibling::input[@type='checkbox']");
    static By clientDOB = By.xpath("//input[@name='DateOfBirth']");
    static By dateOfBirthYearDropdown = By.xpath("//div[@id='ui-datepicker-div' and contains(@style,'display: block;')]//select[@data-handler='selectYear']");

    public static String returncreatereportPageHeader() {
        String createreportPageHeaderText = WebDriverTasks.waitForElement(createreportMenuItem).getText();
        System.out.println("The page header is:" + createreportPageHeaderText);
        return createreportPageHeaderText;
    }

    public static void clickOnNameEdit() {
        WebDriverTasks.waitForElement(edit).click();
    }

    public static void clickOnSaveButton() {
        WebDriverTasks.waitForElement(save).click();
    }

    public static String enterReportName() {
        String reportNameText = "Automation Report " + Helpers.returnRandomNumber();
        WebDriverTasks.waitForElement(reportName).sendKeys(reportNameText);
        return reportNameText;
    }

    public static String returnReportName() {
        System.out.println(WebDriverTasks.waitForElement(reportName).getText());
        return WebDriverTasks.waitForElement(reportName).getText();
    }

    //Demographics section
    public static void clickOnDemographicsSection() {
        WebDriverTasks.waitForElement(demographicsSection).click();
    }

    public static void clickStreetSmartIDOnDemographicsSection() {
        WebDriverTasks.waitForElement(streetSmartIDdemographicsSection).click();
    }

    //Enrolled section
    public static void clickOnEnrolledClientsSection() {
        WebDriverTasks.waitForElement(enrolledClientsSection).click();
    }

    public static void clickOnclientCategoryOnEnrolledSection() {
        WebDriverTasks.waitForElement(clientCategoryOnEnrolledSection).click();
    }

    public static void clickOnclientCategoryDropDownOnEnrolledSection() {
        WebDriverTasks.waitForElement(clientCategoryDropDownOnEnrolledSection).click();
    }

    public static void clickOnFacilityNameEnrolledSection() {
        WebDriverTasks.waitForElement(facilityName).click();
    }

    public static void clickOnFacilityNameDropDownEnrolledSection() {
        WebDriverTasks.waitForElement(facilityNameDropDown).click();
    }

    public static void clickOnFacilityNameBrookynSkyDropDownEnrolledSection() {
        WebDriverTasks.waitForElement(facilityNameDropDownBrookynSky).click();
    }

    public static void clickOnFacilityAddressEnrolledSection() {
        WebDriverTasks.waitForElement(facilityAddress).click();
    }

    public static void clickOnBrookynSkyHotelFacilityAddressEnrolledSection() {
        WebDriverTasks.waitForElement(facilityAddressBrookynSkyHotel).click();
    }

    public static void clickOnBethelFacilityAddressEnrolledSection() {
        WebDriverTasks.waitForElement(facilityAddressBethel).click();
    }

    public static void clickOnEngagedUnshelteredProspectclientCategoryDropDownOnEnrolledSection() {
        WebDriverTasks.waitForElement(engagedUnshelteredProspectclientCategoryDropDownOnEnrolledSection).click();
    }

    public static void clickOnEngagedUnshelteredProspectOnClientCategoryDropDown() {
        WebDriverTasks.waitForElement(clientCategoryMenuEngagedUnshelteredProspect).click();
    }

    public static void clickOnInactiveClientCategoryDropDown() {
        WebDriverTasks.waitForElement(clientCategoryMenuInactive).click();
    }

    public static void clickOnPreviewButton() {
        WebDriverTasks.waitForElement(previewButton).click();
    }


    /**
     * This method will expand 'Enrolled Clients' pane
     * Created By : Venkat - 07/10/2020
     */
    public static void expandEnrolledClientsPane() {
        WebDriverTasks.waitForElement(expandEnrolledClientsPane).click();
    }

    /**
     * This method will mark client category option checkbox
     * Created By : Venkat - 07/10/2020
     */
    public static void selectClientCategoryCheckbox() {
        if (!WebDriverTasks.waitForElement(clientCategoryCheckBox).isSelected())
            WebDriverTasks.waitForElement(clientCategoryCheckBox).click();
    }

    /**
     * This method will click on Client category dropdown
     * Created By : Venkat - 07/10/2020
     */
    public static void showClientCategoryOptions() {
        if (!WebDriverTasks.waitForElement(clientCategoryButton).isEnabled())
            Assert.fail("Client category dropdown is disabled");
        WebDriverTasks.waitForElement(clientCategoryButton).click();
    }

    /**
     * This method will verify that user is able to see and interact with Unsheltered Prospect option Client in category dropdown
     * Created By : Venkat - 07/10/2020
     *
     * @return If option is displayed and selectable then it will return true else false
     */
    public static boolean verifyEngagedUnshelteredProspectOptionIsSelectable() {
        return verifyClientCategoryOptions(engagedUnshelteredProspectOption);
    }

    /**
     * This method will verify that user is able to see and interact with Inactive option in Client category dropdown
     * Created By : Venkat - 07/10/2020
     *
     * @return If option is displayed and selectable then it will return true else false
     */
    public static boolean verifyInactiveOptionIsSelectable() {
        return verifyClientCategoryOptions(inactiveOption);
    }

    /**
     * This method will verify that user is able to see and interact with client category options
     * Created By : Venkat - 07/10/2020
     *
     * @param by This option locator for client category
     * @return If the checkbox option is displayed and is selectable then it will return true else false
     */
    public static boolean verifyClientCategoryOptions(By by) {
        if (!WebDriverTasks.verifyIfElementExists(by))
            return false;
        //Below condition will check if the checkbox is already selected , if already selected then it will un-check the checkbox
        if (WebDriverTasks.waitForElement(by).isSelected())
            WebDriverTasks.waitForElement(by).click();
        //Below condition will check that if the checkbox is already not selected then only perform check-uncheck validations
        //If the checkbox is already selected then return false as we already followed uncheck action in above condition, so it will return
        //false as the checkbox could not be un-checked
        if (!WebDriverTasks.waitForElement(by).isSelected()) {
            WebDriverTasks.waitForElement(by).click();
            if (WebDriverTasks.waitForElement(by).isSelected()) {
                WebDriverTasks.waitForElement(by).click();
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * This method will expand Demographics pane
     */
    public static void expandDemographicsPane(){
        WebDriverTasks.waitForElement(expandDemographicsPane).click();
    }

    /**
     * This method will mark Date Of Birth checkbox
     */
    public static void markClientDOBCheckBox(){
        WebDriverTasks.waitForElement(clientDOBCheckbox).click();
    }

    /**
     * This method will verify that values in Year dropdown in DOB field starts with expected year and is selectable
     * @param expectedYear This is expected year
     * @return If values in Year dropdown starts with provided values it will return True else False
     */
    public static boolean verifyDOBYearStartsWith(String expectedYear){
        WebDriverTasks.waitForElement(clientDOB).click();
        Select dobDropDown = WebDriverTasks.waitForElementAndSelect(dateOfBirthYearDropdown);
        WebElement firstOption = dobDropDown.getOptions().get(0);
        if (firstOption.getText().trim().equals(expectedYear)) {
            dobDropDown.selectByVisibleText(expectedYear);
            return true;
        } else {
            return false;
        }
    }
}
