package nycnet.dhs.streetsmart.pages.reports;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class CustomReport extends StartWebDriver{
	static By customreportMenuItem = By.xpath("//span[contains(text(),'Custom Reports')]");
	
	public static String returncustomreportPageHeader() {
		String customreportPageHeaderText = WebDriverTasks.waitForElement(customreportMenuItem).getText();
		System.out.println("The page header is:" +customreportPageHeaderText);
		return customreportPageHeaderText;
	}

}
