package nycnet.dhs.streetsmart.pages;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.By;

public class OutreachCapacityManagement extends StartWebDriver {
	static By outreachCapacityManagementMenuItem = By.xpath("//span[contains(text(),'Outreach Capacity Management')]");

	//Added By Venkat
	static By addNewButton = By.xpath("//a[normalize-space()='Add New']");
	static By addNewFacilityPopup = By.xpath("//b[text()='Add New Stabilization Bed Facility']");
	static By saveFacilityButton = By.xpath("//button[@ng-click='SaveFacility()']");
	static By facilityNameInput = By.id("facilitySearch");
	static By noOfBedsForBW = By.id("capacityBW");
	static By boroughDropdown = By.cssSelector("select[ng-options*='BoroughList']");
	static By noOfBedsInput = By.cssSelector("input[ng-model='Capacity']");
	static By streetNoInput = By.cssSelector("input[ng-model='StreetNumber']");
	static By streetNameInput = By.cssSelector("input[ng-model='StreetName']");
	static By zipInput = By.cssSelector("input[ng-model='Zip']");
	static By facilityStartDateInput = By.cssSelector("input[ng-model='FacilityStartDate']");
	static By selectProviderButton = By.cssSelector("button[title='None Selected']");
	static String selectProviderOption = "//label[normalize-space()='%s']/input[@type='checkbox']";


	public static String returnoutreachCapacityManagementPageHeader() {
		String outreachCapacityManagementPageHeaderText = WebDriverTasks.waitForElement(outreachCapacityManagementMenuItem).getText();
		System.out.println("The page header is:" +outreachCapacityManagementPageHeaderText);
		return outreachCapacityManagementPageHeaderText;
	}

	/**
	 * This method will click on Add new button
	 * Created By : Venkat
	 */
	public  static void clickAddNewButton(){
		WebDriverTasks.waitForElement(addNewButton).click();
	}

	/**
	 * This method will click on Save button
	 * Created By : Venkat
	 */
	public static void clickSaveButton(){
		WebDriverTasks.waitForElement(saveFacilityButton).click();
	}

	/**
	 * This method will verify that Add new facility popup is displayed
	 * @return Boolean If a popup is displayed then it will return True else false
	 */
	public static boolean isPopupDisplayed(){
		WebDriverTasks.waitForAngular();
		return WebDriverTasks.verifyIfElementIsDisplayed(addNewFacilityPopup,5);
	}

	/**
	 * This method will set valies for Facility popup
	 * @param facilityName This is facility name
	 * @param borough This is borough name to be selected
	 * @param noOfBeds No of beds to be submitted
	 * @param streetNo Street No
	 * @param streetName Street Name
	 * @param zip ZIP code
	 * @param facilityStartDate Facility Start date
	 * @param provider This is provider name
	 */
	public static void setFacilityInformation(String facilityName,String borough,String noOfBeds,String streetNo,String streetName,String zip,String facilityStartDate,String provider){
		WebDriverTasks.waitForElement(facilityNameInput).sendKeys(facilityName);
		WebDriverTasks.waitForElementAndSelect(boroughDropdown).selectByVisibleText(borough);
		WebDriverTasks.waitForElement(noOfBedsInput).sendKeys(noOfBeds);
		WebDriverTasks.waitForElement(streetNoInput).sendKeys(streetNo);
		WebDriverTasks.waitForElement(streetNameInput).sendKeys(streetName);
		WebDriverTasks.waitForElement(zipInput).sendKeys(zip);
		Base.selectDate(facilityStartDateInput,facilityStartDate);
		WebDriverTasks.waitForElement(selectProviderButton).click();
		WebDriverTasks.waitForElement(By.xpath(String.format(selectProviderOption,provider))).click();
		WebDriverTasks.waitForElement(noOfBedsForBW).sendKeys(noOfBeds);
	}

}
