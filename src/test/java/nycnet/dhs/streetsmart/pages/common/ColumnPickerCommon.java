package nycnet.dhs.streetsmart.pages.common;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class ColumnPickerCommon {

	//column picker locators
    static By columnPicker = By.xpath("//div[@role='button' and @class='ui-grid-icon-container']");
    static By selectedClientCategoryColumn = By.xpath("//button[normalize-space(text())='Client Category']/i[@class='ui-grid-icon-ok']");
    static String selectcolumnOnColumnPicker = "//button[normalize-space(text())='%s']/i[@class='ui-grid-icon-cancel']";
    static String deselectcolumnOnColumnPicker = "//button[normalize-space(text())='%s']/i[@class='ui-grid-icon-ok']";
    
    //column picker actions
    public static void clickOnColumnPicker() {
        WebDriverTasks.waitForElement(columnPicker).click();
    }
    
    public static void selectColumnOnColumnPicker(String columnName) {
        WebDriverTasks.waitForElement(By.xpath(String.format(selectcolumnOnColumnPicker , columnName))).click();
    }
    
    public static void deselectColumnOnColumnPicker(String columnName) {
        WebDriverTasks.waitForElement(By.xpath(String.format(deselectcolumnOnColumnPicker, columnName))).click();
    }
    

}
