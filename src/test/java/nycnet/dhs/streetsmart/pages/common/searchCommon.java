package nycnet.dhs.streetsmart.pages.common;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class searchCommon {
	
	//search locators
	static By searchInputBox  = By.xpath("//*[@ng-blur='searchList()']");
	
	//search actions
	 public static void inputTextInSearchInputBox(String query) {
		 WebDriverTasks.waitForElement(searchInputBox).sendKeys(query);
	 }
	 
	 public static void submitSearchInSearchInputBox() {
		 WebDriverTasks.waitForElement(searchInputBox).sendKeys(Keys.ENTER);
	 }
	 
	 public static void clearSearchInSearchInputBox() {
		 WebDriverTasks.waitForElement(searchInputBox).clear();
	 }

}
