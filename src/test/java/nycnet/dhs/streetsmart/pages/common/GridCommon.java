package nycnet.dhs.streetsmart.pages.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class GridCommon {
	
	//grid locators
	static String columnOnGrid = "//div[@role='button']/span[text()='%s']";
	
	static By firstColumnFirstRowIDOnGrid = By.xpath("//*[@role='grid']/*[@role='rowgroup'][@class='ui-grid-viewport ng-isolate-scope']/*[@class='ui-grid-canvas']/*[@class='ui-grid-row ng-scope']/*[@role='row']/*[@role='gridcell'][1]");
 	 static By secondColumnFirstRowOnGrid = By.xpath("//*[@role='grid']/*[@role='rowgroup'][@class='ui-grid-viewport ng-isolate-scope']/*[@class='ui-grid-canvas']/*[@class='ui-grid-row ng-scope']/*[@role='row']/*[@role='gridcell'][2]");
	
	//grid actions
	 public static boolean isColumnPresentOnGrid(String columnName) {
	        boolean isColumnPresent =  WebDriverTasks.verifyIfElementIsDisplayed(By.xpath(String.format(columnOnGrid, columnName)));
	        return isColumnPresent;
	    }
	 
	 public static void verifyValueOFAllRecordsOnFirstColumn(String expectedRecord) {
	    	List<WebElement> recordsOnFirstColumn = WebDriverTasks.waitForElements(firstColumnFirstRowIDOnGrid);
	    	 for (WebElement record : recordsOnFirstColumn) {
	           String recordOnFirstColumn =  record.getText();
	           System.out.println("The name of the record on the first  column is " + recordOnFirstColumn);
	           Assert.assertEquals(recordOnFirstColumn, expectedRecord);
	         }
	    }
	    
	    public static void verifyValueOFAllRecordsOnSecondColumn(String expectedRecord) {
	    	List<WebElement> recordsOnSecondColumn = WebDriverTasks.waitForElements(secondColumnFirstRowOnGrid);
	    	 for (WebElement record : recordsOnSecondColumn) {
	           String recordOnSecondColumn =  record.getText();
	           System.out.println("The name of the record on the first  column is " + recordOnSecondColumn);
	           Assert.assertEquals(recordOnSecondColumn, expectedRecord);
	         }
	    }
	    
	    public static String returnFirstRecordOnFirstColumn() {
	        String Text = WebDriverTasks.waitForElement(firstColumnFirstRowIDOnGrid).getText();
	        System.out.println("The first record on the first column column is " + Text);
	        return Text;
	    }
	    
	    public static String returnFirstRecordOnSecondColumn() {
	        String Text = WebDriverTasks.waitForElement(secondColumnFirstRowOnGrid).getText();
	        System.out.println("The first record on second column is " + Text);
	        return Text;
	    }

}
