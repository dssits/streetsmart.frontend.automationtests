package nycnet.dhs.streetsmart.pages.common;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class AdvancedFilterCommon {
	
	//Advanced filtering locators
    static By clickOnAdvancedFilter = By.linkText("Advanced Filtering");
    static String selectedFilterOnAdvancedFilters = "//input[@placeholder='%s']";
    
    static By clickOnChooseFilter = By.xpath("//button[@id='dropDown_Add Filter']//b[contains(@class,'caret')]");
    
    static String columnToFilterOnAdvancedFilters = "//*[@ng-repeat='option in (filteredOptions = (multiselectoptions| filter:search))']/label[@title='%s']";
    
    static By clickOnApplyFilter = By.id("nonDHSVeteranDataSaveButton");
    static By clickOnResetFilter = By.xpath("//button[@id='nonDHSVeteranDataCancelButton']");
    static By closeButton = By.xpath("//a[@ng-click='Close()']");
    
    static By enteredValueOnAdvancedFilter = By.xpath("//*[@class='token-input-token']/p");
    
  //advanced filtering actions
    public static void clickOnAdvancedFilter() {
        WebDriverTasks.scrollToElement(clickOnAdvancedFilter);
        WebDriverTasks.waitForElement(clickOnAdvancedFilter).click();
    }
    
    public static void clickOnChooseFiter() {
        WebDriverTasks.waitForElement(clickOnChooseFilter).click();
    }
    
    public static void chooseColumnFilterOnAdvancedFilter(String columnFilter) {
        String selectedColumnFilter = String.format(columnToFilterOnAdvancedFilters, columnFilter);
        WebDriverTasks.waitForElement(By.xpath(selectedColumnFilter)).click();
    }
    
    public static void enterFilterValueOnSelectedFilterOnAdvancedFilter(String selectFilter, String filterValue ) {
        String selectedFilter = String.format(selectedFilterOnAdvancedFilters, selectFilter);
        System.out.println("The name of the column filter is " + selectedFilter );
        System.out.println("The value of the column filter is " + filterValue);
        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).click();
        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).clear();
        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(filterValue);
        WebDriverTasks.specificWait(2000);
        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(Keys.DOWN);
        WebDriverTasks.specificWait(2000);
        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(Keys.ENTER);
    }
    
    public static String returnEnteredValueOnAdvancedFilter() {
        String valueOfAdvancedFilter = WebDriverTasks.waitForElement(enteredValueOnAdvancedFilter).getText();
        System.out.println("The value entered on the selected filter is " + valueOfAdvancedFilter);     
        return valueOfAdvancedFilter;
    }
    
    public static void clickOnResetFilterButton() {
        WebDriverTasks.waitForElement(clickOnResetFilter).click();
    }
    
    public static boolean verifyPresenceOfFilterResetButton() {
        return WebDriverTasks.verifyIfElementExists(clickOnResetFilter);
    }
    
    public static void clickOnApplyFilter() {
        WebDriverTasks.waitForElement(clickOnApplyFilter).click();
    }
    
    public static void clickOnCloseButton() {
        WebDriverTasks.waitForElement(closeButton).click();
    }

}
