package nycnet.dhs.streetsmart.pages.engagements;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

import java.util.LinkedList;
import java.util.List;

public class AllEngagements extends StartWebDriver {

	static By allEngagementsPageHeader = By.xpath("//h1[@class='page-header '][contains(text(), 'All Engagements')]");
	
	//Daily, Monthly, Yearly filters locators
	static By dateFilterDailyButton = By.xpath("//button[contains(text(), 'Daily')]");
	static By dateFilterMonthlyButton = By.xpath("//button[contains(text(), 'Monthly')]");
	static By dateFilterYearlyButton = By.xpath("//button[contains(text(), 'Yearly')]");
	
	//static By dateFilterActiveDay = By.cssSelector("a[class='ui-state-default ui-state-highlight ui-state-active']");
	static By dateFilterActiveDay = By.className("ui-datepicker-days-cell-over.ui-datepicker-current-day.ui-datepicker-today");
	static By dateFilterYearlyMenu  = By.id("ClientEngagementGrid-Yearly");
	static By dateFilterYearlySelect  = By.xpath("//select[@ng-model='selectYear']");
	static By clientAllEngagemementSearchInput = By.id("clntEngSearchText");
	static By StreetSmartID = By.linkText("C160911");
	static By engagedUnshelteredOnGrid  = By.xpath("//*[@title='Engaged Unsheltered Prospect']");
	static By inactiveOnGrid  = By.xpath("//*[@title='Inactive']");
	
	static By otherProspectLegendLessThan3Label  = By.xpath("//*[@class='allEngagementLegends badge bg-green']");
	static By otherProspectLegendMoreThan3Label  = By.xpath("//*[@class='allEngagementLegends badge bg-red']");
	
	public static String returnAllEngagementsPageHeader() {
		  String allEngagementsPageHeaderText = WebDriverTasks.waitForElement(allEngagementsPageHeader).getText();
		  System.out.println("The page header is " + allEngagementsPageHeaderText);
		  return allEngagementsPageHeaderText;
	  }
	
	//Daily, Monthly, Yearly filters actions
	public static void clickOnActiveDay() {
		 WebDriverTasks.waitForElement(dateFilterActiveDay).click();
	 }
	
	public static void clickOnDateFilterDailyButton() {
		 WebDriverTasks.waitForElement(dateFilterDailyButton).click();
	 }
	
	public static void clickOnDateFilterMonthlyButton() {
		 WebDriverTasks.waitForElement(dateFilterMonthlyButton).click();
	 }
	
	public static void clickOnDateFilterYearlyButton() {
		 WebDriverTasks.waitForElement(dateFilterYearlyButton).click();
	 }
	
	public static void clickOnDateFilterYearlyMenu() {
		 WebDriverTasks.waitForElement(dateFilterYearlyMenu).click();
	 }
	
	 public static void inputTextinAllEngagementSearchBox(String searchQuery) {
		 WebDriverTasks.waitForElement(clientAllEngagemementSearchInput).sendKeys(searchQuery);
	  }
	 
	 public static void submitSearchinGlobalSearchBox() {
		 WebDriverTasks.waitForElement(clientAllEngagemementSearchInput).sendKeys(Keys.ENTER);
	 }
	
	public static void selectYearonYearlyFilter(String year) {		
		
		WebDriverTasks.waitForElementAndSelect(dateFilterYearlySelect).selectByVisibleText(year);
	 }
	
	 public static void clickOnStreetSmartID() {
		 WebDriverTasks.waitForElement(StreetSmartID).click();
	 }
	 
	 public static String verifyPresenceOfEngagedUnshelteredOnGrid() {
		 String text = WebDriverTasks.waitForElement(engagedUnshelteredOnGrid).getText();
		 System.out.println("The value on the grid is " + text);
		 return text;
	 }
	 
	 public static String verifyPresenceOfInactiveOnGrid() {
		 String text = WebDriverTasks.waitForElement(inactiveOnGrid).getText();
		 System.out.println("The value on the grid is " + text);
		 return text;
	 }
	 
	 public static String returnOtherProspectLegendLessThan3Label() {
		 String text = WebDriverTasks.waitForElement(otherProspectLegendLessThan3Label).getText();
		 System.out.println("The label on the legend is " + text);
		 return text;
	 }
	 
	 public static String returnOtherProspectLegendMoreThan3Label() {
		 String text = WebDriverTasks.waitForElement(otherProspectLegendMoreThan3Label).getText();
		 System.out.println("The label on the legend is " + text);
		 return text;
	 }
	
	
	

  

    //Added By Venkat
    static By jccClient = By.xpath("//div[@role='button']/span[text()='JCC Client']");
    static By jccClientStartDate = By.xpath("//div[@role='button']/span[text()='JCC Client Start Date']");
    static By preferredPronounColumn = By.xpath("//div[@role='button']/span[text()='Preferred Pronoun']");
    static By preferredNameColumn = By.xpath("//div[@role='button']/span[text()='Preferred Name']");
    static By columnPicker = By.xpath("//div[@role='button' and @class='ui-grid-icon-container']");
    static By selectedJCCClientOptionInColumnPicker = By.xpath("//button[normalize-space(text())='JCC Client']/i[@class='ui-grid-icon-ok']");
    static By selectedJCCClientStartDateOptionInColumnPicker = By.xpath("//button[normalize-space(text())='JCC Client Start Date']/i[@class='ui-grid-icon-ok']");
    static By uncheckedPreferredPronounOptionInColumnPicker = By.xpath("//button[normalize-space(text())='Preferred Pronoun']/i[@class='ui-grid-icon-cancel']");
    static By uncheckedPreferredNameOptionInColumnPicker = By.xpath("//button[normalize-space(text())='Preferred Name']/i[@class='ui-grid-icon-cancel']");
    static By allColumns = By.cssSelector("div[role='columnheader']>div>span.ui-grid-header-cell-label");


    /**
     * This method will check if JCC Client column exists on page
     * Created By : Venkat
     *
     * @return Boolean This will be True if column exists else Flase
     */
    public static boolean verifyJCCClientColumnExists() {
        return WebDriverTasks.verifyIfElementExists(jccClient);
    }

    /**
     * This method will check if JCC Client Start Date column exists on page
     * Created By : Venkat
     *
     * @return Boolean This will be True if column exists else Flase
     */
    public static boolean verifyJCCClientStartDateColumnExists() {
        return WebDriverTasks.verifyIfElementExists(jccClientStartDate);
    }

    /**
     * This method will get all visible column names.
     * Created By : Venkat
     *
     * @return List<String> This is string list of column names
     */
    public static List<String> getDataGridColumns() {
        List<String> colsList = new LinkedList<>();
        WebDriverTasks.waitForAngular();
        List<WebElement> allCols = WebDriverTasks.driver.findElements(allColumns);
        for (WebElement col : allCols)
            colsList.add(col.getText());
        return colsList;
    }

    /**
     * This method will verify if last two visible columns in data grid are 'JCC Client' and 'JCC Client Start Date' or not
     * Created By : Venkat
     *
     * @return If last two columns are as expected then it will return True else False
     */
    public static boolean verifyLastTwoColumnInGridJCCClient() {
        List<String> colsList = getDataGridColumns();
        int totalCols = colsList.size();
        return colsList.get(totalCols - 2).equals("JCC Client") & colsList.get(totalCols - 1).equals("JCC Client Start Date");
    }

    /**
     * This method will click on column picker.
     */
    public static void clickOnColumnPicker() {
        WebDriverTasks.waitForElement(columnPicker).click();
    }

    /**
     * This method will verify that 'JCC Client' option is checked by default or not
     * Created By : Venkat
     *
     * @return boolean If option is checked/selected by default then it will return True else False
     */
    public static boolean verifyJCCClientOptionIsCheckedInColumnPicker() {
        return WebDriverTasks.verifyIfElementIsDisplayed(selectedJCCClientOptionInColumnPicker);
    }

    /**
     * This method will verify that 'JCC Client Start Date' option is checked by default or not
     * Created By : Venkat
     *
     * @return boolean If option is checked/selected by default then it will return True else False
     */
    public static boolean verifyJCCClientStartDateIsCheckedInColumnPicker() {
        return WebDriverTasks.verifyIfElementIsDisplayed(selectedJCCClientStartDateOptionInColumnPicker);
    }

    /**
     * This method will verify that Preferred Pronoun column is not displayed
     * Created By : Venkat
     *
     * @return If column is not displayed then it will return True else False
     */
    public static boolean verifyPreferredPronounColumnIsNotDisplayed() {
        return !WebDriverTasks.verifyIfElementIsDisplayed(preferredPronounColumn);
    }


    /**
     * This method will verify that Preferred Name column is not displayed
     * Created By : Venkat
     *
     * @return If column is not displayed then it will return True else False
     */
    public static boolean verifyPreferredNameColumnIsNotDisplayed() {
        return !WebDriverTasks.verifyIfElementIsDisplayed(preferredNameColumn);
    }

    /**
     * This method will verify that Preferred Pronoun option with unchecked sing is displayed in column picker
     * Created By : Venkat
     * @return If unchecked option is displayed in column picker then it will return True else False
     */
    public static boolean VerifyPreferredPronounOptionIsUncheckedInColumnPicker(){
        return WebDriverTasks.verifyIfElementIsDisplayed(uncheckedPreferredPronounOptionInColumnPicker);
    }

    /**
     * This method will verify that Preferred Name option with unchecked sing is displayed in column picker
     * Created By : Venkat
     * @return If unchecked option is displayed in column picker then it will return True else False
     */
    public static boolean VerifyPreferredNameOptionIsUncheckedInColumnPicker(){
        return WebDriverTasks.verifyIfElementIsDisplayed(uncheckedPreferredNameOptionInColumnPicker);
    }
}

