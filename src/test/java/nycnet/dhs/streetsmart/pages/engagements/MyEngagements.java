package nycnet.dhs.streetsmart.pages.engagements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.WebElement;

import java.util.LinkedList;
import java.util.List;


public class MyEngagements  {
	//static By myEngagementsPageHeader = By.xpath("//h1[@class='page-header'][contains(text(), ' My Engagements ')]");
	static By myEngagementsPageHeader = By.xpath("//h1[@class='page-header']/div");
	static By myEngagementsBreadcrumbs = By.xpath("//ol[@class='breadcrumb pull-right']/li[contains(text(), 'My Engagements')]");
	static By searchInputField = By.id("myEngSearchText");
	//static By providerSelector = By.xpath("//*[@ng-model='OutreachProviderCodeId']");
	static By providerSelector = By.cssSelector("select[ng-model='OutreachProviderCodeId']");
	static By yearlyFilterMenu = By.id("ClientEngagementGrid-Yearly");
	static By inactiveClientCategory = By.cssSelector("div[title='Inactive']");
	static By engagedUnshelteredProspectClientCategory = By.cssSelector("div[title='Engaged Unsheltered Prospect']");
	
	 public static String returnMyEngagementsPageHeader() {
		 String myEngagementsPageHeaderText = WebDriverTasks.waitForElement(myEngagementsPageHeader).getText();
		 System.out.println("The My Engagements page header is " +  myEngagementsPageHeaderText);
		  return  myEngagementsPageHeaderText;
		  
	  }
	 
	 public static String returnMyEngagementsBreadcrumb() {
		 String myEngagementsBreadCrumbText = WebDriverTasks.waitForElement(myEngagementsBreadcrumbs).getText();
		 System.out.println("The My Engagements Breadcrumb is " + myEngagementsBreadCrumbText);
		  return myEngagementsBreadCrumbText;
		  
	  }
	 
	 public static String returnInactiveClientCategory() {
		 String inactiveClientCategoryText = WebDriverTasks.waitForElement(inactiveClientCategory).getText();
		 System.out.println("The Client Category is " + inactiveClientCategoryText);
		  return inactiveClientCategoryText;
		  
	  }
	 
	 public static void verifyInactiveCellsInClientCategoryColumn() {
		 List<WebElement> clientCategoryColumn = WebDriverTasks.waitForElements(inactiveClientCategory);
	     for(WebElement clientCategoryCell : clientCategoryColumn) {
	    	 String clientCategoryText = clientCategoryCell.getText();
	    	 System.out.println("The Client Category is " + clientCategoryText);
	    	 Assert.assertEquals(clientCategoryText, "Inactive");
	     }
	 }
	     
	     public static void verifyEngagedUnshelteredProspectCellsInClientCategoryColumn() {
			 List<WebElement> clientCategoryColumn = WebDriverTasks.waitForElements(engagedUnshelteredProspectClientCategory);
		     for(WebElement clientCategoryCell : clientCategoryColumn) {
		    	 String clientCategoryText = clientCategoryCell.getText();
		    	 System.out.println("The Client Category is " + clientCategoryText);
		    	 Assert.assertEquals(clientCategoryText, "Engaged Unsheltered Prospect");
		     }
	        
	 }
	 
	 public static String returnEngagedUnshelteredProspectCategory() {
		 String Text = WebDriverTasks.waitForElement(engagedUnshelteredProspectClientCategory).getText();
		 System.out.println("The Client Category is " + Text);
		  return Text;
		  
	  }
	 
	 public static void inputTextInSearchField(String query) {
		 WebDriverTasks.waitForElement(searchInputField).sendKeys(query);
		 System.out.println("Searched for " + query);
	  } 
	 
	 public static void submitSearchField() {
		 WebDriverTasks.waitForElement(searchInputField).sendKeys(Keys.ENTER);
	  } 
	 
	 public static void clickOnYearlyDateFilterMenu() {
		 WebDriverTasks.waitForElement(yearlyFilterMenu).click();
	  }
	 
	 public static void selectProvider(String provider) {
		 WebDriverTasks.waitForElementAndSelect(providerSelector).selectByVisibleText(provider);
	  } 


 


    //Added By Venkat
    static By jccClient = By.xpath("//div[@role='button']/span[text()='JCC Client']");
    static By jccClientStartDate = By.xpath("//div[@role='button']/span[text()='JCC Client Start Date']");
    static By preferredPronounColumn = By.xpath("//div[@role='button']/span[text()='Preferred Pronoun']");
    static By preferredNameColumn = By.xpath("//div[@role='button']/span[text()='Preferred Name']");
    static By columnPicker = By.xpath("//div[@role='button' and @class='ui-grid-icon-container']");
    static By selectedJCCClientOptionInColumnPicker = By.xpath("//button[normalize-space(text())='JCC Client']/i[@class='ui-grid-icon-ok']");
    static By selectedJCCClientStartDateOptionInColumnPicker = By.xpath("//button[normalize-space(text())='JCC Client Start Date']/i[@class='ui-grid-icon-ok']");
    static By uncheckedPreferredPronounOptionInColumnPicker = By.xpath("//button[normalize-space(text())='Preferred Pronoun']/i[@class='ui-grid-icon-cancel']");
    static By uncheckedPreferredNameOptionInColumnPicker = By.xpath("//button[normalize-space(text())='Preferred Name']/i[@class='ui-grid-icon-cancel']");
    static By allColumns = By.cssSelector("div[role='columnheader']>div>span.ui-grid-header-cell-label");



    /**
     * This method will check if JCC Client column exists on page
     * Created By : Venkat
     *
     * @return Boolean This will be True if column exists else Flase
     */
    public static boolean verifyJCCClientColumnExists() {
        return WebDriverTasks.verifyIfElementExists(jccClient);
    }

    /**
     * This method will check if JCC Client Start Date column exists on page
     * Created By : Venkat
     *
     * @return Boolean This will be True if column exists else Flase
     */
    public static boolean verifyJCCClientStartDateColumnExists() {
        return WebDriverTasks.verifyIfElementExists(jccClientStartDate);
    }

    /**
     * This method will get all visible column names.
     * Created By : Venkat
     *
     * @return List<String> This is string list of column names
     */
    public static List<String> getDataGridColumns() {
        List<String> colsList = new LinkedList<>();
        WebDriverTasks.waitForAngular();
        List<WebElement> allCols = WebDriverTasks.driver.findElements(allColumns);
        for (WebElement col : allCols)
            colsList.add(col.getText());
        return colsList;
    }

    /**
     * This method will verify if last two visible columns in data grid are 'JCC Client' and 'JCC Client Start Date' or not
     * Created By : Venkat
     *
     * @return boolean If last two columns are as expected then it will return True else False
     */
    public static boolean verifyLastTwoColumnInGridJCCClient() {
        List<String> colsList = getDataGridColumns();
        int totalCols = colsList.size();
        return colsList.get(totalCols - 2).equals("JCC Client") & colsList.get(totalCols - 1).equals("JCC Client Start Date");
    }

    /**
     * This method will click on column picker.
     * Created By : Venkat
     */
    public static void clickOnColumnPicker() {
        WebDriverTasks.waitForElement(columnPicker).click();
    }

    /**
     * This method will verify that 'JCC Client' option is checked by default or not
     * Created By : Venkat
     *
     * @return boolean If option is checked/selected by default then it will return True else False
     */
    public static boolean verifyJCCClientOptionIsCheckedInColumnPicker() {
        return WebDriverTasks.verifyIfElementIsDisplayed(selectedJCCClientOptionInColumnPicker);
    }

    /**
     * This method will verify that 'JCC Client Start Date' option is checked by default or not
     * Created By : Venkat
     *
     * @return boolean If option is checked/selected by default then it will return True else False
     */
    public static boolean verifyJCCClientStartDateIsCheckedInColumnPicker() {
        return WebDriverTasks.verifyIfElementIsDisplayed(selectedJCCClientStartDateOptionInColumnPicker);
    }

    /**
     * This method will verify that Preferred Pronoun column is not displayed
     * Created By : Venkat
     *
     * @return If column is not displayed then it will return True else False
     */
    public static boolean verifyPreferredPronounColumnIsNotDisplayed() {
        return !WebDriverTasks.verifyIfElementIsDisplayed(preferredPronounColumn);
    }


    /**
     * This method will verify that Preferred Name column is not displayed
     * Created By : Venkat
     *
     * @return If column is not displayed then it will return True else False
     */
    public static boolean verifyPreferredNameColumnIsNotDisplayed() {
        return !WebDriverTasks.verifyIfElementIsDisplayed(preferredNameColumn);
    }

    /**
     * This method will verify that Preferred Pronoun option with unchecked sing is displayed in column picker
     * Created By : Venkat
     *
     * @return If unchecked option is displayed in column picker then it will return True else False
     */
    public static boolean VerifyPreferredPronounOptionIsUncheckedInColumnPicker() {
        return WebDriverTasks.verifyIfElementIsDisplayed(uncheckedPreferredPronounOptionInColumnPicker);
    }


    /**
     * This method will verify that Preferred Name option with unchecked sing is displayed in column picker
     * Created By : Venkat
     *
     * @return If unchecked option is displayed in column picker then it will return True else False
     */
    public static boolean VerifyPreferredNameOptionIsUncheckedInColumnPicker() {
        return WebDriverTasks.verifyIfElementIsDisplayed(uncheckedPreferredNameOptionInColumnPicker);
    }
}

