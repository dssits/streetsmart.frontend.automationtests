package nycnet.dhs.streetsmart.pages.engagements;

import java.util.List;

import org.openqa.selenium.By;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;


import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class ClientAndEngagementSearch {


	 static By duplicateSearchPageHeader = By.xpath("//h4[@class='panel-title']/*[contains(text(), 'Duplicate Search')]");
	 
	 static By duplicateSearchFirstName = By.xpath("(//*[@id='first-name'])[1]");
	 static By duplicateSearchLastName = By.xpath("(//*[@id='last-name'])[1]");
	 
	 static By duplicateSearchButton = By.xpath("(//*[@id='cint-info-save'])[1]");
	 
	 static By duplicateSearchAddNewClient = By.xpath("(//*[@id='data-table_wrapper']//a[contains(text(),'Add New Client')])[1]");

	 static By clientSearchFirstName = By.xpath("(//*[@id='first-name'])[2]");
	 static By clientSearchLastName = By.xpath("(//*[@id='last-name'])[2]");
	 
	 static By clientSearchButton = By.xpath("(//*[@id='cint-info-save'])[2]");
	 static By clientSearchAddNewClient = By.xpath("(//*[@id='data-table_wrapper']//a[contains(text(),'Add New Client')])[2]");
	 static By expandClientSearchButton = By.id("panel-body-collapse");
	 static By duplicateSearchExpandClientIDDetails = By.id("dubiconaccordion2");
	 static By duplicateSearchClientCategoryCaseload = By.xpath("(//*[@ng-model='ClientCategory']/*[@label='Caseload'])[1]");
	 static By duplicateSearchClientCategoryOtherProspect = By.xpath("(//*[@ng-model='ClientCategory']/*[@label='Other Prospect'])[1]");
	 static By duplicateSearchClientCategoryEnrollmentEnded = By.xpath("(//*[@ng-model='ClientCategory']/*[@label='Enrollment Ended'])[1]");
	 static By duplicateSearchClientCategoryEngagedUnshelteredProspect = By.xpath("(//*[@ng-model='ClientCategory']/*[@label='Engaged Unsheltered Prospect'])[1]");
	 static By duplicateSearchClientCategoryInactive = By.xpath("(//*[@ng-model='ClientCategory']/*[@label='Inactive'])[1]");
	 static By duplicateSearchClientCategoryEngagedUnshelteredProspectOnGrid = By.cssSelector("div[title='Engaged Unsheltered Prospect']");
	 
	 static By clientCategoryColumnOnDuplicateSearch = By.cssSelector("#clinetSearchPanel div[title='Client Category']");
		
	 static By clientCategoryValueongridOnDuplicateSearch = By.cssSelector("div[title='Engaged Unsheltered Prospect']");
	 static By duplicateSearchColumnPicker = By.xpath("(//*[@class='ui-grid-menu-button ng-scope'])[1]");
	 static By clientCategoryOnColumnPicker = By.xpath("//*[@class='ui-grid-menu-item ng-binding'][contains(text(), 'Client Category')]");
	 
	 
	 
	 static By engagedUnshelteredButton = By.id("EngagedUnshelteredButton");
	 
	 static By clientCategory = By.xpath("(//*[@ng-model='ClientCategory'])[1]");
	
	 public static String returnDuplicateSearchPageHeader() {
		  String duplicateSearchPageHeaderText =  WebDriverTasks.waitForElement(duplicateSearchPageHeader).getText();
		  System.out.println("The page header is " + duplicateSearchPageHeaderText);
		  return duplicateSearchPageHeaderText;
	  }
	 
	 public static void expandDuplicateSearchClientIDDetails () {
		 WebDriverTasks.waitForElement(duplicateSearchExpandClientIDDetails).click();
	 }
	 
	 public static String returnduplicateSearchClientCategoryCaseload () {
		 return WebDriverTasks.waitForElement(duplicateSearchClientCategoryCaseload).getText();
		
	 }
	 
	 public static String returnduplicateSearchClientCategoryOtherProspect () {
		 return WebDriverTasks.waitForElement(duplicateSearchClientCategoryOtherProspect).getText();
		
	 }
	 
	 public static String returnduplicateSearchClientCategoryEnrollmentEnded () {
		 return WebDriverTasks.waitForElement(duplicateSearchClientCategoryEnrollmentEnded).getText();
		
	 }
	 
	 public static String returnduplicateSearchClientCategoryEngagedUnshelteredProspect () {
		 return WebDriverTasks.waitForElement(duplicateSearchClientCategoryEngagedUnshelteredProspect).getText();
	 }
	 
	 public static String returnduplicateSearchClientCategoryInactive () {
		 return WebDriverTasks.waitForElement(duplicateSearchClientCategoryInactive).getText();
		
	 }
	 
	 public static String returnduplicateSearchEngagedUnshelteredProspectOnGrid () {
		 String text = WebDriverTasks.waitForElement(duplicateSearchClientCategoryEngagedUnshelteredProspectOnGrid).getText();
		 System.out.println("The Client Category on the grid is " + text);
		 return text;
	 }
	
	 public static void selectDuplicateSearachClientCategory (String clientCategoryvalue) {
		 WebDriverTasks.waitForElementAndSelect(clientCategory).selectByVisibleText(clientCategoryvalue);
	 }
	 
	 public static void returnDuplicateSearchClientCategories () {
		List <WebElement> clientCategoryList = WebDriverTasks.waitForElementAndSelect(clientCategory).getOptions();
		
		 for(WebElement clientCategory : clientCategoryList) {
	    	 String clientCategoryText = clientCategory.getText();
	    	 System.out.println("The Client Category is " + clientCategoryText);
	    	
	     }
	 }
	 
	 public static String returnclientCategoryColumnOnDuplicateSearch() {
		  String Text =  WebDriverTasks.waitForElement(clientCategoryColumnOnDuplicateSearch).getText();
		  System.out.println("The value of the column is  " + Text);
		  return Text;
	  }
	 
	 public static void inputTextinFirstNameField(String firstName) {
		 WebDriverTasks.waitForElement(duplicateSearchFirstName ).sendKeys(firstName);
	  }
	 
	 public static void inputTextinLastNameField(String lastName) {
		 WebDriverTasks.waitForElement(duplicateSearchLastName ).sendKeys(lastName);
	  }
	 
	 public static void clickOnDuplicateSearchAddNewClient () {
		 WebDriverTasks.waitForElement(duplicateSearchAddNewClient).click();
	 }
	 
	 
	 public static void clickOnDupliceSearchButton () {
		 WebDriverTasks.waitForElement(duplicateSearchButton).click();
	 }
	 
	 public static boolean verifyIfclientCategoryColumnExistsOnDuplicateSearch() {
		 boolean clientCategoryIsPresent =  WebDriverTasks.verifyIfElementExists(clientCategoryColumnOnDuplicateSearch);
		  System.out.println("The presence of the column is  " + clientCategoryIsPresent);
		  return clientCategoryIsPresent;
	  }
	 
	 public static String returnclientCategoryValueongridOnDuplicateSearch() {
		  String Text =  WebDriverTasks.waitForElement(clientCategoryValueongridOnDuplicateSearch).getText();
		  System.out.println("The value of the cell on the grid  " + Text);
		  return Text;
	  }
	 
	 public static void clickOnColumnPickerOnDuplicateSearch () {
		 WebDriverTasks.waitForElement(duplicateSearchColumnPicker).click();
	 }
	 
	 //Will work on both Client Search grid and Duplicate Search grid
	 public static void clickOnclientCategoryOnColumnPicker () {
		 WebDriverTasks.waitForElement(clientCategoryOnColumnPicker).click();
	 }
	 
	 public static void expandClientSearchPanel () {
		 WebDriverTasks.waitForElement(expandClientSearchButton).click();
	 }
	 
	 public static void inputTextinClientSearchFirstNameField(String firstName) {
		 WebDriverTasks.waitForElement(clientSearchFirstName).sendKeys(firstName);
	  }
	 
	 public static void inputTextinClientSearchLastNameField(String lastName) {
		 WebDriverTasks.waitForElement(clientSearchLastName).sendKeys(lastName);
	  }
	 
	 public static void clickOnClientSearchButton () {
		 WebDriverTasks.waitForElement(clientSearchButton).click();
	 } 
	 
	 public static void scrollToClientSearchButton () {
		 WebDriverTasks.scrollToElement(clientSearchButton);
	 } 
	 
	 public static void clickOnClientSearchAddNewClient () {
		 WebDriverTasks.waitForElement(clientSearchAddNewClient).click();
	 }
	 
	 public static void clickOnEngagedUnshelteredButton () {
		 WebDriverTasks.waitForElement(engagedUnshelteredButton).click();
	 }
	 
	
	 
	 
	 
	 

   

    //Added by Venkat
    static By unExpandClientID_ClientSearch = By.xpath("//div[contains(@class,'new-expanded-panel')]//b[@id='iconaccordion2' and @aria-expanded='false']/..");
    static By clientIDSectionLabel= By.xpath("//div[contains(@class,'new-expanded-panel')]//b[@id='iconaccordion2']/..");
    static By clientCategoryDropdown_ClientSearch = By.xpath("//div[contains(@class,'new-expanded-panel')]//select[@ng-model='ClientCategory']");
    static By clientSearchButton_ClientSearch = By.xpath("//div[contains(@class,'new-expanded-panel')]//button[text()='Search']");
    static By clientCategoryColumn_ClientSearch = By.xpath("//div[contains(@class,'new-expanded-panel')]//div[@title='Client Category' and @role='button']");
    static By columnPicker_ClientSearchPane = By.xpath("//div[contains(@class,'new-expanded-panel')]//div[contains(@class,'ui-grid-menu-button')]");
    static By clientCategory_ColPicker_ClientSearchPane = By.xpath("//div[contains(@class,'new-expanded-panel')]//button[text()=' Client Category' and not(contains(@class,'ng-hide'))]");
    static By clientCategoryColumnData = By.cssSelector("div.ui-grid-canvas>div.ui-grid-row>div>div[role='gridcell']:nth-child(8)>div");
    static By hideClientCategoryColumn = By.xpath("//button[normalize-space(text())='Client Category' and not(contains(@class,'ng-hide'))]/i[@class='ui-grid-icon-ok']");
    static By showClientCategoryColumn = By.xpath("//button[normalize-space(text())='Client Category' and not(contains(@class,'ng-hide'))]/i[@class='ui-grid-icon-cancel']");

   

    /**
     * This method will expand Client ID section in Client and Engagement search pane
     * Added By : Venkat - 07/08/2020
     */
    public static void expandClientIDSectionInClientSearch() {
        WebDriverTasks.waitForAngular();
        WebDriverTasks.waitForElement(clientIDSectionLabel).click();
        if (WebDriverTasks.verifyIfElementExists(unExpandClientID_ClientSearch)) {
            WebDriverTasks.waitForElement(unExpandClientID_ClientSearch).click();
        }
    }

    /**
     * This method will verify that Client Category dropdown is displayed in Client search pane
     * Added By : Venkat - 07/08/2020
     *
     * @return If the dropdown is displayed then it will return True else False
     */
    public static boolean verifyClientCategoryDisplayedInClientSearch() {
        WebDriverTasks.waitForAngular();
        return WebDriverTasks.verifyIfElementExists(clientCategoryDropdown_ClientSearch);
    }

    /**
     * This method will verify that the options displayed in Client Category dropdown matches with expected values "","Caseload",
     * "Other Prospect","Enrollment Ended","Engaged Unsheltered Prospect","Inactive"
     *
     * @return If the values matched then it will return True else False
     */
    public static boolean verifyClientCategoryOptionsInClientSearch() {
        LinkedList<String> expectedOptions = new LinkedList<>(Arrays.asList("", "Caseload", "Other Prospect", "Enrollment Ended", "Engaged Unsheltered Prospect", "Inactive"));

        Select selectClientCategory = new Select(WebDriverTasks.waitForElement(clientCategoryDropdown_ClientSearch));
        List<WebElement> availableOptions = selectClientCategory.getOptions();

        //If number of options available in Client Category does not match that of expected options then return false
        if (expectedOptions.size() != availableOptions.size())
            return false;

        for (WebElement option : availableOptions) {
            String optionText = option.getText().trim();
            if (!expectedOptions.contains(optionText))
                return false;
        }
        return true;
    }

    /**
     * This method will click on 'Search' button in Client Search pane
     * Added By : Venkat - 07/08/2020
     */
    public static void clickOnSearchButtonInClientSearchPane() {
        WebDriverTasks.waitForElement(clientSearchButton_ClientSearch).click();
    }

    /**
     * This method will verify if 'Client Category' column is displayed or not
     * Added By : Venkat - 07/08/2020
     *
     * @return This will be True if column is displayed else False
     */
    public static boolean verifyClientCategoryColumnInClientSearchGrid() {
        if (WebDriverTasks.verifyIfElementExists(clientCategoryColumn_ClientSearch))
            return WebDriverTasks.waitForElement(clientCategoryColumn_ClientSearch).isDisplayed();
        else
            return false;
    }

    /**
     * This method will click on column picker in Client search pane
     * Added By : Venkat - 07/09/2020
     */
    public static void clickOnColumnPickerInClientSearchPane() {
        WebDriverTasks.waitForElement(columnPicker_ClientSearchPane).click();
    }

    /**
     * This method will check for Client Category option in column picker
     * Added By : Venkat - 07/09/2020
     *
     * @return This will be True if Client category option is displayed else False
     */
    public static boolean verifyClientCategoryIsDisplayedInColumnPicker() {
        if (WebDriverTasks.verifyIfElementExists(clientCategory_ColPicker_ClientSearchPane)) {
            return WebDriverTasks.waitForElement(clientCategory_ColPicker_ClientSearchPane).isDisplayed();
        } else {
            return false;
        }
    }


    /**
     * This meyhod will select client category option in client category dropdown
     * Created By : Venkat - 07/19
     *
     * @param option This is client category dropdown option
     */
    public static void selectClientCategory(String option) {
        Select selectClientCategory = new Select(WebDriverTasks.waitForElement(clientCategoryDropdown_ClientSearch));
        selectClientCategory.selectByVisibleText(option);
    }

    /**
     * This method will verify that client category data is matching the filtered data.
     * It will check the data for first pagination only.
     * Created By : Venkat - 07/19
     *
     * @param filteredBy This is string value from which the data is filtered by and also it will be the expected value for the column Client Category
     * @return If the data matched with expected data then it will be True else false
     */
    public static boolean verifyClientCategoryColumnDataIs(String filteredBy) {
        WebDriverTasks.waitForAngular();
        List<WebElement> clientCategoryCells = WebDriverTasks.driver.findElements(clientCategoryColumnData);
        for (WebElement cell : clientCategoryCells) {
            if (!filteredBy.equals(cell.getText()))
                return false;
        }
        return true;
    }

    /**
     * This method will Show/Hide column based on boolean value passed.
     * It will also check that the column is displayed/hidden based on type of selection
     * Created By : Venkat - 07/19
     *
     * @param show This is boolean value.If user want to show the column in data grid then it should be True, if want to hide
     *             then it should be False
     * @return boolean It will be True if parameter is true and column is displayed and if parameter is false and column is hidden in other case it will return False
     */
    public static boolean verifyHideColumnPickerFunctionalityForClientCategory(boolean show) {
        if (show) {
            WebDriverTasks.waitForElement(showClientCategoryColumn).click();
            return verifyClientCategoryColumnInClientSearchGrid();
        } else {
            WebDriverTasks.waitForElement(hideClientCategoryColumn).click();
            return !verifyClientCategoryColumnInClientSearchGrid();
        }

    }

}
