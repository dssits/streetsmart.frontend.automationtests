package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class Panhandling extends StartWebDriver{
	
	static By panhandlingMenuItem = By.xpath("//a[contains(text(),'Panhandling')]");
	static By dateFilterYearlyMenu  = By.id("panhandlingGrid-Yearly");
	
	public static String returnPanhandlingPageHeader() {
		String panhandlingPageHeaderText = WebDriverTasks.waitForElement(panhandlingMenuItem).getText();
		System.out.println("The page header is " +panhandlingPageHeaderText);
		return panhandlingPageHeaderText;
		
	}
	
	public static void clickOnDateFilterYearlyMenu() {
		 WebDriverTasks.waitForElement(dateFilterYearlyMenu).click();
	 }
	
	

}
