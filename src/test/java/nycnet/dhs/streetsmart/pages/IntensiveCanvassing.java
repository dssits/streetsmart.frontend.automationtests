package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class IntensiveCanvassing extends StartWebDriver {

    static By intensivecanvassingMenuItem = By.xpath("//a[contains(text(),'Intensive Canvassing')]");
    static By expandedDataview = By.cssSelector("div.ui-grid-render-container-body>div[role='rowgroup']>div.ui-grid-canvas>div.ui-grid-row>div.expandableRow");
    static By rowExpander = By.cssSelector("div.ui-grid-render-container-left>div[role='rowgroup']>div.ui-grid-canvas>div.ui-grid-row>div>div");

    public static String returnintensivecanvassingPageHeader() {
        String intensivecanvassingPageHeaderText = WebDriverTasks.waitForElement(intensivecanvassingMenuItem).getText();
        System.out.println("The page header is:" + intensivecanvassingPageHeaderText);
        return intensivecanvassingPageHeaderText;
    }

    /**
     * This method will verify that on clicking on Expand icon the data gid is expanded and records are displayed
     * Created By : Venakt
     *
     * @return If grid is expanded then it will return True else False
     */
    public static boolean expandAndVerifyRecord() {
        WebDriverTasks.waitForElement(rowExpander).click();
        WebDriverTasks.wait(5);
        return WebDriverTasks.verifyIfElementIsDisplayed(expandedDataview, 3);
    }

    /**
     * This method will verify that on clicking on Collapse icon the data gid is expanded and records are collapsed
     * Created By : Venakt
     *
     * @return If grid is collapsed then it will return True else False
     */
    public static boolean collapseAndVerifyRecord() {
        WebDriverTasks.waitForElement(rowExpander).click();
        WebDriverTasks.wait(5);
        return !WebDriverTasks.verifyIfElementIsDisplayed(expandedDataview, 3);
    }
}
