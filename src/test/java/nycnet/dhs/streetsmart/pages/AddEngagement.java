package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class AddEngagement extends StartWebDriver {

    static By myEngagementsPageHeader = By.xpath("//h1[@class='page-header '][contains(text(), 'My Engagements')]");

    // click on the engagements tab on clients details page
    static By engagementsTab = By.id("engagementtab");

    // click on the add new button
    static By addnewEngagement = By.id("clientengagementaddnew");

    // enter the details in the form
    static By selectProvider = By.xpath("//*[@ng-model='ClientEngagement.ProviderId']");
    static By selectArea = By.id("ddnArea");
    static By selectShift = By.xpath("//*[@ng-model='ClientEngagement.ShiftInfo.ShiftId']");
    static By enterTeam = By.id("teamNameId");

    static By selectJointOutreach = By.xpath("//*[@ng-model='ClientEngagement.ShiftInfo.JointOutreachId']");
    static By selectContactReason = By.xpath("//*[@ng-model='ClientEngagement.ContactReason']");
    static By selectContactDateField = By.xpath("//*[@ng-model='ClientEngagement.ContactDate']");
    static By selectDatePickerPrevious = By.xpath("/html[1]/body[1]/div[4]/div[1]/a[1]");
    static By selectFirstDate = By.linkText("30");
    static By selectDatePickerNext = By.xpath("/html[1]/body[1]/div[4]/div[1]/a[2]");
    static By selectDatePickerNext2 = By.xpath("//*[@class='ui-datepicker-next ui-corner-all']");
    static By selectSecondDate = By.linkText("1");
    static By selectThirdDate = By.linkText("3");
    static By selectContactTime = By.id("timepicker1");

    static By selectEngagementOutcome = By.xpath("//*[@ng-model='ClientEngagement.ClientEngagementOutcomeId']");

    static By selectBorough = By.xpath("//*[@ng-model='ClientEngagement.LocationInfo.BoroughId']");
    static By selectLocationType = By.xpath("//*[@ng-model='ClientEngagement.LocationInfo.LocationTypeId']");

    static By selectParkName = By.id("engagementParkNameSearch");
    static By addEngagementNotes = By.xpath("//*[@ng-model='ClientEngagement.Notes']");

    static By selectSaveEngagement = By.xpath("//div[@class='col-md-12 text-right p-t-5 overflow-hidden']//button[2]");

    static By verifyEngagementProvider = By.cssSelector("span[title='Breaking Ground']");
    static By errorMessage = By.xpath("//*[@id='errorDiv']//span");

    //Added By Venkat
    static By clientDOB = By.xpath("//div[(@id='clinetSearchPanel' or @class='panel-body') and not(@style='display: none;')]//div[@id='dubcollapse1' or @id='collapse1' ]//input[@name='DateOfBirth']");
    static By dateOfBirthYearDropdown = By.xpath("//div[@id='ui-datepicker-div' and contains(@style,'display: block;')]//select[@data-handler='selectYear']");

    static By collapseDuplicateSearchButton = By.xpath("//h4/b[text()='Duplicate Search']/ancestor::div[@class='panel-heading']//a[@data-click='panel-collapse']/i[@class='fa fa-minus']");
    static By expandClientSearchButton = By.xpath("//h4/b[text()='Client and Engagement Search']/ancestor::div[@class='panel-heading']//a[@data-click='panel-collapse']/i[@class='fa fa-plus']");
    //-----

    public static String returnMyEngagementsPageHeader() {
        return WebDriverTasks.waitForElement(myEngagementsPageHeader).getText();
    }

    public static void clickOnEngagementTab() {
        WebDriverTasks.waitForElement(engagementsTab).click();
    }

    public static void clickOnAddNewEngagementButton() {
        WebDriverTasks.waitForElement(addnewEngagement).click();
    }

    public static void selectEngagementProvider(String provider) {
        WebDriverTasks.waitForElementAndSelect(selectProvider).selectByVisibleText(provider);
    }

    public static void selectEngagementArea(String area) {
        WebDriverTasks.waitForElementAndSelect(selectArea).selectByVisibleText(area);
    }

    public static void selectEngagementShift(String shift) {
        WebDriverTasks.waitForElementAndSelect(selectShift).selectByVisibleText(shift);
    }

    public static void selectEngagementJointOutreach(String jointoutreach) {
        WebDriverTasks.waitForElementAndSelect(selectJointOutreach).selectByVisibleText(jointoutreach);
    }

    public static void selectEngagementContactReason(String contactreason) {
        WebDriverTasks.waitForElementAndSelect(selectContactReason).selectByVisibleText(contactreason);

    }

    public static void selectEngagementContactDateField() {
        WebDriverTasks.waitForElement(selectContactDateField).click();
    }

    public static void selectEngagementDatePickerPrevious() {
        WebDriverTasks.waitForElement(selectDatePickerPrevious).click();
    }

    public static void selectEngagementFirstDate() {
        WebDriverTasks.waitForElement(selectFirstDate).click();
        System.out.println("First Engagement created successfully");
    }

    public static void selectEngagementDatePickerNext() {
        WebDriverTasks.waitForElement(selectDatePickerNext).click();
    }

    public static void selectEngagementDatePickerNext2() {
        WebDriverTasks.waitForElement(selectDatePickerNext2).click();
    }

    public static void selectEngagementSecondDate() {
        WebDriverTasks.waitForElement(selectSecondDate).click();
        System.out.println("Second Engagement created successfully");
    }

    public static void selectEngagementThirdDate() {
        WebDriverTasks.waitForElement(selectThirdDate).click();
        System.out.println("Third Engagement created successfully");
    }

    public static void selectEngagementContactTime() {
        WebDriverTasks.waitForElement(selectContactTime).click();
    }

    public static void selectEngagementOutcome(String engagementoutcome) {
        WebDriverTasks.waitForElementAndSelect(selectEngagementOutcome).selectByVisibleText(engagementoutcome);
    }

    public static void selectEngagementBorough(String borough) {
        WebDriverTasks.waitForElementAndSelect(selectBorough).selectByVisibleText(borough);
    }

    public static void selectLocationType(String locationtype) {
        WebDriverTasks.waitForElementAndSelect(selectLocationType).selectByVisibleText(locationtype);
    }

    public static void selectParkName(String park) {
        WebDriverTasks.waitForElement(selectParkName).sendKeys(park);
        WebDriverTasks.waitForElement(selectParkName).sendKeys(Keys.DOWN);
        WebDriverTasks.waitForElement(selectParkName).sendKeys(Keys.ENTER);
    }

    public static void inputEngagementNotes(String testnote) {
        WebDriverTasks.waitForElement(addEngagementNotes).sendKeys(testnote);
    }

    public static void selectSaveEngagement() {
        WebDriverTasks.waitForElement(selectSaveEngagement).click();

    }

    public static String verifyEngagementProvider() {
        String engagementProviderText = WebDriverTasks.waitForElement(verifyEngagementProvider).getText();
        System.out.println("Record created with " + engagementProviderText + " as the Engagement Provider ");
        return engagementProviderText;
    }

    public static String returnErrorMessage() {
        String Text = WebDriverTasks.waitForElement(errorMessage).getText();
        System.out.println("The error message " + Text);
        return Text;
    }


    /**
     * This method will collapse Duplicate search pane
     * Created By - Venkat
     */
    public static void collapseDuplicateSearchPane(){
        if(WebDriverTasks.verifyIfElementExists(collapseDuplicateSearchButton)) {
            WebDriverTasks.scrollToElement(collapseDuplicateSearchButton);
            WebDriverTasks.waitForElement(collapseDuplicateSearchButton).click();
            WebDriverTasks.wait(3);
        }
    }

    /**
     * This method will expand Client search pane
     * Created By - Venkat
     */
    public static void expandClientSearchPane(){
        if(WebDriverTasks.verifyIfElementExists(expandClientSearchButton)) {
            WebDriverTasks.waitForElement(expandClientSearchButton).click();
            WebDriverTasks.wait(3);
        }
    }

    /**
     * This method will verify that values in Year dropdown in DOB field starts with expected year and is selectable
     * @param expectedYear This is expected year
     * @return If values in Year dropdown starts with provided values it will return True else False
     */
    public static boolean verifyDOBYearStartsWith(String expectedYear){
        WebDriverTasks.waitForElement(clientDOB).click();
        Select dobDropDown = WebDriverTasks.waitForElementAndSelect(dateOfBirthYearDropdown);
        WebElement firstOption = dobDropDown.getOptions().get(0);
        if (firstOption.getText().trim().equals(expectedYear)) {
            dobDropDown.selectByVisibleText(expectedYear);
            return true;
        } else {
            return false;
        }
    }


}
