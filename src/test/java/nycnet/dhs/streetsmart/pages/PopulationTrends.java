package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;



public class PopulationTrends extends StartWebDriver {

	static By populationTrendsPageHeader = By.xpath("//h1[@class='page-header']");
	
	static By expandActivityStreamButton = By.id("change-log-accordion");
	static By activityStreamSearchInputBox = By.id("activity-stream-search");
	static By dateUpdatedColumnOfActivityStreamGrid = By.xpath("//*[@class='ui-grid-canvas']/div[1]/div/div[1]//span");
	static By updatedByColumnOfActivityStreamGrid = By.xpath("//*[@class='ui-grid-canvas']/div[1]/div/div[2]//span");
	static By providerColumnOfActivityStreamGrid = By.xpath("//*[@class='ui-grid-canvas']/div[1]/div/div[3]//span");
	static By actionTakenColumnOfActivityStreamGrid = By.xpath("//*[@class='ui-grid-canvas']/div[1]/div/div[4]//span");
	static By itemColumnOfActivityStreamGrid = By.xpath("//*[@class='ui-grid-canvas']/div[1]/div/div[5]//span");
	static By actionSummaryColumnOfActivityStreamGrid = By.xpath("//*[@class='ui-grid-canvas']/div[1]/div/div[6]//span");
	
	 public static String returnPopulationTrendsPageHeader() {
		 String populationTrendsPageHeaderText = WebDriverTasks.waitForElement(populationTrendsPageHeader).getText();
		 System.out.println("The page header is " + populationTrendsPageHeaderText);
		 return populationTrendsPageHeaderText;
	  }
	 
	 public static void clickOnExpandActivityStreamButton() {
		WebDriverTasks.waitForElement(expandActivityStreamButton).click();
	  }
	 
	 public static void inputTexInSearchFieldOfActivityStream(String searchQuery) {
			WebDriverTasks.waitForElement(activityStreamSearchInputBox).sendKeys(searchQuery);
		  }
	 
	 public static void submitSearchInActivityStream() {
			WebDriverTasks.waitForElement(activityStreamSearchInputBox).sendKeys(Keys.ENTER);
		  }
	 
	 public static String returnUpdatedByFirstRowOfActivityStreamGrid() {
		 String updatedByText = WebDriverTasks.waitForElement(updatedByColumnOfActivityStreamGrid).getText();
		 System.out.println("This activity was performed by " + updatedByText);
		 return updatedByText;
	  }
	 
	 public static String returnProviderColumnFirstRowOfActivityStreamGrid() {
		 String providerText = WebDriverTasks.waitForElement(providerColumnOfActivityStreamGrid).getText();
		 System.out.println("The provider for this activity is  " + providerText);
		 return providerText;
	  }
	 
	 public static String returnItemColumnFirstRowOfActivityStreamGrid() {
		 String itemText = WebDriverTasks.waitForElement(itemColumnOfActivityStreamGrid).getText();
		 System.out.println("The type of item for this activity is " + itemText);
		 return itemText;
	  }
	 
	 public static String returnActionSummaryFirstRowOfActivityStreamGrid() {
		 String actionSummaryText = WebDriverTasks.waitForElement(actionSummaryColumnOfActivityStreamGrid).getText();
		 System.out.println("The action summary for this activity is " + actionSummaryText);
		 return actionSummaryText;
	  }
}
