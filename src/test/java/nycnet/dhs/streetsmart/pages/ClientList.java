package nycnet.dhs.streetsmart.pages;

import nycnet.dhs.streetsmart.core.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class ClientList {
    //TODO Added By Venkat----
    static String[] monthNames = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    //-----------------

    static By clientListPageHeader = By.xpath("//h1[@class='page-header '][contains(text(), 'Client List')]");
    static By clientListSearchInput = By.id("clntListSearchText");
    static By selectorForFirstRow = By.xpath("(//*[@class='ui-grid-cell-contents'])[2]");
    static By selectorForThirdRow = By.xpath("(//*[@class='ui-grid-cell-contents'])[4]");
    static By exportToExcel = By.linkText("Excel");
    static By exportToExcelSelected = By.linkText("Selected Records");
    static By exportToExcelFilteredRecords = By.linkText("Filtered Records");
    static By exportToExcelAllRecords = By.linkText("All Records");
    static By exportToExcelCurrentView = By.linkText("Current View");
    static By exportToExcelMessage = By.xpath("//*[@class='toast-message']");

    static By clickOnClientC160996 = By.xpath("//a[contains(text(),'C160996')]");
    static By clickOnClientC147736 = By.xpath("//a[contains(text(),'C147736')]");
    static By clickonClientC146864 = By.xpath("//a[contains(text(),'C146864')]");
    static By clickonClientC161003 = By.xpath("//a[contains(text(),'C161003')]");
    static By clickonClientC161014 = By.xpath("//a[contains(text(),'C161014')]");
    static By clickonClientC161015 = By.xpath("//a[contains(text(),'C161015')]");

    static By clickOnClientCategoryC161003 = By.id("1588900315321-0-uiGrid-000F-cell");
    // static By clickOnClientCategoryC161003 = By.id("//*[@id='1593922744436-uiGrid-000F-sortdir-text']");


    static By clickOnJCCClient = By.xpath("/html[1]/body[1]/div[2]/div[3]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[3]"
            + "/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[8]/div[1]/div[1]");

    static By clickOnJCCClientStartDate = By.xpath("/html[1]/body[1]/div[2]/div[3]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]"
            + "/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[9]/div[1]/div[1]/span[1]");

    static By clickOnJCCClientEndDate = By.xpath("/html[1]/body[1]/div[2]/div[3]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/"
            + "div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[10]/div[1]/div[1]/span[1]");

    //	TODO Added by Venkat
    static By jccClient = By.xpath("//div[@role='button']/span[text()='JCC Client']");
    static By jccClientStartDate = By.xpath("//div[@role='button']/span[text()='JCC Client Start Date']");
    static By jccClientEndDate = By.xpath("//div[@role='button']/span[text()='JCC Client End Date']");
    static By profileimage = By.xpath("//div[@role='button']/span[text()='Profile Image']");
    static By userSearchTextBox = By.id("clntListSearchText");
    static By userSearchTextBoxProvider = By.xpath("//div[@title='Provider']");
    static By clickOnJCCClientCheckbox = By.cssSelector("label[title='JCCClient']>input");
    static By clickOnClientCategoryCheckbox = By.cssSelector("label[title='ClientCategory']>input");
    static By clickOnJCCClientStartDateCheckbox = By.cssSelector("label[title='JCC Client Start Date']>input");
    static By clickOnJCCClientEndDateCheckbox = By.cssSelector("label[title='JCC Client End Date']>input");
    static By allOptions = By.xpath("//div[contains(@class,'open')]//li/label[@title]/input");
    static By selectJCCClientDropdown = By.xpath("//div[@id='divFiltes']//button[@id='dropDown_JCC Client']");
    static By selectClientCategoryDropdown = By.xpath("//div[@id='divFiltes']//button[@id='dropDown_Client Category']");
    static By clientCategoryDropdownOptions = By.xpath("//div[@id='divFiltes']//button[@id='dropDown_Client Category']/following-sibling::ul/li[@ng-repeat]/label");
    static By clickOnJCCCLientYes = By.cssSelector("label[title='Yes']>input");
    static By clickOnJCCCLientNo = By.cssSelector("label[title='No']>input");
    static By noRecordFound = By.xpath("//div[text()='No Record Found']");
    static By dataGridRows = By.cssSelector("div[role='grid'].ui-grid-render-container-body>div[role='rowgroup']>div>div.ui-grid-row");
    static By dataGridJCCClientCells = By.cssSelector("div[role='grid'].ui-grid-render-container-body>div[role='rowgroup']>div>div.ui-grid-row>div>div:nth-child(10)>div");
    static By dataGridJCCClientStartDateCells = By.cssSelector("div[role='grid'].ui-grid-render-container-body>div[role='rowgroup']>div>div.ui-grid-row>div>div:nth-child(11)>div");
    static By dataGridJCCClientEndDateCells = By.cssSelector("div[role='grid'].ui-grid-render-container-body>div[role='rowgroup']>div>div.ui-grid-row>div>div:nth-child(12)>div");
    static By inputJCCClientStartDate = By.name("JCCClientStartDate");
    static By inputJCCClientEndDate = By.name("JCCClientEndDate");
    static By exportToExcelConfirmationMsg = By.xpath("//*[@ng-show='!records']");
    static By clickOnOkForExportPopup = By.xpath("//button[@type='submit' and text()='OK']");
    static By clientIdLink = By.cssSelector("div.ui-grid-cell-contents>a[title^='C']");
    static By clientCategoryLabel = By.xpath("//label[text()='Client Category:']/following-sibling::div[text() and contains(@class,'ng-binding')]");
    static By clientCategoryDropdown = By.xpath("//div[@class='col-md-6']/select[@ng-model='Client.ClientType'  and not(contains(@class,'ng-hide'))]");
    static By editClientDetails = By.xpath("//a[contains(@ng-disabled,'disableClientInformation') and text()='Edit']");
    static By saveClientDetails = By.xpath("//button[@id='cint-info-save' and text()='Save']");
    static By doNotSaveClientDetails = By.xpath("//div[not(contains(@class,'ng-hide'))]/div/button[@id='cint-info-cancel' and text()='Cancel']");
    static By selectCaseloadOkButton = By.xpath("//button[text()='OK' or text()='AGREE']");

    static By selectClientCategorySubDropdown = By.xpath("//div[contains(@class,'advance-filter-inputs')]//button[@id='dropDown_Client Category']");
    static By clickEngagedUnshelteredRadio = By.cssSelector("label[title='Engaged Unsheltered Prospect']>input");
    static By clickOtherProspectRadio = By.cssSelector("label[title='Other Prospect']>input");

    //-----------------

    static By clickOnAdvancedFilter = By.linkText("Advanced Filtering");
    static By clickOnChooseFilter = By.xpath("//button[@id='dropDown_Add Filter']//b[contains(@class,'caret')]");
    static By clickOnRadioClientCategory = By.xpath("//input[@id='comboBoxClient Category']");

    static By clickOnClientCategoryFilterButton = By.xpath("//div[@class='ng-scope']//b[@class='caret']");
    static By clickOnRadioEngagedUnsheltered = By.xpath("//input[@id='comboBoxEngaged Unsheltered Prospect']");
    static By clickOnRadioInactive = By.xpath("//input[@id='comboBoxInactive']");
    static By clickOnApplyFilter = By.xpath("//button[@id='nonDHSVeteranDataSaveButton']");
    static By clickOnResetFilter = By.xpath("//button[@id='nonDHSVeteranDataCancelButton']");

    public static void clickOnApplyFilter() {
        WebDriverTasks.waitForElement(clickOnApplyFilter).click();
    }

    public static void clickOnRadioInactive() {
        WebDriverTasks.waitForElement(clickOnRadioInactive).click();
        System.out.println("Inactive option selected");
    }

    public static void clickOnRadioEngagedUnsheltered() {
        WebDriverTasks.waitForElement(clickOnRadioEngagedUnsheltered).click();
        System.out.println("Client Category option selected");
    }

    public static void clickOnClientCategoryFilterButton() {
        WebDriverTasks.waitForElement(clickOnClientCategoryFilterButton).click();
    }

    public static void clickOnRadioClientCategory() {
        WebDriverTasks.waitForElement(clickOnRadioClientCategory).click();
    }

    public static void clickOnChooseFiter() {
        WebDriverTasks.waitForAngular();
        WebDriverTasks.waitForElement(clickOnChooseFilter).click();
    }

    public static void clickOnAdvancedFilter() {
        WebDriverTasks.scrollToElement(clickOnAdvancedFilter);
        WebDriverTasks.waitForElement(clickOnAdvancedFilter).click();
    }

    public static String returnClientListPageHeader() {
        String clientListPageHeaderText = WebDriverTasks.waitForElement(clientListPageHeader).getText();
        System.out.println("The page header is " + clientListPageHeaderText);
        return clientListPageHeaderText;
    }

    public static void inputTextinClientListSearchBox(String searchQuery) {
        WebDriverTasks.waitForElement(clientListSearchInput).sendKeys(searchQuery);
    }

    public static void clearTextinClientListSearchBox() {
        WebDriverTasks.waitForElement(clientListSearchInput).clear();
    }

    public static void submitSearchinGlobalSearchBox() {
        WebDriverTasks.waitForElement(clientListSearchInput).sendKeys(Keys.ENTER);
    }

    public static void selectFirstRowinClientListGrid() {
        WebDriverTasks.waitForElement(selectorForFirstRow).click();
    }

    public static void selectThirdRowinClientListGrid() {
        WebDriverTasks.waitForElement(selectorForThirdRow).click();
    }

    public static void clickOnStreetSmartID(String streetsmartID) {
        By streetSmartClient = By.linkText(streetsmartID);
        WebDriverTasks.waitForElement(streetSmartClient).click();
    }

    public static void clickOnExportToExcel() {
        WebDriverTasks.waitForElement(exportToExcel).click();
    }

    public static void clickOnExportToExcelSelectedRecords() {
        WebDriverTasks.waitForElement(exportToExcelSelected).click();
    }

    public static void clickOnExportToExcelFilteredRecords() {
        WebDriverTasks.waitForElement(exportToExcelFilteredRecords).click();
    }

    public static void clickOnExportToExcelCurrentView() {
        WebDriverTasks.waitForElement(exportToExcelCurrentView).click();
    }

    public static void clickOnExportToExcelAllRecords() {
        WebDriverTasks.waitForElement(exportToExcelAllRecords).click();
    }

    public static String verifyExportToExcelMessage() {
        String exportToExcelText = WebDriverTasks.waitForElementButNotForAngular(exportToExcelMessage).getText();
        System.out.println("The Export To Excel Message is " + exportToExcelText);
        // WebDriverTasks.waitForAngular();
        return exportToExcelText;
    }

    public static void clickOnProspectClientC160996() {
        WebDriverTasks.waitForElement(clickOnClientC160996).click();
    }

    public static void clickOnProspectClientC147736() {
        WebDriverTasks.waitForElement(clickOnClientC147736).click();
    }

    public static void clickOnProspectClientC146864() {
        WebDriverTasks.waitForElement(clickonClientC146864).click();
    }

    public static void clickOnProspectClientC161003() {
        WebDriverTasks.waitForElement(clickonClientC161003).click();
    }

    public static void clickOnProspectClientC161015() {
        WebDriverTasks.waitForElement(clickonClientC161015).click();
    }

    public static String verifyClientCategoryOnC161003() {
        String Text = WebDriverTasks.waitForElement(clickOnClientCategoryC161003).getText();
        System.out.println(Text + " is seen as an Client Category ");
        return Text;

    }

    public static String clickOnJCCClient() {
        String Text = WebDriverTasks.waitForElement(clickOnJCCClient).getText();
        System.out.println(Text + " is seen on default view ");
        return Text;
    }

    public static String clickOnJCCClientStartDate() {
        String Text = WebDriverTasks.waitForElement(clickOnJCCClientStartDate).getText();
        System.out.println(Text + " is seen on default view ");
        return Text;
    }

    public static String clickOnJCCClientEndDate() {
        String Text = WebDriverTasks.waitForElement(clickOnJCCClientEndDate).getText();
        System.out.println(Text + " is seen on default view ");
        return Text;
    }


    /**
     * This method will click on JCC Client check box in Advanced Filter popup
     * Created By : Venkat
     */
    public static void setClickOnJCCClientCheckbox() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.waitForElement(clickOnJCCClientCheckbox).isSelected())
            WebDriverTasks.waitForElement(clickOnJCCClientCheckbox).click();
    }

    /**
     * This method will click on JCC Client Start Date check box in Advanced Filter popup
     * Created By : Venkat
     */
    public static void setClickOnJCCClientStartDateCheckbox() {
        if (!WebDriverTasks.waitForElement(clickOnJCCClientStartDateCheckbox).isSelected())
            WebDriverTasks.waitForElement(clickOnJCCClientStartDateCheckbox).click();
    }

    /**
     * This method will click on JCC Client End Date check box in Advanced Filter popup
     * Created By : Venkat
     */
    public static void setClickOnJCCClientateEndDateCheckbox() {
        if (!WebDriverTasks.waitForElement(clickOnJCCClientEndDateCheckbox).isSelected())
            WebDriverTasks.waitForElement(clickOnJCCClientEndDateCheckbox).click();
    }

    /**
     * This method will uncheck / remove all existing selection on Advanced Filter popup
     * Created By : Venkat
     */
    public static void clearExistingSelection() {
        WebDriverTasks.waitForAngular();
        for (WebElement checkbox : WebDriverTasks.driver.findElements(allOptions)) {
            if (checkbox.isSelected()) {
                checkbox.click();
                WebDriverTasks.waitForAngular();
            }
        }
    }

    /**
     * This method will Select JCC Client value
     * Created By : Venkat
     *
     * @param value This can be either 'Yes' or 'No'
     */
    public static void selectJCCClient(String value) {
        WebDriverTasks.waitForElement(selectJCCClientDropdown).click();
        if (value.equalsIgnoreCase("yes"))
            WebDriverTasks.waitForElement(clickOnJCCCLientYes).click();
        else if (value.equalsIgnoreCase("no"))
            WebDriverTasks.waitForElement(clickOnJCCCLientNo).click();
        else
            System.out.println("PLease provide either Yes or No");
        WebDriverTasks.waitForAngular();
    }

    /**
     * This method will verify that filtered value for target column is displayed as expected.
     * It includes  columns "JCC Client","JCC Client STart Date","JCC Client Edn Date
     * Created By : Venkat
     *
     * @param columnName      This is target column name
     * @param filteredByValue This is expected value that should be displayed in the data grid
     * @return boolean If all the data matched expected value then it will return True if not then false.
     * Also if no matching records foudn then it will return True.
     */
    public static boolean verifyFilteredValuesForJCCClient(String columnName, String filteredByValue) {
        WebDriverTasks.waitForAngular();
        By targetCells;
        if (WebDriverTasks.verifyIfElementIsDisplayed(noRecordFound)) {
            System.out.println("No records found on JCC Client filtered value " + filteredByValue);
            return true;
        }
        switch (columnName) {
            case ("JCC Client"):
                targetCells = dataGridJCCClientCells;
                break;
            case ("JCC Client Start Date"):
                targetCells = dataGridJCCClientStartDateCells;
                break;
            case ("JCC Client End Date"):
                targetCells = dataGridJCCClientEndDateCells;
                break;
            default:
                System.out.println("Please enter valid column name");
                return false;
        }

        List<WebElement> jccClientCells = WebDriverTasks.driver.findElements(targetCells);
        for (WebElement cell : jccClientCells) {
            if (!filteredByValue.equalsIgnoreCase(cell.getText()))
                return false;
        }
        return true;
    }

    /**
     * This method will set provided value for JCC Client Start Date.
     * Created By : Venkat
     *
     * @param startDate This is start date in mm/dd/yyyy format
     */
    public static void setJCCClientStartDate(String startDate) {
        WebDriverTasks.waitForElement(inputJCCClientStartDate).click();
        int day = Integer.parseInt(startDate.split("/")[1]);
        int month = Integer.parseInt(startDate.split("/")[0]);
        int year = Integer.parseInt(startDate.split("/")[2]);
        selectCalenderDate(day, month, year);
    }

    /**
     * This method will set provided value for JCC Client End Date.
     * Created By : Venkat
     *
     * @param endDate This is start date in mm/dd/yyyy format
     */
    public static void setJCCClientEndDate(String endDate) {
        WebDriverTasks.waitForElement(inputJCCClientEndDate).click();
        int day = Integer.parseInt(endDate.split("/")[1]);
        int month = Integer.parseInt(endDate.split("/")[0]);
        int year = Integer.parseInt(endDate.split("/")[2]);
        selectCalenderDate(day, month, year);
    }

    /**
     * This method will select day,month and year on calendar
     * Created By : Venkat
     *
     * @param day   This is day
     * @param month This is month that can range from 1 to 12
     * @param year  This is year
     */
    public static void selectCalenderDate(int day, int month, int year) {
        WebDriverTasks.waitForAngular();
        By selectYear = By.className("ui-datepicker-year");
        By selectMonth = By.className("ui-datepicker-month");
        By selectday = By.xpath("//td[@data-handler='selectDay']/a[text()='" + day + "']");
        String monthName = monthNames[month - 1];

        new Select(WebDriverTasks.waitForElement(selectYear)).selectByVisibleText(String.valueOf(year));
        new Select(WebDriverTasks.waitForElement(selectMonth)).selectByVisibleText(monthName);
        WebDriverTasks.waitForElement(selectday).click();
    }

    /**
     * This method will get the confirmation popup message.
     * Created By : Venkat
     *
     * @return String This is conformation message
     */
    public static String getExportToExcelConfirmationText() {
        String exportToExcelText = WebDriverTasks.waitForElementButNotForAngular(exportToExcelConfirmationMsg).getText();
        System.out.println("The Export To Excel Message is " + exportToExcelText);
        return exportToExcelText;
    }

    /**
     * This method will click on Ok button of confirmation popup
     * Created By : Venkat
     */
    public static void clickOnOkForExportPopup() {
        WebDriverTasks.waitForElement(clickOnOkForExportPopup).click();
    }


    /**
     * This method will check if JCC Client column exists on page
     * Created By : Venkat
     *
     * @return Boolean This will be True if column exists else Flase
     */
    public static boolean verifyJCCClientColumnExists() {
        return WebDriverTasks.verifyIfElementExists(jccClient);
    }

    /**
     * This method will check if JCC Client Start Date column exists on page
     * Created By : Venkat
     *
     * @return Boolean This will be True if column exists else Flase
     */
    public static boolean verifyJCCClientSTartDateColumnExists() {
        return WebDriverTasks.verifyIfElementExists(jccClientStartDate);
    }

    /**
     * This method will check if JCC Client End Date column exists on page
     * Created By : Venkat
     *
     * @return Boolean This will be True if column exists else Flase
     */
    public static boolean verifyJCCClientEndDateColumnExists() {
        return WebDriverTasks.verifyIfElementExists(jccClientEndDate);
    }

    public static boolean verifyProfileImagecolumnExists() {
        return WebDriverTasks.verifyIfElementExists(profileimage);
    }

    /**
     * This method will read all the cell values for JCC Client and make sure that the value is either '','Yes' or 'No'
     *
     * @param reportFileName This is excel name downloaded
     * @param sheetName      This is excel sheet name
     * @param columnNumber   This is target column number for which we want to validate data
     * @return boolean This will be true if all the cells data is matching any of expected data else false
     * @throws IOException
     */
    public static boolean verifyJCCClientColumnDataInExcel(String reportFileName, String sheetName, int columnNumber) throws IOException {
        LinkedList<String> expectedValues = new LinkedList<>(Arrays.asList("", "Yes", "No"));
        LinkedList<String> jccClientColData = Helpers.readColumnData(reportFileName, sheetName, columnNumber);
        for (String val : jccClientColData) {
            if (!expectedValues.contains(val))
                return false;
        }
        return true;
    }

    /**
     * This method will read all the cell values for JCC Client Start and make sure that the value is either '' or a date
     *
     * @param reportFileName This is excel name downloaded
     * @param sheetName      This is excel sheet name
     * @param columnNumber   This is target column number for which we want to validate data
     * @return boolean This will be true if all the cells data is matching any of expected data else false
     * @throws IOException
     */
    public static boolean verifyJCCClientStartDateColumnDataInExcel(String reportFileName, String sheetName, int columnNumber) throws IOException {
        String expectedDateFormat = "MM/dd/yyyy";
        LinkedList<String> jccClientStartDateColData = Helpers.readColumnData(reportFileName, sheetName, columnNumber);
        for (String val : jccClientStartDateColData) {
            if (!val.trim().isEmpty())
                if (!Helpers.isValidDate(val, expectedDateFormat))
                    return false;
        }
        return true;
    }

    /**
     * This method will read all the cell values for JCC Client End and make sure that the value is either '' or a date
     *
     * @param reportFileName This is excel name downloaded
     * @param sheetName      This is excel sheet name
     * @param columnNumber   This is target column number for which we want to validate data
     * @return boolean This will be true if all the cells data is matching any of expected data else false
     * @throws IOException
     */
    public static boolean verifyJCCClientEndDateColumnDataInExcel(String reportFileName, String sheetName, int columnNumber) throws IOException {
        String expectedDateFormat = "MM/dd/yyyy";
        LinkedList<String> jccClientEndDateColData = Helpers.readColumnData(reportFileName, sheetName, columnNumber);
        for (String val : jccClientEndDateColData) {
            if (!val.trim().isEmpty())
                if (!Helpers.isValidDate(val, expectedDateFormat))
                    return false;
        }
        return true;
    }

    /**
     * This method will check if Provider column exists on page
     * Created By : Venkat
     *
     * @return Boolean This will be True if column exists else Flase
     */
    public static boolean verifyProviderColumnExists() {
        return WebDriverTasks.verifyIfElementExists(userSearchTextBoxProvider);
    }

    /**
     * This Method will search for provided value
     * Created By : Venkat
     *
     * @param searchValue This is the value for which we want to search in data grid
     */
    public static void searchFor(String searchValue) {
        WebDriverTasks.waitForElement(userSearchTextBox).click();
        WebDriverTasks.waitForElement(userSearchTextBox).clear();
        WebDriverTasks.waitForElement(userSearchTextBox).sendKeys(searchValue);
        WebDriverTasks.waitForElement(userSearchTextBox).sendKeys(Keys.ENTER);
        WebDriverTasks.waitForAngular();
    }


    /**
     * This method will click on Client category check box in Advanced Filter popup
     * Created By : Venkat - 07/07
     */
    public static void clickOnClientCategoryCheckbox() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.waitForElement(clickOnClientCategoryCheckbox).isSelected())
            WebDriverTasks.waitForElement(clickOnClientCategoryCheckbox).click();
    }

    /**
     * This method will verify that client category only contains 'Caseload' option
     * Created By : Venkat - 07/07
     *
     * @return boolan If 'Caseload' is the only option displayed Then it will return True else False
     */
    public static boolean verifyClientCategoryOptions() {
        WebDriverTasks.waitForElement(selectClientCategoryDropdown).click();
        WebDriverTasks.waitForElement(clientCategoryDropdownOptions);
        List<WebElement> clientCategoryOptions = WebDriverTasks.driver.findElements(clientCategoryDropdownOptions);
        if (clientCategoryOptions.size() != 1)
            return false;
        else
            return ("Caseload").equals(clientCategoryOptions.get(0).getText());
    }

    /**
     * This method will click on the first streetsmart id
     * Created By : Venkat - 07/09
     */
    public static void clickonFirstClientID() {
        WebDriverTasks.waitForElement(clientIdLink).click();
    }


    /**
     * This method will select client category option on Client Details page andl will verify that selected option is displayed as expected.
     * Flow -> Edit Client>Change value in Client category dropdown>Verify selected value is reflected>Click Cancel button(Undo changes)
     * Created By : Venkat - 07/19
     *
     * @return This method will return true if selected value is reflected in Client Category dropdown else False
     * r     *
     */
    public static boolean verifyUserIsAbleToSelectClientCategoryOption(String option) {
        boolean isSelected = false;
        clickOnEditClient();
        Select clientCategory = new Select(WebDriverTasks.waitForElement(clientCategoryDropdown));
        clientCategory.selectByVisibleText(option);
        if (option.equals("Caseload"))
            WebDriverTasks.waitForElement(selectCaseloadOkButton).click();
        WebDriverTasks.waitForAngular();
        clientCategory = new Select(WebDriverTasks.driver.findElement(clientCategoryDropdown));
        isSelected = clientCategory.getFirstSelectedOption().getText().equals(option);
        clickOnDoNotSave();
        return isSelected;
    }

    /**
     * This method will verify client category dropdown options which should be "Inactive","Caseload","Engaged Unsheltered Prospect",
     * Created By : Venkat - 07/19
     *
     * @return boolan If dropdown contains sll thr expected option it will be True else False
     */
    public static boolean verifyClientCategoryOptionsOnClientPage() {
        boolean bFlag = true;
        clickOnEditClient();
        List<String> clientCategoryExpectedOptions = new LinkedList<>(Arrays.asList("Inactive", "Caseload", "Engaged Unsheltered Prospect"));
        Select clientCategory = new Select(WebDriverTasks.waitForElement(clientCategoryDropdown));
        List<WebElement> clientCategoryOptions = clientCategory.getOptions();
        if(clientCategoryExpectedOptions.size()!=clientCategoryOptions.size()){
            clickOnDoNotSave();
            return false;
        }

        for (WebElement option : clientCategoryOptions) {
            if (!clientCategoryExpectedOptions.contains(option.getText())) {
                bFlag = false;
                break;
            }
        }
        clickOnDoNotSave();
        return bFlag;
    }

    /**
     * This method will click on Edit client detail button
     * Created By : Venkat - 07/19
     */
    public static void clickOnEditClient() {
        WebDriverTasks.waitForElement(editClientDetails).click();
    }

    /**
     * This method will click on Cancel button, to whcihc will revert all the unsaved changes on Client details page
     * Created By : Venkat - 07/19
     */
    public static void clickOnDoNotSave() {
        WebDriverTasks.waitForElement(doNotSaveClientDetails).click();
        try {
            WebDriverTasks.driver.switchTo().alert().accept();
        } catch (Exception e) {

        }
    }

    public static void clickOnClientCategorySubDropdown(){
        WebDriverTasks.waitForElement(selectClientCategorySubDropdown).click();
    }

    public static void clickOnEngagedUnshelteredRadio(){
        WebDriverTasks.waitForElement(clickEngagedUnshelteredRadio).click();
    }

    public static void clickOnOtherProspectdRadio(){
        WebDriverTasks.waitForElement(clickOtherProspectRadio).click();
    }

    public static void clickOnResetFilter(){
        WebDriverTasks.waitForElement(clickOnResetFilter).click();
    }
}