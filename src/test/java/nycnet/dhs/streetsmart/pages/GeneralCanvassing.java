package nycnet.dhs.streetsmart.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class GeneralCanvassing {
	
	static By generalcanvassingMenuItem = By.xpath("//a[contains(text(),'General Canvassing')]");
	static By dateFilterYearlyMenu  = By.id("generalCanvassingGrid-Yearly");
	

	public static String returnGeneralCanvassingPageHeader() {
		  String generalcanvassingPageHeaderText =  WebDriverTasks.waitForElement(generalcanvassingMenuItem).getText();
		  System.out.println("The page header is " + generalcanvassingPageHeaderText);
		  return generalcanvassingPageHeaderText;
	  }	
	
	public static void clickOnDateFilterYearlyMenu() {
		 WebDriverTasks.waitForElement(dateFilterYearlyMenu).click();
	 }
	
	
	 
}