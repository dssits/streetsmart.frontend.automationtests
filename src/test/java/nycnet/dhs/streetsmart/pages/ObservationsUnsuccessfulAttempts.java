package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class ObservationsUnsuccessfulAttempts {
	static By observationsUnsuccessfulAttemptsPageHeader = By.xpath("//h1[@class='page-header '][contains(text(), 'Observations & Unsuccessful Attempts')]");
	static By dateFilterYearlyMenu  = By.id("UnsuccessfulEngagementsGrid-Yearly");
	static By searchInputBox  = By.id("observationListSearchText");
	static By addNewButton  = By.xpath("//*[@ng-click='AddObservationAndUnsuccessfulAttempts()']");
	
	//Add new observations locators 
	static By saveButton  = By.xpath("//*[@ng-click='SaveObservationAndUnsuccessfullAttempts()']");
	static By shiftDropDown  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.ShiftId']");
	static By observationDatePicker  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.ObservationDateTime']");
	static By observationTimePicker  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.ObservationTime']");
	static By observationTimePickerLabel  = By.xpath("//*[@class='col-md-5 control-label'][.='*Observation Time:']");
	static By outreachWorkerDropDown  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.OutreachWorker']");
	static By numberOfIndividualsInput  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.NoOfIndividual']");
	static By areaDropDown  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.AreaId']");
	static By boroughDropDown  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.BoroughId']");
	static By subwayLineDropDown  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.SubwayLineSubwayStationMapping.SubwayLineId']");
	static By subwayStationDropDown  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.SubwayLineSubwayStationMapping.SubwayStationId']");
	static By subwayLocationDropDown  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.SubwayLocationId']");
	static By additionalDetailsInput  = By.xpath("//*[@ng-model='ObservationsAndUnccessfulAttempts.ObservationDetails.AdditionalDetails']");
	
	//Add new unsuccessful attempts locators
	static By firstNameInput  = By.xpath("//*[@ng-model='attempts.FirstName']");
	static By lastNameInput  = By.xpath("//*[@ng-model='attempts.LastName']");
	static By aliasInput  = By.xpath("//*[@ng-model='attempts.Alias']");
	
	//column picker locators
    static By columnPicker = By.xpath("//div[@role='button' and @class='ui-grid-icon-container']");
    static By selectedClientCategoryColumn = By.xpath("//button[normalize-space(text())='Client Category']/i[@class='ui-grid-icon-ok']");
    static String selectcolumnOnColumnPicker = "//button[normalize-space(text())='%s']/i[@class='ui-grid-icon-cancel']";
    static String deselectcolumnOnColumnPicker = "//button[normalize-space(text())='%s']/i[@class='ui-grid-icon-ok']";
    static String columnOnGrid = "//div[@role='button']/span[text()='%s']";
    
    //grid locators
 	 static By secondColumnFirstRowOnGrid = By.xpath("//*[@role='grid']/*[@role='rowgroup'][@class='ui-grid-viewport ng-isolate-scope']/*[@class='ui-grid-canvas']/*[@class='ui-grid-row ng-scope']/*[@role='row']/*[@role='gridcell'][2]");

    //Advanced filtering locators
    static By clickOnAdvancedFilter = By.linkText("Advanced Filtering");
    static String selectedFilterOnAdvancedFilters = "//input[@placeholder='%s']";
    
    static By clickOnChooseFilter = By.xpath("//button[@id='dropDown_Add Filter']//b[contains(@class,'caret')]");
    
    static String columnToFilterOnAdvancedFilters = "//*[@ng-repeat='option in (filteredOptions = (multiselectoptions| filter:search))']/label[@title='%s']";
    
    static By clickOnApplyFilter = By.id("nonDHSVeteranDataSaveButton");
    static By clickOnResetFilter = By.xpath("//button[@id='nonDHSVeteranDataCancelButton']");
    static By closeButton = By.xpath("//a[@ng-click='Close()']");
    
    static By enteredValueOnAdvancedFilter = By.xpath("//*[@class='token-input-token']/p");
    
	 public static String  returnObservationsUnsuccessfulAttemptsPageHeader() {
		 String observationsUnsuccessfulAttemptsPageHeaderText = WebDriverTasks.waitForElement(observationsUnsuccessfulAttemptsPageHeader).getText();
		 System.out.println("The page header is " + observationsUnsuccessfulAttemptsPageHeaderText);
		  return observationsUnsuccessfulAttemptsPageHeaderText;
	  }
	 
	 public static void clickOnDateFilterYearlyMenu() {
		 WebDriverTasks.waitForElement(dateFilterYearlyMenu).click();
	 }
	 
	 public static void clickOnAddNewButton() {
		 WebDriverTasks.waitForElement(addNewButton).click();
	 }
	 
	 public static void inputTextInSearchInputBox(String query) {
		 WebDriverTasks.waitForElement(searchInputBox).sendKeys(query);
	 }
	 
	 public static void submitSearchInSearchInputBox() {
		 WebDriverTasks.waitForElement(searchInputBox).sendKeys(Keys.ENTER);
	 }
	 
	 public static void clearSearchInSearchInputBox() {
		 WebDriverTasks.waitForElement(searchInputBox).clear();
	 }
	 
	 public static void clickOnSaveButton() {
		 WebDriverTasks.waitForElement(saveButton).click();
		 WebDriverTasks.waitForAngular();
	 }
	 
	 //add new observations actions
	 public static void selectShift(String shift) {
		 WebDriverTasks.waitForElementAndSelect(shiftDropDown).selectByVisibleText(shift);
	 }
	 
	 public static void selectOutreachWorker(String outreachWorker) {
		 WebDriverTasks.waitForElementAndSelect(outreachWorkerDropDown).selectByVisibleText(outreachWorker);
	 }
	 
	 public static void selectArea(String area) {
		 WebDriverTasks.waitForElementAndSelect(areaDropDown).selectByVisibleText(area);
	 }
	 
	 public static void selectborough(String borough) {
		 WebDriverTasks.waitForElementAndSelect(boroughDropDown).selectByVisibleText(borough);
	 }
	 
	 public static void selectSubWayLine(String subway) {
		 WebDriverTasks.waitForElementAndSelect(subwayLineDropDown).selectByVisibleText(subway);
	 }
	 
	 public static void selectSubwayStation(String subwayStation) {
		 WebDriverTasks.waitForElementAndSelect(subwayStationDropDown).selectByVisibleText(subwayStation);
	 }
	 
	 public static void selectSubwayLocation(String subwayLocation) {
		 WebDriverTasks.waitForElementAndSelect(subwayLocationDropDown).selectByVisibleText(subwayLocation);
	 }
	 
	 public static void inputTextInAdditionalDetailsInput(String query) {
		 WebDriverTasks.waitForElement(additionalDetailsInput).sendKeys(query);
	 }
	 
	 public static void inputTextInNumberOfIndividualsInput(String query) {
		 WebDriverTasks.waitForElement(numberOfIndividualsInput).sendKeys(query);
	 }
	 
	 public static void clickOnObservationDatePicker() {
		 WebDriverTasks.waitForElement(observationDatePicker).click();
	 }
	 
	 public static void clickOnObservationTimePicker() {
		 WebDriverTasks.waitForElement(observationTimePicker).click();
	 }
	 
	 public static void clickOnObservationTimeLabel() {
		 WebDriverTasks.waitForElement(observationTimePickerLabel).click();
	 }
	 
	 //add new unsuccessful attempts actions
	 public static void inputFirstName(String firstName) {
		 WebDriverTasks.waitForElement(firstNameInput).sendKeys(firstName);
	 }
	 
	 public static void inputLastName(String lastName) {
		 WebDriverTasks.waitForElement(lastNameInput).sendKeys(lastName);
	 }
	 
	 public static void inputAlias(String alias) {
		 WebDriverTasks.waitForElement(aliasInput).sendKeys(alias);
	 }
	 
	  //column picker actions
	    public static void clickOnColumnPicker() {
	        WebDriverTasks.waitForElement(columnPicker).click();
	    }
	    
	    public static void selectColumnOnColumnPicker(String columnName) {
	        WebDriverTasks.waitForElement(By.xpath(String.format(selectcolumnOnColumnPicker , columnName))).click();
	    }
	    
	    public static void deselectColumnOnColumnPicker(String columnName) {
	        WebDriverTasks.waitForElement(By.xpath(String.format(deselectcolumnOnColumnPicker, columnName))).click();
	    }
	    
	    public static boolean isColumnPresentOnGrid(String columnName) {
	        boolean isColumnPresent =  WebDriverTasks.verifyIfElementIsDisplayed(By.xpath(String.format(columnOnGrid, columnName)));
	        return isColumnPresent;
	    }
	 
	//advanced filtering actions
	    public static void clickOnAdvancedFilter() {
	        WebDriverTasks.scrollToElement(clickOnAdvancedFilter);
	        WebDriverTasks.waitForElement(clickOnAdvancedFilter).click();
	    }
	    
	    public static void clickOnChooseFiter() {
	        WebDriverTasks.waitForElement(clickOnChooseFilter).click();
	    }
	    
	    public static void chooseColumnFilterOnAdvancedFilter(String columnFilter) {
	        String selectedColumnFilter = String.format(columnToFilterOnAdvancedFilters, columnFilter);
	        WebDriverTasks.waitForElement(By.xpath(selectedColumnFilter)).click();
	    }
	    
	    public static void enterFilterValueOnSelectedFilterOnAdvancedFilter(String selectFilter, String filterValue ) {
	        String selectedFilter = String.format(selectedFilterOnAdvancedFilters, selectFilter);
	        System.out.println("The name of the column filter is " + selectedFilter );
	        System.out.println("The value of the column filter is " + filterValue);
	        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).click();
	        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).clear();
	        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(filterValue);
	        WebDriverTasks.specificWait(2000);
	        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(Keys.DOWN);
	        WebDriverTasks.specificWait(2000);
	        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(Keys.ENTER);
	    }
	    
	    public static String returnEnteredValueOnAdvancedFilter() {
	        String valueOfAdvancedFilter = WebDriverTasks.waitForElement(enteredValueOnAdvancedFilter).getText();
	        System.out.println("The value entered on the selected filter is " + valueOfAdvancedFilter);     
	        return valueOfAdvancedFilter;
	    }
	    
	    public static void clickOnResetFilterButton() {
	        WebDriverTasks.waitForElement(clickOnResetFilter).click();
	    }
	    
	    public static boolean verifyPresenceOfFilterResetButton() {
	        return WebDriverTasks.verifyIfElementExists(clickOnResetFilter);
	    }
	    
	    public static void clickOnApplyFilter() {
	        WebDriverTasks.waitForElement(clickOnApplyFilter).click();
	    }
	    
	    public static void clickOnCloseButton() {
	        WebDriverTasks.waitForElement(closeButton).click();
	    }
	    
	    public static void enterTexToFilterInputJavascriptExecutor(String filterName, String filterText) {
	    	By advancedFilter = By.xpath(String.format(selectedFilterOnAdvancedFilters, filterName));
	        WebDriverTasks.enterTextToInputJavascriptExcecutor(advancedFilter, filterText);	   
	    }
	    
	    public static String returnFirstRecordOnSecondColumn() {
	        String Text = WebDriverTasks.waitForElement(secondColumnFirstRowOnGrid).getText();
	        System.out.println("The first record on second column is " + Text);
	        return Text;
	    }
	 
	 
	 
}
