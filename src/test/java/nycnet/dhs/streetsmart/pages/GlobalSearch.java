package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class GlobalSearch {
	static By globalSearchInputBox = By.xpath("//*[@class='col-md-12 col-sm-12 col-xs-12']/input");
	static By globalSearchSubmitButton = By.xpath("//*[@class='fa fa-2x fa-search open-search cursor-pointer']");
	
	 public static void clickOnGlobalSearchInputBox() {
		 WebDriverTasks.waitForElement(globalSearchInputBox).click();
	  }
	 
	 public static String returnGlobalSearchInputtedText() {
		  String globalSearchInputBoxText = WebDriverTasks.waitForElement(globalSearchInputBox).getAttribute("value");
		  System.out.println("The text inputted in the Global Search box is " + globalSearchInputBoxText);
		  return globalSearchInputBoxText;
	  }
	 
	 public static void inputTextinGlobalSearchBox(String searchQuery) {
		 WebDriverTasks.waitForElement(globalSearchInputBox).sendKeys(searchQuery);
	  }
	 
	 public static void  clickOnGlobalSearchSubmitButtonToSubmit() {
		 WebDriverTasks.waitForElement(globalSearchSubmitButton).click();
	  }

}
