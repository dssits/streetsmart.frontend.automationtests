package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class OutreachRequests extends StartWebDriver {
	static By outreachrequestsMenuItem = By.xpath("//span[contains(text(),'Outreach Requests')]");
	
	public static String returnoutreachrequestsPageHeader() {
		String outreachrequestsPageHeaderText = WebDriverTasks.waitForElement(outreachrequestsMenuItem).getText();
		System.out.println("The page header is:" +outreachrequestsPageHeaderText);
		return outreachrequestsPageHeaderText;
	}
}
