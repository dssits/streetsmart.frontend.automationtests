package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class MonthlyDashboard {
	
	static By monthlydashboardMenuItem = By.xpath("//a[contains(text(),'Monthly Dashboard')]");

	public static String returnmonthlydashboardPageHeader() {
		  String monthlydashboardPageHeaderText =  WebDriverTasks.waitForElement(monthlydashboardMenuItem).getText();
		  System.out.println("The page header is " + monthlydashboardPageHeaderText);
		  return monthlydashboardPageHeaderText;
	  }

}
