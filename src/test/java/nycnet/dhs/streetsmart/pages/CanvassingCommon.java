package nycnet.dhs.streetsmart.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class CanvassingCommon {
	
	static By canvassingMenuItem = By.xpath("//span[contains(text(),'Canvassing')]");

	static By generalcanvassingExpandCanvassingItem = By.xpath("//*[@ng-click='grid.api.expandable.toggleRowExpansion(row.entity)']");
	static By fieldsOnExpandGeneralCanvassingItem = By.xpath("//*[@class='col-md-5 col-sm-5 col-xs-5 control-label p-t-0']");

	 static By recordsPagerDetails = By.xpath("//div[@class='ui-grid-pager-count']/span");
  	 
	public static String returncanvassingPageHeader() {
		String canvassingPageHeaderText = WebDriverTasks.waitForElement(canvassingMenuItem).getText();
		System.out.println("The page header is " + canvassingPageHeaderText);
		return canvassingPageHeaderText;
	}
	
	 public static void expandFirstCanvassingRecord() {
		 WebDriverTasks.waitForElement(generalcanvassingExpandCanvassingItem).click();
	 }
	 
	 public static void verifyFieldsOnExpandedCanvassingItems(String expectedRecord) {
			List<WebElement> itemsOnExpandedRecord = WebDriverTasks.waitForElements(fieldsOnExpandGeneralCanvassingItem);
	    	 for (WebElement record : itemsOnExpandedRecord) {
	           String itemsOnExpandedRecordText =  record.getText();
	           System.out.println("The name of the field is " + itemsOnExpandedRecordText);
	           if (itemsOnExpandedRecordText.equalsIgnoreCase(expectedRecord)) {
	        	   Assert.assertEquals(itemsOnExpandedRecordText, expectedRecord);
	        	   break;
	           }
	         }
	 }
	 
	  
	
	

}
