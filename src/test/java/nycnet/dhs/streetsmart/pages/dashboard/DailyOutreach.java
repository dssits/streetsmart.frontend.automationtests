/**
 *
 */
package nycnet.dhs.streetsmart.pages.dashboard;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.ClientList;
import nycnet.dhs.streetsmart.pages.reports.ViewReports;

public class DailyOutreach extends StartWebDriver {

    static String detailsButtonOnTile = "//span[text()='%s']/ancestor::div[contains(@class,'panel')]//*[normalize-space()='Details']/i";
    static String detailsPaneFor = "//h4[contains(@class,'panel-title')]//div[normalize-space()='%s – Details']";
    static String tileBreakDownValues = "//li[text()=' %s ']/span";
    static String allInfoButtonForTile = "//span[text()='%s']/ancestor::div[contains(@class,'panel')]//i[@class='fa fa-info-circle pointer']";
    
    static By toggleExpandOfMiddlePanel = By.xpath("//*[@ng-click='togglewigdets()']");
    
    //Tile expanded middle panel locators
    static String expandedMiddlePanelTotalCount = "//span[text()=' %s ']/label";
    static String expandedMiddlePanelSubCount = "//label[text()='%s']/preceding-sibling::label";

    //Added By Venkat
    static By engagedUnshelteredTile = By.xpath("//h4/a/span[text()='Engaged Unsheltered Prospects & Other Prospects']");
    static By engagedUnshelteredBreakdown = By.xpath("//li[normalize-space(text())='Engaged Unsheltered Prospects']");
    static By otherProspectBreakdown = By.xpath("//li[normalize-space(text())='Other Prospects']");
    static By engagedUnshelteredRecordCount = By.xpath("//li[normalize-space(text())='Engaged Unsheltered Prospects']/span");
    static By otherPorspectRecordCount = By.xpath("//li[normalize-space(text())='Other Prospects']/span");
    static By engagedUnshelteredInfoButton = By.xpath("//li[normalize-space(text())='Engaged Unsheltered Prospects']//i");
    static By engagedUnshelteredInfoModal = By.xpath("//div[@class='modal-header']/h4[text()='Engaged Unsheltered Prospects']/parent::div/following-sibling::div/p[text()]");
    static By otherProspectInfoButton = By.xpath("//li[normalize-space(text())='Other Prospects']//i");
    static By otherProspectsInfoModal = By.xpath("//div[@class='modal-header']/h4[text()='Other Prospects']/parent::div/following-sibling::div/p[text()]");
    static By closeInformationModal = By.cssSelector("div.modal[style^='display: block;']>div>div>div>button");
    static By totalEngagedUnshelteredRecordCount = By.xpath("//h4/a/span[text()='Engaged Unsheltered Prospects & Other Prospects']/ancestor::div[contains(@class,'panel') and @data-sortable-id]//div[contains(@class,'stats-number')]");
    static By recordsPagerDetails = By.xpath("//div[@class='ui-grid-pager-count']/span");
    static By engagedUnshelteredTileDetails = By.xpath("//span[text()='Engaged Unsheltered Prospects & Other Prospects']/ancestor::div[contains(@class,'panel')]//a[normalize-space()='Details']");
    static By engagedUnshelteredTileSetting = By.xpath("//span[text()='Engaged Unsheltered Prospects & Other Prospects']/ancestor::div[contains(@class,'panel')]//a[@title='Settings']");
    static By engagedUnshelteredTileDailyOption = By.xpath("//span[text()='Engaged Unsheltered Prospects & Other Prospects']/ancestor::div[contains(@class,'panel')]//button[normalize-space(text())='Daily']");
    static By engagedUnshelteredTileWeeklyOption = By.xpath("//span[text()='Engaged Unsheltered Prospects & Other Prospects']/ancestor::div[contains(@class,'panel')]//button[normalize-space(text())='Weekly']");
    static By engagedUnshelteredTileMonthlyOption = By.xpath("//span[text()='Engaged Unsheltered Prospects & Other Prospects']/ancestor::div[contains(@class,'panel')]//button[normalize-space(text())='Monthly']");
    static By exportToExcel = By.xpath("//a[@ng-show='UserHasAllExportPermission' and normalize-space()='Excel']");
    static By exportAll = By.xpath("//ul[not(@style='display: none;') and @style]//a[normalize-space()='All']");
    static By exportFilteredRecords = By.xpath("//ul[not(@style='display: none;') and @style]//a[normalize-space()='Filtered Records']");
    static By exportCurrentView = By.xpath("//ul[not(@style='display: none;') and @style]//a[normalize-space()='Current View']");
    static By enagedUnshelteredDataSection = By.xpath("//h4[contains(@class,'panel-title ')]//div[normalize-space()='Engaged Unsheltered Prospects & Other Prospects – Details']");
    static By nextDatGridPage = By.cssSelector("button.ui-grid-pager-next");
    static By firstDatGridPage = By.cssSelector("button.ui-grid-pager-first");
    static By columnPicker = By.xpath("//div[@role='button' and @class='ui-grid-icon-container']");
    static By selectedClientCategoryColumn = By.xpath("//button[normalize-space(text())='Client Category']/i[@class='ui-grid-icon-ok']");
    static String selectcolumnOnColumnPicker = "//button[normalize-space(text())='%s']/i[@class='ui-grid-icon-cancel']";
    static String deselectcolumnOnColumnPicker = "//button[normalize-space(text())='%s']/i[@class='ui-grid-icon-ok']";
    static By deselectedClientCategoryColumn = By.xpath("//button[normalize-space(text())='Client Category']/i[@class='ui-grid-icon-cancel']");
    static By clientCategoryColumn = By.xpath("//div[@role='button']/span[text()='Client Category']");
    static String columnOnGrid = "//div[@role='button']/span[text()='%s']";
    static By currentPlacementColumn = By.xpath("//div[@role='button']/span[text()='Current Placement']");
    static By allOptions = By.xpath("//div[contains(@class,'open')]//li/label[@title]");
    static By searchForInputBox = By.xpath("//input[@aria-controls='data-table' and @type='text']");
    static By currentLocationTileSetting = By.xpath("//span[text()='Current Location of Clients & Prospects']/ancestor::div[contains(@class,'panel')]//a[@title='Settings']");
    static By currentLocationTileDailyOption = By.xpath("//span[text()='Current Location of Clients & Prospects']/ancestor::div[contains(@class,'panel')]//button[normalize-space(text())='Daily']");
    static By currentLocationTileWeeklyOption = By.xpath("//span[text()='Current Location of Clients & Prospects']/ancestor::div[contains(@class,'panel')]//button[normalize-space(text())='Weekly']");
    static By currentLocationTileMonthlyOption = By.xpath("//span[text()='Current Location of Clients & Prospects']/ancestor::div[contains(@class,'panel')]//button[normalize-space(text())='Monthly']");
    static By generalDailyDetails = By.xpath("//div[contains(@class,'outer-calendar-div') and contains(@style,'display: block;')]//button[normalize-space(text())='Daily']");
    static By generalWeeklyDetails = By.xpath("//div[contains(@class,'outer-calendar-div') and contains(@style,'display: block;')]//button[normalize-space(text())='Weekly']");
    static By generalMonthlyDetails = By.xpath("//div[contains(@class,'outer-calendar-div') and contains(@style,'display: block;')]//button[normalize-space(text())='Monthly']");

    static By currentLocationTileBreakups = By.xpath("//h4/a/span[text()='Current Location of Clients & Prospects']/ancestor::div[contains(@class,'panel')]//div[contains(@class,'widget-stats')]//ul/li/span/..");
    static By currentLocationTileBreakupsInfo = By.xpath("//h4/a/span[text()='Current Location of Clients & Prospects']/ancestor::div[contains(@class,'panel')]//div[contains(@class,'widget-stats')]//ul/li/i[@class='fa fa-info-circle pointer']");

    static By currentLocationTileInfoButton = By.xpath("//h4/a/span[text()='Current Location of Clients & Prospects']/ancestor::div[contains(@class,'panel')]//div[contains(@class,'stats-number')]/span/i[@class='fa fa-info-circle pointer']");
    static By currentLocationTileInfoPopup = By.xpath("//div[@class='modal fade modal-dashboard in' and contains(@style,'display: block;')]/div[@class='modal-dialog']");

    static By latestContactDate = By.xpath("//div[@role='button']/span[text()='Latest Contact Date']");
    static By lastDateFieldOnAdvancedFilterPopup = By.cssSelector("div.scroll-filter-option>div:last-child>*>*>*>*>input");
    static By selectedLatestContactDateColumn = By.xpath("//button[normalize-space(text())='Latest Contact Date']/i[@class='ui-grid-icon-ok']");
    static By deselectedLatestContactDateColumn = By.xpath("//button[normalize-space(text())='Latest Contact Date']/i[@class='ui-grid-icon-cancel']");

    static By lastComboBoxOnAdvancedFilterPopup = By.cssSelector("div.scroll-filter-option>div:last-child>*>*>*>*>ul>li>input");

    //Current Location of Clients & Prospects Tile
    static By currentLocationTile = By.xpath("//h4/a/span[text()='Current Location of Clients & Prospects']");
    static By currentLocationTileDetails = By.xpath("//span[text()='Current Location of Clients & Prospects']/ancestor::div[contains(@class,'panel')]//a[normalize-space()='Details']");
    static By totalCurrentLocationTileCount = By.xpath("//h4/a/span[text()='Current Location of Clients & Prospects']/ancestor::div[contains(@class,'panel') and @data-sortable-id]//div[contains(@class,'stats-number')]");
    static By currentLocationDataSection = By.xpath("//h4[contains(@class,'panel-title ')]//div[normalize-space()='Current Location of Clients & Prospects – Details']");

    static By columnPickerOptions = By.xpath("//li[@name='item.title']/button[not(contains(@class,'ng-hide'))]/i[@class]/..");
    static By checkedColumnPickerOptions = By.xpath("//li[@name='item.title']/button[not(contains(@class,'ng-hide'))]/i[@class='ui-grid-icon-ok']/..");

    static By selectRadioDateOfBirth2 = By.xpath("//label[@title='Date Of Birth']/input[@type='checkbox']");
    static By selectRadioDateOfBirth = By.id("comboBoxDate of Birth");
    static By dateOfBirthInputBox = By.name("DateOfBirth");
    static By dateOfBirthYearDropdown = By.xpath("//div[@id='ui-datepicker-div' and contains(@style,'display: block;')]//select[@data-handler='selectYear']");

    //Month Picker
    static By showYears = By.xpath("//div[@date-picker and @style='display: block;']//td[@class='month-picker-title']");
    static By monthPicker = By.xpath("//div[@date-picker and @style='display: block;']//div[contains(@id,'MonthPicker_')]");

    static By dailyoutreachMenuItem = By.xpath("//a[contains(text(),'Daily Outreach')]");

    //Engaged Unsheltered Prospect and other prospect Tile locators
    static By totalInfoIconEngagedUnshelteredProspectAndOtherProspect = By.xpath("//*[@class='widget widget-stats bg-green']//*[@class='stats-number  ng-binding']//*[@class='fa fa-info-circle pointer']");

    static By EngagedUnshelteredProspectAndOtherProspectPopupTitle = By.xpath("//*[@class='modal-title'][contains(text(),'Engaged Unsheltered Prospects and Other Prospects')]");

    static By EngagedUnshelteredProspectAndOtherProspectPopupText = By.xpath("//p[contains(text(),'The total number of Engaged Unsheltered Prospects and Other Prospects as of today. Counts will vary depending on time frame selected: daily, weekly, monthly.')]");

    static By EngagedUnshelteredProspectPopupTitle = By.xpath("(//*[@class='modal-title'][contains(text(),'Engaged Unsheltered Prospects')])[2]");

    static By infoiconEngagedUnshelteredProspect = By.xpath("//*[@class='col-xs-12 col-sm-12 col-md-12 col-lg-6 p-r-0 p-l-0']//*[contains(text(),'Engaged Unsheltered Prospects')]/span/following-sibling::*");

    static By EngagedUnshelteredProspectPopupText = By.xpath("//*[@class='modal-body']/p[contains(text(),'The total number of Engaged Unsheltered Prospects with any of the following as of today: first and last name as well as alias, first and last name, first name and alias, last name and alias. Counts will vary depending on timeframe selected: daily, weekly, monthly.')]");

    static By closeEngagedUnshelteredProspectPopupOnEngagedUnshelteredProspectAndOtherProspectTile = By.xpath("//*[@class='modal fade modal-dashboard in']//*[@class='modal-title'][contains(text(),'Engaged Unsheltered Prospects')]/preceding-sibling::*");

    static By infoiconOtherProspect = By.xpath("//*[@class='col-xs-12 col-sm-12 col-md-12 col-lg-6 p-r-0 p-l-0']//*[contains(text(),'Other Prospects')]/span/following-sibling::*");

    static By OtherProspectPopupTitle = By.xpath("//*[@class='modal-title'][.='Other Prospects']");

    static By OtherProspectPopupText = By.xpath("//*[@class='modal-body']/p[contains(text(),'The total number of Other Prospects with any of the following as of today: first and last name as well as alias, first and last name, first name and alias, last name and alias. Counts will vary depending on timeframe selected: daily, weekly, monthly.')]");

    static By clientCategoryColumnByCss = By.cssSelector("div[title='Client Category']");

    static By engagedUnshelteredOtherProspectDetails = By.xpath("//*[@ng-click='getProspectiveClientDetailsDetails()']");

    //Current location of clients & prospects Tile locators
    static By currentLocationProspectsDetails = By.xpath("//*[@ng-click='getCurrentPlacementDetailsDetails()']");

    static By engagedUnshelteredOnGrid = By.xpath("//*[@title='Engaged Unsheltered Prospect']");


    static By infoIconUnshelteredCurrentLocationOfProspects = By.xpath("(//*[@class='widget widget-stats bg-blue-darker']//*[@class='fa fa-info-circle pointer'])[2]");

    static By UnshelteredCurrentLocationOfProspectsPopupTitle = By.xpath("//*[@class='modal-title'][.='Unsheltered']");

    static By infoIconOnStreetAndOtherSettingsCurrentLocationOfProspects = By.xpath("(//*[@class='widget widget-stats bg-blue-darker']//*[@class='fa fa-info-circle pointer'])[3]");
    static By infoOnStreetAndOtherSettingsCurrentLocationOfProspectsPopupTitle = By.xpath("//*[@class='modal-title'][.='On Street & Other Settings']");
    static By infoIconUndeterminedHousingStatusCurrentLocationOfProspects = By.xpath("(//*[@class='widget widget-stats bg-blue-darker']//*[@class='fa fa-info-circle pointer'])[6]");
    static By closeUndeterminedHousingStatusPopupOnCurrentLocationOfProspectsTile = By.xpath("//*[@class='modal fade modal-dashboard in']//*[@class='modal-title'][.='Undetermined Housing Status']/preceding-sibling::*");
    static By infoUndeterminedHousingStatusOnCurrentLocationOfProspectsPopupTitle = By.xpath("//*[@class='modal-title'][.='Undetermined Housing Status']");
    static By closeUnshelteredInfoPopupOnCurrentLocationOfProspectsTile = By.xpath("//*[@class='modal fade modal-dashboard in']//*[@class='modal-title'][.='Unsheltered']/preceding-sibling::*");
    static By closeOnStreetOtherSettingsInfoPopupOnCurrentLocationOfProspectsTile = By.xpath("//*[@class='modal fade modal-dashboard in']//*[@class='modal-title'][.='On Street & Other Settings']/preceding-sibling::*");

    static By clientCategoryFirstRecord = By.xpath("(//*[@role='row']/*[@role='gridcell'])[11]");
    
    //General Daily Dashboard tile and grid locators
    static String totalRecordOnTile = "//h4/a/span[text()='%s']/ancestor::div[contains(@class,'panel') and @data-sortable-id]//div[contains(@class,'stats-number')]";
    static By streetSmartIDOnGrid = By.xpath("//*[@role='grid']/*[@role='rowgroup'][@class='ui-grid-viewport ng-isolate-scope']/*[@class='ui-grid-canvas']/*[@class='ui-grid-row ng-scope']/*[@role='row']/*[@role='gridcell'][1]");
    
    //Advanced Filters locators
    static By closeAdvancedFilterPopup = By.xpath("//div[@class='modal-footer']/a[text()='Close']");
    static By clickOnAdvancedFilter = By.xpath("//a[@class='btn btn-sm btn-success pull-right m-r-5']");
    static String selectedFilterOnAdvancedFilters = "//input[@placeholder='%s']";
    static String columnToFilterOnAdvancedFilters = "//*[@ng-repeat='option in (filteredOptions = (multiselectoptions| filter:search))']/label[@title='%s']";
    
    
    //General Daily Dashboard tile and grid actions
    public static String returnFirstRecordOnStreetSmartColumn() {
        String Text = WebDriverTasks.waitForElement(streetSmartIDOnGrid).getText();
        System.out.println("The first record on the Streetsmart column is " + Text);
        return Text;
    }
    
    public static void scrollToIconToToggleExpandMiddlePanel() {
    	WebDriverTasks.scrollToElement(toggleExpandOfMiddlePanel);
    }
    
    public static void clickOnIconToToggleExpandMiddlePanel() {
        WebDriverTasks.waitForElement(toggleExpandOfMiddlePanel).click();
    }
    
    public static int getTotalRecordCountOnTile(String sectionName) {
        By value = By.xpath(String.format(totalRecordOnTile, sectionName));
        String strValue = WebDriverTasks.waitForElement(value).getText();
        System.out.println("The total count of " + sectionName + " is " + strValue);
        return Integer.parseInt(strValue);
    }
    
    public static int getTotalRecordCountExpandedMiddlePanel(String sectionName) {
        By value = By.xpath(String.format(expandedMiddlePanelTotalCount, sectionName));
        String strValue = WebDriverTasks.waitForElement(value).getText();
        System.out.println("The total count of " + sectionName + " is " + strValue);
        return Integer.parseInt(strValue);
    }
    
    public static int getSubRecordCountExpandedMiddlePanel(String sectionName) {
        By value = By.xpath(String.format(expandedMiddlePanelSubCount, sectionName));
        String strValue = WebDriverTasks.waitForElement(value).getText();
        System.out.println("The count of " + sectionName + " is " + strValue);
        return Integer.parseInt(strValue);
    }

    public static String returnFirstRecordOnClientCategoryColumn() {
        String Text = WebDriverTasks.waitForElement(clientCategoryFirstRecord).getText();
        System.out.println("The first record on the client category column is " + Text);
        return Text;
    }

    public static String returndailyoutreachPageHeader() {
        String dailyoutreachPageHeaderText = WebDriverTasks.waitForElement(dailyoutreachMenuItem).getText();
        System.out.println("The page header is " + dailyoutreachPageHeaderText);
        return dailyoutreachPageHeaderText;
    }

    public static String verifyPresenceOfEngagedUnshelteredOnGrid() {
        String text = WebDriverTasks.waitForElement(engagedUnshelteredOnGrid).getText();
        System.out.println("The value on the grid is " + text);
        return text;
    }

    //Actions for Engaged Unsheltered prospect and Other Prospects Tile
  

    public static void openEngagedUnshelteredProspectAndOtherProspectGrid() {
        WebDriverTasks.waitForElement(engagedUnshelteredOtherProspectDetails).click();
    }

    public static void clickOntotalInfoIconEngagedUnshelteredProspectAndOtherProspect() {
        WebDriverTasks.waitForElement(totalInfoIconEngagedUnshelteredProspectAndOtherProspect).click();
    }

    public static void clickOnInfoIconEngagedUnshelteredProspect() {
        WebDriverTasks.waitForElement(infoiconEngagedUnshelteredProspect).click();
    }

    public static String returnEngagedUnshelteredProspectPopupTitle() {
        String Text = WebDriverTasks.waitForElement(EngagedUnshelteredProspectPopupTitle).getText();
        System.out.println("The Title is " + Text);
        return Text;
    }

    public static String returnEngagedUnshelteredProspectPopupText() {
        String Text = WebDriverTasks.waitForElement(EngagedUnshelteredProspectPopupText).getText();
        System.out.println("The text is " + Text);
        return Text;
    }


    public static String EngagedUnshelteredProspectAndOtherProspectPopupTile() {
        String Text = WebDriverTasks.waitForElement(EngagedUnshelteredProspectAndOtherProspectPopupTitle).getText();
        System.out.println("The title is " + Text);
        return Text;
    }

    public static String returnEngagedUnshelteredProspectAndOtherProspectPopupText() {
        String Text = WebDriverTasks.waitForElement(EngagedUnshelteredProspectAndOtherProspectPopupText).getText();
        System.out.println("The text is " + Text);
        return Text;
    }

    public static void closeEngagedUnshelteredProspectPopupOnEngagedUnshelteredProspectAndOtherProspectTile() {
        WebDriverTasks.waitForElement(closeEngagedUnshelteredProspectPopupOnEngagedUnshelteredProspectAndOtherProspectTile).click();
    }

    public static void clickinfoiconOtherProspect() {
        WebDriverTasks.waitForElement(infoiconOtherProspect).click();
    }

    public static String returnOtherProspectPopupTitle() {
        String Text = WebDriverTasks.waitForElement(OtherProspectPopupTitle).getText();
        System.out.println("The Title is " + Text);
        return Text;
    }

    public static String returnOtherProspectPopupText() {
        String Text = WebDriverTasks.waitForElement(OtherProspectPopupText).getText();
        System.out.println("The text is " + Text);
        return Text;
    }


    //actions for current location of clients and prospects Tiles
    public static void openCurrentLocationProspectsAndClientsGrid() {
        WebDriverTasks.waitForElement(currentLocationProspectsDetails).click();
    }

    public static void clickOninfoIconUnshelteredCurrentLocationOfProspectsTileToOpenPopup() {
        WebDriverTasks.waitForElement(infoIconUnshelteredCurrentLocationOfProspects).click();
    }

    public static String returnUnshelteredCurrentLocationOfProspectsPopupTitle() {
        String Text = WebDriverTasks.waitForElement(UnshelteredCurrentLocationOfProspectsPopupTitle).getText();
        System.out.println("The text is " + Text);
        return Text;
    }


    public static void clickOninfoIconOnStreetAndOtherSettingsCurrentLocationOfProspectsTileToOpenPopup() {
        WebDriverTasks.waitForElement(infoIconOnStreetAndOtherSettingsCurrentLocationOfProspects).click();
    }

    public static String returninfoOnStreetAndOtherSettingsCurrentLocationOfProspectsPopupTitle() {
        String Text = WebDriverTasks.waitForElement(infoOnStreetAndOtherSettingsCurrentLocationOfProspectsPopupTitle).getText();
        System.out.println("The text is " + Text);
        return Text;
    }

    public static void clickOnXOnStreetOtherSettingsInfoPopupOnCurrentLocationOfProspectsTileToClosePopup() {
        WebDriverTasks.waitForElement(closeOnStreetOtherSettingsInfoPopupOnCurrentLocationOfProspectsTile).click();
    }

    public static void clickOninfoIconUndeterminedHousingStatusCurrentLocationOfProspectsTileToOpenPopup() {
        WebDriverTasks.waitForElement(infoIconUndeterminedHousingStatusCurrentLocationOfProspects).click();
    }

    public static void clickOnXUndeterminedHousingStatusCurrentLocationOfProspectsTileToClosePopup() {
        WebDriverTasks.waitForElement(closeUndeterminedHousingStatusPopupOnCurrentLocationOfProspectsTile).click();
    }

    public static String returnInfoUndeterminedHousingStatusCurrentLocationOfProspectsTilePopupTitle() {
        String Text = WebDriverTasks.waitForElement(infoUndeterminedHousingStatusOnCurrentLocationOfProspectsPopupTitle).getText();
        System.out.println("The text is " + Text);
        return Text;
    }


    public static void clickOnXUnshelteredInfoPopupOnCurrentLocationOfProspectsTileToClosePopup() {
        WebDriverTasks.waitForElement(closeUnshelteredInfoPopupOnCurrentLocationOfProspectsTile).click();
    }

    //actions for current location of clients and prospects grid

    public static boolean verifyIfclientCategoryColumnExists() {
        boolean clientCategoryIsPresent = WebDriverTasks.verifyIfElementExists(clientCategoryColumnByCss);
        System.out.println("The presence of the column is  " + clientCategoryIsPresent);
        return clientCategoryIsPresent;
    }


    /**
     * This method will verify if Engaged Unsheltered Prospect and Other Prospect tile is displayed or not
     * Created By : Venkat
     * @return boolean If tile is displayed then it will be True else False
     */
    public static boolean isEngagedUnsheltedProspectTileDisplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(engagedUnshelteredTile, 60);
    }

    /**
     * This Method will verify that sum of Engaged unsheltered prospect count and Other prospect count is equal to total count
     * Created By : Venkat
     * @return boolean If the count matches then it will be True else False
     */
    public static boolean verifyRecordCountForEngagedUnshelteredTile() {
        String engagedUnshelteredRecords = WebDriverTasks.waitForElement(engagedUnshelteredRecordCount).getText().trim();
        String otherProspectRecords = WebDriverTasks.waitForElement(otherPorspectRecordCount).getText().trim();
        String recordCountInTile = WebDriverTasks.waitForElement(totalEngagedUnshelteredRecordCount).getText().trim();
        return (Integer.parseInt(engagedUnshelteredRecords) + Integer.parseInt(otherProspectRecords)) == Integer.parseInt(recordCountInTile);
    }

    /**
     * This method will get number of records available for the report
     * Created By : Venkat
     *
     * @return int This is total records count
     */
    public static int getTotalRecordsCount() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(recordsPagerDetails, 15))
            return 0;
        String pagerString = WebDriverTasks.waitForElement(recordsPagerDetails).getText().trim();
        String[] arr = pagerString.split("of");
        String totalRecords = arr[1].trim().replace(" items", "");
        System.out.println("The total number of records on the grid is " + totalRecords);
        return Integer.parseInt(totalRecords);
    }

    /**
     * This method will click on Detail button for engaged unsheltered tile
     * Created By : Venkat
     */
    public static void clickOnDetailsButtonForEngagedUnshelteredTile() {
        WebDriverTasks.scrollToElement(engagedUnshelteredTileDetails);
        WebDriverTasks.waitForElement(engagedUnshelteredTileDetails).click();
    }

    /**
     * This method will click on Settings button for engaged unsheltered tile
     * Created By : Venkat
     */
    public static void clickOnSettingsButtonForEngagedUnshelteredTile() {
        WebDriverTasks.scrollToElement(engagedUnshelteredTileSetting);
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(engagedUnshelteredTileSetting).click();
    }

    /**
     * This method will verify that total records count displayed in tile matches with records count displayed in details data grid
     * @return boolean If count matches then True else False
     */
    public static boolean verifyTotalRecordsCountsMatchesWithGridRecordCount() {
        String recordCountInTile = WebDriverTasks.waitForElement(totalEngagedUnshelteredRecordCount).getText().trim();
        return Integer.parseInt(recordCountInTile) == getTotalRecordsCount();
    }

    /**
     * This method will verify that in Daily,Weekly or Monthly option are displayed in Settings
     * @param option This can be Daily,Monthly or Weekly
     * @return If the provided option is displayed then True else False
     */
    public static boolean verifySettingOptionOnEngagedUnshelteredTile(String option) {
        By targetOption = null;
        switch (option.toLowerCase()) {
            case "daily":
                targetOption = engagedUnshelteredTileDailyOption;
                break;
            case "weekly":
                targetOption = engagedUnshelteredTileWeeklyOption;
                break;
            case "monthly":
                targetOption = engagedUnshelteredTileMonthlyOption;
                break;
            default:
                Assert.fail("Please provide any from Daily,Weekly or Monthly");
        }
        return WebDriverTasks.verifyIfElementIsDisplayed(targetOption, 60);
    }

    /**
     * This method will verify if Export to excel link is displayed or not
     * Created By : Venkat
     * @return If displayed then it will return True else False
     */
    public static boolean verifyExportToExcelIsDisplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(exportToExcel, 60);
    }

    /**
     * This method will click on Export button
     */
    public static void clickOnExportToExcel() {
        WebDriverTasks.waitForElement(exportToExcel).click();
    }

    /**
     * This method will verify if Export All records to excel link is displayed or not
     * Created By : Venkat
     * @return If displayed then it will return True else False
     */
    public static boolean verifyExportToExcelOption_All() {
        return WebDriverTasks.verifyIfElementIsDisplayed(exportAll, 60);
    }

    /**
     * This method will verify if Export Current view to excel link is displayed or not
     * Created By : Venkat
     * @return If displayed then it will return True else False
     */
    public static boolean verifyExportToExcelOption_CurrentView() {
        return WebDriverTasks.verifyIfElementIsDisplayed(exportCurrentView, 60);
    }

    /**
     * This method will verify if data pane is displayed for engaged unsheltered
     * Created By : Venkat
     * @return If displayed then it will return True else False
     */
    public static boolean verifyDataPaneIsDisplayedForEngagedUnsheltered() {
        return WebDriverTasks.verifyIfElementIsDisplayed(enagedUnshelteredDataSection, 120);
    }

    /**
     * This method will verify if Engaged Unsheltered Prospects breakdown is displayed or not
     * @return boolean If breakdown is displayed then True else False
     */
    public static boolean verifyEngagedUnshelteredBreakdownIsDIsplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(engagedUnshelteredBreakdown, 60);
    }

    /**
     * This method will verify if Other Prospects breakdown is displayed or not
     * @return boolean If breakdown is displayed then True else False
     */
    public static boolean verifyOtherProspectBreakdownIsDIsplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(otherProspectBreakdown, 60);
    }

    /**
     * This method will verify if description popup is displayed
     * @return boolean If the information popup is displayed then True then False
     */
    public static boolean verifyDescriptionDisplayedForEngagedUnshelteredProspect() {
        boolean isDisplayed = false;
        WebDriverTasks.waitForElement(engagedUnshelteredInfoButton).click();
        isDisplayed = WebDriverTasks.verifyIfElementIsDisplayed(engagedUnshelteredInfoModal, 10);
        if (isDisplayed)
            WebDriverTasks.waitForElement(closeInformationModal).click();
        return isDisplayed;
    }

    /**
     * This method will verify if description popup is displayed
     * @return boolean If the information popup is displayed then True then False
     */
    public static boolean verifyDescriptionDisplayedForOtherProspect() {
        boolean isDisplayed = false;
        WebDriverTasks.waitForElement(otherProspectInfoButton).click();
        isDisplayed = WebDriverTasks.verifyIfElementIsDisplayed(otherProspectsInfoModal, 10);
        if (isDisplayed)
            WebDriverTasks.waitForElement(closeInformationModal).click();
        return isDisplayed;
    }

    /**
     * This method will select the date
     * @param date This is date in MM/DD/YYYY
     */
    public static void filterByDaily(String date) {
        selectDate(generalDailyDetails, date);
    }

    /**
     * This method will select the week (date)
     * @param date This is date in MM/DD/YYYY
     */
    public static void filterByWeekly(String date) {
        selectDate(generalWeeklyDetails, date);
    }

    /**
     * This method will click on 'Monthly' option on Engagement Unsheltered Prospect and select provided year and month
     * @param year This is year to be selected
     * @param month This is month to be selected
     */
    public static void filterByMonthly(String year, String month) {
        WebDriverTasks.scrollToElement(generalMonthlyDetails);
        WebDriverTasks.wait(2);
        WebDriverTasks.waitForElement(generalMonthlyDetails).click();
        WebElement calendar = WebDriverTasks.waitForElement(monthPicker);
        WebDriverTasks.waitForElement(showYears).click();
        WebDriverTasks.wait(2);
        calendar.findElement(By.xpath(".//span[text()='" + year + "']")).click();
        WebDriverTasks.wait(2);
        calendar.findElement(By.xpath(".//span[text()='" + month + "']")).click();
    }

    /**
     * This method will set provided value for JCC Client Start Date.
     * Created By : Venkat
     *
     * @param startDate This is start date in mm/dd/yyyy format
     */
    public static void selectDate(By date, String startDate) {
        WebDriverTasks.scrollToElement(date);
        WebDriverTasks.wait(1);
        WebDriverTasks.waitForElement(date).click();
        int day = Integer.parseInt(startDate.split("/")[1]);
        int month = Integer.parseInt(startDate.split("/")[0]);
        int year = Integer.parseInt(startDate.split("/")[2]);
        selectCalenderDate(day, month, year);
    }

    /**
     * This method will select day,month and year on calendar
     * Created By : Venkat
     *
     * @param day   This is day
     * @param month This is month that can range from 1 to 12
     * @param year  This is year
     */
    public static void selectCalenderDate(int day, int month, int year) {
        WebDriverTasks.waitForAngular();
        By selectYear = By.xpath("//div[@date-picker and contains(@style,'display: block;')]//select[@class='ui-datepicker-year']");
        By selectMonth = By.xpath("//div[@date-picker and contains(@style,'display: block;')]//select[@class='ui-datepicker-month']");
        By selectday = By.xpath("//div[@date-picker and contains(@style,'display: block;')]//td[@data-handler='selectDay']/a[text()='" + day + "']");
        String monthName = Base.monthNames[month - 1];
        WebDriverTasks.waitForElementAndSelect(selectYear).selectByVisibleText(String.valueOf(year));
        WebDriverTasks.waitForElementAndSelect(selectMonth).selectByVisibleText(monthName);
        WebDriverTasks.waitForElement(selectday).click();
    }

    /**
     * This method will iterate through each data grid page and verify that Client category column contains only 'Engaged Unsheltered Prospects' or 'Other Prospects'
     * Created By : Venkat
     * @return boolean If values matched expected values then True else False
     */
    public static boolean verifyDataInClientCategoryColumn(String expectedData) {
        boolean isValid = true;
        boolean hasNext;
        List<String> expectedValues = new LinkedList<>(Arrays.asList(expectedData.split(",")));
        int colNoForClientCategory = ViewReports.getColumnNo("Client Category");
        String clientCategoryCells = String.format("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(%d)", colNoForClientCategory);
        By dataCells = By.cssSelector(clientCategoryCells);
        do {
            WebDriverTasks.waitForElement(dataCells);
            for (WebElement cell : WebDriverTasks.driver.findElements(dataCells)) {
                if (!expectedValues.contains(cell.getText().trim())) {
                    isValid = false;
                    break;
                }
            }
            hasNext = WebDriverTasks.driver.findElement(nextDatGridPage).isEnabled();
            if (hasNext & isValid)
                WebDriverTasks.waitForElement(nextDatGridPage).click();
        } while (hasNext & isValid);
        if (WebDriverTasks.driver.findElement(firstDatGridPage).isEnabled())
            WebDriverTasks.waitForElement(firstDatGridPage).click();
        return isValid;
    }

    /**
     * This method will iterate through each data grid page and verify that Current Placement column contains only 'On Street'
     * Created By : Venkat
     * @return boolean If values matched expected values then True else False
     */
    public static boolean verifyDataInCurrentPlacementColumn() {
        boolean isValid = true;
        boolean hasNext;
        int colNoForClientCategory = ViewReports.getColumnNo("Current Placement");
        String clientCategoryCells = String.format("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(%d)", colNoForClientCategory);
        By dataCells = By.cssSelector(clientCategoryCells);
        do {
            WebDriverTasks.waitForElement(dataCells);
            for (WebElement cell : WebDriverTasks.driver.findElements(dataCells)) {
                if (!("On Street").contains(cell.getText().trim())) {
                    isValid = false;
                    break;
                }
            }
            hasNext = WebDriverTasks.driver.findElement(nextDatGridPage).isEnabled();
            if (hasNext & isValid)
                WebDriverTasks.waitForElement(nextDatGridPage).click();
        } while (hasNext & isValid);
        if (WebDriverTasks.driver.findElement(firstDatGridPage).isEnabled())
            WebDriverTasks.waitForElement(firstDatGridPage).click();
        return isValid;
    }

    /**
     * This method will verify that Client Category column is after Client Type and before Current Placement column
     * Created By : Venkat
     * @return boolean If client category is in between Client Type and Current Placement then True else False
     */
    public static boolean verifyClientCategoryColumnPosition() {
        int clientTypeColNo = ViewReports.getColumnNo("Client Type");
        int clientCategoryColNo = ViewReports.getColumnNo("Client Category");
        int currentPlacementColNo = ViewReports.getColumnNo("Current Placement");
        return clientCategoryColNo > clientTypeColNo && clientCategoryColNo < currentPlacementColNo;
    }

    /**
     * This method will click on Excel->Filtered Records
     * Created By : Venkat
     */
    public static void exportFilteredRecords() {
        WebDriverTasks.waitForElement(exportToExcel).click();
        WebDriverTasks.waitForElement(exportFilteredRecords).click();
    }

    /**
     * This method will verify if Client Category column is selected by default ot not in column picker
     * Created By : Venkat
     * @return boolean If selected by default then True else False
     */
    public static boolean isClientCategorySelected() {
        WebDriverTasks.waitForElement(columnPicker).click();
        return WebDriverTasks.verifyIfElementIsDisplayed(selectedClientCategoryColumn, 60);
    }

    /**
     * This method will verify if client category column is displayed or nor
     * @return If column is displayed then it will return True else False
     */
    public static boolean verifyClientCategoryColumnIsDisplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(clientCategoryColumn, 60);

    }

    /**
     * This method will uncheck client category column in column picker and verify that it's removed from grid
     * Created By : Venkat
     * @return boolean If removed then True else False
     */
    public static boolean deselectClientCategoryColumn() {
        WebDriverTasks.waitForElement(selectedClientCategoryColumn).click();
        return !WebDriverTasks.verifyIfElementIsDisplayed(clientCategoryColumn);
    }

    /**
     * This method will select client category column in column picker and verify that it's displayed in grid
     * Created By : Venkat
     * @return boolean If displayed then True else False
     */
    public static boolean selectClientCategoryColumn() {
        WebDriverTasks.waitForElement(deselectedClientCategoryColumn).click();
        return WebDriverTasks.verifyIfElementIsDisplayed(clientCategoryColumn, 60);
    }

    /**
     * This method will return records/row count in exported excel, excluding column
     * Created By : Venkat
     *
     * @return int This is number of record/rows in excel
     * @throws IOException File not found
     */
    public static int getRecordCountInExportedExcel(String sheetName) throws IOException {
        String exportedFileName = Helpers.returnMostRecentFileinDownloadFolder();
        return Helpers.getRecordCountInExcel(exportedFileName, sheetName) - 1;
    }

    /**
     * This method wil verify that provided column name exists in downloaded excel for 'Engaged Unsheltered Prospect & Other Prospect'
     * @param colName This is column name whihc is to be validated
     * @return boolean If column exists in excel then it will be True elase False
     * @throws IOException File not found
     */
    public static boolean isColumnDisplayedInExcel(String colName, String sheetName) throws IOException {
        String exportedFileName = Helpers.returnMostRecentFileinDownloadFolder();
        return Helpers.getAllColumnNames(exportedFileName, sheetName, 0).contains(colName);
    }

    /**
     * This method will verify position of 'Client Category' checkbox
     * Created By : Venkat
     */
    public static boolean verifyPositionOfClientCategoryCheckBox() {
        WebDriverTasks.waitForAngular();
        LinkedList<String> filterOptions = new LinkedList<>();
        for (WebElement option : WebDriverTasks.driver.findElements(allOptions)) {
            if (option.isDisplayed())
                filterOptions.add(option.getText().trim());
        }
        int seqNoClientType = filterOptions.indexOf("StreetSmart Client Type");
        int seqNoClientCategory = filterOptions.indexOf("Client Category");
        int seqNoCurrentPlacement = filterOptions.indexOf("Current Placement");
        return seqNoClientType < seqNoClientCategory & seqNoClientCategory < seqNoCurrentPlacement;
    }

    /**
     * This Method will search for provided value
     * Created By : Venkat
     *
     * @param searchValue This is the value for which we want to search in data grid
     */
    public static void searchFor(String searchValue) {
        WebDriverTasks.waitForElement(searchForInputBox).clear();
        WebDriverTasks.waitForElement(searchForInputBox).sendKeys(searchValue);
        WebDriverTasks.waitForElement(searchForInputBox).sendKeys(Keys.ENTER);
        WebDriverTasks.waitForAngular();
    }

    /**This method will verify if 'Current location of clients & Prospects' tile is displayed or not
     *
     * @return It will return True if displayed else False
     */
    public static boolean verifyCurrentLocationTileDisplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(currentLocationTile, 60);
    }

    /**
     * This method will click on Details button in 'Current location of clients & Prospects'
     */
    public static void clickOnDetailsButtonForCurrentLocationTile() {
        WebDriverTasks.scrollToElement(currentLocationTileDetails);
        WebDriverTasks.pageUpCommon();
        WebDriverTasks.waitForElement(currentLocationTileDetails).click();
    }

    /**
     * This method wil verify id Data pane is displayed or not for 'Current location of clients & Prospects'
     * @return It will return True if displayed else False
     */
    public static boolean verifyDataPaneIsDisplayedForCurrentLocation() {
        return WebDriverTasks.verifyIfElementIsDisplayed(currentLocationDataSection, 60);
    }

    /**
     * This method will verify that the provided columns exists in downloaded excel in provided sheet
     * @param sheetName This is sheet name
     * @param expectedColumns This is list of expected columns
     * @return boolean If all provided columns matched with columns in excel then it will return True else False
     * @throws IOException File not found
     */
    public static boolean verifyColumnsDisplayedInDownloadedExcel(String sheetName, List<String> expectedColumns) throws IOException {
        String downloadedExcelName = Helpers.returnMostRecentFileinDownloadFolder();
        List<String> excelColumns = Helpers.getAllColumnNames(downloadedExcelName, sheetName, 0);
        if (excelColumns.size() != expectedColumns.size())
            return false;
        for (int i = 0; i < excelColumns.size(); i++) {
            if (!excelColumns.get(i).trim().equals(excelColumns.get(i).trim()))
                return false;
        }
        return true;
    }

    /**
     * This method will return list of all checked/unchecked column from column picker,
     * @return List<String> This is columns list
     */
    public static List<String> getAllColumnPickerColumns() {
        WebDriverTasks.waitForAngular();
        List<String> allOptions = new LinkedList<>();
        List<WebElement> options = WebDriverTasks.driver.findElements(columnPickerOptions);
        for (WebElement option : options) {
            allOptions.add(option.getText().trim());
        }
        return allOptions;
    }

    /**
     * This method will return only checked column names from column picker
     * @return List<String> This is columns list
     */
    public static List<String> getCheckedColumnPickerColumns() {
        WebDriverTasks.waitForAngular();
        List<String> allOptions = new LinkedList<>();
        List<WebElement> options = WebDriverTasks.driver.findElements(checkedColumnPickerOptions);
        for (WebElement option : options) {
            allOptions.add(option.getText().trim());
        }
        return allOptions;
    }

    /**
     * This method will click on column picker in data grid
     */
    public static void clickOnColumnPicker() {
        WebDriverTasks.waitForElement(columnPicker).click();
    }
    
    public static void selectColumnOnColumnPicker(String columnName) {
        WebDriverTasks.waitForElement(By.xpath(String.format(selectcolumnOnColumnPicker , columnName))).click();
    }
    
    public static void deselectColumnOnColumnPicker(String columnName) {
        WebDriverTasks.waitForElement(By.xpath(String.format(deselectcolumnOnColumnPicker, columnName))).click();
    }
    
    public static boolean isColumnPresentOnGrid(String columnName) {
        boolean isColumnPresent =  WebDriverTasks.verifyIfElementIsDisplayed(By.xpath(String.format(columnOnGrid, columnName)));
        return isColumnPresent;
    }

    /**
     *This method will verify that 'Client Category' option is displayed after 'Client Type' and before 'Client Placement'
     */
    public static boolean verifyPositionOfClientCategoryColumnInColumnPicker() {
        List<String> allColumns = getAllColumnPickerColumns();
        int indexOfClientType = allColumns.indexOf("Client Type");
        int indexOfClientCategory = allColumns.indexOf("Client Category");
        int indexOfClientPlacement = allColumns.indexOf("Current Placement");

        return indexOfClientType < indexOfClientCategory & indexOfClientCategory < indexOfClientPlacement;
    }

    /**
     * This method will verify if total record count on tile is sames as records count on data grid for Client location tile
     * @return boolean If count matches then True else False
     */
    public static boolean verifyTotalCountMatchesGridRecordCountForClientLocationTile() {
        int tileRecordCount = Integer.parseInt(WebDriverTasks.waitForElement(totalCurrentLocationTileCount).getText().trim());
        int gridDataCount = getTotalRecordsCount();
        return tileRecordCount == gridDataCount;
    }

    /**
     * This method will verify if Current Placement column is displayed or not
     * @return boolean It will return True if column is displayed else False
     */
    public static boolean verifyCurrentPlacementColumnIsDisplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(currentPlacementColumn, 60);
    }

    /**
     * This method will click on Settings button for Current location tile
     * Created By : Venkat
     */
    public static void clickOnSettingsButtonForCurrentLocationTile() {
        WebDriverTasks.scrollToElement(currentLocationTileSetting);
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(currentLocationTileSetting).click();
    }

    /**
     * This method will verify that in Daily,Weekly or Monthly option are displayed in Settings
     * @param option This can be Daily,Monthly or Weekly
     * @return If the provided option is displayed then True else False
     */
    public static boolean verifySettingOptionOnCurrentLocationTile(String option) {
        By targetOption = null;
        switch (option.toLowerCase()) {
            case "daily":
                targetOption = currentLocationTileDailyOption;
                break;
            case "weekly":
                targetOption = currentLocationTileWeeklyOption;
                break;
            case "monthly":
                targetOption = currentLocationTileMonthlyOption;
                break;
            default:
                Assert.fail("Please provide any from Daily,Weekly or Monthly");
        }
        return WebDriverTasks.verifyIfElementIsDisplayed(targetOption, 60);
    }

    /**
     * This method will click on 'Details' button for provided tile name
     * Created By : Venkat
     * @param tileName This is tile name
     */
    public static void clickOnDetailsButtonFor(String tileName) {
        By detailsButton = By.xpath(String.format(detailsButtonOnTile, tileName));
        WebDriverTasks.scrollToElement(detailsButton);
        WebDriverTasks.wait(5);
        WebDriverTasks.pageUpCommon();
        WebDriverTasks.waitForElement(detailsButton).click();
    }

    /**
     * This method will verify that Detail pane is displayed with provided header
     * Created By : Venkat
     * @param paneName This is pane header name
     * @return boolean If pane is displayed it will return True, elae False
     */
    public static boolean verifyDataPaneIsDisplayedFor(String paneName) {
        By detailsPane = By.xpath(String.format(detailsPaneFor, paneName));
        return WebDriverTasks.verifyIfElementIsDisplayed(detailsPane, 60);
    }

    /**
     * This method will select Date of Birth checkbox in Advanced filter popup
     * Created By : Venkat
     */
    public static void clickOnRadioDateOfBirth() {
        if (WebDriverTasks.verifyIfElementIsDisplayed(selectRadioDateOfBirth, 3))
            WebDriverTasks.waitForElement(selectRadioDateOfBirth).click();
        else
            WebDriverTasks.waitForElement(selectRadioDateOfBirth2).click();
    }

    /**
     * This method will verify that values in Year dropdown in DOB field starts with expected year and is selectable
     * @param expectedYear This is expected year
     * @return If values in Year dropdown starts with provided values it will return True else False
     */
    public static boolean verifyDOBYearStartsWith(String expectedYear) {
        WebDriverTasks.waitForElement(dateOfBirthInputBox).click();
        Select dobDropDown = WebDriverTasks.waitForElementAndSelect(dateOfBirthYearDropdown);
        WebElement firstOption = dobDropDown.getOptions().get(0);
        if (firstOption.getText().trim().equals(expectedYear)) {
            dobDropDown.selectByVisibleText(expectedYear);
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method will close Advanced Filter Popup
     */
    public static void closeAdvancedFilterPopup() {
        WebDriverTasks.waitForElement(closeAdvancedFilterPopup).click();
    }

    /**
     * This will get breakdown count for provided type on 'Current location of client & Prospects' tile
     * Created By : Venkat
     * @param sectionName This is area/section name for which breakdown values are required
     * @return Integer This will be breakdown value
     */
    public static int getBreakdownCountFor(String sectionName) {
        By value = By.xpath(String.format(tileBreakDownValues, sectionName));
        String strValue = WebDriverTasks.waitForElement(value).getText();
        System.out.println("The count of " + sectionName + " is " + strValue);
        return Integer.parseInt(strValue);
    }
    
    public static void clickOnAdvancedFilter() {
        WebDriverTasks.waitForElement(clickOnAdvancedFilter).click();
    }

    public static void scrollToAdvancedFilter() {
        WebDriverTasks.scrollToElement(clickOnAdvancedFilter);
    }
    
    public static void enterFilterValueOnSelectedFilterOnAdvancedFilter(String selectFilter, String filterValue ) {
        String selectedFilter = String.format(selectedFilterOnAdvancedFilters, selectFilter);
        System.out.println("The name of the column filter is " + selectedFilter );
        System.out.println("The value of the column filter is " + filterValue);
        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).click();
        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).clear();
        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(filterValue);
        WebDriverTasks.specificWait(2000);
        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(Keys.DOWN);
        WebDriverTasks.specificWait(2000);
        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(Keys.ENTER);
    }
    
    public static void chooseColumnFilterOnAdvancedFilter(String columnFilter) {
        String selectedColumnFilter = String.format(columnToFilterOnAdvancedFilters, columnFilter);
        WebDriverTasks.waitForElement(By.xpath(selectedColumnFilter)).click();
    }

    /**
     * This method will apply advanced filter for given column with given values
     * Created By : Venkat
     * @param columnName This is column name
     * @param values This is filter values, in case of multiple values provide multiple values separated by comma(,)
     * @param clearExistingSelection If existing advanced filters needs to be cleared before applying filter provide True if not then False
     */  
    public static void advancedFilterBy(String columnName, String values, boolean clearExistingSelection) {
        HashMap<String,String> misc = new HashMap<>();
        misc.put("ClientCategory","Client Category");
        misc.put("Chronicity","HOME-STAT Client Type");
        misc.put("ProviderName","Provider");
        misc.put("EmailId","Email Id");
        misc.put("AreaName","Area Name");

        //Click on Advanced filter
        ClientList.clickOnAdvancedFilter();
        WebDriverTasks.wait(3);
        //Open Dropdown
        ClientList.clickOnClientCategoryFilterButton();
        //Clear existing checked checkboxes
        if (clearExistingSelection)
            ClientList.clearExistingSelection();

        String columnCheckbox = String.format("//li/label[@title='%s']/input[@type='checkbox']", columnName);
        String newFilterAdded = String.format("//div[contains(@class,'filter-options')]//button/div/span[normalize-space(@title)='%s']", misc.containsKey(columnName) ? misc.get(columnName)  : columnName);
        By selectColumn = By.xpath(columnCheckbox);
        By newFilterDropdown = By.xpath(newFilterAdded);
        String filterByValue = "div.open>ul.advance-filter-container>li>label[title='%s']>input";

        //Mark target column checkbox
        WebDriverTasks.waitForElement(selectColumn).click();

        //Check if filter by value is date
        if (Helpers.isValidDate(values, "MM/dd/yyyy")) {
            Base.selectDate(lastDateFieldOnAdvancedFilterPopup, values);
        } else {
            if(WebDriverTasks.verifyIfElementIsDisplayed(lastComboBoxOnAdvancedFilterPopup,3)){
                String filterBy[] = values.split(",");
                for (String value : filterBy) {
                    WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup).click();
                    WebDriverTasks.wait(3);
                    WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup).sendKeys(value);
                    WebDriverTasks.wait(5);
                    WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup).sendKeys(Keys.DOWN);
                    WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup).sendKeys(Keys.ENTER);

                    /*actions.click(WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup)).build().perform();
                    actions.sendKeys(WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup),value);
                    actions.sendKeys(WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup), Keys.DOWN);
                    actions.sendKeys(WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup), Keys.ENTER);*/
                }
            } else {
                //Open dropdown
                WebDriverTasks.waitForElement(newFilterDropdown).click();
                //If multiple filter values to be selected, split each value separated by comma
                String filterBy[] = values.split(",");
                for (String value : filterBy) {
                    By selectFilterByValue = By.cssSelector(String.format(filterByValue, value.trim()));
                    WebDriverTasks.waitForElement(selectFilterByValue).click();
                }
            }
        }
        WebDriverTasks.wait(5);
        //Click on Apply filter
        ClientList.clickOnApplyFilter();
    }

    /**
     * This method will reset all advanced filters
     *
     * Created By : Venkat
     */
    public static void clearAdvancedFilter() {
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clickOnResetFilter();
        ClientList.clickOnApplyFilter();
    }

    /**
     * This method will verify that all provided breakup components are displayed and  are in sequence
     * Created By : Venkat
     * @param breakup This is list of all breakup components separated by comma
     * @return boolean If all breakups are displayed and in sequence then True else False
     */
    public static boolean verifyBreakupSequenceForCurrentLocationTile(String breakup) {
        List<WebElement> breakupComponentElements = WebDriverTasks.waitForElements(currentLocationTileBreakups);
        List<String> actualCompoenents = new LinkedList<>();
        List<String> expectedComponents = new LinkedList<>(Arrays.asList(breakup.split(",")));
        for (WebElement element : breakupComponentElements) {
            actualCompoenents.add(element.getText().trim().split("\n")[1]);
        }
        if (actualCompoenents.size() != expectedComponents.size())
            return false;
        expectedComponents.removeAll(actualCompoenents);
        return expectedComponents.size() == 0;
    }

    /**
     * This method will verify that all breakup components contains information (i) icon on 'Current location of client & Prospects' tile
     * Created By : Venkat
     * @return boolean If all components displayed with information (i) icon then True else False
     */
    public static boolean verifyAllBreakupComponentsAreWithInforButtonForCurrentLocationTile() {
        List<WebElement> breakupComponentElements = WebDriverTasks.waitForElements(currentLocationTileBreakups);
        List<WebElement> breakupComponentElementsWithInforButton = WebDriverTasks.waitForElements(currentLocationTileBreakupsInfo);

        return breakupComponentElements.size() == breakupComponentElementsWithInforButton.size();
    }

    /**
     * This method will click on  (i) button near total records count on 'Current location of client & Prospects' tile
     * Created By : Venkat
     */
    public static void clickOnCurrentLocationTileInfo() {
        WebDriverTasks.waitForElement(currentLocationTileInfoButton).click();
    }

    /**
     * This method will verify that information popup is displayed or not for 'Current location of client & Prospects' tile when (i) button near total records count is clicked
     * Created By : Venkat
     * @return boolean
     */
    public static boolean verifyInfoPopupIsDisplayedForCurrentLocationTile() {
        return WebDriverTasks.verifyIfElementIsDisplayed(currentLocationTileInfoPopup, 30);
    }

    /**
     * This method will return expected information text for 'Current location of client & Prospects' tile
     * Created By : Venkat
     * @return String This is expected information text
     */
    public static String getExpectedCurrentLocationInfoText() {
        return "Current Location of Clients & Prospects" +
                "The total number of HOME-STAT Clients and Prospects living in all location types as of today. Counts will vary depending on timeframe selected: daily, weekly, monthly." +
                "The Current Location Type (on street, in subway, drop-in center, correctional facility, hospital, detox, safe haven, stabilization bed, DHS shelter, church bed, HRA 2010e, DHS gen pop, HUD VASH, city subsidies, family reunification, fair market, permanent assisted living/nursing home, transitional assisted living/nursing home) of HOME-STAT Clients with an open case, Engaged Unsheltered Prospects and Other Prospects." +
                "Source: DHS StreetSmart" +
                "Contact information:" +
                "Ijeoma Genevieve Mbamalu" +
                "mbamalui@dss.nyc.gov" +
                "(212) 487-2355";
    }

    /**
     * This method will return information modal text
     * @return String This is information text
     */
    public static String getInformationPopupText() {
        return WebDriverTasks.waitForElement(currentLocationTileInfoPopup).getText();
    }

    /**
     * This method will fetch text from existing information popup and will compare it with provided expected text.
     * Created By : Venkat
     * @param expectedText This is expected text
     * @return boolean If actual and expected text matches then it will be True else False
     */
    public static boolean verifyPopupTextMatches(String expectedText, String actualText) {
        //New lines and tabs are replaced by blanks and excluding first char whihc is close button 'x'
        actualText = actualText.replaceAll("[\\\n\\\r\\\t]", "").substring(1);
        expectedText = expectedText.replaceAll("[\\\n\\\r\\\t]", "");
        return actualText.equals(expectedText);
    }

    /**
     * This method will compare number of information (i) icons for given tile name matches expected count
     * Created By : Venkat
     * @param tileName This is tile name
     * @param expectInfoIconCount This is expected information icon count
     * @return boolean If actual count matches expected count then it will return True else False
     */
    public static boolean verifyTileContainsInformationIcons(String tileName, int expectInfoIconCount) {
        By allButtonsOnTile = By.xpath(String.format(allInfoButtonForTile, tileName));
        return WebDriverTasks.waitForElements(allButtonsOnTile).size() == expectInfoIconCount;
    }

    /**
     * This method will click on close (X) button on information modal
     * Created By : Venkat
     */
    public static void clickOnCloseInformationModal() {
        WebDriverTasks.waitForElement(closeInformationModal).click();
    }

    /**
     * This method will open click on each information button and verify that information modal contains expected text
     * Created By : Venkat
     * @param tileName This is tile name
     * @param expectedText This is expected text that should be present in information text
     * @return boolean If expected text is present then it will return True else False
     */
    public static boolean verifyInformationPopupContains(String tileName, String expectedText) {
        By allButtonsOnTile = By.xpath(String.format(allInfoButtonForTile, tileName));
        List<WebElement> infoIcons = WebDriverTasks.waitForElements(allButtonsOnTile);
        for (WebElement element : infoIcons) {
            WebDriverTasks.waitForAngular();
            WebDriverTasks.wait(3);
            element.click();
            WebDriverTasks.wait(3);
            String informationText = getInformationPopupText();
            clickOnCloseInformationModal();
            if (!informationText.contains(expectedText))
                return false;
        }
        return true;
    }

    public static boolean verifyLatestContactDateColumnDisplayed() {
        WebDriverTasks.waitForAngular();
        return WebDriverTasks.verifyIfElementIsDisplayed(latestContactDate, 30);
    }

    /**
     * This method will verify that 'Latest Contact Date' is displayed after 'Client Category' column
     * Created By : Venkat
     * @return boolean If 'Latest Contact Date' column is displayed after 'Client Category' column then it will return True else False
     */
    public static boolean verifyLatestContactDateColumnPosition() {
        int latestContactDate = ViewReports.getColumnNo("Latest Contact Date");
        int clientCategoryColNo = ViewReports.getColumnNo("Client Category");
        return clientCategoryColNo < latestContactDate;
    }

    /**
     * This method will verify that date sorted in given column is in ascending order
     * Created By : Venkat
     * @param columnName This is column name for which order will be validated
     * @return boolean If dates are in ascending order then it will return True else False
     * @throws ParseException Date parse exception
     */
    public static boolean verifyDateIsInAscendingOrder(String columnName) throws ParseException {
        boolean isValid = true;
        boolean hasNext;
        String dateFormat = "MM/dd/yyyy";
        String cellData1, cellData2;
        Date date1, date2;
        int colNo = ViewReports.getColumnNo(columnName);
        String cellsLocator = String.format("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(%d)", colNo);
        By dataCells = By.cssSelector(cellsLocator);
        do {
            List<WebElement> cells = WebDriverTasks.waitForElements(dataCells);
            for (int i = 0; i < (cells.size() - 1); i++) {
                cellData1 = cells.get(i).getText().trim();
                cellData2 = cells.get(i + 1).getText().trim();
                if (!(Helpers.isValidDate(cellData1, dateFormat) & Helpers.isValidDate(cellData2, dateFormat))) {
                    isValid = false;
                    break;
                }
                date1 = Helpers.parseDate(cellData1, dateFormat);
                date2 = Helpers.parseDate(cellData2, dateFormat);
                if (!Helpers.isAfterOrSame(date1, date2)) {
                    isValid = false;
                    break;
                }
            }
            hasNext = WebDriverTasks.driver.findElement(nextDatGridPage).isEnabled();
            if (hasNext & isValid)
                WebDriverTasks.waitForElement(nextDatGridPage).click();
        } while (hasNext & isValid);
        if (WebDriverTasks.driver.findElement(firstDatGridPage).isEnabled())
            WebDriverTasks.waitForElement(firstDatGridPage).click();
        return isValid;
    }

    /**
     * This method will verify that date sorted in given column is in descending order
     * Created By : Venkat
     * @param columnName This is column name for which order will be validated
     * @return boolean If dates are in descending order then it will return True else False
     * @throws ParseException Date parse exception
     */
    public static boolean verifyDateIsInDescendingOrder(String columnName) throws ParseException {
        boolean isValid = true;
        boolean hasNext;
        String dateFormat = "MM/dd/yyyy";
        String cellData1, cellData2;
        Date date1, date2;
        int colNo = ViewReports.getColumnNo(columnName);
        String cellsLocator = String.format("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(%d)", colNo);
        By dataCells = By.cssSelector(cellsLocator);
        do {
            List<WebElement> cells = WebDriverTasks.waitForElements(dataCells);
            for (int i = 0; i < (cells.size() - 1); i++) {
                cellData1 = cells.get(i).getText().trim();
                cellData2 = cells.get(i + 1).getText().trim();
                if (!(Helpers.isValidDate(cellData1, dateFormat) & Helpers.isValidDate(cellData2, dateFormat))) {
                    isValid = false;
                    break;
                }
                date1 = Helpers.parseDate(cellData1, dateFormat);
                date2 = Helpers.parseDate(cellData2, dateFormat);
                if (!Helpers.isBeforeOrSame(date1, date2)) {
                    isValid = false;
                    break;
                }
            }
            hasNext = WebDriverTasks.driver.findElement(nextDatGridPage).isEnabled();
            if (hasNext & isValid)
                WebDriverTasks.waitForElement(nextDatGridPage).click();
        } while (hasNext & isValid);
        if (WebDriverTasks.driver.findElement(firstDatGridPage).isEnabled())
            WebDriverTasks.waitForElement(firstDatGridPage).click();
        return isValid;
    }

    /**
     * This method will click on 'Latest Contact Date' column
     * Created By : Venkat
     */
    public static void clickOnLatestContactDateColumn() {
        WebDriverTasks.waitForElement(latestContactDate).click();
    }

    /**
     * This will verify that data in 'Latest Contact Date' column is matching with expected data
     * Created By : Venkat
     * @param expectedData This is expected data under the column
     * @return boolean If all values match with expected data then it will return True else False
     */
    public static boolean verifyDataInLatestContactDateColumn(String expectedData) {
        boolean isValid = true;
        boolean hasNext;
        List<String> expectedValues = new LinkedList<>(Arrays.asList(expectedData.split(",")));
        int columnNo = ViewReports.getColumnNo("Latest Contact Date");
        String cells = String.format("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(%d)", columnNo);
        By dataCells = By.cssSelector(cells);
        do {
            WebDriverTasks.waitForElement(dataCells);
            for (WebElement cell : WebDriverTasks.driver.findElements(dataCells)) {
                if (!expectedValues.contains(cell.getText().trim())) {
                    isValid = false;
                    break;
                }
            }
            hasNext = WebDriverTasks.driver.findElement(nextDatGridPage).isEnabled();
            if (hasNext & isValid)
                WebDriverTasks.waitForElement(nextDatGridPage).click();
        } while (hasNext & isValid);
        if (WebDriverTasks.driver.findElement(firstDatGridPage).isEnabled())
            WebDriverTasks.waitForElement(firstDatGridPage).click();
        return isValid;
    }

    /**
     * This method will get Latest contact date from first record
     * Created By : Venkat
     * @return String Latest Contact Date
     */
    public static String getLatestContactDateForFirstRecord() {
        int columnNo = ViewReports.getColumnNo("Latest Contact Date");
        String cells = String.format("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(%d)", columnNo);
        By dataCells = By.cssSelector(cells);
        return WebDriverTasks.waitForElement(dataCells).getText().trim();
    }

    /**
     *This method will verify that 'Latest Contact Date' is displayed after 'Client Category' option
     * Created By : Venkat
     *
     * @return boolean If  'Latest Contact Date' is displayed after 'Client Category' option then it will return True else False
     */
    public static boolean verifyPositionOfLatestContactDateColumnInColumnPicker() {
        List<String> allColumns = getAllColumnPickerColumns();
        int indexOfClientType = allColumns.indexOf("Client Type");
        int indexOfLatestContactDate = allColumns.indexOf("Latest Contact Date");
        return indexOfClientType < indexOfLatestContactDate;
    }

    /**
     * This method will verify if Client Category column is selected by default ot not in column picker
     * Created By : Venkat
     * @return boolean If selected by default then True else False
     */
    public static boolean isLatestContactDateSelected() {
        WebDriverTasks.waitForElement(columnPicker).click();
        return WebDriverTasks.verifyIfElementIsDisplayed(selectedLatestContactDateColumn, 30);
    }

    /**
     * This method will uncheck Latest contact date column in column picker and verify that it's removed from grid
     * Created By : Venkat
     * @return boolean If removed then True else False
     */
    public static boolean deselectLatestContactDateColumn() {
        WebDriverTasks.waitForElement(selectedLatestContactDateColumn).click();
        return !WebDriverTasks.verifyIfElementIsDisplayed(latestContactDate, 30);
    }

    /**
     * This method will select Latest Contact Date column in column picker and verify that it's displayed in grid
     * Created By : Venkat
     * @return boolean If displayed then True else False
     */
    public static boolean selectLatestContactDate() {
        WebDriverTasks.waitForElement(deselectedLatestContactDateColumn).click();
        return WebDriverTasks.verifyIfElementIsDisplayed(latestContactDate, 30);
    }
    
    
    
    static By Top_element = By.xpath("//*[@id=\"content\"]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/div/div[1]/h4/a/span");
	static By Grid_Count = By.xpath("//*[@id=\"dtlGrid\"]/div[2]/div[2]/div/span");
	static By Search_box = By.id("HomestatClientDetailsSearchText");
	static By Advance_Filter = By.xpath("//*[@id='data-table_wrapper']/a");
	static By Filter_options = By.xpath("//*[@id=\"dropDown_Add Filter\"]/div");
	static By StreetSmart_ID = By.xpath("//*[@id=\"column1\"]/ul/li[2]/label");
	static By type = By.xpath("//*[@id=\"token-input-\"]");
	static By Apply = By.xpath("//*[@id=\"nonDHSVeteranDataSaveButton\"]");
	static By Remove_filter = By.xpath("//*[@id=\"divFiltes\"]/div[2]/div/div/div/div/div");
	static By Search = By.xpath("//*[@id='data-table_filter']/label/input");
	static By Filter_applied= By.xpath("//*[@id=\"data-table_wrapper\"]/div[1]/advance-filter-selected-columns/div/div/div");
	static String filter;
	
	static String Search_text;
	//Performs validation of counts on all the 12 dashboard tiles, compares count on the tiles and on the grid. Search and Advance filters are performed on the grid for all the tiles.
	public static void Tile_verification(int i) throws InterruptedException {
		for (int j = 1; j <= 4; j++)
		{
			int Sub_count = 0;
			String text = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/div/div/div[" + i + "]/div["
					+ j + "]/div[1]/div/div[1]/h4/a/span")).getText();
			System.out.println("***************************************");
			System.out.println("Tile name is : " + text);
			String Count = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/div/div/div[" + i + "]/div["
					+ j + "]/div[1]/div/div[3]/div/div[1]")).getText();
			int Big_number = Integer.parseInt(Count);
			// Added below condition as the child elements of 'HOME-STAT Clients Placed' tile are not in the same structure as others.
			if (!text.equalsIgnoreCase("HOME-STAT Clients Placed") ){
				WebElement parentElement = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/div/div/div["
						+ i + "]/div[" + j + "]/div[1]/div/div[3]/div/div[2]/ul"));
				List<WebElement> childrenElements = parentElement.findElements(By.xpath("li/span"));
				int count = childrenElements.size();
				for (WebElement webElement : childrenElements) {
					String a = webElement.getText();
					int child_count = tryParse(a);
					Sub_count = Sub_count + child_count;
				}

			} else // Below will be executed only for 'HOME-STAT Clients Placed' tile.
			{
				for (int k = 1; k <= 3; k++) {
				String a = driver.findElement(By.xpath("//*[@id='content']/div[1]/div[2]/div/div/div[3]/div[1]/div[1]/div/div[3]/div/div[2]/ul[" + k + "]/li/span")).getText();
				int child_count = tryParse(a);
				Sub_count = Sub_count + child_count;
			}
			}// Compare the big Number on tile with the sum of all the small numbers.
			if (Big_number == Sub_count) {
				System.out.println("Big number and sum of all the small number are equal, Test case Pass");
			} else {
				System.out.println("Test case Fail");
			}
			// Xpath link for Details on 'Housing Packet/Subsidy in Process' is not same as other tiles, Need below logic to open grid for 'Housing Packet/Subsidy in Process' tile.
			if (!text.equalsIgnoreCase("Housing Packet/Subsidy in Process") )
			{
			driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/div/div/div[" + i + "]/div[" + j
					+ "]/div[1]/div/div[4]/div/div/a")).click();
			}
			else
			{
				driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/div/div/div[3]/div[2]/div[1]/div/div[4]/div/div/span")).click();
			}
			Thread.sleep(10000);
			
			// Compare the tile count and the grid count.
			if (Big_number == gridcount()) {
				System.out.println("Number of the records on the tile and grid are same, Test case Pass");
			} else {
				System.out.println("Total records on the grid and tile are not same, Test case Fail");
			}

			//Search and Advance filters tests are executed only when there are records, will not execute when the total count on tile is zero.
			if(gridcount() !=0 )
			{
			// Search functionality is performed on the grid.
			Search();
			if (gridcount() == 1) {
				System.out.println("Search successful, Test case Pass");
			} else {
				System.out.println("Search was not successful, Test case Fail");
			}
			Clear_Search();
			//Applies advance filter for StreetSmart ID.
			Advance_filter();
			if (gridcount() == 1) {
				System.out.println("Advance filter successful, Test case Pass");
			} else {
				System.out.println("Advance filter was not successful, Test case Fail");
			}
			
			Reset_filter();
			}
			WebDriverTasks.scrollToElement(Top_element);
		}

	}
// Method to convert String to an Integer.
	public static Integer tryParse(String obj) {
		Integer retVal;
		try {
			retVal = Integer.parseInt(obj);
		} catch (NumberFormatException nfe) {
			retVal = 0; 
		}
		return retVal;
	}

	// Method to fetch the count on the grid.
	public static int gridcount() throws InterruptedException 
	{WebDriverTasks.waitForAngular();
		String a = driver.findElement(By.xpath("//*[@id=\"dtlGrid\"]/div[2]/div[2]/div/span")).getText();
		if (a.length() > 10)
		{
		a= a.replaceFirst("items", " ");
		String array1[]= a.split("of");
		if(array1.length > 1)
			
		      return tryParse(array1[1].trim());
		}
		return 0;
	}
// Method to apply advance filter for Street smart ID.
	public static String Advance_filter() throws InterruptedException {
		Search_text=WebDriverTasks.waitForElement(streetSmartIDOnGrid).getText();
		WebDriverTasks.waitForElement(Advance_Filter).click();
		WebDriverTasks.waitForElement(Filter_options).click();
		WebDriverTasks.waitForElement(StreetSmart_ID).click();
		WebDriverTasks.waitForElement(type).click();
		WebDriverTasks.waitForElement(type).sendKeys(Search_text);
		Thread.sleep(1000);
		WebDriverTasks.waitForElement(type).sendKeys(Keys.DOWN);
		WebDriverTasks.waitForElement(type).sendKeys(Keys.ENTER);
		WebDriverTasks.waitForElement(Apply).click();
		WebElement element = WebDriverTasks.waitForElement(Filter_applied);
		WebDriverTasks.waitUntilElementIsVisible(element, 15);
		 filter= element.getText();
		
		return filter;
	}

	//Method to reset the filter.
	public static void Reset_filter() throws InterruptedException {
		
		WebDriverTasks.waitForElement(Advance_Filter).click();
		WebDriverTasks.waitForElement(Remove_filter).click();
		WebDriverTasks.waitForElement(Apply).click();
		
	}
//Method to perform search on the grid.
	public static void Search() throws InterruptedException {
		WebDriverTasks.waitForAngular();
		Search_text=WebDriverTasks.waitForElement(streetSmartIDOnGrid).getText();
		WebDriverTasks.waitForElement(Search).click();
		WebDriverTasks.waitForElement(Search).sendKeys(Search_text);
		WebDriverTasks.waitForElement(Search).sendKeys(Keys.ENTER);
	
	}
//Method to perform search on the grid.
	public static void Clear_Search() throws InterruptedException {
			
		WebDriverTasks.waitForElement(Search).click();
		WebDriverTasks.waitForElement(Search).clear();
		WebDriverTasks.waitForElement(Search).sendKeys(Keys.ENTER);
		
		}



}
	

