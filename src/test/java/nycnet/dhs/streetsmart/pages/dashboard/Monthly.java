package nycnet.dhs.streetsmart.pages.dashboard;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.reports.ViewReports;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Monthly extends StartWebDriver {
    //Added By Venkat
    public static By allTileHeaders = By.xpath("//div[contains(@class,'widget-boxes')]/div[not(contains(@class,'ng-hide'))]//h4[contains(@class,'panel-title')]/span[text()]");
    //    public static String wideTile = "//span[text()='']/parent::h4/../following-sibling::div[contains(@class,'widget-stats')]//*[text()='']/../preceding-sibling::div[contains(@class,'stats-number')]/span";
    public static String wideTile_1 = "//span[text()='%s']/parent::h4/../following-sibling::div[contains(@class,'widget-stats')]//*[text()='%s']/../preceding-sibling::div[contains(@class,'stats-number')]/span";
    public static String wideTile_2 = "//span[text()='%s']/parent::h4/../following-sibling::div[contains(@class,'widget-stats')]//*[text()='%s']/preceding-sibling::div[contains(@class,'stats-number')]/span";
    public static String smallTile = "//span[text()='%s']/parent::h4/../following-sibling::div[contains(@class,'widget-stats')]//*[text()='%s']/../preceding-sibling::span";
    public static String detailsLink = "//span[text()='%s']/parent::h4/../following-sibling::a[@class='view-details']";
    public static By homeStatOverviewLink = By.xpath("//a[text()='HOME-STAT Overview']");
    public static String countBySection_1 = "//*[text()='%s']/../preceding-sibling::div/span";
    public static String countBySection_2 = "//*[text()='%s']/preceding-sibling::div/span";
    public static By columnPicker = By.xpath("//div[@role='button']/i[@aria-label='Grid Menu']");
    public static By visibleColumns = By.xpath("//ul[@class='ui-grid-menu-items']/li/button[not(contains(@class,'ng-hide'))]/i[@class='ui-grid-icon-ok']/..");
    public static String hideColumn = "//ul[@class='ui-grid-menu-items']/li/button[not(contains(@class,'ng-hide')) and contains(normalize-space(),'%s')]/i[@class='ui-grid-icon-ok']/..";
    public static String unhideColumn = "//ul[@class='ui-grid-menu-items']/li/button[not(contains(@class,'ng-hide')) and contains(normalize-space(),'%s')]/i[@class='ui-grid-icon-cancel']/..";
    public static String columnInGrid = "//div[@role='columnheader']/div[@role='button']/span[text()='%s']";
    public static By removeFilterButton = By.cssSelector("div[ng-if='val.Selected']>div.cursor-pointer");
    public static By selectYearAndMonth = By.xpath("//select[@ng-model='ChDashboardCalenderId']");

    static By nextDatGridPage = By.cssSelector("button.ui-grid-pager-next");
    static By firstDatGridPage = By.cssSelector("button.ui-grid-pager-first");

    /**
     * This method will get list of all tiles on page
     * Created By : Venkat
     *
     * @return List This is List of string for all the tiles on page
     */
    public static LinkedList<String> getAllTiles() {
        LinkedList<String> tileHeaders = new LinkedList<>();
        List<WebElement> tiles = WebDriverTasks.waitForElements(allTileHeaders);
        for (WebElement tile : tiles) {
            tileHeaders.add(tile.getText().trim());
        }
        return tileHeaders;
    }

    /**
     * This method will get count of records displayed on tile for specified section
     * Created By : Venkat
     *
     * @param tileName This is tile name
     * @param field    This is field name
     * @return Integer This is count displayed on tile
     */
    public static int getCountOf(String tileName, String field) {
        LinkedList<String> wideTileSubSection = new LinkedList<>(Arrays.asList("Unique Clients"));
        LinkedList<String> wideTiles = new LinkedList<>(Arrays.asList("Housing Packet/Subsidy in Process", "Active Housing Packet/Subsidy"));
        String locatorXpath;
        if (wideTiles.contains(tileName))
            if (wideTileSubSection.contains(field))
                locatorXpath = wideTile_2;
            else
                locatorXpath = wideTile_1;
        else
            locatorXpath = smallTile;
        locatorXpath = String.format(locatorXpath, tileName, field);
        By byLocator = By.xpath(locatorXpath);
        WebDriverTasks.scrollToElement(byLocator);
        String count = WebDriverTasks.waitForElement(byLocator).getText().trim();
        return Integer.parseInt(count);
    }

    /**
     * This method will get count of records displayed for specified label/field on details page
     * Created By : Venkat
     *
     * @param field This is field name
     * @return Integer This is count displayed on tile
     */
    public static int getCountOf(String field) {
        WebDriverTasks.waitForAngular();
        By byLocator = null;
        By locator_1 = By.xpath(String.format(countBySection_1, field));
        By locator_2 = By.xpath(String.format(countBySection_2, field));
        if (WebDriverTasks.verifyIfElementIsDisplayed(locator_1, 2)) {
            byLocator = locator_1;
        } else {
            byLocator = locator_2;
        }
        String count = WebDriverTasks.waitForElement(byLocator).getText().trim();
        return Integer.parseInt(count);
    }

    /**
     * This method will verify that total record count matches the sum of it's sub-section record count.
     * Created By : Venkat
     *
     * @param tile This is tile name to be validated
     * @return Boolean If the total matches then it will return True else False
     */
    public static boolean verifyTotalTileCount(String tile) {
        HashMap<String, LinkedList<String>> parentChildLink = new HashMap<>();
        parentChildLink.put("HOME-STAT Clients", new LinkedList<>(Arrays.asList("Clients", "Full Name Given", "Full Name Not Given")));
        parentChildLink.put("Current Location of Clients and Prospects", new LinkedList<>(Arrays.asList("Total Not in Permanent Housing", "Total On Street & Other Settings", "Clients In Transitional Settings")));
//        parentChildLink.put("HOME-STAT Clients Placed in Month",new LinkedList<>());
        parentChildLink.put("Housing Packet/Subsidy in Process", new LinkedList<>(Arrays.asList("Unique Clients", "No Steps Completed Yet", "Verified All Vital Documents", "Verified at least One Income Source", "Eligibility Determined")));
        parentChildLink.put("Active Housing Packet/Subsidy", new LinkedList<>(Arrays.asList("Unique Clients", "No Steps Completed Yet", "Searching for Housing", "Linked", "Leased")));
        parentChildLink.put("Prospect Clients", new LinkedList<>(Arrays.asList("Clients", "Full Name Given", "Full Name Not Given")));
        parentChildLink.put("Engagements", new LinkedList<>(Arrays.asList("Engagements", "HOME-STAT Clients", "Other Prospects", "Engaged Unsheltered Prospects")));
        if (!parentChildLink.containsKey(tile))
            return true;

        System.out.println("Validating counts for " + tile);

        int totalCount = getCountOf(tile, parentChildLink.get(tile).get(0));
        int childCount = 0;
        LinkedList<String> childCounts = parentChildLink.get(tile);
        WebDriverTasks.wait(5);
        for (int i = 1; i < childCounts.size(); i++) {
            WebDriverTasks.wait(2);
            childCount = childCount + getCountOf(tile, childCounts.get(i));
        }
        WebDriverTasks.wait(5);
        return totalCount == childCount;
    }

    /**
     * This method will verify that grid count matches with the count displayed on tile
     * Created By : Venkat
     *
     * @param tile This is tile name to be validated
     * @return Boolean If the tile record count matches with grid record count then it will return True else False
     */
    public static boolean verifyGridCount(String tile) {
        LinkedList<String> tileWithDetailsLink = new LinkedList<>(Arrays.asList("HOME-STAT Clients", "Current Location of Clients and Prospects",
                "HOME-STAT Clients Placed in Month", "Housing Packet/Subsidy in Process", "Active Housing Packet/Subsidy"));
        String subSections;
        int countInTile = 0;
        HashMap<String, String> tileCount = new HashMap<>();
        tileCount.put("HOME-STAT Clients", "Clients");
        tileCount.put("Current Location of Clients and Prospects", "Clients In Permanent Housing & Receiving Aftercare##Total Not in Permanent Housing");
        tileCount.put("HOME-STAT Clients Placed in Month", "Placed to Other Settings##Placed in Transitional Settings##Placed to Permanent Housing");
        tileCount.put("Housing Packet/Subsidy in Process", "Unique Clients");
        tileCount.put("Active Housing Packet/Subsidy", "Unique Clients");

        if (!tileWithDetailsLink.contains(tile))
            return true;
        System.out.println("Verifying record count on tile with record count in grid " + tile);
        subSections = tileCount.get(tile);
        if (subSections.contains("##")) {
            for (String sectionName : subSections.split("##")) {
                WebDriverTasks.wait(2);
                countInTile += getCountOf(tile, sectionName);
            }
        } else {
            countInTile = getCountOf(tile, subSections);
        }

        By detailsLocator = By.xpath(String.format(detailsLink, tile));
        WebDriverTasks.waitForElement(detailsLocator).click();
        WebDriverTasks.wait(5);
        removeAdvancedFilters();
        WebDriverTasks.wait(2);
        int countInGrid = ViewReports.getTotalRecordsCount();
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(homeStatOverviewLink).click();
        return countInGrid == countInTile;
    }

    /**
     * This method will verify that tile's count/sub-section record count on home screen matches with tile's details sub-section count.
     * Created By : Venkat
     *
     * @param tile This is tile name to be validated
     * @return Boolean If the count matches then it will return True else False
     */
    public static boolean verifyTileCountMatchesWithDetails(String tile) {
        HashMap<String, String> labelMapper = new HashMap<>();
        labelMapper.put("Clients In Permanent Housing & Receiving Aftercare", "In Permanent Housing & Receiving Aftercare");
        labelMapper.put("Total On Street & Other Settings", "On Street & Other Settings");
        labelMapper.put("Clients In Transitional Settings", "In Transitional Settings");
        labelMapper.put("Placed to Permanent Housing", "Placements to Permanent Housing");

        HashMap<String, LinkedList<String>> parentChildLink = new HashMap<>();
        parentChildLink.put("HOME-STAT Clients", new LinkedList<>(Arrays.asList("Clients", "Full Name Given", "Full Name Not Given", "Assigned a Case Manager")));
        parentChildLink.put("Current Location of Clients and Prospects", new LinkedList<>(Arrays.asList("Clients In Permanent Housing & Receiving Aftercare", "Total On Street & Other Settings", "Clients In Transitional Settings")));
        parentChildLink.put("HOME-STAT Clients Placed in Month", new LinkedList<>(Arrays.asList("Placed to Other Settings", "Placed in Transitional Settings", "Placed to Permanent Housing")));
        parentChildLink.put("Housing Packet/Subsidy in Process", new LinkedList<>(Arrays.asList("Unique Clients", "No Steps Completed Yet", "Verified All Vital Documents", "Verified at least One Income Source", "Eligibility Determined")));
        parentChildLink.put("Active Housing Packet/Subsidy", new LinkedList<>(Arrays.asList("Unique Clients", "No Steps Completed Yet", "Searching for Housing", "Linked", "Leased")));

        HashMap<String, Integer> gridData = new HashMap<>();

        if (!parentChildLink.containsKey(tile))
            return true;

        System.out.println("Verifying tile count with details page for " + tile);

        for (String label : parentChildLink.get(tile)) {
            gridData.put(label, getCountOf(tile, label));
        }

        By detailsLocator = By.xpath(String.format(detailsLink, tile));
        WebDriverTasks.waitForElement(detailsLocator).click();
        WebDriverTasks.wait(5);

        for (String label : gridData.keySet()) {
            WebDriverTasks.wait(1);
            int expectedCount = gridData.get(label);
            if (labelMapper.containsKey(label)) {
                label = labelMapper.get(label);
            }
            if (expectedCount != getCountOf(label))
                return false;
        }
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(homeStatOverviewLink).click();
        return true;
    }

    /**
     * This method will verify column picker functionality.
     * Created By : Venkat
     *
     * @param tile This is tile to be validated
     * @return Boolean If column picker functionality works as expected then it will return True else False
     */
    public static boolean verifyColumnPickerFunctionality(String tile) {
        LinkedList<String> validTiles = new LinkedList<>();
        validTiles.add("HOME-STAT Clients");
        validTiles.add("Current Location of Clients and Prospects");
        validTiles.add("HOME-STAT Clients Placed in Month");
        validTiles.add("Housing Packet/Subsidy in Process");
        validTiles.add("Active Housing Packet/Subsidy");

        if (!validTiles.contains(tile))
            return true;

        By detailsLocator = By.xpath(String.format(detailsLink, tile));
        WebDriverTasks.waitForElement(detailsLocator).click();
        WebDriverTasks.wait(2);

        String targetColumn = getVisibleColumnName();
        WebDriverTasks.wait(2);

        if (!hideColumnAndVerify(targetColumn))
            return false;

        if (!unhideColumnAndVerify(targetColumn))
            return false;

        WebDriverTasks.waitForElement(homeStatOverviewLink).click();
        return true;
    }

    /**
     * This method will return first visible column from column picker
     * Created By : Venkat
     *
     * @return String This is visible column name
     */
    public static String getVisibleColumnName() {
        WebDriverTasks.waitForAngular();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(columnPicker).click();
        String colName = WebDriverTasks.waitForElement(visibleColumns).getText().trim();
        WebDriverTasks.waitForElement(columnPicker).click();
        return colName;
    }

    /**
     * This method will click on column picker and uncheck given column to hide it and verify that column is hidden from data grid
     * Created By : Venkat
     *
     * @param colName This is column name to be hidden.
     * @return Boolean If the column is hidden from data grid then it will return True else False
     */
    public static boolean hideColumnAndVerify(String colName) {
        WebDriverTasks.waitForElement(columnPicker).click();
        WebDriverTasks.wait(2);
        WebDriverTasks.waitForElement(By.xpath(String.format(hideColumn, colName))).click();
        WebDriverTasks.waitForElement(columnPicker).click();
        WebDriverTasks.waitForAngular();
        WebDriverTasks.wait(4);
        return !WebDriverTasks.verifyIfElementIsDisplayed(By.xpath(String.format(columnInGrid, colName)), 5);
    }

    /**
     * This method will click on column picker and uncheck given column to un-hide it and verify that column is displayed in data grid
     * Created By : Venkat
     *
     * @param colName This is column name to be un-hidden.
     * @return Boolean If the column is visible in data grid then it will return True else False
     */
    public static boolean unhideColumnAndVerify(String colName) {
        WebDriverTasks.waitForElement(columnPicker).click();
        WebDriverTasks.wait(2);
        WebDriverTasks.waitForElement(By.xpath(String.format(unhideColumn, colName))).click();
        WebDriverTasks.waitForElement(columnPicker).click();
        WebDriverTasks.waitForAngular();
        WebDriverTasks.wait(4);
        return WebDriverTasks.verifyIfElementIsDisplayed(By.xpath(String.format(columnInGrid, colName)), 5);
    }

    /**
     * This method will verify that advanced filter is filtering data for given tile
     * Created By : Venkat
     *
     * @param tile This is tile name for wich advanced filter is to be validated
     * @return Boolean If the advanced filter works as expected for given tile then it will return True else False
     */
    public static boolean verifyAdvancedFilter(String tile) {
        boolean isAsExpected = false;
        HashMap<String, String> tileAdvacedFilter = new HashMap<>();
        tileAdvacedFilter.put("HOME-STAT Clients", "Chronicity##Medium-term##CLIENT TYPE");
        tileAdvacedFilter.put("Current Location of Clients and Prospects", "ClientCategory##Caseload##CLIENT CATEGORY");
        tileAdvacedFilter.put("HOME-STAT Clients Placed in Month", "ProviderName##BronxWorks##PROVIDER");
//        tileAdvacedFilter.put("Housing Packet/Subsidy in Process","Verified Docs##Yes##VERIFIED DOCS");
        tileAdvacedFilter.put("Active Housing Packet/Subsidy", "Housing Search Status##Search for Housing##Housing Search Status");

        if (!tileAdvacedFilter.containsKey(tile))
            return true;

        System.out.println("Verifying advanced filter for " + tile);

        By detailsLocator = By.xpath(String.format(detailsLink, tile));
        WebDriverTasks.waitForElement(detailsLocator).click();

        String advancedFilterByCol = tileAdvacedFilter.get(tile).split("##")[0];
        String advancedFilterBy = tileAdvacedFilter.get(tile).split("##")[1];
        String filteredCol = tileAdvacedFilter.get(tile).split("##")[2];

        DailyOutreach.advancedFilterBy(advancedFilterByCol, advancedFilterBy, true);
        isAsExpected = verifyDataInColumn(filteredCol, advancedFilterBy);

        WebDriverTasks.scrollToTop();
        WebDriverTasks.waitForElement(homeStatOverviewLink).click();

        return isAsExpected;
    }

    /**
     * This method will verify expected data against provided column name
     * Created By : Venkat
     *
     * @param columnName   This is target column name
     * @param expectedData This is expected data that should be displayed in each cell for given column
     * @return boolean If all cell values contains expected data then it will return True else false
     */
    public static boolean verifyDataInColumn(String columnName, String expectedData) {
        boolean isValid = true;
        boolean hasNext;
        int loopLimiter = 3;
        WebDriverTasks.waitForAngular();
        int colNoForClientCategory = ViewReports.getColumnNo(columnName) + 1;
        String clientCategoryCells = String.format("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(%d)", (colNoForClientCategory - 1));
        By dataCells = By.cssSelector(clientCategoryCells);
        WebDriverTasks.wait(3);
        do {
            if (WebDriverTasks.verifyIfElementIsDisplayed(dataCells, 5)) {
                WebDriverTasks.waitForElement(dataCells);
                for (WebElement cell : WebDriverTasks.driver.findElements(dataCells)) {
                    if (!cell.getText().trim().contains(expectedData)) {
                        isValid = false;
                        break;
                    }
                }
            }
            hasNext = WebDriverTasks.driver.findElement(nextDatGridPage).isEnabled();
            if (hasNext & isValid)
                WebDriverTasks.waitForElement(nextDatGridPage).click();
            loopLimiter--;
        } while (hasNext & isValid & loopLimiter>0);
        if (WebDriverTasks.driver.findElement(firstDatGridPage).isEnabled())
            WebDriverTasks.waitForElement(firstDatGridPage).click();
        return isValid;
    }

    /**
     * This method will remove all applied advanced filters
     * <p>
     * Created By : Venkat
     */
    public static void removeAdvancedFilters() {
        if (!WebDriverTasks.verifyIfElementIsDisplayed(removeFilterButton, 3))
            return;
        for (WebElement close : WebDriverTasks.waitForElements(removeFilterButton)) {
            close.click();
        }
    }

    /**
     * This method will verify below 5 validations for each tile
     * 1.Verify Counts match total count
     * 2.Verify counts match total count on view details page
     * 3.Verify total items on grid matches counts
     * 4.On the Grid - Disable and enable column using column picker
     * 5.Verify advanced filter can be applied
     * <p>
     * Created By : Venkat
     */
    public static void verifyEachTile() {
        LinkedList<String> tiles = getAllTiles();
        for (String tile : tiles) {
            System.out.println("*************** Validation started for " + tile + " ***************");
            Assert.assertTrue(verifyTotalTileCount(tile), "Total count does not match with it's child count for tile '" + tile + "'");
            Assert.assertTrue(verifyTileCountMatchesWithDetails(tile), "Record count in tile does not match with count in details page for tile '" + tile + "'");
            Assert.assertTrue(verifyGridCount(tile), "Record count in tile does not match with count in grid for tile '" + tile + "'");
            Assert.assertTrue(verifyColumnPickerFunctionality(tile), "Column picker functionality not working in data in grid for tile '" + tile + "'");
            Assert.assertTrue(verifyAdvancedFilter(tile), "Advanced filter functionality is not working as expected for tile '" + tile + "'");
            System.out.println("*************** Validation completed for " + tile + " ***************");
        }
    }

    /**
     * This method will select Year and month combination for Year and month on Monthly Dashboard page
     * Created By : Venkat
     *
     * @param yearAndMonth This is year and month to be selected
     */
    public static void selectYearAndMonth(String yearAndMonth){
        WebDriverTasks.waitForElementAndSelect(selectYearAndMonth).selectByVisibleText(yearAndMonth);
    }

}
