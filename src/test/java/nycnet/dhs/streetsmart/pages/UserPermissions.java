package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class UserPermissions {
	static By usersPageHeader = By.xpath("//h1[@class='page-header'][contains(text(), 'Users')]");
	
	 public static String returnUsersPageHeader() {
		  String usersPageHeaderText = WebDriverTasks.waitForElement(usersPageHeader).getText();
		  System.out.println("The page header is " + usersPageHeaderText);
		  return usersPageHeaderText;
	  }

}
