package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;



public class ViewClientInformationfromSearch {
	 
	 static By clientCategory = By.xpath("//*[@ng-bind='Client.ClientEnrollment.EnrollmentReasonName']");
	 
	 public static String returnClientCategory() {
		  String clientCategoryText =  WebDriverTasks.waitForElement(clientCategory).getText();
		  System.out.println("The client category is " + clientCategoryText);
		  return clientCategoryText;
	  }
	 
	}
	 
