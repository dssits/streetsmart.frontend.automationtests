package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class Webinars {
	static By webinarsPageHeader  = By.xpath("//h1[@class='page-header '][contains(text(), 'Webinars')]");
	
	 public static String returnWebinarsPageHeader() {
		  String webinarsPageHeaderText = WebDriverTasks.waitForElement(webinarsPageHeader).getText();
		  System.out.println("The page header is " + webinarsPageHeaderText);
		  return webinarsPageHeaderText;
	  }

}
