package nycnet.dhs.streetsmart.pages.clientdetails;

import nycnet.dhs.streetsmart.pages.Base;
import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class HousingSearchInformationTab extends StartWebDriver {

	static By clickHousingInformationTab = By.xpath("//a[@id='housingSearchtab']");
	static By clickOnClientDetailsButton = By.xpath("//button[contains(@class,'btn btn-sm btn-default m-r-10')]");
	static By returnClientDetailsTitle = By.xpath("//h3[contains(@class,'popover-title')]");
	static By clickOnAddNewButton = By.xpath("//*[@ng-click='AddHousingSearchButton()']");
	static By selectProvider = By.xpath("//*[@ng-model='HousingSearchInfo.ProviderId']");
	static By selectHousingSearchStatus = By.xpath("//*[@ng-model='HousingSearchInfo.HousingSearchStatusId']");
	static By clickOnSaveButton = By.xpath("(//button[@id='cint-info-save'])[6]");
	static By returnHousingSearchStatus = By.cssSelector("span[title='Non-Compliant']");

	static By selectHousingSearchSubStatus = By.xpath("//*[@ng-model='HousingSearchInfo.HousingSearchSubStatusId']");
	static By expectedExitDate = By.xpath("//*[@ng-model='HousingSearchInfo.ExpectedExitDate']");
	static By minApptReq = By.xpath("//*[@ng-model='HousingSearchInfo.MinApartmentRequirementId']");
	static By openBoroughPrefList = By.xpath("//label[normalize-space()='Borough Preference:']/following-sibling::div//*[@title='None Selected']");
	static By openVoucherInHandList = By.xpath("//label[normalize-space()='Voucher in-hand:']/following-sibling::div//*[@title='None Selected']");
	static By openAptDisabilityAccessList = By.xpath("//label[normalize-space()='Apt. Disability Access:']/following-sibling::div//*[@title='None Selected']");
	static String option_checkbox = "//label[normalize-space()='%s']/input";

	static By addApartmentButton = By.xpath("//*[@ng-click='AddNewApartmentsViewed()']");

	static By apt_table_save = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:first-child>i:first-child");
	static By apt_table_saveUpdatedData = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:first-child>i[title='Save']");
	static By apt_table_cancel =  By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:first-child>i:last-child");
	static By apt_table_edit =  By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:first-child>i:first-child");
	static By apt_table_aptSize = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(2)>select");
	static By apt_table_aptSource = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(3)>input");
	static By apt_table_streetNo = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(4)>input");
	static By apt_table_streetName= By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(5)>input");
	static By apt_table_zip = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(9)>input");
	static By apt_table_state = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(6)>select");
	static By apt_table_county = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(7)>select");
	static By apt_table_city = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(8)>select");
	static By apt_table_dateOfViewing = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(10)>div>input");
	static By apt_table_ll = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(11)>select");
	static By apt_table_clientOutcome = By.cssSelector("div.table-responsive>table[style]>tbody>tr:not([class*=ng-hide])>td:nth-child(12)>select");

	static By apt_table_streetName_dd = By.xpath("//input[@ng-model='item.StreetAddress']");

	static By housingSearchRecords = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']");

	static By actionButton = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:first-child>div>a");

	static By deleteButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='Delete']");
	static By confirmDeleteButton = By.xpath("//button[text()='Yes, delete it!']");
	static By closeRecordDeletionConfirmationMessage = By.xpath("//button[text()='Ok']");

	static String tableTextLocator = "div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(%d)>div>span:nth-child(%d)";

	static By viewCaseInformationButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='View Details']");
	static String fieldValues = "//div[not(contains(@class,'ng-hide'))]/form//div[@class='background-view']//label[normalize-space()='%s:']/following-sibling::div";

	static By tableHeaders = By.xpath("//table[contains(@style,'border: none')]/thead/tr/th");
	static By editButton = By.xpath("//button[@id='cint-info-edit']");
	static By cancelButton = By.xpath("//button[@id='cint-info-cancel' and @style and not(contains(@class,'ng-hide'))]");

	static By t_log_actiontaken = By.cssSelector("div[id='CleanUpDataGrid']>div.ui-grid-contents-wrapper>div>div.ui-grid-viewport>div>div.ui-grid-row>div[role='row']>div:nth-child(1)>div>span:nth-child(4)");

	public static void clickOnHousingInformationTab() {
		WebDriverTasks.waitForElement(clickHousingInformationTab).click();
	}
	
	public static void clickClientDetailsButton() {
		WebDriverTasks.waitForElement(clickOnClientDetailsButton).click();
	}

	public static String returnClientDetailsTitle() {
		String clientDetailsTitleText1 = WebDriverTasks.waitForElement(returnClientDetailsTitle).getText();
		System.out.println(clientDetailsTitleText1 + " accessed on Housing Search Information Tab");
		return clientDetailsTitleText1;
	}
	public static void clickOnAddNewButton () {
		WebDriverTasks.waitForElement(clickOnAddNewButton).click();
	}
	
	public static void selectProvider(String provider) {
		WebDriverTasks.waitForElementAndSelect(selectProvider).selectByVisibleText(provider);
	}
	
	public static void selectHousingSearchStatus (String housingsearchstatus) {
		WebDriverTasks.waitForElementAndSelect(selectHousingSearchStatus).selectByVisibleText(housingsearchstatus);
	}
	
	public static void clickOnSaveButton () {
		WebDriverTasks.waitForElement(clickOnSaveButton).click();
	}	
	
	public static String returnHousingSearchStatus() {
		String housingSearchStatusText = WebDriverTasks.waitForElement(returnHousingSearchStatus).getText();
		System.out.println("Record with Housing Search Status as " +housingSearchStatusText+ " created");
		return housingSearchStatusText;
	}

	public static void selectHousingSearchSubStatus(String substatus){
		WebDriverTasks.waitForElementAndSelect(selectHousingSearchSubStatus).selectByVisibleText(substatus);
	}

	public static void selectExitDate(String date){
		Base.selectDate(expectedExitDate,date);
	}

	public static void selectMinimumApartment(String apartment){
		WebDriverTasks.waitForElementAndSelect(minApptReq).selectByVisibleText(apartment);
	}

	public static void selectBorough(String options){
		WebDriverTasks.waitForElement(openBoroughPrefList).click();
		String[] optionsArray = options.split(",");
		for(String option:optionsArray){
			By locator = By.xpath(String.format(option_checkbox,option));
			WebDriverTasks.waitForElement(locator).click();
		}
	}

	public static void selectVoucherInHand(String options){
		WebDriverTasks.waitForElement(openVoucherInHandList).click();
		String[] optionsArray = options.split(",");
		for(String option:optionsArray){
			By locator = By.xpath(String.format(option_checkbox,option));
			WebDriverTasks.waitForElement(locator).click();
		}
	}

	public static void selectAptDisabilityAccess(String options){
		WebDriverTasks.waitForElement(openAptDisabilityAccessList).click();
		String[] optionsArray = options.split(",");
		for(String option:optionsArray){
			By locator = By.xpath(String.format(option_checkbox,option));
			WebDriverTasks.waitForElement(locator).click();
		}
	}

	public static void clickOnAddNewAptButton(){
		WebDriverTasks.waitForElement(addApartmentButton).click();
	}

	public static void selectAptSizeInTable(String size){
		if(size.isEmpty()) return;
		WebDriverTasks.waitForElementAndSelect(apt_table_aptSize).selectByVisibleText(size);
	}

	public static void setAptSourceInTable(String source){
		if(source.isEmpty()) return;
		WebDriverTasks.waitForElement(apt_table_aptSource).sendKeys(source);
	}

	public static void setStreetNoInTable(String streetNo){
		if(streetNo.isEmpty()) return;
		WebDriverTasks.waitForElement(apt_table_streetNo).sendKeys(streetNo);
	}

	public static void setStreetNameInTable(String streetName){
		if(streetName.isEmpty()) return;
		WebDriverTasks.waitForElement(apt_table_streetName).sendKeys(streetName);
	}

	public static void selectStateInTable(String state){
		if(state.isEmpty()) return;
		WebDriverTasks.waitForElementAndSelect(apt_table_state).selectByVisibleText(state);
	}

	public static void selectCityInTable(String city){
		if(city.isEmpty()) return;
		WebDriverTasks.waitForElementAndSelect(apt_table_city).selectByVisibleText(city);
	}

	public static void selectCountyInTable(String county){
		if(county.isEmpty()) return;
		WebDriverTasks.waitForElementAndSelect(apt_table_county).selectByVisibleText(county);
	}

	public static void setZipInTable(String zip){
		if(zip.isEmpty()) return;
		WebDriverTasks.waitForElement(apt_table_zip).sendKeys(zip);
	}

	public static void selectDateOfViewingInTable(String dateOfViewing){
		if(dateOfViewing.isEmpty()) return;
		Base.selectDate(apt_table_dateOfViewing,dateOfViewing);
	}

	public static void selectLLOutcomeInTable(String llOutcome){
		if(llOutcome.isEmpty()) return;
		WebDriverTasks.waitForElementAndSelect(apt_table_ll).selectByVisibleText(llOutcome);
	}

	public static void selectClientOutcomeInTable(String clientOutcome){
		if(clientOutcome.isEmpty()) return;
		WebDriverTasks.waitForElementAndSelect(apt_table_clientOutcome).selectByVisibleText(clientOutcome);
	}

	public static void addNewApartmentDetail(HashMap<String,String> data){
		WebDriverTasks.waitForElement(addApartmentButton).click();
		selectAptSizeInTable(data.get("Apt. Size"));
		setAptSourceInTable(data.get("Apt. Source"));

		setStreetNoInTable(data.get("Street No."));

		setStreetNameInTable(data.get("Street Name"));
		selectStateInTable(data.get("State"));
		selectCountyInTable(data.get("County"));
		selectCityInTable(data.get("City"));

		setZipInTable(data.get("Zip Code"));
		selectDateOfViewingInTable(data.get("Date of Viewing"));

		selectLLOutcomeInTable(data.get("LL Outcome"));
		selectClientOutcomeInTable(data.get("Client Outcome"));

		WebDriverTasks.waitForElement(apt_table_save).click();
	}

	/**
	 * This method will get number of health information records
	 * Created By : Venkat
	 *
	 * @return Integer This is count of health information records
	 */
	public static int getNoOfRecords() {
		WebDriverTasks.waitForAngular();
		if (!WebDriverTasks.verifyIfElementIsDisplayed(housingSearchRecords, 5))
			return 0;
		return WebDriverTasks.waitForElements(housingSearchRecords).size();
	}


	/**
	 * This method will delete all Housing Search Information records
	 * Created By : Venkat
	 */
	public static void deleteAllHousingSearchRecords() {
		WebDriverTasks.waitForAngular();
		if (!WebDriverTasks.verifyIfElementIsDisplayed(actionButton, 3))
			return;
		List<WebElement> actionButtons = WebDriverTasks.waitForElements(actionButton);
		for (int i = actionButtons.size() - 1; i >= 0; i--) {
			deleteHousingSearchRecord(i);
		}
	}

	/**
	 * This method will delete Housing Search Information record for provided row number
	 * Created By : Venkat
	 *
	 * @param rowNo This is row number for which we want to delete, row number starts from 0
	 */
	public static void deleteHousingSearchRecord(int rowNo) {
		WebDriverTasks.waitForAngular();
		WebDriverTasks.scrollToTop();
		List<WebElement> actionButtons = WebDriverTasks.waitForElements(actionButton);
		assert rowNo <= actionButtons.size();
		actionButtons.get(rowNo).click();
		WebDriverTasks.wait(3);
		WebDriverTasks.waitForElement(deleteButton).click();
		WebDriverTasks.wait(5);
		WebDriverTasks.waitForElement(confirmDeleteButton).click();
		WebDriverTasks.wait(3);
		if (WebDriverTasks.verifyIfElementIsDisplayed(closeRecordDeletionConfirmationMessage, 5))
			WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
	}

	public static String getValueFromTable(String label){
		LinkedList<String> multiValuedFields = new LinkedList<>(Arrays.asList("Voucher in-hand", "Borough Preference", "Apt. Disability access"));

		HashMap<String,int[]> map = new HashMap<>();
		map.put("Provider",new int[]{2,1});
		map.put("Housing Search Status",new int[]{2,3});
		map.put("Housing Search Sub-status",new int[]{2,5});

		map.put("Minimum Apt. Requirement",new int[]{3,1});
//		map.put("Expected Exit Date",new int[]{3,3});
		map.put("Voucher in-hand",new int[]{3,5});

		map.put("Borough Preference",new int[]{4,1});
		map.put("Apt. Disability access",new int[]{4,3});

		map.put("Apt. Size",new int[]{6,1});
		map.put("Apt. Source",new int[]{6,3});
		map.put("Date of Viewing",new int[]{6,5});

		map.put("Client Outcome",new int[]{7,1});
		map.put("LL Outcome",new int[]{7,3});

		map.put("Street No.",new int[]{8,1});
		map.put("Street Name",new int[]{8,3});
		map.put("Zip Code",new int[]{8,5});

		map.put("State",new int[]{9,1});
		map.put("City",new int[]{9,3});
		map.put("County",new int[]{9,5});

		if(!map.containsKey(label)){
			return null;
		}

		int[] matrix = map.get(label);
		String xpath = String.format(tableTextLocator,matrix[0],matrix[1]);
		By element = By.cssSelector(xpath);
		String value = WebDriverTasks.waitForElement(element).getText().trim();
		if(multiValuedFields.contains(label)){
			value = value.replace(", ",",");
		}
		return value;
	}

	public static String verifyDataInTable(HashMap<String, String> data) {
		LinkedList<String> mismatchedData = new LinkedList<>();
		for(String key : data.keySet()){
			if(!data.get(key).equals(getValueFromTable(key)))
				mismatchedData.add(key);
		}
		return  String.join(",",mismatchedData);
	}

	/**
	 * This method will click on Action an then View Details button
	 * Created By : Venkat
	 *
	 * @param rowNo This is row number for which we want to view details, row number starts from 0
	 */
	public static void viewHousingSearchInformationRecord(int rowNo) {
		WebDriverTasks.scrollToTop();
		List<WebElement> actionButtons = WebDriverTasks.waitForElements(actionButton);
		assert rowNo <= actionButtons.size();
		WebDriverTasks.wait(3);
		actionButtons.get(rowNo).click();
		WebDriverTasks.wait(3);
		WebDriverTasks.waitForElement(viewCaseInformationButton).click();
		WebDriverTasks.wait(3);
	}

	public static String verifyHousingSearchStatusData(HashMap<String,String> data){
		By fieldValue;
		String actualValue,expectedValue;
		HashMap<String,String> colMapper = new HashMap<>();
		colMapper.put("Housing Search Sub Status","Housing Search Sub-status");
		colMapper.put("Min Apt. Requirement","Minimum Apt. Requirement");
		colMapper.put("Apt. Disability Access","Apt. Disability access");
		LinkedList<String> fields = new LinkedList<>(Arrays.asList("Provider","Housing Search Status","Housing Search Sub Status","Min Apt. Requirement","Borough Preference","Voucher in-hand","Apt. Disability Access"));
		LinkedList<String> mismatchedFields = new LinkedList<>();
		for(String field: fields){
			if(!data.containsKey(field)) {
				if(!colMapper.containsKey(field)) {
					mismatchedFields.add(field);
					continue;
				}
				expectedValue = data.get(colMapper.get(field));
			} else {
				expectedValue = data.get(field);
			}
			fieldValue = By.xpath(String.format(fieldValues,field));
			actualValue = WebDriverTasks.waitForElement(fieldValue).getText().trim();
			if(!expectedValue.equals(actualValue)){
				LinkedList<String> expectedList = new LinkedList<>(Arrays.asList(expectedValue.split(",")));
				LinkedList<String> actualList = new LinkedList<>(Arrays.asList(actualValue.split(",")));
				if(expectedList.size()==actualList.size()){
					expectedList.removeAll(actualList);
					if(expectedList.size()>0)
						mismatchedFields.add(field);
				} else {
					mismatchedFields.add(field);
				}
			}
		}
		return String.join(",",mismatchedFields);
	}

	public static String verifyApartmentDetails(HashMap<String,String> data) {
		HashMap<String,String> colMapper = new HashMap<>();
		colMapper.put("Street No","Street No.");

		String actualValue,expectedValue = null;
		LinkedList<String> fields = new LinkedList<>(Arrays.asList("Apt. Size","Apt. Source","Street No","Street Name","State","County","City","Zip Code","Date of Viewing","LL Outcome","Client Outcome","Modified By"));
		LinkedList<String> mismatchedFields = new LinkedList<>();
		for(String field: fields){
			if(!data.containsKey(field)) {
				if(colMapper.containsKey(field)) {
					expectedValue = data.get(colMapper.get(field));
				} else {
					mismatchedFields.add(field);
					continue;
				}
			} else {
				expectedValue = data.get(field);
			}
			actualValue = getCellData(field);
			if(!expectedValue.equals(actualValue)){
				mismatchedFields.add(field);
			}
		}
		return String.join(",",mismatchedFields);
	}


	public static String getCellData(String colName){
		int colNo = getColumnNo(colName);
		if(colNo==-1)
			return null;
		colNo++;
		String cssLocator = String.format("table.table-dhs>tbody>tr[ng-repeat='item in ApartmentViewedList']>td:nth-child(%d)",colNo);
		By element = By.cssSelector(cssLocator);
		return WebDriverTasks.waitForElement(element).getText().trim();
	}

	public static int getColumnNo(String colName){
		List<WebElement> cols = WebDriverTasks.waitForElements(tableHeaders);
		int i = 0;
		for(WebElement col : cols){
			if(col.getText().trim().equals(colName))
				return i;
			i++;
		}
		return -1;
	}

	public static void clickOnEditButton(){
		WebDriverTasks.waitForElement(editButton).click();
	}

	public static void clickOnCancelButton(){
		WebDriverTasks.waitForElement(cancelButton).click();
	}

	public static void clickOnEditAptDetails(){
		WebDriverTasks.waitForElement(apt_table_edit).click();
	}

	public static void clickOnSaveAptDetails(){
		WebDriverTasks.waitForElement(apt_table_saveUpdatedData).click();
	}

	/**
	 *
	 * @param streetName
	 */
	public static void updateStreetName(String streetName){
		WebDriverTasks.waitForElement(apt_table_streetName_dd).clear();
		WebDriverTasks.waitForElement(apt_table_streetName_dd).sendKeys(streetName);
	}

	/**
	 * This method will get Action Taken from log table
	 * Created By : Venkat
	 *
	 * @return This is Action Taken
	 */
	public static String getActionTakenFromLog() {
		WebDriverTasks.wait(3);
		return WebDriverTasks.waitForElement(t_log_actiontaken).getText();
	}

}