package nycnet.dhs.streetsmart.pages.clientdetails;

import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;

public class PlacementInformationTab {
    static By placementsTab = By.xpath("//a[contains(text(),'Placements') and contains(.,'Information')]");
    static By casesGridActionButton = By.cssSelector("div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:first-child>div>a");
    static By deleteCaseButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='Delete']");
    static By confirmEngagementDeleteButton = By.xpath("//button[text()='Yes, delete it!']");
    static By closeRecordDeletionConfirmationMessage = By.xpath("//button[text()='Ok']");
    static By addPlacement = By.xpath("//a[@ng-click='AddPlacementButton()']");
    static By placementType = By.xpath("//select[@ng-model='PlacementType']");
    static By placementDescription = By.xpath("//select[@ng-model='Placement.PlacementDescription']");
    static By caseNo = By.xpath("//select[@ng-model='Placement.CaseId']");
    static By placementStartDate = By.name("PlacementStartDate");
    static By placementEndDate = By.name("PlacementEndDate");
    static By buildingNo = By.xpath("//input[@ng-model='Placement.Address.StreetNumber']");
    static By streetAddress = By.xpath("//input[@ng-model='Placement.Address.StreetAddress']");
    static By addressCity = By.xpath("//input[@ng-model='Placement.Address.City']");
    static By placementKnownStatus = By.xpath("//select[@ng-model='Placement.IsKnown']");
    static By addressState = By.xpath("//input[@ng-model='Placement.Address.State']");
    static By addressZip = By.xpath("//input[@ng-model='Placement.Address.Zip']");
    static By addressCounty = By.xpath("//input[@ng-model='Placement.Address.County']");
    static By saveButton = By.xpath("//div[starts-with(@id,'default-tab-') and not(contains(@class,'ng-hide'))]//button[@id='cint-info-save' and not(contains(@class,'ng-hide'))]");
    static By editButton = By.xpath("//div[starts-with(@id,'default-tab-') and not(contains(@class,'ng-hide'))]//button[@id='cint-info-save' and not(contains(@class,'ng-hide'))]");
    static By cancelButton = By.xpath("//div[starts-with(@id,'default-tab-') and not(contains(@class,'ng-hide'))]//button[@id='cint-info-cancel' and not(contains(@class,'ng-hide'))]");

    static By modal_yes = By.cssSelector("div.modal-content>div.modal-footer>button[ng-click='ok()']");

    static By table_ssno = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(2)>div>span:last-child");
    static By table_placementtype = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(4)>div>span:first-child");
    static By table_placement_description = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(5)>div");
    static By table_placement_start_date = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(6)>div>span:first-child");
    static By table_length_of_stay = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(6)>div>span:last-child");
    static By table_modified_by = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(10)>div>span:first-child");

    static By actionButton = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:first-child>div>a");
    static By viewCaseInformationButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='View Details']");

    static String fieldValue = "//div[contains(@style,'display: block;') and @ng-show='placementTypeName']//label[normalize-space()='%s:']/following-sibling::div[@style]";

    /**
     * This method will delete placement record on provided row number.
     * Created By : Venkat
     *
     * @param rowNo This is row number which is to be deleted, Row number starts from 0.
     */
    public static void deletePlacementRecord(int rowNo) {
        WebDriverTasks.scrollToTop();
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(casesGridActionButton);
        assert rowNo <= actionButtons.size();
        actionButtons.get(rowNo).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(deleteCaseButton).click();
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(confirmEngagementDeleteButton).click();
        WebDriverTasks.wait(3);
        if (WebDriverTasks.verifyIfElementIsDisplayed(closeRecordDeletionConfirmationMessage, 5))
            WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
    }

    /**
     * This method will delete all placements records from table
     * Created By : Venkat
     */
    public static void deleteAllPlacementRecords() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(casesGridActionButton, 3))
            return;
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(casesGridActionButton);
        for (int i = actionButtons.size() - 1; i >= 0; i--) {
            deletePlacementRecord(i);
        }
    }

    /**
     * This method will click on 'Placements Information' tab
     * Created By : Venkat
     */
    public static void clickOnPlacementsTab() {
        WebDriverTasks.waitForElement(placementsTab).click();
    }

    /**
     * This method will click on 'Add New' button Placement Information page
     * Created By : Venkat
     */
    public static void clickOnAddNewPlacementTab() {
        WebDriverTasks.waitForElement(addPlacement).click();
    }

    /**
     * This method will select placement type
     * Created By : Venkat
     *
     * @param type This is placement type to be selected
     */
    public static void selectPlacementType(String type) {
        WebDriverTasks.waitForElementAndSelect(placementType).selectByVisibleText(type);
    }

    /**
     * This method will select placement description value
     * Created By : Venkat
     *
     * @param description This is description value to be selected
     */
    public static void selectPlacementDescription(String description) {
        WebDriverTasks.waitForElementAndSelect(placementDescription).selectByVisibleText(description);
    }

    /**
     * This method will select case value by index. Index starts from 0
     * Created By : Venkat
     *
     * @param indexNo This is index no
     */
    public static void selectCaseNo(int indexNo) {
        WebDriverTasks.waitForElementAndSelect(caseNo).selectByIndex(indexNo);
    }

    /**
     * This method will select case no if value passed is not empty
     * Created By : Venkat
     *
     * @param caseID This is case id to be selected
     */
    public static void selectCaseNo(String caseID) {
        if (caseID.isEmpty()) return;
        WebDriverTasks.waitForElementAndSelect(caseNo).selectByVisibleText(caseID);
    }

    /**
     * This method will set start date if value passed is not empty
     * Created By : Venkat
     *
     * @param startDate This is start date to be provided
     */
    public static void selectStartDate(String startDate) {
        if (startDate.isEmpty()) return;
        Base.selectDate(placementStartDate, startDate);
    }

    /**
     * This method will set end date if value passed is not empty
     * Created By : Venkat
     *
     * @param endDate This is end date to be provided
     */
    public static void selectEndDate(String endDate) {
        if (endDate.isEmpty()) return;
        Base.selectDate(placementEndDate, endDate);
    }

    /**
     * This method will set Building No if value passed is not empty
     * Created By : Venkat
     *
     * @param buildingNumber This is building number to be provided
     */
    public static void setBuildingNo(String buildingNumber) {
        if (buildingNumber.isEmpty()) return;
        WebDriverTasks.waitForElement(buildingNo).clear();
        WebDriverTasks.waitForElement(buildingNo).sendKeys(buildingNumber);
    }

    /**
     * This method will set Street name if value passed is not empty
     * Created By : Venkat
     *
     * @param street This is street name to be provided
     */
    public static void setStreetName(String street) {
        if (street.isEmpty()) return;
        WebDriverTasks.waitForElement(streetAddress).clear();
        WebDriverTasks.waitForElement(streetAddress).sendKeys(street);
    }

    /**
     * This method will set city if value passed is not empty
     * Created By : Venkat
     *
     * @param city This is city value to be provided
     */
    public static void setCity(String city) {
        if (city.isEmpty()) return;
        WebDriverTasks.waitForElement(addressCity).clear();
        WebDriverTasks.waitForElement(addressCity).sendKeys(city);
    }

    /**
     * This method will select permanent placement dropdown if value provided not empty
     * Created By : Venkat
     *
     * @param placementKnowledge This is value to be selected. It can be "Known" or "Unknown"
     */
    public static void selectPermanentPlacementKnowledge(String placementKnowledge) {
        if (placementKnowledge.isEmpty()) return;
        WebDriverTasks.waitForElementAndSelect(placementKnownStatus).selectByVisibleText(placementKnowledge);
    }

    /**
     * This method will set state if value passed is not empty
     * Created By : Venkat
     *
     * @param state This is state value of 2 chars
     */
    public static void setState(String state) {
        if (state.isEmpty()) return;
        WebDriverTasks.waitForElement(addressState).clear();
        WebDriverTasks.waitForElement(addressState).sendKeys(state);
    }

    /**
     * This method will set ZIP code if value passed is not empty
     * Created By : Venkat
     *
     * @param zip This is zip code of length 5
     */
    public static void setZip(String zip) {
        if (zip.isEmpty()) return;
        WebDriverTasks.waitForElement(addressZip).clear();
        WebDriverTasks.waitForElement(addressZip).sendKeys(zip);
    }

    /**
     * This method will set county value if value passed is not empty
     * Created By : Venkat
     *
     * @param county This is county value
     */
    public static void setCounty(String county) {
        if (county.isEmpty()) return;
        WebDriverTasks.waitForElement(addressCounty).clear();
        WebDriverTasks.waitForElement(addressCounty).sendKeys(county);
    }

    /**
     * This method will fill placement details for provided keys.
     * It includes Description,CaseIndex,StartDate,EndDate,PermanentPlacementKnowledge,BuildingNo,
     * StreetName,City,State, County
     * Created By : Venkat
     *
     * @param data This is hashmap and it contains 11 key maps
     */
    public static void fillPlacementFormDetails(HashMap<String, String> data) {
        selectPlacementDescription(data.get("Description"));
        selectCaseNo(Integer.parseInt(data.get("CaseIndex")));
        selectStartDate(data.get("StartDate"));
        selectEndDate(data.get("EndDate"));
        selectPermanentPlacementKnowledge(data.get("PermanentPlacementKnowledge"));
        if (data.get("PermanentPlacementKnowledge").equals("Known")) {
            setBuildingNo(data.get("BuildingNo"));
            setStreetName(data.get("StreetName"));
            setCity(data.get("City"));
            setState(data.get("State"));
            setZip(data.get("Zip"));
            setCounty(data.get("County"));
        }
    }

    /**
     * This method will click on 'Save' button on Placement Information form
     * Created By : Venkat
     */
    public static void clickOnSaveButton() {
        WebDriverTasks.waitForElement(saveButton).click();
    }

    /**
     * This method will click on 'Cancel' button on Placement Information form
     * Created By : Venkat
     */
    public static void clickOnCancelButton() {
        WebDriverTasks.waitForElement(cancelButton).click();
    }

    /**
     * This method will click On Yes button on the popup
     * Created By : Venkat
     */
    public static void clickYesOnPopup() {
        WebDriverTasks.waitForElement(modal_yes).click();
    }

    /**
     * This method will get Case No from Placement Information table
     * Created By : Venkat
     *
     * @return String This is case no
     */
    public static String getCaseNoFromTable() {
        return WebDriverTasks.waitForElement(table_ssno).getText();
    }

    /**
     * This method will get Placement Type from Placement Information table
     * Created By : Venkat
     *
     * @return String This is placement type value
     */
    public static String getPlacementTypeFromTable() {
        return WebDriverTasks.waitForElement(table_placementtype).getText();
    }

    /**
     * This method will get Placement Description from Placement Information table
     * Created By : Venkat
     *
     * @return String This is placement description value
     */
    public static String getPlacementDescriptionFromTable() {
        return WebDriverTasks.waitForElement(table_placement_description).getText();
    }

    /**
     * This method will get Placement Start Date from Placement Information table
     * Created By : Venkat
     *
     * @return String This is start date
     */
    public static String getPlacementStartDateFromTable() {
        return WebDriverTasks.waitForElement(table_placement_start_date).getText();
    }

    /**
     * This method will get value for Length Of Stay from Placement Information table
     * Created By : Venkat
     *
     * @return String This is length of stay
     */
    public static String getLengthOfStayFromTable() {
        return WebDriverTasks.waitForElement(table_length_of_stay).getText();
    }

    /**
     * This method will get Modified By value from Placement Information table
     * Created By : Venkat
     *
     * @return String This is user name who has modified the record
     */
    public static String getModifiedByFromTable() {
        return WebDriverTasks.waitForElement(table_modified_by).getText();
    }

    /**
     * This method will click on Action an then View Details button
     * Created By : Venkat
     *
     * @param rowNo This is row number for which we want to view details, row number starts from 0
     */
    public static void viewRecord(int rowNo) {
        WebDriverTasks.scrollToTop();
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(actionButton);
        assert rowNo <= actionButtons.size();
        WebDriverTasks.wait(3);
        actionButtons.get(rowNo).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(viewCaseInformationButton).click();
        WebDriverTasks.wait(3);
    }

    /**
     * This method will get saved value from placement information form for provided label/field name
     * Created By : Venkat
     *
     * @param label This is label name/field name for which value is to be selected
     * @return String This is value for given field
     */
    public static String getTextForField(String label) {
        By element = By.xpath(String.format(fieldValue, label));
        return WebDriverTasks.waitForElement(element).getText().trim();
    }

    /**
     * This method will click on Edit button on Placement Information page
     * Created By : Venkat
     */
    public static void clickOnEditButton() {
        WebDriverTasks.waitForElement(editButton).click();
    }
}
