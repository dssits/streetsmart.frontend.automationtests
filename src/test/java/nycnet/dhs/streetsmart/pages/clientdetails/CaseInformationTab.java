package nycnet.dhs.streetsmart.pages.clientdetails;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.pages.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.testng.Assert;

import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public class CaseInformationTab extends StartWebDriver {

    static By actionButton = By.xpath("//a[@class='btn btn-default btn-xs dropdown-toggle action-header'][contains(text(),'Action')]");
    static By deleteButton = By.xpath("(//a[contains(text(),'Delete')]/*[@class='fa fa-trash'])[2]");
    static By confirmButton = By.cssSelector("button.confirm");
    static By makeProspect = By.id("make-prospect");
    static By nextButton = By.xpath("//button[contains(text(),'Next')]");
    static By caseInformationTab = By.xpath("//*[@id='casetab']");
    static By caseStartDateField = By.xpath("(//*[@ng-model='Case.CaseStartDate'])[2]");
    static By caseEndDateField = By.xpath("//div[contains(@ng-show,'!HandleSingleCaseDelete')]//input[@id='caseEndDate']");
    static By startDateInputBox = By.xpath("//div[@style='display: block;' and @class]//input[@title = 'Case Start Date']");
    static By selectStartDate = By.linkText("1");
    static By caseProviderDropDown = By.xpath("//div[@style='display: block;']//*[@ng-model='Case.Provider']");
    static By caseBoroughDropDown = By.xpath("//div[@style='display: block;']//*[@ng-model='Case.Borough']");
    static By caseInformationSaveButton = By.xpath("//div[not(contains(@class,'ng-hide'))]/div/button[text()='Save']");
    static By serviceType = By.id("ddlServiceType");
    static By caseType = By.xpath("//div[@style='display: block;']//select[@ng-model='Case.CaseType']");
    static By addNewButton = By.xpath("//div[contains(@class,'viewCurrentCaseinformion')]//div[@id='data-table_wrapper']//a[contains(.,'Add New')]");
    static By casesRows = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div>div>div>div>div>div.ui-grid-row");
    static By notesSection = By.xpath("//a[contains(.,'Notes')]");
    static By addNewNote = By.xpath("//div[@id='notes_collapsable_table']//a[contains(.,'Add New')]");
    static By notesTextBox = By.xpath("//div[@id='notes_collapsable_table']//textarea[@ng-model='CurrentClientNote.Notes']");
    static By editNotesTextBox = By.xpath("//div[@id='notes_collapsable_table']//textarea[@ng-model='item.Notes']");
    static By saveNotesButton = By.xpath("//div[@id='notes_collapsable_table']//div[not(contains(@class,'ng-hide'))]/table//tr[not(contains(@class,'ng-hide'))]//i[@title='Save']");

    static By selectType = By.xpath("//div[@ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']//select[@ng-change='CaseServiceTypeDDChange()']");

    static By caseEndReason = By.xpath("//div[contains(@ng-show,'!HandleSingleCaseDelete')]//select[@ng-disabled='!Case.CaseEndDate']");

    static By t_ssCaseNo = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div:first-child>div[role='row']>div:nth-child(2)>div>span:last-child");
    static By t_caseStartDate = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div:first-child>div[role='row']>div:nth-child(3)>div>span:first-child");
    static By t_serviceType = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div:first-child>div[role='row']>div:nth-child(4)>div>span:first-child");
    static By t_caseStatus = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div:first-child>div[role='row']>div:nth-child(4)>div>span:nth-child(3)");
    static By t_caseStatuses = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(4)>div>span:nth-child(3)");
    static By t_caseType = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div:first-child>div[role='row']>div:nth-child(5)>div>span:first-child");
    static By t_provider = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div:first-child>div[role='row']>div:nth-child(6)>div>span:first-child");
    static By t_borough = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div:first-child>div[role='row']>div:nth-child(6)>div>span:last-child");
    static By t_enrolmentStatus = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div:first-child>div[role='row']>div:nth-child(8)>div>div>span:first-child");
    static By t_modifiedBy = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div:first-child>div[role='row']>div:nth-child(9)>div>span:first-child");
    static By t_modifiedDate = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div:first-child>div[role='row']>div:nth-child(9)>div>span:last-child");

    static By casesGridActionButton = By.cssSelector("div[ng-show='!HandleSingleCaseDelete || !HandleSingleCaseDelete.deleteCaseId']>div>div>div>div>div>div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:first-child>div>a");
    static By viewCaseInformationButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='View Details']");

    static By deleteCaseButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='Delete']");
    static By confirmEngagementDeleteButton = By.xpath("//button[text()='Yes, delete it!']");
    static By closeRecordDeletionConfirmationMessage = By.xpath("//button[text()='Ok']");

    static String savedCaseData = "//form[contains(@class,'ng-dirty')]//label[text()='%s: ']/following-sibling::div[not(contains(@class,'ng-hide'))]";

    static By savedNoted = By.cssSelector("div[id='notes_collapsable_table']>div>div>div.table-responsive>table>tbody>tr.ng-scope>td:nth-child(4)");

    static By notesRecords = By.cssSelector("div[id='notes_collapsable_table']>div>div>div.table-responsive>table>tbody>tr.ng-scope");

    static By deleteNote = By.cssSelector("div[id='notes_collapsable_table']>div>div>div.table-responsive>table>tbody>tr.ng-scope>td:first-child>i[title='Delete']");
    static By confirmDeleteNote = By.cssSelector("div.modal-footer >button[ng-click='ok()']");

    static By editNote = By.cssSelector("div[id='notes_collapsable_table']>div>div>div.table-responsive>table>tbody>tr.ng-scope>td:first-child>i[title='Edit']");

    static By editCaseDetails = By.xpath("//button[text()='Edit']");
    static By editPageCancelButton = By.xpath("//button[@id='cint-info-cancel' and contains(@ng-click,'CancelAddCase()')]");

    static By expandChangelog = By.xpath("//a[@ng-click='InitializeChangeLog();']");

    static By t_log_updatedby = By.cssSelector("div[id='CleanUpDataGrid']>div.ui-grid-contents-wrapper>div>div.ui-grid-viewport>div>div.ui-grid-row>div[role='row']>div:nth-child(1)>div>span:nth-child(3)");
    static By t_log_actiontaken = By.cssSelector("div[id='CleanUpDataGrid']>div.ui-grid-contents-wrapper>div>div.ui-grid-viewport>div>div.ui-grid-row>div[role='row']>div:nth-child(1)>div>span:nth-child(5)");
    static By t_log_servicetype = By.cssSelector("div[id='CleanUpDataGrid']>div.ui-grid-contents-wrapper>div>div.ui-grid-viewport>div>div.ui-grid-row>div[role='row']>div:nth-child(2)>div>span:nth-child(1)");
    static By t_log_provider = By.cssSelector("div[id='CleanUpDataGrid']>div.ui-grid-contents-wrapper>div>div.ui-grid-viewport>div>div.ui-grid-row>div[role='row']>div:nth-child(2)>div>span:nth-child(3)");
    static By t_log_borough = By.cssSelector("div[id='CleanUpDataGrid']>div.ui-grid-contents-wrapper>div>div.ui-grid-viewport>div>div.ui-grid-row>div[role='row']>div:nth-child(2)>div>span:nth-child(5)");
    static By t_log_casestartdate = By.cssSelector("div[id='CleanUpDataGrid']>div.ui-grid-contents-wrapper>div>div.ui-grid-viewport>div>div.ui-grid-row>div[role='row']>div:nth-child(4)>div>span:nth-child(1)");

    static By caseSearchTextBox = By.id("clientCasesSearchText");

	 /*
	 public static String returnClientListPageHeader() {
		  String clientListPageHeaderText =  WebDriverTasks.waitForElement(clientListPageHeader).getText();
		  System.out.println("The page header is " + clientListPageHeaderText);
		  return clientListPageHeaderText;
	  }
	  */

    public static String returnCaseInformationTabHeader() {
        String caseInformationTabHeaderText = WebDriverTasks.waitForElement(caseInformationTab).getText();
        System.out.println("Current tab is " + caseInformationTabHeaderText);
        return caseInformationTabHeaderText;
    }

    public static void clickOnActionButton() {
        WebDriverTasks.waitForElement(actionButton).click();
    }

    public static void clickOnDeleteButton() {
        WebDriverTasks.waitForElement(deleteButton).click();
    }

    public static void clickOnconfirmButton() {
        WebDriverTasks.waitForElement(confirmButton).click();
    }

    public static void clickOnmakeProspectOption() {
        //WebDriverTasks.waitForElement(makeProspect).click();
        driver.findElement(makeProspect).click();
    }

    public static void clickOnNextButton() {
        WebDriverTasks.waitForElement(nextButton).click();
    }

    public static void clickOncaseInformationTab() {
        WebDriverTasks.waitForElement(caseInformationTab).click();
    }

    public static void selectCaseProvider(String caseprovider) {
        //Select selectByCaseProvider = new Select (driver.findElement(caseProviderDropDown));
        //selectByCaseProvider.selectByVisibleText(caseprovider);
        WebDriverTasks.waitForElementAndSelect(caseProviderDropDown).selectByVisibleText(caseprovider);
    }


    public static void selectCaseBorough(String caseborough) {
        WebDriverTasks.wait(2);
        Select selectByCaseBorough = new Select(driver.findElement(caseBoroughDropDown));
        selectByCaseBorough.selectByVisibleText(caseborough);
    }

    public static void selectSaveCaseInformation() {
        WebDriverTasks.waitForElement(caseInformationSaveButton).click();
    }

    public static void selectCaseStartDateField() {
        WebDriverTasks.waitForElement(caseStartDateField).click();
    }

    public static void selectCaseStartDate() {
        WebDriverTasks.waitForElement(selectStartDate).click();
    }

    //

    /**
     * This method will get total case count in table
     * Created By : Venkat
     *
     * @return int This is count
     */
    public static int getCaseCount() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(casesRows, 5))
            return 0;
        return WebDriverTasks.waitForElements(casesRows).size();
    }

    /**
     * This method will select service type for new case
     * Created By : Venkat
     *
     * @param type This is service type
     */
    public static void selectServiceType(String type) {
        WebDriverTasks.waitForElementAndSelect(serviceType).selectByVisibleText(type);
    }

    /**
     * This method will select case type for new case
     * Created By : Venkat
     *
     * @param casetype This is case type
     */
    public static void selectCaseType(String casetype) {
        WebDriverTasks.waitForElementAndSelect(caseType).selectByVisibleText(casetype);
    }

    /**
     * This method will select start date
     * Created By : Venkat
     *
     * @param startDate This is start dare to be selected
     */
    public static void selectCaseStartDate(String startDate) {
        Base.selectDate(startDateInputBox, startDate);
    }

    /**
     * This method will click on Add new button on Case Information tab
     * Created By : Venkat
     */
    public static void clickOnAddNewButton() {
        WebDriverTasks.waitForElement(addNewButton).click();
    }

    /**
     * This method will delete all Cases
     * Created By : Venkat
     */
    public static void deleteAllCases() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(casesGridActionButton, 3))
            return;
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(casesGridActionButton);
        for (int i = actionButtons.size() - 2; i >= 0; i--) {
            deleteCaseRecord(i);
        }
    }

    /**
     * This method will delete Case record for provided row number
     * Created By : Venkat
     *
     * @param rowNo This is row number for which we want to delete, row number starts from 0
     */
    public static void deleteCaseRecord(int rowNo) {
        WebDriverTasks.scrollToTop();
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(casesGridActionButton);
        assert rowNo <= actionButtons.size();
        actionButtons.get(rowNo).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(deleteCaseButton).click();
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(confirmEngagementDeleteButton).click();
        WebDriverTasks.wait(3);
        if (WebDriverTasks.verifyIfElementIsDisplayed(closeRecordDeletionConfirmationMessage, 5))
            WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
    }


    /**
     * This method will click on Action an then View Details button
     * Created By : Venkat
     *
     * @param rowNo This is row number for which we want to view details, row number starts from 0
     */
    public static void viewCaseInformationRecord(int rowNo) {
        WebDriverTasks.scrollToTop();
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(casesGridActionButton);
        assert rowNo <= actionButtons.size();
        WebDriverTasks.wait(3);
        actionButtons.get(rowNo).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(viewCaseInformationButton).click();
        WebDriverTasks.wait(3);
    }

    /**
     * This method will click on Notes section and expand it
     * Created By : Venkat
     */
    public static void clickOnNotesSection() {
        WebDriverTasks.waitForElement(notesSection).click();
    }

    /**
     * This method will click on Add New note in Notes section
     * Created By : Venkat
     *
     * @param note
     */
    public static void addNewNote(String note) {
        WebDriverTasks.waitForElement(addNewNote).click();
        WebDriverTasks.waitForElement(notesTextBox).sendKeys(note);
        WebDriverTasks.waitForElement(saveNotesButton).click();
        //Verify that success message is displayed after new record is added
        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");
    }

    /**
     * This method will get case no from case table for first row
     * Created By : Venkat
     *
     * @return String This is case no
     */
    public static String getCaseNo() {
        return WebDriverTasks.waitForElement(t_ssCaseNo).getText();
    }

    /**
     * This method will get start date from case table for first row
     * Created By : Venkat
     *
     * @return String This is case start date
     */
    public static String getCaseStartDate() {
        return WebDriverTasks.waitForElement(t_caseStartDate).getText();
    }

    /**
     * This method will get service type from case table for first row
     * Created By : Venkat
     *
     * @return String This is service type
     */
    public static String getServiceType() {
        return WebDriverTasks.waitForElement(t_serviceType).getText();
    }

    /**
     * This method will get case status from case table for first row
     * Created By : Venkat
     *
     * @return This will  be case status
     */
    public static String getCaseStatus() {
        return WebDriverTasks.waitForElement(t_caseStatus).getText();
    }

    /**
     * This method will get case type from case table for first row
     *
     * @return This will  be case type
     */
    public static String getCaseType() {
        return WebDriverTasks.waitForElement(t_caseType).getText();
    }

    /**
     * This method will get case provider from case table for first row
     * Created By : Venkat
     *
     * @return This will  be case provider
     */
    public static String getProvider() {
        return WebDriverTasks.waitForElement(t_provider).getText();
    }

    /**
     * This method will get borough from case table for first row
     * Created By : Venkat
     *
     * @return This will  be borough
     */
    public static String getBorough() {
        return WebDriverTasks.waitForElement(t_borough).getText();
    }

    /**
     * This method will get  modified by from case table for first row
     * Created By : Venkat
     *
     * @return This will be modified by
     */
    public static String getModifiedBy() {
        return WebDriverTasks.waitForElement(t_modifiedBy).getText();
    }

    /**
     * This method wil verify case information with those on case details page
     * Created By : Venkat
     *
     * @param serviceType   This is expected service type
     * @param caseType      This is expected case type
     * @param caseStartDate This is expected case start date
     * @param caseStatus    This is expected case status
     * @param provider      This is expected provider
     * @param borough       This is expected brough
     * @return String This is list of fields for which actual values does not matched with provided expected values
     */
    public static String verifyCaseInformationData(String serviceType, String caseType, String caseStartDate, String caseStatus, String provider, String borough) {
        HashMap<String, String> map = new HashMap<>();
        LinkedList<String> unmatchedLabels = new LinkedList<>();
        if (!serviceType.isEmpty())
            map.put("Service Type", serviceType);
        if (!caseType.isEmpty())
            map.put("Case Type", caseType);
        if (!caseStartDate.isEmpty())
            map.put("Case Start Date", caseStartDate);
        if (!caseStatus.isEmpty())
            map.put("Case Status", caseStatus);
        if (!provider.isEmpty())
            map.put("Provider", provider);
        if (!borough.isEmpty())
            map.put("Borough", borough);

        for (String label : map.keySet()) {
            if (!getSavedDataFor(label).equals(map.get(label)))
                unmatchedLabels.add(label);
        }
        return String.join(",", unmatchedLabels);
    }

    /**
     * This method will get data for provided field
     * Created By : Venkat
     *
     * @param label This is field label
     * @return String This is field data
     */
    public static String getSavedDataFor(String label) {
        String xpath = String.format(savedCaseData, label);
        By labelElement = By.xpath(xpath);
        return WebDriverTasks.waitForElement(labelElement).getText();
    }

    /**
     * This is method will compare actual not and expected note
     * Created By : Venkat
     *
     * @param note This is expected note
     * @return boolean If expected note matches with actual note it will return True else False
     */
    public static boolean isNoteAsExpected(String note) {
        String actualNote = WebDriverTasks.waitForElement(savedNoted).getText().trim();
        return note.equals(actualNote);
    }

    /**
     * This method will eidt case details for provided non-empty values
     * Created By : Venkat
     *
     * @param serviceType   This is new service type
     * @param caseType      This is new case type
     * @param caseStartDate This is new case start date
     * @param caseStatus    This is new case status
     * @param provider      This is new provider
     * @param borough       This is new borough
     */
    public static void editCaseDetails(String serviceType, String caseType, String caseStartDate, String caseStatus, String provider, String borough) {
        WebDriverTasks.waitForElement(editCaseDetails).click();

        if (!serviceType.isEmpty())
            selectServiceType(serviceType);
        if (!caseType.isEmpty())
            selectCaseType(caseType);
        if (!caseStartDate.isEmpty())
            selectCaseStartDate(caseStartDate);
        if (!provider.isEmpty())
            selectCaseProvider(provider);
        if (!borough.isEmpty())
            selectCaseBorough(borough);

        selectSaveCaseInformation();
    }

    /**
     * This method will get notes count
     * Created By : Venkat
     *
     * @return int This is note count
     */
    public static int getNotesCount() {
        return WebDriverTasks.waitForElements(notesRecords).size();
    }

    /**
     * This method will delete note, with index no
     * Created By : Venkat
     *
     * @param indexNo This is index no, it starts from 0.
     */
    public static void deleteNote(int indexNo) {
        WebDriverTasks.waitForElements(deleteNote).get(indexNo).click();
        WebDriverTasks.waitForElement(confirmDeleteNote).click();
        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Note has been deleted successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");
    }

    /**
     * This method will edit first note
     * Created By : Venkat
     *
     * @param newNote This is new note
     */
    public static void editFirstNote(String newNote) {
        WebDriverTasks.waitForElements(editNote).get(0).click();
        WebDriverTasks.waitForElement(editNotesTextBox).clear();
        WebDriverTasks.waitForElement(editNotesTextBox).sendKeys(newNote);
        WebDriverTasks.waitForElement(saveNotesButton).click();
        //Verify that success message is displayed after new record is added
        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");
    }

    /**
     * This method will click on cancel button on edit case page
     * Created By : Venkat
     */
    public static void clickOnCancelOnEditPage() {
        WebDriverTasks.waitForElement(editPageCancelButton).click();
    }

    /**
     * This method will click on change log pane to expand it
     * Created By : Venkat
     */
    public static void expandChangelog() {
        WebDriverTasks.waitForElement(expandChangelog).click();
    }

    /**
     * This method will get update by from log table
     *
     * @return This is updated by
     */
    public static String getUpdatedByFromLog() {
        return WebDriverTasks.waitForElement(t_log_updatedby).getText();
    }

    /**
     * This method will get Action Taken from log table
     * Created By : Venkat
     *
     * @return This is Action Taken
     */
    public static String getActionTakenFromLog() {
        WebDriverTasks.wait(3);
        return WebDriverTasks.waitForElement(t_log_actiontaken).getText();
    }

    /**
     * This method will get Service Type from log table
     * Created By : Venkat
     *
     * @return This is service Type
     */
    public static String getServiceTypeFromLog() {
        return WebDriverTasks.waitForElement(t_log_servicetype).getText();
    }

    /**
     * This method will get Service Provider from log table
     * Created By : Venkat
     *
     * @return This is service provider
     */
    public static String getProviderFromLog() {
        return WebDriverTasks.waitForElement(t_log_provider).getText();
    }

    /**
     * This method will get Borough from log table
     * Created By : Venkat
     *
     * @return This is borough
     */
    public static String getBoroughFromLog() {
        return WebDriverTasks.waitForElement(t_log_borough).getText();
    }

    /**
     * This method will get Start Date from log table
     * Created By : Venkat
     *
     * @return This is borostart Date
     */
    public static String getStartDateFromLog() {
        return WebDriverTasks.waitForElement(t_log_casestartdate).getText();
    }

    /**
     * This method will search for provided value
     * Created By : Venkat
     *
     * @param searchFor This is value to be searched
     */
    public static void searchFor(String searchFor) {
        WebDriverTasks.waitForElement(caseSearchTextBox).clear();
        WebDriverTasks.waitForElement(caseSearchTextBox).sendKeys(searchFor);
        WebDriverTasks.waitForElement(caseSearchTextBox).sendKeys(Keys.ENTER);
    }

    /**
     * This method will verify if provided case no is displayed on page or not
     * Created By : Venkat
     *
     * @param caseNo This is case no
     * @return boolean If case no is displayed then it will return True else False
     */
    public static boolean isCaseNoDisplayedAfterSearch(String caseNo) {
        String stdXpath = "//span[@title='%s']";
        By locator = By.xpath(String.format(stdXpath, caseNo));
        WebDriverTasks.waitForAngular();
        return WebDriverTasks.verifyIfElementIsDisplayed(locator, 10);
    }

    /**
     * This method will select Type on Case Information page
     * Created By : Venkat
     *
     * @param type This is type to be selected
     */
    public static void selectType(String type) {
        WebDriverTasks.waitForElementAndSelect(selectType).selectByVisibleText(type);
    }

    /**
     * This method will change status of open Cases to close
     * Created By : Venkat
     *
     * @throws ParseException
     */
    public static void changeCaseStatusToClose() throws ParseException {
        String dateFormat = "MM/dd/yyyy";
        String date = Helpers.getCurrentDate(dateFormat);
        date = Helpers.addDays(date, dateFormat, -1);
        List<WebElement> caseStatuses = WebDriverTasks.waitForElements(t_caseStatuses);
        for (int i = 0; i <= caseStatuses.size() - 1; i++) {
            caseStatuses = WebDriverTasks.waitForElements(t_caseStatuses);
            if (caseStatuses.get(i).getText().equals("Open")) {
                viewCaseInformationRecord(i);
                WebDriverTasks.waitForElement(editCaseDetails).click();
                selectCaseEndDate(date);
                selectCaseEndReason("Cannot be located");
                selectSaveCaseInformation();
            }
        }
    }

    /**
     * This method will select case end date
     * Created By : Venkat
     *
     * @param date This is end date
     */
    public static void selectCaseEndDate(String date) {
        Base.selectDate(caseEndDateField, date);
    }

    /**
     * This method will select case end reason
     * Created By : Venkat
     *
     * @param reason This is reason to be selected
     */
    public static void selectCaseEndReason(String reason) {
        WebDriverTasks.waitForElementAndSelect(caseEndReason).selectByVisibleText(reason);
    }
}