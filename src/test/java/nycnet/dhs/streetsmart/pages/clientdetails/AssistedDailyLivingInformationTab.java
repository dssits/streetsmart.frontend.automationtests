package nycnet.dhs.streetsmart.pages.clientdetails;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class AssistedDailyLivingInformationTab extends StartWebDriver {

	static By clickOnAssistedDailyLivingInformationTab = By.xpath("//ul[@class='nav nav-tabs']//li[9]//a[1]");
	static By clickAddNewButton = By.xpath("//a[@class='btn btn-sm btn-default pull-right addnew m-b-5']");
	static By selectProvider = By.xpath("//*[@ng-model='AssistedDailyLivingDetails.ProviderId']");
	static By selectRadioForGrooming = By.xpath("(//*[@ng-model='item.ClientActivitiesDailyLivingResponseId'])[5]");
	static By selectRadioForWalking = By.xpath("(//*[@ng-model='item.ClientActivitiesDailyLivingResponseId'])[14]");
	static By selectRadioForManagingMedications = By
			.xpath("(//*[@ng-model='item.ClientActivitiesDailyLivingResponseId'])[68]");
	static By inputOtherActivities = By.xpath("//td[contains(text(),'Other')]/input[@ng-model='AssistedDailyLivingDetails.ClientADLInfoList.OtherActivitiesDailyLivingType']");
	static By editButton = By.xpath("//*[@ng-click='EditADLDetails()']");
	static By clickOnSaveButton = By.xpath("(//*[@id='cint-info-save'])[6]");
	static By returnAssistedDailyLivingInformationProvider = By.cssSelector("span[title='BG']");
	static By actionButton = By.linkText("Action");
	static By viewDetails = By.linkText("View Details");
	static By deleteOption = By.linkText("Delete");
	static By confirmButton = By.xpath("//button[.='Yes, delete it!']");
	
	static By okConfirmButton = By.xpath("//button[.='Ok']");
	
	//Notes locators
	static By selectNotesCollapseIcon = By
			.xpath("//a[@id='notes-accordion']//i[@class='fa fa-plus-circle pull-right']");
	static By clickOnAddNewNotes = By
			.xpath("//a[@class='btn btn-sm btn-default addnew m-b-5'][contains(text(),'Add New')]");
	static By inputNotesText = By.xpath("//*[@ng-model='CurrentClientNote.Notes']");
	static By selectSaveNotes = By.xpath("//*[@ng-click='SaveClientNote(CurrentClientNote)']");
	//static By returnNotesText = By.xpath("//td[@class='pre-wrap-apply ng-binding']");
	static By returnNotesText = By.xpath("/html[1]/body[1]/div[2]/div[3]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/table[1]/tbody[1]/tr[3]/td[4]");
	static By firstRecordNoteTextOnNotesGrid = By.xpath("//tr[@pagination-id='clientNotes']/td[@class='pre-wrap-apply ng-binding']");
	//static By clickOnClientDetailsButton = By.xpath("//button[@class='btn btn-sm btn-default pull-right m-r-10 m-b-5 m-l-10']");
	static By clickOnDeleteNote = By.xpath("//i[@class='fa fa-trash cursor-pointer fa-lg notesIcon grid-action-icon']");
	static By clickOnDeleteConfirmation = By.xpath("//button[@ng-click='ok()']");
	
	static By clickOnClientDetailsButton = By.xpath("//*[@ng-click='addAssistedDailyLiving()']/following-sibling::button");
	static By returnClientDetailsTitle = By.xpath("//h3[@class='popover-title']");
	
	//Changelog locators
			static By changelogWakingText = By.cssSelector("div[role='grid']>div[role='rowgroup']>div[class='ui-grid-canvas']>div:nth-child(1)>div>div[role='gridcell']:nth-child(3)>div>span:nth-child(1)");
			static By expandChangeLogIcon = By.cssSelector("a.collapse-panel[id='change-log-accordion']");
			static By changeLogGridNoRecordsFound = By.xpath("(//*[@ng-show='changeLogGridList_GridOptions.data.length == 0'])[1]");

	public static void clickOnAssistedDailyLivingInformationTab() {
		WebDriverTasks.waitForElement(clickOnAssistedDailyLivingInformationTab).click();
	}

	public static void clickOnAddNewButton() {
		WebDriverTasks.waitForElement(clickAddNewButton).click();
	}

	public static void selectProvider(String provider) {
		WebDriverTasks.waitForElementAndSelect(selectProvider).selectByVisibleText(provider);
	}

	public static void clickOnRadioForGrooming() {
		WebDriverTasks.waitForElement(selectRadioForGrooming).click();
	}

	public static void clickOnRadioForWalking() {
		WebDriverTasks.waitForElement(selectRadioForWalking).click();
	}

	public static void clickOnRadioForManagingMedications() {
		WebDriverTasks.waitForElement(selectRadioForManagingMedications).click();
	}
	
	public static void ScrollToOtherActivityInput() {
		WebDriverTasks.scrollToElement(inputOtherActivities);
	}
	
	public static void inputOtherActivity(String query) {
		WebDriverTasks.waitForElement(inputOtherActivities).sendKeys(query);
	}
	
	public static void clearOtherActivityInput() {
		WebDriverTasks.waitForElement(inputOtherActivities).clear();
	}
	
	public static String returnOtherActivityInputEditAddPage() {
		return WebDriverTasks.waitForElement(inputOtherActivities).getAttribute("value");
	}
	
	public static void scrollToEditButton() {
		WebDriverTasks.scrollToElement(editButton);
	}
	
	public static void clickOnEditButton() {
		WebDriverTasks.waitForElement(editButton).click();
	}

	public static void clickOnSaveButton() {
		WebDriverTasks.waitForElement(clickOnSaveButton).click();
	}

	public static String returnProviderDetails() {
		String providerDetailTitleText = WebDriverTasks.waitForElement(returnAssistedDailyLivingInformationProvider)
				.getText();
		System.out.println("New record with" + providerDetailTitleText + " as a provider is created");
		return providerDetailTitleText;
	}
	
	public static void clickOnActionButton() {
		WebDriverTasks.waitForElement(actionButton).click();
	}
	
	public static void clickOnViewDetails() {
		WebDriverTasks.waitForElement(viewDetails).click();
	}
	
	public static void clickOnDeleteOption() {
		WebDriverTasks.waitForElement(deleteOption).click();
	}
	
	public static void clickOnConfirmButton() {
		WebDriverTasks.waitForElement(confirmButton).click();
	}
	
	public static void clickOnOKConfirmButton() {
		WebDriverTasks.waitForElement(okConfirmButton).click();
	}

	//Notes actions
	public static void selectNotesCollapseIcon() {
		WebDriverTasks.waitForElement(selectNotesCollapseIcon).click();
	}

	public static void clickOnAddNewNotesButton() {
		WebDriverTasks.waitForElement(clickOnAddNewNotes).click();
	}

	public static void inputNotesText(String notestext) {
		WebDriverTasks.waitForElement(inputNotesText).sendKeys(notestext);
	}

	public static void selectSaveNotes() {
		WebDriverTasks.waitForElement(selectSaveNotes).click();
	}

	public static String returnNotesTextDetails() {
		String notesDetailText = WebDriverTasks.waitForElement(firstRecordNoteTextOnNotesGrid).getText();
		System.out.println(notesDetailText + " was added");
		return notesDetailText;
	}
	
	public static void selectDeleteNoteButton() {
		WebDriverTasks.waitForElement(clickOnDeleteNote).click();

	}

	public static void selectDeleteNoteButtonConfirmation() {
		WebDriverTasks.waitForElement(clickOnDeleteConfirmation).click();
		System.out.println("Note Deleted");
	}

	public static void clickClientDetailsButton() {
		WebDriverTasks.waitForElement(clickOnClientDetailsButton).click();
	}

	public static String returnClientDetailsTitle() {
		String clientDetialsTitleText = WebDriverTasks.waitForElement(returnClientDetailsTitle).getText();
		System.out.println(clientDetialsTitleText + " accessed on Assisted Daily Living Information Tab");
		return clientDetialsTitleText;
	}
	
	//Changelog actions
	public static void clickOnChangeLogExpandIcon() {
		WebDriverTasks.scrollToElement(expandChangeLogIcon);
		WebDriverTasks.waitForElement(expandChangeLogIcon).click();
	}
	
public static void scrollToWalkingTextOnChangeLogGrid() {
		WebDriverTasks.scrollToElement(changelogWakingText);	
	}

	public static String returnWalkingTextOnChangeLogGrid() {
		
		String Text = WebDriverTasks.waitForElement(changelogWakingText).getText();
		System.out.println("The value on Grid is " + Text);
		return Text;
	}
	
public static String returnNoRecordFoundOnChangeLogGrid() {
		
		String Text = WebDriverTasks.waitForElement(changeLogGridNoRecordsFound).getText();
		System.out.println("The value on Grid is " + Text);
		return Text;
	}

}
