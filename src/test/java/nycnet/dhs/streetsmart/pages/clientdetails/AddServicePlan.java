package nycnet.dhs.streetsmart.pages.clientdetails;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class AddServicePlan {

	static By initialPlanDate = By.xpath("//*[@ng-model='ServicePlan.InitialPlanDate']");
	
	static By firstReviewDueDate = By.xpath("//*[@ng-model='ServicePlan.FirstReviewDueDate']");
	
	static By goalStartDueDate = By.xpath("//*[@ng-model='ServicePlan.GoalStartDate']");
	
	
	static By goalStartText = By.xpath("//*[@ng-model='ServicePlan.GoalText']");
	
	static By selectCase = By.xpath("//button[@ng-model='ServicePlan.CaseIds']");
	
	static By selectAllCases = By.xpath("(//label[@class='checkbox'][contains(text(), 'Select All')])[5]");
	
	static By category = By.xpath("//button[@ng-model='ServicePlan.ServicePlanCategoryIds']");
	static By categoryEducation = By.xpath("//label[@class='checkbox'][contains(text(), 'Education')]");
	
	static By signatureStaff = By.xpath("//*[@ng-model='ServicePlan.SignatureStaff']");
	
	static By serviceProvider = By.xpath("//*[@ng-model='ServicePlan.ProviderId']");
	
	static By useExistingButton = By.xpath("//button[.='Use Existing']");

	static By useExistingSignature = By.xpath("//button[.='Use Signature']");
	
	static By saveServicePlan = By.xpath("//*[@ng-click='SaveServicePlan()']");
	
	public static void clickOnInitialPlanDateToOpenDatePicker() {
		WebDriverTasks.waitForElement(initialPlanDate).click();
	}
	
	public static void clickOnFirstReviewDueDateToOpenDatePicker() {
		WebDriverTasks.waitForElement(firstReviewDueDate).click();
	}
	
	public static void clickOnGoalStartDate() {
		WebDriverTasks.waitForElement(goalStartDueDate).click();
	}
	
	public static void inputGoalStartDateText(String goalText) {
		WebDriverTasks.waitForElement(goalStartText).sendKeys(goalText);
	}
	
	public static void clickOnGoalStartDateText() {
		WebDriverTasks.waitForElement(goalStartText).click();
	}
	
	public static void clickOnCategory() {
		WebDriverTasks.scrollToElement(category);
		WebDriverTasks.waitForElement(category).click();
	}
	
	public static void selectEducationOnCategoryDropDown() {
		WebDriverTasks.waitForElement(categoryEducation).click();
	}
	
	public static void clickOnCases() {
		WebDriverTasks.waitForElement(selectCase).click();
	}
	
	public static void selectAllCases() {
		WebDriverTasks.waitForElement(selectAllCases).click();
	}
	
	public static void clickOnStaffSignatureToOpenSignaturePopup() {
		WebDriverTasks.waitForElement(signatureStaff).click();
	}
	
	public static void selectServiceProvider(String provider) {
		WebDriverTasks.waitForElementAndSelect(serviceProvider).selectByVisibleText(provider);
	}
	
	public static void clickOnUseExistingButton () {
		WebDriverTasks.waitForElement(useExistingButton).click();
	}
	
	public static void clickOnUseExistingSignatureButton () {
		WebDriverTasks.waitForElement(useExistingSignature).click();
	}
	
	public static void clickOnSaveButton () {
		WebDriverTasks.waitForElement(saveServicePlan).click();
	}
	
	

	
	


}
