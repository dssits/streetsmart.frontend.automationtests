package nycnet.dhs.streetsmart.pages.clientdetails;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class IncomeInformationTab extends StartWebDriver {

	static By clickOnIncomeInformationTab = By.xpath("//ul[@class='nav nav-tabs']//li[10]//a[1]");
	static By selectIncomeDataDropDown = By.xpath("//*[@ng-model='incomeDataValue']");
	static By returnVisibleDenialCode = By.cssSelector("span[title='N37']");
	static By clickOnClientDetailsButton = By.xpath("//button[@class='btn btn-sm btn-default pull-right']");
	static By returnClientDetailsTitle = By.xpath("//h3[@class='popover-title']");

	
	static By actionButton = By.linkText("Action");
	static By viewDetails = By.linkText("View Details");
	static By deleteOption = By.linkText("Delete");
	static By confirmButton = By.xpath("//button[.='Yes, delete it!']");
	
	static By okConfirmButton = By.xpath("//button[.='Ok']");
	static By NoRecordFoundOnGrid = By.xpath("//*[@ng-show='incomeData_GridOptions.data.length == 0']");
	
	//VA benefits and other income locators
	static By addIncomeButton = By.xpath("//*[@ng-click='AddIncome()']");
	
	static By incomeTypeDropDownMenu = By.xpath("//*[@ng-model='ClientAdditionalIncome.Additional_Income_Source_Id']");
	
	static By incomeStatusDropDownMenu = By.xpath("//*[@ng-model='ClientAdditionalIncome.Income_Status_Id']");
	
	static By effectiveDatePicker = By.xpath("//*[@ng-model='ClientAdditionalIncome.Income_Activation_Date']");
	
	static By monthlyIncomeInput = By.xpath("//*[@ng-model='ClientAdditionalIncome.Monthly_Income']");
	
	static By incomeTypeInput = By.xpath("//*[@ng-model='ClientAdditionalIncome.Additional_Income_Source_Id']");
	
	static By saveIncomeButton = By.xpath("//*[@ng-click='SaveAdditionalIncome()']");
	
	static By editButton = By.xpath("//*[@ng-click='EditAdditionalIncome()']");
	
	//Notes locators
	static By selectNotesCollapseIcon = By.xpath("//a[@id='notes-accordion']//i[@class='fa fa-plus-circle pull-right']");
	static By clickOnDeleteNote = By.xpath("//i[@class='fa fa-trash cursor-pointer fa-lg notesIcon grid-action-icon']");
	static By clickOnDeleteConfirmation = By.xpath("//button[@ng-click='ok()']");
	
	static By clickOnAddNewNotes = By
			.xpath("//a[@class='btn btn-sm btn-default addnew m-b-5'][contains(text(),'Add New')]");
	static By inputNotesText = By.xpath("//*[@ng-model='CurrentClientNote.Notes']");
	static By selectSaveNotes = By.xpath("//*[@ng-click='SaveClientNote(CurrentClientNote)']");
	static By firstRecordNoteTextOnNotesGrid = By.xpath("//tr[@pagination-id='clientNotes']/td[@class='pre-wrap-apply ng-binding']");

	//Changelog locators
	static By changelogWakingText = By.cssSelector("div[role='grid']>div[role='rowgroup']>div[class='ui-grid-canvas']>div:nth-child(1)>div>div[role='gridcell']:nth-child(3)>div>span:nth-child(1)");
	static By expandChangeLogIcon = By.cssSelector("a.collapse-panel[id='change-log-accordion']");
	static By changeLogGridNoRecordsFound = By.xpath("(//*[@ng-show='changeLogGridList_GridOptions.data.length == 0'])[1]");
	
	
	//Grid locators
	static By monthlyIncomeFirstRecordGrid = By.xpath("//*[@id='incomeDataGrid']//*[@role='grid']/*[@role='rowgroup'][@class='ui-grid-viewport ng-isolate-scope']//*[@role='row']/*[@role='gridcell'][4]");
	
	public static void clickOnIncomeInformationTab() {
		WebDriverTasks.waitForElement(clickOnIncomeInformationTab).click();
	}

	public static void selectIncomeDataDropDownMenu(String incomedatavalue) {
		WebDriverTasks.waitForElementAndSelect(selectIncomeDataDropDown).selectByVisibleText(incomedatavalue);
	}

	public static String returnVisiblePaymentStatus() {
		String denialCodeText = WebDriverTasks.waitForElement(returnVisibleDenialCode).getText();
		System.out.println(denialCodeText + " is the payment status");
		return denialCodeText;
	}

	public static void clickClientDetailsButton() {
		WebDriverTasks.waitForElement(clickOnClientDetailsButton).click();
	}

	public static String returnClientDetailsTitle() {
		String incomeinfoclientDetialsTitleText = WebDriverTasks.waitForElement(returnClientDetailsTitle).getText();
		System.out.println(incomeinfoclientDetialsTitleText + " is accessed on Income Information Tab");
		return incomeinfoclientDetialsTitleText;
	}

	
	
	public static void clickOnActionButton() {
		WebDriverTasks.waitForElement(actionButton).click();
	}
	
	public static void clickOnViewDetails() {
		WebDriverTasks.waitForElement(viewDetails).click();
	}
	
	public static void clickOnDeleteOption() {
		WebDriverTasks.waitForElement(deleteOption).click();
	}
	
	public static void clickOnConfirmButton() {
		WebDriverTasks.waitForElement(confirmButton).click();
	}
	
	public static void clickOnOKConfirmButton() {
		WebDriverTasks.waitForElement(okConfirmButton).click();
	}
	
	public static String returnNoRecordsFoundOnServicePlanGrid() {
		
		String Text = null;
		
		if (WebDriverTasks.verifyIfElementExists(NoRecordFoundOnGrid)) {
			Text = WebDriverTasks.waitForElement(NoRecordFoundOnGrid).getText();
		System.out.println("The value on Grid is " + Text);
		}
	
		return Text;
	}
	
	//VA benefit and other income actions
	public static void clickOnAddNewIncome() {
		WebDriverTasks.waitForElement(addIncomeButton).click();
	}
	
	public static void selectIncomeTypeDropDownMenu(String incomeType) {
		WebDriverTasks.waitForElementAndSelect(incomeTypeDropDownMenu).selectByVisibleText(incomeType);
	}
	
	public static void selectIncomeStatusDropDownMenu(String incomeStatus) {
		WebDriverTasks.waitForElementAndSelect(incomeStatusDropDownMenu).selectByVisibleText(incomeStatus);
	}
	
	public static void clickOnEffectiveDateToOpenCalendar() {
		WebDriverTasks.waitForElement(effectiveDatePicker).click();
	}
	
	public static void inputMonthlyIncome(String query) {
		WebDriverTasks.waitForElement(monthlyIncomeInput).sendKeys(query);
	}
	
	public static void clearMonthlyIncomeInput() {
		WebDriverTasks.waitForElement(monthlyIncomeInput).clear();
	}
	
	public static String returnMonthlyIncomeInputtedEditAddPage() {
		return WebDriverTasks.waitForElement(monthlyIncomeInput).getAttribute("value");
	}
	
	public static String returnMonthlyIncomeFirstRecordOnGrid() {
		return WebDriverTasks.waitForElement(monthlyIncomeFirstRecordGrid).getText();
	}
	
	public static void clickOnSaveButton() {
		WebDriverTasks.waitForElement(saveIncomeButton).click();
	}
	
	public static void clickOnEditButton() {
		WebDriverTasks.waitForElement(editButton).click();
	}
	
	//Notes actions
		public static void selectNotesCollapseIcon() {
			WebDriverTasks.waitForElement(selectNotesCollapseIcon).click();
		}

		public static void clickOnAddNewNotesButton() {
			WebDriverTasks.waitForElement(clickOnAddNewNotes).click();
		}

		public static void inputNotesText(String notestext) {
			WebDriverTasks.waitForElement(inputNotesText).sendKeys(notestext);
		}

		public static void selectSaveNotes() {
			WebDriverTasks.waitForElement(selectSaveNotes).click();
		}

		public static String returnNotesTextDetails() {
			String notesDetailText = WebDriverTasks.waitForElement(firstRecordNoteTextOnNotesGrid).getText();
			System.out.println(notesDetailText + " was added");
			return notesDetailText;
		}
		
		public static void selectDeleteNoteButton() {
			WebDriverTasks.waitForElement(clickOnDeleteNote).click();

		}

		public static void selectDeleteNoteButtonConfirmation() {
			WebDriverTasks.waitForElement(clickOnDeleteConfirmation).click();
			System.out.println("Note Deleted");
		}
		
		
		//Changelog actions
		public static void ScrollToChangeLogExpandIcon() {
			WebDriverTasks.scrollToElement(expandChangeLogIcon);
		}
		
		public static void clickOnChangeLogExpandIcon() {
			WebDriverTasks.waitForElement(expandChangeLogIcon).click();
		}
		
	public static void scrollToWalkingTextOnChangeLogGrid() {
			WebDriverTasks.scrollToElement(changelogWakingText);	
		}

		public static String returnWalkingTextOnChangeLogGrid() {
			
			String Text = WebDriverTasks.waitForElement(changelogWakingText).getText();
			System.out.println("The value on Grid is " + Text);
			return Text;
		}
		
	public static String returnNoRecordFoundOnChangeLogGrid() {
			
			String Text = WebDriverTasks.waitForElement(changeLogGridNoRecordsFound).getText();
			System.out.println("The value on Grid is " + Text);
			return Text;
		}

}
