package nycnet.dhs.streetsmart.pages.clientdetails;

import nycnet.dhs.streetsmart.pages.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.interactions.Actions;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class CriticalIncidentsTab extends StartWebDriver {

    static By clickOnCriticalIncidentsTab = By.xpath(("//a[@id='incidentReporttab']"));
    static By clickOnClientDetailsButton = By.xpath("//div[@id='default-tab-13']//button[contains(@class,'btn btn-sm btn-default pull-right m-r-10 m-l-10')]"
            + "[contains(text(),'View Client Details')]");
    static By returnClientDetailsTitle = By.xpath("//h3[@class='popover-title']");
    static By clickOnAddNewButton = By.xpath("//*[@ng-click='AddIncidentReport()']");

    static By clickOnProviderField = By.xpath("//*[@ng-model='IncidentReportDetails.ProviderId']");
    static By clickOnAreaField = By.xpath("//*[@ng-model='IncidentReportDetails.AreaId']");
    static By clickOnShiftField = By.xpath("//*[@ng-model='IncidentReportDetails.ShiftId']");
    static By clickOnResponseTeamField = By.xpath("(//*[@ng-model='IncidentReportDetails.ResponseTeamIds'])[2]");
    static By selectResponseTeam = By.xpath("//label[contains(@class,'checkbox')][contains(text(),'DHSPD')]");

    static By selectBorough = By.xpath("//*[@ng-model='IncidentReportDetails.BoroughId']");
    static By selectLocationType = By.xpath("//*[@ng-model='IncidentReportDetails.LocationTypeId']");
    static By enterPhone = By.xpath("//input[@id='txtPhone']");
    static By selectSaveAsDraft = By.xpath("//button[contains(text(),'Save As Draft')]");

    static By verifyProviderText = By.xpath("//span[contains(text(),'Breaking Ground')]");

    //Added By : Venkat
    static By actionButton = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:first-child>div>a");
    static By deleteButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='Delete']");
    static By confirmDeleteButton = By.xpath("//button[text()='Yes, delete it!']");
    static By closeRecordDeletionConfirmationMessage = By.xpath("//button[text()='Ok']");

    static String sectionXpath = "//div[contains(@class,'form-data') and @style]//b[normalize-space()='%s' and @aria-expanded='false']";
    static String expandedSectionXpath = "//div[contains(@class,'form-data') and @style]//b[normalize-space()='%s' and @aria-expanded='true']";
    //Shift Details
    static By provider = By.xpath("//select[@ng-model='IncidentReportDetails.ProviderId']");
    static By area = By.xpath("//select[@ng-model='IncidentReportDetails.AreaId']");
    static By shift = By.xpath("//select[@ng-model='IncidentReportDetails.ShiftId']");
    static By responseTeamListButton = By.xpath("//button[@ng-model='IncidentReportDetails.ResponseTeamIds']");
    static String checkboxOption = "//ul[@style='display: block;']//label[normalize-space()='%s']/input[@type='checkbox']";
    static By teamType = By.xpath("//select[@ng-model='ng-model='IncidentReportDetails.TeamTypeId']");
    static By teamName = By.xpath("//input[@ng-model='ng-model='IncidentReportDetails.TeamName']");
    static By subwayLine = By.xpath("//select[@ng-model='ng-model='IncidentReportDetails.SubwayLineId']");
    static By subwayStation = By.xpath("//select[@ng-model='ng-model='IncidentReportDetails.SubwayStationId']");
    //Location Details
    static By borough = By.xpath("//select[@ng-model='IncidentReportDetails.BoroughId']");
    static By locationType = By.xpath("//select[@ng-model='IncidentReportDetails.LocationTypeId']");
    static By clientSleepLocation = By.xpath("//select[@ng-model='IncidentReportDetails.ClientSleepLocationId']");
    static By additionalDetails = By.xpath("//textarea[@ng-model='IncidentReportDetails.AdditionalDetails']");
    static By phone = By.xpath("//input[@ng-model='IncidentReportDetails.Phone']");
    //Engagement Details
    static By linkToEng_yes = By.xpath("//input[@ng-model='IncidentReportDetails.LinkEngagement' and @value='true']");
    static By linkToEng_no = By.xpath("//input[@ng-model='IncidentReportDetails.LinkEngagement' and @value='false']");
    static By engagement = By.xpath("//select[@ng-model='IncidentReportDetails.ClientBriefContactId']");
    //Incident Details
    static By incidentPriority = By.xpath("//select[@ng-model='IncidentReportDetails.IncidentPriorityId']");
    static By incidentDate = By.xpath("//input[@ng-model='IncidentReportDetails.IncidentDate']");
    static By incidentTime = By.xpath("//input[@ng-model='IncidentReportDetails.IncidentTime']");
    static By reportNo = By.xpath("//input[@ng-model='IncidentReportDetails.ReporterCallbackNumber']");
    static By victimsListDropdown = By.xpath("//button[@ng-model='IncidentReportDetails.VictimRoleTypeId']");
    static By victim_client = By.id("token-input-victimClients");
    static By perpetratorListDropdown = By.xpath("//button[@ng-model='IncidentReportDetails.PerpetratorRoleTypeId']");
    static By perpetrator_client = By.id("token-input-perpetratorClients");
    static By witnessesListDropdown = By.xpath("//button[@ng-model='IncidentReportDetails.WitnessRoleTypeId']");
    static By witness_client = By.id("token-input-witnessClients");
    static String optionItem = "//ul[@style='display: block;']/li/b[text()='%s']";
    //Incident Type
    static String incidentTypeXpath = "//label[text()='%s']/../preceding-sibling::label/input[@type='checkbox']";
    //Incident Description
    static By whatHappened = By.xpath("//textarea[@ng-model='IncidentReportDetails.IncidentDescription']");
    static By recurringIncident_yes = By.xpath("//input[@ng-model='IncidentReportDetails.IsRecurringIncident' and @value='true']");
    static By recurringIncident_no = By.xpath("//input[@ng-model='IncidentReportDetails.IsRecurringIncident' and @value='false']");
    //Incident Outcome
    static By immediateAction = By.xpath("//input[@ng-model='IncidentReportDetails.ImmediateActionTaken']");
    //Additional Notes
    static By additionalNotes = By.xpath("//textarea[@ng-model='IncidentReportDetails.Notes']");
    static By submitPendingApproval = By.xpath("//button[text()='Submit - Pending Approval']");

    static By recordsRow = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']");
    static String tableTextLocator = "div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(%d)>div>span:nth-child(%d)";
    static By viewInformation = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='View Details']");

    static String fieldValue = "//div[@ng-controller='IncidentReportController']//label[normalize-space()=\"%s:\"]/following-sibling::div";
    static String incidentUsers = "//div[@ng-controller='IncidentReportController']//div[@class='abs' and text()='%s']/following-sibling::div[@class='row']//label[normalize-space()='Client:']/following-sibling::div";

    static By editButton = By.xpath("//button[text()='Edit' and not(contains(@class,'ng-hide'))]");
    static By cancelButton = By.xpath("//button[@ng-click='CancelIncidentReport()']");

    public static void clickOnCriticalIncidentsTab() {
        WebDriverTasks.waitForElement(clickOnCriticalIncidentsTab).click();
    }

    public static void clickOnClientDetailsButton() {
        WebDriverTasks.waitForElement(clickOnClientDetailsButton).click();
    }

    public static String returnClientDetailsTitle() {
        String clientDetialsTitleTextOnCriticalIncidentsTab = WebDriverTasks.waitForElement(returnClientDetailsTitle).getText();
        System.out.println(clientDetialsTitleTextOnCriticalIncidentsTab + " accessed on Critical Incidents Tab");
        return clientDetialsTitleTextOnCriticalIncidentsTab;
    }

    public static void clickOnAddNewButton() {
        WebDriverTasks.waitForElement(clickOnAddNewButton).click();
    }

    public static void clickOnProviderField(String provider) {
        WebDriverTasks.waitForElementAndSelect(clickOnProviderField).selectByVisibleText(provider);
    }

    public static void clickOnAreaField(String area) {
        WebDriverTasks.waitForElementAndSelect(clickOnAreaField).selectByVisibleText(area);
    }

    public static void clickOnShiftField(String shift) {
        WebDriverTasks.waitForElementAndSelect(clickOnShiftField).selectByVisibleText(shift);
    }

    public static void clickOnResponseTeamField() {
        WebDriverTasks.waitForElement(clickOnResponseTeamField).click();
    }

    public static void selectResponseTeam() {
        WebDriverTasks.waitForElement(selectResponseTeam).click();
    }

    /**
     * This method will set value for Borough field
     *
     * @param borough This is value to be saved
     */
    public static void selectBorough(String borough) {
        WebDriverTasks.waitForElementAndSelect(selectBorough).selectByVisibleText(borough);
    }

    /**
     * This method will set value for Location Type field
     *
     * @param locationtype This is value to be saved
     */
    public static void selectLocationType(String locationtype) {
        WebDriverTasks.waitForElementAndSelect(selectLocationType).selectByVisibleText(locationtype);
    }

    /**
     * This method will set value for Phone field
     *
     * @param phone This is value to be saved
     */
    public static void enterPhone(String phone) {
        WebDriverTasks.waitForElement(enterPhone).sendKeys(phone);
    }


    public static void selectSaveAsDraft() {
        WebDriverTasks.waitForElement(selectSaveAsDraft).click();
    }

    public static String verifyProviderText() {
        String providerText = WebDriverTasks.waitForElement(verifyProviderText).getText();
        System.out.println("Record Created with the provider " + providerText);
        return providerText;
    }

    /**
     * This method will delete all Critical Incident records
     * Created By : Venkat
     */
    public static void deleteAllCriticalIncidentRecords() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(actionButton, 3))
            return;
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(actionButton);
        for (int i = actionButtons.size() - 1; i >= 0; i--) {
            deleteCriticalIncidentRecord(i);
        }
    }

    /**
     * This method will delete record with given row number
     * Created By : Venkat
     *
     * @param rowNo This is row number, and the value should be greated than or equal to 0.
     */
    public static void deleteCriticalIncidentRecord(int rowNo) {
        WebDriverTasks.waitForAngular();
        WebDriverTasks.scrollToTop();
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(actionButton);
        assert rowNo <= actionButtons.size();
        actionButtons.get(rowNo).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(deleteButton).click();
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(confirmDeleteButton).click();
        WebDriverTasks.wait(3);
        if (WebDriverTasks.verifyIfElementIsDisplayed(closeRecordDeletionConfirmationMessage, 5))
            WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
    }

    /**
     * This method will expand section with given name
     * Created By : Venkat
     *
     * @param section This is section name
     */
    public static void expandSection(String section) {
        By expander = By.xpath(String.format(sectionXpath, section));
        By expandedSection = By.xpath(String.format(expandedSectionXpath, section));
        if (WebDriverTasks.verifyIfElementIsDisplayed(expander, 3)) {
            WebDriverTasks.scrollToElement(expander);
            WebDriverTasks.waitForElement(expander).click();
            WebDriverTasks.wait(3);
        } else {
            WebDriverTasks.scrollToElement(expandedSection);
        }
    }

    /**
     * This method will set value for Provider field
     * Created By : Venkat
     *
     * @param providerName This is value to be saved
     */
    public static void selectProvider(String providerName) {
        WebDriverTasks.waitForElementAndSelect(provider).selectByVisibleText(providerName);
    }

    /**
     * This method will set value for Area field
     * Created By : Venkat
     *
     * @param areaName This is value to be saved
     */
    public static void selectArea(String areaName) {
        if (areaName.chars().allMatch(Character::isDigit))
            WebDriverTasks.waitForElementAndSelect(area).selectByIndex(Integer.parseInt(areaName));
        else
            WebDriverTasks.waitForElementAndSelect(area).selectByVisibleText(areaName);
    }

    /**
     * This method will set value for Response Team field
     * Created By : Venkat
     *
     * @param responseTeams This is value to be saved
     */
    public static void selectResponseTeams(String responseTeams) {
        String[] teams = responseTeams.split(",");
        WebDriverTasks.waitForElement(responseTeamListButton).click();
        for (String team : teams) {
            By option = By.xpath(String.format(checkboxOption, team.trim()));
            if (!WebDriverTasks.waitForElement(option).isSelected())
                WebDriverTasks.waitForElement(option).click();
        }
    }

    /**
     * This method will set values for Shift Details section
     * Created By : Venkat
     *
     * @param data This is data to be saved
     */
    public static void submitShiftDetails(HashMap<String, String> data) {
        expandSection("Shift Details");
	/*	HashMap<String,LinkedList<By>> conditionalFields = new HashMap<>();
		conditionalFields.put("BRC",new LinkedList<>(Arrays.asList(teamType,teamName,subwayLine,subwayStation)));*/

        if (data.containsKey("Provider")) selectProvider(data.get("Provider"));
        if (data.containsKey("Area")) selectArea(data.get("Area"));
        if (data.containsKey("Shift")) selectShift(data.get("Shift"));
        if (data.containsKey("Response Team")) selectResponseTeams(data.get("Response Team"));
    }

    /**
     * This method will set value for Shift field
     * Created By : Venkat
     *
     * @param shiftValue This is value to be saved
     */
    private static void selectShift(String shiftValue) {
        WebDriverTasks.waitForElementAndSelect(shift).selectByVisibleText(shiftValue);
    }

    /**
     * This method will set value for Borough field
     * Created By : Venkat
     *
     * @param boroughValue This is value to be saved
     */
    public static void select_Borough(String boroughValue) {
        if (boroughValue.chars().allMatch(Character::isDigit))
            WebDriverTasks.waitForElementAndSelect(borough).selectByIndex(Integer.parseInt(boroughValue));
        else
            WebDriverTasks.waitForElementAndSelect(borough).selectByVisibleText(boroughValue);
    }

    /**
     * This method will set value for Location Type field
     * Created By : Venkat
     *
     * @param locationTypeValue This is value to be saved
     */
    public static void select_LocationType(String locationTypeValue) {
        WebDriverTasks.waitForElementAndSelect(locationType).selectByVisibleText(locationTypeValue);
    }

    /**
     * This method will set value for Phone field
     * Created By : Venkat
     *
     * @param phoneNo This is value to be saved
     */
    public static void setPhoneNo(String phoneNo) {
        WebDriverTasks.waitForElement(phone).clear();
        WebDriverTasks.waitForElement(phone).sendKeys(phoneNo);
    }

    /**
     * This method will set value for Is Current Sleep Location? field
     * Created By : Venkat
     *
     * @param currentSleepLocation This is value to be saved
     */
    public static void isCurrentSleepLocation(String currentSleepLocation) {
        WebDriverTasks.waitForElementAndSelect(clientSleepLocation).selectByVisibleText(currentSleepLocation);
    }

    /**
     * This method will set value for Additional Details field
     * Created By : Venkat
     *
     * @param detail This is value to be saved
     */
    public static void addAdditionalDetails(String detail) {
        WebDriverTasks.waitForElement(additionalDetails).clear();
        WebDriverTasks.waitForElement(additionalDetails).sendKeys(detail);
    }

    /**
     * This method will set values for Location Details section
     * Created By : Venkat
     *
     * @param data This is data to be saved
     */
    public static void submitLocationDetails(HashMap<String, String> data) {
        expandSection("Location Details");
        if (data.containsKey("Borough")) select_Borough(data.get("Borough"));
        if (data.containsKey("Location Type")) select_LocationType(data.get("Location Type"));
        if (data.containsKey("Phone")) setPhoneNo(data.get("Phone"));
        if (data.containsKey("Is this the client's sleep location?"))
            isCurrentSleepLocation(data.get("Is this the client's sleep location?"));
        if (data.containsKey("Additional Details")) addAdditionalDetails(data.get("Additional Details"));
    }

    /**
     * This method will set value for Link To Existing Engagement field
     * Created By : Venkat
     *
     * @param link This is value to be saved
     */
    public static void linkToExistingEngagement(String link) {
        if (link.equalsIgnoreCase("yes"))
            WebDriverTasks.waitForElement(linkToEng_yes).click();
        else
            WebDriverTasks.waitForElement(linkToEng_no).click();
    }

    /**
     * This method will set value for Engagement field
     * Created By : Venkat
     *
     * @param engagementValue This is value to be saved
     */
    public static void selectEngagement(String engagementValue) {
        if (engagementValue.chars().allMatch(Character::isDigit))
            WebDriverTasks.waitForElementAndSelect(engagement).selectByIndex(Integer.parseInt(engagementValue));
        else
            WebDriverTasks.waitForElementAndSelect(engagement).selectByVisibleText(engagementValue);
    }

    /**
     * This method will set values for Engagement Details section
     * Created By : Venkat
     *
     * @param data This is data to be saved
     */
    public static void submitEngagementDetails(HashMap<String, String> data) {
        expandSection("Engagement Details");
        if (data.containsKey("Link to an existing engagement"))
            linkToExistingEngagement(data.get("Link to an existing engagement"));
        if (data.containsKey("Link to an existing engagement")) {
            if (data.get("Link to an existing engagement").equalsIgnoreCase("yes")) {
                if (data.containsKey("Select Engagement")) selectEngagement(data.get("Select Engagement"));
            }
        }
    }

    /**
     * This method will set values for Incident Details section
     * Created By : Venkat
     *
     * @param data This is data to be saved
     */
    public static void submitIncidentDetails(HashMap<String, String> data) {
        expandSection("Incident Details");
        if (data.containsKey("Incident Priority")) selectIncidentPriority(data.get("Incident Priority"));
        if (data.containsKey("Incident Date")) selectIncidentDate(data.get("Incident Date"));
        if (data.containsKey("Incident Time")) setIncidentTime(data.get("Incident Time"));
        if (data.containsKey("Reporter Number")) setReportNo(data.get("Reporter Number"));
        if (data.containsKey("Victims")) selectVictims(data.get("Victims"), data.get("Victim Client"));
        if (data.containsKey("Perpetrators"))
            selectPerpetrators(data.get("Perpetrators"), data.get("Perpetrator Client"));
        if (data.containsKey("Witnesses")) selectWitnesses(data.get("Witnesses"), data.get("Witness Client"));
    }

    /**
     * This method will set value for Incident Priority field
     * Created By : Venkat
     *
     * @param priority This is value to be saved
     */
    public static void selectIncidentPriority(String priority) {
        WebDriverTasks.waitForElementAndSelect(incidentPriority).selectByVisibleText(priority);
    }

    /**
     * This method will set value for Incident Date field
     * Created By : Venkat
     *
     * @param date This is value to be saved
     */
    public static void selectIncidentDate(String date) {
        Base.selectDate(incidentDate, date);
    }

    /**
     * This method will set value for Incident Time field
     * Created By : Venkat
     *
     * @param time This is value to be saved
     */
    public static void setIncidentTime(String time) {
        WebDriverTasks.waitForElement(incidentTime).sendKeys(time);
    }

    /**
     * This method will set value for Report No field
     * Created By : Venkat
     *
     * @param reportNumber This is value to be saved
     */
    public static void setReportNo(String reportNumber) {
        WebDriverTasks.waitForElement(reportNo).sendKeys(reportNumber);
    }

    /**
     * This method will set value for Victims field
     * Created By : Venkat
     *
     * @param victims This is type of victim value
     * @param userID  This is user id to be selected
     */
    public static void selectVictims(String victims, String userID) {
        String[] teams = victims.split(",");
        WebDriverTasks.waitForElement(victimsListDropdown).click();
        for (String team : teams) {
            By option = By.xpath(String.format(checkboxOption, team.trim()));
            if (!WebDriverTasks.waitForElement(option).isSelected())
                WebDriverTasks.waitForElement(option).click();
        }

        Actions actions = new Actions(WebDriverTasks.driver);
        actions.sendKeys(WebDriverTasks.waitForElement(victim_client), userID).build().perform();
        WebDriverTasks.wait(3);
        By option = By.xpath(String.format(optionItem, userID));
        WebDriverTasks.waitForElement(option).click();
    }

    /**
     * This method will set value for Perpetrators field
     * Created By : Venkat
     *
     * @param perpetrators This is type of Perpetrator value to be saved
     * @param userID       This is user id to be selected
     */
    public static void selectPerpetrators(String perpetrators, String userID) {
        String[] teams = perpetrators.split(",");
        WebDriverTasks.waitForElement(perpetratorListDropdown).click();
        for (String team : teams) {
            By option = By.xpath(String.format(checkboxOption, team.trim()));
            if (!WebDriverTasks.waitForElement(option).isSelected())
                WebDriverTasks.waitForElement(option).click();
        }

        Actions actions = new Actions(WebDriverTasks.driver);
        actions.sendKeys(WebDriverTasks.waitForElement(perpetrator_client), userID).build().perform();
        WebDriverTasks.wait(3);
        By option = By.xpath(String.format(optionItem, userID));
        WebDriverTasks.waitForElement(option).click();
    }

    /**
     * This method will set value for Witnesses field
     * Created By : Venkat
     *
     * @param witnesses This is type of Witness value to be saved
     * @param userID    This is user id to be selected
     */
    public static void selectWitnesses(String witnesses, String userID) {
        String[] teams = witnesses.split(",");
        WebDriverTasks.waitForElement(witnessesListDropdown).click();
        for (String team : teams) {
            By option = By.xpath(String.format(checkboxOption, team.trim()));
            if (!WebDriverTasks.waitForElement(option).isSelected())
                WebDriverTasks.waitForElement(option).click();
        }

        Actions actions = new Actions(WebDriverTasks.driver);
        actions.sendKeys(WebDriverTasks.waitForElement(witness_client), userID).build().perform();
        WebDriverTasks.wait(3);
        By option = By.xpath(String.format(optionItem, userID));
        WebDriverTasks.waitForElement(option).click();
    }

    /**
     * This method will set values for Incident Description section
     * Created By : Venkat
     *
     * @param data This is data to be saved
     */
    public static void submitIncidentDescriptionDetails(HashMap<String, String> data) {
        expandSection("Incident Description");
        if (data.containsKey("What Happened?")) setWhatHappened(data.get("What Happened?"));
        if (data.containsKey("Recurring Incident?")) isRecurringIncident(data.get("Recurring Incident?"));
    }

    /**
     * This method will set value for What Happened? field
     * Created By : Venkat
     *
     * @param incident This is value to be saved
     */
    public static void setWhatHappened(String incident) {
        WebDriverTasks.waitForElement(whatHappened).clear();
        WebDriverTasks.waitForElement(whatHappened).sendKeys(incident);
    }

    /**
     * This method will set value for Is Recurring Incident? field
     *
     * @param isRecurring This is value to be saved
     */
    public static void isRecurringIncident(String isRecurring) {
        if (isRecurring.equalsIgnoreCase("yes"))
            WebDriverTasks.waitForElement(recurringIncident_yes).click();
        else
            WebDriverTasks.waitForElement(recurringIncident_no).click();
    }

    /**
     * This method will set values for Incident Type section
     * Created By : Venkat
     *
     * @param data This is data to be saved
     */
    public static void submitIncidentTypeDetails(HashMap<String, String> data) {
        expandSection("Incident Type");
        if (data.containsKey("Incident Type")) selectIncidentType(data.get("Incident Type"));
    }

    /**
     * This method will set values for Incident Type field
     * Created By : Venkat
     *
     * @param incidentTypes This is value to be saved
     */
    public static void selectIncidentType(String incidentTypes) {
        String[] incidents = incidentTypes.split(",");
        for (String incident : incidents) {
            By checkBox = By.xpath(String.format(incidentTypeXpath, incident));
            if (!WebDriverTasks.waitForElement(checkBox).isSelected())
                WebDriverTasks.waitForElement(checkBox).click();
        }
    }

    /**
     * This method will set values for Incident Outcome section
     * Created By : Venkat
     *
     * @param data This is hashmap for each field and value
     */
    public static void submitIncidentOutcomeDetails(HashMap<String, String> data) {
        expandSection("Incident Outcome");
        if (data.containsKey("Immediate Action Taken")) setActionTaken(data.get("Immediate Action Taken"));
    }

    /**
     * This method will set values for Action taken field
     * Created By : Venkat
     *
     * @param actionTaken This is value to be saved
     */
    public static void setActionTaken(String actionTaken) {
        WebDriverTasks.waitForElement(immediateAction).sendKeys(actionTaken);
    }

    /**
     * This method will set values for additional note field
     * Created By : Venkat
     *
     * @param data This is value to be saved
     */
    public static void setAdditionalNotes(HashMap<String, String> data) {
        expandSection("Additional Notes");
        if (data.containsKey("Additional Notes"))
            WebDriverTasks.waitForElement(additionalNotes).sendKeys(data.get("Additional Notes"));
    }

    /**
     * This method will click on Submit Pending approval button
     * Created By : Venkat
     */
    public static void clickSubmitPendingApproval() {
        WebDriverTasks.waitForElement(submitPendingApproval).click();
    }

    /**
     * This method will get number of Critical Incident records
     * Created By : Venkat
     *
     * @return Integer This is count of health information records
     */
    public static int getNoOfRecords() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(recordsRow, 5))
            return 0;
        return WebDriverTasks.waitForElements(recordsRow).size();
    }

    /**
     * This method will verify expected data with data displayed in table
     * Created By : Venkat
     *
     * @param data This is hashmap with key as field label and value as expected value
     * @return String This is comma separated list of fields for which mismatch is observed
     */
    public static String verifyDataInTable(HashMap<String, String> data) {
        LinkedList<String> mismatchedData = new LinkedList<>();
        //Validation will be done for below fields
        HashMap<String, String> colMapper = new HashMap<>();
        colMapper.put("Incident Priority", "Incident Priority");
        colMapper.put("Status", "Status");
        colMapper.put("Incident Date", "Incident Date");
        colMapper.put("Incident Type", "Incident Type");
        colMapper.put("Response Team", "Response Team");
        colMapper.put("Incident Description", "What Happened?");
        colMapper.put("Incident Notes", "Additional Notes");
        colMapper.put("Borough", "Borough");
        colMapper.put("Location Type", "Location Type");
        colMapper.put("Location Details", "Phone");
        colMapper.put("Witnesses", "Witnesses");
        colMapper.put("Victims", "Victims");
        colMapper.put("Perpetrators", "Perpetrators");
        colMapper.put("Reporter", "Modified By");
        colMapper.put("Modified By", "Modified By");

        WebDriverTasks.scrollToElement(recordsRow);

        for (String key : colMapper.keySet()) {
            if (!data.get(colMapper.get(key)).equals(getValueFromTable(key)))
                mismatchedData.add(key);
        }
        return String.join(",", mismatchedData);
    }

    /**
     * This method will get value from table for given label/field name
     * Created By : Venkat
     *
     * @param label This is label/field name
     * @return String This is actual value displayed for given field
     */
    public static String getValueFromTable(String label) {
        LinkedList<String> multiValuedFields = new LinkedList<>(Arrays.asList("Incident Type", "Response Team"));
        //Below is location indexes for given column name
        HashMap<String, int[]> map = new HashMap<>();
        map.put("Incident #", new int[]{2, 1});
        map.put("Incident Priority", new int[]{2, 5});

        map.put("Provider", new int[]{3, 1});
        map.put("Area", new int[]{3, 3});
        map.put("Status", new int[]{3, 5});

        map.put("Incident Date", new int[]{4, 1});

        map.put("Incident Type", new int[]{5, 1});
        map.put("Response Team", new int[]{5, 3});

        map.put("Incident Description", new int[]{6, 1});
        map.put("Incident Notes", new int[]{6, 3});

        map.put("Borough", new int[]{7, 1});
        map.put("Location Type", new int[]{7, 3});
        map.put("Location Details", new int[]{7, 5});

        map.put("Reporter", new int[]{8, 1});
        map.put("Witnesses", new int[]{8, 3});
        map.put("Victims", new int[]{8, 5});
        map.put("Perpetrators", new int[]{8, 7});

        map.put("Modified By", new int[]{12, 1});


        if (!map.containsKey(label)) {
            return null;
        }

        int[] matrix = map.get(label);
        String xpath = String.format(tableTextLocator, matrix[0], matrix[1]);
        By element = By.cssSelector(xpath);
        String value = WebDriverTasks.waitForElement(element).getText().trim();
        if (multiValuedFields.contains(label)) {
            value = value.replace(", ", ",");
        }
        return value;
    }

    /**
     * This method will click on Action an then View Details button
     * Created By : Venkat
     *
     * @param rowNo This is row number for which we want to view details, row number starts from 0
     */
    public static void viewCriticalInformationRecord(int rowNo) {
        WebDriverTasks.scrollToTop();
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(actionButton);
        assert rowNo <= actionButtons.size();
        WebDriverTasks.wait(3);
        actionButtons.get(rowNo).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(viewInformation).click();
        WebDriverTasks.wait(3);
    }

    /**
     * This method will verify expected data with data displayed in form and return mismatched fields
     * Created By : Venkat
     *
     * @param data This is hashmap with fields labels and values
     * @return String This is comma separated list of fields for which mismatch is observed
     */
    public static String verifyCriticalIncidentData(HashMap<String, String> data) {
        String expectedVal, actualVal;
        LinkedList<String> mismatchedFields = new LinkedList<>();
        LinkedList<String> skipFields = new LinkedList<>(Arrays.asList("Provider", "Modified By", "Status", "Area", "Additional Notes"));
        LinkedList<String> multiValuesFields = new LinkedList<>(Arrays.asList("Response Team"));
        HashMap<String, String> clientNames = new HashMap<>();
        clientNames.put("Victim Client", "Victims");
        clientNames.put("Perpetrator Client", "Perpetrators");
        clientNames.put("Witness Client", "Witnesses");

        for (String field : data.keySet()) {
            if (skipFields.contains(field)) continue;
            expectedVal = data.get(field);
            if (clientNames.containsKey(field))
                actualVal = getClientName(clientNames.get(field));
            else
                actualVal = getFieldValue(field);
            if (!expectedVal.equalsIgnoreCase(actualVal)) {
                if (multiValuesFields.contains(field)) {
                    LinkedList<String> expectedList = new LinkedList<>(Arrays.asList(expectedVal.split(",")));
                    LinkedList<String> actualList = new LinkedList<>(Arrays.asList(actualVal.replace(", ", ",").split(",")));
                    if (expectedList.size() != actualList.size())
                        mismatchedFields.add(field);
                    else {
                        actualList.removeAll(expectedList);
                        if (actualList.size() != 0)
                            mismatchedFields.add(field);
                    }
                } else if (!expectedVal.replace(" ", "").equalsIgnoreCase(actualVal.replace(" ", ""))) {
                    mismatchedFields.add(field);
                }
            }
        }
        return String.join(",", mismatchedFields);
    }

    /**
     * This method will return value for given field name
     * Created By : Venkat
     *
     * @param fieldName This is field name
     * @return String This is field value
     */
    public static String getFieldValue(String fieldName) {
        By fieldElement = By.xpath(String.format(fieldValue, fieldName));
        return WebDriverTasks.waitForElement(fieldElement).getText().trim();
    }

    /**
     * This method will get saved client name for Victims,Perpetrators and Witnesses section
     * Created By : Venkat
     *
     * @param type This is type and it can be any of Victims,Perpetrators or Witnesses
     * @return String This is saved user name
     */
    public static String getClientName(String type) {
        By fieldElement = By.xpath(String.format(incidentUsers, type));
        return WebDriverTasks.waitForElement(fieldElement).getText().trim();
    }

    /**
     * This method will click on Edit button to edit saved data in Critical Incident form
     * Created By : Venkat
     */
    public static void clickEditButton() {
        WebDriverTasks.waitForElement(editButton).click();
    }

    /**
     * This method will click on Cancel button to navigate back to Critical Incident table
     * Created By : Venkat
     */
    public static void clickCancelButton() {
        WebDriverTasks.waitForElement(cancelButton).click();
    }
}