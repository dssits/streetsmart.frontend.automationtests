package nycnet.dhs.streetsmart.pages.clientdetails;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class ServicePlanTab {

	static By servicePlanTab = By.id("serviceplantab");
	
	static By addNewServicePlan = By.xpath("//*[@ng-click='CanAddServicePlan()']");
	
	static By actionButton = By.linkText("Action");
	
	static By viewDetails = By.linkText("View Details");
	
	static By deleteOption = By.linkText("Delete");
	
	static By goalTextOnGrid = By.xpath("//div[@role='rowgroup']/div/div/div[@role='row']/div[@role='gridcell'][8]/div/span[2]");
	
	static By NoRecordFoundOnGrid = By.xpath("//*[@ng-show='servicePlanGrid_GridOptions.data.length == 0']");
	
	//static By confirmButton = By.cssSelector("button[class='confirm']");
	
	static By confirmButton = By.xpath("//button[.='Yes, delete it!']");
	
	static By okConfirmButton = By.xpath("//button[.='Ok']");
	
	//Service plan view page locators
	static By EditServicePlanOnViewPage = By.xpath("//*[@ng-click='EditServicePlan()']");
	
	//Service plan Edit page locators
		static By SaveServicePlanOnEditPage = By.xpath("//*[@ng-click='SaveServicePlan()']");
		
		static By goalTextInputBoxOnEditPage = By.xpath("//*[@ng-model='ServicePlan.GoalText']");
		
		//Notes
		static By selectNotesCollapseIcon = By
				.xpath("//a[@id='notes-accordion']//i[@class='fa fa-plus-circle pull-right']");
		static By clickOnAddNewNotes = By
				.xpath("//a[@class='btn btn-sm btn-default addnew m-b-5'][contains(text(),'Add New')]");
		static By inputNotesText = By.xpath("//*[@ng-model='CurrentClientNote.Notes']");
		static By inputNotesTextOnEdit = By.xpath("//textarea[@ng-model='item.Notes']");
		static By selectSaveNotes = By.xpath("//*[@ng-click='SaveClientNote(CurrentClientNote)']");
		static By selectSaveNotesOnEdit = By.xpath("//*[@ng-click='SaveClientNote(item)']");
		static By noRecordFoundOnNotesGrid = By.xpath("//*[@ng-show='(!ClientNotesList || ClientNotesList.length < 1) && TopNotesRowMode != utils.constant.UIViewMode.Add']");
		static By firstRecordNoteTextOnNotesGrid = By.xpath("//tr[@pagination-id='clientNotes']/td[@class='pre-wrap-apply ng-binding']");
		static By EditfirstRecordOnNotesGrid = By.xpath("//i[@ng-click='EditClientNote(item)']");
		static By DeletefirstRecordOnNotesGrid = By.xpath("//i[@ng-click='DeleteClientNote(item)']");
		static By ConfirmDeleteRecordOnNotesGrid = By.xpath("//button[@ng-click='ok()']");
		
		//Changelog
		static By changelogGoalText = By.cssSelector("div[role='grid']>div[role='rowgroup']>div[class='ui-grid-canvas']>div:nth-child(1)>div>div[role='gridcell']:nth-child(8)>div>span:nth-child(3)");
		static By expandChangeLogIcon = By.cssSelector("a.collapse-panel[id='change-log-accordion']");
		static By changeLogGridNoRecordsFound = By.xpath("(//*[@ng-show='changeLogGridList_GridOptions.data.length == 0'])[1]");
		
		
	public static void clickOnServicePlanTab() {
		WebDriverTasks.waitForElement(servicePlanTab).click();
	}
	
	public static void clickOnActionButton() {
		WebDriverTasks.waitForElement(actionButton).click();
	}
	
	public static void clickOnViewDetails() {
		WebDriverTasks.waitForElement(viewDetails).click();
	}
	
	public static void clickOnDeleteOption() {
		WebDriverTasks.waitForElement(deleteOption).click();
	}
	
	public static void clickOnConfirmButton() {
		WebDriverTasks.waitForElement(confirmButton).click();
	}
	
	public static void clickOnOKConfirmButton() {
		WebDriverTasks.waitForElement(okConfirmButton).click();
	}
	
	public static void clickOnAddNewButton() {
		WebDriverTasks.scrollToElement(addNewServicePlan);
		WebDriverTasks.waitForElement(addNewServicePlan).click();
	}
	
	public static String returnGoalText() {
		String Text = WebDriverTasks.waitForElement(goalTextOnGrid).getText();
		System.out.println("The Goal Text is " + Text);
		return Text;
	}
	
	public static String returnNoRecordsFoundOnServicePlanGrid() {
		
		String Text = null;
		
		if (WebDriverTasks.verifyIfElementExists(NoRecordFoundOnGrid)) {
			Text = WebDriverTasks.waitForElement(NoRecordFoundOnGrid).getText();
		System.out.println("The value on Grid is " + Text);
		}
	
		return Text;
	}
	
	//Service plan view page actions
	public static void clickOnEditServicePlanButtonOnViewPage() {
		WebDriverTasks.scrollToElement(EditServicePlanOnViewPage);
		WebDriverTasks.waitForElement(EditServicePlanOnViewPage).click();
	}
	
	//Service plan edit page actions
		public static void clickOnSaveServicePlanButtonOnEditPage() {
			WebDriverTasks.scrollToElement(SaveServicePlanOnEditPage);
			WebDriverTasks.waitForElement(SaveServicePlanOnEditPage).click();
		}
		
		public static void scrollToGoalTextOnEditPage() {
			WebDriverTasks.scrollToElement(goalTextInputBoxOnEditPage);
			
		}
		
		public static void InputGoalTextOnEditPage(String text) {
			WebDriverTasks.scrollToElement(goalTextInputBoxOnEditPage);
			WebDriverTasks.waitForElement(goalTextInputBoxOnEditPage).clear();
			WebDriverTasks.waitForElement(goalTextInputBoxOnEditPage).sendKeys(text);
		}
		
		//service plan notes actions
		public static void selectNotesCollapseIcon() {
			WebDriverTasks.scrollToElement(selectNotesCollapseIcon);
			WebDriverTasks.waitForElement(selectNotesCollapseIcon).click();
		}

		public static void clickOnAddNewNotesButton() {
			WebDriverTasks.waitForElement(clickOnAddNewNotes).click();
		}

		public static void inputNotesText(String notestext) {
			WebDriverTasks.waitForElement(inputNotesText).clear();
			WebDriverTasks.waitForElement(inputNotesText).sendKeys(notestext);
		}
		
		public static void inputNotesTextOnEdit(String notestext) {
			WebDriverTasks.waitForElement(inputNotesTextOnEdit).clear();
			WebDriverTasks.waitForElement(inputNotesTextOnEdit).sendKeys(notestext);
		}

		public static void selectSaveNotes() {
			WebDriverTasks.waitForElement(selectSaveNotes).click();
		}
		
		public static void clickOnSaveNotesOnEdit() {
			WebDriverTasks.waitForElement(selectSaveNotesOnEdit).click();
		}
		
		public static String returnNoRecordsFoundOnNotesGrid() {
			
			String Text = null;
			
			if (WebDriverTasks.verifyIfElementExists(noRecordFoundOnNotesGrid)) {
				Text = WebDriverTasks.waitForElement(noRecordFoundOnNotesGrid).getText();
			System.out.println("The value on Grid is " + Text);
			}
		
			return Text;
		}
		
	public static String returnFirstNotesTextOnNotesGrid() {
			
			String Text = WebDriverTasks.waitForElement(firstRecordNoteTextOnNotesGrid).getText();
			System.out.println("The value on Grid is " + Text);
			return Text;
		}
	
	public static void clickOnIconToEditFirstNoteOnNotesGrid() {
		WebDriverTasks.waitForElement(EditfirstRecordOnNotesGrid).click();
	}
	
	public static void clickOnIconToDeleteFirstNoteOnNotesGrid() {
		WebDriverTasks.waitForElement(DeletefirstRecordOnNotesGrid).click();
	}
	
	public static void confirmDeleteNoteOnNotesGrid() {
		WebDriverTasks.waitForElement(ConfirmDeleteRecordOnNotesGrid).click();
	}
	
	//Changelog

	
	public static void clickOnChangeLogExpandIcon() {
		WebDriverTasks.scrollToElement(expandChangeLogIcon);
		WebDriverTasks.waitForElement(expandChangeLogIcon).click();
	}
	
	public static String returnGoalTextOnChangeLogGrid() {
		
		String Text = WebDriverTasks.waitForElement(changelogGoalText).getText();
		System.out.println("The value on Grid is " + Text);
		return Text;
	}
	
public static String returnNoRecordFoundOnChangeLogGrid() {
		
		String Text = WebDriverTasks.waitForElement(changeLogGridNoRecordsFound).getText();
		System.out.println("The value on Grid is " + Text);
		return Text;
	}
	
}
