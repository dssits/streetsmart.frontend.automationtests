package nycnet.dhs.streetsmart.pages.clientdetails;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;



public class EngagementsTab {
	 
	static By actionButton = By.linkText("Action");
	
	 static By viewDetails = By.linkText("View Details");
	 
	 static By engagementTab = By.id("engagementtab");
	
	 static By editButton = By.xpath("(//*[@id='default-tab-7']//button[contains(text(),'Edit')])[2]");
	 
	 static By saveButton = By.xpath("//*[@id='default-tab-7']//button[contains(text(),'Save')]");
	 
	 static By calendarDate = By.xpath("(//table[@class='ui-datepicker-calendar']/tbody/tr[1]/td[5]/a[contains(text(),'2')])[3]");
	 
	 static By calendarOldDate = By.xpath("(//table[@class='ui-datepicker-calendar']/tbody/tr[1]/td[4]/a[contains(text(),'1')])[3]");
	 
	 static By openCalendar = By.xpath("//*[@ng-model='ClientEngagement.ContactDate']");
	 static By doneCalendarButton = By.xpath("//*[@id='ui-datepicker-div']//button[contains(text(),'Done')]");
	 static By nextMonthOnCalendar = By.xpath("//*[@id='ui-datepicker-div']//a[@class='ui-datepicker-next ui-corner-all']");
	 static By subwayDiversionProgramStartDate  = By.xpath("(//*[@ng-bind='Client.ClientEnrollment.SubwayDiversionProgramStartDate'])[3]");
	 
	static By contactDateSelectedYear = By.xpath("(//select[@class='ui-datepicker-year'][@data-handler='selectYear']/option[@selected='selected'])[3]");
	static By contactDateSelectedMonth = By.xpath("(//select[@class='ui-datepicker-month']/option[@selected='selected'])[3]");

	static By caseProvider = By.cssSelector("div.ui-grid-canvas>div.ui-grid-row>div[role='row']>div:nth-child(2)>div:first-child>span");
	 public static void clickOnEngagementsTab () {
		 WebDriverTasks.waitForElement(engagementTab).click();
	 }
	 
	 public static void clickOnActionButton () {
		 WebDriverTasks.waitForElement(actionButton).click();
	 }
	 
	 public static void clickOnViewDetails () {
		 WebDriverTasks.waitForElement(viewDetails).click();
	 }
	 
	 public static void scrollToEditButton () {
		 WebDriverTasks.scrollToElement(editButton);
	 }
	 
	 public static void scrollToElementJavascriptExecutor () {
		 WebDriverTasks.scrollToElementJavascriptExecutor(editButton);
	 }
	 
	 public static void clickOnEditButton () {
		 WebDriverTasks.waitForElement(editButton).click();
	 }
	  
	 public static void pageDownToEditButton () {
		 WebDriverTasks.waitForElement(editButton).sendKeys(Keys.PAGE_DOWN);
	 }
	 
	 public static void clickOnDatePickerCalendar () {
		 WebDriverTasks.waitForElement(openCalendar).click();
	 }
	 
	 public static void clearDatePickerCalendarInput () {
		 WebDriverTasks.waitForElement(openCalendar).clear();
	 }
	 
	 public static void chooseNewDateOnCalendar () {
		 WebDriverTasks.waitForElement(calendarDate).click();
	 }
	 
	 public static void chooseOldDateOnCalendar () {
		 WebDriverTasks.waitForElement(calendarOldDate).click();
	 }
	 
	 public static void pageUpToOpenCalendar () {
		 WebDriverTasks.waitForElement(openCalendar).sendKeys(Keys.PAGE_UP);
	 }
	 
	 public static void clickOnDoneButtonOnCalendar() {
		 WebDriverTasks.waitForElement(doneCalendarButton).click();
	 }
	 
	 public static void clickOnSaveButtonOnCalendar() {
		 WebDriverTasks.waitForElement(saveButton).click();
	 }
	 
	 public static void pageDownToSaveButton () {
		 WebDriverTasks.waitForElement(saveButton).sendKeys(Keys.PAGE_DOWN);
	 }
	 
	 public static void clickOnNextMonthOnCalendar() {
		 WebDriverTasks.waitForElement(nextMonthOnCalendar).click();
	 }
	 
	 public static String returnSubwayDiversionProgramStartDate() {
		  String subwayDiversionProgramStartDateText =  WebDriverTasks.waitForElement(subwayDiversionProgramStartDate).getText();
		  System.out.println("The Subway Diversion Program Start Date is " + subwayDiversionProgramStartDateText);
		  return subwayDiversionProgramStartDateText;
	  }
	 
	 public static String returnContactDateSelectedYear() {
			String Text = WebDriverTasks.waitForElement(contactDateSelectedYear).getText();
			System.out.println("The selected year in the contact date is " + Text);
			return Text;
		}
}
