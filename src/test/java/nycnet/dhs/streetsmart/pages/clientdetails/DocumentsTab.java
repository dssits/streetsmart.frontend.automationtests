package nycnet.dhs.streetsmart.pages.clientdetails;

import net.bytebuddy.utility.RandomString;
import nycnet.dhs.streetsmart.core.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.Key;
import java.util.List;

public class DocumentsTab extends StartWebDriver {

    static By clickOnDocumentsTab = By.xpath("//a[@id='documenttab']");
    static By clickOnClientDetailsButton = By.xpath("//*[@class='btn btn-sm btn-default m-b-5 m-r-5 pull-right']");
    static By returnClientDetailsTitle = By.xpath("//h3[@class='popover-title']");
    static By clickOnAddNewDocumentButton = By.xpath("//*[@ng-click='AddDocument()']");
    static By selectDocumentStatus = By.xpath("//*[@ng-model='document.DocumentStatusId']");
    static By selectDocumentType = By.xpath("//*[@ng-model='document.DocumentTypeId']");
    static By clickOnSaveButton = By.xpath("(//*[@ng-click='SaveADocument(document)'])[1]");
    static By returnDocumentStatus = By.xpath("//span[contains(text(),'Documents Applying For')]");

    //Added By Venkat
    static By documentNotes = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']:first-child>td:nth-child(6)>textarea");
    static By fileUploadInput = By.xpath("//div[contains(@ng-show,'EditMode')]/input[@name='uploadText1']");
    static By editFirstDocument = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']:first-child>td:first-child>div>div[class='']>i[ng-click*='Edit']");
    static By deleteFirstDocument = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']:first-child>td:first-child>div>div[class='']>i[ng-click*='Delete']");
    static By deleteDocuments = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']>td:first-child>div>div[class='']>i[ng-click*='Delete']");
    static By documentStatusText = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']:first-child>td:nth-child(2)>span");
    static By documentTypeText = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']:first-child>td:nth-child(3)>span");
    static By documentCategoryText = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']:first-child>td:nth-child(4)>span");
    static By notesText = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']:first-child>td:nth-child(6)>span");
    static By modifiedByText = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']:first-child>td:nth-child(7)>span:first-child");
    static By uploadedFileLink = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']:first-child>td:nth-child(8)>div:nth-child(2)>label:first-child>a");
    static By documentRecords = By.cssSelector("div.table-responsive>table>tbody>tr[pagination-id='documentGrid']");

    static By documentAvailableWithCopies = By.xpath("//label[text()='Documents Available With Copies:']/following-sibling::div//button");
    static By documentAvailableWithoutCopies = By.xpath("//label[text()='Documents Available Without Copies:']/following-sibling::div//button");
    static By optionGreenCard = By.xpath("//ul[contains(@style,'display: block;')]//label[normalize-space()='Green Card']/input");
    static By goButton = By.xpath("//button[text()='Go']");

    static By firstDocumentUploadButton = By.xpath("//div[not(contains(@class,'ng-hide'))]/div/input[contains(@name,'clientDocument')]/preceding-sibling::span[text()='Upload']");
    static By uploadButton = By.xpath("//div[@ng-show='InlineEditingMode']//div[not(contains(@class,'ng-hide'))]/div/input[contains(@name,'clientDocument')]/preceding-sibling::span[text()='Upload']");
    static By saveButton = By.xpath("//button[text()='Save' and @ng-click='SaveDocuemnts()']");

    static By confirmDeletion = By.xpath("//button[text()='Yes, delete it!']");
    static By closeRecordDeletionConfirmationMessage = By.xpath("//button[text()='Ok']");


    public static void clickOnDocumentsTab() {
        WebDriverTasks.waitForElement(clickOnDocumentsTab).click();
    }

    public static void clickClientDetailsButton() {
        WebDriverTasks.waitForElement(clickOnClientDetailsButton).click();
    }

    public static String returnClientDetailsTitle() {
        String clientDetialsTitleText = WebDriverTasks.waitForElement(returnClientDetailsTitle).getText();
        System.out.println(clientDetialsTitleText + " accessed on Documents Tab");
        return clientDetialsTitleText;
    }

    public static void clickOnAddNewDocumentButton() {
        WebDriverTasks.waitForElement(clickOnAddNewDocumentButton).click();
    }

    public static void selectDocumentStatus(String documentstatus) {
        WebDriverTasks.waitForElementAndSelect(selectDocumentStatus).selectByVisibleText(documentstatus);
    }

    public static void selectDocumentType(String documenttype) {
        WebDriverTasks.waitForElementAndSelect(selectDocumentType).selectByVisibleText(documenttype);
    }

    public static void clickOnSaveButton() {
        WebDriverTasks.waitForElement(clickOnSaveButton).click();
    }

    public static String returnDocumentStatus() {
        String documentStatusText = WebDriverTasks.waitForElement(returnDocumentStatus).getText();
        System.out.println(documentStatusText + " is seen as the Document Status ");
        return documentStatusText;
    }

    /**
     * This method will set document notes
     * Created By : Venkat
     *
     * @param note This is notes text to be set
     */
    public static void setDocumentNotes(String note) {
        WebDriverTasks.waitForElement(documentNotes).clear();
        WebDriverTasks.waitForElement(documentNotes).sendKeys(note);
    }

    /**
     * This method will upload the document
     * Created By : Venkat
     *
     * @param documentPath This is document path
     */
    public static void uploadDocument(String documentPath) throws AWTException {
//        WebDriverTasks.driver.findElement(fileUploadInput).sendKeys(documentPath);
        Actions actions = new Actions(WebDriverTasks.driver);
        actions.click(WebDriverTasks.waitForElement(uploadButton)).build().perform();
        setFilePathToFileExplorer(documentPath);
    }

    /**
     * This method will get no of records available in documents table
     * Created By : Venkat
     *
     * @return Integer This is records count
     */
    public static int getNoOfDocumentRecords() {
        if (!WebDriverTasks.verifyIfElementIsDisplayed(documentRecords, 5))
            return 0;
        return WebDriverTasks.waitForElements(documentRecords).size();
    }

    /**
     * This method will compare actual document status value with expected file name
     * Created By : Venkat
     *
     * @param status This is expected document status
     * @return boolean If actual value matches with expected value it will return True else False
     */
    public static boolean verifyDocumentStatusIs(String status) {
        String actualDocumentStatus = WebDriverTasks.waitForElement(documentStatusText).getText().trim();
        return status.equalsIgnoreCase(actualDocumentStatus);
    }

    /**
     * This method will compare actual document type value with expected file name
     * Created By : Venkat
     *
     * @param documentType This is expected document type
     * @return boolean If actual value matches with expected value it will return True else False
     */
    public static boolean verifyDocumentTypeIs(String documentType) {
        String actualDocumentType = WebDriverTasks.waitForElement(documentTypeText).getText().trim();
        return documentType.equalsIgnoreCase(actualDocumentType);
    }

    /**
     * This method will compare actual document category value with expected file name
     * Created By : Venkat
     *
     * @param documentCategory This is expected document category
     * @return boolean If actual value matches with expected value it will return True else False
     */
    public static boolean verifyDocumentCategoryIs(String documentCategory) {
        String actualDocumentCategory = WebDriverTasks.waitForElement(documentCategoryText).getText().trim();
        return documentCategory.equalsIgnoreCase(actualDocumentCategory);
    }

    /**
     * This method will compare actual notes value with expected file name
     *
     * @param notes This is expected notes text
     * @return boolean If actual value matches with expected value it will return True else False
     */
    public static boolean verifyNotesIs(String notes) {
        String actualNotes = WebDriverTasks.waitForElement(notesText).getText().trim();
        return notes.equalsIgnoreCase(actualNotes);
    }

    /**
     * This method will compare actual modified by value with expected file name
     * Created By : Venkat
     *
     * @param modifiedBy This is expected modified by value
     * @return boolean If actual value matches with expected value it will return True else False
     */
    public static boolean verifyModifiedByIs(String modifiedBy) {
        String actuallyModifiedBy = WebDriverTasks.waitForElement(modifiedByText).getText().trim();
        return modifiedBy.equalsIgnoreCase(actuallyModifiedBy);
    }

    /**
     * This method will compare actual file name with expected file name
     * Created By : Venkat
     *
     * @param filePath This is file path for the file which is uploaded
     * @return boolean If actual value matches with expected value it will return True else False
     */
    public static boolean verifyFileNameIs(String filePath) {
        File file = new File(filePath);
        String expectedFileName = file.getName();
        String actualFileName = WebDriverTasks.waitForElement(uploadedFileLink).getText().trim();
        return expectedFileName.equalsIgnoreCase(actualFileName);
    }

    public static void clickOnUploadedFileLink(){
        WebDriverTasks.waitForElement(uploadedFileLink).click();
    }

    /**
     * This method will delete all documents records
     * Created By : Venkat
     */
    public static void deleteAllDocuments() {
        WebDriverTasks.waitForAngular();
        while (WebDriverTasks.verifyIfElementIsDisplayed(deleteFirstDocument, 5)) {
            WebDriverTasks.waitForElement(deleteFirstDocument).click();
            WebDriverTasks.wait(3);
            WebDriverTasks.waitForElement(confirmDeletion).click();
            WebDriverTasks.wait(3);
            WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
            WebDriverTasks.waitForAngular();
        }
    }

    /**
     * This method will delete docuemtn record for provided row number
     * Created By : Venkat
     *
     * @param rowNo This is row number and it starts from 0
     */
    public static void deleteDocument(int rowNo) {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(deleteDocuments, 1))
            return;
        List<WebElement> allDeleteIcons = WebDriverTasks.waitForElements(deleteDocuments);
        assert rowNo <= allDeleteIcons.size() - 1;
        if (!allDeleteIcons.get(rowNo).isDisplayed())
            return;
        allDeleteIcons.get(rowNo).click();
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(confirmDeletion).click();
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
    }

    /**
     * This method will click on Edit button for the first record
     * Created By : Venkat
     */
    public static void clickOnEditDocument() {
        WebDriverTasks.waitForElement(editFirstDocument).click();
    }

    /**
     * This method will save parameter text to clipboard
     * Created By : Venkat
     * @param text Text to be saved in clipboard
     */
    public static void saveToClipBoard(String text){
        StringSelection stringSelection = new StringSelection(text);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection,null);
    }

    /**
     * This method will copy file path to clipboard and then paste it into file explorer and hit enter to upload the file
     * Created By : Venkat
     * @param filePath This is target file path
     * @throws AWTException AWT exception
     */
    public static void setFilePathToFileExplorer(String filePath) throws AWTException {
        assert new File(filePath).exists();
        WebDriverTasks.wait(5);
        saveToClipBoard(filePath);
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        WebDriverTasks.wait(10);
    }

    /**
     * This method will add a document record if empty
     * Created By : Venkat
     * @throws IOException IO Exception
     * @throws AWTException AWT exception
     */
    public static void addDocumentRecordIfEmpty() throws IOException, AWTException {
        Actions actions = new Actions(WebDriverTasks.driver);
        int existingCount = getNoOfDocumentRecords();
        if(existingCount!=0)
            return;

        //Create New File
        String filePath = Paths.get("target", RandomString.make(5) + ".pdf").toAbsolutePath().toString();
        Helpers.createEmptyFile(filePath);

        WebDriverTasks.waitForElement(documentAvailableWithoutCopies).click();
        WebDriverTasks.waitForElement(optionGreenCard).click();
        WebDriverTasks.waitForElement(goButton).click();
//        actions.click(WebDriverTasks.waitForElement(firstDocumentUploadButton)).build().perform();
//        setFilePathToFileExplorer(filePath);
        WebDriverTasks.waitForElement(saveButton).click();
    }
}