package nycnet.dhs.streetsmart.pages.clientdetails;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class ClientContactsTab extends StartWebDriver {

	static By clickOnClientContactsTab = By.xpath("//a[@id='contacttab']");
	static By selectContactType = By.xpath("//*[@ng-model='$parent.ClientContactTypeId']");
	static By clickOnAddNewButton = By.xpath("//*[@ng-click='AddClientContact()']");
	static By enterEmailAddress = By.xpath("//*[@ng-model='ClientContact.Email']");
	static By enterPhoneNumber = By.xpath("//*[@ng-model='ClientContact.Phone']");
	static By clickOnSave = By.xpath("//*[@ng-click='SaveClientContact()']");
	static By editButton = By.xpath("//*[@ng-click='EditClientContact({})']");
	
	//detox locators
	static By clientContactDetoxDropDown = By.id("detoxCenterProgramSearch");
	
	//grid locators
	static By verifyContactType = By.xpath("//*[@role='grid']/*[@role='rowgroup'][@class='ui-grid-viewport ng-isolate-scope']"
			+ "/*[@class='ui-grid-canvas']/*[@class='ui-grid-row ng-scope']/*[@role='row']/*[@role='gridcell'][2]");
	
	static By emailAddressOnContactGrid = By.xpath("//*[@role='grid']/*[@role='rowgroup'][@class='ui-grid-viewport ng-isolate-scope']"
			+ "/*[@class='ui-grid-canvas']/*[@class='ui-grid-row ng-scope']/*[@role='row']/*[@role='gridcell'][5]");
	static By noRecordsOnGrid = By.xpath("//*[@ng-show='clientContactGrid_GridOptions.data.length == 0']");
	
	static By actionButton = By.linkText("Action");
    static By viewDetails = By.linkText("View Details");
    static By deleteOption = By.linkText("Delete");
    static By confirmButton = By.xpath("//button[@class='confirm']");
    
	//notes locators
	static By clickOnNotesButton = By.xpath("//a[@id='notes-accordion']//i[@class='fa fa-plus-circle pull-right']");
	static By clickOnAddNewNote = By.xpath("//a[@class='btn btn-sm btn-default addnew m-b-5'][contains(text(),'Add New')]");
	static By enterNoteText = By.xpath("//*[@ng-model='CurrentClientNote.Notes']");
	static By saveNote = By.xpath("//i[@ng-click='SaveClientNote(CurrentClientNote)']");
	static By verifyNote = By.xpath("//td[@class='pre-wrap-apply ng-binding']");

	
	
	public static void clickOnClientContactsTab() {
		WebDriverTasks.waitForElement(clickOnClientContactsTab).click();
	}
	
	public static void selectContactType (String contacttype) {
		WebDriverTasks.waitForElementAndSelect(selectContactType).selectByVisibleText(contacttype);
	}
	
	public static void clickOnAddNewButton() {
		WebDriverTasks.waitForElement(clickOnAddNewButton).click();
	}
	
	public static void clearEmailAddress() {
		WebDriverTasks.waitForElement(enterEmailAddress).clear();
	}
	
	public static void enterEmailAddress(String emailaddress) {
		WebDriverTasks.waitForElement(enterEmailAddress).sendKeys(emailaddress);
	}
	
	public static void enterPhoneNumber(String phonenumber) {
		WebDriverTasks.waitForElement(enterPhoneNumber).sendKeys(phonenumber);
	}	
	
	public static void clickOnSaveButton() {
		WebDriverTasks.waitForElement(clickOnSave).click();
	}
	
	public static void clickOnEditButton() {
		WebDriverTasks.waitForElement(editButton).click();
	}
	
	public static String verifyContactType() {
		String contactTypeText = WebDriverTasks.waitForElement(verifyContactType).getText();
		System.out.println("Record created with Contact Type as "+ contactTypeText);
		return contactTypeText;
	}
	
	public static String returnEmailAddressOnContactGrid() {
		String text = WebDriverTasks.waitForElement(emailAddressOnContactGrid).getText();
		System.out.println("The value of the email address on grid is  "+ text);
		return text;
	}
	
	public static String returnNoRecordsOnGrid() {
		String text = WebDriverTasks.waitForElement(noRecordsOnGrid).getText();
		System.out.println("The value is  "+ text);
		return text;
	}
	
	public static void clickOnActionButton() {
		WebDriverTasks.waitForElement(actionButton).click();
	}
	
	public static void clickOnViewDetailsButton() {
		WebDriverTasks.waitForElement(viewDetails).click();
	}
	
	public static void clickOnDeleteButton() {
		WebDriverTasks.waitForElement(deleteOption).click();
	}
	
	public static void clickOnConfirmButton() {
		WebDriverTasks.waitForElement(confirmButton).click();
	}
	
	//Detox contact options
	public static void clearClientContactDetoxDropDown() {
		WebDriverTasks.waitForElement(clientContactDetoxDropDown).clear();
	}

	public static void selectClientContactDetoxDropDown(String detox) {
		WebDriverTasks.waitForElement(clientContactDetoxDropDown).sendKeys(detox);
		//WebDriverTasks.waitForElement(clientContactDetoxDropDown).sendKeys(Keys.ARROW_DOWN);
		WebDriverTasks.waitForElement(clientContactDetoxDropDown).sendKeys(Keys.DOWN);
		WebDriverTasks.waitForElement(clientContactDetoxDropDown).sendKeys(Keys.ENTER);
	}
	
	//Notes actions
	public static void clickOnNotesButton() {
		WebDriverTasks.waitForElement(clickOnNotesButton).click();
	}
	
	public static void clickOnAddNewNote() {
		WebDriverTasks.waitForElement(clickOnAddNewNote).click();
	}
	
	public static void enterNotesText(String notetext) {
		WebDriverTasks.waitForElement(enterNoteText).sendKeys(notetext);
	}
	
	public static void clickSaveNote () {
		WebDriverTasks.waitForElement(saveNote).click();
	}
	
	public static String verifyNoteText() {
		String verifyNoteText = WebDriverTasks.waitForElement(verifyNote).getText();
		System.out.println("Note created as:"+verifyNoteText);
		return verifyNoteText;
	}
	

	
	
	
	
	
	
	
}