package nycnet.dhs.streetsmart.pages.clientdetails;

import nycnet.dhs.streetsmart.pages.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.testng.Assert;

import java.util.List;

public class HealthInformationTab extends StartWebDriver {

    static By clickOnHealthInformationTab = By.xpath("//li/a[contains(text(),'Health')]");
    static By clickOnAddNewButton = By.xpath("//*[@ng-click='AddClientHealthInfo()']");
    static By selectProvider = By.xpath("//*[@ng-model='ClientHealthInfo.ProviderId']");
    static By selectMedicalCoverage = By.xpath("//*[@ng-model='ClientHealthInfo.MedicalCoverageStatusId']");
    static By selectTypeOfCoverage = By.xpath("//*[@ng-model='ClientHealthInfo.MedicalCoverageId']");
    static By selectPsychosocialStatus = By.xpath("(//*[@ng-model='assessment.AssessmentScreeningStatusId'])[1]");
    static By selectPsychEvaluationStatus = By.xpath("(//*[@ng-model='assessment.AssessmentScreeningStatusId'])[2]");
    static By clickOnSaveHealthInformationButton = By.xpath("(//*[@id='cint-info-save'])[6]");

    static By providerName = By.xpath("(//div[@ng-bind='Client.ClientProvider.Source'])[1]");

    static By returnHealthInformationMedicalRecordNumber = By.cssSelector("span[title='MR52307']");
    static By clickOnClientDetailsButton = By.xpath("//button[@class='btn btn-sm btn-default m-r-10 m-b-5']");
    static By returnClientDetailsTitle = By.xpath("//h3[@class='popover-title']");

    static By abuseDetailsSection = By.xpath("//b[normalize-space()='Medical, Mental and Substance Abuse Details' and @aria-expanded='false']");
    static By medicationSection = By.xpath("//b[normalize-space()='Medications, Allergies and Vaccines' and @aria-expanded='false']");
    static By mentalIllness = By.xpath("//select[@ng-model='ClientHealthInfo.DiagnosisId']");
    static By medicationInfo = By.xpath("//select[@ng-model='ClientHealthInfo.MedicineStatusId']");
    static By medications = By.xpath("//input[@ng-model='ClientHealthInfo.Medication']");

    static By labelLocators = By.xpath("//div/table/tbody/tr/td/table/tbody/tr/td[text() and @class]");
    static By completedStatusSelector = By.xpath("(//div[contains(@class,'table-vac')]/table/tbody/tr/td)[2]//select");
    static By completionDateSelector = By.xpath("(//div[contains(@class,'table-vac')]/table/tbody/tr/td)[3]//input");
    static By resultFields = By.xpath("(//div[contains(@class,'table-vac')]/table/tbody/tr/td)[4]//td");
    static By followupSelector = By.xpath("(//div[contains(@class,'table-vac')]/table/tbody/tr/td)[5]//select");
    static By followupDateSelector = By.xpath("(//div[contains(@class,'table-vac')]/table/tbody/tr/td)[6]//input");
    static By followupTimeFrom = By.xpath("(//div[contains(@class,'table-vac')]/table/tbody/tr/td)[7]//input[@ng-model='assessment.FollowUpTimeFrom']");
    static By followupTimeTo = By.xpath("(//div[contains(@class,'table-vac')]/table/tbody/tr/td)[7]//input[@ng-model='assessment.FollowUpTimeTo']");

    static By healthInfoActionButton = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:first-child>div>a");

    static By deleteButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='Delete']");
    static By confirmDeleteButton = By.xpath("//button[text()='Yes, delete it!']");
    static By closeRecordDeletionConfirmationMessage = By.xpath("//button[text()='Ok']");

    static By healthInformationRecords = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']");

    static By t_provider = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(2)>div>span:nth-child(1)");
    static By t_medicalRecord = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(2)>div>span:nth-child(1)");
    static By t_healthCoverage = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(3)>div>span:nth-child(1)");
    static By t_coverageType = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(3)>div>span:nth-child(3)");
    static By t_modifiedby = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(9)>div>span:nth-child(1)");
    static By t_illnessAndAbuse = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:nth-child(4)>div>span:nth-child(1)");

    static By actionButton = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:first-child>div>a");
    static By viewCaseInformationButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='View Details']");

    static String generalLabelText = "//div[not(contains(@class,'ng-hide'))]/form//div[@class='background-view']//label[normalize-space()='%s:']/following-sibling::div/label";

    static By editHealthInformation = By.xpath("//button[@ng-click='EditHealthDetails()']");
    static By cancelHealthInformation = By.xpath("//button[@ng-click='CancelHealthInfoSaveConfirmed()']");

    public static String returnClientHealthInformationTabHeader() {
        String clientHealthInformationTabHeaderText = WebDriverTasks.waitForElement(clickOnHealthInformationTab)
                .getText();
        System.out.println("The page header is " + clientHealthInformationTabHeaderText);
        return clientHealthInformationTabHeaderText;
    }

    public static void clickOnHealthInformationTab() {
        WebDriverTasks.waitForElement(clickOnHealthInformationTab).click();
    }

    public static void clickOnAddNewButton() {
        WebDriverTasks.waitForElement(clickOnAddNewButton).click();
    }

    public static void selectProvider(String provider) {
        WebDriverTasks.waitForElementAndSelect(selectProvider).selectByVisibleText(provider);
    }

    public static void selectMedicalCoverage(String medicalcoverage) {
        WebDriverTasks.waitForElementAndSelect(selectMedicalCoverage).selectByVisibleText(medicalcoverage);
    }

    public static void selectTypeofCoverage(String typeofcoverage) {
        WebDriverTasks.waitForElementAndSelect(selectTypeOfCoverage).selectByVisibleText(typeofcoverage);
    }

    public static void selectPsychosocialStatus(String psychosocialstatus) {
        WebDriverTasks.waitForElementAndSelect(selectPsychosocialStatus).selectByVisibleText(psychosocialstatus);
    }

    public static void selectPsychEvaluationStatus(String psychevaluationstatus) {
        WebDriverTasks.waitForElementAndSelect(selectPsychEvaluationStatus).selectByVisibleText(psychevaluationstatus);
    }

    public static void clickSaveHealthInformationButton() {
        WebDriverTasks.waitForElement(clickOnSaveHealthInformationButton).click();
    }

    public static void clickClientDetailsButton() {
        WebDriverTasks.waitForElement(clickOnClientDetailsButton).click();
    }

    public static String returnClientDetailsTitle() {
        String clientDetialsTitleText = WebDriverTasks.waitForElement(returnClientDetailsTitle).getText();
        System.out.println(clientDetialsTitleText + " accessed on Health Information Page");
        return clientDetialsTitleText;
    }

    public static String returnMedicalRecordNumber() {
        String medicalRecordNumberText = WebDriverTasks.waitForElement(returnHealthInformationMedicalRecordNumber).getText();
        System.out.println(medicalRecordNumberText + " is seen as Medical Record");
        return medicalRecordNumberText;
    }

    //

    /**
     * This method will provide assessment screening details.
     * It will only feed details for non-empty values.
     * Created By : Venkat
     *
     * @param type              This is type for which details will be added
     * @param completed         This is completed status value for the dropdown
     * @param completionDate    This is completion date for date input field
     * @param result            This is result value, it can be an input box or dropdown
     * @param followup          This is followup dropdown value
     * @param followupDate      This is followup date value
     * @param followupSTartTime This is followup start time
     * @param followupEndTime   This is followup end time
     */
    public static void submitAssessmentAndScreeningDetails(String type, String completed, String completionDate, String result, String followup, String followupDate, String followupSTartTime, String followupEndTime) {

        int rowNo = getRowNumberForType(type);
        if (rowNo == -1)
            Assert.fail("Can not find any row with type text : " + type);
        selectCompletedStatus(rowNo, completed);
        selectCompletionDate(rowNo, completionDate);
        selectResult(rowNo, result);
        selectFollowup(rowNo, followup);
        selectFollowupDate(rowNo, followupDate);
        selectFollowupTimes(rowNo, followupSTartTime, followupEndTime);
    }

    /**
     * This method will get row number for provided type value, row no starts from 0.
     * Created By : Venkat
     *
     * @param type This is type value
     * @return Integer This is row number for provided type label
     */
    public static int getRowNumberForType(String type) {
        List<WebElement> typeLabels = WebDriverTasks.waitForElements(labelLocators);
        int i = 0;
        for (i = 0; i < typeLabels.size() - 1; i++) {
            if (typeLabels.get(i).getText().equals(type))
                return i;
        }
        return -1;
    }

    /**
     * This method will select completed status for provided row number
     * Created By : Venkat
     *
     * @param rowNo This is row number
     * @param value This is completed status value to be selected
     */
    public static void selectCompletedStatus(int rowNo, String value) {
        if (value.isEmpty())
            return;
        List<WebElement> completedStatusDropdowns = WebDriverTasks.waitForElements(completedStatusSelector);
        new Select(completedStatusDropdowns.get(rowNo)).selectByVisibleText(value);
    }

    /**
     * This method will select completion date for provided row number
     * Created By : Venkat
     *
     * @param rowNo This is row number
     * @param date  This is date to be selected
     */
    public static void selectCompletionDate(int rowNo, String date) {
        if (date.isEmpty())
            return;
        List<WebElement> completionDateSelectors = WebDriverTasks.waitForElements(completionDateSelector);
        Base.selectDate(completionDateSelectors.get(rowNo), date);
    }

    /**
     * This method will select Result value. Result field can be an input box ot can be a dropdown.
     * This method will first check for dropdown, if not found then it will go for input box.
     * Created By : Venkat
     *
     * @param rowNo  This is target row number
     * @param result This is result value to be provided/selected
     */
    public static void selectResult(int rowNo, String result) {
        if (result.isEmpty())
            return;
        rowNo = rowNo + 1;
        List<WebElement> resultFieldElements = WebDriverTasks.waitForElements(resultFields);
        if (resultFieldElements.get(rowNo).findElements(By.tagName("select")).size() == 1) {
            new Select(resultFieldElements.get(0)).selectByVisibleText(result);
        } else {
            resultFieldElements.get(rowNo).findElement(By.tagName("input")).sendKeys(result);
        }
    }

    /**
     * This method will select followup value
     * Created By : Venkat
     *
     * @param rowNo This is target row number
     * @param value This is followup value to be selected
     */
    public static void selectFollowup(int rowNo, String value) {
        if (value.isEmpty())
            return;
        List<WebElement> followupDropdowns = WebDriverTasks.waitForElements(followupSelector);
        new Select(followupDropdowns.get(rowNo)).selectByVisibleText(value);
    }

    /**
     * This method will select followup date for target row number
     * Created By : Venkat
     *
     * @param rowNo        This is target row number
     * @param followupDate This is followup date to be selected
     */
    public static void selectFollowupDate(int rowNo, String followupDate) {
        if (followupDate.isEmpty())
            return;
        List<WebElement> followupDates = WebDriverTasks.waitForElements(completionDateSelector);
        Base.selectDate(followupDates.get(rowNo), followupDate);
    }

    /**
     * This method will select followup time from and followup time to values for target row
     * Created By : Venkat
     *
     * @param rowNo    This is row number
     * @param timeFrom This is time from
     * @param timeTo   This is time to
     */
    public static void selectFollowupTimes(int rowNo, String timeFrom, String timeTo) {
        if (timeFrom.isEmpty() || timeTo.isEmpty())
            return;
        List<WebElement> timeFromFields = WebDriverTasks.waitForElements(followupTimeFrom);
        List<WebElement> timeToFields = WebDriverTasks.waitForElements(followupTimeTo);
        timeFromFields.get(rowNo).sendKeys(timeFrom);
        timeToFields.get(rowNo).sendKeys(timeTo);
    }

    /**
     * This method will select dropdown value for field Mental illness and substance abuse
     * Created By : Venkat
     *
     * @param type This is type value to be selected
     */
    public static void selectMentalIllnessAndSubstanceAbuse(String type) {
        WebDriverTasks.waitForElementAndSelect(mentalIllness).selectByVisibleText(type);
    }

    /**
     * This method will click on abuse detail section to expand it if unexpanded
     * Created By : Venkat
     */
    public static void expandAbuseDetailsSection() {
        if (WebDriverTasks.verifyIfElementIsDisplayed(abuseDetailsSection, 2))
            WebDriverTasks.waitForElement(abuseDetailsSection).click();
    }

    /**
     * This method will click on Medication section to expand it if unexpanded
     * Created By : Venkat
     */
    public static void expandMedicationSection() {
        if (WebDriverTasks.verifyIfElementIsDisplayed(medicationSection, 2))
            WebDriverTasks.waitForElement(medicationSection).click();
    }

    /**
     * This method will select medical information dropdown value
     * Created By : Venkat
     *
     * @param medicalInformation This is dropdown value to be selected
     */
    public static void selectMedicalInformation(String medicalInformation) {
        if (WebDriverTasks.verifyIfElementIsDisplayed(medicationInfo, 2))
            WebDriverTasks.waitForElementAndSelect(medicationInfo).selectByVisibleText(medicalInformation);
    }

    /**
     * This method will set medication value
     * Created By : Venkat
     *
     * @param medication This is medication value to be provided
     */
    public static void setMedicationText(String medication) {
        WebDriverTasks.waitForElement(medications).sendKeys(medication);
    }


    /**
     * This method will delete all Health Information records
     * Created By : Venkat
     */
    public static void deleteAllHealthInfoRecords() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(healthInfoActionButton, 3))
            return;
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(healthInfoActionButton);
        for (int i = actionButtons.size() - 1; i >= 0; i--) {
            deleteHealthRecord(i);
        }
    }

    /**
     * This method will delete Health Information record for provided row number
     * Created By : Venkat
     *
     * @param rowNo This is row number for which we want to delete, row number starts from 0
     */
    public static void deleteHealthRecord(int rowNo) {
        WebDriverTasks.waitForAngular();
        WebDriverTasks.scrollToTop();
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(healthInfoActionButton);
        assert rowNo <= actionButtons.size();
        actionButtons.get(rowNo).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(deleteButton).click();
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(confirmDeleteButton).click();
        WebDriverTasks.wait(3);
        if (WebDriverTasks.verifyIfElementIsDisplayed(closeRecordDeletionConfirmationMessage, 5))
            WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
    }

    /**
     * This method will get number of health information records
     * Created By : Venkat
     *
     * @return Integer This is count of health information records
     */
    public static int getNoOfRecords() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(healthInformationRecords, 5))
            return 0;
        return WebDriverTasks.waitForElements(healthInformationRecords).size();
    }

    /**
     * This method will get Provider name from table for first record
     * Created By : Venkat
     *
     * @return String This is provider name
     */
    public static String getProviderFromTable() {
        return WebDriverTasks.waitForElement(t_provider).getText().trim();
    }

    /**
     * This method will get medical record number from table for first record
     * Created By : Venkat
     *
     * @return String This is medical record number
     */
    public static String getMedicalRecordNoFromTable() {
        return WebDriverTasks.waitForElement(t_medicalRecord).getText().trim();
    }

    /**
     * This method will return coverage type from health information table for first record
     * Created By : Venkat
     *
     * @return String This is coverage type
     */
    public static String getCoverageType() {
        return WebDriverTasks.waitForElement(t_coverageType).getText().trim();
    }

    /**
     * This method will get health coverage from health information table for first record
     * Created By : Venkat
     *
     * @return String This is medical coverage value
     */
    public static String getHealthCoverageFromTable() {
        return WebDriverTasks.waitForElement(t_healthCoverage).getText().trim();
    }

    /**
     * This method will get Illness And Abuse value from health information table for first record
     * Created By : Venkat
     *
     * @return String This is Illness and abuse value
     */
    public static String getIllnessAndAbuseFromTable() {
        return WebDriverTasks.waitForElement(t_illnessAndAbuse).getText().trim();
    }

    /**
     * This method will get modified by value from health information table for first record
     * Created By : Venkat
     *
     * @return String This is modified by value
     */
    public static String getModifiedByFromTable() {
        return WebDriverTasks.waitForElement(t_modifiedby).getText().trim();
    }

    /**
     * This method will click on Action an then View Details button
     * Created By : Venkat
     *
     * @param rowNo This is row number for which we want to view details, row number starts from 0
     */
    public static void viewHealthInformationRecord(int rowNo) {
        WebDriverTasks.scrollToTop();
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(actionButton);
        assert rowNo <= actionButtons.size();
        WebDriverTasks.wait(3);
        actionButtons.get(rowNo).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(viewCaseInformationButton).click();
        WebDriverTasks.wait(3);
    }

    /**
     * This method will get saved value from health information form for provided label/field name
     * Created By : Venkat
     *
     * @param label This is label name/field name for which value is to be selected
     * @return String This is value for given field
     */
    public static String getTextForField(String label) {
        By element = By.xpath(String.format(generalLabelText, label));
        return WebDriverTasks.waitForElement(element).getText().trim();
    }

    /**
     * This method will click on Edit button to edit Health Information field
     * Created By : Venkat
     */
    public static void clickOnEditButton() {
        WebDriverTasks.waitForElement(editHealthInformation).click();
    }

    /**
     * This method will click on canel button on health information details page
     * Created By : Venkat
     */
    public static void clickOnCancelButton() {
        WebDriverTasks.waitForElement(cancelHealthInformation).click();
    }

    /**
     * This method will return provider name from Client Information tab
     * Created By : Venkat
     *
     * @return String This is provider name
     */
    public static String getProviderName() {
        return WebDriverTasks.waitForElement(providerName).getText();
    }
}
