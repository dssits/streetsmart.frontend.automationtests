package nycnet.dhs.streetsmart.pages.clientdetails;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class RentalSubsidyTab extends StartWebDriver {

	static By clickOnRentalSubsidyTab = By.xpath("//ul[@class='nav nav-tabs']/li[10]");
	static By verifyActiveSubsidiesButton = By.xpath("//button[@class='btn btn-success m-r-5 m-b-5']");
	static By verifyInactiveSubsidiesButton = By.xpath("//button[@class='btn btn-default gray m-r-5 m-b-5']");
	static By verifyWithdrawnSubsidiesButton = By.xpath("//button[@class='btn btn-danger m-r-5 m-b-5']");
	static By verifyPendingSubsidiesButton = By.xpath("//button[@class='btn btn-inverse m-r-5 m-b-5']");
	static By verifyApplyingButton = By.xpath("//button[@class='btn btn-primary lightBlue m-r-5 m-b-5']");
	static By verifyEligibilityDeterminedButton = By.xpath("//button[contains(text(),'Eligibility Determined')]");
	
	static By clickOnClientDetailsButton = By.xpath("//button[@class='btn btn-default m-r-5 m-b-5']");
	static By returnClientDetailsTitle = By.xpath("//h3[@class='popover-title']");
	
	static By clickOnAddSubsidies = By.xpath("//button[@id='btn-add-subsidies']");
	static By clickOnAddVashSubsidy = By.xpath("(//i[@class='fa fa-pencil-square-o'])[1]");
	static By selectVashStatus = By.xpath("//*[@ng-model='ClientSubsidies.VASHInfo.SubsidyStatusId']");
	static By clickOnSaveSubsidy = By.xpath("//*[@ng-click='SaveSubsidy(5)']");
	static By clickOnSaveSubsidyGenPop = By.xpath("//*[@ng-click='SaveSubsidy(1)']");
	static By clickOnViewMySubsidy = By.xpath("//*[@ng-click='IntializeDefaultSubsidy(0)']");
	static By verifyVashSubsidy = By.xpath("/html[1]/body[1]/div[2]/div[3]/div[2]/div[1]/div[7]/div[1]/div[1]/div[3]/div[7]/div[1]/div[1]/h4[1]");
	
	static By vashSubsidyStatus = By.xpath("//label[@ng-bind='FormatLabelControl(ClientSubsidies.VASHInfo.SubsidyStatus)']");
	
	static By clickOnLetterHistory = By.xpath("//a[@id='linc-letter-accordion']//i[contains(@class,'fa fa-plus-circle pull-right')]");
	
	//Edit Vash subsidies
	static By editVashSubsidy = By.xpath("//*[@ng-click='EditSubsidy(5,ClientSubsidies.VASHInfo.SubsidyStatusId);SyncDates(5);']");
	static By vashSubsidyEditDateSubmitted = By.xpath("//*[@ng-model='ClientSubsidies.VASHInfo.SubmittedDate']");
	static By vashSubsidyEditReceivedDate = By.xpath("//*[@ng-model='ClientSubsidies.VASHInfo.ReceivedDate']");
	static By vashSubsidyEditCertificateDate = By.xpath("//*[@ng-model='ClientSubsidies.VASHInfo.CertificationDate']");
	static By vashSubsidyEditExpirationDate = By.xpath("//*[@ng-model='ClientSubsidies.VASHInfo.ExpirationDate']");
	static By documentRedirectConfirmationNo = By.xpath("//*[@ng-click='cancel()']");
	
	//View Vash subsidies
	static By vashSubsidyViewDateSubmitted = By.xpath("//*[@ng-bind='FormatLabelControl(ClientSubsidies.VASHInfo.SubmittedDate_String)']");
	static By vashSubsidyViewReceivedDate = By.xpath("//*[@ng-bind='FormatLabelControl(ClientSubsidies.VASHInfo.ReceivedDate_String)']");
	static By vashSubsidyViewCertificateDate = By.xpath("//*[@ng-bind='FormatLabelControl(ClientSubsidies.VASHInfo.CertificationDate_String)']");
	static By vashSubsidyViewExpirationDate = By.xpath("//*[@ng-bind='FormatLabelControl(ClientSubsidies.VASHInfo.ExpirationDate_String)']");
	
	//Add Gen pop
	static By clickOnAddGenPopSubsidy = By.xpath("//*[@ng-click='EditSubsidy(1,ClientSubsidies.GenPopInfo.SubsidyStatusId);SyncDates(1);']");
	static By selectGenPopSubsidy = By.xpath("//*[@ng-model='ClientSubsidies.GenPopInfo.SubsidyStatusId']");
	static By addGenPopDateSubmitted = By.xpath("//*[@ng-model='ClientSubsidies.GenPopInfo.SubmittedDate']");
	
	//Edit Gen pop
	
	static By editGenPopDateSubmitted = By.xpath("//*[@ng-model='ClientSubsidies.GenPopInfo.SubmittedDate']");
	
	//View Gen pop
	//static By viewGenPopSubsidyStatus = By.xpath("//*[@ng-model='ClientSubsidies.GenPopInfo.SubsidyStatusId']");
	static By viewGenPopSubsidyStatus = By.xpath("//*[@ng-bind='FormatLabelControl(ClientSubsidies.GenPopInfo.SubsidyStatus)']");
	static By viewGenPopSubmittedDate = By.xpath("//*[@ng-bind='FormatLabelControl(ClientSubsidies.GenPopInfo.SubmittedDate_String)']");
	
	//Notes
			static By selectNotesCollapseIcon = By
					.xpath("//a[@id='notes-accordion']//i[@class='fa fa-plus-circle pull-right']");
			static By clickOnAddNewNotes = By
					.xpath("//a[@class='btn btn-sm btn-default addnew m-b-5'][contains(text(),'Add New')]");
			static By inputNotesText = By.xpath("//*[@ng-model='CurrentClientNote.Notes']");
			static By inputNotesTextOnEdit = By.xpath("//textarea[@ng-model='item.Notes']");
			static By selectSaveNotes = By.xpath("//*[@ng-click='SaveClientNote(CurrentClientNote)']");
			static By selectSaveNotesOnEdit = By.xpath("//*[@ng-click='SaveClientNote(item)']");
			static By noRecordFoundOnNotesGrid = By.xpath("//*[@ng-show='(!ClientNotesList || ClientNotesList.length < 1) && TopNotesRowMode != utils.constant.UIViewMode.Add']");
			static By firstRecordNoteTextOnNotesGrid = By.xpath("//tr[@pagination-id='clientNotes']/td[@class='pre-wrap-apply ng-binding']");
			static By EditfirstRecordOnNotesGrid = By.xpath("//i[@ng-click='EditClientNote(item)']");
			static By DeletefirstRecordOnNotesGrid = By.xpath("//i[@ng-click='DeleteClientNote(item)']");
			static By ConfirmDeleteRecordOnNotesGrid = By.xpath("//button[@ng-click='ok()']");
			
			//Changelog
			static By changelogGoalText = By.cssSelector("div[role='grid']>div[role='rowgroup']>div[class='ui-grid-canvas']>div:nth-child(1)>div>div[role='gridcell']:nth-child(8)>div>span:nth-child(3)");
			static By expandChangeLogIcon = By.cssSelector("a.collapse-panel[id='change-log-accordion']");
			static By changeLogGridNoRecordsFound = By.xpath("(//*[@ng-show='changeLogGridList_GridOptions.data.length == 0'])[1]");
			
	static By consentToCaseLoad = By.xpath("//*[@ng-model='Client.ClientEnrollment.IsCaseloadConsented']");
	
	
	public static void clickOnRentalSubsidyTab() {
		WebDriverTasks.waitForElement(clickOnRentalSubsidyTab).click();
	}
	
	public static void verifyActiveSubsidiesButton () {
		WebDriverTasks.waitForElement(verifyActiveSubsidiesButton).click();
		System.out.println("Active Subsidies Button Clicked");
	} 
	
	public static void verifyInactiveSubsidiesButton () {
		WebDriverTasks.waitForElement(verifyInactiveSubsidiesButton).click();
		System.out.println("Inactive Subsidies Button Clicked");
	}
	
	public static void verifyWithdrawnSubsidiesButton () {
		WebDriverTasks.waitForElement(verifyWithdrawnSubsidiesButton).click();
		System.out.println("Withdrawn Subsidies Button Clicked");
	}
	
	public static void verifyPendingSubsidiesButton () {
		WebDriverTasks.waitForElement(verifyPendingSubsidiesButton).click();
		System.out.println("Pending Subsidies Button Clicked");
	}
	
	public static void verifyApplyingButton () {
		WebDriverTasks.waitForElement(verifyApplyingButton).click();
		System.out.println("Applying Button Clicked");
	}
	
	public static void verifyEligibilityDeterminedButton () {
		WebDriverTasks.waitForElement(verifyEligibilityDeterminedButton).click();
		System.out.println("Eligibility Determined Button Clicked");
	}
	
	public static void clickClientDetailsButton() {
		WebDriverTasks.waitForElement(clickOnClientDetailsButton).click();
	}

	public static String returnClientDetailsTitle() {
		String clientDetialsTitleText = WebDriverTasks.waitForElement(returnClientDetailsTitle).getText();
		System.out.println(clientDetialsTitleText + " accessed on Rental Subsidy Tab");
		return clientDetialsTitleText;
	}
	
	public static void clickOnAddSubsidies() {
		WebDriverTasks.waitForElement(clickOnAddSubsidies).click();
	}
	
	public static void clickOnAddVashSubsidy() {
		WebDriverTasks.waitForElement(clickOnAddVashSubsidy).click();
	}
	
	public static void selectVashStatus(String vashstatus) {
		WebDriverTasks.waitForElementAndSelect(selectVashStatus).selectByVisibleText(vashstatus);
	}
	
	public static void clickOnSaveSubsidy() {
		WebDriverTasks.waitForElement(clickOnSaveSubsidy).click();
	}
	
	public static void clickOnSaveSubsidyForGenPopOnEdit() {
		WebDriverTasks.waitForElement(clickOnSaveSubsidyGenPop).click();
	}
	
	public static void clickOnViewMySubsidy() {
		WebDriverTasks.waitForElement(clickOnViewMySubsidy).click();
	}
	
	public static String verifyVashSubsidy() {
		String vashSubsidyText = WebDriverTasks.waitForElement(verifyVashSubsidy).getText();
		System.out.println(vashSubsidyText + " is seen as my Subsidy");
		return vashSubsidyText;
	}
	
	public static String returnVashSubsidyStatus() {
		String vashSubsidyText = WebDriverTasks.waitForElement(vashSubsidyStatus).getText();
		System.out.println(vashSubsidyText + " is the Vash Subsidy Status");
		return vashSubsidyText;
	}
	
	public static void clickOnLetterHistoryButton() {
		WebDriverTasks.waitForElement(clickOnLetterHistory).click();
		System.out.println("Letter History accessed");
	}
	
	public static void clickOnEditIconToEditVashSubsidy() {
		WebDriverTasks.waitForElement(editVashSubsidy).click();
	}
	
	public static void openDatePickerOnDateSubmittedOnVashSubsidy() {
		WebDriverTasks.waitForElement(vashSubsidyEditDateSubmitted).click();
	}
	
	public static void openDatePickerOnReceivedDateOnVashSubsidy() {
		WebDriverTasks.waitForElement(vashSubsidyEditReceivedDate).click();
	}
	
	public static void openDatePickerOnCertificateDateOnVashSubsidy() {
		WebDriverTasks.waitForElement(vashSubsidyEditCertificateDate).click();
	}
	
	public static void openDatePickerOnExpirationDateOnVashSubsidy() {
		WebDriverTasks.waitForElement(vashSubsidyEditExpirationDate).click();
	}
	
	public static String returnDateOnDateSubmittedOnVashSubsidy() {
		//String Text = WebDriverTasks.waitForElement(vashSubsidyEditDateSubmitted).getText();
		String Text = WebDriverTasks.waitForElement(vashSubsidyEditDateSubmitted).getAttribute("value");
		System.out.println(Text + " is seen as the Date");
		return Text;
	}
	
	public static String returnDateOnDateSubmittedOnVashSubsidyView() {
		String Text = WebDriverTasks.waitForElement(vashSubsidyViewDateSubmitted).getText();
		System.out.println(Text + " is seen as the Date");
		return Text;
	}
	
	public static String returnDateOnReceivedDateOnVashSubsidyView() {
		String Text = WebDriverTasks.waitForElement(vashSubsidyViewReceivedDate ).getText();
		System.out.println(Text + " is seen as the Date");
		return Text;
	}
	
	public static String returnDateOnCertificateDateOnVashSubsidyView() {
		String Text = WebDriverTasks.waitForElement(vashSubsidyViewCertificateDate).getText();
		System.out.println(Text + " is seen as the Date");
		return Text;
	}
	
	public static String returnDateOnExpirationDateOnVashSubsidyView() {
		String Text = WebDriverTasks.waitForElement(vashSubsidyViewExpirationDate).getText();
		System.out.println(Text + " is seen as the Date");
		return Text;
	}
	
	// Add Gen Pop actions
	public static void clickOnAddEditGenPopSubsidy() {
		WebDriverTasks.waitForElement(clickOnAddGenPopSubsidy).click();
	}
	
	public static void AddGenPopSubsidySelectStatus(String subsidyStatus) {
		WebDriverTasks.waitForElementAndSelect(selectGenPopSubsidy).selectByVisibleText(subsidyStatus);
	}
	
	public static void clickOnAddGenPopDateSubmitted() {
		WebDriverTasks.waitForElement(addGenPopDateSubmitted).click();
	}
	
	//Edit Gen Pop actions
	public static void clickOnEditGenPopDateSubmitted() {
		WebDriverTasks.waitForElement(editGenPopDateSubmitted).click();
	}
	
	public static String returnOnEditGenPopDateSubmitted() {
		String Text = WebDriverTasks.waitForElement(addGenPopDateSubmitted).getAttribute("value");
		System.out.println(Text + " is seen as the Date");
		return Text;
	}
	
	//View Gen Pop actions
	public static String returnGenPopStatus() {
		String Text = WebDriverTasks.waitForElement(viewGenPopSubsidyStatus).getText();
		System.out.println(Text + " is seen as Gen Pop date submitted");
		return Text;
	}

		public static String returnGenPopDateSubmitted() {
			String Text = WebDriverTasks.waitForElement(viewGenPopSubmittedDate).getText();
			System.out.println(Text + " is seen as Gen Pop date submitted");
			return Text;
		}
	
	//service plan notes actions
			public static void selectNotesCollapseIcon() {
				WebDriverTasks.scrollToElement(selectNotesCollapseIcon);
				WebDriverTasks.waitForElement(selectNotesCollapseIcon).click();
			}

			public static void clickOnAddNewNotesButton() {
				WebDriverTasks.waitForElement(clickOnAddNewNotes).click();
			}

			public static void inputNotesText(String notestext) {
				WebDriverTasks.waitForElement(inputNotesText).clear();
				WebDriverTasks.waitForElement(inputNotesText).sendKeys(notestext);
			}
			
			public static void inputNotesTextOnEdit(String notestext) {
				WebDriverTasks.waitForElement(inputNotesTextOnEdit).clear();
				WebDriverTasks.waitForElement(inputNotesTextOnEdit).sendKeys(notestext);
			}

			public static void selectSaveNotes() {
				WebDriverTasks.waitForElement(selectSaveNotes).click();
			}
			
			public static void clickOnSaveNotesOnEdit() {
				WebDriverTasks.waitForElement(selectSaveNotesOnEdit).click();
			}
			
			public static String returnNoRecordsFoundOnNotesGrid() {
				
				String Text = null;
				
				if (WebDriverTasks.verifyIfElementExists(noRecordFoundOnNotesGrid)) {
					Text = WebDriverTasks.waitForElement(noRecordFoundOnNotesGrid).getText();
				System.out.println("The value on Grid is " + Text);
				}
			
				return Text;
			}
			
		public static String returnFirstNotesTextOnNotesGrid() {
				
				String Text = WebDriverTasks.waitForElement(firstRecordNoteTextOnNotesGrid).getText();
				System.out.println("The value on Grid is " + Text);
				return Text;
			}
		
		public static void clickOnIconToEditFirstNoteOnNotesGrid() {
			WebDriverTasks.waitForElement(EditfirstRecordOnNotesGrid).click();
		}
		
		public static void clickOnIconToDeleteFirstNoteOnNotesGrid() {
			WebDriverTasks.waitForElement(DeletefirstRecordOnNotesGrid).click();
		}
		
		public static void confirmDeleteNoteOnNotesGrid() {
			WebDriverTasks.waitForElement(ConfirmDeleteRecordOnNotesGrid).click();
		}
		
		public static void rentalSubsidyRedirectNo() {
			WebDriverTasks.waitForElement(documentRedirectConfirmationNo).click();
		}
		
		//Changelog

		
		public static void clickOnChangeLogExpandIcon() {
			WebDriverTasks.scrollToElement(expandChangeLogIcon);
			WebDriverTasks.waitForElement(expandChangeLogIcon).click();
		}
		
		public static String returnGoalTextOnChangeLogGrid() {
			
			String Text = WebDriverTasks.waitForElement(changelogGoalText).getText();
			System.out.println("The value on Grid is " + Text);
			return Text;
		}
		
	public static String returnNoRecordFoundOnChangeLogGrid() {
			
			String Text = WebDriverTasks.waitForElement(changeLogGridNoRecordsFound).getText();
			System.out.println("The value on Grid is " + Text);
			return Text;
		}
	
	public static void selectConsentToCaseload(String consent ) {
		WebDriverTasks.waitForElementAndSelect(consentToCaseLoad).selectByVisibleText(consent);
	}
	
	
	
}