package nycnet.dhs.streetsmart.pages.clientdetails;

import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class TravelAssistanceProgramTab {


    static By travelAssistanceProgramTab = By.xpath("//li/a[normalize-space()='Travel Assistance Program']");
    static By travelAssistanceProgramGridActionButton = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']>div:first-child>div>a");
    static By travelAssistanceProgramRows = By.cssSelector("div.ui-grid>div>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div>div[role='row']");
    static By viewTravelAssistanceButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='View Details']");
    static By deleteTravelAssistanceButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='Delete']");
    static By confirmEngagementDeleteButton = By.xpath("//button[text()='Yes, delete it!']");
    static By closeRecordDeletionConfirmationMessage = By.xpath("//button[text()='Ok']");
    static By addNewTravelAssistance = By.xpath("//div[@ng-controller='TravelAsstProgCtrl']//a[normalize-space()='Add New']");

    static By travelAssistanceType = By.xpath("//select[@ng-model='TravelAssistance.TravelAssistanceTypeId']");
    final static String caseEligibilityVerification = "//label[normalize-space()='%s']";

    static By clientNameInputbox = By.xpath("//input[@ng-model='TravelAssistance.ReferenceName']");
    static By countryDropdown = By.xpath("//select[@ng-model='TravelAssistance.Address.CountryId']");
    static By stateInputbox = By.xpath("//input[@ng-model='TravelAssistance.Address.State']");
    static By cityInputbox = By.xpath("//input[@ng-model='TravelAssistance.Address.City']");
    static By departureDate = By.xpath("//input[@ng-model='TravelAssistance.ProposedDepatureDate']");
    static By reservationAmountInputbox = By.xpath("//input[@ng-model='TravelAssistance.ReservationAmount']");
    static By modeOfTravelDropdown = By.xpath("//select[@ng-model='TravelAssistance.ModeOfTravelId']");

    static By saveTravelAssistance = By.xpath("//button[contains(@ng-click,'SaveTravelAssistanceDetails(false)')]");
    static By editTravelAssistance = By.xpath("//button[text()='Edit']");
    static By cancelTravelAssistance = By.xpath("//button[@ng-click='CancelTravelAssistance()']");

    static By successMessage = By.cssSelector("div.toast-success>div>div.toast-message");
    static By uploadPassportButton = By.xpath("//td[text()='Passport']/following-sibling::td//span[text()='Upload']");

    //TODO Update below 2 locators
    static By passportDocumentModifiedBy = By.xpath("//td[text()='Passport']/following-sibling::td[1]");
    static By passportDocumentLink =  By.xpath("//td[text()='Passport']/following-sibling::td[2]//a");

    final static String getLabelData = "//div[@ng-controller='TravelAsstProgCtrl']//label[normalize-space()='%s:']/following-sibling::div";

    /**
     * This method will click on Travel Assistance Program tab
     * Created By : Venkat
     */
    public static void clickOnTravelAssistanceProgramTab() {
        WebDriverTasks.waitForElement(travelAssistanceProgramTab).click();
    }

    /**
     * This method will click on Action an then View Details button
     * Created By : Venkat
     *
     * @param rowNo This is row number for which we want to view details, row number starts from 0
     */
    public static void viewTravelAssistanceRecord(int rowNo) {
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(travelAssistanceProgramGridActionButton);
        assert rowNo <= actionButtons.size();
        WebDriverTasks.wait(3);
        actionButtons.get(rowNo).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(viewTravelAssistanceButton).click();
        WebDriverTasks.wait(3);
    }

    /**
     * This method will delete all Travel Assistance records
     * Created By : Venkat
     */
    public static void deleteAllTravelAssistanceRecords() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(travelAssistanceProgramGridActionButton, 3))
            return;
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(travelAssistanceProgramGridActionButton);
        for (int i = actionButtons.size() - 1; i >= 0; i--) {
            deleteTravelAssistanceRecord(i);
        }
    }

    /**
     * This method will delete Travel assistance record for provided row number
     * Created By : Venkat
     *
     * @param rowNo This is row number for which we want to delete, row number starts from 0
     */
    public static void deleteTravelAssistanceRecord(int rowNo) {
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(travelAssistanceProgramGridActionButton);
        assert rowNo <= actionButtons.size();
        actionButtons.get(rowNo).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(deleteTravelAssistanceButton).click();
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(confirmEngagementDeleteButton).click();
        WebDriverTasks.wait(3);
        if (WebDriverTasks.verifyIfElementIsDisplayed(closeRecordDeletionConfirmationMessage, 5))
            WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
    }

    /**
     * This method will create new Travel Assistance
     * Created By : Venkat
     *
     * @param type                      This is type
     * @param eligibilityOptions        Eligibility options, in case of multiple selection data to be passed separated by comma
     * @param name                      This is client name
     * @param country                   Country
     * @param state                     State
     * @param city                      City
     * @param proposedDepartureDate     This is proposed departure date, and it should be future date
     * @param proposedReservationAmount This is estimated reservation amount
     * @param modeOfTravel              Mode of travel
     */
    public static void createNewTravelAssistance(String type, String eligibilityOptions, String name, String country, String state, String city, String proposedDepartureDate, String proposedReservationAmount, String modeOfTravel, String passportDocument) throws AWTException {
        Actions actions = new Actions(WebDriverTasks.driver);
        WebDriverTasks.waitForElement(addNewTravelAssistance).click();
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElementAndSelect(travelAssistanceType).selectByVisibleText(type);
        selectCaseEligibilityOptions(eligibilityOptions);
        fillDestinationDetails(name, country, state, city);
        fillReservationDetails(proposedDepartureDate, proposedReservationAmount, modeOfTravel);
        WebDriverTasks.wait(2);
        //TODO Uncomment once file upload issue is resolved
        //actions.click(WebDriverTasks.waitForElement(uploadPassportButton)).build().perform();
        //DocumentsTab.setFilePathToFileExplorer(passportDocument);
        WebDriverTasks.waitForElement(saveTravelAssistance).click();
    }

    /**
     * This method will fill data for Destination sectino
     * Created By : Venkat
     *
     * @param name    Name
     * @param country Country
     * @param state   State
     * @param city    City
     */
    public static void fillDestinationDetails(String name, String country, String state, String city) {
        WebDriverTasks.waitForElement(clientNameInputbox).clear();
        WebDriverTasks.waitForElement(clientNameInputbox).sendKeys(name);
        WebDriverTasks.waitForElementAndSelect(countryDropdown).selectByVisibleText(country);
        WebDriverTasks.waitForElement(stateInputbox).clear();
        WebDriverTasks.waitForElement(stateInputbox).sendKeys(state);
        WebDriverTasks.waitForElement(cityInputbox).clear();
        WebDriverTasks.waitForElement(cityInputbox).sendKeys(city);
    }

    /**
     * This method will fill data for Reservation section
     * Created By : Venkat
     *
     * @param proposedDepartureDate     Proposed departure date, it should be future date only
     * @param proposedReservationAmount Estimated reservation amount
     * @param modeOfTravel              Mode of travel
     */
    public static void fillReservationDetails(String proposedDepartureDate, String proposedReservationAmount, String modeOfTravel) {
        Base.selectDate(departureDate, proposedDepartureDate);
        WebDriverTasks.waitForElement(reservationAmountInputbox).clear();
        WebDriverTasks.waitForElement(reservationAmountInputbox).sendKeys(proposedReservationAmount);
        WebDriverTasks.waitForElementAndSelect(modeOfTravelDropdown).selectByVisibleText(modeOfTravel);
    }

    /**
     * This method will select Case Eligibility Options.
     * Created By : Venkat
     *
     * @param options Case eligibility options separated by comma
     */
    public static void selectCaseEligibilityOptions(String options) {
        String allItems[] = options.split(",");
        for (String item : allItems) {
            WebDriverTasks.waitForElement(By.xpath(String.format(caseEligibilityVerification, item.trim()))).click();
        }
    }

    /**
     * This method will verify saved values for Travel assistance
     * Created By : Venkat
     *
     * @param type                       Type
     * @param eligibilityOptions         Eligibility options
     * @param name                       Name
     * @param country                    Country
     * @param state                      State
     * @param city                       City
     * @param proposedDepartureDate      Proposed Departure Date
     * @param estimatedReservationAmount Reservation amount
     * @param modeOfTravel               Mode of travel
     * @return It will return labels separated comma for which either value is not displayed or is not matching expected value
     */
    public static String verifyTravelAssistanceDetails(String type, String eligibilityOptions, String name, String country, String state, String city, String proposedDepartureDate, String estimatedReservationAmount, String modeOfTravel, String passportDocument) {
        HashMap<String, String> elementMap = new HashMap<>();
        elementMap.put("Travel Assistance Type", type);
        elementMap.put("Eligibility Verification Options", eligibilityOptions);
        elementMap.put("Name", name);
        elementMap.put("Country", country);
        elementMap.put("State", state);
        elementMap.put("City", city);
        elementMap.put("Proposed Departure Date", proposedDepartureDate);
        elementMap.put("Estimated Reservation Amount($)", estimatedReservationAmount);
        elementMap.put("Mode of Travel", modeOfTravel);
        LinkedList<String> exception = new LinkedList<>();
        WebDriverTasks.waitForAngular();

        for (String label : elementMap.keySet()) {
            if (!verifyValueForLabel(label, elementMap.get(label)))
                exception.add(label);
        }
        //TODO Uncomment once file upload issue is resolved
        /*String modifiedBy = Base.returnLoggedInByUser();
        String fileName = new File(passportDocument).getName();

        if(!WebDriverTasks.waitForElement(passportDocumentModifiedBy).getText().contains(modifiedBy))
            exception.add("Passport Document- Modified By");

        if(!WebDriverTasks.waitForElement(passportDocumentLink).getText().equals(fileName))
            exception.add("Passport Document- Document Link");*/

        return String.join(",", exception);
    }

    public static void clickOnPassportDocumentLink(){
        WebDriverTasks.waitForElement(passportDocumentLink).click();
    }

    /**
     * This method will verify that success message is matching with provided message
     * Created By : Venkat
     *
     * @param message This is expected message text
     * @return If actual message matches with expected message it return True else False
     */
    public static boolean verifySuccessMessageIsDisplayed(String message) {
        String actualMessage = WebDriverTasks.waitForElement(successMessage).getText();
        return message.equals(actualMessage);
    }

    /**
     * This method will verify value against provided label and compare with expected value
     * Created By : Venkat
     *
     * @param label Label for which value is to be compared
     * @param value Expected value
     * @return boolean If actual value matches with expected value then it will return True else False
     */
    public static boolean verifyValueForLabel(String label, String value) {
        String actualValue;
        By by = By.xpath(String.format(getLabelData, label));
        if (WebDriverTasks.verifyIfElementIsDisplayed(by, 1)) {
            actualValue = WebDriverTasks.waitForElement(by).getText();
            if(value.split(",").length>1) {

                LinkedList<String> actualValues = new LinkedList<>();
                for(String val : actualValue.split(","))
                    actualValues.add(val.trim());
                LinkedList<String> expectedValues = new LinkedList<>();
                for(String val : value.split(","))
                    expectedValues.add(val.trim());

                if(actualValues.size()!=expectedValues.size())
                    return false;

                actualValues.removeAll(expectedValues);

                if(actualValues.size()!=0)
                    return false;
            }
            else {
                if (!value.equalsIgnoreCase(actualValue))
                    return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * This method will click on Edit button on Travel Assistance details page
     * Created By : Venkat
     */
    public static void clickOnEditTravelAssistance() {
        WebDriverTasks.scrollToElement(editTravelAssistance);
        WebDriverTasks.waitForElement(editTravelAssistance).click();
    }

    /**
     * This method wil click on Cancel button on Travel Assistance details page
     * Created By : Venkat
     */
    public static void clickOnCancelTravelAssistanceButton() {
        WebDriverTasks.scrollToElement(cancelTravelAssistance);
        WebDriverTasks.waitForElement(cancelTravelAssistance).click();
    }

    /**
     * This method will click on Save button
     * Created By : Venkat
     */
    public static void clickOnSaveTravelAssistance() {
        WebDriverTasks.scrollToElement(saveTravelAssistance);
        WebDriverTasks.waitForElement(saveTravelAssistance).click();
    }

    /**
     * This method will get total records count displayed in table
     * Created By : Venkat
     *
     * @return If records are displayed it will return non-zero count, else it will return 0
     */
    public static int getTravelAssistanceRecordCount() {
        WebDriverTasks.waitForAngular();
        if (!WebDriverTasks.verifyIfElementIsDisplayed(travelAssistanceProgramRows, 5))
            return 0;
        return WebDriverTasks.waitForElements(travelAssistanceProgramRows).size();
    }
}
