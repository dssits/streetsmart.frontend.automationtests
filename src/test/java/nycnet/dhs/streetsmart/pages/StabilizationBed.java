package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class StabilizationBed extends StartWebDriver {
	static By stabilizationbedMenuItem = By.xpath("//a[contains(text(),'Stabilization Bed')]");
	
	public static String returnstabilizationbedPageHeader() {
		String stabilizationbedPageHeaderText = WebDriverTasks.waitForElement(stabilizationbedMenuItem).getText();
		System.out.println("The page header is:" +stabilizationbedPageHeaderText);
		return stabilizationbedPageHeaderText;
	}

}
