package nycnet.dhs.streetsmart.pages;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;


public class ClientInformation extends StartWebDriver {

	
	
	static By clientListPageHeader = By.xpath("//h1[@class='page-header '][contains(text(), 'Client List')]");

	static By clickOnEditButton = By.xpath("(//*[@id='cint-info-edit'])[2]");
	static By engagedUnshelteredClientCategoryMenuOption = By
			.xpath("//*[@class='col-md-6']/select[4]/option[contains(text(),'Engaged Unsheltered')]");

	static By InactivedClientCategoryMenuOption = By
			.xpath("//*[@class='col-md-6']/select[4]/option[contains(text(),'Inactive')]");

	static By streetSmartIDPageHeader = By.xpath("//h4/span");

	static By clientDetailsHeaderMenuItem = By.xpath("//h1[contains(text(),'Client: View Client Details')]");

	static By clientInformationTab = By.xpath("(//*[@class='nav nav-tabs']//a[contains(text(), 'Client')]/..)[2]");

	static By clientCategoryUpdate = By.xpath("(//*[@ng-model='Client.ClientType'])[4]");
	static By clientCategoryUpdate2 = By.xpath("(//*[@ng-model='Client.ClientType'])[3]");

	static By clientCategoryUpdateEditView = By
			.xpath("/html[1]/body[1]/div[2]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/fieldset"
					+ "[1]/div[7]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/select[4]");

	static By clientCategoryUpdateViewOnly = By.xpath("(//*[@ng-model='Client.ClientType'])[7]");
	static By editClientInformationButton = By.xpath("//*[@id='cint-info-edit']");
	static By editClientInformationButton2 = By.xpath("(//*[@id='cint-info-edit'])[2]");
	
	static By saveInactiveClientInformationButton = By
			.xpath("//div[@class='col-md-4 p-r-0 pull-right']//button[@id='cint-info-save']");

	static By saveEngagedUnshelteredClientInformationButton = By
			.xpath("//div[@class='col-md-4 p-r-0 pull-right']//button[@id='cint-info-save']");

	static By clientCategoryChosenOptionViewPage = By
			.xpath("//*[@id='accordion2']//*[.='Client Category:']/following-sibling::div");

	static By selectOtherProspectOrCaseloadClientCategoryOnViewPage = By.
			cssSelector("select[ng-change='handleProspectToEnrolledFromViewOnlyMode(Client.ClientEnrollment.ServiceTypeId)'] > option[selected='selected']");
	
	static By clientCategoryMenuEditPage = By.xpath("(//*[@ng-model='Client.ClientType'])[4]");
	static By clientCategoryMenuEditPage2 = By.xpath("(//*[@ng-model='Client.ClientType'])[5]");
	
	//static By clientCategoryAllTabsMenuEditPage = By.xpath("(//*[@ng-model='Client.ClientType'])[6]");
	static By clientCategoryAllTabsMenuEditPage = By.cssSelector("select[ng-change='handleAutoflipClientToEnrolled(6,false)']");
	
	static By caseChangePopupMessage = By.xpath("//*[@class='modal-body']/h5");

	static By enterLegalFirstName = By.xpath("//*[@ng-model='Client.FirstName']");
	static By enterLegalLastName = By.xpath("//*[@ng-model='Client.LastName']");
	static By enterPreferredFirstName = By.xpath("//*[@ng-model='Client.PreferredFirstName']");
	static By enterPreferredLastName = By.xpath("//*[@ng-model='Client.PreferredLastName']");
	static By enterAlternateName = By.xpath("//*[@ng-model='Client.Alias']");
	static By selectGender = By.xpath("//*[@ng-model='Client.Gender']");
	static By selectArea = By.xpath("//*[@ng-model='Client.AreaId']");
	static By clickSaveClientInformationButton = By.xpath("(//*[@id='cint-info-save'])[1]");

	static By expandChangelog = By.cssSelector("a.collapse-panel[id='change-log-accordion']");
	static By changelogSubwayDiversionProgramStartDate = By.xpath("//div[@role='grid']//div[@role='rowgroup'][@class='ui-grid-viewport ng-isolate-scope']/div[@class='ui-grid-canvas']/div[1]/div/div[7]//span[3]");
	
	static By enrollmentEndDate = By.xpath("(//*[@ng-model='Client.ClientEnrollment.EnrollmentEndDate'])[2]");
	
	static By enrollmentEndDateOnEnrolledClient = By.xpath("//*[@ng-model='Client.ClientEnrollment.EnrollmentEndDate']");
	
	static By okConfirm = By.xpath("//*[@ng-click='ok()']");
	
	//Close enrollment pop-up locators
	static By itemCaseEndDate = By.xpath("//*[@ng-model='item.CaseEndDate']");
	static By itemCaseEndReason = By.xpath("//*[@ng-model='item.CaseEndReasonId']");
	static By saveButton = By.xpath("//*[@ng-click='Save()']");
	static By reOpenButton = By.xpath("//*[@ng-click='AddEnrollment(Client.ClientProvider.StartDateOnStreet,Client.ClientEnrollment.ServiceTypeId )']");
	
	//Reopen enrollment pop-up locators
	static By reOpenEnrollMentProvider = By.xpath("//*[@id='ddlProvider']");
	
	static By reOpenEnrollMentArea = By.xpath("//*[@id='ddlArea']");
	
	static By saveOnReopenEnrollmentPopup = By.xpath("//*[@ng-click='AddEnrollment()']");
	
	public static String returnEnrollmentEndDateEnablementStatus() {
		WebDriverTasks.waitForAngular();
		String Text = driver.findElement(enrollmentEndDate).getAttribute("ng-disabled");
		System.out.println("The enrollment end date is disabled " + Text);
		return Text;
	}
	
	public static String returnClientCategoryEnablementStatus() {
		WebDriverTasks.waitForAngular();
		String Text = driver.findElement(clientCategoryUpdate2).getAttribute("ng-disabled");
		System.out.println("The enrollment end date is disabled " + Text);
		return Text;
	}
	
	public static String returnClientListPageHeader() {
		String clientListPageHeaderText = WebDriverTasks.waitForElement(clientListPageHeader).getText();
		System.out.println("The page header is " + clientListPageHeaderText);
		return clientListPageHeaderText;
	}

	public static String returnStreetsmartID() {
		String streetSmartIDPageHeaderText = WebDriverTasks.waitForElement(streetSmartIDPageHeader).getText();
		System.out.println("The StreetSmartID is " + streetSmartIDPageHeaderText);
		return streetSmartIDPageHeaderText;
	}

	public static String returnEngagedUnshelteredClientCategoryMenuOption() {
		String Text = WebDriverTasks.waitForElement(engagedUnshelteredClientCategoryMenuOption).getText();
		System.out.println("The page header is " + Text);
		return Text;
	}

	public static String returnClientCategoryChosenOptionOnViewPage() {
		String clientCategoryChosenOptionText = WebDriverTasks.waitForElement(clientCategoryChosenOptionViewPage)
				.getText();
		System.out.println("The client category is " + clientCategoryChosenOptionText);
		return clientCategoryChosenOptionText;
	}

	public static String returnEngagedInactiveCategoryMenuOption() {
		String Text = WebDriverTasks.waitForElement(InactivedClientCategoryMenuOption).getText();
		System.out.println("The page header is " + Text);
		return Text;
	}

	public static void clickOnEditButton() {
		WebDriverTasks.waitForElement(clickOnEditButton).click();
	}

	public static void clickOnSaveButtonforInactiveClient() {
		WebDriverTasks.waitForElement(saveInactiveClientInformationButton).click();
		System.out.println("Test Pass: Inactive is seen as a Client Category");
	}

	public static void clickOnSaveButtonforEngagedUnshelteredClient() {
		WebDriverTasks.waitForElement(saveEngagedUnshelteredClientInformationButton).click();
		System.out.println("Test Pass: Engaged Unsheltered Prospect is seen as a Client Category");
	}

	public static String returnClientDetailsPageHeader() {
		String clientDetailsPageHeaderText = WebDriverTasks.waitForElement(clientDetailsHeaderMenuItem).getText();
		System.out.println("The page header is " + clientDetailsPageHeaderText);
		return clientDetailsPageHeaderText;
	}

	public static String returnClientCategoryViewOnly() {
		String clientCategoryText = WebDriverTasks.waitForElement(clientCategoryUpdateViewOnly).getText();
		System.out.println("The client category is" + clientCategoryText );
		return clientCategoryText;
	}
	
	//Return selected Other Prospect or Caseload Client Category on the View Page
	public static String returnSelectedProspectOrCaseLoadClientCategoryOnViewPage() {
		String clientCategoryText = WebDriverTasks.waitForElement(selectOtherProspectOrCaseloadClientCategoryOnViewPage).getText();
		System.out.println("The client category is " + clientCategoryText );
		return clientCategoryText;
	}

	public static String returnClientCategoryEditView() {
		String clientCategoryText = WebDriverTasks.waitForElement(clientCategoryUpdateEditView).getText();
		System.out.println("The client category is" + clientCategoryText + "sample");
		return clientCategoryText;
	}

	public static void returnClientInformationTab() {
		WebDriverTasks.waitForElement(clientInformationTab).click();
	}

	public static void selectCaseLoad(String clientcategory) {
		WebDriverTasks.waitForElementAndSelect(clientCategoryUpdateViewOnly).selectByVisibleText(clientcategory);
		// Select newClientCategory = new Select
		// (driver.findElement(clientCategoryUpdateViewOnly));
		// newClientCategory.selectByVisibleText(clientcategory);
	}

	public static void clickOnEditClientInformationButton() {
		WebDriverTasks.waitForElement(editClientInformationButton).click();
	}
	
	public static void clickOnEditClientInformationButton2() {
		WebDriverTasks.waitForElement(editClientInformationButton2).click();
	}

	public static void verifyEngagedUnshelteredClientCategory(String engagedunsheltered) {
		Select updateClientCategory = new Select(driver.findElement(clientCategoryUpdateEditView));
		updateClientCategory.selectByVisibleText(engagedunsheltered);

	}

	public static String returnUpdatedClientCategory() {
		String clientCategoryText1 = WebDriverTasks.waitForElement(clientCategoryUpdateEditView).getText();
		System.out.println(clientCategoryText1 + "is an available client category option");
		return clientCategoryText1;
	}

	public static void selectClientCategoryOnEditPage(String clientCategory) {
		WebDriverTasks.waitForElementAndSelect(clientCategoryMenuEditPage).selectByVisibleText(clientCategory);
	}
	
	public static void selectClientCategoryOnEditPage2(String clientCategory) {
		WebDriverTasks.waitForElementAndSelect(clientCategoryMenuEditPage2).selectByVisibleText(clientCategory);
	}
	
	public static void selectClientCategoryAllTabsOnEditPage(String clientCategory) {
		WebDriverTasks.waitForElement(clientCategoryAllTabsMenuEditPage).click();
		WebDriverTasks.waitForElementAndSelect(clientCategoryAllTabsMenuEditPage).selectByValue(clientCategory);
		
		//Select newClientCategory = new Select (driver.findElement(clientCategoryAllTabsMenuEditPage));
				//newClientCategory.selectByVisibleText(clientCategory);
	}

	public static String returnCaseChangeMessagepopup() {
		String caseChangeText = WebDriverTasks.waitForElement(caseChangePopupMessage).getText();
		System.out.println("The popup mesage is  " + caseChangeText);
		return caseChangeText;
	}

	public static void inputLegalFirstName(String legalfirstname) {
		WebDriverTasks.waitForElement(enterLegalFirstName).clear();
		WebDriverTasks.waitForElement(enterLegalFirstName).sendKeys(legalfirstname);
	}

	public static void inputLegalLastName(String legallastname) {
		WebDriverTasks.waitForElement(enterLegalLastName).clear();
		WebDriverTasks.waitForElement(enterLegalLastName).sendKeys(legallastname);
	}

	public static void inputPreferredFirstName(String preferredfirstname) {
		WebDriverTasks.waitForElement(enterPreferredFirstName).clear();
		WebDriverTasks.waitForElement(enterPreferredFirstName).sendKeys(preferredfirstname);
	}

	public static void inputPreferredLastName(String preferredlastname) {
		WebDriverTasks.waitForElement(enterPreferredLastName).clear();
		WebDriverTasks.waitForElement(enterPreferredLastName).sendKeys(preferredlastname);
	}

	public static void inputAlternateName(String alternatename) {
		WebDriverTasks.waitForElement(enterAlternateName).clear();
		WebDriverTasks.waitForElement(enterAlternateName).sendKeys(alternatename);
	}

	public static void selectGender(String gender) {
		WebDriverTasks.waitForElementAndSelect(selectGender).selectByVisibleText(gender);
	}

	public static void selectArea(String area) {
		WebDriverTasks.waitForElementAndSelect(selectArea).selectByVisibleText(area);
	}

	public static void clickSaveClientInformationButton() {
		WebDriverTasks.waitForElement(clickSaveClientInformationButton).click();
	}

	public static String returnClientInformationTabHeader() {
		String clientinformationTabHeaderText = WebDriverTasks.waitForElement(clientInformationTab).getText();
		System.out.println("The page header is " + clientinformationTabHeaderText);
		return clientinformationTabHeaderText;
	}
	
	public static void expandChangelog() {
		WebDriverTasks.waitForElement(expandChangelog).click();
		
	}
	
	public static String returnSubwayDiversionProgramStartDateChangelog() {
		String Text = WebDriverTasks.waitForElement(changelogSubwayDiversionProgramStartDate).getText();
		System.out.println("The Subway Diversion Program Start Date in the Changelog is " + Text);
		return Text;
	}
	
	




    



 
    static By dateOfBirthYearDropdown = By.xpath("//div[@id='ui-datepicker-div' and contains(@style,'display: block;')]//select[@data-handler='selectYear']");

    //Added By Venakt

    //Created By Venkat
    static By engagementsTab = By.id("engagementtab");
    static By clientInformationsTab = By.xpath("//a[normalize-space()='Client Information']");
    static By providerNameCells = By.cssSelector("div.ui-grid-canvas>div.ui-grid-row>div[role='row']>div:nth-child(2)>div>span:nth-child(1)");
    static By contactDateTimeCells = By.cssSelector("div.ui-grid-canvas>div.ui-grid-row>div[role='row']>div:nth-child(3)>div>span:nth-child(1)");
    static By deleteEngagementButton = By.xpath("//ul[contains(@style,'display: block')]/li/a[@class='cursor-pointer' and normalize-space()='Delete']");
    static By viewEngagementButton = By.xpath("//ul[contains(@style,'display: block;')]/li/a[@class='cursor-pointer' and normalize-space()='View Details']");
    static By editEngagementsButton = By.xpath("//button[@id='cint-info-save' and @ng-click='EditEngagement()']");
    static By saveEngagementButton = By.xpath("//button[@id='cint-info-save' and @ng-click='SaveClientEngagement()']");
    static By confirmEngagementDeleteButton = By.xpath("//button[text()='Yes, delete it!']");
    static By recordDeletionConfirmation = By.xpath("//p[text()='Record Deleted Successfully.']");
    static By closeRecordDeletionConfirmationMessage = By.xpath("//button[text()='Ok']");
    static By clientCategoryDropdown = By.xpath("//select[@ng-model='Client.ClientType'  and not(contains(@class,'ng-hide')) and not(@disabled)]");
    static By providerDropdown = By.xpath("//select[@ng-model='ClientEngagement.ProviderId']");
    static By baroughDropdown = By.xpath("//select[@ng-model='ClientEngagement.LocationInfo.BoroughId']");
    static By locationTypeDropdown = By.xpath("//select[@ng-model='ClientEngagement.LocationInfo.LocationTypeId']");
    static By phoneNoInputbox = By.xpath("//input[@ng-model='ClientEngagement.LocationInfo.Phone']");
    static By dataSavedSuccessMessage = By.xpath("//div[text()='Changes have been saved successfully.' and @class='toast-message']");
    static By selectEngagementArea = By.xpath("//*[@ng-model='ClientEngagement.AreaId']");
    static By engagementActionButton = By.cssSelector("div.ui-grid-canvas>div.ui-grid-row>div[role='row']>div:nth-child(1)>div>a");
    static By engagementContactDateInputbox = By.xpath("//input[@ng-model='ClientEngagement.ContactDate']");
    static By engagementContactTimeInputbox = By.xpath("//input[@ng-model='ClientEngagement.ContactTime']");
    static By engagementShiftDropdown = By.xpath("//select[@ng-model='ClientEngagement.ShiftInfo.ShiftId']");
    static By engagementTeamNameInputbox = By.xpath("//input[@ng-model='ClientEngagement.ShiftInfo.TeamName']");
    static By engagementJointOutreachDropdown = By.xpath("//select[@ng-model='ClientEngagement.ShiftInfo.JointOutreachId']");
    static By engagementContactReasonDropdown = By.xpath("//select[@ng-model='ClientEngagement.ContactReason']");
    static By engagementEngagementOutcomeDropdown = By.xpath("//select[@ng-model='ClientEngagement.ClientEngagementOutcomeId']");
    static By engagementNote = By.xpath("//textarea[@ng-model='ClientEngagement.Notes']");
    static By engagementSubwayLine = By.xpath("//select[@ng-model='ClientEngagement.LocationInfo.SubwayDetail.SubwayLineId']");
    static By engagementSubwayStation = By.xpath("//select[@ng-model='ClientEngagement.LocationInfo.SubwayDetail.SubwayStationId']");
    static By addNewEngagement = By.id("clientengagementaddnew");
    static By expandChangeLog = By.cssSelector("a.collapse-panel[id='change-log-accordion']");
    static By engagementLogClientCategoryForFirstRow = By.cssSelector("div[role='grid']>div[role='rowgroup']>div[class='ui-grid-canvas']>div:nth-child(1)>div>div[role='gridcell']:nth-child(7)>div>span:nth-child(3)");
    static By engagementLogChangedByForFirstRow = By.cssSelector("div[role='grid']>div[role='rowgroup']>div[class='ui-grid-canvas']>div:nth-child(1)>div>div[role='gridcell']:nth-child(1)>div>span:nth-child(3)");
    static By clientDOB = By.xpath("//input[@name='DOB']");
    static By caseInformationTab = By.xpath("//li/a[normalize-space()='Case Information']");
    static By deleteCasePopup = By.xpath("//h1[normalize-space()='Delete Case']");
    static By makeClientEngagedUnshelteredRadio = By.name("make-engagedUnsheltered");
    static By nextButton = By.xpath("//button[@type='button' and text()='Next']");
    static By activeClientInformationTabButton = By.xpath("//li[@class='active']/a[normalize-space()='Client Information']");
    static By clientCategoryText = By.xpath("//div[@id='accordion2']//label[text()='Client Category:']/following-sibling::div[@style]");
    static By allVisibleTabs = By.xpath("//ul[@class='nav nav-tabs']/li[not(@class='ng-hide')]/a");
    static By caseActionButton = By.xpath("//div[contains(@ng-show,'HandleSingleCaseDelete ') and @class]//div[contains(@class,'ui-grid-render-container')]//div[contains(@class,'ui-grid-viewport')]//div[contains(@class,'ui-grid-canvas')]//div[contains(@class,'ui-grid-row')]//a[normalize-space()='Action']");
    static By clientChangedToEngagedSuccessMessage = By.xpath("//div[@class='toast toast-success']//div[text()='Client Category has been updated to Engaged Unsheltered Prospect']");
    static By clientChangedToOtherProspectSuccessMessage = By.xpath("//div[@class='toast toast-success']//div[text()='Client Category has been updated to Other Prospect']");
    static By clientCategoryChangedSuccessMessage = By.xpath("//div[@class='toast toast-success']//div[contains(text(),'Client Category has been updated to')]");
    static By subwayLineIDDropdown = By.xpath("//select[contains(@ng-model,'SubwayLineId')]");
    static By subwayStationDropdown = By.xpath("//select[contains(@ng-model,'SubwayStationId')]");
    static By latestEngagementDate = By.cssSelector("div.ui-grid-canvas>div.ui-grid-row:nth-child(1)>div[role='row']>div:nth-child(3)>div>span:first-child");

    static String dynamicClientDetailsPageTabButton = "//ul[@class='nav nav-tabs']/li[not(@class='ng-hide')]/a[normalize-space()='%s']";
    

 


    /**
     * This method will click on Engagements tab
     * Created By : Venkat
     */
    public static void clickOnEngagementsTab() {
        WebDriverTasks.scrollToElement(engagementsTab);
        WebDriverTasks.waitForElement(engagementsTab).click();
    }

    /**
     * This method will Click on Action->COnfirm->Verify Deletion confirmation message for given row number(row number starts from 0).
     * Created By : Venkat
     *
     * @param rowNo This is row no in table for which deletion is to be performed
     * @return boolean If the row deletion message is displayed then it will be True else False
     */
    public static boolean deleteEnagagementRecord(int rowNo) {
        By action = By.cssSelector(String.format("div.ui-grid-canvas>div.ui-grid-row:nth-child(%d)>div[role='row']>div:nth-child(1)>div>a", rowNo + 1));
        WebDriverTasks.waitForElement(action).click();
        WebDriverTasks.waitForElement(deleteEngagementButton).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(confirmEngagementDeleteButton).click();
        WebDriverTasks.waitForAngular();
        WebDriverTasks.wait(3);
        if (WebDriverTasks.verifyIfElementIsDisplayed(recordDeletionConfirmation, 10))
            WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
        else
            return false;
        return true;
    }

    /**
     * This method will Click on Action->COnfirm->Verify Deletion confirmation message for given row number(row number starts from 0).
     * Created By : Venkat
     *
     * @param rowNo This is row no in table for which deletion is to be performed
     * @return boolean If the row deletion message is displayed then it will be True else False
     */
    public static boolean changeEngagementProvider(int rowNo) {
        By action = By.cssSelector(String.format("div.ui-grid-canvas>div.ui-grid-row:nth-child(%d)>div[role='row']>div:nth-child(1)>div>a", rowNo + 1));
        WebDriverTasks.waitForElement(action).click();
        WebDriverTasks.waitForElement(viewEngagementButton).click();
        WebDriverTasks.waitForElement(editEngagementsButton).click();
        selectProvider("BW");
        selectEngagementArea("Bronx");
        selectEngagementOutcome("Refused Services/Engagement");
        selectBarough("Bronx");
        selectlocationType("Phone");
        setPhoneNo("9998889998");
        saveEngagement();
        return WebDriverTasks.verifyIfElementIsDisplayed(dataSavedSuccessMessage, 20);
    }

    /**
     * This method will Click on Action->COnfirm->Verify Deletion confirmation message for given row number(row number starts from 0).
     * Created By : Venkat
     *
     * @param rowNo This is row no in table for which deletion is to be performed (row number starts from 0)
     * @return boolean If the row deletion message is displayed then it will be True else False
     */
    public static boolean changeEngagementProviderToBRC(int rowNo) {
        By action = By.cssSelector(String.format("div.ui-grid-canvas>div.ui-grid-row:nth-child(%d)>div[role='row']>div:nth-child(1)>div>a", rowNo + 1));
        WebDriverTasks.waitForElement(action).click();
        WebDriverTasks.waitForElement(viewEngagementButton).click();
        WebDriverTasks.waitForElement(editEngagementsButton).click();
        selectProvider("BRC");
        selectEngagementArea("Bronx");
        selectSubwayLine("1");
        selectSubwayStation("231 St");
        saveEngagement();
        return WebDriverTasks.verifyIfElementIsDisplayed(dataSavedSuccessMessage, 20);
    }

    /**
     * This method will select Subway Line
     * Created By : Venkat
     */
    public static void selectSubwayLine(String subwayLine) {
        WebDriverTasks.waitForElementAndSelect(subwayLineIDDropdown).selectByVisibleText(subwayLine);
    }

    /**
     * This method will select subway station
     * Created By : Venkat
     */
    public static void selectSubwayStation(String subwayStation) {
        WebDriverTasks.waitForElementAndSelect(subwayStationDropdown).selectByVisibleText(subwayStation);
    }

    /**
     * This method will click on Save button on edit engagement page
     * Created By : Venkat
     */
    public static void saveEngagement() {
        WebDriverTasks.waitForElement(saveEngagementButton).click();
    }

    /**
     * This method will select Are on edit engagement screen
     * Created By : Venkat
     *
     * @param area This is area to be selected
     */
    public static void selectEngagementArea(String area) {
        WebDriverTasks.waitForElementAndSelect(selectEngagementArea).selectByVisibleText(area);
    }

    /**
     * This method will set phone number on edit engagement screen
     * Created By : Venkat
     *
     * @param phoneNo This is phone no
     */
    public static void setPhoneNo(String phoneNo) {
        WebDriverTasks.waitForElement(phoneNoInputbox).sendKeys(phoneNo);
    }

    /**
     * This method will select location type dropdown
     * Created By : Venkat
     *
     * @param location This is location to be selected
     */
    public static void selectlocationType(String location) {
        WebDriverTasks.waitForElementAndSelect(locationTypeDropdown).selectByVisibleText(location);
    }

    /**
     * This method will select barough dropdown
     * Created By : Venkat
     *
     * @param barough This is barough value to be selected
     */
    public static void selectBarough(String barough) {
        WebDriverTasks.waitForElementAndSelect(baroughDropdown).selectByVisibleText(barough);
    }

    /**
     * This method will select provider
     * Created By : Venkat
     *
     * @param provider This is provider to be selected
     */
    public static void selectProvider(String provider) {
        WebDriverTasks.waitForElementAndSelect(providerDropdown).selectByVisibleText(provider);
    }

    /**
     * This method will click on client information tab
     * Created By : Venkat
     */
    public static void clickOnClientInformationTab() {
        WebDriverTasks.scrollToElement(clientInformationsTab);
        WebDriverTasks.waitForElement(clientInformationsTab).click();
    }

    /**
     * This method will verify if client category value is changed to 'Other Prospect' or not
     * Created By : Venkat
     *
     * @return boolean If value is changed then it will return True else False
     */
    public static boolean verifyClientCategoryIsChangedToOtherProspect() {
        WebDriverTasks.waitForAngular();
        Select clientCategory = new Select(WebDriverTasks.driver.findElement(clientCategoryDropdown));
        return clientCategory.getFirstSelectedOption().getText().equals("Other Prospect");
    }

    /**
     * This method will delete all engagements
     * Created By : Venkat
     */
    public static void deleteAllEngagements() {
        WebDriverTasks.waitForAngular();
        List<WebElement> actionButtons = WebDriverTasks.driver.findElements(engagementActionButton);
        int rowsToDelete = actionButtons.size() - 1;
        for (int i = rowsToDelete; i >= 0; i--) {
            WebDriverTasks.waitForAngular();
            WebDriverTasks.wait(3);
            WebDriverTasks.driver.findElements(engagementActionButton).get(i).click();
            WebDriverTasks.wait(1);
            WebDriverTasks.waitForElement(deleteEngagementButton).click();
            WebDriverTasks.wait(5);
            WebDriverTasks.waitForElement(confirmEngagementDeleteButton).click();
            WebDriverTasks.wait(3);
            WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
        }
    }

    /**
     * This method will delete all cases
     * Created By : Venkat
     */
    public static void deleteAllCases() {
        WebDriverTasks.waitForAngular();
        List<WebElement> actionButtons = WebDriverTasks.waitForElements(caseActionButton);
        int rowsToDelete = actionButtons.size() - 1;
        for (int i = rowsToDelete; i >= 0; i--) {
            WebDriverTasks.waitForAngular();
            WebDriverTasks.waitForElements(caseActionButton).get(i).click();
            WebDriverTasks.wait(1);
            WebDriverTasks.waitForElement(deleteEngagementButton).click();
            WebDriverTasks.wait(5);
            WebDriverTasks.waitForElement(confirmEngagementDeleteButton).click();
            WebDriverTasks.wait(3);
            if (WebDriverTasks.verifyIfElementIsDisplayed(closeRecordDeletionConfirmationMessage, 5))
                WebDriverTasks.waitForElement(closeRecordDeletionConfirmationMessage).click();
        }
    }

    /**
     * This method will fill shift details
     * Created By : Venkat
     *
     * @param provider      This is engagement provider to be selected
     * @param area          This is area of engagement
     * @param shift         This is shift when engagement done
     * @param team          This is team
     * @param subwayLine    This is subway line where engagement was done
     * @param subwayStation This is subway station where engagement was done
     * @param jointOutreach If it was joint outreach
     */
    public static void fillShiftDetails(String provider, String area, String shift, String team, String subwayLine, String subwayStation, String jointOutreach) {
        selectProvider(provider);
        selectEngagementArea(area);
        WebDriverTasks.waitForElementAndSelect(engagementShiftDropdown).selectByVisibleText(shift);
        WebDriverTasks.waitForElement(engagementTeamNameInputbox).sendKeys(team);
        WebDriverTasks.waitForElementAndSelect(engagementSubwayLine).selectByVisibleText(subwayLine);
        WebDriverTasks.waitForElementAndSelect(engagementSubwayStation).selectByVisibleText(subwayStation);
        WebDriverTasks.waitForElementAndSelect(engagementJointOutreachDropdown).selectByVisibleText(jointOutreach);
    }

    /**
     * This method will fill contact details
     * Created By : Venkat
     *
     * @param contactDate   This is date when engagement was done
     * @param contactTime   This is time of engagement
     * @param contactReason This is reason for contact
     */
    public static void fillContactDetails(String contactDate, String contactTime, String contactReason) {
        selectEngagementDate(contactDate);
        WebDriverTasks.waitForElement(engagementContactTimeInputbox).sendKeys(contactTime);
        WebDriverTasks.waitForElementAndSelect(engagementContactReasonDropdown).selectByVisibleText(contactReason);
    }

    /**
     * This method will select engagement date
     * Created By : Venkat
     *
     * @param contactDate This is date of engagement date
     */
    public static void selectEngagementDate(String contactDate) {
        WebDriverTasks.scrollToElement(engagementContactDateInputbox);
        Base.selectDate(engagementContactDateInputbox, contactDate);
    }

    /**
     * This method will select engagement outcome
     * Created By : Venkat
     *
     * @param engagementOutcome This is engagement outcome to be selected
     */
    public static void selectEngagementOutcome(String engagementOutcome) {
        WebDriverTasks.waitForElementAndSelect(engagementEngagementOutcomeDropdown).selectByVisibleText(engagementOutcome);
    }

    /**
     * This method will set engagement notes
     * Created By : Venkat
     *
     * @param note This is engagement not
     */
    public static void setEngagementNotes(String note) {
        WebDriverTasks.waitForElement(engagementNote).sendKeys(note);
    }

    /**
     * This method will click on '+Add new' engagement button on engagement screen
     * Created By : Venkat
     */
    public static void clickOnAddNewEngagement() {
        WebDriverTasks.waitForElement(addNewEngagement).click();
    }

    /**
     * This method will change engagement date for given row number
     * Created By : Venkat
     *
     * @param rowNo   This is row/record number for which engagement date will we changed, row number starts from 0
     * @param newDate This is new date
     * @return boolean Is date is changed successfully the it will be True else False
     */
    public static boolean changeEngagementDateForRow(int rowNo, String newDate) {
        By action = By.cssSelector(String.format("div.ui-grid-canvas>div.ui-grid-row:nth-child(%d)>div[role='row']>div:nth-child(1)>div>a", rowNo + 1));
        WebDriverTasks.waitForElement(action).click();
        WebDriverTasks.wait(1);
        WebDriverTasks.waitForElement(viewEngagementButton).click();
        WebDriverTasks.waitForElement(editEngagementsButton).click();
        selectEngagementDate(newDate);
        saveEngagement();
        return true;
    }

    /**
     * This method will expand chage log pane in client details tab
     * Created By : Venkat
     */
    public static void expandChangeLog() {
        if (WebDriverTasks.verifyIfElementIsDisplayed(expandChangeLog, 60))
            WebDriverTasks.waitForElement(expandChangeLog).click();
    }

    /**
     * This method will verify that change log contains valid 'Changed By' value in first row of change log data grid
     * Created By : Venkat
     *
     * @param changedBy This is expected value
     * @return boolean If actual value matched expected value then it will be True else False
     */
    public static boolean verifyLastClientCategoryChangedByInLog(String changedBy) {
        return WebDriverTasks.waitForElement(engagementLogChangedByForFirstRow).getText().equals(changedBy);
    }

    /**
     * This method will verify that change log contains expected 'Client Category' value in first row of change log data grid
     * Created By : Venkat
     *
     * @param expectedChange This is expected value
     * @return boolean If actual value matched expected value then it will be True else False
     */
    public static boolean verifyLatestClientCategoryChangeInLog(String expectedChange) {
        return WebDriverTasks.waitForElement(engagementLogClientCategoryForFirstRow).getText().equals(expectedChange);
    }


    /**
     * This method will verify that values in Year dropdown in DOB field starts with expected year and is selectable
     * Created By : Venkat
     *
     * @param expectedYear This is expected year
     * @return If values in Year dropdown starts with provided values it will return True else False
     */
    public static boolean verifyDOBYearStartsWith(String expectedYear) {
        WebDriverTasks.waitForElement(clientDOB).click();
        Select dobDropDown = WebDriverTasks.waitForElementAndSelect(dateOfBirthYearDropdown);
        WebElement firstOption = dobDropDown.getOptions().get(0);
        if (firstOption.getText().trim().equals(expectedYear)) {
            dobDropDown.selectByVisibleText(expectedYear);
            return true;
        } else {
            return false;
        }

    }

    /**
     * This method will click on Case Information tab
     * Created By : Venkat
     */
    public static void clickOnCaseInformationTab() {
        WebDriverTasks.waitForElement(caseInformationTab).click();
    }

    /**
     * This method will verify that when call cases are deleted Delete Case popup is displayed
     * Created By : Venkat
     *
     * @return boolean If popup is displayed then it will return True else False
     */
    public static boolean verifyDeleteCasePopupDisplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(deleteCasePopup, 10);
    }

    /**
     * This method will verify that radio option 'Update this client's status to Engaged Unsheltered Prospect' is displayed
     * Created By : Venkat
     *
     * @return boolean If radio option is displayed then it will return True else False
     */
    public static boolean verifyEngagedRadioOptionIsDisplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(makeClientEngagedUnshelteredRadio, 5);
    }

    /**
     * This will click on 'Update this client's status to Engaged Unsheltered Prospect' radio option
     * Created By : Venkat
     */
    public static void clickOnUpdateToEngagedRadio() {
        WebDriverTasks.waitForElement(makeClientEngagedUnshelteredRadio).click();
    }

    /**
     * This method will click on Next button
     * Created By : Venkat
     */
    public static void clickOnNextButton() {
        WebDriverTasks.waitForElement(nextButton).click();
    }

    /**
     * This method will verify that Client Information tab is displayed by default
     * Created By : Venkat
     *
     * @return boolen If displayed then it will return True else False
     */
    public static boolean verifyClientInformationPageIsDisplayed() {
        WebDriverTasks.waitForAngular();
        return WebDriverTasks.verifyIfElementIsDisplayed(activeClientInformationTabButton, 30);
    }

    /**
     * This method will return current client category text
     * Created By : Venkat
     *
     * @return String This will contain current client category
     */
    public static String getClientCategory() {
        return WebDriverTasks.waitForElement(clientCategoryText).getText();
    }

    /**
     * This method will verify that given tab is displayed or not
     * Created By : Venkat
     *
     * @param tabName This is tab name
     * @return boolean If tab is displayed then it will be True else False
     */
    public static boolean verifyTabDisplayed(String tabName) {
        By tab = By.xpath(String.format(dynamicClientDetailsPageTabButton, tabName));
        return WebDriverTasks.verifyIfElementIsDisplayed(tab, 5);
    }

    /**
     * This method will get count of visible tabs on page
     * Created By : Venkat
     *
     * @return Int This is count of visible tabs
     */
    public static int getVisibleTabsCount() {
        int actualTabCount = WebDriverTasks.waitForElements(allVisibleTabs).size();
        return actualTabCount;
    }

    /**
     * This method will verify that a client category change notification when Client category is autoflipped to 'Engaged Unsheltered Prospect'
     * Created By : Venkat
     *
     * @return boolean If notification is displayed then it will return True else False
     */
    public static boolean verifyClientCategoryChangedToEngagedSuccessMessageIsDisplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(clientChangedToEngagedSuccessMessage, 20);
    }

    /**
     * This method will verify that a client category change notification when Client category is autoflipped to 'Other Prospect'
     * Created By : Venkat
     *
     * @return boolean If notification is displayed then it will return True else False
     */
    public static boolean verifyClientCategoryChangedToOtherProspectSuccessMessageIsDisplayed() {
        return WebDriverTasks.verifyIfElementIsDisplayed(clientChangedToOtherProspectSuccessMessage, 20);
    }


    /**
     * This method will return autoflip notification text
     * Created By : Venkat
     *
     * @return String This contains notification text
     */
    public static String getAutoflipNotificationText() {
        return WebDriverTasks.waitForElement(clientCategoryChangedSuccessMessage).getText().trim();
    }

    /**
     * This method will return latest engagement date
     * Created By : Venkat
     *
     * @return String This will return latest engagement date
     */
    public static String getLatestEngagementDate() {
        clickOnEngagementsTab();
        return WebDriverTasks.waitForElement(latestEngagementDate).getAttribute("title");
    }
    
    public static void scrollToEnrollmentEndDate() {
        WebDriverTasks.scrollToElement(enrollmentEndDateOnEnrolledClient);
    }
    
    public static void clickOnEnrollmentEndDate() {
        WebDriverTasks.waitForElement(enrollmentEndDateOnEnrolledClient).click();
    }
    
    public static void clickOnOKConfirm() {
        WebDriverTasks.waitForElement(okConfirm).click();
    }
    
    public static void clickOnReOpenButton() {
        WebDriverTasks.waitForElement(reOpenButton).click();;
    }
    
    //Close Enrollment pop-up actions
    public static void clickOnCaseEndDateToOpenCalendar() {
        WebDriverTasks.waitForElement(itemCaseEndDate).click();
    }
    
    public static void selectCaseEndReason(String caseEndReason) {
        WebDriverTasks.waitForElementAndSelect(itemCaseEndReason).selectByVisibleText(caseEndReason);
    }
    
    public static void clickOnSaveButton() {
        WebDriverTasks.waitForElement(saveButton).click();
    }
    
    //Reopen enrollment pop-up actions
    public static void selectProviderOnOpenEnrollMentProvider(String provider) {
        WebDriverTasks.waitForElementAndSelect(reOpenEnrollMentProvider).selectByVisibleText(provider);
    }
    
    public static void selectProviderOnOpenEnrollMentArea(String area) {
        WebDriverTasks.waitForElementAndSelect(reOpenEnrollMentArea).selectByVisibleText(area);
    }
    
    public static void clickOnSaveButtonReOpenEnrollMentPopup() {
        WebDriverTasks.waitForElement(saveOnReopenEnrollmentPopup).click();
    }
    
    
  
    
   


}
