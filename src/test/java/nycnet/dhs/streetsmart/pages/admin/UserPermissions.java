package nycnet.dhs.streetsmart.pages.admin;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.ClientList;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

import java.util.HashMap;

public class UserPermissions {
	static By webinarsPageHeader  = By.xpath("//h1[@class='page-header '][contains(text(), 'Webinars')]");
	
	static By searchInput  = By.id("userDetailListSearchText");
	
	static By actionButton  = By.linkText("Action");
	
	static By viewDetailsButtonOption  = By.linkText("View Details");
	
	static By editUserDetails  = By.id("editUserDetailsIcon");
	
	static By saveUserDetailsButton  = By.xpath("//*[@ng-click='saveUserPermissions()']");
	
	static By clickOnUserRoleDropDown  = By.xpath("//*[@id='ddnRole']//*[@class='btn btn-sm btn-primary dropdown-toggle ng-binding']");

	//Added by Venkat
	static By latestContactDate = By.xpath("//div[@role='button']/span[text()='Latest Contact Date']");
	static By lastDateFieldOnAdvancedFilterPopup = By.cssSelector("div.scroll-filter-option>div:last-child>*>*>*>*>input");
	static By selectedLatestContactDateColumn = By.xpath("//button[normalize-space(text())='Latest Contact Date']/i[@class='ui-grid-icon-ok']");
	static By deselectedLatestContactDateColumn = By.xpath("//button[normalize-space(text())='Latest Contact Date']/i[@class='ui-grid-icon-cancel']");
	static By lastComboBoxOnAdvancedFilterPopup = By.cssSelector("div.scroll-filter-option>div:last-child>*>*>*>*>ul>li>input");
	static By resetFilters = By.xpath("//button[text()='Reset']");
	static By closeAdvancedFiltersPopup = By.xpath("//*[text()='Close']");
	static By appliedFilters = By.xpath("//div[@ng-if='val.Selected']");
	static By advancedFilterPopup = By.cssSelector("div.modal-advance-filter");


	public static void enterQueryInSearchInput(String query) {
		 WebDriverTasks.waitForElement(searchInput).sendKeys(query);
		  System.out.println("Entered the search query - " + query);
	  }
	 
	 public static void submitQueryInSearchInput() {
		 WebDriverTasks.waitForElement(searchInput).sendKeys(Keys.ENTER); 
	  }
	 
	 public static void clickOnActionButtonOnGridResults() {
		 WebDriverTasks.waitForElement(actionButton).click(); 
	  }
	 
	 public static void clickOnViewDetailsButtonOnGridResults() {
		 WebDriverTasks.waitForElement(viewDetailsButtonOption).click(); 
	  }
	 
	 public static void clickOnEditUserDetails() {
		 WebDriverTasks.waitForElement(editUserDetails).click(); 
	  }
	 
	 public static void scrollToSaveUserDetailsButton() { 
		 WebDriverTasks.scrollToElement(saveUserDetailsButton);
	  }
	 
	 public static void clickOnSaveUserDetailsButton() {
		 WebDriverTasks.waitForElement(saveUserDetailsButton).click(); 
	  }
	 
	 public static void clickOnUserRoleDropDown() {
		 WebDriverTasks.waitForElement(clickOnUserRoleDropDown).click(); 
	  }
	 
	 public static void selectOnUserRoleDropDownOption(String dropDownOption) {
		 WebDriverTasks.waitForElement(By.linkText(dropDownOption)).click(); 
	  }


	/**
	 * This method will apply advanced filter for given column with given values
	 * Created By : Venkat
	 * @param columnName This is column name
	 * @param values This is filter values, in case of multiple values provide multiple values separated by comma(,)
	 * @param clearExistingSelection If existing advanced filters needs to be cleared before applying filter provide True if not then False
	 */
	public static void advancedFilterBy(String columnName, String values,String columnNameOnDropdown,boolean clearExistingSelection) {
		//Click on Advanced filter
		ClientList.clickOnAdvancedFilter();
		WebDriverTasks.wait(3);
		//Open Dropdown
		ClientList.clickOnClientCategoryFilterButton();
		//Clear existing checked checkboxes
		if (clearExistingSelection)
			ClientList.clearExistingSelection();

		String columnCheckbox = String.format("//li/label[@title='%s']/input[@type='checkbox']", columnName);
		String newFilterAdded = String.format("//div[contains(@class,'filter-options')]//button/div/span[normalize-space(@title)='%s']", columnNameOnDropdown);
		By selectColumn = By.xpath(columnCheckbox);
		By newFilterDropdown = By.xpath(newFilterAdded);
		String filterByValue = "div.open>ul.advance-filter-container>li>label[title='%s']>input";

		//Mark target column checkbox
		WebDriverTasks.waitForElement(selectColumn).click();

		//Check if filter by value is date
		if (Helpers.isValidDate(values, "MM/dd/yyyy")) {
			Base.selectDate(lastDateFieldOnAdvancedFilterPopup, values);
		} else {
			if(WebDriverTasks.verifyIfElementIsDisplayed(lastComboBoxOnAdvancedFilterPopup,3)){
				String filterBy[] = values.split(",");
				for (String value : filterBy) {
					WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup).click();
					WebDriverTasks.wait(3);
					WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup).sendKeys(value);
					WebDriverTasks.wait(5);
					WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup).sendKeys(Keys.DOWN);
					WebDriverTasks.waitForElement(lastComboBoxOnAdvancedFilterPopup).sendKeys(Keys.ENTER);
				}
			} else {
				//Open dropdown
				WebDriverTasks.waitForElement(newFilterDropdown).click();
				//If multiple filter values to be selected, split each value separated by comma
				String filterBy[] = values.split(",");
				for (String value : filterBy) {
					By selectFilterByValue = By.cssSelector(String.format(filterByValue, value.trim()));
					WebDriverTasks.waitForElement(selectFilterByValue).click();
				}
			}
		}
		WebDriverTasks.wait(5);
		//Click on Apply filter
		ClientList.clickOnApplyFilter();
	}

	/**
	 * This method will open advanced filter popup and click on 'Reset' button
	 * Created By : Venkat
	 */
	public static void resetAdvancedFilters(){
		//Click on Advanced filter
		ClientList.clickOnAdvancedFilter();
		WebDriverTasks.wait(3);
		WebDriverTasks.waitForElement(resetFilters).click();
		WebDriverTasks.wait(3);
		ClientList.clickOnApplyFilter();
		WebDriverTasks.wait(3);
	}

	/**
	 * This method will verify if advanced filter has been removed or not
	 * Created By : Venkat
	 * @return Boolean If filter is removed then it will return True else False
	 */
	public static boolean isFilterRemoved() {
		WebDriverTasks.waitForAngular();
		return !WebDriverTasks.verifyIfElementIsDisplayed(appliedFilters,5);
	}

	/**
	 * This method will click on 'Advanced Filter' button and verify that popup is displayed
	 * Created By : Venkat
	 * @return Boolean If popup is displayed then it will return True else False
	 */
	public static boolean openAdvancedFilterPopup(){
		//Click on Advanced filter
		ClientList.clickOnAdvancedFilter();
		WebDriverTasks.wait(3);
		return WebDriverTasks.verifyIfElementIsDisplayed(advancedFilterPopup,5);
	}

	/**
	 * THis method will click on 'Close' button on advanced filter popup and verify that popup is closed
	 * Created By : Venkat
	 * @return Boolean If popup is closed then it will return True else False
	 */
	public static boolean closeAdvancedFilterPopup(){
		WebDriverTasks.waitForElement(closeAdvancedFiltersPopup).click();
		WebDriverTasks.waitForAngular();
		WebDriverTasks.wait(3);
		return !WebDriverTasks.verifyIfElementIsDisplayed(advancedFilterPopup,5);
	}

}
