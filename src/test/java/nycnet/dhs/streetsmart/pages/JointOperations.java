package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class JointOperations extends StartWebDriver{
	
	static By jointoperationsMenutItem = By.xpath("//a[contains(text(),'Joint Operations')]");
	static By dateFilterYearlyMenu  = By.id("jointOperationsGrid-Yearly");
	
	public static String returnJointOperationsPageHeader() {
		String jointoperationsPageHeaderText = WebDriverTasks.waitForElement(jointoperationsMenutItem).getText();
		System.out.println("The page header is " +jointoperationsPageHeaderText);
		return jointoperationsPageHeaderText;
		
	}
	
	public static void clickOnDateFilterYearlyMenu() {
		 WebDriverTasks.waitForElement(dateFilterYearlyMenu).click();
	 }

}
