package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class DailyDropIn {
	
	static By daildropinMenuItem = By.xpath("//a[contains(text(),'Daily Drop-In')]");

	public static String returndaildropinPageHeader() {
		  String daildropinPageHeaderText =  WebDriverTasks.waitForElement(daildropinMenuItem).getText();
		  System.out.println("The page header is " + daildropinPageHeaderText);
		  return daildropinPageHeaderText;
	  }

}
