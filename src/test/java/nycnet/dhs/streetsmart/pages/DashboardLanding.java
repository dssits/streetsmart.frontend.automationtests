package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class DashboardLanding {
	static By dashboardLandingMenuItem = By.xpath("//span[contains(text(),'Dashboard')]");

	public static String returndashboardLandingPageHeader() {
		  String dashboardLandingPageHeaderText =  WebDriverTasks.waitForElement(dashboardLandingMenuItem).getText();
		  System.out.println("The page header is " + dashboardLandingPageHeaderText);
		  return dashboardLandingPageHeaderText;
	  }

}
