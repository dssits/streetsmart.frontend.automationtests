package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class Reports extends StartWebDriver{
	
	static By reportsMenu = By.xpath("/html[1]/body[1]/div[2]/div[2]/div[1]/div[1]/div[1]/ul[1]/li[11]/a[1]");
	
	public static String returnreportsPageHeader() {
		String reportsPageHeaderText = WebDriverTasks.waitForElement(reportsMenu).getText();
		System.out.println("The page header is:" +reportsPageHeaderText);
		return reportsPageHeaderText;
		
	}
}
