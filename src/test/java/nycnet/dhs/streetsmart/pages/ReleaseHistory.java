package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class ReleaseHistory {
	static By releaseHistoryPageHeader  = By.xpath("//h1[@class='page-header '][contains(text(), 'Release History')]");
	
	 public static String returnReleaseHistoryPageHeader() {
		  String releaseHistoryPageHeaderText = WebDriverTasks.waitForElement(releaseHistoryPageHeader).getText();
		  System.out.println("The page header is " + releaseHistoryPageHeaderText);
		  return releaseHistoryPageHeaderText;
	  }

}
