package nycnet.dhs.streetsmart.pages;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddClient extends StartWebDriver {
	//Added By Venkat
	static By clientDOB = By.xpath("//div[(@id='clinetSearchPanel' or @class='panel-body') and not(@style='display: none;')]//div[@id='dubcollapse1' or @id='collapse1' ]//input[@name='DateOfBirth']");
	static By dateOfBirthYearDropdown = By.xpath("//div[@id='ui-datepicker-div' and contains(@style,'display: block;')]//select[@data-handler='selectYear']");

	static By collapseDuplicateSearchButton = By.xpath("//h4/b[text()='Duplicate Search']/ancestor::div[@class='panel-heading']//a[@data-click='panel-collapse']/i[@class='fa fa-minus']");
	static By expandClientSearchButton = By.xpath("//h4/b[text()='Client Search']/ancestor::div[@class='panel-heading']//a[@data-click='panel-collapse']/i[@class='fa fa-plus']");
	//-----

	/**
	 * This method will collapse Duplicate search pane
	 * Created By - Venkat
	 */
	public static void collapseDuplicateSearchPane(){
		if(WebDriverTasks.verifyIfElementIsDisplayed(collapseDuplicateSearchButton,10)) {
			WebDriverTasks.scrollToElement(collapseDuplicateSearchButton);
			WebDriverTasks.waitForElement(collapseDuplicateSearchButton).click();
			WebDriverTasks.wait(3);
		}
	}

	/**
	 * This method will expand Client search pane
	 * Created By - Venkat
	 */
	public static void expandClientSearchPane(){
		if(WebDriverTasks.verifyIfElementIsDisplayed(expandClientSearchButton,10)){
			WebDriverTasks.waitForElement(expandClientSearchButton).click();
			WebDriverTasks.wait(3);
		}
	}

	/**
	 * This method will verify that values in Year dropdown in DOB field starts with expected year and is selectable
	 * @param expectedYear This is expected year
	 * @return If values in Year dropdown starts with provided values it will return True else False
	 */
	public static boolean verifyDOBYearStartsWith(String expectedYear){
		WebDriverTasks.waitForElement(clientDOB).click();
		Select dobDropDown = WebDriverTasks.waitForElementAndSelect(dateOfBirthYearDropdown);
		WebElement firstOption = dobDropDown.getOptions().get(0);
		if (firstOption.getText().trim().equals(expectedYear)) {
			dobDropDown.selectByVisibleText(expectedYear);
			return true;
		} else {
			return false;
		}
	}
	
}
