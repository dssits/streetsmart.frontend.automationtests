package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class Census extends StartWebDriver {
	
	static By censusMenuItem = By.xpath("//div[@id='sidebar']//li[1]//ul[1]//li[1]//a[1]");
	
	public static String returncensusPageHeader() {
		String censusPageHeaderText = WebDriverTasks.waitForElement(censusMenuItem).getText();
		System.out.println("The page header is: " +censusPageHeaderText);
		return censusPageHeaderText;
	}
}
