package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class Placement extends StartWebDriver {
	static By placementMenuItem = By.xpath("//a[contains(text(),'Placement')]");
	
	public static String returnplacementPageHeader() {
		String placementPageHeaderText = WebDriverTasks.waitForElement(placementMenuItem).getText();
		System.out.println("The page header is:" +placementPageHeaderText);
		return placementPageHeaderText;
	}
	

}
