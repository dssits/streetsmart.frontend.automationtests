package nycnet.dhs.streetsmart.pages;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.pages.dashboard.DailyOutreach;
import nycnet.dhs.streetsmart.pages.reports.ViewReports;
import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ServiceRequests extends StartWebDriver {

    //Service Request Map locators
    static By serviceRequestMapHeading = By.xpath("//*[@id='servicerequestmap-all-collapsable-table']//h1");
    static By serviceRequestMapSectionCollapseExpand = By.xpath("//*[@id='change-log-accordion1']/i[@class='fa fa-plus-circle pull-right']");
    static By serviceRequestMapBoroughDropDownMenu = By.xpath("//*[@ng-model='ServiceRequestMapBoroughId']");
    static By serviceRequestMapStatusDropDownMenu = By.xpath("//*[@ng-model='ServiceRequestMapStatusId']");
    static By serviceRequestMapGoButton = By.xpath("//*[@ng-click='GetServiceRequestMapData()']");

    static By servicerequestsMenuItem = By.xpath("//a[contains(text(),'311 Service Requests')]");
    static By receivedDurationColumnSortingDirection = By.xpath("//*[@role='button']//*[contains(text(),'Received Duration')]/following-sibling::*");
    static By assignedDurationColumnSortingDirection = By.xpath("//*[@role='button']//*[contains(text(),'Assigned Duration')]/following-sibling::*");
    static By assignedTileDetailsButton = By.xpath("(//*[@id='widgets']//*[contains(text(),'Details')])[2]");

    //Onsite Tile and grid locators
    static By OnsiteTileDetailsButton = By.xpath("(//*[@id='widgets']//*[contains(text(),'Details')])[3]");
    static By OnsiteColumnSortingDirection = By.xpath("//*[@role='button']//*[contains(text(),'Onsite Time')]/following-sibling::*");
    static By OnsiteColumn = By.xpath("//*[@role='button']//*[contains(text(),'Onsite Time')]");
    static By onsiteTimeOnColumnPicker = By.xpath("//*[@class='ui-grid-menu-item ng-binding'][contains(text(), 'Onsite Time')]");
    static By onsiteTileTotalCount = By.xpath("//h4/span[text()='On Site']/ancestor::div[contains(@class,'panel-heading')]/following-sibling::div//div[contains(@class,'stats-number')]");

    //Service request General locators
    static By gridNoRecordsFound = By.xpath("//*[@ng-show='gridOptions.data.length == 0']");
    static By serviceRequestOnTitle = By.xpath("//*[@class='panel-title ng-binding']");
    static By closeOutServiceRequestButton = By.xpath("//button[@data-toggle='dropdown' and contains(normalize-space(),'Close Out Request')]");
    static By closeOutServiceRequestAdministrativelyClosedOption = By.xpath("//ul[@class='dropdown-menu' and contains(@style,'display: block;')]/li/a[text()='Administratively Closed']");

    //Closed/Resolved Tile and grid locators
    static By closedResolvedTileDetailsButton = By.xpath("(//*[@id='widgets']//*[contains(text(),'Details')])[4]");
    static By closedResolvedTileTotalCount = By.xpath("//h4/span[text()='Closed/Resolved']/ancestor::div[contains(@class,'panel-heading')]/following-sibling::div//div[contains(@class,'stats-number')]");

    //Email Configuration locators
    static By emailConfigurationButton = By.xpath("//*[@ng-click='Configure311Email()']");
    static By emailConfigurationFromInput = By.xpath("//*[@ng-model='SR311EmailConfigurations.FromContains']");
    static By emailConfigurationSubjectInput = By.xpath("//*[@ng-model='SR311EmailConfigurations.SubjectContains']");
    static By emailConfigurationBodyInput = By.xpath("//*[@ng-model='SR311EmailConfigurations.BodyContains']");
    static By emailConfigurationSendAfterCalender = By.xpath("//*[@ng-model='SR311EmailConfigurations.SendAfter']");
    static By emailConfigurationSaveButton = By.xpath("//*[@ng-click='UpdateConfigurations()']");
    static By emailConfigurationCloseButton = By.xpath("//button[@class='btn btn-default'][@ng-click='Close()']");

    //Added By Venkat
    static By pageHeader = By.xpath("//span[normalize-space()='311 Service Requests' and @class='page-header']");
    static By unassignedCount = By.xpath("//h4/span[text()='Unassigned']/ancestor::div[contains(@class,'panel-heading')]/following-sibling::div//div[contains(@class,'stats-number')]");
    static By assignedCount = By.xpath("//h4/span[text()='Assigned']/ancestor::div[contains(@class,'panel-heading')]/following-sibling::div//div[contains(@class,'stats-number')]");
    static By assignedDetailsLink = By.xpath("//h4/span[text()='Assigned']/ancestor::div[contains(@class,'panel-heading')]/following-sibling::div[contains(@class,'new-view-details')]//a[normalize-space()='Details']");
    static By onSiteDetailsLink = By.xpath("//h4/span[text()='On Site']/ancestor::div[contains(@class,'panel-heading')]/following-sibling::div[contains(@class,'new-view-details')]//a[normalize-space()='Details']");
    static By closedDetailsLink = By.xpath("//h4/span[text()='Closed/Resolved']/ancestor::div[contains(@class,'panel-heading')]/following-sibling::div[contains(@class,'new-view-details')]//a[normalize-space()='Details']");
    static String providerCount = "//h4/span[text()='Assigned']/ancestor::div[contains(@class,'panel-heading')]/following-sibling::div//li[contains(normalize-space(),'%s')]/span";
    static By serviceRequestLink = By.cssSelector("div#serviceReqGrid>div.ui-grid-contents-wrapper>div[role='grid']>div.ui-grid-viewport>div>div:first-child>div>div:nth-child(1)>div>a");
    static By selectProviderButton = By.xpath("//button[@data-toggle='dropdown' and contains(normalize-space(),'Assign to')]");
    static By reassignToProviderOption = By.xpath("//button[@data-toggle='dropdown' and contains(normalize-space(),'Reassign to')]");
    static String providerOption = "//ul[@class='dropdown-menu' and contains(@style,'display: block;')]/li/a[text()='%s']";
    static By incidentDetailsHeader = By.xpath("//h1[text()='Incident Details']");
    static By searchAll = By.id("searchAllSearchText");
    static By searchServiceRequest = By.id("serviceRequestgSearchText");
    static By unassignedGridRecordCount = By.cssSelector("div.ui-grid-pager-panel>div>div.ui-grid-pager-count>span");
    static By confirmProvider = By.xpath("//button[@ng-click='ok()' and text()='Confirm']");
    static By expandSearchAll = By.xpath("//a[normalize-space()='Search All']/i[contains(@class,'fa-plus')]");
    static String generalTableCells = "div#%s>div.ui-grid-contents-wrapper>div[role='grid']>div.ui-grid-viewport>div>div>div>div:nth-child(%d)>div";
    static String generalTableHeaders = "div#%s>div.ui-grid-contents-wrapper>div[role='grid']>div.ui-grid-header>div>div>div>div>div>div.ui-grid-header-cell";
    static String generalNextGrid = "//div[@id='%s']//button[@title='Page forward']";
    static String generalFirstGridPage = "//div[@id='%s']//button[@title='Page to first']";
    static By assignedToProvider = By.xpath("//label[text()='Assigned Team']/following-sibling::label");
    static By dailyFilterButton = By.xpath("//button[normalize-space()='Daily']");
    static By monthlyFilterButton = By.xpath("//button[normalize-space()='Monthly']");
    static By yearlyFilterButton = By.xpath("//button[normalize-space()='Yearly']");
    static By yearlyFilterDropdown = By.xpath("//select[@ng-model='selectYear']");
    //Month Picker
    static By showYears = By.xpath("//div[@date-picker and @style='display: block;']//td[@class='month-picker-title']");
    static By monthPicker = By.xpath("//div[@date-picker and @style='display: block;']//div[contains(@id,'MonthPicker_')]");
    //Excel Download
    static By downloadExcelButton = By.xpath("//a[@data-toggle='dropdown' and normalize-space()='Excel']");
    static String downloadBy = "//ul/li/a[normalize-space()='%s']";
    static By downloadSelectedRecords = By.xpath("//ul/li/a[normalize-space()='Selected Records']");
    static By downloadAllRecords = By.xpath("//ul/li/a[normalize-space()='All Records']");
    static By downloadCurrentView = By.xpath("//ul/li/a[normalize-space()='Current View']");
    static By exportCurrentViewConfirmationButton = By.xpath("//button[@id='nonDHSVeteranDataSaveButton' and text()='OK']");
    static By recordSelector = By.cssSelector("div#serviceReqGrid>div.ui-grid-contents-wrapper>div.ui-grid-pinned-container>div>div>div>div.ui-grid-row>div>div>div>div>div.ui-grid-selection-row-header-buttons");
    static By recordsPagerDetails = By.xpath("//div[@class='ui-grid-pager-count']/span[not(contains(@class,'ng-hide'))]");
    static By recordsInGrid = By.cssSelector("div#serviceReqGrid>div.ui-grid-contents-wrapper>div[role='grid']>div.ui-grid-viewport>div>div>div[role='row']");

    public enum TableType { Unassigned, All}

    //Service Request Map actions
    public static String returnServiceRequestMapHeading() {
        String Text = WebDriverTasks.waitForElement(serviceRequestMapHeading).getText();
        System.out.println("The service request Map page heading is :" + Text);
        return Text;
    }

    public static void clickOnServiceRequestMapSectionToExpandCollapse() {
        WebDriverTasks.waitForElement(serviceRequestMapSectionCollapseExpand);
        WebDriverTasks.clickOnElementJavascriptExcecutor(serviceRequestMapSectionCollapseExpand);
    }

    public static void selectMapsBoroughOnDropDown(String borough) {
        WebDriverTasks.waitForElementAndSelect(serviceRequestMapBoroughDropDownMenu).selectByVisibleText(borough);
    }

    public static void selectMapstatusOnDropDown(String mapStatus) {
        WebDriverTasks.waitForElementAndSelect(serviceRequestMapStatusDropDownMenu).selectByVisibleText(mapStatus);
    }

    public static boolean verifyPresenceOfServiceRequestMapHeading() {
        return WebDriverTasks.verifyIfElementExists(serviceRequestMapHeading);
    }

    public static boolean verifyPresenceOfGoButton() {
        return WebDriverTasks.verifyIfElementExists(serviceRequestMapGoButton);
    }

    public static String returnservicerequestsPageHeader() {
        String servicerequestsPageHeaderText = WebDriverTasks.waitForElement(servicerequestsMenuItem).getText();
        System.out.println("The page header is:" + servicerequestsPageHeaderText);
        return servicerequestsPageHeaderText;
    }

    public static String receivedDurationColumnSortingDirection() {
        String receivedDurationColumnSortingDirectionAttribute = WebDriverTasks.waitForElement(receivedDurationColumnSortingDirection).getAttribute("aria-label");
        System.out.println("The column sorting direction is :" + receivedDurationColumnSortingDirectionAttribute);
        return receivedDurationColumnSortingDirectionAttribute;
    }

    public static String assignedDurationColumnSortingDirection() {
        String assigneddDurationColumnSortingDirectionAttribute = WebDriverTasks.waitForElement(assignedDurationColumnSortingDirection).getAttribute("aria-label");

        System.out.println("The column sorting direction is :" + assigneddDurationColumnSortingDirectionAttribute);
        return assigneddDurationColumnSortingDirectionAttribute;
    }

    public static void clickOnAssignedDurationDetails() {
        WebDriverTasks.waitForElement(assignedTileDetailsButton).click();
    }

    //Onsite Tile and grid actions
    public static void clickOnOnsiteDetailsButtonToOpenGrid() {
        WebDriverTasks.waitForElement(OnsiteTileDetailsButton).click();
    }

    public static void clickOnOnsiteColumnOnColumnPickerToEnableIt() {
        WebDriverTasks.waitForElement(onsiteTimeOnColumnPicker).click();
    }

    public static String OnsiteTimeColumnSortingDirection() {
        String text = WebDriverTasks.waitForElement(OnsiteColumnSortingDirection).getAttribute("aria-label");
        System.out.println("The column sorting direction is :" + text);
        return text;
    }

    public static boolean verifyIfOnsiteColumn() {
        return WebDriverTasks.verifyIfElementExists(OnsiteColumn);
    }

    public static String returnOnsiteColumnText() {
        WebDriverTasks.verifyIfElementExists(OnsiteColumn);
        String Text = WebDriverTasks.waitForElement(OnsiteColumn).getText();
        System.out.println("The value of the onsite column is " + Text);
        return Text;
    }

    /**
     * This method will return count of unassigned records from 'Unassigned' tile
     * Created By : Venkat
     *
     * @return Integer This is count of unassigned records
     */
    public static int getUnassignedRecordCount() {
        return Integer.parseInt(WebDriverTasks.waitForElement(unassignedCount).getText().trim());
    }

    /**
     * This method will return count of assigned records from 'Assigned' tile
     * Created By : Venkat
     *
     * @return Integer This is count of assigned records
     */
    public static int getAssignedRecordCount() {
        return Integer.parseInt(WebDriverTasks.waitForElement(assignedCount).getText().trim());
    }

    /**
     * This method will return count of records assigned to given provider
     * Created By : Venkat
     *
     * @param providerName This is provider pseudo name
     * @return Integer This is count of records assigned to given provider
     */
    public static int getProviderCount(String providerName) {
        By count = By.xpath(String.format(providerCount, providerName));
        return Integer.parseInt(WebDriverTasks.waitForElement(count).getText().trim());
    }

    /**
     * This method will click on service request link from unassigned table and return service request code
     * Created By : Venkat
     *
     * @return String This is service request code
     */
    public static String clickOnServiceRequest() {
        WebDriverTasks.waitForAngular();
        assert WebDriverTasks.verifyIfElementIsDisplayed(serviceRequestLink, 10);
        String serviceRequestNo = WebDriverTasks.waitForElement(serviceRequestLink).getText().trim();
        WebDriverTasks.waitForElement(serviceRequestLink).click();
        return serviceRequestNo;
    }

    /**
     * This method will click on provided service request code link
     * Created By : Venkat
     *
     * @param requestNo This is service request code no.
     */
    public static void clickOnServiceRequest(String requestNo) {
        WebDriverTasks.waitForElement(By.linkText(requestNo)).click();
        return;
    }

    /**
     * This method will select given provider and also confirms the confirmation popup
     * Created By : Venkat
     *
     * @param provider This is provider to be selected
     */
    public static void selectProvider(String provider) {
        WebDriverTasks.waitForElement(selectProviderButton).click();
        WebDriverTasks.wait(3);
        By option = By.xpath(String.format(providerOption, provider));
        WebDriverTasks.waitForElement(option).click();
        WebDriverTasks.wait(3);
        if (WebDriverTasks.verifyIfElementIsDisplayed(confirmProvider, 10))
            WebDriverTasks.waitForElement(confirmProvider).click();
    }


    /**
     * This method will select given provider and also confirms the confirmation popup
     * Created By : Venkat
     *
     * @param provider This is provider to be selected
     */
    public static void reassignToProvider(String provider) {
        WebDriverTasks.waitForElement(reassignToProviderOption).click();
        By option = By.xpath(String.format(providerOption, provider));
        WebDriverTasks.waitForElement(option).click();
        if (WebDriverTasks.verifyIfElementIsDisplayed(confirmProvider, 10))
            WebDriverTasks.waitForElement(confirmProvider).click();
    }


    /**
     * This method will verify that Incident details popup is displayed or not
     * Created By : Venkat
     *
     * @return Boolean If Incident Details popup is displayed then it will return True else False
     */
    public static boolean verifyIncidentDetailsPopup() {
        WebDriverTasks.waitForAngular();
        return WebDriverTasks.verifyIfElementIsDisplayed(incidentDetailsHeader, 10);
    }

    /**
     * This method will expand Search all section
     * Created By : Venkat
     */
    public static void expandSearchAll() {
        if (WebDriverTasks.verifyIfElementIsDisplayed(expandSearchAll, 5))
            WebDriverTasks.waitForElement(expandSearchAll).click();
    }

    /**
     * This method will search for provided value in Search Table section
     * Created By : Venkat
     *
     * @param searchFor This is value to be searched
     */
    public static void searchAll(String searchFor) {
        WebDriverTasks.waitForElement(searchAll).clear();
        WebDriverTasks.waitForElement(searchAll).sendKeys(searchFor);
        WebDriverTasks.waitForElement(searchAll).sendKeys(Keys.ENTER);
    }

    /**
     * This method will search for provided value in Unassigned table
     * Created By : Venkat
     *
     * @param searchFor This is value to be searched
     */
    public static void searchServiceRequest(String searchFor) {
        WebDriverTasks.waitForElement(searchServiceRequest).clear();
        WebDriverTasks.waitForElement(searchServiceRequest).sendKeys(searchFor);
        WebDriverTasks.waitForElement(searchServiceRequest).sendKeys(Keys.ENTER);
    }

    /**
     * This method will return total records available in Unassigned frid table
     * Created By : Venkat
     *
     * @return Integer This is unassigned grid record count
     */
    public static int getUnassignedGridCount() {
        if (!WebDriverTasks.verifyIfElementIsDisplayed(unassignedGridRecordCount, 5))
            return 0;
        String rawString = WebDriverTasks.waitForElement(unassignedGridRecordCount).getText().trim();
        rawString = rawString.split(" of ")[1];
        rawString = rawString.split(" ")[0].trim();
        return Integer.parseInt(rawString);
    }

    /**
     * This method will verify search result for given table type, for given column name with expected data.
     * Created By : Venkat
     *
     * @param tableType    This is table type, it can be Unassigned or All.
     * @param columnName   This is column name for which data is to be validated
     * @param expectedData This is expected data to be displayed in given column
     * @return Boolean If all the data for given column matches with expected data then it will return True else False
     */
    public static boolean verifySearchResult(TableType tableType, String columnName, String expectedData) {
        String tableID;
        int loopWatcher = 3;
        boolean hasNext, isValid = true;
        if (tableType == TableType.All)
            tableID = "SearchAllGrid";
        else
            tableID = "serviceReqGrid";
        By tableHeaders = By.cssSelector(String.format(generalTableHeaders, tableID));
        By nextDataGridPage = By.xpath(String.format(generalNextGrid, tableID));
        By firstDataGridPage = By.xpath(String.format(generalFirstGridPage, tableID));
        int columnNo = getColumnNo(tableHeaders, columnName);
        String dataCellsCss = String.format(generalTableCells, tableID, columnNo);
        By dataCells = By.cssSelector(dataCellsCss);
        do {
            if (WebDriverTasks.verifyIfElementIsDisplayed(dataCells, 5)) {
                WebDriverTasks.waitForElement(dataCells);
                for (WebElement cell : WebDriverTasks.driver.findElements(dataCells)) {
                    if (!cell.getText().trim().contains(expectedData)) {
                        isValid = false;
                        break;
                    }
                }
            }
            hasNext = WebDriverTasks.driver.findElement(nextDataGridPage).isEnabled();
            if (hasNext & isValid)
                WebDriverTasks.waitForElement(nextDataGridPage).click();
            loopWatcher--;
        } while (hasNext & isValid & loopWatcher>0);
        if (WebDriverTasks.driver.findElement(firstDataGridPage).isEnabled()) {
            WebDriverTasks.scrollToElement(firstDataGridPage);
            WebDriverTasks.waitForElement(firstDataGridPage).click();
        }
        return isValid;
    }

    /**
     * This method will get column no for given table
     * Created By : Venkat
     *
     * @param tableHeaders This is table header cells
     * @param columnName   This is column name
     * @return Integer This is column no
     */
    public static int getColumnNo(By tableHeaders, String columnName) {
        List<WebElement> columns = WebDriverTasks.waitForElements(tableHeaders);
        int colNo = 0;
        for (WebElement column : columns) {
            WebDriverTasks.scrollToElement(column);
            if (column.getText().trim().equals(columnName)) {
                WebDriverTasks.scrollToElement(columns.get(0));
                return colNo + 1;
            }
            colNo++;
        }
        return colNo;
    }

    /**
     * This method will verify that provided value is reflected on popup as 'Assigned To' text
     * Created By : Venkat
     *
     * @param assignedTo This is expected assigned to text
     * @return Boolean If provided value is reflected as assigned to text, it will return True else it will return False
     */
    public static boolean verifyAssignedTo(String assignedTo) {
        return WebDriverTasks.waitForElement(assignedToProvider).getText().trim().equals(assignedTo);
    }

    /**
     * This method will click on Details link on 'Assigned' tile
     * Created By : Venkat
     */
    public static void clickAssignedDetailsLink() {
        WebDriverTasks.scrollToElement(pageHeader);
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(assignedDetailsLink).click();
    }


    /**
     * This method will click on Details link on 'On Site' tile
     * Created By : Venkat
     */
    public static void clickOnsiteDetailsLink() {
        WebDriverTasks.scrollToElement(pageHeader);
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(onSiteDetailsLink).click();
    }


    /**
     * This method will click on Details link on 'Closed/Resolved' tile
     * Created By : Venkat
     */
    public static void clickClosedDetailsLink() {
        WebDriverTasks.scrollToElement(pageHeader);
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(closedDetailsLink).click();
    }


    //Onsite tile and grid actions
    public static int returnOnsiteCountOnTileServiceRequest() {
        int count = Integer.parseInt(WebDriverTasks.waitForElement(onsiteTileTotalCount).getText().trim());
        System.out.println("The total count on the onsite tile is " + count);
        return count;
    }

    public static String returnServiceRequestOnTitle() {
        String Text = WebDriverTasks.waitForElement(serviceRequestOnTitle).getText();
        System.out.println("The service request on the Title is " + Text);
        return Text;
    }

    public static void clickOnOnsiteTileDetailsButton() {
        WebDriverTasks.waitForElement(OnsiteTileDetailsButton).click();
    }

    //Service request general actions
    public static void clickOnCloseOutServiceRequestButton() {
        WebDriverTasks.waitForElement(closeOutServiceRequestButton).click();
    }

    public static void chooseCloseOutAdministrativelyClosedOptionOption() {
        WebDriverTasks.waitForElement(closeOutServiceRequestAdministrativelyClosedOption).click();
    }

    //Closed/Resolved Tile and grid locators
    public static int returnClosedResolvedOnCountOnTile() {
        int count = Integer.parseInt(WebDriverTasks.waitForElement(closedResolvedTileTotalCount).getText().trim());
        System.out.println("The total count on the closed/resolved tile is " + count);
        return count;
    }

    public static void clickOnclosedResolvedTileDetailsButton() {
        WebDriverTasks.waitForElement(closedResolvedTileDetailsButton).click();
    }

    public static void clickOnConfirmButton() {
        WebDriverTasks.waitForElement(confirmProvider).click();
    }

    //Email Configuration actions
    public static void clickOnEmailConfigurationButton() {
        WebDriverTasks.waitForElement(emailConfigurationButton).click();
    }

    public static void clickOnEmailConfigurationSaveButton() {
        WebDriverTasks.waitForElement(emailConfigurationSaveButton).click();
    }

    public static void clickOnEmailConfigurationCloseButton() {
        WebDriverTasks.waitForElement(emailConfigurationCloseButton).click();
    }

    public static void clearEmailConfigurationFromInput() {
        WebDriverTasks.waitForElement(emailConfigurationFromInput).clear();
    }

    public static void clearEmailConfigurationSubjectInput() {
        WebDriverTasks.waitForElement(emailConfigurationSubjectInput).clear();
    }

    public static void clearEmailConfigurationBodyInput() {
        WebDriverTasks.waitForElement(emailConfigurationBodyInput).clear();
    }

    public static void clickOnEmailConfigurationSendAfterInput() {
        WebDriverTasks.waitForElement(emailConfigurationSendAfterCalender).click();
    }

    public static void inputTextToEmailConfigurationFromInput(String value) {
        WebDriverTasks.enterTextToInput(emailConfigurationFromInput, value);
    }

    public static void inputTextToEmailConfigurationSubjectInput(String value) {
        WebDriverTasks.enterTextToInput(emailConfigurationSubjectInput, value);
    }

    public static void inputTextToEmailConfigurationBodyInput(String value) {
        WebDriverTasks.enterTextToInput(emailConfigurationBodyInput, value);
    }

    public static String returnTextToEmailConfigurationFromInput() {
        return WebDriverTasks.returnValueOfInput(emailConfigurationFromInput);
    }

    public static String returnTextToEmailConfigurationSubjectInput() {
        return WebDriverTasks.returnValueOfInput(emailConfigurationSubjectInput);
    }

    public static String returnTextToEmailConfigurationBodyInput() {
        return WebDriverTasks.returnValueOfInput(emailConfigurationBodyInput);
    }


    /**
     * This method will select the date
     * Created By : Venkat
     *
     * @param date This is date in MM/DD/YYYY
     */
    public static void filterByDaily(String date) {
        selectDate(dailyFilterButton, date);
    }

    /**
     * This method will click on 'Monthly' option on Engagement Unsheltered Prospect and select provided year and month
     * Created By : Venkat
     *
     * @param year This is year to be selected
     * @param month This is month to be selected
     */
    public static void filterByMonthly(String year, String month) {
        WebDriverTasks.scrollToElement(monthlyFilterButton);
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(monthlyFilterButton).click();
        WebElement calendar = WebDriverTasks.waitForElement(monthPicker);
        WebDriverTasks.waitForElement(showYears).click();
        WebDriverTasks.wait(5);
        calendar.findElement(By.xpath(".//span[text()='" + year + "']")).click();
        WebDriverTasks.wait(2);
        calendar.findElement(By.xpath(".//span[text()='" + month + "']")).click();
    }

    /**
     * This method will set provided value for JCC Client Start Date.
     * Created By : Venkat
     *
     * @param startDate This is start date in mm/dd/yyyy format
     */
    public static void selectDate(By date, String startDate) {
        WebDriverTasks.scrollToElement(date);
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(date).click();
        int day = Integer.parseInt(startDate.split("/")[1]);
        int month = Integer.parseInt(startDate.split("/")[0]);
        int year = Integer.parseInt(startDate.split("/")[2]);
        WebDriverTasks.wait(3);
        selectCalenderDate(day, month, year);
    }

    /**
     * This method will select day,month and year on calendar
     * Created By : Venkat
     *
     * @param day   This is day
     * @param month This is month that can range from 1 to 12
     * @param year  This is year
     */
    public static void selectCalenderDate(int day, int month, int year) {
        WebDriverTasks.waitForAngular();
        By selectYear = By.xpath("//div[@date-picker and contains(@style,'display: block;')]//select[@class='ui-datepicker-year']");
        By selectMonth = By.xpath("//div[@date-picker and contains(@style,'display: block;')]//select[@class='ui-datepicker-month']");
        By selectday = By.xpath("//div[@date-picker and contains(@style,'display: block;')]//td[@data-handler='selectDay']/a[text()='" + day + "']");
        String monthName = Base.monthNames[month - 1];
        WebDriverTasks.wait(2);
        WebDriverTasks.waitForElementAndSelect(selectYear).selectByVisibleText(String.valueOf(year));
        WebDriverTasks.wait(2);
        WebDriverTasks.waitForElementAndSelect(selectMonth).selectByVisibleText(monthName);
        WebDriverTasks.wait(2);
        WebDriverTasks.waitForElement(selectday).click();
    }

    /**
     * This method will select the year
     * Created By : Venkat
     *
     * @param year This is year to be selected
     */
    public static void filterByYearly(String year) {
        WebDriverTasks.scrollToElement(yearlyFilterButton);
        WebDriverTasks.wait(5);
        WebDriverTasks.waitForElement(yearlyFilterButton).click();
        WebDriverTasks.wait(7);
        WebDriverTasks.waitForElementAndSelect(yearlyFilterDropdown).selectByVisibleText(year);
    }

    /**
     * This method will download Click on Export > All Records / Current View/ Selected Records and verify
     * that records count in exported excel matches with the count displayed in data grid
     * Created By : Venkat
     *
     * @throws IOException
     */
    public static void downloadAndVerifyRecordCount() throws IOException {
        List<String> downloadTypes = new LinkedList<>(Arrays.asList("All Records","Current View","Selected Records"));
        String downloadXpath;
        int expectedRecordCount;
        for(String downloadType : downloadTypes){
            WebDriverTasks.waitForAngular();
            WebDriverTasks.wait(7);
            WebDriverTasks.waitForAngular();

            if(downloadType.equals("All Records"))
                expectedRecordCount = getTotalRecordsCount();
            else if(downloadType.equals("Current View"))
                expectedRecordCount = getVisibleRecordCount();
            else
                expectedRecordCount = 1;

            if(downloadType.equals("Selected Records")) {
                WebDriverTasks.wait(7);
                WebDriverTasks.waitForElement(recordSelector).click();
                WebDriverTasks.waitForAngular();
            }

            int fileCountBefore = Helpers.countFilesInDownloadFolder();
            WebDriverTasks.waitForElement(downloadExcelButton).click();
            WebDriverTasks.wait(5);
            downloadXpath = String.format(downloadBy,downloadType);
            WebDriverTasks.waitForElement(By.xpath(downloadXpath)).click();
            //Click on Ok if confirmation popup is displayed
            if (WebDriverTasks.verifyIfElementIsDisplayed(exportCurrentViewConfirmationButton,5)) {
                WebDriverTasks.wait(3);
                WebDriverTasks.waitForElement(exportCurrentViewConfirmationButton).click();
                WebDriverTasks.wait(1);
            }
            //Verify that the notification message matches with expected value
            Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
            WebDriverTasks.waitForAngularPendingRequests();
            WebDriverTasks.wait(10);
            int fileCountAfter = Helpers.countFilesInDownloadFolder();
            Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder for '" + downloadType + "' export");

            int allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("ServiceRequestReport");
            Assert.assertTrue(expectedRecordCount == allActualRecordsCount, "Exported record count are not matching that of total record count");
        }
    }

    /**
     * This method will get number of records available for the report
     * Created By : Venkat
     *
     * @return int This is total records count
     */
    public static int getTotalRecordsCount() {
        String pagerString = WebDriverTasks.waitForElement(recordsPagerDetails).getText().trim();
        String[] arr = pagerString.split("of");
        String totalRecords = arr[1].trim().replace(" items", "");
        return Integer.parseInt(totalRecords);
    }

    /**
     * This method will get visible records count in data grid
     * Created By : Venkat
     *
     * @return Integer This is record count
     */
    public static int getVisibleRecordCount(){
        return WebDriverTasks.waitForElements(recordsInGrid).size();
    }
}
