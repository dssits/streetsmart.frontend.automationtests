package nycnet.dhs.streetsmart.pages;

import nycnet.dhs.streetsmart.pages.clientdetails.TravelAssistanceProgramTab;
import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.testng.Assert;

import java.util.HashMap;

public class EncampmentsList extends StartWebDriver {
    static By encampmentsListMenuItem = By.xpath("//a[contains(text(),'Encampments List')]");
    static By encampmentsDataGrid = By.cssSelector("div.ui-grid-render-container-body>div.ui-grid-viewport");
    static By addNewEncampment = By.cssSelector("a.addnew");

    static By userNameInputBox = By.xpath("//input[@ng-model='EncampmentDetail.Name']");
    static By providerDropdown = By.xpath("//select[@ng-model='EncampmentDetail.ProviderId']");
    static By areaDropdown = By.xpath("//select[@ng-model='EncampmentDetail.AreaId']");
    static By propertyTypeDropdown = By.xpath("//select[@ng-model='EncampmentDetail.PropertyTypeId']");
    static By propertyOwnerDropdpwn = By.xpath("//select[@ng-model='EncampmentDetail.PropertyOwnerId']");
    static By boroughDropdown = By.xpath("//select[@ng-model='EncampmentDetail.EncampmentLocationInfo.BoroughId']");
    static By beachNameDropdown = By.xpath("//div[contains(@ng-show,'PropertyTypeId') and not(@class='ng-hide')]//select[@ng-model='EncampmentDetail.PropertyNameValue']");
    static By syringesPresentDropdown = By.xpath("//select[@ng-model='EncampmentDetail.SyringesPresent']");
    static By structuresDropdown = By.xpath("//button[@ng-model='EncampmentDetail.StructureList']");
    static String multiSelectOption = "//label[normalize-space()='%s']/input[@type='checkbox']";
    static By saveEncampmentButton = By.xpath("//div[@id='save-cancel-btngroup']/button[text()='Save']");
    static By saveCleanupButton = By.xpath("//button[@ng-click='SaveCleanUpData()']");
    static By cleanupsTab = By.xpath("//li/a[normalize-space()='Cleanups']");
    static By cleanupDateInputBox = By.xpath("//input[@ng-model='CleanUp.CleanUpDate']");
    static By cleanupTimeInputBox = By.xpath("//input[@ng-model='CleanUp.CleanUpTime']");
    static By jointOutreachDropdown = By.xpath("//select[@ng-model='CleanUp.JointOutreachId']");
    static By clientExistsDropdown = By.xpath("//select[@ng-model='CleanUp.ClientExists']");
    static By encampmentsListLink = By.linkText("Encampment List");

    public static String returnencampmentsListsPageHeader() {
        String encampmentsListPageHeaderText = WebDriverTasks.waitForElement(encampmentsListMenuItem).getText();
        System.out.println("The page header is:" + encampmentsListPageHeaderText);
        return encampmentsListPageHeaderText;
    }

    /**
     * This method will verify thay data grid is displayed on page
     *
     * @return Boolean This will be True if data grid is displayed else False
     * Created By : Venkat
     */
    public static boolean verifyDataGridIsDisplayed() {
        WebDriverTasks.waitForAngular();
        return WebDriverTasks.verifyIfElementIsDisplayed(encampmentsDataGrid, 20);
    }

    /**
     * This method will click on Add new button
     * Created By : Venkat
     */
    public static void clickOnAddNew() {
        WebDriverTasks.waitForElement(addNewEncampment).click();
    }

    /**
     * This method will set username in username field
     *
     * @param userName This is username value.
     *                 Created By : Venkat
     */
    public static void setUserName(String userName) {
        WebDriverTasks.waitForElement(userNameInputBox).sendKeys(userName);
    }

    /**
     * This method will select provider
     *
     * @param provider This is proider name
     *                 Created By : Venkat
     */
    public static void selectProvider(String provider) {
        WebDriverTasks.waitForElementAndSelect(providerDropdown).selectByVisibleText(provider);
    }

    /**
     * This method will select Area
     *
     * @param area This is area
     *             Created By : Venkat
     */
    public static void selectArea(String area) {
        WebDriverTasks.waitForElementAndSelect(areaDropdown).selectByVisibleText(area);
    }

    /**
     * This method will select Property Type
     *
     * @param propertyType This is property type
     *                     Created By : Venkat
     */
    public static void selectPropertyType(String propertyType) {
        WebDriverTasks.waitForElementAndSelect(propertyTypeDropdown).selectByVisibleText(propertyType);
    }

    /**
     * This method will select Property owner
     *
     * @param propertyOwner This is property owner name
     *                      Created By : Venkat
     */
    public static void selectPropertyOwner(String propertyOwner) {
        WebDriverTasks.waitForElementAndSelect(propertyOwnerDropdpwn).selectByVisibleText(propertyOwner);
    }

    /**
     * This method will select Borough
     *
     * @param borough This is borough to be selected
     *                Created By : Venkat
     */
    public static void selectBorough(String borough) {
        WebDriverTasks.waitForElementAndSelect(boroughDropdown).selectByVisibleText(borough);
    }

    /**
     * This method will select Beach
     *
     * @param beachName This is beach name.
     *                  Created By : Venkat
     */
    public static void selectBeach(String beachName) {
        WebDriverTasks.waitForElementAndSelect(beachNameDropdown).selectByVisibleText(beachName);
    }

    /**
     * This method will select Syringes Present checkbox
     *
     * @param syringesPresent This is value to be selected
     */
    public static void selectSyringesPresent(String syringesPresent) {
        WebDriverTasks.waitForElementAndSelect(syringesPresentDropdown).selectByVisibleText(syringesPresent);
    }

    /**
     * This method will select Structures.
     *
     * @param structure This is value to be selected.
     *                  Created By : Venkat
     */
    public static void selectStructures(String structure) {
        WebDriverTasks.waitForElement(structuresDropdown).click();
        By option = By.xpath(String.format(multiSelectOption, structure));
        WebDriverTasks.waitForElement(option).click();
    }

    /**
     * This method will click on Save Encampment button
     * Created By : Venkat
     */
    public static void clickOnSaveEncampment() {
        WebDriverTasks.waitForElement(saveEncampmentButton).click();
    }

    /**
     * This method will click on Save cleanup button
     * Created By : Venkat
     */
    public static void clickOnSaveCleanup() {
        WebDriverTasks.waitForElement(saveCleanupButton).click();
    }

    /**
     * This method will add new encampment
     *
     * @param data This is hashmap with keys as field label and values as values we want to set
     */
    public static void addNewEncampment(HashMap<String, String> data) {
        clickOnAddNew();
        setUserName(data.get("Name"));
        selectProvider(data.get("Provider"));
        selectArea(data.get("Area"));
        selectPropertyType(data.get("Property Type"));
        selectPropertyOwner(data.get("Property Owner"));
        selectBorough(data.get("Borough"));
        selectBeach(data.get("Beach Name"));
        selectSyringesPresent(data.get("Syringes Present"));
        selectStructures(data.get("Structures"));
        clickOnSaveEncampment();
        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Encampment saved successfully");
        Assert.assertTrue(isSaved, "Success message is not displayed");
    }

    /**
     * This method will verify encamapment details
     *
     * @param data This is hashmap with labels as keys and expected values as their values
     * @return Boolean If all the data matches then it will return True else False
     * Created By : Venkat
     */
    public static boolean verifyEncampmentData(HashMap<String, String> data) {
        String labelValue = "//form[@name='addClient' and not(contains(@class,'ng-hide'))]//label/span[normalize-space()='%s:']/../following-sibling::div";
        By labelledElement;
        for (String label : data.keySet()) {
            labelledElement = By.xpath(String.format(labelValue, label));
            String actual = WebDriverTasks.waitForElement(labelledElement).getText().trim();
            if (!actual.equals(data.get(label))) {
                System.out.println();
                return false;
            }
        }
        return true;
    }

    /**
     * This will click on Cleanup tab
     * Created By : Venkat
     */
    public static void clickOnCleanupsTab() {
        WebDriverTasks.waitForElement(cleanupsTab).click();
    }

    /**
     * This method will add new cleanup
     *
     * @param data This is hashmap with keys as field label and values as values we want to set
     */
    public static void addNewCleanup(HashMap<String, String> data) {
        clickOnCleanupsTab();
        clickOnAddNew();
        selectCleanupDate(data.get("Cleanup Date"));
        setCleanupTime(data.get("Cleanup Time"));
        selectJointOutreach(data.get("Joint Outreach"));
        selectClientExists(data.get("Client Exists"));
        clickOnSaveCleanup();
        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");
    }

    /**
     * This method will select cleanup date
     *
     * @param cleanupDate This is cleanup date value
     *                    Created By : Venkat
     */
    public static void selectCleanupDate(String cleanupDate) {
        Base.selectDate(cleanupDateInputBox, cleanupDate);
    }

    /**
     * This method will set cleanup time.
     *
     * @param cleanupTime This is cleanup time to be selected
     *                    Created By : Venkat
     */
    public static void setCleanupTime(String cleanupTime) {
        WebDriverTasks.waitForElement(cleanupTimeInputBox).sendKeys(cleanupTime);
    }

    /**
     * This method will Joint outreach dropdown alue
     *
     * @param jointOutreach This is jointoutreach value.
     */
    public static void selectJointOutreach(String jointOutreach) {
        WebDriverTasks.waitForElementAndSelect(jointOutreachDropdown).selectByVisibleText(jointOutreach);
    }

    /**
     * This method will select If the client exists.
     *
     * @param clientExists This is client exists value.
     *                     Created By : Venkat
     */
    public static void selectClientExists(String clientExists) {
        WebDriverTasks.waitForElementAndSelect(clientExistsDropdown).selectByVisibleText(clientExists);
    }

    /**
     * This method will click on Encampment list
     * Created By : Venkat
     */
    public static void clickOnEncampmentsListLink() {
        WebDriverTasks.waitForElement(encampmentsListLink).click();
    }
}
