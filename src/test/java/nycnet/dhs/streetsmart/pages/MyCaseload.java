package nycnet.dhs.streetsmart.pages;

import nycnet.dhs.streetsmart.pages.dashboard.DailyOutreach;
import nycnet.dhs.streetsmart.pages.reports.ViewReports;
import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import sun.awt.image.ImageWatched;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class MyCaseload extends StartWebDriver {

    static By myCaseLoadPageHeader = By.xpath("//h1[@class='page-header '][contains(text(), 'My Caseload')]");
    static By nextDatGridPage = By.cssSelector("button.ui-grid-pager-next");
    static By firstDatGridPage = By.cssSelector("button.ui-grid-pager-first");
    static By dataGrid = By.cssSelector("div.ui-grid-contents-wrapper>div[role='grid']");
    static By dataRows = By.cssSelector("div.ui-grid-contents-wrapper>div[role='grid']>div[role='rowgroup']>div.ui-grid-canvas>div.ui-grid-row");
    static By itemsPerPage = By.xpath("//select[@ng-model='grid.options.paginationPageSize']");
    static By exportToExcel = By.xpath("//a[@data-toggle='dropdown' and normalize-space()='Excel']");
    static By exportAll = By.xpath("//a[normalize-space()='All Records']");
    static By exportCurrentView = By.xpath("//a[normalize-space()='Current View']");
    static By exportCurrentView_Confirm = By.xpath("//button[@data-dismiss='modal' and @id and @ng-click='Ok()']");

    public static String returnMyCaseLoadPageHeader() {
        String myCaseLoadPageHeaderText = WebDriverTasks.waitForElement(myCaseLoadPageHeader).getText();
        System.out.println("The page header is " + myCaseLoadPageHeaderText);
        return myCaseLoadPageHeaderText;
    }

    /**
     * This method will iterate through each data grid page and verify that Client category column contains only 'Engaged Unsheltered Prospects' or 'Other Prospects'
     * Created By : Venkat
     *
     * @return boolean If values matched expected values then True else False
     */
    public static boolean verifyDataInJCCClientColumn(String expectedData) {
        boolean isValid = true;
        boolean hasNext;
        List<String> expectedValues = new LinkedList<>(Arrays.asList(expectedData.split(",")));
        //Since this data grid contains two more action columns we need to reduce column count by 1
        int colNoForClientCategory = ViewReports.getColumnNo("JCC Client") - 1;
        String clientCategoryCells = String.format("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(%d)", colNoForClientCategory);
        By dataCells = By.cssSelector(clientCategoryCells);
        do {
            WebDriverTasks.waitForElement(dataCells);
            for (WebElement cell : WebDriverTasks.driver.findElements(dataCells)) {
                if (!expectedValues.contains(cell.getText().trim())) {
                    isValid = false;
                    break;
                }
            }
            hasNext = WebDriverTasks.driver.findElement(nextDatGridPage).isEnabled();
            if (hasNext & isValid)
                WebDriverTasks.waitForElement(nextDatGridPage).click();
        } while (hasNext & isValid);
        if (WebDriverTasks.driver.findElement(firstDatGridPage).isEnabled())
            WebDriverTasks.waitForElement(firstDatGridPage).click();
        return isValid;
    }

    /**
     * This method will verify if My Case load data grid is displayed or not.
     * Created By : Venkat
     *
     * @return boolean This will be True if data grid is displayed else False
     */
    public static boolean verifyCaseLoadDataGrid() {
        return WebDriverTasks.verifyIfElementIsDisplayed(dataGrid, 20);
    }

    /**
     * This method will change values for items per page and will verify that data grid shows less or same records.
     * The dropdown values will be selected 5, 10,15,20 or 25 one by one.
     * Created By : Venkat
     *
     * @return boolean If no.of rows are updated in table as expected then it will return True else False
     */
    public static boolean verifyRecordsPerPage() {
        LinkedList<Integer> items = new LinkedList<>(Arrays.asList(5, 10, 15, 20, 25));
        for (int item : items) {
            selectItemsPerPage(String.valueOf(item));
            if (!verifyRecordsDisplayed(item))
                return false;
        }
        return true;
    }

    /**
     * This method will compare expected record count against total rows displayed in data grid
     * Created By : Venkat
     *
     * @param expectedCount This is expected row count
     * @return boolean If both the expected and actual record count matches then it will return True else False
     */
    public static boolean verifyRecordsDisplayed(int expectedCount) {
        List<WebElement> rows = WebDriverTasks.waitForElements(dataRows);
        int totalRecords = DailyOutreach.getTotalRecordsCount();
        if (totalRecords <= expectedCount)
            return rows.size() <= expectedCount;
        return expectedCount == rows.size();
    }

    /**
     * This method will select provided value in Items Per Page dropdown
     * Created By : Venkat
     *
     * @param itemsPP This is Items per page value
     */
    public static void selectItemsPerPage(String itemsPP) {
        WebDriverTasks.waitForElementAndSelect(itemsPerPage).selectByVisibleText(itemsPP);
    }

    /**
     * This method will verify expected data against provided column name
     * Created By : Venkat
     *
     * @param columnName   This is target column name
     * @param expectedData This is expected data that should be displayed in each cell for given column
     * @return boolean If all cell values contains expected data then it will return True else false
     */
    public static boolean verifyDataInColumn(String columnName, String expectedData) {
        boolean isValid = true;
        boolean hasNext;
        int loopLimiter = 3;
        WebDriverTasks.waitForAngular();
        int colNoForClientCategory = ViewReports.getColumnNo(columnName);
        String clientCategoryCells = String.format("div.ui-grid-render-container-body>div.ui-grid-viewport>div>div>div>div:nth-child(%d)", (colNoForClientCategory - 1));
        By dataCells = By.cssSelector(clientCategoryCells);
        do {
            if(WebDriverTasks.verifyIfElementIsDisplayed(dataCells,5)) {
                WebDriverTasks.waitForElement(dataCells);
                for (WebElement cell : WebDriverTasks.driver.findElements(dataCells)) {
                    if (!cell.getText().trim().contains(expectedData)) {
                        isValid = false;
                        break;
                    }
                }
            }
            hasNext = WebDriverTasks.driver.findElement(nextDatGridPage).isEnabled();
            if (hasNext & isValid)
                WebDriverTasks.waitForElement(nextDatGridPage).click();
            loopLimiter--;
        } while (hasNext & isValid & loopLimiter>0);
        if (WebDriverTasks.driver.findElement(firstDatGridPage).isEnabled())
            WebDriverTasks.waitForElement(firstDatGridPage).click();
        return isValid;
    }

    /**
     * This method will click on Export > All Records button
     * Created By : Venkat
     */
    public static void exportAllRecordsToExcel() {
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(exportToExcel).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(exportAll).click();
    }

    /**
     * This method will click on Export > Current View and then on Ok button for confirmation popup
     * Created By : Venkat
     */
    public static void exportCurrentViewToExcel() {
        WebDriverTasks.waitForElement(exportToExcel).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(exportCurrentView).click();
        WebDriverTasks.wait(3);
        WebDriverTasks.waitForElement(exportCurrentView_Confirm).click();
    }


}
