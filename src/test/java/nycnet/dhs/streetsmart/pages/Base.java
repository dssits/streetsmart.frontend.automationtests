package nycnet.dhs.streetsmart.pages;

import org.apache.commons.exec.util.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import org.openqa.selenium.support.ui.Select;

public class Base extends StartWebDriver {
	public static By htmlTag = By.tagName("html");
	//Added By Venkat - To be used in selectCalenderDate
	public static String[] monthNames = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

	//Sidebar menu expand status
	static By sideBarSubMenuExpanded = By.xpath("//*[@class='sidebar page-sidebar-fixed']//*[@class='has-sub expand']");
	
	// About Streetsmart locator element
	static By aboutStreetSmartMenuItem = By
			.xpath("//*[@class='sidebar page-sidebar-fixed']//a/*[contains(text(), 'About StreetSmart')]");

	// Population Trends menu locator element
	static By populationTrendsMenu = By
			.xpath("//*[@class='sidebar page-sidebar-fixed']//a/*[contains(text(), 'Population Trends')]");

	// Clients menu locator elements
	//static By clientsMenu = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a//*[contains(text(), 'Clients')]");
	static By clientsMenu = By.xpath("//span[contains(text(),'Clients')]");
	static By clientListMenuItem = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), 'Client List')]");
	static By myCaseLoadListMenuItem = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), 'My Caseload')]");
	static By addClientListMenuItem = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), 'Add Client')]");
	
	// Engagements menu locator elements
	static By engagementsMenu = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a/*[contains(text(), 'Engagements')]");
	static By allEngagementsMenuItem = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), 'All Engagements')]");
	static By myEngagementsMenuItem = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), 'My Engagements')]");
	static By AddEngagementsMenuItem = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), 'Add Engagement')]");
	static By observationsUnsuccessfulAttemptsMenuItem = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), "
			+ "'Observations & Unsuccessful Attempts')]");

	// Authentication locator elements
	static By logOutNavbar = By.xpath("//*[@class='caret m-l-5']");
	static By logOutNavbarUserNameAndRole = By.xpath("//*[@class='caret m-l-5']/..");
	static By userToggle = By.cssSelector("ul[ng-show^='UserName']>li:nth-child(2)>a");
	static By whatsNewNotificationClose = By.linkText("Close Notification");
	static By logOutButton = By.linkText("Log Out");
	static By floatingError = By.cssSelector("div[class='toast-message']");

	// Dashboard locator elements
	static By dashboardLanding = By.xpath("//span[contains(text(),'Dashboard')]");
	static By dailyoutreachMenuItem = By.xpath("//a[contains(text(),'Daily Outreach')]");
	static By dailydropinMenuItem = By.xpath("//a[contains(text(),'Daily Drop-In')]");
	static By monthlydashboardMenuItem = By.xpath("//a[contains(text(),'Monthly Dashboard')]");

	// Canvassing locator elements
	static By canvassingMenu = By.xpath("//span[contains(text(),'Canvassing')]");
	static By generalcanvassingMenuItem = By.xpath("//a[contains(text(),'General Canvassing')]");
	static By panhandlingMenuItem = By.xpath("//a[contains(text(),'Panhandling')]");
	static By jointoperationsMenuItem = By.xpath("//a[contains(text(),'Joint Operations')]");
	static By intensivecanvassingMenuItem = By.xpath("//a[contains(text(),'Intensive Canvassing')]");

	//Capacity Management locators
	static By capacityManagementMenuItem = By.xpath("//a[normalize-space()='Capacity Management']");
	static By outreachCapacityManagementMenuItem = By.xpath("//a[normalize-space()='Outreach Capacity Management']");


	// Reports locator elements

	//Updated 'reportsMenu' By - Venkat
	static By reportsMenu = By.xpath("//span[text()='Reports']");

	static By outreachcannedreportsMenuItem = By.xpath("//span[contains(text(),'Outreach Canned Reports')]");
	static By censusMenuItem = By.xpath("//div[@id='sidebar']//li[1]//ul[1]//li[1]//a[1]");
	static By placementMenuItem = By.xpath("//a[contains(text(),'Placement')]");
	static By stabilizationbedMenuItem = By.xpath("//a[contains(text(),'Stabilization Bed')]");

	static By customreportMenuItem = By.xpath("//span[contains(text(),'Custom Reports')]");
	static By createreportMenuItem = By.xpath("//a[contains(text(),'Create Report')]");
	static By viewreportsMenuItem = By.xpath("//a[contains(text(),'View Reports')]");
	static By requestareportMenuItem = By.xpath("//a[contains(text(),'Request a Report')]");
	
	// Global Search menu locator element
	static By globalSearchMenuItem = By
			.xpath("//*[@class='sidebar page-sidebar-fixed']//a/*[contains(text(), 'Global Search')]");

	// Admin menu locator elements
	static By adminMenu = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a/*[contains(text(), 'Admin')]");
	static By userPermissionsMenuItem = By
			.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), 'User Permissions')]");
	static By requestsApprovalsMenuItem = By
			.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), 'Requests & Approvals')]");

	// Resources menu locator elements
	static By resourcesMenu = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a/*[contains(text(), 'Resources')]");
	static By webinarsMenuItem = By.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), 'Webinars')]");
	static By releaseHistoryMenuItem = By
			.xpath("//*[@class='sidebar page-sidebar-fixed']//a[contains(text(), 'Release History')]");

	// Outreach Requests locator elements
	static By outreachrequestsMenuItem = By.xpath("//span[contains(text(),'Outreach Requests')]");
	static By servicerequestsMenuItem = By.xpath("//a[contains(text(),'311 Service Requests')]");

	// Encampment locator elements
	static By encampmentsMenu = By.xpath("//span[contains(text(),'Encampments')]");
	static By encampmentsListMenuItem = By.xpath("//a[contains(text(),'Encampments List')]");
    
	//Access Denied Page
	static By accessDeniedHeader = By.xpath("//h4[contains(text(),'Access Denied')]");

	//Logged In By User name
	static By loggedInBy = By.xpath("//div[@class='navbar-header']/following-sibling::ul/li/a/div/span");

	public static void clickOnAboutStreetMenuItem() {

		WebDriverTasks.waitForElement(aboutStreetSmartMenuItem).click();
	}

	public static void clickOnPopulationTrendsMenu() {

		WebDriverTasks.waitForElement(populationTrendsMenu).click();
	}

	public static void waitForSideBarMenuToBexpanded() {
		WebDriverTasks.waitForElement(sideBarSubMenuExpanded);
	}
	
	public static void clickOnClientsMenu() {
		WebDriverTasks.waitForElement(clientsMenu).click();
	}

	public static void clickOnClientListMenuItem() {
		WebDriverTasks.waitForElement(clientListMenuItem).click();
		//WebElement element = driver.findElement(clientListMenuItem);
		//WebDriverTasks.waitUntilElementIsVisible(element, 15);
		//element.click();
	}

	public static void clickOnMyCaseLoadMenuItem() {
		WebDriverTasks.waitForElement(myCaseLoadListMenuItem).click();
	}

	public static void clickOnAddClientMenuItem() {
		WebDriverTasks.waitForElement(addClientListMenuItem).click();
	}

	public static void clickOnEngagementsMenu() {
		WebDriverTasks.waitForElement(engagementsMenu).click();
	}

	public static void clickOnAllEngagementsMenuItem() {
		WebDriverTasks.waitForElement(allEngagementsMenuItem).click();
	}

	public static void clickOnMyEngagementsMenuItem() {
		WebDriverTasks.waitForElement(myEngagementsMenuItem).click();
	}

	public static void clickOnAddEngagementsMenuItem() {
		WebDriverTasks.waitForElement(AddEngagementsMenuItem).click();
	}

	public static void clickObservationsUnsuccessfulAttemptsMenuItem() {
		WebDriverTasks.waitForElement(observationsUnsuccessfulAttemptsMenuItem).click();
	}

	public static void clickOnLogoutNavBar() {
		WebDriverTasks.waitForElement(logOutNavbar).click();
	}
	
	public static String returnLogOutNavBarName() {
		String nameAndRole = WebDriverTasks.waitForElement(logOutNavbarUserNameAndRole).getText();
        System.out.println("The name and role of the user that is signed-in is " + nameAndRole);
        String[] split = nameAndRole.split("\\s");
        String nameOnly = split[2] + " " + split[3];
        System.out.println("The name of the user is " + nameOnly);
        return nameOnly;
	}

	public static void clickOnUserToogle() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		WebDriverTasks.waitForElement(userToggle).click();
	}


	/**
	 *  Below method will check for floating error message and wait until it's invisible
	 *  Created By : Venkat
	 */
	public static void waitUntillErrorIsInvisible(){
		if(WebDriverTasks.verifyIfElementExists(floatingError))
			WebDriverTasks.waitUntilElementIsInVisible(WebDriverTasks.waitForElement(floatingError),10);
	}

	/**
	 *  Below method will check the user toggle, if the user has already logged int hen it will return true else false
	 *  Created By : Venkat
	 *
	 * @return boolean It will be either true or false
	 */
	public static boolean verifyUserToggleIsDisplayed(){
		return WebDriverTasks.verifyIfElementExists(userToggle);
	}

	public static void clickOnWhatsNewNotificaitonCloseButton() {
		WebDriverTasks.waitForAngular();
		if (WebDriverTasks.verifyIfElementExists(whatsNewNotificationClose)) {
			
			WebDriverTasks.waitForElement(whatsNewNotificationClose).click();
		}
	}
	
	public static void clickOnLogoutButton() {
		WebDriverTasks.waitForElement(logOutButton).click();
	}

	public static void clickOndashboardLanding() {
		WebDriverTasks.waitForElement(dashboardLanding).click();
	}

	public static void clickOnDashBoard() {
		WebDriverTasks.waitForElement(dashboardLanding).click();
	}

	public static void clickOnDailyOutreach() {
		WebDriverTasks.waitForElement(dailyoutreachMenuItem).click();
	}

	public static void clickOnDailyDropInMenuItem() {
		WebDriverTasks.waitForElement(dailydropinMenuItem).click();
	}

	public static void clickonMonthlyDashboardMenuItem() {
		WebDriverTasks.waitForElement(monthlydashboardMenuItem).click();
	}

	public static void ClickOnGeneralCanvassing() {
		WebDriverTasks.waitForElement(generalcanvassingMenuItem).click();
	}

	public static void clickOnCanvassing() {
		WebDriverTasks.waitForElement(canvassingMenu).click();
	}

	public static void clickOnPanhandling() {
		WebDriverTasks.waitForElement(panhandlingMenuItem).click();
	}

	public static void clickOnJointOperations() {
		WebDriverTasks.waitForElement(jointoperationsMenuItem).click();
	}

	public static void clickOnIntensiveCanvassing() {
		WebDriverTasks.waitForElement(intensivecanvassingMenuItem).click();
	}

	public static void clickOnReports() {
		WebDriverTasks.waitForElement(reportsMenu).click();
	}

	public static void clickOnOutreachCannedReports() {
		WebDriverTasks.waitForElement(outreachcannedreportsMenuItem).click();
	}

	public static void clickOnCensus() {
		WebDriverTasks.waitForElement(censusMenuItem).click();
	}

	public static void clickOnPlacement() {
		WebDriverTasks.waitForElement(placementMenuItem).click();
	}

	public static void clickOnStabilizationBed() {
		WebDriverTasks.waitForElement(stabilizationbedMenuItem).click();
	}

	public static void clickOnCustomReport() {
		WebDriverTasks.waitForElement(customreportMenuItem).click();
	}

	public static void clickOnCreateReport() {
		WebDriverTasks.waitForElement(createreportMenuItem).click();
	}

	public static void clickOnViewReports() {
		WebDriverTasks.waitForElement(viewreportsMenuItem).click();
	}

	public static void clickOnRequestAReport() {
		WebDriverTasks.waitForElement(requestareportMenuItem).click();
	}

	public static void clickOnGlobalSearchMenuItem() {
		WebDriverTasks.waitForElement(globalSearchMenuItem).click();
	}

	public static void clickOnAdminMenu() {
		WebDriverTasks.waitForElement(adminMenu).click();
	}
	
	public static void scrollToAdminMenu() {
		WebDriverTasks.scrollToElement(adminMenu);
	}

	public static void clickOnUserPermissionsMenuItem() {
		WebDriverTasks.waitForElement(userPermissionsMenuItem).click();
	}

	public static void clickOnRequestsApprovalsMenuitem() {
		WebDriverTasks.waitForElement(requestsApprovalsMenuItem).click();
	}

	public static void clickOnResourcesMenu() {
		WebDriverTasks.waitForElement(resourcesMenu).click();
	}

	public static void clickOnWebinarsMenuitem() {
		WebDriverTasks.waitForElement(webinarsMenuItem).click();
	}

	public static void clickOnReleaseHistoryMenuitem() {
		WebDriverTasks.waitForElement(releaseHistoryMenuItem).click();
	}

	public static void clickOnOutreachRequests() {
		WebDriverTasks.waitForElement(outreachrequestsMenuItem).click();
	}

	public static void clickOnServiceRequests() {
		WebDriverTasks.waitForElement(servicerequestsMenuItem).click();
	}

	public static void clickOnEncampments() {
		WebDriverTasks.waitForElement(encampmentsMenu).click();
	}

	public static void clickOnEncampmentsList() {
		WebDriverTasks.waitForElement(encampmentsListMenuItem).click();
	}
	
	public static String returnaccessDeniedHeader() {
		  String accessDeniedPageHeaderText =  WebDriverTasks.waitForElement(accessDeniedHeader).getText();
		  System.out.println("The page header is " + accessDeniedPageHeaderText);
		  return accessDeniedPageHeaderText;
	  }


	/**
	 * This method will set provided value for JCC Client Start Date.
	 * Created By : Venkat
	 *
	 * @param date This is date field locator
	 * @param startDate This is start date in mm/dd/yyyy format
	 */
	public static void selectDate(By date,String startDate) {
		WebDriverTasks.waitForElement(date).click();
		int day = Integer.parseInt(startDate.split("/")[1]);
		int month = Integer.parseInt(startDate.split("/")[0]);
		int year = Integer.parseInt(startDate.split("/")[2]);
		selectCalenderDate(day, month, year);
	}

	/**
	 * This method will set provided value for JCC Client Start Date.
	 * Created By : Venkat
	 *
	 * @param dateField This is date input box webelement
	 * @param startDate This is start date in mm/dd/yyyy format
	 */
	public static void selectDate(WebElement dateField,String startDate) {
		dateField.click();
		int day = Integer.parseInt(startDate.split("/")[1]);
		int month = Integer.parseInt(startDate.split("/")[0]);
		int year = Integer.parseInt(startDate.split("/")[2]);
		selectCalenderDate(day, month, year);
	}



	/**
	 * This method will select day,month and year on calendar
	 * Created By : Venkat
	 *
	 * @param day   This is day
	 * @param month This is month that can range from 1 to 12
	 * @param year  This is year
	 */
	public static void selectCalenderDate(int day, int month, int year) {
		WebDriverTasks.waitForAngular();
		By selectYear = By.xpath("//div[@id='ui-datepicker-div']//select[@class='ui-datepicker-year']");
		By selectMonth = By.xpath("//div[@id='ui-datepicker-div']//select[@class='ui-datepicker-month']");
		By selectday = By.xpath("//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[text()='" + day + "']");
		String monthName = monthNames[month - 1];
		WebDriverTasks.waitForElementAndSelect(selectYear).selectByVisibleText(String.valueOf(year));
		WebDriverTasks.waitForElementAndSelect(selectMonth).selectByVisibleText(monthName);
		WebDriverTasks.waitForElement(selectday).click();
	}

	/**
	 * This method will return logged in user name
	 * @return String This will contain  user name
	 */
	public static String returnLoggedInByUser(){
		String[] replacers = {"Welcome","(SA)","(DHS)","(RO)","(ED)","(ESB)"};
		String name = WebDriverTasks.waitForElement(loggedInBy).getText();
		for(String replace : replacers){
			name = name.replace(replace,"");
		}
		return name.trim();
	}

    public static void clickOnCapacityManagement() {
		WebDriverTasks.waitForElement(capacityManagementMenuItem).click();
    }

	public static void clickOnOutreachCapacityManagement() {
		WebDriverTasks.waitForElement(outreachCapacityManagementMenuItem).click();
	}
}
