package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;



public class ClientAddNewClient extends StartWebDriver {

	 static By addNewClientPageHeader = By.xpath("//h1[@id='pageHdr']");
	 
	 static By clientFirstNameInput = By.cssSelector("input[ng-model='Client.FirstName");
	 
	 static By clientLastNameInput = By.cssSelector("input[ng-model='Client.LastName']");
	 
	 static By clientPreferredFirstNameInput = By.cssSelector("input[ng-model='Client.PreferredFirstName']");
	 
	 static By clientPreferredLastNameInput = By.cssSelector("input[ng-model='Client.PreferredLastName']");
	 
	 static By clientAliasInput = By.cssSelector("input[ng-model='Client.Alias']");
	 
	 static By selectClientProvider = By.cssSelector("select[ng-model='Client.ClientProvider.Source']");
	 
	 static By selectClientGender = By.cssSelector("select[ng-model='Client.Gender']");
	 
	 static By selectClientArea = By.cssSelector("select[ng-model='Client.AreaId']");
	 
	 static By selectClientEnrollmentStartDate = By.cssSelector("input[ng-model='Client.ClientEnrollment.EnrollmentStartDate']");
	 
	 static By clientCategoryMenuEngagedUnsheltered = By.xpath("(//*[@class='col-md-6']/select"
	 		+ "/option[contains(text(),'Engaged Unsheltered Prospect')])[1]");
	 
	 static By clientCategoryMenuProspect = By.xpath("//select[@class='form-control ng-pristine ng-valid "
	 		+ "ng-not-empty ng-touched']");
	 
	 static By consentToCaseLoad = By.xpath("//*[@ng-model='Client.ClientEnrollment.IsCaseloadConsented']");
	 
	 static By saveButton = By.cssSelector("button[id='cint-info-save']");
	 static By saveButton2 = By.xpath("(//button[@id='cint-info-save'])[2]");
	 
	 //Add new case
	 static By selectServiceType = By.id("ddlServiceType");
	
	 static By caseProvider = By.xpath("//*[@ng-model='Case.Provider']");
	 
	 static By caseStartDate = By.xpath("//*[@ng-model='Case.CaseStartDate']");
	 
	 static By caseBoroughDropDown = By.xpath("//*[@ng-model='Case.Borough']");
	 
	 public static String returnClientListPageHeader() {
		  String addNewClientPageHeaderText =  WebDriverTasks.waitForElement(addNewClientPageHeader).getText();
		  System.out.println("The page header is " + addNewClientPageHeaderText);
		  return addNewClientPageHeaderText;
	  }
	 
	 public static String verifyEngagedUnshelteredProspectisSelected() {
		// String isSelected = WebDriverTasks.waitForElement(clientCategoryMenuEngagedUnsheltered).getAttribute("selected");
		 String isSelected = driver.findElement(clientCategoryMenuEngagedUnsheltered).getAttribute("selected");
		 System.out.println("Engaged Unsheltered Prospect is " + isSelected + " in the Client Category Menu" );
		 return isSelected;
	  }
	 
	 public static String verifyProspectisSelected() {
			// String isSelected = WebDriverTasks.waitForElement(clientCategoryMenuEngagedUnsheltered).getAttribute("selected");
			 String isSelected = driver.findElement(clientCategoryMenuProspect).getAttribute("selected");
			 System.out.println("Client Category is: " + isSelected);
			 return isSelected;
		  }
	 
	 public static void inputFirstName(String firstName) {
		 WebDriverTasks.waitForElement(clientFirstNameInput).sendKeys(firstName);
	 }
	 
	 public static void inputLastName(String lastName) {
		 WebDriverTasks.waitForElement(clientLastNameInput).sendKeys(lastName);
	 }
	 
	 public static void preferredFirstNameInput(String preferredFirstName) {
		 WebDriverTasks.waitForElement(clientPreferredFirstNameInput).sendKeys(preferredFirstName);
	 }
	 
	 public static void preferredLastNameInput(String preferredLastName) {
		 WebDriverTasks.waitForElement(clientPreferredLastNameInput).sendKeys(preferredLastName);
	 }
	 
	 public static void aliasInput(String alias) {
		 WebDriverTasks.waitForElement(clientAliasInput).sendKeys(alias);
	 }
	 
	 public static void selectProvider(String provider) {
		 WebDriverTasks.waitForElementAndSelect(selectClientProvider).selectByVisibleText(provider);
	 }
	 
	 public static void selectGender(String gender) {
		 WebDriverTasks.waitForElementAndSelect(selectClientGender).selectByVisibleText(gender);
	 }
	 
	 public static void selectClientArea(String area) {
		 WebDriverTasks.waitForElementAndSelect(selectClientArea).selectByVisibleText(area);
	 }
	 
	 public static void clickOnClientEnrollmentStartDate() {
		 WebDriverTasks.waitForElement(selectClientEnrollmentStartDate).click();
	 }
	 
	 public static void clickOnSaveButton() {
		 WebDriverTasks.waitForElement(saveButton).click();
	 }
	 
	 public static void clickOnSaveButton2() {
		 WebDriverTasks.waitForElement(saveButton2).click();
	 }
	 
	 public static void selectServiceType(String serviceType) {
		 WebDriverTasks.waitForElementAndSelect(selectServiceType).selectByVisibleText(serviceType);
	 }
	 
	 public static void scrollToServiceType() {
		 WebDriverTasks.scrollToElement(selectServiceType);
	 }
	 
	 public static void scrollToSaveButton() {
		 WebDriverTasks.scrollToElement(saveButton);
	 }
	 
	 public static void selectCaseProvider(String provider) {
		 WebDriverTasks.waitForElementAndSelect(caseProvider).selectByVisibleText(provider);
	 }
	 
	 public static void selectCaseStartDate() {
		 WebDriverTasks.waitForElement(caseStartDate).click();
	 }
	 
	 public static void selectCaseBorough(String caseborough) {
			WebDriverTasks.waitForElementAndSelect(caseBoroughDropDown).selectByVisibleText(caseborough);
		}
	 
	 public static void selectConsentToCaseload(String consent ) {
			WebDriverTasks.waitForElementAndSelect(consentToCaseLoad).selectByVisibleText(consent);
		}
	 
}
