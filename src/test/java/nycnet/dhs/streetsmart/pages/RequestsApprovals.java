package nycnet.dhs.streetsmart.pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class RequestsApprovals {
	static By requestsApprovalsBreadcrumb = By.xpath("//ol[@class='breadcrumb pull-right']/li[contains(text(), 'Requests & Approvals')]");
	
	//tile locators
	 static By toggleExpandOfMiddlePanel = By.xpath("//*[@ng-click='togglewigdets()']");
	static String detailsButtonOnTile = "//span[text()='%s']/ancestor::div[contains(@class,'panel')]//*[normalize-space()='Details']/i";
	static String tileBreakDownValues = "//h4/a/span[text()='%s']/ancestor::div[contains(@class,'panel') and @data-sortable-id]//li[normalize-space(text())='%s']/span";
	static String tileBreakDownValuesNoSpace = "//h4/a/span[text()='%s']/ancestor::div[contains(@class,'panel') and @data-sortable-id]//li[text()='%s']/span";
	static String totalRecordOnTile = "//h4/a/span[text()='%s']/ancestor::div[contains(@class,'panel') and @data-sortable-id]//div[contains(@class,'stats-number')]";
	
	//middle panel count
	//static String expandedMiddlePanelTotalCount = "//div[normalize-space(text()='HOME-STAT Deleted Clients – Details')]/following-sibling::div/span[@class='col-md-4 col-lg-3 col-sm-12']/label";
	static String expandedMiddlePanelTotalCount = "//div[normalize-space(text()='%s')]/following-sibling::div/span[@class='col-md-4 col-lg-3 col-sm-12']/label";
	static By mergeRequestsMiddlePanelTotalCount = By.xpath("//span[@class='col-md-2 col-sm-12']/label");
	//For Provisionally Closed Cases, Engaged Unsheltered Prospect and InActive Clients middle panel total count
	static By otherMiddlePanelTotalCount = By.xpath("//span[@class='col-md-2 p-r-0 col-sm-12']/label");
	static By otherProspectClientsMiddlePanelTotalCount = By.xpath("//label[@class='m-t-0 ng-binding']");
    static String expandedMiddlePanelSubCount = "//label[text()='%s']/preceding-sibling::label";
    
    //column picker locators
    static By columnPicker = By.xpath("//div[@role='button' and @class='ui-grid-icon-container']");
    static By selectedClientCategoryColumn = By.xpath("//button[normalize-space(text())='Client Category']/i[@class='ui-grid-icon-ok']");
    static String selectcolumnOnColumnPicker = "//button[normalize-space(text())='%s']/i[@class='ui-grid-icon-cancel']";
    static String deselectcolumnOnColumnPicker = "//button[normalize-space(text())='%s']/i[@class='ui-grid-icon-ok']";
    static String columnOnGrid = "//div[@role='button']/span[text()='%s']";
    
  //grid locators
  	 static By recordsPagerDetails = By.xpath("//div[@class='ui-grid-pager-count']/span");
  	 static By firstColumnFirstRowIDOnGrid = By.xpath("//*[@role='grid']/*[@role='rowgroup'][@class='ui-grid-viewport ng-isolate-scope']/*[@class='ui-grid-canvas']/*[@class='ui-grid-row ng-scope']/*[@role='row']/*[@role='gridcell'][1]");
  	 static By secondColumnFirstRowOnGrid = By.xpath("//*[@role='grid']/*[@role='rowgroup'][@class='ui-grid-viewport ng-isolate-scope']/*[@class='ui-grid-canvas']/*[@class='ui-grid-row ng-scope']/*[@role='row']/*[@role='gridcell'][2]");
  			
  //search locators
  static By searchForInputBox = By.xpath("//input[@aria-controls='data-table' and @type='text']");
  
  //Advanced filtering locators
  static By clickOnAdvancedFilter = By.linkText("Advanced Filtering");
  static String selectedFilterOnAdvancedFilters = "//input[@placeholder='%s']";
  
  static By clickOnChooseFilter = By.xpath("//button[@id='dropDown_Add Filter']//b[contains(@class,'caret')]");
  
  static String columnToFilterOnAdvancedFilters = "//*[@ng-repeat='option in (filteredOptions = (multiselectoptions| filter:search))']/label[@title='%s']";
  
  static By clickOnApplyFilter = By.id("nonDHSVeteranDataSaveButton");
  static By clickOnResetFilter = By.xpath("//button[@id='nonDHSVeteranDataCancelButton']");
  static By closeButton = By.xpath("//a[@ng-click='Close()']");
  
  static By enteredValueOnAdvancedFilter = By.xpath("//*[@class='token-input-token']/p");
  
  static By lastDateFieldOnAdvancedFilterPopup = By.cssSelector("div.scroll-filter-option>div:last-child>*>*>*>*>input");

	 public static String returnRequestsApprovalsBreadcrumb() {
		  String requestsApprovalsBreadcrumbText = WebDriverTasks.waitForElement(requestsApprovalsBreadcrumb).getText();
		  System.out.println("The Requests Approvals Breadcrumb is " + requestsApprovalsBreadcrumbText);
		  return requestsApprovalsBreadcrumbText;
	  }
	 
	
	 //tile actions
	  public static int getTotalRecordCountOnTile(String sectionName) {
	        By value = By.xpath(String.format(totalRecordOnTile, sectionName));
	        String strValue = WebDriverTasks.waitForElement(value).getText();
	        System.out.println("The total count of " + sectionName + " is " + strValue);
	        return Integer.parseInt(strValue);
	    }
	  
	 public static int getBreakdownCountFor(String sectionName, String subCountName) {
	        By value = By.xpath(String.format(tileBreakDownValues, sectionName, subCountName));
	        String strValue = WebDriverTasks.waitForElement(value).getText();
	        System.out.println("The count of " + sectionName + " is " + strValue);
	        return Integer.parseInt(strValue);
	    }
	 
	 public static int getBreakdownCountForNoSpace(String sectionName, String subCountName) {
	        By value = By.xpath(String.format(tileBreakDownValuesNoSpace, sectionName, subCountName));
	        String strValue = WebDriverTasks.waitForElement(value).getText();
	        System.out.println("The count of " + sectionName + " is " + strValue);
	        return Integer.parseInt(strValue);
	    }
	 
	 public static void clickOnDetailsButtonFor(String tileName) {
	        By detailsButton = By.xpath(String.format(detailsButtonOnTile, tileName));
	        WebDriverTasks.scrollToElement(detailsButton);
	        WebDriverTasks.waitForElement(detailsButton).click();
	    }
	 
	 //middle panel actions
	    public static void clickOnIconToToggleExpandMiddlePanel() {
	        WebDriverTasks.waitForElement(toggleExpandOfMiddlePanel).click();
	    }
	    
	    public static int getTotalRecordCountExpandedMiddlePanel(String sectionName) {
	        By value = By.xpath(String.format(expandedMiddlePanelTotalCount, sectionName));
	        String strValue = WebDriverTasks.waitForElement(value).getText();
	        System.out.println("The total count of " + sectionName + " is " + strValue);
	        return Integer.parseInt(strValue);
	    }
	    
	    public static int returnMergeRequestsMiddlePanelTotalCount() {
			  String Text = WebDriverTasks.waitForElement(mergeRequestsMiddlePanelTotalCount).getText();
			  System.out.println("The total count of the middle panel is " + Text);
			  return Integer.parseInt(Text);
		  }
	    
	    //return Provisionally Closed Cases, Engaged Unsheltered Prospect or InActive Clients middle panel total count
	    public static int returnMiddlePanelTotalCount() {
			  String Text = WebDriverTasks.waitForElement(otherMiddlePanelTotalCount).getText();
			  System.out.println("The total count of the middle panel is " + Text);
			  return Integer.parseInt(Text);
		  }
	    
	    public static int returnotherProspectMiddlePanelTotalCount() {
			  String Text = WebDriverTasks.waitForElement(otherProspectClientsMiddlePanelTotalCount).getText();
			  System.out.println("The total count of the middle panel is " + Text);
			  return Integer.parseInt(Text);
		  }
	    
	    public static int getSubRecordCountExpandedMiddlePanel(String sectionName) {
	        By value = By.xpath(String.format(expandedMiddlePanelSubCount, sectionName));
	        String strValue = WebDriverTasks.waitForElement(value).getText();
	        System.out.println("The count of " + sectionName + " is " + strValue);
	        return Integer.parseInt(strValue);
	    }
	    
	    //grid actions
	    public static int getTotalRecordsCount() {
	        WebDriverTasks.waitForAngular();
	        if (!WebDriverTasks.verifyIfElementIsDisplayed(recordsPagerDetails, 15))
	            return 0;
	        String pagerString = WebDriverTasks.waitForElement(recordsPagerDetails).getText().trim();
	        String[] arr = pagerString.split("of");
	        String totalRecords = arr[1].trim().replace(" items", "");
	        System.out.println("The total number of records on the grid is " + totalRecords);
	        return Integer.parseInt(totalRecords);
	    }
	    
	    public static void verifyValueOFAllRecordsOnFirstColumn(String expectedRecord) {
	    	List<WebElement> recordsOnFirstColumn = WebDriverTasks.waitForElements(firstColumnFirstRowIDOnGrid);
	    	 for (WebElement record : recordsOnFirstColumn) {
	           String recordOnFirstColumn =  record.getText();
	           System.out.println("The name of the record on the first  column is " + recordOnFirstColumn);
	           Assert.assertEquals(recordOnFirstColumn, expectedRecord);
	         }
	    }
	    
	    public static void verifyValueOFAllRecordsOnSecondColumn(String expectedRecord) {
	    	List<WebElement> recordsOnSecondColumn = WebDriverTasks.waitForElements(secondColumnFirstRowOnGrid);
	    	 for (WebElement record : recordsOnSecondColumn) {
	           String recordOnSecondColumn =  record.getText();
	           System.out.println("The name of the record on the first  column is " + recordOnSecondColumn);
	           Assert.assertEquals(recordOnSecondColumn, expectedRecord);
	         }
	    }
	    
	    public static String returnFirstRecordOnFirstColumn() {
	        String Text = WebDriverTasks.waitForElement(firstColumnFirstRowIDOnGrid).getText();
	        System.out.println("The first record on the first column column is " + Text);
	        return Text;
	    }
	    
	    public static String returnFirstRecordOnSecondColumn() {
	        String Text = WebDriverTasks.waitForElement(secondColumnFirstRowOnGrid).getText();
	        System.out.println("The first record on second column is " + Text);
	        return Text;
	    }
	    
	    //column picker actions
	    public static void clickOnColumnPicker() {
	        WebDriverTasks.waitForElement(columnPicker).click();
	    }
	    
	    public static void selectColumnOnColumnPicker(String columnName) {
	        WebDriverTasks.waitForElement(By.xpath(String.format(selectcolumnOnColumnPicker , columnName))).click();
	    }
	    
	    public static void deselectColumnOnColumnPicker(String columnName) {
	        WebDriverTasks.waitForElement(By.xpath(String.format(deselectcolumnOnColumnPicker, columnName))).click();
	    }
	    
	    public static boolean isColumnPresentOnGrid(String columnName) {
	        boolean isColumnPresent =  WebDriverTasks.verifyIfElementIsDisplayed(By.xpath(String.format(columnOnGrid, columnName)));
	        return isColumnPresent;
	    }
	    
	    //search actions
	    /**
	     * This Method will search for provided value
	     * Created By : Venkat
	     *
	     * @param searchValue This is the value for which we want to search in data grid
	     */
	    public static void searchFor(String searchValue) {
	        WebDriverTasks.waitForElement(searchForInputBox).clear();
	        WebDriverTasks.waitForElement(searchForInputBox).sendKeys(searchValue);
	        WebDriverTasks.waitForElement(searchForInputBox).sendKeys(Keys.ENTER);
	        WebDriverTasks.waitForAngular();
	    }
	    
	    //advanced filtering actions
	    public static void clickOnAdvancedFilter() {
	        WebDriverTasks.scrollToElement(clickOnAdvancedFilter);
	        WebDriverTasks.waitForElement(clickOnAdvancedFilter).click();
	    }
	    
	    public static void clickOnChooseFiter() {
	        WebDriverTasks.waitForElement(clickOnChooseFilter).click();
	    }
	    
	    public static void chooseColumnFilterOnAdvancedFilter(String columnFilter) {
	        String selectedColumnFilter = String.format(columnToFilterOnAdvancedFilters, columnFilter);
	        WebDriverTasks.waitForElement(By.xpath(selectedColumnFilter)).click();
	    }
	    
	    public static void enterFilterValueOnSelectedFilterOnAdvancedFilter(String selectFilter, String filterValue ) {
	        String selectedFilter = String.format(selectedFilterOnAdvancedFilters, selectFilter);
	        System.out.println("The name of the column filter is " + selectedFilter );
	        System.out.println("The value of the column filter is " + filterValue);
	        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).click();
	        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).clear();
	        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(filterValue);
	        WebDriverTasks.specificWait(2000);
	        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(Keys.DOWN);
	        WebDriverTasks.specificWait(2000);
	        WebDriverTasks.waitForElement(By.xpath(selectedFilter)).sendKeys(Keys.ENTER);
	    }
	    
	    public static String returnEnteredValueOnAdvancedFilter() {
	        String valueOfAdvancedFilter = WebDriverTasks.waitForElement(enteredValueOnAdvancedFilter).getText();
	        System.out.println("The value entered on the selected filter is " + valueOfAdvancedFilter);     
	        return valueOfAdvancedFilter;
	    }
	    
	    public static void clickOnResetFilterButton() {
	        WebDriverTasks.waitForElement(clickOnResetFilter).click();
	    }
	    
	    public static boolean verifyPresenceOfFilterResetButton() {
	        return WebDriverTasks.verifyIfElementExists(clickOnResetFilter);
	    }
	    
	    public static void clickOnApplyFilter() {
	        WebDriverTasks.waitForElement(clickOnApplyFilter).click();
	    }
	    
	    public static void clickOnCloseButton() {
	        WebDriverTasks.waitForElement(closeButton).click();
	    }
	    
	    public static void enterTexToFilterInputJavascriptExecutor(String filterName, String filterText) {
	    	By advancedFilter = By.xpath(String.format(selectedFilterOnAdvancedFilters, filterName));
	        WebDriverTasks.enterTextToInputJavascriptExcecutor(advancedFilter, filterText);	   
	    }
	    
	   
	 
	
	 
	

}
