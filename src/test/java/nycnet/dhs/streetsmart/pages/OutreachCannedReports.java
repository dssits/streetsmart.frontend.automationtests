package nycnet.dhs.streetsmart.pages;

import org.openqa.selenium.By;

import nycnet.dhs.streetsmart.core.StartWebDriver;
import nycnet.dhs.streetsmart.core.WebDriverTasks;

public class OutreachCannedReports extends StartWebDriver{
	
	static By outreachcannedreportsMenuItem = By.xpath("//span[contains(text(),'Outreach Canned Reports')]");
	
	public static String returnoutreachcannedreports() {
		String outreachcannedreportsPageHeaderText = WebDriverTasks.waitForElement(outreachcannedreportsMenuItem).getText();
		System.out.println("The page header is: " +outreachcannedreportsPageHeaderText);
		return outreachcannedreportsPageHeaderText;
	}
}
