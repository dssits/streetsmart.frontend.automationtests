package nycnet.dhs.streetsmart.core;

public class Config {

//localchrome, localinternetexplorer, remotechrome
public static String browser = "localchrome";

public static String streetsmartURL = "https://streetsmartqa.dhs.nycnet";
//public static String streetsmartURL = "https://streetsmartstg.dhs.nycnet/";

public static String seleniumServerURL = "http://localhost:4444/wd/hub";
}
