package nycnet.dhs.streetsmart.core;

import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Helpers {
    public static int countFilesInDownloadFolder() {
        String userProfileDirectory = System.getenv("USERPROFILE");
        File directory = new File(userProfileDirectory + "\\Downloads");
        int fileCount = directory.list().length;
        System.out.println("The number of files in the Downloads directory is " + fileCount);
        System.out.println(directory);
        return fileCount;
    }

    public static String returnMostRecentFileinDownloadFolder() {
        String userProfileDirectory = System.getenv("USERPROFILE");
        File directory = new File(userProfileDirectory + "\\Downloads");
        File[] files = directory.listFiles(File::isFile);
        long lastModifiedTime = Long.MIN_VALUE;
        File chosenFile = null;

        if (files != null) {
            for (File file : files) {
                if (file.lastModified() > lastModifiedTime) {
                    chosenFile = file;
                    lastModifiedTime = file.lastModified();
                }
            }
        }
        String mostrecentFile = chosenFile.getName();
        System.out.println("The name of the latest file in Directory is " + mostrecentFile);
        return mostrecentFile;
    }

    public static String returnExcelCellValue(String fileName, String sheetName, int rowNumber, int cellNumber) throws IOException {

        String userProfileDirectory = System.getenv("USERPROFILE");
        String downloadDirectory = userProfileDirectory + "\\Downloads";

        File file = new File(downloadDirectory + "\\" + fileName);

        FileInputStream inputStream = new FileInputStream(file);


        XSSFWorkbook XLSXWorkbook = new XSSFWorkbook(inputStream);

        XSSFSheet XLSXSheet = XLSXWorkbook.getSheet(sheetName);

        XSSFRow row = XLSXSheet.getRow(rowNumber);

        XSSFCell cell = row.getCell(cellNumber);

        String cellValue = cell.toString();

        System.out.println("The value in the cell contains " + cellValue);

        return cellValue;

    }


    /**
     * This method will read provided excel file as per provided file name from default downloads folder and return list of cell values for given column number
     * Created By : Venkat
     *
     * @param fileName  This is file name
     * @param sheetName This is sheet name
     * @param columnNo  This is column number
     * @return LinkedList<String> This is list of cell values
     * @throws IOException
     */
    public static LinkedList<String> readColumnData(String fileName, String sheetName, int columnNo) throws IOException {
        LinkedList<String> colData = new LinkedList<>();
        String userProfileDirectory = System.getenv("USERPROFILE");
        String downloadDirectory = userProfileDirectory + "\\Downloads";
        File file = new File(downloadDirectory + "\\" + fileName);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook XLSXWorkbook = new XSSFWorkbook(inputStream);
        XSSFSheet XLSXSheet = XLSXWorkbook.getSheet(sheetName);
        int lastRow = XLSXSheet.getLastRowNum();
        for (int i = 1; i < lastRow; i++)
            colData.add(XLSXSheet.getRow(i).getCell(columnNo, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).toString());
        return colData;
    }

    /**
     * This method will read provided excel file as per provided file name from default downloads folder and return number of records in excel sheet(Including columns)
     * Created By : Venkat
     *
     * @param fileName  This is file name
     * @param sheetName This is sheet name
     * @return int This is integer
     * @throws IOException
     */
    public static int getRecordCountInExcel(String fileName, String sheetName) throws IOException {
        LinkedList<String> colData = new LinkedList<>();
        String userProfileDirectory = System.getenv("USERPROFILE");
        String downloadDirectory = userProfileDirectory + "\\Downloads";
        File file = new File(downloadDirectory + "\\" + fileName);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook XLSXWorkbook = new XSSFWorkbook(inputStream);
        XSSFSheet XLSXSheet = XLSXWorkbook.getSheet(sheetName);
        int lastRow = XLSXSheet.getLastRowNum();
        return lastRow + 1;
    }


    /**
     * This method will read provided excel file as per provided file name from default downloads folder and return maximum number of cells in provided row
     * Created By : Venkat
     *
     * @param fileName  This is file name
     * @param sheetName This is sheet name
     * @param rowNo     This is row number
     * @return int This is number of cells used for given row
     * @throws IOException
     */
    public static int getUsedCellCount(String fileName, String sheetName, int rowNo) throws IOException {
        LinkedList<String> colData = new LinkedList<>();
        String userProfileDirectory = System.getenv("USERPROFILE");
        String downloadDirectory = userProfileDirectory + "\\Downloads";
        File file = new File(downloadDirectory + "\\" + fileName);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook XLSXWorkbook = new XSSFWorkbook(inputStream);
        XSSFSheet XLSXSheet = XLSXWorkbook.getSheet(sheetName);
        int lastCellNo = XLSXSheet.getRow(rowNo).getLastCellNum();
        return lastCellNo;
    }

    /**
     * This method will read provided excel file as per provided file name from default downloads folder and return columns list
     * Created By : Venkat
     *
     * @param fileName  This is file name
     * @param sheetName This is sheet name
     * @param rowNo     This is row number
     * @return List This is List of column name
     * @throws IOException
     */
    public static List<String> getAllColumnNames(String fileName, String sheetName, int rowNo) throws IOException {
        LinkedList<String> colsList = new LinkedList<>();
        String userProfileDirectory = System.getenv("USERPROFILE");
        String downloadDirectory = userProfileDirectory + "\\Downloads";
        File file = new File(downloadDirectory + "\\" + fileName);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook XLSXWorkbook = new XSSFWorkbook(inputStream);
        XSSFSheet XLSXSheet = XLSXWorkbook.getSheet(sheetName);
        XSSFRow row = XLSXSheet.getRow(rowNo);
        int lastCellNo = row.getLastCellNum();
        for (int i = 0; i < lastCellNo; i++) {
            colsList.add(row.getCell(i).toString());
        }
        return colsList;
    }


    /**
     * This method will verify if provided string is a valid rowNo
     * Created By : Venkat
     *
     * @param dateString         This is date string
     * @param expectedDateFormat This is expected date format
     * @return If the dateString is a valid date and as per expected formatthen it will be true else false
     */
    public static boolean isValidDate(String dateString, String expectedDateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(expectedDateFormat);
        sdf.setLenient(false);
        try {
            sdf.parse(dateString.trim());
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public static int returnRandomNumber() {
        Random objGenerator = new Random();
        int randomNumber = 0;

        randomNumber = objGenerator.nextInt(10000);
        System.out.println("Random Number is : " + randomNumber);
        return randomNumber;
    }


    /**
     * This method will get difference ind ays between two dates
     * Created By : Venkat
     *
     * @param startDate This is start date
     * @param endDate   This is end date
     * @return int THis will be difference between days
     */
    public static int getDateDifferenceInDays(Date startDate, Date endDate) {
        return Math.abs((int) ChronoUnit.DAYS.between(startDate.toInstant(), endDate.toInstant()));
    }

    /**
     * This method will parse date from string
     *
     * @param date       This is date in string
     * @param dateFormat This is date format
     * @return Date This is pased date
     * @throws ParseException
     */
    public static Date parseDate(String date, String dateFormat) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.parse(date);
    }

    /**
     * This will parse LocalDate from string date
     * Created By : Venkat
     *
     * @param date       This is date in string
     * @param dateFormat THis is date format
     * @throws ParseException
     */
    public static LocalDate parseLocalDate(String date, String dateFormat) throws ParseException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
        LocalDate lcd = LocalDate.parse(date, dtf);
        return lcd;
    }

    /**
     * This will verify that Date1 is falling after Date2 or it's same
     * Created By : Venkat
     *
     * @param date1 Date
     * @param date2 Date
     * @return boolean If date1 is falling after or on same date2 then it will return True else false
     */
    public static boolean isAfterOrSame(Date date1, Date date2){
       int diff = (int) ChronoUnit.DAYS.between(date1.toInstant(), date2.toInstant());
        return diff>=0;
    }

    /**
     * This will verify that Date1 is falling before Date2 or it's same
     * Created By : Venkat
     *
     * @param date1 Date
     * @param date2 Date
     * @return boolean If date1 is falling before or on same date2 then it will return True else false
     */
    public static boolean isBeforeOrSame(Date date1, Date date2){
        int diff = (int) ChronoUnit.DAYS.between(date1.toInstant(), date2.toInstant());
        return diff<=0;
    }

    public static boolean createEmptyFile(String filePath) throws IOException {
        File file = new File(filePath);
        return file.createNewFile();
    }

    public static String getCurrentDate(String dateFormat){
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Date date = new Date();
        return sdf.format(date);
    }

    public static String addDays(String date,String dateFormat, int diff) throws ParseException {
        LocalDate newDate;
        if(diff<0)
            newDate = parseLocalDate(date,dateFormat).minusDays(Math.abs(diff));
        else
            newDate = parseLocalDate(date,dateFormat).plusDays(diff);
        return newDate.format(DateTimeFormatter.ofPattern(dateFormat));
    }
    
    public static String returnCurrentDayOfTheMonth() {
    	  ZoneId z = ZoneId.of("America/New_York");
          LocalDate ld = LocalDate.now(z);
          int dayOfMonth = ld.getDayOfMonth();
          String currentDate = ld.now().toString();
          String dayOfMonthString = Integer.toString(dayOfMonth);
          return dayOfMonthString;
    }
}
