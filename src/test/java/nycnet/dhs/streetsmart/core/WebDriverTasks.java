package nycnet.dhs.streetsmart.core;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.apache.commons.exec.util.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.paulhammant.ngwebdriver.NgWebDriver;

import nycnet.dhs.streetsmart.pages.Base;

public class WebDriverTasks extends StartWebDriver {

    public static void goToURL(String url) {
        driver.get(url);

    }

    public static String returnPageURL() {
        WebDriverTasks.waitForAngular();
        return driver.getCurrentUrl();

    }

    public static String returnEndingPageURL() {
        WebDriverTasks.waitForAngular();
        String currentURL = driver.getCurrentUrl();
        System.out.println("The current URL of the page is " + currentURL);
        String streetsmartEndingSplit[] = StringUtils.split(currentURL, ".");
        String streetsmartEndingCombined = streetsmartEndingSplit[1] + "." + streetsmartEndingSplit[2];
        return streetsmartEndingCombined;
    }

    public static void maximizeWindow() {

        driver.manage().window().maximize();

    }

    public static void setWindowSize(int width, int height) {
        System.out.println("The size of the browser window is " + driver.manage().window().getSize());
        Dimension d = new Dimension(width, height);
        driver.manage().window().setSize(d);
        System.out.println("The size of the browser window is " + driver.manage().window().getSize());
    }

    public static void switchToNewWindow() {
        waitForAngular();

        String MainWindow = driver.getWindowHandle();

        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();

        while (i1.hasNext()) {
            String ChildWindow = i1.next();


            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {

                driver.switchTo().window(ChildWindow);
            }
        }
    }

    public static void closeWindow() {

        driver.close();

    }

    public static void scrollToElement(By elementLocator) {
        waitForAngular();
        WebElement element = driver.findElement(elementLocator);
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();

    }


    public static void scrollToElement(WebElement element) {
        waitForAngular();
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
    }


    public static void scrollToElementJavascriptExecutor(By elementLocator) {
        waitForAngular();
        WebElement element = driver.findElement(elementLocator);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public static void pageDown(By elementLocator) {
        waitForAngular();
        WebElement element = driver.findElement(elementLocator);
        element.sendKeys(Keys.PAGE_DOWN);
    }

    public static void pageDownCommon() {
        waitForAngular();
        WebElement element = driver.findElement(Base.htmlTag);
        element.sendKeys(Keys.PAGE_DOWN);
    }

    public static void pageUpCommon() {
        waitForAngular();
        WebElement element = driver.findElement(Base.htmlTag);
        element.sendKeys(Keys.PAGE_UP);
    }

    public static void scrollDownJavascriptExcecutor() {
        waitForAngular();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("javascript:window.scrollBy(0,document.body.scrollHeight)");
    }

    public static void scrollSpecificallyJavascriptExecutor(int horizontal, int vertical) {
        waitForAngular();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String string = MessageFormat.format("javascript:window.scrollBy({0},{1})", horizontal, vertical);
        // js.executeScript("javascript:window.scrollBy(0,0)");
    }

    public static void clickOnElementJavascriptExcecutor(By elementLocator) {
        waitForAngular();
        WebElement element = driver.findElement(elementLocator);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
    }
    
    public static void enterTextToInputJavascriptExcecutor(By elementLocator, String textToInput) {
        waitForAngular();
        WebElement element = driver.findElement(elementLocator);
        String scriptToInputText = String.format("arguments[0].value='%s';", textToInput);
        ((JavascriptExecutor) driver).executeScript(scriptToInputText, element);
    }

    public static void quitWebDriver() {

        driver.quit();

    }

    public static void specificWait(int time) {

        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitForAngular() {

        NgWebDriver ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
        ngWebDriver.waitForAngularRequestsToFinish();
    }

    public static void waitForAngularPendingRequests() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String pendingRequestStatus = js.executeScript("return angular.element(document.body).injector().get(\'$http\').pendingRequests.length;").toString();
        while (pendingRequestStatus.equalsIgnoreCase("1")) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            pendingRequestStatus = js.executeScript("return angular.element(document.body).injector().get(\'$http\').pendingRequests.length;").toString();
            //System.out.println(pendingRequestStatus);
        }
    }


    public static WebElement waitForElement(By elementLocator) {
        waitForAngular();
        WebDriverWait wait = new WebDriverWait(driver, 90);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
        return element;
    }

    public static WebElement waitForElementButNotForAngular(By elementLocator) {
        WebDriverWait wait = new WebDriverWait(driver, 90);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
        return element;
    }

    public static Select waitForElementAndSelect(By elementLocator) {
        waitForAngular();
        WebDriverWait wait = new WebDriverWait(driver, 90);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
        Select select = new Select(driver.findElement(elementLocator));
        return select;
    }

    public static boolean verifyIfElementExists(By elementLocator) {
        waitForAngular();
        Boolean isPresent = driver.findElements(elementLocator).size() > 0;
        System.out.println("The element is present " + isPresent);
        return isPresent;
    }

    public static List<WebElement> waitForElements(By elementLocator) {
        waitForAngular();
        WebDriverWait wait = new WebDriverWait(driver, 90
        );

        wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
        List<WebElement> elements = driver.findElements(elementLocator);
        return elements;
    }

    public static void waitUntilElementIsVisible(WebElement element, int time) {
        new WebDriverWait(driver, time).until(ExpectedConditions.visibilityOf(element));
    }

    /**
     * This method will wait until the webelement is invisible
     *
     * @param element This is target webelement
     * @param time    This is timeout in seconds
     */
    public static void waitUntilElementIsInVisible(WebElement element, int time) {
        new WebDriverWait(driver, time).until(ExpectedConditions.invisibilityOf(element));
    }


    /*
     * This method will wait until; element is displayed on screen
     *
     * @param elementLocator This is element locator
     * @return Boolean If the element exists in dom and is visible then it will be True.
     * If element is there in dom but not vissble then it will return False
     * If element is not there in dom then it will return False.
     */
    public static boolean verifyIfElementIsDisplayed(By elementLocator) {
        waitForAngular();
        if (verifyIfElementExists(elementLocator))
            return driver.findElement(elementLocator).isDisplayed();
        else
            return false;
    }

    /**
     * This method will wait until; element is displayed on screen
     *
     * @param elementLocator This is element locator
     * @param timeout        This is timeout in seconds.
     * @return Boolean If the element exists in dom and is visible then it will be True.
     * If element is there in dom but not vissble then it will return False
     * If element is not there in dom then it will return False.
     */
    public static boolean verifyIfElementIsDisplayed(By elementLocator, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    return element.isDisplayed();
                }
            });
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * This Method will wait for given amount of seconds
     *
     * @param timeInSeconds This is time in seconds
     */
    public static void wait(int timeInSeconds) {
        try {
            Thread.sleep(timeInSeconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void scrollToTop() {
        try {
            JavascriptExecutor js = ((JavascriptExecutor) driver);
            js.executeScript("window.scrollTo(0,0)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static String returnValueOfInput (By elementLocator) {
    	String text = WebDriverTasks.waitForElement(elementLocator).getAttribute("value");
    	
    	System.out.println("The value of the input is :" + text);
    	return text;
    }
    
    public static void enterTextToInput (By elementLocator, String valueToInput) {
    	WebDriverTasks.waitForElement(elementLocator).sendKeys(valueToInput);
    }
}
