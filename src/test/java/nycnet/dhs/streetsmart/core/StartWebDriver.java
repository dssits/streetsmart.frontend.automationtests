package nycnet.dhs.streetsmart.core;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class StartWebDriver {
	
	public static WebDriver driver;
	static String projectPath = System.getProperty("user.dir");
    static String browser = Config.browser;
	
	public static void startWebDriver() throws MalformedURLException {
		
		if(browser.equalsIgnoreCase("localchrome")) {
			System.setProperty("webdriver.chrome.driver",projectPath+"\\src\\test\\resources\\browserdrivers\\chromedriver.exe");
			driver = new ChromeDriver();

		}
		
		//Running firefox locally does not work right now.
		if(browser.equalsIgnoreCase("localfirefox")) {
			System.setProperty("webdriver.gecko.driver",projectPath+"\\src\\test\\resources\\browserdrivers\\geckodriver.exe");
			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("network.negotiate-auth.delegation-uris", "https://streetsmartqa.dhs.nycnet");
			profile.setPreference("network.negotiate-auth.trusted-uris", "https://streetsmartqa.dhs.nycnet");
			profile.setPreference("network.negotiate-auth.delegation-uris", "https://streetsmartstg.dhs.nycnet");
			profile.setPreference("network.negotiate-auth.trusted-uris", "https://streetsmartstg.dhs.nycnet");
			profile.setPreference("network.negotiate-auth.allow-non-fqdn", true);
			
			profile.setPreference("network.automatic-ntlm-auth.trusted-uris", "https://streetsmartqa.dhs.nycnet");
			profile.setPreference("network.automatic-ntlm-auth.trusted-uris", "https://streetsmartstg.dhs.nycnet");
			profile.setPreference("network.automatic-ntlm-auth.allow-non-fqdn", true);
			
			FirefoxOptions options = new FirefoxOptions();
			options.setProfile(profile);
			
			driver = new FirefoxDriver(options);

		}
		
		if(browser.equalsIgnoreCase("localinternetexplorer")) {
			new DesiredCapabilities();
			DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
			capability.setCapability("ignoreProtectedModeSettings", true);
			System.setProperty("webdriver.ie.driver",projectPath+"\\src\\test\\resources\\browserdrivers\\IEDriverServer.exe");
			driver = new InternetExplorerDriver(capability);
			
		}
		
		//Browser loads and tests run but test fails
		if(browser.equalsIgnoreCase("localedge")) {
			System.setProperty("webdriver.edge.driver",projectPath+"\\src\\test\\resources\\browserdrivers\\MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
			
		}
		
		if(browser.equalsIgnoreCase("remotechrome")) {
			new DesiredCapabilities();
			DesiredCapabilities capability = DesiredCapabilities.chrome();
			driver = new RemoteWebDriver(new URL(Config.seleniumServerURL), capability);

		}
		//Remote Firefox does not work due to application authentication
		if(browser.equalsIgnoreCase("remotefirefox")) {
			//new DesiredCapabilities();
			//DesiredCapabilities capability = DesiredCapabilities.firefox();
			
			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("network.negotiate-auth.delegation-uris", "https://streetsmartqa.dhs.nycnet");
			profile.setPreference("network.negotiate-auth.trusted-uris", "https://streetsmartqa.dhs.nycnet");
			profile.setPreference("network.negotiate-auth.delegation-uris", "https://streetsmartstg.dhs.nycnet");
			profile.setPreference("network.negotiate-auth.trusted-uris", "https://streetsmartstg.dhs.nycnet");
			profile.setPreference("network.negotiate-auth.allow-non-fqdn", true);
			
			profile.setPreference("network.automatic-ntlm-auth.trusted-uris", "https://streetsmartqa.dhs.nycnet");
			profile.setPreference("network.automatic-ntlm-auth.trusted-uris", "https://streetsmartstg.dhs.nycnet");
			profile.setPreference("network.automatic-ntlm-auth.allow-non-fqdn", true);
			
			FirefoxOptions options = new FirefoxOptions();
			options.setProfile(profile);
			
			driver = new RemoteWebDriver(new URL(Config.seleniumServerURL), options);

		}
		
		//Running Internet Explorer remotely does not work due to protected mode settings
		if(browser.equalsIgnoreCase("remoteinternetexplorer")) {
			new DesiredCapabilities();
			DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
			capability.setCapability("ignoreProtectedModeSettings", true);
			driver = new RemoteWebDriver(new URL(Config.seleniumServerURL), capability);

		}
		
		//Browser loads and tests run but test fails
		if(browser.equalsIgnoreCase("remoteedge")) {
			new DesiredCapabilities();
			DesiredCapabilities capability = DesiredCapabilities.edge();
			driver = new RemoteWebDriver(new URL(Config.seleniumServerURL), capability);
		}
		
		
	}
}
