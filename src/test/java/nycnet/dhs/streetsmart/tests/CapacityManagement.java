package nycnet.dhs.streetsmart.tests;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.OutreachCapacityManagement;
import nycnet.dhs.streetsmart.pages.dashboard.Monthly;
import nycnet.dhs.streetsmart.pages.reports.CensusReport;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CapacityManagement extends AutoRunTestController{

    @Test
    public static void VerifyOutreachCapacityManagement(){
        //Navifate to Outreach Capacity management page
        Base.clickOnCapacityManagement();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnOutreachCapacityManagement();

        //Verify that data grid is displayed
        boolean isDisplayed = CensusReport.verifyDataGridIsDisplayed();
        Assert.assertTrue(isDisplayed,"Data grid is not displayed on Census report page");

        //Add new Outreach capacity and verify that popup is displayed
        OutreachCapacityManagement.clickAddNewButton();
        WebDriverTasks.wait(5);
        OutreachCapacityManagement.clickSaveButton();
        WebDriverTasks.wait(5);
        isDisplayed = OutreachCapacityManagement.isPopupDisplayed();
        Assert.assertTrue(isDisplayed,"Even after not providing values in popup, the popup is closed after clicking on Save button");

        //Provide outreach details and click on save button and verify that outreach is closed ion saving it
        OutreachCapacityManagement.setFacilityInformation("ALADDIN HOTEL","Bronx","3","493","Exterior Street","10451",Helpers.getCurrentDate("MM/dd/yyyy"),"BronxWorks");
        OutreachCapacityManagement.clickSaveButton();
        isDisplayed = OutreachCapacityManagement.isPopupDisplayed();
        Assert.assertTrue(!isDisplayed,"On providing values in popup, the popup is not closed after clicking on Save button");

        //Uncheck facility name column from column picker in data grid and verify that it's not displayed in data grid
        boolean isHidden = Monthly.hideColumnAndVerify("Facility Name");
        Assert.assertTrue(isHidden,"The column 'Gender' is not hidden");

        //Check(select) facility name column from column picker in data grid and verify that it's displayed in data grid
        boolean isVisible = Monthly.unhideColumnAndVerify("Facility Name");
        Assert.assertTrue(isVisible,"The column 'Gender' is not visible");
    }
}
