package nycnet.dhs.streetsmart.tests;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.testng.Assert;
import org.testng.annotations.Test;

import nycnet.dhs.streetsmart.core.Config;
import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.ClientAddNewClient;
import nycnet.dhs.streetsmart.pages.ClientInformation;
import nycnet.dhs.streetsmart.pages.ClientSearch;
import nycnet.dhs.streetsmart.pages.Common;
import nycnet.dhs.streetsmart.pages.PopulationTrends;


public class PopulationTrendsTests extends AutoRunTestController {

	//H20-458 - Create new Engaged Unsheltered Prospect client, change client category to Inactive and then verify activity in Activity stream
	@Test()
	 	public void changeClientCategoryandVerifyActivityInActivityStream() {
		  Base.clickOnClientsMenu();
			Base.waitForSideBarMenuToBexpanded();
			Base.clickOnAddClientMenuItem();

			Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
			Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
			int random = Helpers.returnRandomNumber();
			String firstName = "User" + random;
			System.out.println(firstName);
			ClientSearch.inputTextinFirstNameField(firstName);
			ClientSearch.inputTextinLastNameField("Automation");
			ClientSearch.clickOnDupliceSearchButton();
			ClientSearch.clickOnDuplicateSearchAddNewClient();
			
			Common.clickOnEngagedUnshelteredButton();

			Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
			Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");
		
			ClientAddNewClient.inputFirstName(firstName);
			ClientAddNewClient.inputLastName("Automation");
			ClientAddNewClient.preferredFirstNameInput(firstName);
			ClientAddNewClient.preferredLastNameInput("Automation");
			ClientAddNewClient.aliasInput(firstName);
			ClientAddNewClient.selectProvider("BRC");
			ClientAddNewClient.selectGender("Male");
			ClientAddNewClient.clickOnClientEnrollmentStartDate();
			Common.clickOnLinkText("1");
			WebDriverTasks.pageDownCommon();
			ClientAddNewClient.clickOnSaveButton();
			String streetSmartID = ClientInformation.returnStreetsmartID();
			String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
			Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");
			WebDriverTasks.pageDownCommon();
			ClientInformation.clickOnEditButton();
			WebDriverTasks.pageDownCommon();
			
			//ClientInformation.clickOnEditClientInformationButton2();
			ClientInformation.selectClientCategoryOnEditPage2("Inactive");
			ClientInformation.clickSaveClientInformationButton();
			ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
			Assert.assertEquals(ClientCategory, "Inactive");
			
			Base.clickOnPopulationTrendsMenu();
			Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/PopulationTrend");
			Assert.assertEquals(PopulationTrends.returnPopulationTrendsPageHeader(), "Population Trends");
			PopulationTrends.clickOnExpandActivityStreamButton();
			
			PopulationTrends.inputTexInSearchFieldOfActivityStream(streetSmartID);
			PopulationTrends.submitSearchInActivityStream();
			Assert.assertTrue(PopulationTrends.returnActionSummaryFirstRowOfActivityStreamGrid().endsWith("Client Category: Inactive"),
					"The Client Category is Inactive");
		
	 	}
	

	
	
	
	 
  
}




  


