package nycnet.dhs.streetsmart.tests;

import java.io.IOException;
import java.net.MalformedURLException;

import javafx.scene.control.Alert;
import org.testng.Assert;
import org.testng.annotations.Test;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.AddEngagement;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.ClientAddNewClient;
import nycnet.dhs.streetsmart.pages.ClientInformation;
import nycnet.dhs.streetsmart.pages.ClientList;
import nycnet.dhs.streetsmart.pages.ClientSearch;
import nycnet.dhs.streetsmart.pages.Common;
import nycnet.dhs.streetsmart.pages.GeneralCanvassing;
import nycnet.dhs.streetsmart.pages.engagements.MyEngagements;
import nycnet.dhs.streetsmart.pages.ObservationsUnsuccessfulAttempts;
import nycnet.dhs.streetsmart.pages.ViewClientInformationfromSearch;
import nycnet.dhs.streetsmart.pages.clientdetails.EngagementsTab;
import nycnet.dhs.streetsmart.pages.engagements.AllEngagements;
import nycnet.dhs.streetsmart.pages.engagements.ClientAndEngagementSearch;

public class Engagements extends AutoRunTestController {

	 //H20-889 - Verify the Other Prospect legends has correct labels on the All Engagement grid
    @Test()
    public void verifyTheOtherProspectLegendHasCorrectLabelsOntheAllEngagementGrid() {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnAllEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
        Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");
        Assert.assertEquals(AllEngagements.returnOtherProspectLegendLessThan3Label(), "Other Prospect Clients With less than 3 Engagements");
        Assert.assertEquals(AllEngagements.returnOtherProspectLegendMoreThan3Label(), "Other Prospect Clients With 3 or more Engagements");
    }

    //H20-850 - verify changelog updates to Subway Diversion Program Start Date of Engagements
    @Test()
    public void verifySubwayDiversionProgramStartDateUpdatesWhenUpdatingEngagements() {
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
        ClientList.inputTextinClientListSearchBox("C161254");
        ClientList.submitSearchinGlobalSearchBox();
        ClientList.clickOnStreetSmartID("C161254");
        EngagementsTab.clickOnEngagementsTab();
        EngagementsTab.clickOnActionButton();
        EngagementsTab.clickOnViewDetails();
        WebDriverTasks.pageDownCommon();
        WebDriverTasks.pageDownCommon();
        EngagementsTab.clickOnEditButton();
        WebDriverTasks.pageUpCommon();
        WebDriverTasks.pageUpCommon();
        EngagementsTab.clickOnDatePickerCalendar();
        Common.clickOnLinkText("4");
        WebDriverTasks.pageDownCommon();
        WebDriverTasks.pageDownCommon();
        EngagementsTab.clickOnSaveButtonOnCalendar();
        EngagementsTab.clickOnActionButton();
        EngagementsTab.clickOnViewDetails();
        String SubwayDiversionDate = EngagementsTab.returnSubwayDiversionProgramStartDate();
        ClientInformation.clickOnClientInformationTab();
        WebDriverTasks.pageDownCommon();
        ClientInformation.expandChangelog();
        WebDriverTasks.pageDownCommon();
        String changeLogSubwayDiversionProgramStart = ClientInformation.returnSubwayDiversionProgramStartDateChangelog();
        Assert.assertEquals(changeLogSubwayDiversionProgramStart, SubwayDiversionDate);
        WebDriverTasks.pageUpCommon();
        WebDriverTasks.pageUpCommon();
        EngagementsTab.clickOnEngagementsTab();
        EngagementsTab.clickOnActionButton();
        EngagementsTab.clickOnViewDetails();
        WebDriverTasks.pageDownCommon();
        WebDriverTasks.pageDownCommon();
        EngagementsTab.clickOnEditButton();
        WebDriverTasks.pageUpCommon();
        WebDriverTasks.pageUpCommon();
        EngagementsTab.clickOnDatePickerCalendar();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        WebDriverTasks.pageDownCommon();
        EngagementsTab.clickOnSaveButtonOnCalendar();
		
		/*
		EngagementsTab.clickOnActionButton();
		EngagementsTab.clickOnViewDetails();
		SubwayDiversionDate = EngagementsTab.returnSubwayDiversionProgramStartDate();
		ClientInformation.clickOnClientInformationTab();
		WebDriverTasks.pageDownCommon();
		ClientInformation.expandChangelog();
		WebDriverTasks.pageDownCommon();
		ClientInformation.returnSubwayDiversionProgramStartDateChangelog();
		Assert.assertEquals(changeLogSubwayDiversionProgramStart, SubwayDiversionDate);

          */
    }

    //H20-802 - Add new client, add a new Engagement and choose contact date that is earlier than the enrollment start date and verify the
    //the error message appears
    @Test()
    public void verifyErrorMessageAppearsWhenContactDateIsEarlierThanEnrollmentDateWhenAddingEngagement() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + Helpers.returnRandomNumber();
        String lastName = "Automation" + random;

        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation" + random);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnProspectButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("2");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        ClientInformation.returnStreetsmartID();
        String chosenClientCategory = ClientInformation.returnSelectedProspectOrCaseLoadClientCategoryOnViewPage();
        Assert.assertEquals(chosenClientCategory, "Other Prospect");

        // 1st engagement
        AddEngagement.clickOnEngagementTab();

        AddEngagement.clickOnAddNewEngagementButton();
        AddEngagement.selectEngagementProvider("BG");
        AddEngagement.selectEngagementArea("Bronx");
        AddEngagement.selectEngagementShift("Evening");
        AddEngagement.selectEngagementJointOutreach("No");
        AddEngagement.selectEngagementContactReason("311 Response");
        AddEngagement.selectEngagementContactTime();
        AddEngagement.selectEngagementContactDateField();
        Common.clickOnLinkText("1");

        AddEngagement.selectEngagementOutcome("958 Certified Removal");
        AddEngagement.selectEngagementBorough("Bronx");
        AddEngagement.selectLocationType("Park");
        AddEngagement.selectParkName("Amersfort");
        AddEngagement.inputEngagementNotes("1");
        AddEngagement.selectSaveEngagement();
        AddEngagement.returnErrorMessage();
        Assert.assertEquals(AddEngagement.returnErrorMessage(), "The Contact Date cannot be earlier than the Enrollment Start Date.");


    }

    //Verify that the Engaged Unsheltered Client appears in the All Engagement grid when the Daily, Weekly and Monthly filters are applied
    @Test()
    public void verifyEngagedUnshelteredProspectAppearsOnGridwhenDailyMonthlyWeeklyFitlersAreApplied() {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnAllEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
        Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");

        Common.clickOnAdvancedFilter();
        Common.clickOnChooseFiter();
        Common.clickOnRadioClientCategory();
        Common.clickOnClientCategoryFilterButton2();
        Common.clickOnRadioEngagedUnsheltered();
        Common.clickOnApplyFilter();
        String valueOnGrid = AllEngagements.verifyPresenceOfEngagedUnshelteredOnGrid();
        Assert.assertEquals(valueOnGrid, "Engaged Unsheltered Prospect");

        AllEngagements.clickOnDateFilterYearlyButton();
        AllEngagements.clickOnDateFilterYearlyMenu();
        AllEngagements.selectYearonYearlyFilter("2020");

        valueOnGrid = AllEngagements.verifyPresenceOfEngagedUnshelteredOnGrid();
        Assert.assertEquals(valueOnGrid, "Engaged Unsheltered Prospect");

        AllEngagements.clickOnDateFilterMonthlyButton();
        Common.clickOnLinkText("July");

        valueOnGrid = AllEngagements.verifyPresenceOfEngagedUnshelteredOnGrid();
        Assert.assertEquals(valueOnGrid, "Engaged Unsheltered Prospect");


    }

    //Verify that the Inactive Client appears in the All Engagement grid when the Daily, Weekly and Monthly filters are applied


    @Test()
    public void verifyInactiveClientCategoryInExcelMyEngagementsCurrentView() throws IOException {

        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnMyEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyEngagements");
        Assert.assertEquals(MyEngagements.returnMyEngagementsBreadcrumb(), "My Engagements");

        Common.clickOnDateFilterYearlyButton();
        MyEngagements.clickOnYearlyDateFilterMenu();
        Common.selectYearonYearlyFilter("2019");
        MyEngagements.inputTextInSearchField("Inactive");
        MyEngagements.submitSearchField();
        String inactiveClientCategoryText = MyEngagements.returnInactiveClientCategory();
        Assert.assertEquals(inactiveClientCategoryText, "Inactive");

        int fileCountBefore = Helpers.countFilesInDownloadFolder();

        Common.clickOnExportToExcel();
        Common.clickOnExportToExcelCurrentView();
        Common.clickOnOKConfirmationButton();
        Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

        String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
        String clientCategoryColumnHeading = Helpers.returnExcelCellValue(mostRecentFile, "ClientEngagementReport", 0, 6);
        String clientCategoryFirstRecord = Helpers.returnExcelCellValue(mostRecentFile, "ClientEngagementReport", 1, 6);

        Assert.assertEquals(clientCategoryColumnHeading, "Client Category");
        Assert.assertEquals(clientCategoryFirstRecord, "Inactive");
    }

    @Test()
    public void verifyEngagedUnshelteredProspectClientCategoryInExcelMyEngagementsCurrentView() throws IOException {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnMyEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyEngagements");
        Assert.assertEquals(MyEngagements.returnMyEngagementsBreadcrumb(), "My Engagements");

        MyEngagements.selectProvider("BRC");
        Common.clickOnDateFilterYearlyButton();
        MyEngagements.clickOnYearlyDateFilterMenu();
        Common.selectYearonYearlyFilter("2020");
        MyEngagements.inputTextInSearchField("Engaged Unsheltered Prospect");
        MyEngagements.submitSearchField();
        String engagedUnshelteredClientCategoryText = MyEngagements.returnEngagedUnshelteredProspectCategory();
        Assert.assertEquals(engagedUnshelteredClientCategoryText, "Engaged Unsheltered Prospect");
        int fileCountBefore = Helpers.countFilesInDownloadFolder();

        Common.clickOnExportToExcel();
        Common.clickOnExportToExcelCurrentView();
        Common.clickOnOKConfirmationButton();
        Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

        String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
        String clientCategoryColumnHeading = Helpers.returnExcelCellValue(mostRecentFile, "ClientEngagementReport", 0, 6);
        String clientCategoryFirstRecord = Helpers.returnExcelCellValue(mostRecentFile, "ClientEngagementReport", 1, 6);

        Assert.assertEquals(clientCategoryColumnHeading, "Client Category");
        Assert.assertEquals(clientCategoryFirstRecord, "Engaged Unsheltered Prospect");
    }

    @Test()
    public void verifyInactiveClientCategoryInExcelAllEngagementsCurrentView() throws IOException {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnAllEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
        Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");
        AllEngagements.clickOnDateFilterYearlyButton();
        AllEngagements.clickOnDateFilterYearlyMenu();
        AllEngagements.selectYearonYearlyFilter("2020");
        AllEngagements.inputTextinAllEngagementSearchBox("Inactive");
        AllEngagements.submitSearchinGlobalSearchBox();

        String inactiveClientCategoryText = MyEngagements.returnInactiveClientCategory();
        Assert.assertEquals(inactiveClientCategoryText, "Inactive");

        int fileCountBefore = Helpers.countFilesInDownloadFolder();

        Common.clickOnExportToExcel();
        Common.clickOnExportToExcelCurrentView();
        Common.clickOnOKConfirmationButton();
        Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

        String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
        String clientCategoryColumnHeading = Helpers.returnExcelCellValue(mostRecentFile, "ClientEngagementReport", 0, 6);
        String clientCategoryFirstRecord = Helpers.returnExcelCellValue(mostRecentFile, "ClientEngagementReport", 1, 6);

        Assert.assertEquals(clientCategoryColumnHeading, "Client Category");
        Assert.assertEquals(clientCategoryFirstRecord, "Inactive");
    }

    @Test()
    public void verifyEngagedUnshelteredProspectClientCategoryInExcelAllEngagementsCurrentView() throws IOException {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnAllEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
        Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");
        AllEngagements.clickOnDateFilterYearlyButton();
        AllEngagements.clickOnDateFilterYearlyMenu();
        AllEngagements.selectYearonYearlyFilter("2020");
        AllEngagements.inputTextinAllEngagementSearchBox("Engaged Unsheltered Prospect");
        AllEngagements.submitSearchinGlobalSearchBox();

        String engagedUnshelteredClientCategoryText = MyEngagements.returnEngagedUnshelteredProspectCategory();
        Assert.assertEquals(engagedUnshelteredClientCategoryText, "Engaged Unsheltered Prospect");
        int fileCountBefore = Helpers.countFilesInDownloadFolder();

        Common.clickOnExportToExcel();
        Common.clickOnExportToExcelCurrentView();
        Common.clickOnOKConfirmationButton();
        Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

        String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
        String clientCategoryColumnHeading = Helpers.returnExcelCellValue(mostRecentFile, "ClientEngagementReport", 0, 6);
        String clientCategoryFirstRecord = Helpers.returnExcelCellValue(mostRecentFile, "ClientEngagementReport", 1, 6);

        Assert.assertEquals(clientCategoryColumnHeading, "Client Category");
        Assert.assertEquals(clientCategoryFirstRecord, "Engaged Unsheltered Prospect");
    }

    @Test
    public void verifyEngagedUnshelteredAdvancedFilterInAllEngagements() throws MalformedURLException {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnAllEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
        Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");

        Common.clickOnAdvancedFilter();
        Common.clickOnChooseFiter();
        Common.clickOnRadioClientCategory();
        Common.clickOnClientCategoryFilterButton();
        Common.clickOnRadioEngagedUnsheltered();
        Common.clickOnApplyFilter();
        String valueOnGrid = AllEngagements.verifyPresenceOfEngagedUnshelteredOnGrid();
        Assert.assertEquals(valueOnGrid, "Engaged Unsheltered Prospect");

    }


    @Test
    public void verifyInactiveOnGridwhenDailyMonthlyWeeklyFitlersAreApplied() {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnAllEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
        Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");

        Common.clickOnAdvancedFilter();
        Common.clickOnChooseFiter();
        Common.clickOnRadioClientCategory();
        Common.clickOnClientCategoryFilterButton2();
        Common.clickOnRadioInactive();
        Common.clickOnApplyFilter();
        String valueOnGrid = AllEngagements.verifyPresenceOfInactiveOnGrid();
        Assert.assertEquals(valueOnGrid, "Inactive");

        AllEngagements.clickOnDateFilterYearlyButton();
        AllEngagements.clickOnDateFilterYearlyMenu();
        AllEngagements.selectYearonYearlyFilter("2020");

        valueOnGrid = AllEngagements.verifyPresenceOfInactiveOnGrid();
        Assert.assertEquals(valueOnGrid, "Inactive");

        AllEngagements.clickOnDateFilterMonthlyButton();
        Common.clickOnLinkText("July");

        valueOnGrid = AllEngagements.verifyPresenceOfInactiveOnGrid();
        Assert.assertEquals(valueOnGrid, "Inactive");

    }

    //Create a new Engaged Unsheltered Client from Duplicate Search. And then verify client category of same client from duplicate search of client and engagement search
    @Test()
    public void addNewClientEngagedUnshelteredClientAndVerifyClientClientCategoryDuplicateSearchfromClientAndEngagementSearch() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + random;
        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation");
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
        Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName("Automation");
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput("Automation");
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BRC");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        String streetSmartID = ClientInformation.returnStreetsmartID();
        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");

        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientAndEngagementSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        ClientAndEngagementSearch.expandDuplicateSearchClientIDDetails();
        WebDriverTasks.pageDownCommon();
        ClientAndEngagementSearch.inputTextinFirstNameField(firstName);
        ClientAndEngagementSearch.inputTextinLastNameField("Automation");
        ClientAndEngagementSearch.clickOnDupliceSearchButton();
        Common.clickOnLinkText(streetSmartID);

        ClientCategory = ViewClientInformationfromSearch.returnClientCategory();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");

    }

    //Create a new Inactive Client.  And then verify client category of same client from duplicate search of client and engagement search
    @Test()
    public void addNewClientInactiveAndVerifyClientClientCategoryDuplicateSearchfromClientAndEngagementSearch() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + random;
        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation");
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
        Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName("Automation");
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput("Automation");
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BRC");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        String streetSmartID = ClientInformation.returnStreetsmartID();
        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");

        ClientInformation.clickOnEditClientInformationButton2();
        ClientInformation.selectClientCategoryOnEditPage2("Inactive");
        ClientInformation.clickSaveClientInformationButton();
        ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Inactive");

        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientAndEngagementSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        ClientAndEngagementSearch.expandDuplicateSearchClientIDDetails();
        WebDriverTasks.pageDownCommon();
        ClientAndEngagementSearch.inputTextinFirstNameField(firstName);
        ClientAndEngagementSearch.inputTextinLastNameField("Automation");
        ClientAndEngagementSearch.clickOnDupliceSearchButton();
        Common.clickOnLinkText(streetSmartID);

        ClientCategory = ViewClientInformationfromSearch.returnClientCategory();
        Assert.assertEquals(ClientCategory, "Inactive");

    }

    //Verify the client categories (Caseload, Other Prospect, Enrollment Ended, Engaged Unsheltered Prospect, Inactive) in the Duplicate Search
    @Test()
    public void verifyClientCategoriesinDuplicateSearchOfClientAndEngagementSearch() {

        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientAndEngagementSearch");

        ClientAndEngagementSearch.expandDuplicateSearchClientIDDetails();
        Assert.assertEquals(ClientAndEngagementSearch.returnduplicateSearchClientCategoryCaseload(), "Caseload");
        Assert.assertEquals(ClientAndEngagementSearch.returnduplicateSearchClientCategoryOtherProspect(), "Other Prospect");
        Assert.assertEquals(ClientAndEngagementSearch.returnduplicateSearchClientCategoryEnrollmentEnded(), "Enrollment Ended");
        Assert.assertEquals(ClientAndEngagementSearch.returnduplicateSearchClientCategoryEngagedUnshelteredProspect(), "Engaged Unsheltered Prospect");
        Assert.assertEquals(ClientAndEngagementSearch.returnduplicateSearchClientCategoryInactive(), "Inactive");
        ClientAndEngagementSearch.returnDuplicateSearchClientCategories();
        ClientAndEngagementSearch.selectDuplicateSearachClientCategory("Engaged Unsheltered Prospect");
        ClientAndEngagementSearch.clickOnDupliceSearchButton();
        Assert.assertEquals(ClientAndEngagementSearch.returnduplicateSearchEngagedUnshelteredProspectOnGrid(), "Engaged Unsheltered Prospect");
    }

    //Search for Client Category on Duplicate Search grid and verify Client Category is on column picker. And can be enabled and disabled
    //using column picker
    @Test()
    public void searchForClientCategoryOnDuplicateSearchGrid() {

        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientAndEngagementSearch");

        ClientAndEngagementSearch.expandDuplicateSearchClientIDDetails();
        ClientAndEngagementSearch.selectDuplicateSearachClientCategory("Engaged Unsheltered Prospect");
        ClientAndEngagementSearch.clickOnDupliceSearchButton();
        Assert.assertEquals(ClientAndEngagementSearch.returnduplicateSearchEngagedUnshelteredProspectOnGrid(), "Engaged Unsheltered Prospect");

        String clientCategoryColumnText = ClientAndEngagementSearch.returnclientCategoryColumnOnDuplicateSearch();
        Assert.assertEquals(clientCategoryColumnText, "Client Category");
        String valueOnGridText = ClientAndEngagementSearch.returnclientCategoryValueongridOnDuplicateSearch();
        Assert.assertEquals(valueOnGridText, "Engaged Unsheltered Prospect");
        ClientAndEngagementSearch.clickOnColumnPickerOnDuplicateSearch();
        ClientAndEngagementSearch.clickOnclientCategoryOnColumnPicker();
        ClientAndEngagementSearch.clickOnColumnPickerOnDuplicateSearch();
        boolean presenceOfClientCategoryColumn = ClientSearch.verifyIfclientCategoryColumnExistsOnDuplicateSearch();
        Assert.assertFalse(presenceOfClientCategoryColumn, "The Client Category Column is present");
        ClientAndEngagementSearch.clickOnColumnPickerOnDuplicateSearch();
        ClientAndEngagementSearch.clickOnclientCategoryOnColumnPicker();
        ClientAndEngagementSearch.clickOnColumnPickerOnDuplicateSearch();
        presenceOfClientCategoryColumn = ClientSearch.verifyIfclientCategoryColumnExistsOnDuplicateSearch();
        Assert.assertTrue(presenceOfClientCategoryColumn, "The Client Category Column is not present");
    }

    @Test()
    public void verifyInactiveClientCategoryAfterSearchingInAllEngagementsGrid() {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnAllEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
        Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");
        AllEngagements.clickOnDateFilterYearlyButton();
        AllEngagements.clickOnDateFilterYearlyMenu();
        AllEngagements.selectYearonYearlyFilter("2020");
        AllEngagements.inputTextinAllEngagementSearchBox("Inactive");
        AllEngagements.submitSearchinGlobalSearchBox();

        String inactiveClientCategoryText = AllEngagements.verifyPresenceOfInactiveOnGrid();
        Assert.assertEquals(inactiveClientCategoryText, "Inactive");
    }

    @Test()
    public void verifyEngagedUnshelteredProspectClientCategoryAfterSearchinginAllEngagementsGrid() {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnAllEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
        Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");
        AllEngagements.clickOnDateFilterYearlyButton();
        AllEngagements.clickOnDateFilterYearlyMenu();
        AllEngagements.selectYearonYearlyFilter("2020");
        AllEngagements.inputTextinAllEngagementSearchBox("Engaged Unsheltered Prospect");
        AllEngagements.submitSearchinGlobalSearchBox();

        String engagedUnshelteredClientCategoryText = AllEngagements.verifyPresenceOfEngagedUnshelteredOnGrid();

        Assert.assertEquals(engagedUnshelteredClientCategoryText, "Engaged Unsheltered Prospect");

    }


    //Go to My Engagements and search for Inactive and verify it appears in Client Category
    @Test()
    public void verifyInactiveClientCategoryAfterSearchingInMyEngagements() {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnMyEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyEngagements");
        Assert.assertEquals(MyEngagements.returnMyEngagementsBreadcrumb(), "My Engagements");

        Common.clickOnDateFilterYearlyButton();
        MyEngagements.clickOnYearlyDateFilterMenu();
        Common.selectYearonYearlyFilter("2020");
        MyEngagements.inputTextInSearchField("Inactive");
        MyEngagements.submitSearchField();

        MyEngagements.verifyInactiveCellsInClientCategoryColumn();
    }

    //Go to My Engagements and search for Engaged Unsheltered Prospect and verify it appears in Client Category
    @Test()
    public void verifyEngagedUnshelteredProspectClientCategoryAfterSearchingInMyEngagements() {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnMyEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyEngagements");
        Assert.assertEquals(MyEngagements.returnMyEngagementsBreadcrumb(), "My Engagements");

        Common.clickOnDateFilterYearlyButton();
        MyEngagements.clickOnYearlyDateFilterMenu();
        Common.selectYearonYearlyFilter("2020");
        MyEngagements.inputTextInSearchField("Engaged Unsheltered Prospect");
        MyEngagements.submitSearchField();
        MyEngagements.verifyEngagedUnshelteredProspectCellsInClientCategoryColumn();


    }


    @Test
    public void verifyInactiveAdvancedFilterInAllEngagements() throws MalformedURLException {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnAllEngagementsMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
        Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");

        Common.clickOnAdvancedFilter();
        Common.clickOnChooseFiter();
        Common.clickOnRadioClientCategory();
        Common.clickOnClientCategoryFilterButton();
        Common.clickOnRadioInactive();
        Common.clickOnApplyFilter();
        String valueOnGrid = AllEngagements.verifyPresenceOfInactiveOnGrid();
        Assert.assertEquals(valueOnGrid, "Inactive");
    }

    //Create a new Engaged Unsheltered Client from Duplicate Search. And then verify client category of same client from client and engagement search
    @Test()
    public void addNewClientEngagedUnshelteredClientAndVerifyClientAndEngagementSearch() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + random;
        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation");
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
        Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName("Automation");
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput("Automation");
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BRC");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        String streetSmartID = ClientInformation.returnStreetsmartID();
        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");


        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientAndEngagementSearch");

        ClientAndEngagementSearch.expandClientSearchPanel();
        WebDriverTasks.pageDownCommon();
        ClientAndEngagementSearch.inputTextinClientSearchFirstNameField(firstName);
        ClientAndEngagementSearch.inputTextinClientSearchLastNameField("Automation");
        WebDriverTasks.pageDownCommon();
        ClientAndEngagementSearch.clickOnClientSearchButton();
        Common.clickOnLinkText(streetSmartID);

        ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");

    }

    //Create a new Engaged Unsheltered Client from Duplicate Search. And then verify client category of same client from client and engagement search
    @Test(enabled = false)
    public void addNewInactiveClientAndVerifyClientAndEngagementSearch() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + random;
        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation");
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
        Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName("Automation");
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput("Automation");
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BRC");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        String streetSmartID = ClientInformation.returnStreetsmartID();
        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");

        WebDriverTasks.pageDownCommon();
        ClientInformation.clickOnEditButton();
        ClientInformation.selectClientCategoryAllTabsOnEditPage("Inactive");
        WebDriverTasks.pageDownCommon();
        ClientInformation.clickSaveClientInformationButton();

        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientAndEngagementSearch");

        ClientAndEngagementSearch.expandClientSearchPanel();
        WebDriverTasks.pageDownCommon();
        ClientAndEngagementSearch.inputTextinClientSearchFirstNameField(firstName);
        ClientAndEngagementSearch.inputTextinClientSearchLastNameField("Automation");
        WebDriverTasks.pageDownCommon();
        ClientAndEngagementSearch.clickOnClientSearchButton();
        Common.clickOnLinkText(streetSmartID);

        ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Inactive");

    }

    //verify inactive client category of existing client from client and engagement search
    @Test()
    public void verifyInactiveClientAndVerifyClientAndEngagementSearch() {

        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientAndEngagementSearch");

        ClientAndEngagementSearch.expandClientSearchPanel();
        WebDriverTasks.pageDownCommon();

        ClientAndEngagementSearch.inputTextinClientSearchFirstNameField("User3823");
        ClientAndEngagementSearch.inputTextinClientSearchLastNameField("Automation");
        WebDriverTasks.pageDownCommon();
        ClientAndEngagementSearch.clickOnClientSearchButton();
        Common.clickOnLinkText("C161054");

        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Inactive");

    }


    //H20-684 As a User, when I perform a Client and Engagement search using ‘Client Category' then I must see Client Category as a column within the Client and Engagement search results grid
    @Test()
    public static void VerifyClientSearchClientCategoryOptions() {
        //Navigate to Add Engagement page
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();

        //Expand Client search pane
        ClientAndEngagementSearch.expandClientSearchPanel();
        WebDriverTasks.pageDownCommon();

        //Expand Client ID section
        ClientAndEngagementSearch.expandClientIDSectionInClientSearch();

        //Verify Client Category dropdown is displayed in client search
        boolean isClientCategoryDisplayed = ClientAndEngagementSearch.verifyClientCategoryDisplayedInClientSearch();
        Assert.assertTrue(isClientCategoryDisplayed, "Client category dropdown is not displayed");

        //Verify that Client Category dropdown contains "Caseload", "Other Prospect","Enrollment Ended","Engaged Unsheltered Prospect" and "Inactive options"
        boolean isOptionsAsExpected = ClientAndEngagementSearch.verifyClientCategoryOptionsInClientSearch();
        Assert.assertTrue(isOptionsAsExpected, "Unexpected options are displayed in Client category dropdown");

        //Select 'Caseload' and search
        ClientAndEngagementSearch.selectClientCategory("Caseload");
        ClientAndEngagementSearch.clickOnSearchButtonInClientSearchPane();

        //Verify that in result grid 'Client Category' column contains only 'Caseload' data
        boolean isDataFiltered = ClientAndEngagementSearch.verifyClientCategoryColumnDataIs("Caseload");
        Assert.assertTrue(isDataFiltered, "Other than 'Caseload' data is displayed in Client Category column");

        //Expand Client ID section
        ClientAndEngagementSearch.expandClientIDSectionInClientSearch();

        //Select 'Inactive' and search
        ClientAndEngagementSearch.selectClientCategory("Inactive");
        ClientAndEngagementSearch.clickOnSearchButtonInClientSearchPane();

        //Verify that in result grid 'Client Category' column contains only 'Inactive' data
        isDataFiltered = ClientAndEngagementSearch.verifyClientCategoryColumnDataIs("Inactive");
        Assert.assertTrue(isDataFiltered, "Other than 'Inactive' data is displayed in Client Category column");

    }

    // H20-686 As a User, when I perform a Client and Engagement search using ‘Client Category' then I must see Client Category as a column within the Client and
    // Engagement search results grid
    @Test()
    public static void VerifyClientCategoryColumnInClientSearchResult() {
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();

        ClientAndEngagementSearch.expandClientSearchPanel();
        WebDriverTasks.pageDownCommon();

        ClientAndEngagementSearch.clickOnSearchButtonInClientSearchPane();
        boolean isColumnDIsplayed = ClientAndEngagementSearch.verifyClientCategoryColumnInClientSearchGrid();
        Assert.assertTrue(isColumnDIsplayed, "'Client Category' column is not displayed in search result grid");
    }

    //H20-687 As a User, when I perform a Client and Engagement search and see the search results grid, I must see Client Category added in the Column Picker
    @Test()
    public void VerifyClientCategoryColumnPickerInClientSearchPane() {
        //Navigate to Add Engagement page
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();

        //Expand Client Search pane
        ClientAndEngagementSearch.expandClientSearchPanel();
        WebDriverTasks.pageDownCommon();

        // Perform Search
        ClientAndEngagementSearch.clickOnSearchButtonInClientSearchPane();

        //Verify that Client Category is displayed in column picker
        ClientAndEngagementSearch.clickOnColumnPickerInClientSearchPane();
        boolean isClientCategoryDisplayed = ClientAndEngagementSearch.verifyClientCategoryIsDisplayedInColumnPicker();
        Assert.assertTrue(isClientCategoryDisplayed, "Client category column is not displayed in column picker for Client search pane");

        //Verify that on un-checking 'Client Category' from column picker, 'Client Category' column should be removed from data grid
        boolean isClientCategoryRemoved = ClientAndEngagementSearch.verifyHideColumnPickerFunctionalityForClientCategory(false);
        Assert.assertTrue(isClientCategoryRemoved, "Client Category column is not removed from data grid");

        //Verify that on marking 'Client Category' from column picker, 'Client Category' column should be displayed in data grid
        isClientCategoryDisplayed = ClientAndEngagementSearch.verifyHideColumnPickerFunctionalityForClientCategory(true);
        Assert.assertTrue(isClientCategoryDisplayed, "Client Category column is not displayed in data grid");
    }

    //H20-815: As a User,when I click on All Engagements within Engagements on the left navigation,I want JCC,JCC Client Start Date to be displayed on All Engagements grid by default.
    @Test()
    public static void VerifyJCCClientAndJCCClientStartDateColumnsOnAllEngagementsPage() {
        //Navigate to All Engagements page
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAllEngagementsMenuItem();

        //This will verify if columns 'JCC Client' and 'JCC Client Start Date' are displayed
        boolean jccClientColumnDisplayed = AllEngagements.verifyJCCClientColumnExists();
        Assert.assertTrue(jccClientColumnDisplayed, "JCC Client column is not displayed in data grid");
        boolean jccClientStartDateColumnDisplayed = AllEngagements.verifyJCCClientStartDateColumnExists();
        Assert.assertTrue(jccClientStartDateColumnDisplayed, "JCC Client Start Date column is not displayed in data grid");

        //This will verify that columns 'Preferred Pronoun' and 'Preferred Name' are not displayed by default
        boolean preferredPronounColumn = AllEngagements.verifyPreferredPronounColumnIsNotDisplayed();
        Assert.assertTrue(preferredPronounColumn, "'Preferred Pronoun' column is displayed on data grid");
        boolean preferredNameColumn = AllEngagements.verifyPreferredNameColumnIsNotDisplayed();
        Assert.assertTrue(preferredNameColumn, "'Preferred Name' column is displayed on data grid");

        //This will verify that last two columns are 'JCC Client' and 'JCC Client Start Date'
        boolean verifyLastTwoColumnsAreJCC = AllEngagements.verifyLastTwoColumnInGridJCCClient();
        Assert.assertTrue(verifyLastTwoColumnsAreJCC, "Last two columns in data grid should be 'JCC Client' and 'JCC Client Start Date' respectively");

        AllEngagements.clickOnColumnPicker();

        //This will verify that 'JCC Client' and 'JCC Client Start Date' options are checked by default in column picker
        boolean isJCCClientOptionChecked = AllEngagements.verifyJCCClientOptionIsCheckedInColumnPicker();
        Assert.assertTrue(isJCCClientOptionChecked, "'JCC Client' column should be checked by default in column picker");
        boolean isJCCClientStartDateOptionChecked = AllEngagements.verifyJCCClientStartDateIsCheckedInColumnPicker();
        Assert.assertTrue(isJCCClientStartDateOptionChecked, "'JCC Client Start Date' column should be checked by default in column picker");

        //This will verify that 'Preferred Name' and 'Preferred Pronoun' options are unchecked by default in column picker
        boolean isPreferredPronounUnchecked = AllEngagements.VerifyPreferredPronounOptionIsUncheckedInColumnPicker();
        Assert.assertTrue(isPreferredPronounUnchecked, "'Preferred Pronoun' option is either not displayed in column picker or it's checked by default");
        boolean isPreferredNameUnchecked = AllEngagements.VerifyPreferredNameOptionIsUncheckedInColumnPicker();
        Assert.assertTrue(isPreferredNameUnchecked, "'Preferred Name' option is either not displayed in column picker or it's checked by default");
    }


    //H20-816 As a User,when I click on My Engagements within Engagements on the left navigation,I want JCC,JCC Client Start Date to be displayed on My Engagements grid by default.
    @Test()
    public static void VerifyJCCClientAndJCCClientStartDateColumnsOnMyEngagementsPage() {
        //Navigate to My Engagements page
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnMyEngagementsMenuItem();

        //This will verify if columns 'JCC Client' and 'JCC Client Start Date' are displayed
        boolean jccClientColumnDisplayed = MyEngagements.verifyJCCClientColumnExists();
        Assert.assertTrue(jccClientColumnDisplayed, "JCC Client column is not displayed in data grid");
        boolean jccClientStartDateColumnDisplayed = MyEngagements.verifyJCCClientStartDateColumnExists();
        Assert.assertTrue(jccClientStartDateColumnDisplayed, "JCC Client Start Date column is not displayed in data grid");

        //This will verify that columns 'Preferred Pronoun' and 'Preferred Name' are not displayed by default
        boolean preferredPronounColumn = MyEngagements.verifyPreferredPronounColumnIsNotDisplayed();
        Assert.assertTrue(preferredPronounColumn, "'Preferred Pronoun' column is displayed on data grid");
        boolean preferredNameColumn = MyEngagements.verifyPreferredNameColumnIsNotDisplayed();
        Assert.assertTrue(preferredNameColumn, "'Preferred Name' column is displayed on data grid");

        //This will verify that last two columns are 'JCC Client' and 'JCC Client Start Date'
        boolean verifyLastTwoColumnsAreJCC = MyEngagements.verifyLastTwoColumnInGridJCCClient();
        Assert.assertTrue(verifyLastTwoColumnsAreJCC, "Last two columns in data grid should be 'JCC Client' and 'JCC Client Start Date' respectively");

        MyEngagements.clickOnColumnPicker();

        //This will verify that 'JCC Client' and 'JCC Client Start Date' options are checked by default in column picker
        boolean isJCCClientOptionChecked = MyEngagements.verifyJCCClientOptionIsCheckedInColumnPicker();
        Assert.assertTrue(isJCCClientOptionChecked, "'JCC Client' column should be checked by default in column picker");
        boolean isJCCClientStartDateOptionChecked = MyEngagements.verifyJCCClientStartDateIsCheckedInColumnPicker();
        Assert.assertTrue(isJCCClientStartDateOptionChecked, "'JCC Client Start Date' column should be checked by default in column picker");

        //This will verify that 'Preferred Name' and 'Preferred Pronoun' options are unchecked by default in column picker
        boolean isPreferredPronounUnchecked = MyEngagements.VerifyPreferredPronounOptionIsUncheckedInColumnPicker();
        Assert.assertTrue(isPreferredPronounUnchecked, "'Preferred Pronoun' option is either not displayed in column picker or it's checked by default");
        boolean isPreferredNameUnchecked = MyEngagements.VerifyPreferredNameOptionIsUncheckedInColumnPicker();
        Assert.assertTrue(isPreferredNameUnchecked, "'Preferred Name' option is either not displayed in column picker or it's checked by default");
    }

}