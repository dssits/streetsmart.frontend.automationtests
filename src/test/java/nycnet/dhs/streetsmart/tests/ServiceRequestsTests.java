package nycnet.dhs.streetsmart.tests;

import java.io.IOException;
import java.net.MalformedURLException;

import org.testng.Assert;
import org.testng.annotations.Test;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.Common;
import nycnet.dhs.streetsmart.pages.ServiceRequests;
import nycnet.dhs.streetsmart.pages.SignIn;
import nycnet.dhs.streetsmart.pages.admin.UserPermissions;


public class ServiceRequestsTests extends AutoRunTestController {
	//Verify ability to close service request
	@Test
	public static void VerifyServiceRequestClosingFunctionality(){
		        Base.clickOnOutreachRequests();
				Base.waitForSideBarMenuToBexpanded();
				Base.clickOnServiceRequests();
				int onsiteTileTotalCountBefore = ServiceRequests.returnOnsiteCountOnTileServiceRequest();
				int closedResolvedTileTotalCountBefore = ServiceRequests.returnClosedResolvedOnCountOnTile();
				ServiceRequests.clickOnAssignedDurationDetails();
				String serviceRequestNumber = ServiceRequests.clickOnServiceRequest();
				ServiceRequests.clickOnCloseOutServiceRequestButton();
				ServiceRequests.chooseCloseOutAdministrativelyClosedOptionOption();
				ServiceRequests.clickOnConfirmButton();
				ServiceRequests.clickOnclosedResolvedTileDetailsButton();
				int onsiteTileTotalCountAfter = ServiceRequests.returnOnsiteCountOnTileServiceRequest();
				int closedResolvedTileTotalCountAfter = ServiceRequests.returnClosedResolvedOnCountOnTile();
				ServiceRequests.searchServiceRequest(serviceRequestNumber);
				ServiceRequests.clickOnServiceRequest();
				String serviceRequestPopUpOnTitle = ServiceRequests.returnServiceRequestOnTitle();
				Assert.assertEquals(onsiteTileTotalCountAfter, onsiteTileTotalCountBefore);
				Assert.assertEquals(closedResolvedTileTotalCountBefore + 1, closedResolvedTileTotalCountAfter);
				Assert.assertEquals(serviceRequestPopUpOnTitle, serviceRequestNumber);
	}
	
	//Verify the Email Configuration functionality
	 @Test ()
		public void verifyEmailConfigurationOnServiceRequestsPage () {
		  Base.clickOnOutreachRequests();
			Base.waitForSideBarMenuToBexpanded();
			Base.clickOnServiceRequests();
			ServiceRequests.clickOnEmailConfigurationButton();
			String originalFromEmailOnFromInput = ServiceRequests.returnTextToEmailConfigurationFromInput();
			String exampleFromEmail = "testemail1@test1.com";
			String exampleSubject = "Test Subject";
			String exampleBody = "Test Body";
			
			//Verify when you cancel what was inputted is not saved
			ServiceRequests.clearEmailConfigurationFromInput();
			ServiceRequests.inputTextToEmailConfigurationFromInput(exampleFromEmail);
			String inputttedFromEmailOnFromInput = ServiceRequests.returnTextToEmailConfigurationFromInput();
			Assert.assertEquals(inputttedFromEmailOnFromInput, exampleFromEmail);
			ServiceRequests.clickOnEmailConfigurationCloseButton();
			ServiceRequests.clickOnEmailConfigurationButton();
			inputttedFromEmailOnFromInput = ServiceRequests.returnTextToEmailConfigurationFromInput();
			Assert.assertEquals(inputttedFromEmailOnFromInput, originalFromEmailOnFromInput);
			
			String OriginalSubjectOnSubjectInput = ServiceRequests.returnTextToEmailConfigurationSubjectInput();
			String OriginalBodyOnBodyInput = ServiceRequests.returnTextToEmailConfigurationBodyInput();
			
			ServiceRequests.clearEmailConfigurationFromInput();
			ServiceRequests.inputTextToEmailConfigurationFromInput(exampleFromEmail);
			ServiceRequests.clearEmailConfigurationSubjectInput();
			ServiceRequests.inputTextToEmailConfigurationSubjectInput(exampleSubject);
			ServiceRequests.clearEmailConfigurationBodyInput();
			ServiceRequests.inputTextToEmailConfigurationBodyInput(exampleBody);
			ServiceRequests.clickOnEmailConfigurationSaveButton();
			
			ServiceRequests.clickOnEmailConfigurationButton();
			String savedFromEmailOnFromInput = ServiceRequests.returnTextToEmailConfigurationFromInput();
			String savedSubjectFromSubjectInput = ServiceRequests.returnTextToEmailConfigurationSubjectInput();
			String savedBodyFromBodyInput = ServiceRequests.returnTextToEmailConfigurationBodyInput();
			
			Assert.assertEquals(savedFromEmailOnFromInput, exampleFromEmail);
			Assert.assertEquals(savedSubjectFromSubjectInput, exampleSubject);
			Assert.assertEquals(savedBodyFromBodyInput, exampleBody);
			
			ServiceRequests.clearEmailConfigurationFromInput();
			ServiceRequests.inputTextToEmailConfigurationFromInput(originalFromEmailOnFromInput);
			ServiceRequests.clearEmailConfigurationSubjectInput();
			ServiceRequests.inputTextToEmailConfigurationSubjectInput(OriginalSubjectOnSubjectInput);
			ServiceRequests.clearEmailConfigurationBodyInput();
			ServiceRequests.inputTextToEmailConfigurationBodyInput(OriginalBodyOnBodyInput);
			ServiceRequests.clickOnEmailConfigurationSaveButton();
			
			ServiceRequests.clickOnEmailConfigurationButton();
			Assert.assertEquals(ServiceRequests.returnTextToEmailConfigurationFromInput(), originalFromEmailOnFromInput);
			Assert.assertEquals(ServiceRequests.returnTextToEmailConfigurationSubjectInput(), OriginalSubjectOnSubjectInput);
			Assert.assertEquals(ServiceRequests.returnTextToEmailConfigurationBodyInput(), OriginalBodyOnBodyInput);
		}
	
	//Verify the presence of elements in the Service Request Maps section
	 @Test ()
		public void verifyMapsSectionOnServiceRequestsPage () throws IOException {
		  Base.clickOnOutreachRequests();
			Base.waitForSideBarMenuToBexpanded();
			Base.clickOnServiceRequests();
			
			
			Assert.assertEquals(ServiceRequests.returnServiceRequestMapHeading(), " Outreach Team Locations and Open Service Requests");
			ServiceRequests.selectMapsBoroughOnDropDown("Brooklyn");
			ServiceRequests.selectMapstatusOnDropDown("Assigned");
			Assert.assertTrue(ServiceRequests.verifyPresenceOfServiceRequestMapHeading(), "The Map Heading is not present");
			Assert.assertTrue(ServiceRequests.verifyPresenceOfGoButton(), "The Go button is not present");
		}
	 
	 
	//H20-699 - Go to 311 Service Requests, open the OnSite grid, verify the onsite time column is in descending order
	
	  @Test ()
		public void verifyOnsiteTimecolumnIsInDescendingOrder () throws IOException {
		  Base.clickOnOutreachRequests();
			Base.waitForSideBarMenuToBexpanded();
			Base.clickOnServiceRequests();
			Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/Service311Request");
			ServiceRequests.clickOnOnsiteDetailsButtonToOpenGrid ();
			Assert.assertEquals(ServiceRequests.OnsiteTimeColumnSortingDirection(), "Sort Descending");
		}
	  
	  @Test ()
		public void verifyOnsiteTimecolumnCanBeEnabledAndDisabledUsingColumnPicker () throws IOException {
		  Base.clickOnOutreachRequests();
			Base.waitForSideBarMenuToBexpanded();
			Base.clickOnServiceRequests();
			Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/Service311Request");
			ServiceRequests.clickOnOnsiteDetailsButtonToOpenGrid ();
			
			Assert.assertTrue(ServiceRequests.verifyIfOnsiteColumn(), "Onsite Time Column does not exist");
			Assert.assertEquals(ServiceRequests.returnOnsiteColumnText(), "Onsite Time");
			Common.clickOnColumnPicker();
			ServiceRequests.clickOnOnsiteColumnOnColumnPickerToEnableIt();
			Common.clickOnColumnPicker();
			Assert.assertFalse(ServiceRequests.verifyIfOnsiteColumn(), "Onsite Time Column does exist");
			Common.clickOnColumnPicker();
			ServiceRequests.clickOnOnsiteColumnOnColumnPickerToEnableIt();
			Common.clickOnColumnPicker();
			Assert.assertTrue(ServiceRequests.verifyIfOnsiteColumn(), "Onsite Time Column does not exist");
		}
	  
	  /*H20-893 - Changed role of user to 311 Dispatch and verify on the UnAssigned grid the received duration column is in Ascending order
		H20-892- and that on the Assigned grid the assigned duration column is in Ascending order.
		*/
		@Test()
		public void verifyDefaultColumnSortDirectionforAssignedAndUnAssignedGridsin311ServiceRequests() throws MalformedURLException {
			Base.clickOnWhatsNewNotificaitonCloseButton();
			WebDriverTasks.specificWait(2000);
			String nameOfUser = Base.returnLogOutNavBarName();
			
			Base.clickOnLogoutNavBar();
			Base.clickOnLogoutButton();
			SignIn.inputUserName("dhs\\PRA_TestMOC3");
			SignIn.passwordInputBox("Welcome4");
			SignIn.clickOnSignInButton();
			
			Base.clickOnAdminMenu();
			Base.waitForSideBarMenuToBexpanded();
			Base.clickOnUserPermissionsMenuItem();
			UserPermissions.enterQueryInSearchInput(nameOfUser);
			UserPermissions.submitQueryInSearchInput();
		    UserPermissions.clickOnActionButtonOnGridResults();
		    UserPermissions.clickOnViewDetailsButtonOnGridResults();
		    UserPermissions.clickOnEditUserDetails();
		    UserPermissions.clickOnUserRoleDropDown();
		    UserPermissions.selectOnUserRoleDropDownOption("311 Dispatch");
		    UserPermissions.scrollToSaveUserDetailsButton();
		    UserPermissions.clickOnSaveUserDetailsButton();
		    
		    closeWebDriver();
		    setUp();
		   
			Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/Service311Request");
			Assert.assertEquals(ServiceRequests.receivedDurationColumnSortingDirection(), "Sort Ascending");
			ServiceRequests.clickOnAssignedDurationDetails();
			Assert.assertEquals(ServiceRequests.assignedDurationColumnSortingDirection(), "Sort Ascending");
			
			ServiceRequests.clickOnOnsiteDetailsButtonToOpenGrid ();
			Assert.assertTrue(ServiceRequests.verifyIfOnsiteColumn(), "Onsite Time Column does not exist");
			Assert.assertEquals(ServiceRequests.returnOnsiteColumnText(), "Onsite Time");
			Common.clickOnColumnPicker();
			ServiceRequests.clickOnOnsiteColumnOnColumnPickerToEnableIt();
			Common.clickOnColumnPicker();
			Assert.assertFalse(ServiceRequests.verifyIfOnsiteColumn(), "Onsite Time Column does exist");
			Common.clickOnColumnPicker();
			ServiceRequests.clickOnOnsiteColumnOnColumnPickerToEnableIt();
			Common.clickOnColumnPicker();
			Assert.assertTrue(ServiceRequests.verifyIfOnsiteColumn(), "Onsite Time Column does not exist");
			
		    Base.clickOnWhatsNewNotificaitonCloseButton();
			WebDriverTasks.specificWait(2000);
			Base.clickOnLogoutNavBar();
			Base.clickOnLogoutButton();
			SignIn.inputUserName("dhs\\PRA_TestMOC3");
			SignIn.passwordInputBox("Welcome4");
			SignIn.clickOnSignInButton();
			Base.clickOnAdminMenu();
			Base.waitForSideBarMenuToBexpanded();
			Base.clickOnUserPermissionsMenuItem();
			UserPermissions.enterQueryInSearchInput(nameOfUser);
			UserPermissions.submitQueryInSearchInput();
		    UserPermissions.clickOnActionButtonOnGridResults();
		    UserPermissions.clickOnViewDetailsButtonOnGridResults();
		    UserPermissions.clickOnEditUserDetails();
		    UserPermissions.clickOnUserRoleDropDown();
		    UserPermissions.selectOnUserRoleDropDownOption("Super Admin");
		    UserPermissions.scrollToSaveUserDetailsButton();
		    UserPermissions.clickOnSaveUserDetailsButton();
		   
		}
	  
	
  
 

}