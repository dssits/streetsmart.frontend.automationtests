package nycnet.dhs.streetsmart.tests;



import java.io.IOException;

import nycnet.dhs.streetsmart.pages.dashboard.Monthly;
import nycnet.dhs.streetsmart.pages.reports.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import nycnet.dhs.streetsmart.core.Config;
import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.Common;

import javax.swing.text.View;

public class Reports extends AutoRunTestController {

	 //H20-827 - Create a custom report with Facility Name and Facility Address Fields and verify fields are in report and then 
	//edit the report and verify the changes
	  @Test ()
	 	public void createCustomFacilityReportAndEditReport() throws IOException {
	 	  Base.clickOnReports();
	 	  Base.clickOnCustomReport();
	 	  Base.clickOnCreateReport();
	 	  CreateReport.clickOnNameEdit();
	 	  String reportName = CreateReport.enterReportName();
	 	  CreateReport.clickOnSaveButton();
	 	  CreateReport.clickOnDemographicsSection();
	 	  CreateReport.clickStreetSmartIDOnDemographicsSection();
	 	  CreateReport.clickOnEnrolledClientsSection();
	 	  WebDriverTasks.pageDownCommon();
	 	 CreateReport.clickOnFacilityNameEnrolledSection();
	 	 CreateReport.clickOnFacilityNameDropDownEnrolledSection();
	 	 CreateReport.clickOnBrookynSkyHotelFacilityAddressEnrolledSection();
	 	  CreateReport.clickOnFacilityAddressEnrolledSection();
	 	 
	 	  CreateReport.clickOnPreviewButton();
	 	  
	 	 Assert.assertEquals(PreviewReport.returnFacilityNameColumnOnGrid(), "Facility Name");
		 Assert.assertEquals(PreviewReport.returnFacilityAddressColumnOnGrid(), "Facility Address");
		 Assert.assertEquals(PreviewReport.returnFacilityNameBrookynSkyHotelOnGrid(), "Brookyn Sky Hotel");
		 
	 	  PreviewReport.clickOnCreateReport();
	 	  
	 	  WebDriverTasks.specificWait(3000);
	 	  
	 	  PreviewReport.clickOnSaveReport();
	 	 
	 	  PreviewReport.clickOnConfirmButton();
	 	  
	 	WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
		  
		ViewReports.clickByPartialLinkText(reportName);
	 	
		 Assert.assertEquals(ViewReports.returnFacilityNameColumnOnGrid(), "Facility Name");
		 Assert.assertEquals(ViewReports.returnFacilityAddressColumnOnGrid(), "Facility Address");
		 Assert.assertEquals(PreviewReport.returnFacilityNameBrookynSkyHotelOnGrid(), "Brookyn Sky Hotel");
		 ViewReports.clickOnEditButton();
		 CreateReport.clickOnFacilityNameBrookynSkyDropDownEnrolledSection();;
	 	 CreateReport.clickOnBrookynSkyHotelFacilityAddressEnrolledSection();
	 	 CreateReport.clickOnBethelFacilityAddressEnrolledSection();
	 	CreateReport.clickOnPreviewButton();
	 	  
	 	 Assert.assertEquals(PreviewReport.returnFacilityNameColumnOnGrid(), "Facility Name");
		 Assert.assertEquals(PreviewReport.returnFacilityAddressColumnOnGrid(), "Facility Address");
		 Assert.assertEquals(PreviewReport.returnFacilityNameBethelOnGrid(), "Bethel");
		 
          PreviewReport.clickOnCreateReport();
	 	  
	 	  WebDriverTasks.specificWait(3000);
	 	  
	 	  PreviewReport.clickOnSaveReport();
	 	 
	 	  PreviewReport.clickOnConfirmButton();
	 	  
	 	WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
		  
		ViewReports.clickByPartialLinkText(reportName);
		
		 Assert.assertEquals(ViewReports.returnFacilityNameColumnOnGrid(), "Facility Name");
		 Assert.assertEquals(ViewReports.returnFacilityAddressColumnOnGrid(), "Facility Address");
		 Assert.assertEquals(ViewReports.returnFacilityNameBethelOnGrid(), "Bethel");		 
	 	}
	
	 //Create a custom report with Facility Fields and verify fields are in report
	  @Test ()
	 	public void createCustomFacilityReport() throws IOException {
	 	  Base.clickOnReports();
	 	  Base.clickOnCustomReport();
	 	  Base.clickOnCreateReport();
	 	  CreateReport.clickOnNameEdit();
	 	  String reportName = CreateReport.enterReportName();
	 	  CreateReport.clickOnSaveButton();
	 	  CreateReport.clickOnDemographicsSection();
	 	  CreateReport.clickStreetSmartIDOnDemographicsSection();
	 	  CreateReport.clickOnEnrolledClientsSection();
	 	  WebDriverTasks.pageDownCommon();
	 	 CreateReport.clickOnFacilityNameEnrolledSection();
	 	  CreateReport.clickOnFacilityAddressEnrolledSection();
	 	  CreateReport.clickOnPreviewButton();
	 	  
	 	 Assert.assertEquals(PreviewReport.returnFacilityNameColumnOnGrid(), "Facility Name");
		 Assert.assertEquals(PreviewReport.returnFacilityAddressColumnOnGrid(), "Facility Address");
	 	 
	 	  PreviewReport.clickOnCreateReport();
	 	  
	 	  try {
	 		Thread.sleep(3000);
	 	} catch (InterruptedException e) {
	 		
	 		e.printStackTrace();
	 	}
	 	  
	 	  PreviewReport.clickOnSaveReport();
	 	 
	 	  PreviewReport.clickOnConfirmButton();
	 	  
	 	WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
		  
		ViewReports.clickByPartialLinkText(reportName);
	 	
		 Assert.assertEquals(ViewReports.returnFacilityNameColumnOnGrid(), "Facility Name");
		 Assert.assertEquals(ViewReports.returnFacilityAddressColumnOnGrid(), "Facility Address");
		
	 	}
	
	  
	  //Create a custom report with Engaged Unsheltered Prospect and then edit the report
	  @Test ()
	 	public void createCustomReportAndEditReport() throws IOException {
	 	  Base.clickOnReports();
	 	  Base.clickOnCustomReport();
	 	  Base.clickOnCreateReport();
	 	  CreateReport.clickOnNameEdit();
	 	  String reportName = CreateReport.enterReportName();
	 	  CreateReport.clickOnSaveButton();
	 	  CreateReport.clickOnDemographicsSection();
	 	  CreateReport.clickStreetSmartIDOnDemographicsSection();
	 	  CreateReport.clickOnEnrolledClientsSection();
	 	  WebDriverTasks.pageDownCommon();
	 	  CreateReport.clickOnclientCategoryOnEnrolledSection();
	 	  CreateReport.clickOnclientCategoryDropDownOnEnrolledSection();
	 	  CreateReport.clickOnEngagedUnshelteredProspectOnClientCategoryDropDown();
	 	  CreateReport.clickOnPreviewButton();
	 	 Assert.assertEquals(ViewReports.returnEngagedUnshelteredProspectOnGrid(), "Engaged Unsheltered Prospect");
	 	  
	 	  
	 	  PreviewReport.clickOnCreateReport();
	 	  try {
	 		Thread.sleep(3000);
	 	} catch (InterruptedException e) {
	 		// TODO Auto-generated catch block
	 		e.printStackTrace();
	 	}
	 	  PreviewReport.clickOnSaveReport();
	 	 
	 	  PreviewReport.clickOnConfirmButton();
	 	  
	 	 WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
		  
		  ViewReports.clickByPartialLinkText(reportName);
	 	  
	 	 Assert.assertEquals(ViewReports.returnEngagedUnshelteredProspectOnGrid(), "Engaged Unsheltered Prospect");
	 	 
	 	 ViewReports.clickOnEditButton();

	 	 CreateReport.clickOnEngagedUnshelteredProspectclientCategoryDropDownOnEnrolledSection();
	 	CreateReport.clickOnEngagedUnshelteredProspectOnClientCategoryDropDown();
	 	 CreateReport.clickOnInactiveClientCategoryDropDown(); 
	 	 CreateReport.clickOnPreviewButton();
	 	 
	 	Assert.assertEquals(PreviewReport.returnInactiveOnGrid(), "Inactive");
	 	 
	 	PreviewReport.clickOnCreateReport();
	 	  try {
		 		Thread.sleep(3000);
		 	} catch (InterruptedException e) {
		 		
		 		e.printStackTrace();
		 	}
		 	  
		 	  PreviewReport.clickOnSaveReport();
		 	 
		 	  PreviewReport.clickOnConfirmButton();
	 	 
	 	 WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
		  
		 ViewReports.clickByPartialLinkText(reportName);
	 	Assert.assertEquals(ViewReports.returnInactiveOnGrid(), "Inactive");
	 	}
	  
	//H20-548 - Create a custom report and then do a current view export and verify the Client Category of Engaged Unsheltered Prospect
  @Test ()
	public void exportEngagedUnshelteredProspectToExcelCurrentViewCustomReport() throws IOException {
	  Base.clickOnReports();
	  Base.clickOnCustomReport();
	  Base.clickOnCreateReport();
	  CreateReport.clickOnNameEdit();
	  String reportName = CreateReport.enterReportName();
	  CreateReport.clickOnSaveButton();
	  CreateReport.clickOnDemographicsSection();
	  CreateReport.clickStreetSmartIDOnDemographicsSection();
	  CreateReport.clickOnEnrolledClientsSection();
	  WebDriverTasks.pageDownCommon();
	  CreateReport.clickOnclientCategoryOnEnrolledSection();
	  CreateReport.clickOnclientCategoryDropDownOnEnrolledSection();
	  CreateReport.clickOnEngagedUnshelteredProspectOnClientCategoryDropDown();
	  CreateReport.clickOnPreviewButton();
	  PreviewReport.clickOnCreateReport();
	  WebDriverTasks.specificWait(3000);
	
	  PreviewReport.clickOnSaveReport();
	 
	  PreviewReport.clickOnConfirmButton();
	  
	  WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
	  
	  ViewReports.clickByPartialLinkText(reportName);
	  int fileCountBefore = Helpers.countFilesInDownloadFolder();
	  
	  Common.clickOnExportToExcel();
	  WebDriverTasks.specificWait(3000);
	  Common.clickOnExportToExcelCurrentView(); 
	  WebDriverTasks.specificWait(3000);
	  ViewReports.clickOnConfirmButton();
	  WebDriverTasks.waitForAngularPendingRequests();
	  
	  int fileCountAfter = Helpers.countFilesInDownloadFolder();
	  Assert.assertTrue(fileCountBefore +1 == fileCountAfter, "A new file was not added to the directory"); 
	  
	  String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
      String clientCategoryColumnHeading = Helpers.returnExcelCellValue(mostRecentFile, "CustomReport", 0, 1);
      String clientCategoryFirstRecord = Helpers.returnExcelCellValue(mostRecentFile, "CustomReport", 1, 1);
      
      Assert.assertEquals(clientCategoryColumnHeading, "Client Category");
      Assert.assertEquals(clientCategoryFirstRecord, "Engaged Unsheltered Prospect");
	}
  
	//H20-548 - Create a custom report and then do a All export and verify the Client Category of Engaged Unsheltered Prospect
  @Test ()
	public void exportEngagedUnshelteredProspectToExcelAllCustomReport() throws IOException {
	  Base.clickOnReports();
	  Base.clickOnCustomReport();
	  Base.clickOnCreateReport();
	  CreateReport.clickOnNameEdit();
	  String reportName = CreateReport.enterReportName();
	  CreateReport.clickOnSaveButton();
	  CreateReport.clickOnDemographicsSection();
	  CreateReport.clickStreetSmartIDOnDemographicsSection();
	  CreateReport.clickOnEnrolledClientsSection();
	  WebDriverTasks.pageDownCommon();
	  CreateReport.clickOnclientCategoryOnEnrolledSection();
	  CreateReport.clickOnclientCategoryDropDownOnEnrolledSection();
	  CreateReport.clickOnEngagedUnshelteredProspectOnClientCategoryDropDown();
	  CreateReport.clickOnPreviewButton();
	  PreviewReport.clickOnCreateReport();
	  WebDriverTasks.specificWait(3000);
	
	  PreviewReport.clickOnSaveReport();
	 
	  PreviewReport.clickOnConfirmButton();
	  
	  WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
	  
	  ViewReports.clickByPartialLinkText(reportName);
	  int fileCountBefore = Helpers.countFilesInDownloadFolder();
	  
	  Common.clickOnExportToExcel();
	  WebDriverTasks.specificWait(3000);
	  ViewReports.clickOnExportToExcelAllRecords();
	  WebDriverTasks.waitForAngularPendingRequests();
	  
	  int fileCountAfter = Helpers.countFilesInDownloadFolder();
	  Assert.assertTrue(fileCountBefore +1 == fileCountAfter, "A new file was not added to the directory"); 
	  
	  String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
      String clientCategoryColumnHeading = Helpers.returnExcelCellValue(mostRecentFile, "CustomReport", 0, 1);
      String clientCategoryFirstRecord = Helpers.returnExcelCellValue(mostRecentFile, "CustomReport", 1, 1);
      
      Assert.assertEquals(clientCategoryColumnHeading, "Client Category");
      Assert.assertEquals(clientCategoryFirstRecord, "Engaged Unsheltered Prospect");
	}
  
	//H20-548 - Create a custom report and then do a current view export and verify the Client Category of Inactive
  @Test ()
	public void exportInactiveToExcelCurrentViewCustomReport() throws IOException {
	  Base.clickOnReports();
	  Base.clickOnCustomReport();
	  Base.clickOnCreateReport();
	  CreateReport.clickOnNameEdit();
	  String reportName = CreateReport.enterReportName();
	  CreateReport.clickOnSaveButton();
	  CreateReport.clickOnDemographicsSection();
	  CreateReport.clickStreetSmartIDOnDemographicsSection();
	  CreateReport.clickOnEnrolledClientsSection();
	  WebDriverTasks.pageDownCommon();
	  CreateReport.clickOnclientCategoryOnEnrolledSection();
	  CreateReport.clickOnclientCategoryDropDownOnEnrolledSection();
	  CreateReport.clickOnInactiveClientCategoryDropDown();;
	  CreateReport.clickOnPreviewButton();
	  PreviewReport.clickOnCreateReport();
	  WebDriverTasks.specificWait(3000);
	
	  PreviewReport.clickOnSaveReport();
	 
	  PreviewReport.clickOnConfirmButton();
	  
	  WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
	  
	  ViewReports.clickByPartialLinkText(reportName);
	  int fileCountBefore = Helpers.countFilesInDownloadFolder();
	  
	  Common.clickOnExportToExcel();
	  WebDriverTasks.specificWait(3000);
	  Common.clickOnExportToExcelCurrentView(); 
	  WebDriverTasks.specificWait(3000);
	  ViewReports.clickOnConfirmButton();
	  WebDriverTasks.waitForAngularPendingRequests();
	  
	  int fileCountAfter = Helpers.countFilesInDownloadFolder();
	  Assert.assertTrue(fileCountBefore +1 == fileCountAfter, "A new file was not added to the directory"); 
	  
	  String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
      String clientCategoryColumnHeading = Helpers.returnExcelCellValue(mostRecentFile, "CustomReport", 0, 1);
      String clientCategoryFirstRecord = Helpers.returnExcelCellValue(mostRecentFile, "CustomReport", 1, 1);
      
      Assert.assertEquals(clientCategoryColumnHeading, "Client Category");
      Assert.assertEquals(clientCategoryFirstRecord, "Inactive");
	}
  
	//H20-548 - Create a custom report and then do a All export and verify the Client Category of Inactive
  @Test ()
	public void exportInactiveToExcelAllCustomReport() throws IOException {
	  Base.clickOnReports();
	  Base.clickOnCustomReport();
	  Base.clickOnCreateReport();
	  CreateReport.clickOnNameEdit();
	  String reportName = CreateReport.enterReportName();
	  CreateReport.clickOnSaveButton();
	  CreateReport.clickOnDemographicsSection();
	  CreateReport.clickStreetSmartIDOnDemographicsSection();
	  CreateReport.clickOnEnrolledClientsSection();
	  WebDriverTasks.pageDownCommon();
	  CreateReport.clickOnclientCategoryOnEnrolledSection();
	  CreateReport.clickOnclientCategoryDropDownOnEnrolledSection();
	  CreateReport.clickOnInactiveClientCategoryDropDown();;
	  CreateReport.clickOnPreviewButton();
	  PreviewReport.clickOnCreateReport();
	  WebDriverTasks.specificWait(3000);
	
	  PreviewReport.clickOnSaveReport();
	 
	  PreviewReport.clickOnConfirmButton();
	  
	  WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
	  
	  ViewReports.clickByPartialLinkText(reportName);
	  int fileCountBefore = Helpers.countFilesInDownloadFolder();
	  
	  Common.clickOnExportToExcel();
	  WebDriverTasks.specificWait(3000);
	  ViewReports.clickOnExportToExcelAllRecords(); 
	  WebDriverTasks.waitForAngularPendingRequests();
	  
	  int fileCountAfter = Helpers.countFilesInDownloadFolder();
	  Assert.assertTrue(fileCountBefore +1 == fileCountAfter, "A new file was not added to the directory"); 
	  
	  String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
      String clientCategoryColumnHeading = Helpers.returnExcelCellValue(mostRecentFile, "CustomReport", 0, 1);
      String clientCategoryFirstRecord = Helpers.returnExcelCellValue(mostRecentFile, "CustomReport", 1, 1);
      
      Assert.assertEquals(clientCategoryColumnHeading, "Client Category");
      Assert.assertEquals(clientCategoryFirstRecord, "Inactive");
	}
  
  //Create a custom report with Engaged Unsheltered Prospect as the Client Category and verify this appears on the Preview and when the report is viewed
  @Test ()
 	public void createCustomReportVerifyClientCategoryEngagedUnshelteredProspect() throws IOException {
 	  Base.clickOnReports();
 	  Base.clickOnCustomReport();
 	  Base.clickOnCreateReport();
 	  CreateReport.clickOnNameEdit();
 	  String reportName = CreateReport.enterReportName();
 	  CreateReport.clickOnSaveButton();
 	  CreateReport.clickOnDemographicsSection();
 	  CreateReport.clickStreetSmartIDOnDemographicsSection();
 	  CreateReport.clickOnEnrolledClientsSection();
 	  WebDriverTasks.pageDownCommon();
 	  CreateReport.clickOnclientCategoryOnEnrolledSection();
 	  CreateReport.clickOnclientCategoryDropDownOnEnrolledSection();
 	  CreateReport.clickOnEngagedUnshelteredProspectOnClientCategoryDropDown();
 	  CreateReport.clickOnPreviewButton();
 	 Assert.assertEquals(ViewReports.returnEngagedUnshelteredProspectOnGrid(), "Engaged Unsheltered Prospect");
 	  
 	  
 	  PreviewReport.clickOnCreateReport();
 	  try {
 		Thread.sleep(3000);
 	} catch (InterruptedException e) {
 		// TODO Auto-generated catch block
 		e.printStackTrace();
 	}
 	  PreviewReport.clickOnSaveReport();
 	 
 	  PreviewReport.clickOnConfirmButton();
 	  
 	 WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
	  
	  ViewReports.clickByPartialLinkText(reportName);
 	  
 	 Assert.assertEquals(ViewReports.returnEngagedUnshelteredProspectOnGrid(), "Engaged Unsheltered Prospect");
 	 
 	}
  
  //Create a custom report with Inactive as the Client Category and verify this appears on the Preview and when the report is viewed
  @Test ()
 	public void createCustomReportVerifyClientCategoryInactive() throws IOException {
 	  Base.clickOnReports();
 	  Base.clickOnCustomReport();
 	  Base.clickOnCreateReport();
 	  CreateReport.clickOnNameEdit();
 	  String reportName = CreateReport.enterReportName();
 	  CreateReport.clickOnSaveButton();
 	  CreateReport.clickOnDemographicsSection();
 	  CreateReport.clickStreetSmartIDOnDemographicsSection();
 	  CreateReport.clickOnEnrolledClientsSection();
 	  WebDriverTasks.pageDownCommon();
 	  CreateReport.clickOnclientCategoryOnEnrolledSection();
 	  CreateReport.clickOnclientCategoryDropDownOnEnrolledSection();
 	  CreateReport.clickOnInactiveClientCategoryDropDown();
 	  CreateReport.clickOnPreviewButton();
 	 Assert.assertEquals(ViewReports.returnInactiveOnGrid(), "Inactive");
 	  
 	  PreviewReport.clickOnCreateReport();
 	  try {
 		Thread.sleep(3000);
 	} catch (InterruptedException e) {
 		// TODO Auto-generated catch block
 		e.printStackTrace();
 	}
 	  PreviewReport.clickOnSaveReport();
 	 
 	  PreviewReport.clickOnConfirmButton();
 	  
 	 WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
	  
	  ViewReports.clickByPartialLinkText(reportName);
 	  
 	 Assert.assertEquals(ViewReports.returnInactiveOnGrid(), "Inactive");
 	 
 	}
  
  @Test ()
	public void createCustomReportVerifySearchingOfClientCategoriesOnViewGrid() throws IOException {
	  Base.clickOnReports();
	  Base.clickOnCustomReport();
	  Base.clickOnCreateReport();
	  CreateReport.clickOnNameEdit();
	  String reportName = CreateReport.enterReportName();
	  CreateReport.clickOnSaveButton();
	  CreateReport.clickOnDemographicsSection();
	  CreateReport.clickStreetSmartIDOnDemographicsSection();
	  CreateReport.clickOnEnrolledClientsSection();
	  WebDriverTasks.pageDownCommon();
	  CreateReport.clickOnclientCategoryOnEnrolledSection();
	  CreateReport.clickOnclientCategoryDropDownOnEnrolledSection();
	  CreateReport.clickOnPreviewButton();
	
	  PreviewReport.clickOnCreateReport();
	  try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  PreviewReport.clickOnSaveReport();
	 
	  PreviewReport.clickOnConfirmButton();
	  
	 WebDriverTasks.goToURL(Config.streetsmartURL + "/StreetSmart/ViewCustomReports" );
	  
	  ViewReports.clickByPartialLinkText(reportName);
	  
	  ViewReports.searchInputBox("Engaged Unsheltered Prospect");
	  ViewReports.submitInputBox();
	 Assert.assertEquals(ViewReports.returnEngagedUnshelteredProspectOnGrid(), "Engaged Unsheltered Prospect");
	 ViewReports.clearInputBox();
	 
	 ViewReports.searchInputBox("Inactive");
	  ViewReports.submitInputBox();
	 Assert.assertEquals(ViewReports.returnInactiveOnGrid(), "Inactive");
	 ViewReports.clearInputBox();
	 
	 
	 ViewReports.searchInputBox("Other Prospect");
	  ViewReports.submitInputBox();
	 Assert.assertEquals(ViewReports.returnOtherProspectOnGrid(), "Other Prospect");
	 ViewReports.clearInputBox();
	 
	}
  
  //H20-409 As a user,I should be able to see Engaged Unsheltered Prospect and Inactive drop-down options in Client Category field within Enrolled Clients Cluster
  // so that I can create reports for the Engaged Unsheltered Prospect and Inactive clients as well
  @Test()
  public static void VerifyOptionsInClientCategoryForEnrolledClients() {
      Base.clickOnReports();
      Base.waitForSideBarMenuToBexpanded();
      Base.clickOnCustomReport();
      Base.waitForSideBarMenuToBexpanded();
      Base.clickOnCreateReport();

      CreateReport.expandEnrolledClientsPane();
      CreateReport.selectClientCategoryCheckbox();
      CreateReport.showClientCategoryOptions();

      boolean isEngagedUnshelteredOptDisplayed = CreateReport.verifyEngagedUnshelteredProspectOptionIsSelectable();
      Assert.assertTrue(isEngagedUnshelteredOptDisplayed, "Engaged Unsheltered Prospect option is not displayed or not selectable for Client category");
      boolean isInactiveOptDisplayed = CreateReport.verifyInactiveOptionIsSelectable();
      Assert.assertTrue(isInactiveOptDisplayed, "Inactive option is not displayed or not selectable for Client category");
  }

  /* H20-429 As a User,I should be able to search for Facility Name and Facility Address records from the local search box on the Report grid.*/
  @Test()
  public static void VerifySearchFunctionalityInViewReport() {
      //Navigate to View Reports
      Base.clickOnReports();
      Base.waitForSideBarMenuToBexpanded();
      Base.clickOnCustomReport();
      Base.waitForSideBarMenuToBexpanded();
      Base.clickOnViewReports();

      //Search for Report
      ViewReports.searchFor("Untitled Report1234");

      //Open / Click on  Report
      ViewReports.openReport("Untitled Report1234");

      //Search and verify for 'Facility Name' column with value 'The Haven - WestHab'
      boolean isFound = ViewReports.searchAndVerifyFilteredDataForColumn("Facility Name", "The Haven - WestHab");
      Assert.assertTrue(isFound, "Other than 'The Haven - WestHab' data displayed for 'Facility Name' column");

      //Search and verify for 'Facility Address' column with value '2640 3rd Ave Bronx NY 10454'
      isFound = ViewReports.searchAndVerifyFilteredDataForColumn("Facility Address", "2640 3rd Ave Bronx NY 10454");
      Assert.assertTrue(isFound, "Other than '2640 3rd Ave Bronx NY 10454' data displayed for 'Facility Address' column");

      //Partial Search and verify for 'Facility Address' column with value 'NY 10454'
      isFound = ViewReports.searchAndVerifyFilteredDataForColumn("Facility Address", "NY 10454");
      Assert.assertTrue(isFound, "Other than 'NY 10454' data displayed for 'Facility Address' column for partial matching text search");

      //Wild card Search and verify for 'Facility Address' column with value '*NY 10'
      isFound = ViewReports.searchAndVerifyFilteredDataForColumn("Facility Address", "*NY 10");
      Assert.assertTrue(isFound, "Other than '*NY 10.*' data displayed for 'Facility Address' column for regular expression text search");

  }

  //H20-811 As a User,I should be able to excel export on Facility Name and Facility Address fields and their respective records if they are in the report.
  @Test()
  public static void VerifyReportExportFunctionalityForCurrentViewAndAllRecords() throws IOException, InterruptedException {
      //Navigate to View Reports
      Base.clickOnReports();
      Base.waitForSideBarMenuToBexpanded();
      Base.clickOnCustomReport();
      Base.waitForSideBarMenuToBexpanded();
      Base.clickOnViewReports();

      //Search for report
      ViewReports.searchFor("Untitled Reportgh");

      //Click on/Open report
      ViewReports.openReport("Untitled Reportgh");

      //This code will perform validation on Columns in Data Grid and Exported excel for all records, it will also match the record count in exported excel is matching total records as per application
      int fileCountBefore = Helpers.countFilesInDownloadFolder();
      ViewReports.exportAllRecordsToExcel();
      Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
      WebDriverTasks.waitForAngularPendingRequests();
      WebDriverTasks.wait(2);
      int fileCountAfter = Helpers.countFilesInDownloadFolder();
      Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

      //Take values of all visible columns from data grid and verify that those columns are displayed in exported excel
      boolean allColumnsDisplayedInExcel = ViewReports.verifyTableColumnsAreDisplayedInExportedExcel();
      Assert.assertTrue(allColumnsDisplayedInExcel, "All columns of data grid are not displayed in exported excel report with all records");

      //Take total record count from pagination and compare it with total records count in exported excel
      int allExpectedRecordsCount = ViewReports.getTotalRecordsCount();
      int allActualRecordsCount = ViewReports.getRecordCountInExportedExcel();
      Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of total record count");

      //This will change Records to be displayed per page to 5
      ViewReports.changeRecordsPerPageTo("5");

      //This code will perform validation on Columns in Data Grid and Exported excel for current view, it will also match the record count in exported excel is matching total records as per application
      fileCountBefore = Helpers.countFilesInDownloadFolder();
      ViewReports.exportCurrentViewToExcel();
      Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
      WebDriverTasks.waitForAngularPendingRequests();
      Thread.sleep(2000);
      fileCountAfter = Helpers.countFilesInDownloadFolder();
      Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

      //Take values of all visible columns from data grid and verify that those columns are displayed in exported excel
      allColumnsDisplayedInExcel = ViewReports.verifyTableColumnsAreDisplayedInExportedExcel();
      Assert.assertTrue(allColumnsDisplayedInExcel, "All columns of data grid are not displayed in exported excel report with current view records");

      //Take count of records displayed in current data grid and compare it with records count in exported excel
      allExpectedRecordsCount = ViewReports.getVisibleRecordsCount();
      allActualRecordsCount = ViewReports.getRecordCountInExportedExcel();
      Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of record count in current view");

  }
  
  //H20-409 As a user,I should be able to see Engaged Unsheltered Prospect and Inactive drop-down options in Client Category field within Enrolled Clients Cluster
  // so that I can create reports for the Engaged Unsheltered Prospect and Inactive clients as wellm

  /*
  @Test()
  public static void VerifyOptionsInClientCategoryForEnrolledClients(){
      Base.clickOnReports();
      Base.waitForSideBarMenuToBexpanded();
      Base.clickOnCustomReport();
      Base.waitForSideBarMenuToBexpanded();
      Base.clickOnCreateReport();

      CreateReport.expandEnrolledClientsPane();
      CreateReport.selectClientCategoryCheckbox();
      CreateReport.showClientCategoryOptions();

      boolean isEngagedUnshelteredOptDisplayed = CreateReport.verifyEngagedUnshelteredProspectOptionIsSelectable();
      Assert.assertTrue(isEngagedUnshelteredOptDisplayed,"Engaged Unsheltered Prospect option is not displayed or not selectable for Client category");
      boolean isInactiveOptDisplayed = CreateReport.verifyInactiveOptionIsSelectable();
      Assert.assertTrue(isInactiveOptDisplayed,"Inactive option is not displayed or not selectable for Client category");
  }
  */

	@Test()
	public static void VerifyRequestReport(){
		//SetTitle name, which will  be unique each time
		String requestTitle = "TestAutomation" + Helpers.getCurrentDate("ddMMyyyyhhmmss");

		//Navigate to Request Report page
		Base.clickOnReports();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnCustomReport();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnRequestAReport();

		//Create a new Request
		RequestAReport.clickSubmitRequestButton();
		RequestAReport.setTitle(requestTitle);
		RequestAReport.setDetails("Automation test details");
		RequestAReport.clickOnSaveRequestButton();

		//Verify that on saving new request a success message is displayed
		boolean isSaved = RequestAReport.verifySuccessMessageDisplayed();
		Assert.assertTrue(isSaved,"On clicking 'Save' button success message is not displayed");
		//Click Ok on the success message
		RequestAReport.confirmSuccessPopup();

		//Click on Request title and verify request title and details are displayed as expected
		RequestAReport.clickOnRequestLink(requestTitle);
		boolean isAsExpected = RequestAReport.verifySavedTitle(requestTitle);
		Assert.assertTrue(isAsExpected,"Request title is not displayed as expected");
		isAsExpected = RequestAReport.verifySavedDetail("Automation test details");
		Assert.assertTrue(isAsExpected,"Request detail is not displayed as expected");

		//Click on cancel to return to data grid
		RequestAReport.clickOnCancel();

		//Change status to In Progress and verify that it's reflected in data grid
		RequestAReport.changeStatusTo(requestTitle,"In Progress");
		boolean isStatusUpdated = RequestAReport.verifyRequestStatus(requestTitle,"In Progress");
		Assert.assertTrue(isStatusUpdated,"On updating status to 'In Progress' status is not updated in data grid for "+requestTitle);

		//Change status to Completed and verify that it's reflected in data grid
		RequestAReport.changeStatusTo(requestTitle,"Completed");
		isStatusUpdated = RequestAReport.verifyRequestStatus(requestTitle,"Completed");
		Assert.assertTrue(isStatusUpdated,"On updating status to 'Completed' status is not updated in data grid for "+requestTitle);

		//Verify that Action button is disabled
		boolean isDisabled =  RequestAReport.verifyActionButtonDisabled(requestTitle);
		Assert.assertTrue(isDisabled,"Action button is not disabled after completing the request");
	}

	@Test
	public static void VerifyCensusPage(){
		//Navigate to census report page
		Base.clickOnReports();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnOutreachCannedReports();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnCensus();

		//Verify that data gird is displayed
		boolean isDisplayed = CensusReport.verifyDataGridIsDisplayed();
		Assert.assertTrue(isDisplayed,"Data grid is not displayed on Census report page");

		//Click on expand icon on first row and verify that it's expanded
		boolean isExpanded = CensusReport.expandAndVerifyRecord();
		Assert.assertTrue(isExpanded,"First row is not expanded");

		//Click on collapse button and verify that details are collapsed
		boolean isCollapsed = CensusReport.collapseAndVerifyRecord();
		Assert.assertTrue(isCollapsed,"First row is not collapsed");

		//Hide Gender from column picker and verify that it's not displayed on data grid
		boolean isHidden = Monthly.hideColumnAndVerify("Gender");
		Assert.assertTrue(isHidden,"The column 'Gender' is not hidden");

		//Unhide Gender from column picker and verify that it's displayed on data grid
		boolean isVisible = Monthly.unhideColumnAndVerify("Gender");
		Assert.assertTrue(isVisible,"The column 'Gender' is not visible");
	}

}



  



