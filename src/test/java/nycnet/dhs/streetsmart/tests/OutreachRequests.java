package nycnet.dhs.streetsmart.tests;


import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.ServiceRequests;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.ws.Service;

import java.io.IOException;

import static nycnet.dhs.streetsmart.pages.ServiceRequests.TableType.Unassigned;
import static nycnet.dhs.streetsmart.pages.ServiceRequests.TableType.All;

public class OutreachRequests extends AutoRunTestController {

	@Test
	public static void VerifyServiceRequestAssignmentFunctionality(){
		//Set data to be used
		String providerName = "Brooklyn 1";
		String reassignToproviderName = "Brooklyn 2";

		//Navigate to 311 Service Request page
		Base.clickOnOutreachRequests();
		Base.clickOnServiceRequests();

		//Get initial unassigned records count from tile
		int initialUnassignedCount = ServiceRequests.getUnassignedRecordCount();
		//Get initial assigned records count from tile
		int initialAssignedCount = ServiceRequests.getAssignedRecordCount();
		//Get initial total records available in Unassigned data grid
		int initialGridCount = ServiceRequests.getUnassignedGridCount();

		WebDriverTasks.wait(5);

		//Click on Service request code link and store the code no.
		String serviceRequestCode = ServiceRequests.clickOnServiceRequest();
		//Verify that Incident details popup is displayed
		boolean isPopupDisplayed = ServiceRequests.verifyIncidentDetailsPopup();
		Assert.assertTrue(isPopupDisplayed,"After clicking on Service Request link, Incident details popup is not displayed");

		//Select Provider and confirm
		ServiceRequests.selectProvider(providerName);

		//Get current unassigned records count from tile
		int currentUnassignedCount = ServiceRequests.getUnassignedRecordCount();
		//Get current assigned records count from tile
		int currentAssignedCount = ServiceRequests.getAssignedRecordCount();
		//Get total records available in Unassigned data grid
		int currentGridCount = ServiceRequests.getUnassignedGridCount();

		WebDriverTasks.wait(5);

		//Assert that Unassigned count is reduced by 1 on Unassigned tile
		Assert.assertTrue( currentUnassignedCount==(initialUnassignedCount-1),"After assigning a provider, unassigned count is not reduced by 1");
		//Assert that Assigned count is in creased by 1 on Assigned tile
		Assert.assertTrue(currentAssignedCount==(initialAssignedCount+1),"After assigning a provider, assigned count is not increased by 1");
		//Assert that Unassigned grid record count is reduced by 1
		Assert.assertTrue(currentGridCount==(initialGridCount-1),"After assigning a provider, the unassigned record count in grid is not decreased by 1");

		//Expand Search All section
		ServiceRequests.expandSearchAll();
		//Search with request code
		ServiceRequests.searchAll(serviceRequestCode);

		WebDriverTasks.wait(5);

		//Click on service request code link
		ServiceRequests.clickOnServiceRequest(serviceRequestCode);

		WebDriverTasks.wait(5);

		//Verify that Incident details popup is displayed
		isPopupDisplayed = ServiceRequests.verifyIncidentDetailsPopup();
		Assert.assertTrue(isPopupDisplayed,"After clicking on Service Request link, Incident details popup is not displayed");

		WebDriverTasks.wait(5);

		//Verify that provider name is displayed on popup
		boolean isAssigned = ServiceRequests.verifyAssignedTo(providerName);
		Assert.assertTrue(isAssigned,String.format("The expected provider %s name is not displayed on popup",providerName));

		WebDriverTasks.wait(5);

		//Select new provider to verify re-assignment
		ServiceRequests.reassignToProvider(reassignToproviderName);

		WebDriverTasks.wait(5);

		//Get current unassigned records count from tile
		currentUnassignedCount = ServiceRequests.getUnassignedRecordCount();
		//Get current assigned records count from tile
		currentAssignedCount = ServiceRequests.getAssignedRecordCount();

		WebDriverTasks.wait(5);

		//Assert that Unassigned count is reduced by 1 on Unassigned tile
		Assert.assertTrue(currentUnassignedCount==(initialUnassignedCount-1),"After re-assigning to a provider, unassigned count is not reduced by 1");
		//Assert that Assigned count is in creased by 1 on Assigned tile
		Assert.assertTrue(currentAssignedCount==(initialAssignedCount+1),"After re-assigning to a provider, assigned count is not increased by 1");

		//Search with request code
		ServiceRequests.searchAll(serviceRequestCode);

		WebDriverTasks.wait(5);

		//Click on service request code link
		ServiceRequests.clickOnServiceRequest(serviceRequestCode);

		WebDriverTasks.wait(5);

		//Verify that Incident details popup is displayed
		isPopupDisplayed = ServiceRequests.verifyIncidentDetailsPopup();
		Assert.assertTrue(isPopupDisplayed,"After clicking on Service Request link, Incident details popup is not displayed");

		WebDriverTasks.wait(5);

		//Verify that provider name is displayed on popup
		isAssigned = ServiceRequests.verifyAssignedTo(reassignToproviderName);
		Assert.assertTrue(isAssigned,String.format("The expected provider %s name is not displayed on popup",reassignToproviderName));
	}

	@Test
	public static void VerifySearchFunctionalityOnServiceRequestPage(){
		//Setup required data
		String unassigned_Name = "Supv Huebner";
		String unassigned_Email = "NOELLY101@AOL.COM";
		String unassigned_Address = "245 EAST 93 STREET, MANHATTAN (NEW YORK), NY, 10128 MANHATTAN NEW YORK NY 10128";

		String assigned_Team = "Brooklyn 3";
		String assigned_Address = "111 JOHN STREET, MANHATTAN (NEW YORK), NY, 10038 MANHATTAN NEW YORK NY 10038";

		String onSite_IncidentDatTime = "09/03/2020 08:23";
		String onsite_Address = "149 READE STREET, MANHATTAN (NEW YORK), NY, 10013 MANHATTAN NEW YORK NY 10013";

		String closed_Status =  "Resolved";
		String closed_CrossStreets = "70 STREET 37 AVENUE";

		String all_Name = "CLARE MALONE";
		String all_Email = "PETESLEY@GMAIL.COM";

		//Navigate to 311 Service Request
		Base.clickOnOutreachRequests();
		Base.clickOnServiceRequests();

		//Search for 'Customer Name' column in Unassigned table and verify that only matching data is displayed
		ServiceRequests.searchServiceRequest(unassigned_Name);
		WebDriverTasks.wait(3);
		boolean isAsExpected = ServiceRequests.verifySearchResult(Unassigned,"Customer Name",unassigned_Name);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in Unassigned table for '%s' column is not matching with searched data '%s'","Customer Name",unassigned_Name));

		WebDriverTasks.wait(5);

		//Search for 'Customer Email' column in Unassigned table and verify that only matching data is displayed
		ServiceRequests.searchServiceRequest(unassigned_Email);
		WebDriverTasks.wait(3);
		isAsExpected = ServiceRequests.verifySearchResult(Unassigned,"Customer Email",unassigned_Email);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in Unassigned table for '%s' column is not matching with searched data '%s'","Customer Email",unassigned_Email));

		WebDriverTasks.wait(5);

		//Search for 'Address' column in Unassigned table and verify that only matching data is displayed
		ServiceRequests.searchServiceRequest(unassigned_Address);
		WebDriverTasks.wait(3);
		isAsExpected = ServiceRequests.verifySearchResult(Unassigned,"Address",unassigned_Address);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in Unassigned table for '%s' column is not matching with searched data '%s'","Address",unassigned_Address));

		WebDriverTasks.wait(3);

		//Click on Details link in Assigned tile
		ServiceRequests.clickAssignedDetailsLink();
		WebDriverTasks.wait(5);

		//Search for 'Assigned Team' column in Assigned table and verify that only matching data is displayed
		ServiceRequests.searchServiceRequest(assigned_Team);
		WebDriverTasks.wait(3);
		isAsExpected = ServiceRequests.verifySearchResult(Unassigned,"Assigned Team",assigned_Team);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in 'Assigned' data grid for '%s' column is not matching with searched data '%s'","Assigned Team",assigned_Team));

		WebDriverTasks.wait(5);

		//Search for 'Address' column in Assigned table and verify that only matching data is displayed
		ServiceRequests.searchServiceRequest(assigned_Address);
		WebDriverTasks.wait(3);
		isAsExpected = ServiceRequests.verifySearchResult(Unassigned,"Address",assigned_Address);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in 'Assigned' data grid for '%s' column is not matching with searched data '%s'","Address",assigned_Address));

		WebDriverTasks.wait(3);

		//Click on Details link in On Site tile
		ServiceRequests.clickOnsiteDetailsLink();
		WebDriverTasks.wait(5);

		//Search for 'Date and Time of Incident' column in On Site table and verify that only matching data is displayed
		ServiceRequests.searchServiceRequest(onSite_IncidentDatTime);
		WebDriverTasks.wait(3);
		isAsExpected = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident",onSite_IncidentDatTime);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in 'On Site' data grid for '%s' column is not matching with searched data '%s'","Date and Time of Incident",onSite_IncidentDatTime));

		WebDriverTasks.wait(5);

		//Search for 'Address' column in On Site table and verify that only matching data is displayed
		ServiceRequests.searchServiceRequest(onsite_Address);
		WebDriverTasks.wait(3);
		isAsExpected = ServiceRequests.verifySearchResult(Unassigned,"Address",onsite_Address);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in 'On Site' data grid for '%s' column is not matching with searched data '%s'","Address",onsite_Address));

		WebDriverTasks.wait(3);

		//Click on Details link in On Site tile
		ServiceRequests.clickClosedDetailsLink();
		WebDriverTasks.wait(5);

		//Search for 'Status' column in Closed/Resolved table and verify that only matching data is displayed
		ServiceRequests.searchServiceRequest(closed_Status);
		WebDriverTasks.wait(5);
		isAsExpected = ServiceRequests.verifySearchResult(Unassigned,"Status",closed_Status);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in 'Closed/Resolved' data grid for '%s' column is not matching with searched data '%s'","Status",closed_Status));

		WebDriverTasks.wait(5);

		//Search for 'Cross Streets' column in Closed/Resolved table and verify that only matching data is displayed
		ServiceRequests.searchServiceRequest(closed_CrossStreets);
		WebDriverTasks.wait(3);
		isAsExpected = ServiceRequests.verifySearchResult(Unassigned,"Cross Streets",closed_CrossStreets);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in 'Closed/Resolved' data grid for '%s' column is not matching with searched data '%s'","Cross Streets",closed_CrossStreets));

		WebDriverTasks.wait(5);

		//Expand Search all section
		ServiceRequests.expandSearchAll();

		//Search for 'Customer Name' column in Search All table and verify that only matching data is displayed
		ServiceRequests.searchAll(all_Name);
		WebDriverTasks.wait(3);
		isAsExpected = ServiceRequests.verifySearchResult(All,"Customer Name",all_Name);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in Search all table for '%s' column is not matching with searched data '%s'","Customer Name",all_Name));

		WebDriverTasks.wait(5);

		//Search for 'Customer Email' column in Search All table and verify that only matching data is displayed
		ServiceRequests.searchAll(all_Email);
		WebDriverTasks.wait(3);
		isAsExpected = ServiceRequests.verifySearchResult(All,"Customer Email",all_Email);
		Assert.assertTrue(isAsExpected,String.format("Data found after searching in Search all table for '%s' column is not matching with searched data '%s'","Customer Email",all_Email));

		WebDriverTasks.wait(5);
	}

	@Test
	public static void VerifyDailyMonthlyYearlyFilters(){
		//Navigate to 311 Service Request
		Base.clickOnOutreachRequests();
		Base.clickOnServiceRequests();

		//Setting up data
		String filter_daily = "09/01/2020";
		String filter_yearly = "2020";
		String filter_monthly = "Sep.";

		//Filter by daily and verify that records displayed only for given date
		ServiceRequests.filterByDaily(filter_daily);
		boolean isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident",filter_daily);
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for daily filter");
		//Filter by monthly and verify that records displayed only for given Month
		ServiceRequests.filterByMonthly(filter_yearly,filter_monthly);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident","09/");
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for Monthly filter");
		//Filter by yearly and verify that records displayed only for given year
		ServiceRequests.filterByYearly(filter_yearly);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident",filter_yearly);
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for Yearly filter");

		//Click on Detail link on Assigned tile
		ServiceRequests.clickAssignedDetailsLink();

		//Filter by daily and verify that records displayed only for given date
		ServiceRequests.filterByDaily(filter_daily);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident",filter_daily);
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for daily filter");
		//Filter by monthly and verify that records displayed only for given Month
		ServiceRequests.filterByMonthly(filter_yearly,filter_monthly);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident","09/");
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for Monthly filter");
		//Filter by yearly and verify that records displayed only for given year
		ServiceRequests.filterByYearly(filter_yearly);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident",filter_yearly);
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for Yearly filter");

		//Click on Detail link on OnSite tile
		ServiceRequests.clickOnsiteDetailsLink();

		//Updating the date to match available records
		filter_daily = "09/03/2020";

		//Filter by daily and verify that records displayed only for given date
		ServiceRequests.filterByDaily(filter_daily);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident",filter_daily);
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for daily filter");
		//Filter by monthly and verify that records displayed only for given Month
		ServiceRequests.filterByMonthly(filter_yearly,filter_monthly);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident","09/");
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for Monthly filter");
		//Filter by yearly and verify that records displayed only for given year
		ServiceRequests.filterByYearly(filter_yearly);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident",filter_yearly);
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for Yearly filter");

		//Click on Detail link on Closed/Resolved tile
		ServiceRequests.clickClosedDetailsLink();

		//Filter by daily and verify that records displayed only for given date
		ServiceRequests.filterByDaily(filter_daily);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident",filter_daily);
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for daily filter");
		//Filter by monthly and verify that records displayed only for given Month
		ServiceRequests.filterByMonthly(filter_yearly,filter_monthly);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident","09/");
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for Monthly filter");
		//Filter by yearly and verify that records displayed only for given year
		ServiceRequests.filterByYearly(filter_yearly);
		isDisplayed = ServiceRequests.verifySearchResult(Unassigned,"Date and Time of Incident",filter_yearly);
		Assert.assertTrue(isDisplayed,"Data is not filtered as expected for Yearly filter");
	}

	@Test
	public static void VerifyDownloadFunctionality() throws IOException {
		//Navigate to 311 Service Request
		Base.clickOnOutreachRequests();
		Base.clickOnServiceRequests();

		//Verify that record count in exported excel is matching with expected records count for All Record, Current View and Selected Record
		ServiceRequests.downloadAndVerifyRecordCount();

		//Click on Detail link on Assigned tile
		ServiceRequests.clickAssignedDetailsLink();
		//Verify that record count in exported excel is matching with expected records count for All Record, Current View and Selected Record
		ServiceRequests.downloadAndVerifyRecordCount();

		//Click on Detail link on OnSite tile
		ServiceRequests.clickOnsiteDetailsLink();
		//Verify that record count in exported excel is matching with expected records count for All Record, Current View and Selected Record
		ServiceRequests.downloadAndVerifyRecordCount();

		/*	NOTE : In Staging environment there were too many records for Closed/Resolved tile,
		 	which is taking around 1 minute to download 'All Records'
			so commenting below code
		*/
		/*
		//Click on Detail link on Closed/Resolved tile
		ServiceRequests.clickClosedDetailsLink();
		//Verify that record count in exported excel is matching with expected records count for All Record, Current View and Selected Record
		ServiceRequests.downloadAndVerifyRecordCount();
		*/
	}
}



  


