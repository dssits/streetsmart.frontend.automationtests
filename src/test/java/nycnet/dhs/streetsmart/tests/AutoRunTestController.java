package nycnet.dhs.streetsmart.tests;

import java.io.IOException;
import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;

import nycnet.dhs.streetsmart.core.Config;
import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.AddEngagement;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.Census;
import nycnet.dhs.streetsmart.pages.ClientAddNewClient;
import nycnet.dhs.streetsmart.pages.ClientInformation;
import nycnet.dhs.streetsmart.pages.ClientList;
import nycnet.dhs.streetsmart.pages.ClientSearch;
import nycnet.dhs.streetsmart.pages.Common;
import nycnet.dhs.streetsmart.pages.DailyDropIn;
import nycnet.dhs.streetsmart.pages.DashboardLanding;
import nycnet.dhs.streetsmart.pages.EncampmentsList;
import nycnet.dhs.streetsmart.pages.GeneralCanvassing;
import nycnet.dhs.streetsmart.pages.IntensiveCanvassing;
import nycnet.dhs.streetsmart.pages.JointOperations;

import nycnet.dhs.streetsmart.pages.GlobalSearch;

import nycnet.dhs.streetsmart.pages.MonthlyDashboard;
import nycnet.dhs.streetsmart.pages.MyCaseload;
import nycnet.dhs.streetsmart.pages.ObservationsUnsuccessfulAttempts;

import nycnet.dhs.streetsmart.pages.Panhandling;
import nycnet.dhs.streetsmart.pages.Placement;
import nycnet.dhs.streetsmart.pages.SignIn;
import nycnet.dhs.streetsmart.pages.StabilizationBed;
import nycnet.dhs.streetsmart.pages.PopulationTrends;
import nycnet.dhs.streetsmart.pages.ReleaseHistory;
import nycnet.dhs.streetsmart.pages.RequestsApprovals;
import nycnet.dhs.streetsmart.pages.ServiceRequests;
import nycnet.dhs.streetsmart.pages.SignIn;
import nycnet.dhs.streetsmart.pages.UserPermissions;
import nycnet.dhs.streetsmart.pages.Webinars;
import nycnet.dhs.streetsmart.pages.clientdetails.CaseInformationTab;
import nycnet.dhs.streetsmart.pages.clientdetails.EngagementsTab;
import nycnet.dhs.streetsmart.pages.dashboard.DailyOutreach;
import nycnet.dhs.streetsmart.pages.engagements.AllEngagements;
import nycnet.dhs.streetsmart.pages.engagements.ClientAndEngagementSearch;
import nycnet.dhs.streetsmart.pages.engagements.MyEngagements;
import nycnet.dhs.streetsmart.pages.reports.CreateReport;
import nycnet.dhs.streetsmart.pages.reports.RequestAReport;
import nycnet.dhs.streetsmart.pages.reports.ViewReports;

public class AutoRunTestController {

	
@BeforeMethod
  public void setUp() throws MalformedURLException {  
	  WebDriverTasks.startWebDriver();
	  //WebDriverTasks.setWindowSize(1366, 768);
		WebDriverTasks.maximizeWindow();
		WebDriverTasks.goToURL(Config.streetsmartURL);
		
  }
  
  @AfterMethod
  public void closeWebDriver() throws MalformedURLException {  
	  WebDriverTasks.quitWebDriver();
  }
   
  
}