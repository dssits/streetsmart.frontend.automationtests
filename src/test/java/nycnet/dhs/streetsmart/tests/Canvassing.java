package nycnet.dhs.streetsmart.tests;

import nycnet.dhs.streetsmart.pages.*;
import nycnet.dhs.streetsmart.pages.admin.UserPermissions;
import nycnet.dhs.streetsmart.pages.dashboard.DailyOutreach;
import nycnet.dhs.streetsmart.pages.dashboard.Monthly;
import nycnet.dhs.streetsmart.pages.reports.CensusReport;
import org.testng.Assert;
import org.testng.annotations.Test;
import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.CanvassingCommon;
import nycnet.dhs.streetsmart.pages.Common;
import nycnet.dhs.streetsmart.pages.GeneralCanvassing;
import nycnet.dhs.streetsmart.pages.JointOperations;
import nycnet.dhs.streetsmart.pages.ObservationsUnsuccessfulAttempts;
import nycnet.dhs.streetsmart.pages.Panhandling;
import nycnet.dhs.streetsmart.pages.RequestsApprovals;
import nycnet.dhs.streetsmart.pages.common.AdvancedFilterCommon;
import nycnet.dhs.streetsmart.pages.common.ColumnPickerCommon;
import nycnet.dhs.streetsmart.pages.common.GridCommon;
import nycnet.dhs.streetsmart.pages.common.searchCommon;

public class Canvassing extends AutoRunTestController {

	//Verify General Canvassing able to expand and view details, search, advanced filter, column picker
	  @Test ()
		public void verifyGeneralCanvassingGrid() {
		  Base.clickOnCanvassing();
		  Base.waitForSideBarMenuToBexpanded();
		  Base.ClickOnGeneralCanvassing();
		  
			  Common.clickOnDateFilterYearlyButton();
			  GeneralCanvassing.clickOnDateFilterYearlyMenu();
			  Common.selectYearonYearlyFilter("2019");
			  CanvassingCommon.expandFirstCanvassingRecord();
			  CanvassingCommon.verifyFieldsOnExpandedCanvassingItems("User Name:");
			  
			  CanvassingCommon.verifyFieldsOnExpandedCanvassingItems("User ID:");
			  CanvassingCommon.verifyFieldsOnExpandedCanvassingItems("User Email:");
			  CanvassingCommon.verifyFieldsOnExpandedCanvassingItems("Latitude:");
			  CanvassingCommon.verifyFieldsOnExpandedCanvassingItems("Longitude:");
			  CanvassingCommon.verifyFieldsOnExpandedCanvassingItems("Location:");
			  CanvassingCommon.verifyFieldsOnExpandedCanvassingItems("Route Name:");
			  CanvassingCommon.verifyFieldsOnExpandedCanvassingItems("Submitted Date:");
			  CanvassingCommon.expandFirstCanvassingRecord();
			  
			  
			//Verify Age column can be enabled and disabled
		        boolean isAgeColumnPresentOnGrid = GridCommon.isColumnPresentOnGrid("Age");
		        Assert.assertTrue(isAgeColumnPresentOnGrid, "The column is not present on the Grid");
		        ColumnPickerCommon.clickOnColumnPicker();
		        ColumnPickerCommon.deselectColumnOnColumnPicker("Age");
		        ColumnPickerCommon.clickOnColumnPicker();
		        isAgeColumnPresentOnGrid =  GridCommon.isColumnPresentOnGrid("Age");
		        Assert.assertFalse(isAgeColumnPresentOnGrid, "The column is present on the Grid");
		        ColumnPickerCommon.clickOnColumnPicker();
		        ColumnPickerCommon.selectColumnOnColumnPicker("Age");
		        ColumnPickerCommon.clickOnColumnPicker();
		        isAgeColumnPresentOnGrid = GridCommon.isColumnPresentOnGrid("Age");
		        Assert.assertTrue(isAgeColumnPresentOnGrid, "The column is not present on the Grid");
		        
		        String gender = GridCommon.returnFirstRecordOnSecondColumn();
		        String age = GridCommon.returnFirstRecordOnFirstColumn();
		       
		      //Search for age
		        searchCommon.inputTextInSearchInputBox(age);  
		        searchCommon.submitSearchInSearchInputBox();
		        String ageAfterSearch = GridCommon.returnFirstRecordOnFirstColumn();
		        Assert.assertEquals(ageAfterSearch, age);
		        GridCommon.verifyValueOFAllRecordsOnFirstColumn(age);
		        searchCommon.clearSearchInSearchInputBox();
		        searchCommon.submitSearchInSearchInputBox();
		        
		        //Apply advanced filter
		        AdvancedFilterCommon.clickOnAdvancedFilter();
		        AdvancedFilterCommon.clickOnChooseFiter();
		        AdvancedFilterCommon.chooseColumnFilterOnAdvancedFilter("Gender");
		       
		        AdvancedFilterCommon.enterFilterValueOnSelectedFilterOnAdvancedFilter("Gender", gender);
		        AdvancedFilterCommon.clickOnApplyFilter();
		       
		        String genderAfterAdvancedFilter =  GridCommon.returnFirstRecordOnSecondColumn();
		        Assert.assertEquals(genderAfterAdvancedFilter, gender);
		        GridCommon.verifyValueOFAllRecordsOnSecondColumn(gender);
		        
		        AdvancedFilterCommon.clickOnAdvancedFilter();
		        AdvancedFilterCommon.clickOnResetFilterButton();
		        AdvancedFilterCommon.clickOnCloseButton();
		}
	  
	//Verify pandhandling able to expand and view details, search, advanced filter, column picker
	  @Test ()
		public void verifyPanhandlingGrid() {
		  Base.clickOnCanvassing();
		  Base.waitForSideBarMenuToBexpanded();
		  Base.clickOnPanhandling();;
		  
			  Common.clickOnDateFilterYearlyButton();
			  Panhandling.clickOnDateFilterYearlyMenu();
			  Common.selectYearonYearlyFilter("2017");
			  CanvassingCommon.expandFirstCanvassingRecord();
			  CanvassingCommon.verifyFieldsOnExpandedCanvassingItems("Longitude:");
			  CanvassingCommon.expandFirstCanvassingRecord();
			  
			//Verify Age column can be enabled and disabled
		        boolean isAgeColumnPresentOnGrid = GridCommon.isColumnPresentOnGrid("Age");
		        Assert.assertTrue(isAgeColumnPresentOnGrid, "The column is not present on the Grid");
		        ColumnPickerCommon.clickOnColumnPicker();
		        ColumnPickerCommon.deselectColumnOnColumnPicker("Age");
		        ColumnPickerCommon.clickOnColumnPicker();
		        isAgeColumnPresentOnGrid =  GridCommon.isColumnPresentOnGrid("Age");
		        Assert.assertFalse(isAgeColumnPresentOnGrid, "The column is present on the Grid");
		        ColumnPickerCommon.clickOnColumnPicker();
		        ColumnPickerCommon.selectColumnOnColumnPicker("Age");
		        ColumnPickerCommon.clickOnColumnPicker();
		        isAgeColumnPresentOnGrid = GridCommon.isColumnPresentOnGrid("Age");
		        Assert.assertTrue(isAgeColumnPresentOnGrid, "The column is not present on the Grid");
		        
		        String age = GridCommon.returnFirstRecordOnFirstColumn();
		       
		      //Search for age
		        searchCommon.inputTextInSearchInputBox(age);  
		        searchCommon.submitSearchInSearchInputBox();
		        String ageAfterSearch = GridCommon.returnFirstRecordOnFirstColumn();
		        Assert.assertEquals(ageAfterSearch, age);
		        GridCommon.verifyValueOFAllRecordsOnFirstColumn(age);
		        searchCommon.clearSearchInSearchInputBox();
		        searchCommon.submitSearchInSearchInputBox();
		        
		        //Apply advanced filter
		        AdvancedFilterCommon.clickOnAdvancedFilter();
		        AdvancedFilterCommon.clickOnChooseFiter();
		        AdvancedFilterCommon.chooseColumnFilterOnAdvancedFilter("Age");
		       
		        AdvancedFilterCommon.enterFilterValueOnSelectedFilterOnAdvancedFilter("Age", age);
		        AdvancedFilterCommon.clickOnApplyFilter();
		       
		        String ageAfterAdvancedFilter =  GridCommon.returnFirstRecordOnFirstColumn();
		        Assert.assertEquals(ageAfterAdvancedFilter, age);
		        GridCommon.verifyValueOFAllRecordsOnFirstColumn(age);
		        
		        AdvancedFilterCommon.clickOnAdvancedFilter();
		        AdvancedFilterCommon.clickOnResetFilterButton();
		        AdvancedFilterCommon.clickOnCloseButton();
		}
	  
	//Verify joint operations able to expand and view details, search, advanced filter, column picker
	  @Test ()
		public void verifyJointOperationsGrid() {
		  Base.clickOnCanvassing();
		  Base.waitForSideBarMenuToBexpanded();
		  Base.clickOnJointOperations();
		  
			  Common.clickOnDateFilterYearlyButton();
			  JointOperations.clickOnDateFilterYearlyMenu();
			  Common.selectYearonYearlyFilter("2017");
			  CanvassingCommon.expandFirstCanvassingRecord();
			  CanvassingCommon.verifyFieldsOnExpandedCanvassingItems("Longitude:");
			  CanvassingCommon.expandFirstCanvassingRecord();
			  
			//Verify Age column can be enabled and disabled
		        boolean isAgeColumnPresentOnGrid = GridCommon.isColumnPresentOnGrid("Age");
		        Assert.assertTrue(isAgeColumnPresentOnGrid, "The column is not present on the Grid");
		        ColumnPickerCommon.clickOnColumnPicker();
		        ColumnPickerCommon.deselectColumnOnColumnPicker("Age");
		        ColumnPickerCommon.clickOnColumnPicker();
		        isAgeColumnPresentOnGrid =  GridCommon.isColumnPresentOnGrid("Age");
		        Assert.assertFalse(isAgeColumnPresentOnGrid, "The column is present on the Grid");
		        ColumnPickerCommon.clickOnColumnPicker();
		        ColumnPickerCommon.selectColumnOnColumnPicker("Age");
		        ColumnPickerCommon.clickOnColumnPicker();
		        isAgeColumnPresentOnGrid = GridCommon.isColumnPresentOnGrid("Age");
		        Assert.assertTrue(isAgeColumnPresentOnGrid, "The column is not present on the Grid");
		        
		        String age = GridCommon.returnFirstRecordOnFirstColumn();
		       
		      //Search for age
		        searchCommon.inputTextInSearchInputBox(age);  
		        searchCommon.submitSearchInSearchInputBox();
		        String ageAfterSearch = GridCommon.returnFirstRecordOnFirstColumn();
		        Assert.assertEquals(ageAfterSearch, age);
		        GridCommon.verifyValueOFAllRecordsOnFirstColumn(age);
		        searchCommon.clearSearchInSearchInputBox();
		        searchCommon.submitSearchInSearchInputBox();
		        
		        //Apply advanced filter
		        AdvancedFilterCommon.clickOnAdvancedFilter();
		        AdvancedFilterCommon.clickOnChooseFiter();
		        AdvancedFilterCommon.chooseColumnFilterOnAdvancedFilter("Age");
		       
		        AdvancedFilterCommon.enterFilterValueOnSelectedFilterOnAdvancedFilter("Age", age);
		        AdvancedFilterCommon.clickOnApplyFilter();
		       
		        String ageAfterAdvancedFilter =  GridCommon.returnFirstRecordOnFirstColumn();
		        Assert.assertEquals(ageAfterAdvancedFilter, age);
		        GridCommon.verifyValueOFAllRecordsOnFirstColumn(age);
		        
		        AdvancedFilterCommon.clickOnAdvancedFilter();
		        AdvancedFilterCommon.clickOnResetFilterButton();
		        AdvancedFilterCommon.clickOnCloseButton();
		}
	  
  @Test ()
	public void exportToExcelGeneralCanvassingSelected() {
	  Base.clickOnCanvassing();
	  Base.waitForSideBarMenuToBexpanded();
	  Base.ClickOnGeneralCanvassing();
	  Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/GeneralCanvassing");
	  Assert.assertEquals(GeneralCanvassing.returnGeneralCanvassingPageHeader(), "General Canvassing");
	  
		  Common.clickOnDateFilterYearlyButton();
		  GeneralCanvassing.clickOnDateFilterYearlyMenu();
		  Common.selectYearonYearlyFilter("2019");
		  Common.selectFirstRowinClientListGrid();
		  
		  Common.selectThirdRowinClientListGrid();
		  
		  Common.selectFifthRowinClientListGrid();
		  
		  int fileCountBefore = Helpers.countFilesInDownloadFolder();
		  
		  Common.clickOnExportToExcel();
		  Common.clickOnExportToExcelSelectedRecords();
		  Assert.assertEquals( Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		  WebDriverTasks.waitForAngularPendingRequests();
		  int fileCountAfter = Helpers.countFilesInDownloadFolder();
		  Assert.assertTrue(fileCountBefore +1 == fileCountAfter, "A new file was not added to the directory"); 
	}
  
  @Test ()
	public void exportToExcelGeneralCanvassingFiltered() {
	  Base.clickOnCanvassing();
	  Base.waitForSideBarMenuToBexpanded();
	  Base.ClickOnGeneralCanvassing();
	  Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/GeneralCanvassing");
	  Assert.assertEquals(GeneralCanvassing.returnGeneralCanvassingPageHeader(), "General Canvassing");
	  
		  Common.clickOnDateFilterYearlyButton();
		  GeneralCanvassing.clickOnDateFilterYearlyMenu();
		  Common.selectYearonYearlyFilter("2019");
		 searchCommon.inputTextInSearchInputBox("Manhattan");
		 searchCommon.submitSearchInSearchInputBox();
		  int fileCountBefore = Helpers.countFilesInDownloadFolder();
		  
		  Common.clickOnExportToExcel();
		  Common.clickOnExportToExcelFilteredRecords();
		  Assert.assertEquals( Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		  WebDriverTasks.waitForAngularPendingRequests();
		  int fileCountAfter = Helpers.countFilesInDownloadFolder();
		  Assert.assertTrue(fileCountBefore +1 == fileCountAfter, "A new file was not added to the directory"); 
	}

@Test ()
	public void exportToExcelGeneralCanvassingCurrentView() {
	 Base.clickOnCanvassing();
	  Base.waitForSideBarMenuToBexpanded();
	  Base.ClickOnGeneralCanvassing();
	  Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/GeneralCanvassing");
	  Assert.assertEquals(GeneralCanvassing.returnGeneralCanvassingPageHeader(), "General Canvassing");
	  
		  Common.clickOnDateFilterYearlyButton();
		  GeneralCanvassing.clickOnDateFilterYearlyMenu();
		  Common.selectYearonYearlyFilter("2019");
		 
		  int fileCountBefore = Helpers.countFilesInDownloadFolder();
		  
		  Common.clickOnExportToExcel();
		  Common.clickOnExportToExcelCurrentView();
		  Common.clickOnOKConfirmationButton();
		  Assert.assertEquals( Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		  WebDriverTasks.waitForAngularPendingRequests();
		  int fileCountAfter = Helpers.countFilesInDownloadFolder();
		  Assert.assertTrue(fileCountBefore +1 == fileCountAfter, "A new file was not added to the directory"); 
	}

@Test ()
public void exportToExcelGeneralCanvassingAll() {
 Base.clickOnCanvassing();
  Base.waitForSideBarMenuToBexpanded();
  Base.ClickOnGeneralCanvassing();
  Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/GeneralCanvassing");
  Assert.assertEquals(GeneralCanvassing.returnGeneralCanvassingPageHeader(), "General Canvassing");
  
	  Common.clickOnDateFilterYearlyButton();
	  GeneralCanvassing.clickOnDateFilterYearlyMenu();
	  Common.selectYearonYearlyFilter("2019");
	 
	  int fileCountBefore = Helpers.countFilesInDownloadFolder();
	  
	  Common.clickOnExportToExcel();
	  Common.clickOnExportToExcelAllRecords();
	  Assert.assertEquals( Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
	  WebDriverTasks.waitForAngularPendingRequests();
	  int fileCountAfter = Helpers.countFilesInDownloadFolder();
	  Assert.assertTrue(fileCountBefore +1 == fileCountAfter, "A new file was not added to the directory"); 
}


  

    @Test
    public static void VerifyIntensiveCanvasingPage(){
        //Navigate to Intensive Canvasing page
        Base.clickOnCanvassing();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnIntensiveCanvassing();

        //Filter by year 2019 to make sure data is available
        ServiceRequests.filterByYearly("2019");

        //Verify that data grid is displayed
        boolean isDisplayed = CensusReport.verifyDataGridIsDisplayed();
        Assert.assertTrue(isDisplayed,"Data grid is not displayed on Intensive Canvasing page");

        //Expand record for first row and verify that it's expanded
        boolean isExpanded = IntensiveCanvassing.expandAndVerifyRecord();
        Assert.assertTrue(isExpanded,"First row is not expanded");

        //Collapse record for first row and verify that it's collapsed
        boolean isCollapsed = IntensiveCanvassing.collapseAndVerifyRecord();
        Assert.assertTrue(isCollapsed,"First row is not collapsed");

        //Search for 'Where are you staying' column and verify that only respected data is displayed
        DailyOutreach.searchFor("Street - 106 Lafayette");
        boolean verifySearch = MyCaseload.verifyDataInColumn("Where are you staying","Street - 106 Lafayette");
        Assert.assertTrue(verifySearch,"Searched data is not displayed in grid for column 'Where are you staying'");

        //Clear the search results
        DailyOutreach.searchFor("");

        //Apply advanced filter for Age column and verify that the respected data is displayed
        UserPermissions.advancedFilterBy("Age","20 - 30's","Age",true);
        boolean verifyAdvancedFilter = MyCaseload.verifyDataInColumn("Age","20 - 30's");
        Assert.assertTrue(verifyAdvancedFilter,"Advanced filtered data is not displayed in grid for column 'Age'");

        //Hide gender column and verify that it's hidden
        boolean isHidden = Monthly.hideColumnAndVerify("Gender");
        Assert.assertTrue(isHidden,"The column 'Gender' is not hidden");

        //Unhide Gender column and verify that it's displayed on data-grid
        boolean isVisible = Monthly.unhideColumnAndVerify("Gender");
        Assert.assertTrue(isVisible,"The column 'Gender' is not visible");
    }
}