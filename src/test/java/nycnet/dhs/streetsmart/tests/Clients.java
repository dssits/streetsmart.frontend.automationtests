package nycnet.dhs.streetsmart.tests;

//import com.sun.deploy.cache.BaseLocalApplicationProperties;
import net.bytebuddy.utility.RandomString;
import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.*;
import nycnet.dhs.streetsmart.pages.clientdetails.*;
import nycnet.dhs.streetsmart.pages.engagements.ClientAndEngagementSearch;
import nycnet.dhs.streetsmart.pages.reports.ViewReports;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import nycnet.dhs.streetsmart.pages.dashboard.DailyOutreach;

import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;


public class Clients extends AutoRunTestController {


    @Test()
    public void aboutStreetSmartTest() throws MalformedURLException {
        Base.clickOnAboutStreetMenuItem();
        WebDriverTasks.switchToNewWindow();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/AboutStreetSmart");
    }
    
    //Add new enrolled client and test ability to add, edit and delete client contacts
    @Test()
    public void addNewEnrolledClientAndTestClientContacts() {
       //Add a new enrolled client
    	ActionSets.createNewEnrolledClient();
    	
    	//Add new client contact
    	 ClientContactsTab.clickOnClientContactsTab();
         ClientContactsTab.clickOnAddNewButton();
         ClientContactsTab.selectContactType("Client Contact");
         ClientContactsTab.enterEmailAddress("test@test.com");
         ClientContactsTab.enterPhoneNumber("1234567890");
         ClientContactsTab.clickOnSaveButton();

         String contactTypeText = ClientContactsTab.verifyContactType();
         Assert.assertEquals(contactTypeText, "Client Contact");
         
         //Edit client contact
         ClientContactsTab.clickOnActionButton();
         ClientContactsTab.clickOnViewDetailsButton();
         ClientContactsTab.clickOnEditButton();
         ClientContactsTab.clearEmailAddress();
         ClientContactsTab.enterEmailAddress("testedit@testedit.com");
         ClientContactsTab.clickOnSaveButton();
         String clientEmailAddressOnGrid = ClientContactsTab.returnEmailAddressOnContactGrid();
         Assert.assertEquals(clientEmailAddressOnGrid, "testedit@testedit.com");
         ClientContactsTab.clickOnActionButton();
         ClientContactsTab.clickOnDeleteButton();
         
         WebDriverTasks.specificWait(3000);
         ClientContactsTab.clickOnConfirmButton();
         WebDriverTasks.specificWait(3000);
         ClientContactsTab.clickOnConfirmButton();
         String noRecordsOnGrid = ClientContactsTab.returnNoRecordsOnGrid();
         Assert.assertEquals(noRecordsOnGrid, "No Record Found");
        	
         
         //Add detox contact
         ClientContactsTab.selectContactType("Detox Contact");
         ClientContactsTab.clickOnAddNewButton();
         ClientContactsTab.selectClientContactDetoxDropDown("Amethyst House CR");
         
         ClientContactsTab.clickOnSaveButton();
         contactTypeText = ClientContactsTab.verifyContactType();
         Assert.assertEquals(contactTypeText, "Detox Contact- Amethyst House CR");
         
         //delete detox contact
         ClientContactsTab.clickOnActionButton();
         ClientContactsTab.clickOnDeleteButton();
         
         WebDriverTasks.specificWait(3000);
         ClientContactsTab.clickOnConfirmButton();
         WebDriverTasks.specificWait(3000);
         ClientContactsTab.clickOnConfirmButton();
        noRecordsOnGrid = ClientContactsTab.returnNoRecordsOnGrid();
         Assert.assertEquals(noRecordsOnGrid, "No Record Found");
        
         
         //Notes
         ClientContactsTab.clickOnNotesButton();
         ClientContactsTab.clickOnAddNewNote();
         ClientContactsTab.enterNotesText("Test");
         ClientContactsTab.clickSaveNote();

         String verifyNoteText = ClientContactsTab.verifyNoteText();
         Assert.assertEquals(verifyNoteText, "Test ");
         
         
    	
    }
    
    //Add new enrolled client and end the enrollment and then reopen the client
    @Test()
    public void addNewEnrolledClientAndEndEnrollmentAndReopenClient() {
       //Add a new enrolled client
    	ActionSets.createNewEnrolledClient();
    	ClientInformation.clickOnClientInformationTab();
    	String clientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
    	Assert.assertEquals(clientCategory, "Caseload");
    	//ClientInformation.scrollToEnrollmentEndDate();
    	ClientInformation.clickOnEditClientInformationButton2();
    	ClientInformation.clickOnEnrollmentEndDate();
    	Common.clickOnLinkText("1");
    	ClientInformation.clickOnOKConfirm();
    	ClientInformation.clickSaveClientInformationButton();
    	ClientInformation.clickOnCaseEndDateToOpenCalendar();
    	Common.clickOnLinkText("1");
    	ClientInformation.selectCaseEndReason("Cannot be located");
    	ClientInformation.clickOnSaveButton();
    	clientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
    	Assert.assertEquals(clientCategory, "Enrollment Ended");
    	ClientInformation.clickOnReOpenButton();
    	ClientInformation.selectProviderOnOpenEnrollMentProvider("Breaking Ground");
    	ClientInformation.selectProviderOnOpenEnrollMentArea("Brooklyn 1");
    	ClientInformation.clickOnSaveButtonReOpenEnrollMentPopup();
    	clientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
    	Assert.assertEquals(clientCategory, "Caseload");
    	
    	
    	
    }
    
    //Add new enrolled client and add assisted daily living and edit and delete
    @Test()
    public void addNewEnrolledClientTestAssistedDailyLiving() {
       //Add a new enrolled client
    	ActionSets.createNewEnrolledClient();
    	
        //Add assisted daily living
        AssistedDailyLivingInformationTab.clickOnAssistedDailyLivingInformationTab();
        AssistedDailyLivingInformationTab.clickOnAddNewButton();
        AssistedDailyLivingInformationTab.selectProvider("BG");
        AssistedDailyLivingInformationTab.clickOnRadioForGrooming();
        AssistedDailyLivingInformationTab.clickOnRadioForWalking();
        AssistedDailyLivingInformationTab.clickOnRadioForManagingMedications();
        AssistedDailyLivingInformationTab.clickOnSaveButton();
        String providerDetailTitleText = AssistedDailyLivingInformationTab.returnProviderDetails();
        Assert.assertEquals(providerDetailTitleText, "BG");

        AssistedDailyLivingInformationTab.clickClientDetailsButton();
      
        String clientDetialsTitleText = AssistedDailyLivingInformationTab.returnClientDetailsTitle();
        Assert.assertEquals(clientDetialsTitleText, "Client Details");
        
        //Edit assisted daily living
        AssistedDailyLivingInformationTab.clickOnActionButton();
        AssistedDailyLivingInformationTab.clickOnViewDetails();
        AssistedDailyLivingInformationTab.scrollToEditButton();
        AssistedDailyLivingInformationTab.clickOnEditButton();
        
        AssistedDailyLivingInformationTab.ScrollToOtherActivityInput();
        AssistedDailyLivingInformationTab.inputOtherActivity("Drawing and Painting");
        String otherActivityOnGridSavedText = AssistedDailyLivingInformationTab.returnOtherActivityInputEditAddPage();
        AssistedDailyLivingInformationTab.clickOnSaveButton();
        AssistedDailyLivingInformationTab.clickOnActionButton();
        AssistedDailyLivingInformationTab.clickOnViewDetails();
        AssistedDailyLivingInformationTab.scrollToEditButton();
        AssistedDailyLivingInformationTab.clickOnEditButton();
        AssistedDailyLivingInformationTab.ScrollToOtherActivityInput();
        String otherActivityOnGridAfterEditText = AssistedDailyLivingInformationTab.returnOtherActivityInputEditAddPage();
        Assert.assertEquals(otherActivityOnGridSavedText, otherActivityOnGridAfterEditText);
     
        AssistedDailyLivingInformationTab.clickOnSaveButton();
        
        //Add, Edit and delete notes
        AssistedDailyLivingInformationTab.selectNotesCollapseIcon();
        AssistedDailyLivingInformationTab.clickOnAddNewNotesButton();
        AssistedDailyLivingInformationTab.inputNotesText("TestNote");
        AssistedDailyLivingInformationTab.selectSaveNotes();
        String notesDetailText = AssistedDailyLivingInformationTab.returnNotesTextDetails();
        Assert.assertEquals(notesDetailText, "TestNote ");
        
        AssistedDailyLivingInformationTab.selectDeleteNoteButton();
        AssistedDailyLivingInformationTab.selectDeleteNoteButtonConfirmation();
        
        //Verify changelog
        AssistedDailyLivingInformationTab.clickOnChangeLogExpandIcon();
        AssistedDailyLivingInformationTab.scrollToWalkingTextOnChangeLogGrid();
        String walkingText = AssistedDailyLivingInformationTab.returnWalkingTextOnChangeLogGrid();
        Assert.assertEquals(walkingText, "Some Assistance Needed");
    }
    
    //Add new enrolled client, add edit, delete VA benefits and other income. Add, edit and delete notes
    @Test()
    public void addNewEnrolledClientTestIncomeInformationVABenefitsAndOtherIncome() {
    	//Add a new enrolled client
    	ActionSets.createNewEnrolledClient();
    	
        //Add, edit, delete VA benefits
        IncomeInformationTab.clickOnIncomeInformationTab();
        IncomeInformationTab.selectIncomeDataDropDownMenu("VA benefits");
        IncomeInformationTab.clickOnAddNewIncome();
        IncomeInformationTab.selectIncomeStatusDropDownMenu("Active");
        IncomeInformationTab.clickOnEffectiveDateToOpenCalendar();
        Common.clickOnLinkText("1");
        IncomeInformationTab.inputMonthlyIncome("500");
        IncomeInformationTab.clickOnSaveButton();
        IncomeInformationTab.clickOnActionButton();
        IncomeInformationTab.clickOnViewDetails();
        IncomeInformationTab.clickOnEditButton();
        IncomeInformationTab.clearMonthlyIncomeInput();
        IncomeInformationTab.inputMonthlyIncome("1000");
        String monthlyIncomeValueInputted = IncomeInformationTab.returnMonthlyIncomeInputtedEditAddPage();
        IncomeInformationTab.clickOnSaveButton();
        String monthlyIncomeValueSaved =  IncomeInformationTab.returnMonthlyIncomeFirstRecordOnGrid();
        Assert.assertEquals(monthlyIncomeValueSaved, monthlyIncomeValueInputted);
        IncomeInformationTab.clickOnActionButton();
        IncomeInformationTab.clickOnDeleteOption();
        WebDriverTasks.specificWait(3000);
        IncomeInformationTab.clickOnConfirmButton();
        IncomeInformationTab.clickOnOKConfirmButton();
        String noRecordFoundonGrid = IncomeInformationTab.returnNoRecordsFoundOnServicePlanGrid();
        Assert.assertEquals(noRecordFoundonGrid, "No Record Found");
        
        //Add, edit, delete Other income
        IncomeInformationTab.selectIncomeDataDropDownMenu("Other Income");
        IncomeInformationTab.clickOnAddNewIncome();
        IncomeInformationTab.selectIncomeTypeDropDownMenu("Job");
        IncomeInformationTab.clickOnEffectiveDateToOpenCalendar();
        Common.clickOnLinkText("1");
        IncomeInformationTab.inputMonthlyIncome("500");
        IncomeInformationTab.clickOnSaveButton();
        IncomeInformationTab.clickOnActionButton();
        IncomeInformationTab.clickOnViewDetails();
        IncomeInformationTab.clickOnEditButton();
        IncomeInformationTab.clearMonthlyIncomeInput();
        IncomeInformationTab.inputMonthlyIncome("1000");
        monthlyIncomeValueInputted = IncomeInformationTab.returnMonthlyIncomeInputtedEditAddPage();
        IncomeInformationTab.clickOnSaveButton();
        monthlyIncomeValueSaved =  IncomeInformationTab.returnMonthlyIncomeFirstRecordOnGrid();
        Assert.assertEquals(monthlyIncomeValueSaved, monthlyIncomeValueInputted);
        IncomeInformationTab.clickOnActionButton();
        IncomeInformationTab.clickOnDeleteOption();
        WebDriverTasks.specificWait(3000);
        IncomeInformationTab.clickOnConfirmButton();
        IncomeInformationTab.clickOnOKConfirmButton();
        noRecordFoundonGrid = IncomeInformationTab.returnNoRecordsFoundOnServicePlanGrid();
        Assert.assertEquals(noRecordFoundonGrid, "No Record Found");
        
        //Add, edit, delete note
        IncomeInformationTab.selectNotesCollapseIcon();
        IncomeInformationTab.clickOnAddNewNotesButton();
        IncomeInformationTab.inputNotesText("TestNote");
        IncomeInformationTab.selectSaveNotes();
        String notesDetailText = IncomeInformationTab.returnNotesTextDetails();
        Assert.assertEquals(notesDetailText, "TestNote ");
        IncomeInformationTab.selectDeleteNoteButton();
        IncomeInformationTab.selectDeleteNoteButtonConfirmation();
    }

    
    //-  Add a new enrolled client, add a service plan, edit service plan, delete service plan
    @Test()
    public void addNewEnrolledClientAndServicePlan() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + Helpers.returnRandomNumber();
        String lastName = "Automation" + random;

        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation" + random);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEnrolledButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");

        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.selectCaseProvider("Breaking Ground");
        ClientAddNewClient.selectCaseStartDate();
        Common.clickOnLinkText("1");
        ClientAddNewClient.selectCaseBorough("Brooklyn");


        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.selectConsentToCaseload("Agree");
        ClientAddNewClient.clickOnSaveButton2();

        ServicePlanTab.clickOnServicePlanTab();

        ServicePlanTab.clickOnChangeLogExpandIcon();
        Assert.assertEquals(ServicePlanTab.returnNoRecordFoundOnChangeLogGrid(), "No Record Found");

        //Add new service plan
        ServicePlanTab.clickOnAddNewButton();
        AddServicePlan.clickOnInitialPlanDateToOpenDatePicker();
        Common.clickOnLinkText("1");
        AddServicePlan.clickOnFirstReviewDueDateToOpenDatePicker();
        Common.clickOnLinkText("1");
        AddServicePlan.selectServiceProvider("BG");
        AddServicePlan.clickOnCases();
        AddServicePlan.selectAllCases();
        AddServicePlan.clickOnGoalStartDate();
        Common.clickOnLinkText("1");
        //WebDriverTasks.pageDownCommon();
        AddServicePlan.clickOnCategory();
        AddServicePlan.selectEducationOnCategoryDropDown();
        AddServicePlan.inputGoalStartDateText("Goal Text");
        AddServicePlan.clickOnStaffSignatureToOpenSignaturePopup();
        AddServicePlan.clickOnUseExistingButton();
        AddServicePlan.clickOnUseExistingSignatureButton();
        AddServicePlan.clickOnSaveButton();
        Assert.assertEquals(ServicePlanTab.returnGoalText(), "Goal Text");

        ServicePlanTab.clickOnActionButton();

        ServicePlanTab.clickOnViewDetails();

        ServicePlanTab.clickOnEditServicePlanButtonOnViewPage();

        ServicePlanTab.InputGoalTextOnEditPage("Edit Goal Text");

        ServicePlanTab.clickOnSaveServicePlanButtonOnEditPage();

        Assert.assertEquals(ServicePlanTab.returnGoalText(), "Edit Goal Text");

        // Assert.assertEquals(ServicePlanTab.returnNoRecordsFoundOnServicePlanGrid(), null);

        ServicePlanTab.clickOnActionButton();
        ServicePlanTab.clickOnDeleteOption();

        WebDriverTasks.specificWait(3000);
        ServicePlanTab.clickOnConfirmButton();

        ServicePlanTab.clickOnOKConfirmButton();

        Assert.assertEquals(ServicePlanTab.returnNoRecordsFoundOnServicePlanGrid(), "No Record Found");

        ServicePlanTab.selectNotesCollapseIcon();
        ServicePlanTab.clickOnAddNewNotesButton();
        ServicePlanTab.inputNotesText("Service Plan Notes Text");
        ServicePlanTab.selectSaveNotes();

        Assert.assertEquals(ServicePlanTab.returnFirstNotesTextOnNotesGrid(), "Service Plan Notes Text ");

        ServicePlanTab.clickOnIconToEditFirstNoteOnNotesGrid();


        ServicePlanTab.inputNotesTextOnEdit("Edit Service Plan Notes Text");
        ServicePlanTab.clickOnSaveNotesOnEdit();
        Assert.assertEquals(ServicePlanTab.returnFirstNotesTextOnNotesGrid(), "Edit Service Plan Notes Text ");

        ServicePlanTab.clickOnIconToDeleteFirstNoteOnNotesGrid();
        ServicePlanTab.confirmDeleteNoteOnNotesGrid();
        Assert.assertEquals(ServicePlanTab.returnNoRecordsFoundOnNotesGrid(), "No Record Found");

        ServicePlanTab.clickOnChangeLogExpandIcon();
        Assert.assertEquals(ServicePlanTab.returnGoalTextOnChangeLogGrid(), "Edit Goal Text");


    }

    //-  Add a new enrolled client, add a rental subsidy and edit a rental subsidy
    @Test()
    public void addNewEnrolledClientAndRentalSubsidy() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + Helpers.returnRandomNumber();
        String lastName = "Automation" + random;

        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation" + random);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEnrolledButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        ClientAddNewClient.selectConsentToCaseload("Agree");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.selectCaseProvider("Breaking Ground");
        ClientAddNewClient.selectCaseStartDate();
        Common.clickOnLinkText("1");
        ClientAddNewClient.selectCaseBorough("Brooklyn");


        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton2();

        RentalSubsidyTab.clickOnRentalSubsidyTab();
        RentalSubsidyTab.verifyActiveSubsidiesButton();


        RentalSubsidyTab.clickOnAddSubsidies();
        RentalSubsidyTab.clickOnAddVashSubsidy();
        RentalSubsidyTab.selectVashStatus("Applying");
        RentalSubsidyTab.clickOnSaveSubsidy();
        RentalSubsidyTab.clickOnViewMySubsidy();
        Assert.assertEquals(RentalSubsidyTab.returnVashSubsidyStatus(), "Applying");

        //Edit Vash Rental Subsidy
        RentalSubsidyTab.clickOnEditIconToEditVashSubsidy();
        RentalSubsidyTab.selectVashStatus("Active");
        RentalSubsidyTab.openDatePickerOnDateSubmittedOnVashSubsidy();
        Common.clickOnLinkText("1");
        String chosenDateOnVashSubsidy = RentalSubsidyTab.returnDateOnDateSubmittedOnVashSubsidy();
        RentalSubsidyTab.openDatePickerOnReceivedDateOnVashSubsidy();
        Common.clickOnLinkText("1");
        RentalSubsidyTab.openDatePickerOnCertificateDateOnVashSubsidy();
        Common.clickOnLinkText("1");
        RentalSubsidyTab.openDatePickerOnExpirationDateOnVashSubsidy();

        ZoneId z = ZoneId.of("America/New_York");
        LocalDate ld = LocalDate.now(z);
        int dayOfMonth = ld.getDayOfMonth();
        String currentDate = ld.now().toString();
        String dayOfMonthString = Integer.toString(dayOfMonth);

        Common.clickOnLinkText(dayOfMonthString);
        RentalSubsidyTab.clickOnSaveSubsidy();

        RentalSubsidyTab.rentalSubsidyRedirectNo();

        //Verify dates chosen option on view
        Assert.assertEquals(RentalSubsidyTab.returnVashSubsidyStatus(), "Active");
        Assert.assertEquals(RentalSubsidyTab.returnDateOnDateSubmittedOnVashSubsidyView(), chosenDateOnVashSubsidy);
        Assert.assertEquals(RentalSubsidyTab.returnDateOnReceivedDateOnVashSubsidyView(), chosenDateOnVashSubsidy);
        Assert.assertEquals(RentalSubsidyTab.returnDateOnCertificateDateOnVashSubsidyView(), chosenDateOnVashSubsidy);
        // Assert.assertEquals(RentalSubsidyTab.returnDateOnExpirationDateOnVashSubsidyView(), chosenDateOnVashSubsidy);

        //Add Gen Pop subsidy
        RentalSubsidyTab.clickOnAddSubsidies();
        RentalSubsidyTab.clickOnAddEditGenPopSubsidy();
        RentalSubsidyTab.AddGenPopSubsidySelectStatus("Applying");
      //  RentalSubsidyTab.clickOnSaveSubsidy();
        RentalSubsidyTab.clickOnSaveSubsidyForGenPopOnEdit();
        RentalSubsidyTab.clickOnViewMySubsidy();
        Assert.assertEquals(RentalSubsidyTab.returnGenPopStatus(), "Applying");
        
        //Edit Gen Pop subsidy
        RentalSubsidyTab.clickOnAddEditGenPopSubsidy();
        RentalSubsidyTab.AddGenPopSubsidySelectStatus("Pending");
        RentalSubsidyTab.clickOnEditGenPopDateSubmitted();
        Common.clickOnLinkText("1");
        String GenPopDateSubmittedOnEdit = RentalSubsidyTab.returnOnEditGenPopDateSubmitted();
        RentalSubsidyTab.clickOnSaveSubsidyForGenPopOnEdit();
        
        //Verify Gen Pop subsidy on view
        Assert.assertEquals(RentalSubsidyTab.returnGenPopStatus(), "Pending");
        Assert.assertEquals(RentalSubsidyTab.returnGenPopDateSubmitted(), GenPopDateSubmittedOnEdit);
       
        //notes
        RentalSubsidyTab.selectNotesCollapseIcon();
        RentalSubsidyTab.clickOnAddNewNotesButton();
        RentalSubsidyTab.inputNotesText("Rental Subsidy Notes Text");
        RentalSubsidyTab.selectSaveNotes();

        Assert.assertEquals(ServicePlanTab.returnFirstNotesTextOnNotesGrid(), "Rental Subsidy Notes Text ");

        RentalSubsidyTab.clickOnIconToEditFirstNoteOnNotesGrid();


        RentalSubsidyTab.inputNotesTextOnEdit("Edit Rental Subsidy Notes Text");
        RentalSubsidyTab.clickOnSaveNotesOnEdit();
        Assert.assertEquals(ServicePlanTab.returnFirstNotesTextOnNotesGrid(), "Edit Rental Subsidy Notes Text ");

        RentalSubsidyTab.clickOnIconToDeleteFirstNoteOnNotesGrid();
        RentalSubsidyTab.confirmDeleteNoteOnNotesGrid();
        Assert.assertEquals(ServicePlanTab.returnNoRecordsFoundOnNotesGrid(), "No Record Found");


    }


    //H20-1382 -  Add new enrolled client
    @Test()
    public void AddNewEnrolledClient() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + Helpers.returnRandomNumber();
        String lastName = "Automation" + random;

        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation" + random);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEnrolledButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");

        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.selectCaseProvider("Breaking Ground");
        ClientAddNewClient.selectCaseStartDate();
        Common.clickOnLinkText("1");
        ClientAddNewClient.selectCaseBorough("Brooklyn");


        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton2();

    }


    //H20-886 - Verify Enrollment End Date button is disabled
    @Test()

    public void verifyEnrollmentEndDateisDisabledForInactiveClient() {
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
        ClientList.inputTextinClientListSearchBox("C161254");
        ClientList.submitSearchinGlobalSearchBox();
        ClientList.clickOnStreetSmartID("C161254");
        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Inactive");
        Assert.assertEquals(ClientInformation.returnEnrollmentEndDateEnablementStatus(), "true");

    }

    @Test()
    public void AddTwoInactiveClientsAndMerge() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + Helpers.returnRandomNumber();
        String lastName = "Automation" + random;

        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation" + random);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        ClientInformation.returnStreetsmartID();
        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");
        WebDriverTasks.pageDownCommon();
        ClientInformation.clickOnEditButton();
        WebDriverTasks.pageDownCommon();

        ClientInformation.selectClientCategoryOnEditPage2("Inactive");
        ClientInformation.clickSaveClientInformationButton();
        ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Inactive");

        //Add 2nd client
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        firstName = "User" + Helpers.returnRandomNumber();

        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField(lastName);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        ClientInformation.returnStreetsmartID();
        ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");
        WebDriverTasks.pageDownCommon();
        ClientInformation.clickOnEditButton();
        WebDriverTasks.pageDownCommon();

        ClientInformation.selectClientCategoryOnEditPage2("Inactive");
        ClientInformation.clickSaveClientInformationButton();
        ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Inactive");

        //Merge two clients


        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");


        ClientSearch.inputTextinLastNameField(lastName);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.selectFirstRecord();
        ClientSearch.selectSecondRecord();
        ClientSearch.clickOnMergeButton();
        ClientSearch.chooseFirstPrimaryClient();
        String primaryClient = ClientSearch.returnfirstClientStreetSmartIDOnMerge();
        ClientSearch.saveButtonWhenChoosingPrimary();
        ClientSearch.clickOKConfirm();
        ClientSearch.closeSecondaryClient();
        ClientSearch.clickOKConfirm();
        ClientSearch.saveButton();
        ClientSearch.clickConfirmButton();
        Base.clickOnClientListMenuItem();
        ClientList.inputTextinClientListSearchBox(primaryClient);
        ClientList.submitSearchinGlobalSearchBox();
        ClientList.clickOnStreetSmartID(primaryClient);
        Assert.assertEquals(ClientInformation.returnClientCategoryChosenOptionOnViewPage(), "Inactive");

    }


    //H20-828 - Create two engaged unsheltered prospects client. Merge two Engaged Unsheltered Prospect
    //clients and verify that the client category of the Merged Client is Engaged Unsheltered Prospect

    @Test()
    public void AddTwoEngagedUnshelteredProspectClientsAndMerge() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + Helpers.returnRandomNumber();
        String lastName = "Automation" + random;

        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation" + random);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        ClientInformation.returnStreetsmartID();

        //Add 2nd client
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        firstName = "User" + Helpers.returnRandomNumber();

        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField(lastName);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        ClientInformation.returnStreetsmartID();

        //Merge two clients


        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");


        ClientSearch.inputTextinLastNameField(lastName);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.selectFirstRecord();
        ClientSearch.selectSecondRecord();
        ClientSearch.clickOnMergeButton();
        ClientSearch.chooseFirstPrimaryClient();
        String primaryClient = ClientSearch.returnfirstClientStreetSmartIDOnMerge();
        ClientSearch.saveButtonWhenChoosingPrimary();
        ClientSearch.clickOKConfirm();
        ClientSearch.closeSecondaryClient();
        ClientSearch.clickOKConfirm();
        ClientSearch.saveButton();
        ClientSearch.clickConfirmButton();
        Base.clickOnClientListMenuItem();
        ClientList.inputTextinClientListSearchBox(primaryClient);
        ClientList.submitSearchinGlobalSearchBox();
        ClientList.clickOnStreetSmartID(primaryClient);
        Assert.assertEquals(ClientInformation.returnClientCategoryChosenOptionOnViewPage(), "Engaged Unsheltered Prospect");

    }

    //H20-703 - Create two prospects. Add Engagements. Merge two Prospect clients and verify that the client category of the Merged Client is Engaged Unsheltered
    //Prospect
    @Test()
    public void AddTwoProspectClientsAndMerge() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + Helpers.returnRandomNumber();
        String lastName = "Automation" + random;

        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation" + random);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnProspectButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        ClientInformation.returnStreetsmartID();
        String chosenClientCategory = ClientInformation.returnSelectedProspectOrCaseLoadClientCategoryOnViewPage();
        Assert.assertEquals(chosenClientCategory, "Other Prospect");

        // 1st engagement
        AddEngagement.clickOnEngagementTab();

        AddEngagement.clickOnAddNewEngagementButton();
        AddEngagement.selectEngagementProvider("BG");
        AddEngagement.selectEngagementArea("Bronx");
        AddEngagement.selectEngagementShift("Evening");
        AddEngagement.selectEngagementJointOutreach("No");
        AddEngagement.selectEngagementContactReason("311 Response");
        AddEngagement.selectEngagementContactTime();
        AddEngagement.selectEngagementContactDateField();
        Common.clickOnLinkText("1");

        AddEngagement.selectEngagementOutcome("958 Certified Removal");
        AddEngagement.selectEngagementBorough("Bronx");
        AddEngagement.selectLocationType("Park");
        AddEngagement.selectParkName("Amersfort");
        AddEngagement.inputEngagementNotes("1");
        AddEngagement.selectSaveEngagement();

        AddEngagement.clickOnEngagementTab();

        // 2nd Engagement
        AddEngagement.clickOnAddNewEngagementButton();
        AddEngagement.selectEngagementProvider("BG");
        AddEngagement.selectEngagementArea("Bronx");
        AddEngagement.selectEngagementShift("Afternoon");
        AddEngagement.selectEngagementJointOutreach("No");

        AddEngagement.selectEngagementContactReason("311 Response");
        AddEngagement.selectEngagementContactTime();
        AddEngagement.selectEngagementContactDateField();
        Common.clickOnLinkText("14");

        AddEngagement.selectEngagementOutcome("958 Certified Removal");
        AddEngagement.selectEngagementBorough("Bronx");
        AddEngagement.selectLocationType("Park");
        AddEngagement.selectParkName("Astoria");
        AddEngagement.inputEngagementNotes("2");
        AddEngagement.selectSaveEngagement();


        //Add 2nd client
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        firstName = "User" + Helpers.returnRandomNumber();

        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField(lastName);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnProspectButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        ClientInformation.returnStreetsmartID();
        chosenClientCategory = ClientInformation.returnSelectedProspectOrCaseLoadClientCategoryOnViewPage();
        Assert.assertEquals(chosenClientCategory, "Other Prospect");

        // 1st engagement
        AddEngagement.clickOnEngagementTab();

        AddEngagement.clickOnAddNewEngagementButton();
        AddEngagement.selectEngagementProvider("BG");
        AddEngagement.selectEngagementArea("Bronx");
        AddEngagement.selectEngagementShift("Evening");
        AddEngagement.selectEngagementJointOutreach("No");
        AddEngagement.selectEngagementContactReason("311 Response");
        AddEngagement.selectEngagementContactTime();
        AddEngagement.selectEngagementContactDateField();
        Common.clickOnLinkText("20");

        AddEngagement.selectEngagementOutcome("958 Certified Removal");
        AddEngagement.selectEngagementBorough("Bronx");
        AddEngagement.selectLocationType("Park");
        AddEngagement.selectParkName("Amersfort");
        AddEngagement.inputEngagementNotes("1");
        AddEngagement.selectSaveEngagement();

        //Merge two clients


        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");


        ClientSearch.inputTextinLastNameField(lastName);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.selectFirstRecord();
        ClientSearch.selectSecondRecord();
        ClientSearch.clickOnMergeButton();
        ClientSearch.chooseFirstPrimaryClient();
        ClientSearch.saveButtonWhenChoosingPrimary();
        ClientSearch.clickOKConfirm();
        ClientSearch.closeSecondaryClient();
        ClientSearch.clickOKConfirm();
        ClientSearch.saveButton();
        ClientSearch.clickConfirmButton();
        Assert.assertEquals(ClientSearch.returnEngagedUnshelteredProspecClientCategoryOnDuplicateSearchGrid(), "Engaged Unsheltered Prospect");

    }

    @Test()
    public void VerifyClientInformationTabandHealthInformationTabTest() throws MalformedURLException, InterruptedException {
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnClientListMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
        ClientList.inputTextinClientListSearchBox("C135072");
        ClientList.submitSearchinGlobalSearchBox();
        ClientList.clickOnStreetSmartID("C135072");

        ClientInformation.clickOnEditButton();
        ClientInformation.inputLegalFirstName("Test");
        ClientInformation.inputLegalLastName("Test");
        ClientInformation.inputPreferredFirstName("Test");
        ClientInformation.inputPreferredLastName("Test");
        ClientInformation.inputAlternateName("Test");
        ClientInformation.selectGender("Male");
        ClientInformation.selectArea("Bronx");
        ClientInformation.clickSaveClientInformationButton();
        String clientCategorySelectedOption = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(clientCategorySelectedOption, "Engaged Unsheltered Prospect");

        HealthInformationTab.clickOnHealthInformationTab();
        HealthInformationTab.clickOnAddNewButton();
        HealthInformationTab.selectProvider("BG");
        HealthInformationTab.selectMedicalCoverage("Yes");
        HealthInformationTab.selectTypeofCoverage("Medicaid");
        HealthInformationTab.selectPsychosocialStatus("No");
        HealthInformationTab.selectPsychEvaluationStatus("No");
        HealthInformationTab.clickSaveHealthInformationButton();
        String medicalRecordNumberText = HealthInformationTab.returnMedicalRecordNumber();
        Assert.assertEquals(medicalRecordNumberText, "MR52307");
        HealthInformationTab.clickClientDetailsButton();
        String clientDetialsTitleText = HealthInformationTab.returnClientDetailsTitle();
        Assert.assertEquals(clientDetialsTitleText, "Client Details");
    }

    @Test()
    public void VerifyAssistedDailyLivingInfoTabAndIncomeInfoTab() throws MalformedURLException, InterruptedException {
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnClientListMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
        ClientList.inputTextinClientListSearchBox("C161550");
        ClientList.submitSearchinGlobalSearchBox();
        ClientList.clickOnStreetSmartID("C161550");

        AssistedDailyLivingInformationTab.clickOnAssistedDailyLivingInformationTab();
        AssistedDailyLivingInformationTab.clickOnAddNewButton();
        AssistedDailyLivingInformationTab.selectProvider("BG");
        AssistedDailyLivingInformationTab.clickOnRadioForGrooming();
        AssistedDailyLivingInformationTab.clickOnRadioForWalking();
        AssistedDailyLivingInformationTab.clickOnRadioForManagingMedications();
        AssistedDailyLivingInformationTab.clickOnSaveButton();
        String providerDetailTitleText = AssistedDailyLivingInformationTab.returnProviderDetails();
        Assert.assertEquals(providerDetailTitleText, "BG");

        AssistedDailyLivingInformationTab.clickClientDetailsButton();
        String clientDetialsTitleText = AssistedDailyLivingInformationTab.returnClientDetailsTitle();
        Assert.assertEquals(clientDetialsTitleText, "Client Details");
        AssistedDailyLivingInformationTab.selectNotesCollapseIcon();
        AssistedDailyLivingInformationTab.clickOnAddNewNotesButton();
        AssistedDailyLivingInformationTab.inputNotesText("TestNote");
        AssistedDailyLivingInformationTab.selectSaveNotes();
        String notesDetailText = AssistedDailyLivingInformationTab.returnNotesTextDetails();
        Assert.assertEquals(notesDetailText, "TestNote ");

        IncomeInformationTab.clickOnIncomeInformationTab();
        IncomeInformationTab.selectIncomeDataDropDownMenu("SSI/SSDI Data");
        String paymentStatusText = IncomeInformationTab.returnVisiblePaymentStatus();
        Assert.assertEquals(paymentStatusText, "N37");
        IncomeInformationTab.clickClientDetailsButton();
        String incomeinfoclientDetialsTitleText = IncomeInformationTab.returnClientDetailsTitle();
        Assert.assertEquals(incomeinfoclientDetialsTitleText, "Client Details");

        IncomeInformationTab.selectDeleteNoteButton();
        IncomeInformationTab.selectDeleteNoteButtonConfirmation();

    }

    @Test()
    //Verify the Rental Subsidy and Housing Search Tab for Engaged Unsheltered Prospect Client
    public void VerifyRentalSubsidyAndHousingSearchTab() throws MalformedURLException, InterruptedException {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnClientListMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
        ClientList.inputTextinClientListSearchBox("C135072");
        ClientList.submitSearchinGlobalSearchBox();
        ClientList.clickOnStreetSmartID("C135072");

        RentalSubsidyTab.clickOnRentalSubsidyTab();
        RentalSubsidyTab.verifyActiveSubsidiesButton();
        RentalSubsidyTab.verifyInactiveSubsidiesButton();
        RentalSubsidyTab.verifyWithdrawnSubsidiesButton();
        RentalSubsidyTab.verifyPendingSubsidiesButton();
        RentalSubsidyTab.verifyApplyingButton();
        RentalSubsidyTab.verifyEligibilityDeterminedButton();

        RentalSubsidyTab.clickClientDetailsButton();
        String clientDetialsTitleText = RentalSubsidyTab.returnClientDetailsTitle();
        Assert.assertEquals(clientDetialsTitleText, "Client Details");

        RentalSubsidyTab.clickOnViewMySubsidy();
        RentalSubsidyTab.clickOnAddVashSubsidy();
        RentalSubsidyTab.selectVashStatus("Applying");
        RentalSubsidyTab.clickOnSaveSubsidy();
        String vashSubsidyText = RentalSubsidyTab.verifyVashSubsidy();
        Assert.assertEquals(vashSubsidyText, "VASH");
        RentalSubsidyTab.clickOnLetterHistoryButton();

        HousingSearchInformationTab.clickOnHousingInformationTab();
        HousingSearchInformationTab.clickClientDetailsButton();
        String clientDetailsTitleText1 = HousingSearchInformationTab.returnClientDetailsTitle();
        Assert.assertEquals(clientDetailsTitleText1, "Client Details");
        HousingSearchInformationTab.clickOnAddNewButton();
        HousingSearchInformationTab.selectProvider("BG");
        HousingSearchInformationTab.selectHousingSearchStatus("Non-Compliant");
        HousingSearchInformationTab.clickOnSaveButton();
        String housingSearchStatusText = HousingSearchInformationTab.returnHousingSearchStatus();
        Assert.assertEquals(housingSearchStatusText, "Non-Compliant");

    }

    @Test()
    //Verify the Documents and Critical Incidents Tab for Engaged Unsheltered Prospect Client
    public void verifyDocumentandCriticalIncidents() throws MalformedURLException, InterruptedException {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnClientListMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
        ClientList.inputTextinClientListSearchBox("C135072");
        ClientList.submitSearchinGlobalSearchBox();
        ClientList.clickOnStreetSmartID("C135072");

        DocumentsTab.clickOnDocumentsTab();
        DocumentsTab.clickClientDetailsButton();
        String clientDetialsTitleText = DocumentsTab.returnClientDetailsTitle();
        Assert.assertEquals(clientDetialsTitleText, "Client Details");
        DocumentsTab.clickOnAddNewDocumentButton();
        DocumentsTab.selectDocumentStatus("Documents Applying For");
        DocumentsTab.selectDocumentType("Benefit Card");
        DocumentsTab.clickOnSaveButton();
        String documentStatusText = DocumentsTab.returnDocumentStatus();
        Assert.assertEquals(documentStatusText, "Documents Applying For");

        CriticalIncidentsTab.clickOnCriticalIncidentsTab();
        CriticalIncidentsTab.clickOnClientDetailsButton();
        String clientDetialsTitleTextOnCriticalIncidentsTab = CriticalIncidentsTab.returnClientDetailsTitle();
        Assert.assertEquals(clientDetialsTitleTextOnCriticalIncidentsTab, "Client Details");
        CriticalIncidentsTab.clickOnAddNewButton();
        CriticalIncidentsTab.clickOnProviderField("BG");
        CriticalIncidentsTab.clickOnAreaField("Bronx");
        CriticalIncidentsTab.clickOnShiftField("Afternoon");
        CriticalIncidentsTab.clickOnResponseTeamField();
        CriticalIncidentsTab.selectResponseTeam();
        CriticalIncidentsTab.selectBorough("Bronx");
        CriticalIncidentsTab.selectLocationType("Phone");
        CriticalIncidentsTab.enterPhone("1234567890");
        CriticalIncidentsTab.selectSaveAsDraft();
        String providerText = CriticalIncidentsTab.verifyProviderText();
        Assert.assertEquals(providerText, "Breaking Ground");

    }

    @Test()
    //Verify the Engagements and Documents Tab for Engaged Unsheltered Prospect Client
    public void verifyEngagemntsTab() throws MalformedURLException, InterruptedException {
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnClientListMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
        ClientList.inputTextinClientListSearchBox("C135072");
        ClientList.submitSearchinGlobalSearchBox();
        ClientList.clickOnStreetSmartID("C135072");

        EngagementsTab.clickOnEngagementsTab();
        AddEngagement.clickOnEngagementTab();

        AddEngagement.clickOnAddNewEngagementButton();
        AddEngagement.selectEngagementProvider("BG");
        AddEngagement.selectEngagementArea("Bronx");
        AddEngagement.selectEngagementShift("Evening");
        AddEngagement.selectEngagementJointOutreach("No");
        AddEngagement.selectEngagementContactReason("311 Response");
        AddEngagement.selectEngagementContactTime();
        AddEngagement.selectEngagementContactDateField();
        AddEngagement.selectEngagementDatePickerPrevious();
        AddEngagement.selectEngagementFirstDate();

        AddEngagement.selectEngagementOutcome("958 Certified Removal");
        AddEngagement.selectEngagementBorough("Bronx");
        AddEngagement.selectLocationType("Park");
        AddEngagement.selectParkName("Amersfort");
        AddEngagement.inputEngagementNotes("1");
        AddEngagement.selectSaveEngagement();

        String engagementProviderText = AddEngagement.verifyEngagementProvider();
        Assert.assertEquals(engagementProviderText, "Breaking Ground");


    }


    @Test()
    //Verify the Engagements and Documents Tab for Engaged Unsheltered Prospect Client
    public void verifyClientContactsTab() throws MalformedURLException, InterruptedException {
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();


        Base.clickOnClientListMenuItem();
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
        ClientList.inputTextinClientListSearchBox("C161609");
        ClientList.submitSearchinGlobalSearchBox();
        ClientList.clickOnStreetSmartID("C161609");

        ClientContactsTab.clickOnClientContactsTab();
        ClientContactsTab.clickOnAddNewButton();
        ClientContactsTab.selectContactType("Client Contact");
        ClientContactsTab.enterEmailAddress("test@test.com");
        ClientContactsTab.enterPhoneNumber("1234567890");
        ClientContactsTab.clickOnSaveButton();

        String contactTypeText = ClientContactsTab.verifyContactType();
        Assert.assertEquals(contactTypeText, "Client Contact");

        ClientContactsTab.clickOnNotesButton();
        ClientContactsTab.clickOnAddNewNote();
        ClientContactsTab.enterNotesText("Test");
        ClientContactsTab.clickSaveNote();

        String verifyNoteText = ClientContactsTab.verifyNoteText();
        Assert.assertEquals(verifyNoteText, "Test ");

    }


    //Search for Client Category on Duplicate Search grid and verify Client Category is on column picker. And can be enabled and disabled
    //using column picker
    @Test()
    public void searchForClientCategoryOnDuplicateSearchGrid() {
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");

        ClientSearch.clickOnexpandClientIDdetailsOnDuplicateSearch();
        ClientSearch.selectCategoryDropDownMenuOnDuplicateSearch("Engaged Unsheltered Prospect");
        ClientSearch.clickOnDupliceSearchButton();
        String clientCategoryColumnText = ClientSearch.returnclientCategoryColumnOnDuplicateSearch();
        Assert.assertEquals(clientCategoryColumnText, "Client Category");
        String valueOnGridText = ClientSearch.returnclientCategoryValueongridOnDuplicateSearch();
        Assert.assertEquals(valueOnGridText, "Engaged Unsheltered Prospect");
        ClientSearch.clickOnColumnPickerOnDuplicateSearch();
        ClientSearch.clickOnclientCategoryOnColumnPicker();
        ClientSearch.clickOnColumnPickerOnDuplicateSearch();
        boolean presenceOfClientCategoryColumn = ClientSearch.verifyIfclientCategoryColumnExistsOnDuplicateSearch();
        Assert.assertFalse(presenceOfClientCategoryColumn, "The Client Category Column is present");
        ClientSearch.clickOnColumnPickerOnDuplicateSearch();
        ClientSearch.clickOnclientCategoryOnColumnPicker();
        ClientSearch.clickOnColumnPickerOnDuplicateSearch();
        presenceOfClientCategoryColumn = ClientSearch.verifyIfclientCategoryColumnExistsOnDuplicateSearch();
        Assert.assertTrue(presenceOfClientCategoryColumn, "The Client Category Column is not present");

    }

    //Search for Client Category on Client Search grid and verify Client Category is on column picker. And can be enabled and disabled
    //using column picker
    @Test()
    public void searchForClientCategoryOnClientSearchGrid() {
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        ClientSearch.expandClientSearchPanel();
        WebDriverTasks.pageDownCommon();

        ClientSearch.clickOnexpandClientIDdetailsOnClientSearch();
        ClientSearch.selectCategoryDropDownMenuOnClientSearch("Engaged Unsheltered Prospect");
        ClientSearch.clickOnClientSearchButton();
        String clientCategoryColumnText = ClientSearch.returnclientCategoryColumnOnClientSearch();
        Assert.assertEquals(clientCategoryColumnText, "Client Category");
        String valueOnGridText = ClientSearch.returnclientCategoryValueongridOnClientSearch();
        Assert.assertEquals(valueOnGridText, "Engaged Unsheltered Prospect");
        ClientSearch.clickOnColumnPickerOnClientSearch();
        ClientSearch.clickOnclientCategoryOnColumnPicker();
        ClientSearch.clickOnColumnPickerOnClientSearch();
        boolean presenceOfClientCategoryColumn = ClientSearch.verifyIfclientCategoryColumnExistsOnClientSearch();
        Assert.assertFalse(presenceOfClientCategoryColumn, "The Client Category Column is present");
        ClientSearch.clickOnColumnPickerOnClientSearch();
        ClientSearch.clickOnclientCategoryOnColumnPicker();
        ClientSearch.clickOnColumnPickerOnClientSearch();
        presenceOfClientCategoryColumn = ClientSearch.verifyIfclientCategoryColumnExistsOnClientSearch();
        Assert.assertTrue(presenceOfClientCategoryColumn, "The Client Category Column is not present");

    }

    //Verify the client categories (Caseload, Other Prospect, Enrollment Ended, Engaged Unsheltered Prospect, Inactive) in the Duplicate Search
    @Test()
    public void verifyClientCategoriesinDuplicateSearchOfClientSearch() {
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");

        ClientSearch.clickOnexpandClientIDdetailsOnDuplicateSearch();

        Assert.assertEquals(ClientSearch.returnduplicateSearchClientCategoryCaseload(), "Caseload");
        Assert.assertEquals(ClientSearch.returnduplicateSearchClientCategoryOtherProspect(), "Other Prospect");
        Assert.assertEquals(ClientSearch.returnduplicateSearchClientCategoryEnrollmentEnded(), "Enrollment Ended");
        Assert.assertEquals(ClientSearch.returnduplicateSearchClientCategoryEngagedUnshelteredProspect(), "Engaged Unsheltered Prospect");
        Assert.assertEquals(ClientSearch.returnduplicateSearchClientCategoryInactive(), "Inactive");
        ClientAndEngagementSearch.returnDuplicateSearchClientCategories();

        ClientSearch.selectCategoryDropDownMenuOnDuplicateSearch("Engaged Unsheltered Prospect");

        ClientSearch.clickOnDupliceSearchButton();
        String valueOnGridText = ClientSearch.returnclientCategoryValueongridOnDuplicateSearch();
        Assert.assertEquals(valueOnGridText, "Engaged Unsheltered Prospect");

    }

    //Create a new Engaged Unsheltered Client from Duplicate Search. And then verify client category of same client from client search
    @Test()
    public void addNewClientEngagedUnshelteredClientAndVerifyClientSearch() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + random;
        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation");
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
        Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName("Automation");
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput("Automation");
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BRC");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        String streetSmartID = ClientInformation.returnStreetsmartID();
        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");


        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        ClientSearch.expandClientSearchPanel();
        WebDriverTasks.pageDownCommon();
        ClientSearch.inputTextinClientSearchFirstNameField(firstName);
        ClientSearch.inputTextinClientSearchLastNameField("Automation");
        ClientSearch.clickOnClientSearchButton();
        Common.clickOnLinkText(streetSmartID);

        ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");

    }

    //Create a new Engaged Unsheltered Client from Duplicate Search. And then verify client category in duplicate search of same client from client search
    @Test()
    public void addNewClientEngagedUnshelteredClientAndVerifyClientCategoryfromDuplicateSearchOfClientSearch() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + random;
        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation");
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
        Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName("Automation");
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput("Automation");
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BRC");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        String streetSmartID = ClientInformation.returnStreetsmartID();
        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");


        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation");
        ClientSearch.clickOnDupliceSearchButton();
        Common.clickOnLinkText(streetSmartID);

        ClientCategory = ViewClientInformationfromSearch.returnClientCategory();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");

    }

    //Create a new Engaged Unsheltered Client from Duplicate Search. And then verify client category in duplicate search of same client from client search
    @Test()
    public void addInactiveClientAndVerifyClientCategoryfromDuplicateSearchOfClientSearch() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + random;
        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation");
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnEngagedUnshelteredButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
        Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName("Automation");
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput("Automation");
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BRC");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        String streetSmartID = ClientInformation.returnStreetsmartID();
        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");


        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation");
        ClientSearch.clickOnDupliceSearchButton();
        Common.clickOnLinkText(streetSmartID);

        ClientCategory = ViewClientInformationfromSearch.returnClientCategory();
        Assert.assertEquals(ClientCategory, "Engaged Unsheltered Prospect");

    }


    //verify Inactive client category of existing client from client search
    @Test()
    public void verifyInactiveClientFromClientSearch() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        ClientSearch.expandClientSearchPanel();
        WebDriverTasks.pageDownCommon();
        ClientSearch.inputTextinClientSearchFirstNameField("User3823");
        ClientSearch.inputTextinClientSearchLastNameField("Automation");
        ClientSearch.clickOnClientSearchButton();
        Common.clickOnLinkText("C161054");

        String ClientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
        Assert.assertEquals(ClientCategory, "Inactive");

    }

    //Create a new Prospect client
    @Test()
    public void addNewProspectClientfromDuplicateSearch() {

        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();

        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
        Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + random;
        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation");
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();

        Common.clickOnProspectButton();

        Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");

        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName("Automation");
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput("Automation");
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BRC");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.clickOnSaveButton();
        ClientInformation.returnStreetsmartID();
        String chosenClientCategory = ClientInformation.returnSelectedProspectOrCaseLoadClientCategoryOnViewPage();
        Assert.assertEquals(chosenClientCategory, "Other Prospect");

    }

    //H20-389 - As a User,when I click on My Caseload within Clients on the left navigation,I want JCC,JCC Client Start Date,
    // JCC Client End Date to be displayed on My Caseload grid by default.
    @Test()
    public static void VerifyCaseloadJCCColumns() {
        Base.clickOnWhatsNewNotificaitonCloseButton();
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();

        Base.clickOnMyCaseLoadMenuItem();
        Assert.assertTrue(ClientList.verifyJCCClientColumnExists(), "JCC Client column is not displayed");
        Assert.assertTrue(ClientList.verifyJCCClientSTartDateColumnExists(), "JCC Client Start Date column is not displayed");
        Assert.assertTrue(ClientList.verifyJCCClientEndDateColumnExists(), "JCC Client End Date column is not displayed");
    }


    //H20-391 - As a user I want to Search My Caseload grid by JCC Client, JCC Client Start Date, JCC Client End Date so that I can see the records that match the criteria
    @Test()
    public static void VerifyCaseloadJCCColumnsAfterSearch() {
        //This will Navigate to My case load
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnMyCaseLoadMenuItem();

        //This will search for 'Caseload'
        ClientList.searchFor("No");

        //This will verify that search results contains columns 'JCC Client','JCC Client Start Date', 'JCC Client End Date'
        Assert.assertTrue(ClientList.verifyJCCClientColumnExists(), "JCC Client column is not displayed");
        Assert.assertTrue(ClientList.verifyJCCClientSTartDateColumnExists(), "JCC Client Start Date column is not displayed");
        Assert.assertTrue(ClientList.verifyJCCClientEndDateColumnExists(), "JCC Client End Date column is not displayed");

        //This will go through all the records and verify that 'JCC Client' column contains data 'No'
        boolean isExpectedDataDisplayed = MyCaseload.verifyDataInJCCClientColumn("No");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'No' data is displayed for JCC Client column");

    }


    //H20-456 - As a user I want to advance filter on My Caseload grid by JCC Client, JCC Client Start Date, JCC Client End Date,
    // so that I can see all clients that meet the applied filter criteria
    @Test()
    public static void VerifyCaseloadJCCColumnsDataAfterAdvancedFiltering() {
        //Navigate to My Caseload Page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnMyCaseLoadMenuItem();

        //Apply advanced filter for 'JCC Client' column and for records having value 'No'
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        ClientList.setClickOnJCCClientCheckbox();
        ClientList.selectJCCClient("No");
        ClientList.clickOnApplyFilter();

        //This will verify that 'JCC Client' column contains only records with value 'No'
        boolean isFiltered = ClientList.verifyFilteredValuesForJCCClient("JCC Client", "No");
        Assert.assertTrue(isFiltered, "Data is not filtered properly for JCC Client column");

        //Apply advanced filter for 'JCC Client Start Date' column and for records having value '05/07/2020'
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        ClientList.setClickOnJCCClientStartDateCheckbox();
        ClientList.setJCCClientStartDate("05/07/2020");
        ClientList.clickOnApplyFilter();

        //This will verify that 'JCC Client Start Date' column contains only records with start date 05/07/2020
        isFiltered = ClientList.verifyFilteredValuesForJCCClient("JCC Client Start Date", "05/07/2020");
        Assert.assertTrue(isFiltered, "Data is not filtered properly for JCC Client Start Date column");

        //Apply advanced filter for 'JCC Client End Date' column and for records having value '06/18/2020'
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        ClientList.setClickOnJCCClientateEndDateCheckbox();
        ClientList.setJCCClientEndDate("06/18/2020");
        ClientList.clickOnApplyFilter();

        //This will verify that 'JCC Client End Date' column contains only records with start date 06/18/2020
        isFiltered = ClientList.verifyFilteredValuesForJCCClient("JCC Client End Date", "06/18/2020");
        Assert.assertTrue(isFiltered, "Data is not filtered properly for JCC Client End Date column");
    }

    //H20-651 - As a User,when I excel export on the My Caseload grid,I want JCC,JCC Client Start Date and JCC Client End Date fields to appear by default when I export by ALL.
    @Test()
    public static void VerifyMyCaseloadJcCColumnsInExcel() throws IOException {
        int fileCountBefore = Helpers.countFilesInDownloadFolder();
        Base.clickOnWhatsNewNotificaitonCloseButton();
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnMyCaseLoadMenuItem();
        WebDriverTasks.waitForAngular();

        ClientList.clickOnExportToExcel();
        ClientList.clickOnExportToExcelAllRecords();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();

        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
        System.out.println(fileCountAfter);

        String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
        String JCCClientColumn = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 29);
        String JCCClientStartDate = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 30);
        String JCCClientEndDate = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 31);
        Assert.assertTrue(JCCClientColumn.equals("JCC Client"), "The column name on column number 29 is " + JCCClientColumn);
        Assert.assertTrue(JCCClientStartDate.equals("JCC Client Start Date"), "The column name on column number 30 is " + JCCClientStartDate);
        Assert.assertTrue(JCCClientEndDate.equals("JCC Client End Date"), "The column name on column number 31 is " + JCCClientEndDate);

        boolean isValidJCCClientData = ClientList.verifyJCCClientColumnDataInExcel(mostRecentFile, "OutreachClientListReport", 29);
        Assert.assertTrue(isValidJCCClientData, "The data for JCC Client column is other than '','Yes' or 'No'");

        boolean isValidJCCClientStartDateData = ClientList.verifyJCCClientStartDateColumnDataInExcel(mostRecentFile, "OutreachClientListReport", 30);
        Assert.assertTrue(isValidJCCClientStartDateData, "The data for JCC Client Start Date column is not a valid date");

        boolean isValidJCCClientEndDateData = ClientList.verifyJCCClientEndDateColumnDataInExcel(mostRecentFile, "OutreachClientListReport", 31);
        Assert.assertTrue(isValidJCCClientEndDateData, "The data for JCC Client End Date column is not a valid date");
    }

    //H20-348 - Advance Filter on My Caseload grid must have only 1 option for Client Category Filter and that value must be Caseload
    @Test()
    public static void VerifyMycaseloadClientCategoryFilterOptionCaseload() {
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnMyCaseLoadMenuItem();

        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        ClientList.clickOnClientCategoryCheckbox();

        boolean isCaseloadDsiplayed = ClientList.verifyClientCategoryOptions();
        Assert.assertTrue(isCaseloadDsiplayed, "Client category option contains options other than caseload");
    }

    //H20-749 As a user I should not be able to manually Change An Engaged Unsheltered Prospect Client to Prospect
    @Test()
    public static void VerifyUserIsNotAbleToChangeClientCategoryForEngagedUnshelteredProspect() {
        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Search for Engaged Unsheltered Clinet
        ClientList.searchFor("Engaged unsheltered");

        //Click on the first client id in the table
        ClientList.clickonFirstClientID();

        //Verify that all expected client category options are displayed in drop down
        boolean isExpectedOptionsDisplayed = ClientList.verifyClientCategoryOptionsOnClientPage();
        Assert.assertTrue(isExpectedOptionsDisplayed, "Expected options are not displayed for Client category dropdown");

        //Verify that user can select 'Inactive' option for client category
        boolean isSelectable = ClientList.verifyUserIsAbleToSelectClientCategoryOption("Inactive");
        Assert.assertTrue(isSelectable, "Client category can not be changed to ''Inactive''");

        //Verify that user can select 'Caseload' option for client category
        isSelectable = ClientList.verifyUserIsAbleToSelectClientCategoryOption("Caseload");
        Assert.assertTrue(isSelectable, "Client category can not be changed to ''Caseload''");

    }

    /*H20-674 As a user when I delete an Engagement then the Autoflipped Client must be updated from Engaged Unsheltered Prospect to Other Prospect*/
    @Test()
    public static void VerifyAutoFlipFunctionalityOnEngagementDeletion() throws ParseException {
        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Apply Advanced filter for Client Category = 'Other Prospect'
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clickOnRadioClientCategory();
        ClientList.clickOnClientCategorySubDropdown();
        ClientList.clickOnOtherProspectdRadio();
        ClientList.clickOnApplyFilter();

        //Open first Client details
        ClientList.clickonFirstClientID();

        ClientInformation.clickOnEngagementsTab();

        //Delete all existing engagements, so that we can create 3 new engagements to convert the client category
        // out of which on the first one we can delete/update
        ClientInformation.deleteAllEngagements();

        //Add new engagement with engagement date '07/25/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/25/2020", "10:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '07/27/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/27/2020", "11:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '07/29/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/29/2020", "01:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //This will delete first engagement
        boolean isDeleted = ClientInformation.deleteEnagagementRecord(0);
        Assert.assertTrue(isDeleted, "Engagement could not be deleted");

        ClientInformation.clickOnClientInformationTab();

        //This method will verify if client category is changed to 'Other Prospect' or not
        boolean isClientCategoryChanged = ClientInformation.verifyClientCategoryIsChangedToOtherProspect();
        Assert.assertTrue(isClientCategoryChanged, "Client category is not changed to 'Other Prospect'");
    }

    //H20-680 As a user when I update the Provider on one of the three (3) engagements record then the system must
    // update the Client Category from Engaged Unsheltered to Other Prospect*/
    @Test()
    public static void VerifyAutoFlipFunctionalityOnProviderChange() throws ParseException {
        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Apply Advanced filter for Client Category = 'Other Prospect'
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clickOnRadioClientCategory();
        ClientList.clickOnClientCategorySubDropdown();
        ClientList.clickOnOtherProspectdRadio();
        ClientList.clickOnApplyFilter();

        //Open first Client details
        ClientList.clickonFirstClientID();

        ClientInformation.clickOnEngagementsTab();

        //Delete all existing engagements, so that we can create 3 new engagements to convert the client category
        // out of which on the first one we can delete/update
        ClientInformation.deleteAllEngagements();

        //Add new engagement with engagement date '07/25/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/25/2020", "10:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '07/27/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/27/2020", "11:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '07/29/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/29/2020", "01:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //This will change Engagement Provider to 'BR' for first engagement
        boolean isChanged = ClientInformation.changeEngagementProvider(0);
        Assert.assertTrue(isChanged, "Provider has been changed successful");

        ClientInformation.clickOnClientInformationTab();

        //This method will verify if client category is changed to 'Other Prospect' or not
        boolean isClientCategoryChanged = ClientInformation.verifyClientCategoryIsChangedToOtherProspect();
        Assert.assertTrue(isClientCategoryChanged, "Client category is not changed to 'Other Prospect'");
    }

    /*H20-678 As a user when I update the Contact Date on one of the three (3) engagements record then the system must
     update the Client Category from Engaged Unsheltered to Other Prospect*/
    @Test()
    public static void VerifyAutoFlipFunctionalityOnDateChange() {
        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Apply Advanced filter for Client Category = 'Other Prospect'
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clickOnRadioClientCategory();
        ClientList.clickOnClientCategorySubDropdown();
        ClientList.clickOnOtherProspectdRadio();
        ClientList.clickOnApplyFilter();

        //Open first Client details
        ClientList.clickonFirstClientID();

        ClientInformation.clickOnEngagementsTab();

        //Delete all existing engagements, so that we can create 3 new engagements to convert the client category
        // out of which on the first one we can delete/update
        ClientInformation.deleteAllEngagements();

        //Add new engagement with engagement date '07/25/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/25/2020", "10:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '07/25/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/27/2020", "11:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '07/25/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/29/2020", "01:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //This method will change engagement date for first row
        boolean isDeleted = ClientInformation.changeEngagementDateForRow(0, "6/15/2020");
        Assert.assertTrue(isDeleted, "Engagement date has been changed successfully");

        ClientInformation.clickOnClientInformationTab();

        //This method will verify if client category is changed to 'Other Prospect' or not
        boolean isClientCategoryChanged = ClientInformation.verifyClientCategoryIsChangedToOtherProspect();
        Assert.assertTrue(isClientCategoryChanged, "Client category is not changed to 'Other Prospect'");
    }

    /*H20-649 As a User,I want to see the Change Log capturing the Client Category updates made by the user and by the System when the Client Category is either
     auto-flipped or manually flipped from one category to another on the Client Information page.*/
    @Test()
    public static void VerifyChangeLogOnClientCategoryUpdate() {
        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Apply Advanced filter for Client Category = 'Other Prospect'
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clickOnRadioClientCategory();
        ClientList.clickOnClientCategorySubDropdown();
        ClientList.clickOnOtherProspectdRadio();
        ClientList.clickOnApplyFilter();

        //Open first Client details
        ClientList.clickonFirstClientID();

        ClientInformation.clickOnEngagementsTab();

        //Delete all existing engagements, so that we can create 3 new engagements to convert the client category
        // out of which on the first one we can delete/update
        ClientInformation.deleteAllEngagements();

        //Add new engagement with engagement date '07/25/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/25/2020", "10:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '07/25/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/25/2020", "11:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '07/25/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("07/25/2020", "01:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        ClientInformation.clickOnClientInformationTab();

        ClientInformation.expandChangeLog();

        //This will wait until Change log pane is expanded
        WebDriverTasks.waitForAngular();

        //This code will verify that Client Category change has been reflected in Change log
        boolean verifyLog = ClientInformation.verifyLatestClientCategoryChangeInLog("Engaged Unsheltered Prospect");
        Assert.assertTrue(verifyLog, "Change log for Client category change to 'Engaged Unsheltered Prospect' should be displayed in logs");

        //This code will verify that Changed By value has been reflected in Change log
        boolean verifyChangedBy = ClientInformation.verifyLastClientCategoryChangedByInLog("System");
        Assert.assertTrue(verifyChangedBy, "Changed by value is 'System' for Client category change");

        ClientInformation.clickOnEngagementsTab();

        //This will change engagement date to '06/22/2020' for the first engagement
        boolean isDeleted = ClientInformation.changeEngagementDateForRow(0, "6/22/2020");
        Assert.assertTrue(isDeleted, "Engagement date has been changed successfully");

        ClientInformation.clickOnClientInformationTab();

        //This method will verify that Client Category is changed to 'Other Prospect'
        boolean isClientCategoryChanged = ClientInformation.verifyClientCategoryIsChangedToOtherProspect();
        Assert.assertTrue(isClientCategoryChanged, "Client category is not changed to 'Other Prospect'");

        ClientInformation.expandChangeLog();

        //This will wait until Change log pane is expanded
        WebDriverTasks.waitForAngular();

        //This code will verify that Client Category change has been reflected in Change log
        verifyLog = ClientInformation.verifyLatestClientCategoryChangeInLog("Other Prospect");
        Assert.assertTrue(verifyLog, "Change log for Client category change to 'Engaged Unsheltered Prospect' should be displayed in logs");

        //This code will verify that Changed By value has been reflected in Change log
        verifyChangedBy = ClientInformation.verifyLastClientCategoryChangedByInLog("System");
        Assert.assertTrue(verifyChangedBy, "Changed by value is 'System' for Client category change");
    }


    //H20-676 As a user when I delete all "Case" for a client then the user must be prompted the option to change the status of the client to Engaged Unsheltered Prospect Client.
    @Test()
    public static void VerifyDeleteAllCaseAndAddNewEngagedUnshelteredCaseFunctionality() {
        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Advanced filter for 'Client Category' with data 'Caseload'
        DailyOutreach.advancedFilterBy("ClientCategory", "Caseload", false);

        //Open first client
        ClientList.clickonFirstClientID();

        //Click on Case Information tab
        ClientInformation.clickOnCaseInformationTab();

        //Delete all existing cases
        ClientInformation.deleteAllCases();

        //Verify Delete Case popup is displayed
        boolean isPopupDisplayed = ClientInformation.verifyDeleteCasePopupDisplayed();
        Assert.assertTrue(isPopupDisplayed, "Delete Case popup should be displayed");

        //Verify radio option 'Update this client's status to Engaged Unsheltered Prospect' is displayed
        boolean isRadioOptionDisplayed = ClientInformation.verifyEngagedRadioOptionIsDisplayed();
        Assert.assertTrue(isRadioOptionDisplayed, "Radio option for 'Update this client's status to Engaged Unsheltered Prospect' should be displayed");

        //Select 'Update this client's status to Engaged Unsheltered Prospect' option and click on next
        ClientInformation.clickOnUpdateToEngagedRadio();
        ClientInformation.clickOnNextButton();

        //Verify default tab displayed is 'Client Information' tab
        boolean isClinetInformationPageDisplayed = ClientInformation.verifyClientInformationPageIsDisplayed();
        Assert.assertTrue(isClinetInformationPageDisplayed, "'Client Information' page is not displayed after adding Engagement case load");

        //Verify that default client category is 'Engaged Unsheltered Prospect'
        String actualClientCategory = ClientInformation.getClientCategory();
        Assert.assertTrue(("Engaged Unsheltered Prospect").equalsIgnoreCase(actualClientCategory), "Client category should be updated to 'Engaged Unsheltered Prospect', but actual client category is " + actualClientCategory);

        //Verify tab 'Client Information' is displayed
        boolean isTabDisplayed = ClientInformation.verifyTabDisplayed("Client Information");
        Assert.assertTrue(isTabDisplayed, "'Client Information' tab is not displayed on page");

        //Verify tab 'Health Information' is displayed
        isTabDisplayed = ClientInformation.verifyTabDisplayed("HealthInformation");
        Assert.assertTrue(isTabDisplayed, "'Health Information' tab is not displayed on page");

        //Verify tab 'Assisted Daily Living Information' is displayed
        isTabDisplayed = ClientInformation.verifyTabDisplayed("Assisted DailyLiving Information");
        Assert.assertTrue(isTabDisplayed, "'Assisted Daily Living Information' tab is not displayed on page");

        //Verify tab 'Income Information' is displayed
        isTabDisplayed = ClientInformation.verifyTabDisplayed("Income Information");
        Assert.assertTrue(isTabDisplayed, "'Income Information' tab is not displayed on page");

        //Verify tab 'Rental Subsidy' is displayed
        isTabDisplayed = ClientInformation.verifyTabDisplayed("Rental Subsidy");
        Assert.assertTrue(isTabDisplayed, "'Rental Subsidy' tab is not displayed on page");

        //Verify tab 'Housing Search Information' is displayed
        isTabDisplayed = ClientInformation.verifyTabDisplayed("Housing SearchInformation");
        Assert.assertTrue(isTabDisplayed, "'Housing Search Information' tab is not displayed on page");

        //Verify tab 'Documents' is displayed
        isTabDisplayed = ClientInformation.verifyTabDisplayed("Documents");
        Assert.assertTrue(isTabDisplayed, "'Documents' tab is not displayed on page");

        //Verify tab 'Critical Incidents' is displayed
        isTabDisplayed = ClientInformation.verifyTabDisplayed("CriticalIncidents");
        Assert.assertTrue(isTabDisplayed, "'Critical Incidents' tab is not displayed on page");

        //Verify tab 'Engagements' is displayed
        isTabDisplayed = ClientInformation.verifyTabDisplayed("Engagements");
        Assert.assertTrue(isTabDisplayed, "'Engagements' tab is not displayed on page");

        //Verify tab 'Client Contacts' is displayed
        isTabDisplayed = ClientInformation.verifyTabDisplayed("ClientContacts");
        Assert.assertTrue(isTabDisplayed, "'Client Contacts' tab is not displayed on page");

        //Verify no other tabs are displayed except above mentioned 10 tabs
        int visibleTabCount = ClientInformation.getVisibleTabsCount();
        Assert.assertTrue(10 == visibleTabCount, "More than expected tabs (>10) displayed on page");

        //Get Street Smart ID
        String streetSmartID = ClientInformation.returnStreetsmartID();

        //Expand Change log pane
        ClientInformation.expandChangeLog();

        //This code will verify that Client Category change has been reflected in Change log
        boolean verifyLog = ClientInformation.verifyLatestClientCategoryChangeInLog("Engaged Unsheltered Prospect");
        Assert.assertTrue(verifyLog, "Change log for Client category change to 'Engaged Unsheltered Prospect' should be displayed in logs");

        //Get  logged in user name
        String userName = Base.returnLoggedInByUser();

        //This code will verify that Changed By value has been reflected in Change log
        boolean verifyChangedBy = ClientInformation.verifyLastClientCategoryChangedByInLog(userName);
        Assert.assertTrue(verifyChangedBy, "Changed by value is not as expected in Change logs");

        //Navigate to Population Trends page
        Base.clickOnPopulationTrendsMenu();

        //Expand Activity Stream pane
        PopulationTrends.clickOnExpandActivityStreamButton();

        //Search with street smart id
        PopulationTrends.inputTexInSearchFieldOfActivityStream(streetSmartID);
        PopulationTrends.submitSearchInActivityStream();

        //Verify that expected value is displayed for 'Updated By'
        String updatedBy = PopulationTrends.returnUpdatedByFirstRowOfActivityStreamGrid();
        Assert.assertTrue(userName.equals(updatedBy), "Invalid 'Updated By' value '" + updatedBy + "' is displayed on Activity stream table");

        //Verify that expected value is displayed for 'Action Summary'
        String actionSummary = PopulationTrends.returnActionSummaryFirstRowOfActivityStreamGrid();
        Assert.assertTrue(actionSummary.contains("Client Category: Engaged Unsheltered Prospect"), "Invalid 'Action Summary' value '" + actionSummary + "' is displayed on Activity stream table");

        //Verify that expected value is displayed for 'Client'
        String itemType = PopulationTrends.returnItemColumnFirstRowOfActivityStreamGrid();
        Assert.assertTrue(("Client").equals(itemType), "Invalid 'Client' value '" + itemType + "' is displayed on Activity stream table");
    }

    //H20-979 As a user,when I want to see a notification displaying that the Client Category has been transitioned successfully when I add or edit an engagement of a Client and if his Client Category autoflips from Other Prospect to Engaged Unsheltered Prospect.
    @Test()
    public static void VerifyClientCategoryTransitionToEngagedMessage() {
        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Apply Advanced filter for Client Category = 'Other Prospect'
        DailyOutreach.advancedFilterBy("ClientCategory", "Other Prospect", true);

        //Open first Client details
        ClientList.clickonFirstClientID();

        //Open Engagements tab
        ClientInformation.clickOnEngagementsTab();

        //Delete all existing engagements, so that we can create 3 new engagements to convert the client category
        // out of which on the first one we can delete/update
        ClientInformation.deleteAllEngagements();

        //Add new engagement with engagement date '08/20/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("08/20/2020", "10:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '08/20/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("08/20/2020", "11:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '08/20/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("08/20/2020", "01:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //To verify that notification is displayed
        boolean isAutoflipNotificationDisplayed = ClientInformation.verifyClientCategoryChangedToEngagedSuccessMessageIsDisplayed();
        Assert.assertTrue(isAutoflipNotificationDisplayed, "Message 'Client Category has been updated to Engaged Unsheltered Prospect' is not displayed on adding 3rd engagement within 30 days and for same provider");

        //TO fetch notification text and verify that text matches expected text
        String expectedNotificationText = "Client Category has been updated to Engaged Unsheltered Prospect";
        String actualNotificationText = ClientInformation.getAutoflipNotificationText();
        Assert.assertTrue(expectedNotificationText.equals(actualNotificationText), "When client category is changed, expected notification message '" + expectedNotificationText + "' is not displayed");

        //This method will change engagement date for first record
        boolean isUpdated = ClientInformation.changeEngagementDateForRow(0, "7/18/2020");
        Assert.assertTrue(isUpdated, "Engagement date can not be changed");

        //This method will change engagement date for last record
        isUpdated = ClientInformation.changeEngagementDateForRow(2, "8/20/2020");
        Assert.assertTrue(isUpdated, "Engagement date can not be changed");

        //To verify that notification is displayed
        isAutoflipNotificationDisplayed = ClientInformation.verifyClientCategoryChangedToEngagedSuccessMessageIsDisplayed();
        Assert.assertTrue(isAutoflipNotificationDisplayed, "Autoflip success message 'Client Category has been updated to Engaged Unsheltered Prospect' is not displayed on changing the date withing 30 days for same provider");

        //TO fetch notification text and verify that text matches expected text
        actualNotificationText = ClientInformation.getAutoflipNotificationText();
        Assert.assertTrue(expectedNotificationText.equals(actualNotificationText), "When client category is changed, expected notification message '" + expectedNotificationText + "' is not displayed");

        //This will change Engagement Provider to 'BW' for first engagement
        boolean isChanged = ClientInformation.changeEngagementProvider(0);
        Assert.assertTrue(isChanged, "Provider can not be changed to 'BW'");

        //This will change Engagement Provider to 'BRC' for first engagement
        isChanged = ClientInformation.changeEngagementProviderToBRC(0);
        Assert.assertTrue(isChanged, "Provider can not be changed to 'BRC'");

        //To verify that notification is displayed
        isAutoflipNotificationDisplayed = ClientInformation.verifyClientCategoryChangedToEngagedSuccessMessageIsDisplayed();
        Assert.assertTrue(isAutoflipNotificationDisplayed, "Autoflip success message 'Client Category has been updated to Engaged Unsheltered Prospect' is not displayed on changing the provider");

        //TO fetch notification text and verify that text matches expected text
        actualNotificationText = ClientInformation.getAutoflipNotificationText();
        Assert.assertTrue(expectedNotificationText.equals(actualNotificationText), "When client category is changed, expected notification message '" + expectedNotificationText + "' is not displayed");

        //Delete all existing engagements, so client category can be reverted back to 'Other Engagement'
        ClientInformation.deleteAllEngagements();
    }

    //H20-987 As a user,when I want to see a notification banner displaying that the Client Category has been updated when I edit or delete an engagement of a Client and if his Client Category autoflips from Engaged Unsheltered Prospect to Other Prospect.
    @Test()
    public static void VerifyClientCategoryTransitionToOtherProspectMessage() {
        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Apply Advanced filter for Client Category = 'Other Prospect'
        DailyOutreach.advancedFilterBy("ClientCategory", "Other Prospect", true);

        //Open first Client details
        ClientList.clickonFirstClientID();

        //Open engagements tab
        ClientInformation.clickOnEngagementsTab();

        //Delete all existing engagements, so that we can create 3 new engagements to convert the client category
        // out of which on the first one we can delete/update
        ClientInformation.deleteAllEngagements();

        //Add new engagement with engagement date '08/20/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("08/20/2020", "10:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '08/20/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("08/20/2020", "11:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //Add new engagement with engagement date '08/20/2020'
        ClientInformation.clickOnAddNewEngagement();
        ClientInformation.selectProvider("BRC");
        ClientInformation.fillContactDetails("08/20/2020", "01:00 PM", "Outreach Contact");
        ClientInformation.fillShiftDetails("BRC", "Bronx", "Evening", "A", "1", "231 St", "No");
        ClientInformation.selectEngagementOutcome("Refused Services/Engagement");
        ClientInformation.setEngagementNotes("Test");
        ClientInformation.saveEngagement();

        //This method will change engagement date for first record
        boolean isUpdated = ClientInformation.changeEngagementDateForRow(0, "7/18/2020");
        Assert.assertTrue(isUpdated, "Engagement date can not be changed");

        //To verify that notification is displayed
        boolean isAutoflipNotificationDisplayed = ClientInformation.verifyClientCategoryChangedToOtherProspectSuccessMessageIsDisplayed();
        Assert.assertTrue(isAutoflipNotificationDisplayed, "Message 'Client Category has been updated to Other Prospect' is not displayed on changing engagement date outside 30 days");

        //TO fetch notification text and verify that text matches expected text
        String expectedNotificationText = "Client Category has been updated to Other Prospect";
        String actualNotificationText = ClientInformation.getAutoflipNotificationText();
        Assert.assertTrue(expectedNotificationText.equals(actualNotificationText), "When client category is changed, expected notification message '" + expectedNotificationText + "' is not displayed");

        //This method will change engagement date for lst record to make client category back to Engaged Unsheltered Prospect
        isUpdated = ClientInformation.changeEngagementDateForRow(2, "8/20/2020");
        Assert.assertTrue(isUpdated, "Engagement date can not be changed");

        //This will change Engagement Provider to 'BW' for first engagement
        boolean isChanged = ClientInformation.changeEngagementProvider(0);
        Assert.assertTrue(isChanged, "Provider can not be changed to 'BW'");

        //To verify that notification is displayed
        isAutoflipNotificationDisplayed = ClientInformation.verifyClientCategoryChangedToOtherProspectSuccessMessageIsDisplayed();
        Assert.assertTrue(isAutoflipNotificationDisplayed, "Autoflip success message 'Client Category has been updated to Other Prospect' is not displayed on changing the provider");

        //TO fetch notification text and verify that text matches expected text
        actualNotificationText = ClientInformation.getAutoflipNotificationText();
        Assert.assertTrue(expectedNotificationText.equals(actualNotificationText), "When client category is changed, expected notification message '" + expectedNotificationText + "' is not displayed");

        //This will change Engagement Provider to 'BRC' for first engagement to make client category back to Engaged Unsheltered Prospect
        isChanged = ClientInformation.changeEngagementProviderToBRC(0);
        Assert.assertTrue(isChanged, "Provider can not be changed to 'BRC'");

        //Navigate to client information tab and then bac to engagements tab to avoid cache issue which is blocking notification to appear
        ClientInformation.clickOnClientInformationTab();
        ClientInformation.clickOnEngagementsTab();

        //Deleting first engagement
        ClientInformation.deleteEnagagementRecord(0);

        //To verify that notification is displayed
        isAutoflipNotificationDisplayed = ClientInformation.verifyClientCategoryChangedToOtherProspectSuccessMessageIsDisplayed();
        Assert.assertTrue(isAutoflipNotificationDisplayed, "Autoflip success message 'Client Category has been updated to Other Prospect' is not displayed on deleting the engagement");

        //TO fetch notification text and verify that text matches expected text
        actualNotificationText = ClientInformation.getAutoflipNotificationText();
        Assert.assertTrue(expectedNotificationText.equals(actualNotificationText), "When client category is changed, expected notification message '" + expectedNotificationText + "' is not displayed");
    }

    @Test()
    public static void VerifyTravelAssistanceFunctionality() throws IOException, AWTException {
        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Open first Client details
        ClientList.clickonFirstClientID();

        //Click on Travel Assistance Program tab
        TravelAssistanceProgramTab.clickOnTravelAssistanceProgramTab();

        //Delete all Travel Assistance records
        TravelAssistanceProgramTab.deleteAllTravelAssistanceRecords();

        //Create New File
        String filePath = Paths.get("target", RandomString.make(5) + ".pdf").toAbsolutePath().toString();
        Helpers.createEmptyFile(filePath);

        //Create new Travel Assistance
        TravelAssistanceProgramTab.createNewTravelAssistance("Client Expense", "Employment Offer Letter,Financial Support Provided", "Test", "Nepal", "TestState", "TestCity", "10/25/2020", "1000", "Air", filePath);

        //Verify that success message is displayed after new record is added
        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");

        //Click on Action and then View Details for first record
        TravelAssistanceProgramTab.viewTravelAssistanceRecord(0);

        //Verify all the details saved are reflected
        String unmatchedLabels = TravelAssistanceProgramTab.verifyTravelAssistanceDetails("Client Expense", "Employment Offer Letter, Financial Support Provided", "Test", "Nepal", "TestState", "TestCity", "10/25/2020", "1000", "Air", filePath);
        Assert.assertTrue(unmatchedLabels.trim().isEmpty(), "Either no values are displayed or mismatch found for labels " + unmatchedLabels);

        //TODO Uncomment once file upload issue is resolved
        /*
        //Get files count before download
        int filesBefore = Helpers.countFilesInDownloadFolder();
        //Click on file link
        TravelAssistanceProgramTab.clickOnPassportDocumentLink();
        //Get File count in download folder after
        int filesAfter = Helpers.countFilesInDownloadFolder();
        //Verify that file count has increased
        Assert.assertTrue(filesBefore+1 == filesAfter,"On clicking file link, the file should get downloaded");*/

        //Click on Edit button
        TravelAssistanceProgramTab.clickOnEditTravelAssistance();

        //Fill updated details for Destination section
        TravelAssistanceProgramTab.fillDestinationDetails("UpdatedName", "Mali", "UpdtState", "UpdatedCity");

        //Click on Save button
        TravelAssistanceProgramTab.clickOnSaveTravelAssistance();

        //Verify that success message is displayed after new record is added
        isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");

        //Click on Action and then View Details for first record
        TravelAssistanceProgramTab.viewTravelAssistanceRecord(0);

        //Verify updated values are displayed for 'Name' field
        boolean isNmaeUpdated = TravelAssistanceProgramTab.verifyValueForLabel("Name", "UpdatedName");
        Assert.assertTrue(isNmaeUpdated, "Details not updated for 'Name' field");

        //Verify updated values are displayed for 'State' field
        boolean isStateUpdated = TravelAssistanceProgramTab.verifyValueForLabel("State", "UpdtState");
        Assert.assertTrue(isStateUpdated, "Details not updated for 'State' field");

        //Verify updated values are displayed for 'Country' field
        boolean isCountryUpdated = TravelAssistanceProgramTab.verifyValueForLabel("Country", "Mali");
        Assert.assertTrue(isCountryUpdated, "Details not updated for 'Country' field");

        //Verify updated values are displayed for 'City' field
        boolean isCityUpdated = TravelAssistanceProgramTab.verifyValueForLabel("City", "UpdatedCity");
        Assert.assertTrue(isCityUpdated, "Details not updated for 'City' field");

        //Click on Cancel button
        TravelAssistanceProgramTab.clickOnCancelTravelAssistanceButton();

        //Delete first Assistance Record
        TravelAssistanceProgramTab.deleteTravelAssistanceRecord(0);

        //Verify that after deleting one and only record, there should be no records displayed for Travel Assistance
        int noOfTravelAssistanceRecords = TravelAssistanceProgramTab.getTravelAssistanceRecordCount();
        Assert.assertTrue(noOfTravelAssistanceRecords == 0, "Travel assistance record is not deleted");
    }

    @Test()
    public static void VerifyDocumentsTabFunctionality() throws IOException, AWTException {
        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Open first Client details
        ClientList.clickonFirstClientID();

        //Navigate to Documents tab
        DocumentsTab.clickOnDocumentsTab();

        //Delete all existing documents
        DocumentsTab.deleteAllDocuments();

        //Add a document if not present any, so that 'Add New' button is displayed
        DocumentsTab.addDocumentRecordIfEmpty();

        //Get documents count before adding a new
        int documentsCountBefore = DocumentsTab.getNoOfDocumentRecords();

        //Click on Add new document
        DocumentsTab.clickOnAddNewDocumentButton();
        //Select Document Status
        DocumentsTab.selectDocumentStatus("Documents Applying For");
        //Select Document Type
        DocumentsTab.selectDocumentType("Benefit Card");
        //Set Notes text
        DocumentsTab.setDocumentNotes("Automation Testing");
        //Create New File
        String filePath = Paths.get("target", RandomString.make(5) + ".pdf").toAbsolutePath().toString();
        Helpers.createEmptyFile(filePath);
        //TODO Uncomment below code when file upload is working
        //Upload the document
        //DocumentsTab.uploadDocument(filePath);
        //Save document details
        DocumentsTab.clickOnSaveButton();

        //Get documents count after adding new document and verify that count is increased by 1
        int documentsCountAfter = DocumentsTab.getNoOfDocumentRecords();
        Assert.assertTrue(documentsCountAfter == documentsCountBefore + 1, "New document record is not added in table");

        //Verify details saved for 'Document Status'
        boolean isAsExpected = DocumentsTab.verifyDocumentStatusIs("Documents Applying For");
        Assert.assertTrue(isAsExpected, "Expected value is not displayed for 'Document Status'");

        //Verify details saved for 'Document Type'
        isAsExpected = DocumentsTab.verifyDocumentTypeIs("Benefit Card");
        Assert.assertTrue(isAsExpected, "Expected value is not displayed for 'Document Type'");

        //Verify details saved for 'Document Category'
        isAsExpected = DocumentsTab.verifyDocumentCategoryIs("ID");
        Assert.assertTrue(isAsExpected, "Expected value is not displayed for 'Document Category'");

        //Verify details saved for 'Notes'
        isAsExpected = DocumentsTab.verifyNotesIs("Automation Testing");
        Assert.assertTrue(isAsExpected, "Expected value is not displayed for 'Notes'");

        //Get logged in user name and verify details reflected for Modified By column
        String loggedInByUser = Base.returnLoggedInByUser();
        isAsExpected = DocumentsTab.verifyModifiedByIs(loggedInByUser);
        Assert.assertTrue(isAsExpected, "Expected value is not displayed for 'Modified By'");

        //TODO Uncomment below code when file upload functionality is working
        /*//Verify details saved for 'File'
        isAsExpected = DocumentsTab.verifyFileNameIs(filePath);
        Assert.assertTrue(isAsExpected,"Expected value is not displayed for 'File'");

        //Get files count before download
        int filesBefore = Helpers.countFilesInDownloadFolder();
        //Click on file link
        DocumentsTab.clickOnUploadedFileLink();
        //Get File count in download folder after
        int filesAfter = Helpers.countFilesInDownloadFolder();
        //Verify that file count has increased
        Assert.assertTrue(filesBefore+1 == filesAfter,"On clicking file link, the file should get downloaded");*/

        //Edit document record and change Document Type value and save
        DocumentsTab.clickOnEditDocument();
        //Change the document type to Passport
        DocumentsTab.selectDocumentType("Passport");
        //Save updated data
        DocumentsTab.clickOnSaveButton();

        //Verify that updated value is reflected for 'Document Type'
        isAsExpected = DocumentsTab.verifyDocumentTypeIs("Passport");
        Assert.assertTrue(isAsExpected, "Expected value is not displayed for 'Document Type'");

        //Delete first document record
        DocumentsTab.deleteDocument(0);

        //Get record count after deleting a record and verify that records count is decreased by 1
        int deleteCountAfterDeletion = DocumentsTab.getNoOfDocumentRecords();
        Assert.assertTrue(deleteCountAfterDeletion == documentsCountAfter - 1, "New document record is not added in table");
    }

    @Test
    public static void VerifyClientCaseTabFunctionality() throws ParseException {
        String serviceType = "Outreach";
        String caseType = "Single Adult";
        String startDate = Helpers.getCurrentDate("MM/dd/yyyy");
        startDate = Helpers.addDays(startDate,"MM/dd/yyyy",-3);
        String caseProvider = "BronxWorks";
        String borough = "Bronx";
        String noteText = "This is automation testing";

        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Filter only Caseload clients
        DailyOutreach.advancedFilterBy("ClientCategory", "Caseload", true);

        //Click on first client id
        ClientList.clickonFirstClientID();

        //Navigate to Case Information tab
        CaseInformationTab.clickOncaseInformationTab();

        //THis will make Outreach cases visible
        CaseInformationTab.selectType("Outreach");

        //Change case status to Close for any existing Outreach case
        CaseInformationTab.changeCaseStatusToClose();

        //This method will delete all cases except last one
        CaseInformationTab.deleteAllCases();

        //This will save existing case count
        int casesBefore = CaseInformationTab.getCaseCount();

        //Click on Add New if existing case count is non-zero
        if (casesBefore != 0)
            CaseInformationTab.clickOnAddNewButton();
        //Select service type
        CaseInformationTab.selectServiceType(serviceType);
        //Select case type
        CaseInformationTab.selectCaseType(caseType);
        //Select case start date
        CaseInformationTab.selectCaseStartDate(startDate);
        //Select Provider
        CaseInformationTab.selectCaseProvider(caseProvider);
        //Select Brough
        CaseInformationTab.selectCaseBorough(borough);
        //Expand Note section
        CaseInformationTab.clickOnNotesSection();
        //Add new note
        CaseInformationTab.addNewNote(noteText);

        //Click on Save button
        CaseInformationTab.selectSaveCaseInformation();

        //Verify that success message is displayed after new record is added
        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");

        //Get case cout after adding new case
        int casesAfter = CaseInformationTab.getCaseCount();
        Assert.assertTrue(casesBefore + 1 == casesAfter, "A new case has not been added into table");

        //Get Case No  and verify that it's not empty
        String caseNo = CaseInformationTab.getCaseNo();
        Assert.assertTrue(!caseNo.isEmpty(), "SS Case No is not reflected in table");

        //Get case start date and compare it with expected start date
        String actStartDate = CaseInformationTab.getCaseStartDate();
        Assert.assertTrue(startDate.equalsIgnoreCase(actStartDate), "Start date is not reflected as expected in table");

        //Get service type and compare it wth expected service type
        String actServiceType = CaseInformationTab.getServiceType();
        Assert.assertTrue(serviceType.equalsIgnoreCase(actServiceType), "Service type is not reflected as expected in table");

        //Gte case status and verify that it's 'Open'
        String actCaseStatus = CaseInformationTab.getCaseStatus();
        Assert.assertTrue(("Open").equalsIgnoreCase(actCaseStatus), "Case status is not reflected as expected in table");

        //Get case type and compare it with expected case type
        String actCaseType = CaseInformationTab.getCaseType();
        Assert.assertTrue(caseType.equalsIgnoreCase(actCaseType), "Case type is not reflected as expected in table");

        //Get Provider and compare it with expected provider
        String actProvider = CaseInformationTab.getProvider();
        Assert.assertTrue(caseProvider.equalsIgnoreCase(actProvider), "Provider is not reflected as expected in table");

        //Get Brough and compare it with actual brough
        String actBorough = CaseInformationTab.getBorough();
        Assert.assertTrue(borough.equalsIgnoreCase(actBorough), "Borough is not reflected as expected in table");

        //Get Modified By date and compare it with logged in user name
        String expectedModifiedBy = Base.returnLoggedInByUser();
        String actModifiedBy = CaseInformationTab.getModifiedBy();
        Assert.assertTrue(actModifiedBy.equalsIgnoreCase(expectedModifiedBy), "Modified By is not reflected as expected in table");

        //Search with case no and verify that search result is displayed with case no
        CaseInformationTab.searchFor(caseNo);
        boolean isDisplayed = CaseInformationTab.isCaseNoDisplayedAfterSearch(caseNo);
        Assert.assertTrue(isDisplayed, "Case No is not displayed after search");

        //Search with blank value
        CaseInformationTab.searchFor("");

        //Expand Changelog section
        CaseInformationTab.expandChangelog();
/*
        //TODO Existing defect
        //Get Updated By name from log table and compare it with expected value
        String updatedBy = CaseInformationTab.getUpdatedByFromLog();
        Assert.assertTrue(expectedModifiedBy.equals(updatedBy), "Updated By is not displayed as expected in log table");

        //Get Action taken from log table and verify that value is 'Insert'
        String actionTaken = CaseInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Insert".equals(actionTaken), "Action taken is not updated as expected in log table");

        //Ge Service Type from log table and compare it with expected value
        actServiceType = CaseInformationTab.getServiceTypeFromLog();
        Assert.assertTrue(serviceType.equals(actServiceType), "Service Type is not reflected in log table");

        //Get Provider from log table and compare it with expected value
        actProvider = CaseInformationTab.getProviderFromLog();
        Assert.assertTrue(caseProvider.equals(actProvider), "Service Provider is not reflected as expected in log table");

        //Get Borough from log table and compare it with expected value
        actBorough = CaseInformationTab.getBoroughFromLog();
        Assert.assertTrue(actBorough.equals(borough), "Borough is not displayed as expected in log table");

        //Get Start date from log table and compare it with expected valu
        actStartDate = CaseInformationTab.getStartDateFromLog();
        Assert.assertTrue(startDate.equals(actStartDate), "Start Date is not displayed as expected in log table");
*/

        //Click on Action > View Details for first record in table
        CaseInformationTab.viewCaseInformationRecord(0);

        //Verify expected Case details onn case details page
        String unmatchedLabels = CaseInformationTab.verifyCaseInformationData(serviceType, caseType, startDate, "Open", caseProvider, borough);
        Assert.assertTrue(unmatchedLabels.isEmpty(), "Unmatched values found for " + unmatchedLabels);

        //Verify that note is displayed as expected
        boolean isMatching = CaseInformationTab.isNoteAsExpected(noteText);
        Assert.assertTrue(isMatching, "Note text is not as expected");

        //Edit case details
        CaseInformationTab.editCaseDetails("", "", "", "", "Breaking Ground", "Queens");

        //Expand Change log section
        CaseInformationTab.expandChangelog();

/*         TODO : Existing defect
        //Verify that Action status for first record is 'Update'
        actionTaken = CaseInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Update".equals(actionTaken), "Action taken is not updated as expected in log table");

        //Verify that updated Provider is displayed in log table
        actProvider = CaseInformationTab.getProviderFromLog();
        Assert.assertTrue("Breaking Ground".equals(actProvider), "Service Provider is not reflected as expected in log table");

        //Verify that updated Borough is displayed in log table
        actBorough = CaseInformationTab.getBoroughFromLog();
        Assert.assertTrue(actBorough.equals("Queens"), "Borough is not displayed as expected in log table");
*/
        //Click on Action > View Details for first record
        CaseInformationTab.viewCaseInformationRecord(0);

        //Verify that updated values are displayed for Case Information
        unmatchedLabels = CaseInformationTab.verifyCaseInformationData(serviceType, caseType, startDate, "Open", "Breaking Ground", "Queens");
        Assert.assertTrue(unmatchedLabels.isEmpty(), "Unmatched values found for " + unmatchedLabels);

        //Edit note and verify that it is reflected
        CaseInformationTab.editFirstNote("This is updated note");
        isMatching = CaseInformationTab.isNoteAsExpected("This is updated note");
        Assert.assertTrue(isMatching, "Note text is not updated as expected");

        //Delete note and verify that count is reduced by one
        int notesBefore = CaseInformationTab.getNotesCount();
        CaseInformationTab.deleteNote(0);
        int notesAfter = CaseInformationTab.getNotesCount();
//        Assert.assertTrue(notesAfter == notesBefore-1,"Notes has not been deleted");

        //Click Cancel button to return to case details table
        CaseInformationTab.clickOnCancelOnEditPage();

        //Delete first case record and verify that count is same as the count before the case was created
        CaseInformationTab.deleteCaseRecord(0);
        casesAfter = CaseInformationTab.getCaseCount();
        Assert.assertTrue(casesAfter == casesBefore, "Case has not been deleted successfully");

        //Search for case no, which is deleted and verify that after search it's not displayed
        CaseInformationTab.searchFor(caseNo);
        isDisplayed = CaseInformationTab.isCaseNoDisplayedAfterSearch(caseNo);
        Assert.assertTrue(!isDisplayed, "Case No is displayed after search, even after deletion of case");

        //Expand change log section
        CaseInformationTab.expandChangelog();
/*      TODO : Existing defect
        //Verify that Action taken value is 'Delete' for the first record in Change Log
        actionTaken = CaseInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Delete".equals(actionTaken), "Action taken is not updated as expected in log table");
   */
    }

    @Test
    public static void VerifyHealthInformationTabFunctionality() {
        String medicalCoverage = "Yes";
        String typeOfCoverage = "Medicaid";
        String mitoa = "Mental Illness";
        String medicalInfo = "Taking Medicine";
        String medicationText = "Taking medicine";
        String noteText = "This is automation testing";

        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();

        //Click on first client id
        ClientList.clickonFirstClientID();

        //Get Provider name from Client Information tab
        String providerShortName = HealthInformationTab.getProviderName();

        //Navigate to Health Information Tab
        HealthInformationTab.clickOnHealthInformationTab();
        //Delete all health information records
        HealthInformationTab.deleteAllHealthInfoRecords();

        //Get health information records
        int healthInfoRecordsBefore = HealthInformationTab.getNoOfRecords();
        //Click on Add New button
        HealthInformationTab.clickOnAddNewButton();

        //Select Provider
        HealthInformationTab.selectProvider(providerShortName);
        //Select Medical Coverage
        HealthInformationTab.selectMedicalCoverage(medicalCoverage);
        //Select type of coverage
        HealthInformationTab.selectTypeofCoverage(typeOfCoverage);

        //Provide assesment and screening details
        //HealthInformationTab.submitAssessmentAndScreeningDetails("Psychosocial","Yes","10/05/2020","Automation","","","09:00 AM","11:30 AM");

        //Expand Abuse details section
        HealthInformationTab.expandAbuseDetailsSection();
        //Select Mental Illness and Substance Abuse
        HealthInformationTab.selectMentalIllnessAndSubstanceAbuse(mitoa);

        //Expand Medication Section
        HealthInformationTab.expandMedicationSection();
        //Select Medical Information
        HealthInformationTab.selectMedicalInformation(medicalInfo);
        //Set medication text
        HealthInformationTab.setMedicationText(medicationText);

        //Expand Note section
        CaseInformationTab.clickOnNotesSection();
        //Add new note
        CaseInformationTab.addNewNote(noteText);

        //Save Health information
        HealthInformationTab.clickSaveHealthInformationButton();

        //Verify that success message is displayed after new record is added
        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");

        //Get health information record count
        int healthInfoRecordsAfter = HealthInformationTab.getNoOfRecords();
        Assert.assertTrue(healthInfoRecordsBefore + 1 == healthInfoRecordsAfter, "New health information record has not been added");

        //Get Provider name from table and verify that it matches with expected value
        String actualProvider = HealthInformationTab.getProviderFromTable();
        Assert.assertTrue(providerShortName.equals(actualProvider), "Case Provider name is not reflected as expected on health information record");

        //Get Medical record no and verify that it's not blank
        String actualMedicalRecordNo = HealthInformationTab.getMedicalRecordNoFromTable();
        Assert.assertTrue(!actualMedicalRecordNo.replace("-", "").isEmpty(), "Medical record is blank");

        //Get Health coverage value and verify that it matches with expected value
        String actualHealthCoverage = HealthInformationTab.getHealthCoverageFromTable();
        Assert.assertTrue(medicalCoverage.equals(actualHealthCoverage), "Unexpected health coverage value displayed in table");

        //Get Medical coverage type value from table and verify that it matches with expected value
        String actualCoverageType = HealthInformationTab.getCoverageType();
        Assert.assertTrue(typeOfCoverage.equals(actualCoverageType), "Unexpected coverage type is displayed in table");

        //Get Illness and Abuse value from table and verify that it matches with expected value
        String actualMia = HealthInformationTab.getIllnessAndAbuseFromTable();
        Assert.assertTrue(mitoa.equals(actualMia), "Unexpected value found for medical illness and substance abuse");

        //Get Modified By value from table and verify that it matches with logged in username
        String actualModifiedBy = HealthInformationTab.getModifiedByFromTable();
        String expectedModifiedBy = Base.returnLoggedInByUser();
        Assert.assertTrue(actualModifiedBy.equals(expectedModifiedBy), "'Modified By' is not displayed as expected");

        //Expand Changelog section
        CaseInformationTab.expandChangelog();

        //Get Updated By name from log table and compare it with expected value
        String updatedBy = CaseInformationTab.getUpdatedByFromLog();
        Assert.assertTrue(expectedModifiedBy.equals(updatedBy), "Updated By is not displayed as expected in log table");

        //Get Action taken from log table and verify that value is 'Insert'
        String actionTaken = CaseInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Insert".equals(actionTaken), "Action taken is not updated as expected in log table");

        //Click on Action > View Details for the first information record
        HealthInformationTab.viewHealthInformationRecord(0);

        //Get Provider value from form and verify that it matches with expected value
        actualProvider = HealthInformationTab.getTextForField("*Provider");
        Assert.assertTrue(providerShortName.equals(actualProvider), "Case Provider name is not reflected as expected on health information record");

        //Get Medical coverage value from form and verify that it matches with expected value
        actualHealthCoverage = HealthInformationTab.getTextForField("Medical Coverage");
        Assert.assertTrue(medicalCoverage.equals(actualHealthCoverage), "Unexpected medical coverage value displayed");

        //Get Type of coverage value from form and verify that it matches with expected value
        actualCoverageType = HealthInformationTab.getTextForField("*Type of Coverage");
        Assert.assertTrue(typeOfCoverage.equals(actualCoverageType), "Unexpected coverage type is displayed");

        //Expand abuse detail section
        HealthInformationTab.expandAbuseDetailsSection();

        //Get Mental Illness and substance abuse value from form and verify that it matches with expected value
        actualMia = HealthInformationTab.getTextForField("Mental Illness & Substance Abuse");
        Assert.assertTrue(mitoa.equals(actualMia), "Unexpected value found for medical illness and substance abuse");

        //Expand Medication section
        HealthInformationTab.expandMedicationSection();

        //Get Medication Information value from form and verify that it matches with expected value
        String actualMedicationInfo = HealthInformationTab.getTextForField("Medication Info");
        Assert.assertTrue(medicalInfo.equals(actualMedicationInfo), "Unexpected value found for medication info");

        //Get Medication value from form and verify that it matches with expected value
        String actualMedications = HealthInformationTab.getTextForField("*Medications");
        Assert.assertTrue(medicationText.equals(actualMedications), "Unexpected value found for medication");

        //Verify that note is displayed as expected
        boolean isMatching = CaseInformationTab.isNoteAsExpected(noteText);
        Assert.assertTrue(isMatching, "Note text is not as expected");

        //Click on Edit button to edit Health information details
        HealthInformationTab.clickOnEditButton();
        //Update Medical coverage value to No
        HealthInformationTab.selectMedicalCoverage("No");
        //CLick on save button to save updated value
        HealthInformationTab.clickSaveHealthInformationButton();


        //Verify that success message is displayed after new record is added
        isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");
        //Get health coverage value from table and verify that it matches with expected updated value
        actualHealthCoverage = HealthInformationTab.getHealthCoverageFromTable();
        Assert.assertTrue(("No").equals(actualHealthCoverage), "Unexpected health coverage value displayed in table");

        //Get coverage type value from table and verify that it's empty as it's only required if Health coverage is 'Yes'
        actualCoverageType = HealthInformationTab.getCoverageType().replace("-", "");
        Assert.assertTrue(actualCoverageType.isEmpty(), "Coverage type should be blank in table");

        //Expand Change log section
        CaseInformationTab.expandChangelog();

        //Verify that Action status for first record is 'Update'
        actionTaken = CaseInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Update".equals(actionTaken), "Action taken is not updated as expected in log table");

        //Click on Action > View Details for the first information record
        HealthInformationTab.viewHealthInformationRecord(0);


        //Get Medical coverage value from form and verify that it matches with updated value
        actualHealthCoverage = HealthInformationTab.getTextForField("Medical Coverage");
        Assert.assertTrue("No".equals(actualHealthCoverage), "Unexpected medical coverage value displayed");

        //Edit note and verify that it is reflected
        CaseInformationTab.editFirstNote("This is updated note");
        isMatching = CaseInformationTab.isNoteAsExpected("This is updated note");
        Assert.assertTrue(isMatching, "Note text is not updated as expected");

        //Delete note and verify that count is reduced by one
        int notesBefore = CaseInformationTab.getNotesCount();
        CaseInformationTab.deleteNote(0);
        int notesAfter = CaseInformationTab.getNotesCount();
        //Assert.assertTrue(notesAfter == notesBefore-1,"Notes has not been deleted");

        //Click on cancel button to return to Health Information records table
        HealthInformationTab.clickOnCancelButton();

        //Delete first case record and verify that count is same as the count before the case was created
        HealthInformationTab.deleteHealthRecord(0);

        healthInfoRecordsAfter = HealthInformationTab.getNoOfRecords();
        Assert.assertTrue(healthInfoRecordsAfter == healthInfoRecordsBefore, "Record has not been deleted successfully");

        //Expand change log section
        CaseInformationTab.expandChangelog();

        //Verify that Action taken value is 'Delete' for the first record in Change Log
        actionTaken = CaseInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Delete".equals(actionTaken), "Action taken is not updated as expected in log table");
    }

    @Test
    public static void VerifyPlacementInformationTab() throws ParseException {
        //Pre-requisite values
        String placementType = "Permanent Placement";
        String noteText = "Testing Placement information";
        String newDescription = "Job Corps";
        String newCity = "New Jersey";
        //Required data is saved in hashmap to simplify
        HashMap<String,String> data = new HashMap<>();
        data.put("Description","Adult Home/Care Facility");
        data.put("CaseIndex","1");
        data.put("StartDate",Helpers.getCurrentDate("MM/dd/yyyy"));
        data.put("EndDate","");
        data.put("PermanentPlacementKnowledge","Known"); // It can contain values "","Known","Unknown"
        data.put("BuildingNo","12");
        data.put("StreetName","Automation");
        data.put("City","New York");
        data.put("State","NE");
        data.put("Zip","33333");
        data.put("County","County");

        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();
        WebDriverTasks.wait(3);

        //Filter only Caseload clients
        DailyOutreach.advancedFilterBy("ClientCategory", "Caseload", true);
        WebDriverTasks.wait(3);

        //Click on first client id
        ClientList.clickonFirstClientID();

        //Click on Placements Information Tab
        PlacementInformationTab.clickOnPlacementsTab();
        WebDriverTasks.wait(3);

        //Delete all existing placement records
        PlacementInformationTab.deleteAllPlacementRecords();
        WebDriverTasks.wait(3);

        //Click on Add new placement button
        PlacementInformationTab.clickOnAddNewPlacementTab();
        //Select Placement Type
        PlacementInformationTab.selectPlacementType(placementType);
        //Provide all details clubbed in hashmap
        PlacementInformationTab.fillPlacementFormDetails(data);
        WebDriverTasks.wait(3);
        //Expand Note section
        CaseInformationTab.clickOnNotesSection();
        //Add new note
        CaseInformationTab.addNewNote(noteText);
        WebDriverTasks.wait(3);
        //Click on Save button to save Placement Information record
        PlacementInformationTab.clickOnSaveButton();


        //Verify that success message is displayed after new record is added
//        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
//        Assert.assertTrue(isSaved, "Success message is not displayed");
        WebDriverTasks.wait(3);

        //Click on cancel button on Housing search information tab
        PlacementInformationTab.clickOnCancelButton();
        WebDriverTasks.wait(3);
        //Click on Yes to confirm that we want to proceed without saving changes
        PlacementInformationTab.clickYesOnPopup();

        //Navigate back to Placement Information tab
        PlacementInformationTab.clickOnPlacementsTab();
        WebDriverTasks.wait(3);

        //Get Placement information record count and verify that it's value is 1
        int recordCount = HealthInformationTab.getNoOfRecords();
        Assert.assertTrue( 1 == recordCount, "New Placement information record has not been added");

        //Get 'SS Case no' from table and verify that it's not empty
        String actualSSCaseNo = PlacementInformationTab.getCaseNoFromTable();
        Assert.assertTrue(!actualSSCaseNo.isEmpty(),"SS NO should not be blank in Placement information table");

        //Get 'Placement Type' value from table and verify that it matches with expected value
        String actualPlacementType = PlacementInformationTab.getPlacementTypeFromTable();
        Assert.assertTrue( placementType.equals(actualPlacementType) ,"Placement type is not matching with expected value in Placement information table");

        //Get 'Placement description' value from table and verify that it matches with expected value
        String actualPlacementDescription = PlacementInformationTab.getPlacementDescriptionFromTable();
        Assert.assertTrue( data.get("Description").equals(actualPlacementDescription) ,"Placement description is not matching with expected value in Placement information table");

        //Get 'Placement start date' value from table and verify that it matches with expected value
        String actualStartDate = PlacementInformationTab.getPlacementStartDateFromTable();
        Assert.assertTrue( data.get("StartDate").equals(actualStartDate) ,"Placement start date is not matching with expected value in Placement information table");

        //Get 'Length Of Stay' value from table and verify that it's value is 1
        String actualLengthOfStay = PlacementInformationTab.getLengthOfStayFromTable();
        Assert.assertTrue( "1".equals(actualLengthOfStay) ,"Length of stay is not matching with expected value in Placement information table");

        //Get 'Modified By' value from table and verify that it's value is same as logged in user
        String actualModifiedBy = PlacementInformationTab.getModifiedByFromTable();
        String expectedModifiedBy = Base.returnLoggedInByUser();
        Assert.assertTrue( expectedModifiedBy.equals(actualModifiedBy) ,"Modified By is not matching with expected value in Placement information table");
        WebDriverTasks.wait(3);

        //Expand Changelog section
        CaseInformationTab.expandChangelog();
        WebDriverTasks.wait(3);

        //Get Updated By name from log table and compare it with expected value
        String updatedBy = CaseInformationTab.getUpdatedByFromLog();
        Assert.assertTrue(expectedModifiedBy.equals(updatedBy), "Updated By is not displayed as expected in log table");

        //Get Action taken from log table and verify that value is 'Insert'
        String actionTaken = CaseInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Insert".equals(actionTaken), "Action taken is not updated as expected in log table");
        WebDriverTasks.wait(3);

        //Click on Action > View Details for first record
        PlacementInformationTab.viewRecord(0);
        WebDriverTasks.wait(3);

        //Get value of 'Placement Level' field from Placement Information page
        String actualPlacementLevel = PlacementInformationTab.getTextForField("Placement Level");
        Assert.assertTrue(!actualPlacementLevel.isEmpty(),"'Placement Level' should not be empty on placement information page");

        //Get value of 'Placement Description' field from Placement Information page
        actualPlacementDescription = PlacementInformationTab.getTextForField("Placement Description");
        Assert.assertTrue(data.get("Description").equals(actualPlacementDescription),"Unexpected value displayed for 'Placement Description' field on placement information page");

        //Get value of 'Case No' field from Placement Information page
        String actualCaseNo = PlacementInformationTab.getTextForField("Case No");
        Assert.assertTrue(!actualCaseNo.isEmpty(),"'Case No' should not be empty on Placement Information screen");

        //Get value of 'Provider' field from Placement Information page
        String actualProvider = PlacementInformationTab.getTextForField("Provider");
        Assert.assertTrue(!actualProvider.isEmpty(),"'Provider' should not be empty on placement information page");

        //Get value of 'Placement Start Date' field from Placement Information page
        actualStartDate  = PlacementInformationTab.getTextForField("Placement Start Date");
        Assert.assertTrue(data.get("StartDate").equals(actualStartDate),"Unexpected value displayed for 'Placement Start Date' field on placement information page");

        //Get value of 'Placement End Date' field from Placement Information page
        String actualEndDate  = PlacementInformationTab.getTextForField("Placement End Date");
        Assert.assertTrue(data.get("EndDate").equals(actualEndDate),"Unexpected value displayed for 'Placement End Date' field on placement information page");

        //Get value of 'Building No' field from Placement Information page
        String actualBuildingNo = PlacementInformationTab.getTextForField("Building No.");
        Assert.assertTrue(data.get("BuildingNo").equals(actualBuildingNo),"Unexpected value displayed for 'Building No' field on placement information page");

        //Get value of 'Street Name' field from Placement Information page
        String actualStreetName = PlacementInformationTab.getTextForField("Street Name");
        Assert.assertTrue(data.get("StreetName").equals(actualStreetName),"Unexpected value displayed for 'Street Name' field on placement information page");

        //Get value of 'City' field from Placement Information page
        String actualCity = PlacementInformationTab.getTextForField("City");
        Assert.assertTrue(data.get("City").equals(actualCity),"Unexpected value displayed for 'City' field on placement information page");

        //Get value of 'State' field from Placement Information page
        String actualState = PlacementInformationTab.getTextForField("State");
        Assert.assertTrue(data.get("State").equals(actualState),"Unexpected value displayed for 'State' field on placement information page");

        //Get value of 'Zip' field from Placement Information page
        String actualZip = PlacementInformationTab.getTextForField("Zip");
        Assert.assertTrue(data.get("Zip").equals(actualZip),"Unexpected value displayed for 'Zip' field on placement information page");

        //Get value of 'County' field from Placement Information page
        String actualCountry = PlacementInformationTab.getTextForField("County");
        Assert.assertTrue(data.get("County").equals(actualCountry),"Unexpected value displayed for 'County' field on placement information page");

        //Verify that note is displayed as expected
        boolean isMatching = CaseInformationTab.isNoteAsExpected(noteText);
        Assert.assertTrue(isMatching, "Note text is not as expected");
        WebDriverTasks.wait(5);

        //Edit Placement information
        PlacementInformationTab.clickOnEditButton();
        WebDriverTasks.wait(3);

        //Update placement description value
        PlacementInformationTab.selectPlacementDescription(newDescription);
        WebDriverTasks.wait(3);

        //Update city
        PlacementInformationTab.setCity(newCity);

        //Save updated values
        PlacementInformationTab.clickOnSaveButton();
        WebDriverTasks.wait(3);

        //Click on 'Cancel' button on Housing Search Page
        PlacementInformationTab.clickOnCancelButton();
        //Click on 'Yes' button to confirm that we don't want to save information
        PlacementInformationTab.clickYesOnPopup();
        //Navigate back to Placement Information tab
        PlacementInformationTab.clickOnPlacementsTab();
        WebDriverTasks.wait(3);

        //Verify that updated 'Placement Description' value is displayed in Placement Information table
        actualPlacementDescription = PlacementInformationTab.getPlacementDescriptionFromTable();
        Assert.assertTrue( newDescription.equals(actualPlacementDescription) ,"Updated 'Placement description' is not matching with expected value in Placement information table");

        //Expand Changelog section
        CaseInformationTab.expandChangelog();
        WebDriverTasks.wait(3);

        //Verify that Action status for first record is 'Update'
        actionTaken = CaseInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Update".equals(actionTaken), "Action taken is not updated as expected in log table");

        //Click on Action > View Details for the first record in table
        PlacementInformationTab.viewRecord(0);
        WebDriverTasks.wait(3);

        //Verify that updated 'Placement Description' value is displayed in Placement Information details page
        actualPlacementDescription = PlacementInformationTab.getTextForField("Placement Description");
        Assert.assertTrue(newDescription.equals(actualPlacementDescription),"Unexpected value displayed for 'Placement Description f'ield on placement information page");

        //Verify that updated 'City' value is displayed in Placement Information details page
        actualCity = PlacementInformationTab.getTextForField("City");
        Assert.assertTrue(newCity.equals(actualCity),"Unexpected value displayed for City field on placement information page");

        //Edit note and verify that it is reflected
        CaseInformationTab.editFirstNote("This is updated note");
        isMatching = CaseInformationTab.isNoteAsExpected("This is updated note");
        Assert.assertTrue(isMatching, "Note text is not updated as expected");
        WebDriverTasks.wait(3);

        //Delete note and verify that success message is displayed on deletion of note
        CaseInformationTab.deleteNote(0);
        boolean noteDeleted = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Note has been deleted successfully.");
        Assert.assertTrue(noteDeleted,"Success message is not displayed fo deleting a note");
        WebDriverTasks.wait(3);

        //Click on cancel button to navigate to Placement Information tab
        PlacementInformationTab.clickOnCancelButton();

        //Delete first placement record
        PlacementInformationTab.deletePlacementRecord(0);

        //Get Placement information record count
        recordCount = HealthInformationTab.getNoOfRecords();
        Assert.assertTrue( 0 == recordCount, "Placement information record has not been deleted");
        WebDriverTasks.wait(3);

        //Expand Changelog section
        CaseInformationTab.expandChangelog();

        //Verify that Action taken value is 'Delete' for the first record in Change Log
        actionTaken = CaseInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Delete".equals(actionTaken), "Action taken is not updated as expected in log table");
        WebDriverTasks.wait(3);
    }

    @Test
    public static void VerifyHousingSearchInformationTab(){
        //Created hashmap to store important data
        HashMap<String,String> data = new HashMap<>();
        data.put("Housing Search Status", "Awaiting Voucher");
        data.put("Housing Search Sub-status","Eligible for Voucher");
        data.put("Minimum Apt. Requirement", "1-bedroom");
        data.put("Voucher in-hand","15/15 Congregate");
        data.put("Borough Preference","Bronx,Brooklyn");
        data.put("Apt. Disability access","Blind accessible,Wheelchair accessible");

        String noteText = "This is Housing search testing";
        String modifiedBy;

        data.put("Apt. Size","1-bedroom");
        data.put("Apt. Source","Src");
        data.put("Street No.","1");
        data.put("Street Name","Test Street");
        data.put("State","AK");
        data.put("County","Denali");
        data.put("City","Clear");
        data.put("Zip Code","33333");
        data.put("Date of Viewing", Helpers.getCurrentDate("MM/dd/yyyy"));
        data.put("LL Outcome","Accepted");
        data.put("Client Outcome","Accepted");

        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();
        WebDriverTasks.wait(3);

        //Filter only Caseload clients
        DailyOutreach.advancedFilterBy("ClientCategory", "Caseload", true);
        WebDriverTasks.wait(3);

        //Click on first client id
        ClientList.clickonFirstClientID();

        //Get Provider name from Client Information tab
        data.put("Provider",HealthInformationTab.getProviderName());

        //Click on Housing Information tab
        HousingSearchInformationTab.clickOnHousingInformationTab();

        //Delete all housing search information records
        HousingSearchInformationTab.deleteAllHousingSearchRecords();
        WebDriverTasks.wait(3);

        //Click on Add new button
        HousingSearchInformationTab.clickOnAddNewButton();
        WebDriverTasks.wait(3);
        //Provide data for 'Provider' field
        HousingSearchInformationTab.selectProvider(data.get("Provider"));
        //Provide data for 'Housing Search Status' field
        HousingSearchInformationTab.selectHousingSearchStatus(data.get("Housing Search Status"));
        //Provide data for 'Housing Search Sub-status' field
        HousingSearchInformationTab.selectHousingSearchSubStatus(data.get("Housing Search Sub-status"));
        //Provide data for 'Minimum Apt. Requirement' field
        HousingSearchInformationTab.selectMinimumApartment(data.get("Minimum Apt. Requirement"));
        //Provide data for 'Borough Preference' field
        HousingSearchInformationTab.selectBorough(data.get("Borough Preference"));
        //Provide data for 'Voucher in-hand' field
        HousingSearchInformationTab.selectVoucherInHand(data.get("Voucher in-hand"));
        //Provide data for 'Apt. Disability access' field
        HousingSearchInformationTab.selectAptDisabilityAccess(data.get("Apt. Disability access"));

        //Add new apartment detal
        HousingSearchInformationTab.addNewApartmentDetail(data);
        WebDriverTasks.wait(3);

        //Expand Note section
        CaseInformationTab.clickOnNotesSection();
        //Add new note
        CaseInformationTab.addNewNote(noteText);
        WebDriverTasks.wait(3);

        //Save Housing search information details
        HousingSearchInformationTab.clickOnSaveButton();
        WebDriverTasks.wait(3);

        //Verify that success message is displayed after new record is added
        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");
        WebDriverTasks.wait(3);

        //Get No of Housing information records
        int recordCount = HousingSearchInformationTab.getNoOfRecords();
        Assert.assertTrue(recordCount==1,"Record count has not been increased");

        //Verify all saved data in Housing information table
        String exceptions = HousingSearchInformationTab.verifyDataInTable(data);
        Assert.assertTrue(exceptions.isEmpty(),"Mismatch found with data submitted and data displayed in table for : " + exceptions);

        //Return logged in username
        modifiedBy = Base.returnLoggedInByUser();
        data.put("Modified By",modifiedBy);

        //Expand Changelog section
        CaseInformationTab.expandChangelog();

        //Get Updated By name from log table and compare it with expected value
        String updatedBy = CaseInformationTab.getUpdatedByFromLog();
        Assert.assertTrue(modifiedBy.equals(updatedBy), "Updated By is not displayed as expected in log table");

        //Get Action taken from log table and verify that value is 'Insert'
        String actionTaken = HousingSearchInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Insert".equals(actionTaken), "Action taken is not updated as expected in log table");
        WebDriverTasks.wait(3);

        //Click on Action > View Details for the first record in Health information table
        HousingSearchInformationTab.viewHousingSearchInformationRecord(0);
        WebDriverTasks.wait(3);

        //Verify that the submitted data is displayed as is for Housing Search Status section
        exceptions = HousingSearchInformationTab.verifyHousingSearchStatusData(data);
        Assert.assertTrue(exceptions.isEmpty(),"Mismatch found with data submitted and data displayed in status section for : " + exceptions);

        //Verify that the submitted data is displayed as is for Apartment details
        exceptions = HousingSearchInformationTab.verifyApartmentDetails(data);
        Assert.assertTrue(exceptions.isEmpty(),"Mismatch found with data submitted and data displayed in apartment details section for : " + exceptions);

        //Click on Edit button
        HousingSearchInformationTab.clickOnEditButton();
        WebDriverTasks.wait(3);

        //Update Minimum Apt. Requirement value in data map
        data.put("Minimum Apt. Requirement","2-bedroom");

        //Select Minimum Apt. Requirement value
        HousingSearchInformationTab.selectMinimumApartment(data.get("Minimum Apt. Requirement"));

        //Click on Edit button
        HousingSearchInformationTab.clickOnEditAptDetails();
        //Updated Street Name value in data map
        data.put("Street Name","Updated Street");
        //Updated Street name value in Apartment table
        HousingSearchInformationTab.updateStreetName(data.get("Street Name"));
        //Click on Save Apartment details button
        HousingSearchInformationTab.clickOnSaveAptDetails();
        WebDriverTasks.wait(3);

        //Click on Save button to save updated values
        HousingSearchInformationTab.clickOnSaveButton();

        //Verify that success message is displayed after record is updated
        isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");

        //Temporary removal of Modified By field, as it's not needed in table data validation
        data.remove("Modified By");

        //Verify that updated data matches with actual data in table
        exceptions = HousingSearchInformationTab.verifyDataInTable(data);
        Assert.assertTrue(exceptions.isEmpty(),"Mismatch found with data submitted and data displayed in table for : " + exceptions);

        //Expand Changelog section
        CaseInformationTab.expandChangelog();
        WebDriverTasks.wait(3);

        //Verify that Action take in updated in Change log table
        actionTaken = HousingSearchInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Update".equals(actionTaken), "Action taken is not updated as expected in log table");

        //Click on Action > View Details button for first record
        HousingSearchInformationTab.viewHousingSearchInformationRecord(0);
        WebDriverTasks.wait(3);

        //Verify that Housing Search Status data matches with expected data
        exceptions = HousingSearchInformationTab.verifyHousingSearchStatusData(data);
        Assert.assertTrue(exceptions.isEmpty(),"Mismatch found with data submitted and data displayed in status section for : " + exceptions);

        //Add Modified By details in data map
        data.put("Modified By",modifiedBy);

        //Verify that Housing Apartment Details matches with expected data
        exceptions = HousingSearchInformationTab.verifyApartmentDetails(data);
        Assert.assertTrue(exceptions.isEmpty(),"Mismatch found with data submitted and data displayed in apartment details section for : " + exceptions);

        //Edit note and verify that it is reflected
        CaseInformationTab.editFirstNote("This is updated note");
        boolean isMatching = CaseInformationTab.isNoteAsExpected("This is updated note");
        Assert.assertTrue(isMatching, "Note text is not updated as expected");
        WebDriverTasks.wait(3);

        //Delete note and verify that success message is displayed on deletion of note
        CaseInformationTab.deleteNote(0);
        boolean noteDeleted = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Note has been deleted successfully.");
        Assert.assertTrue(noteDeleted,"Success message is not displayed fo deleting a note");
        WebDriverTasks.wait(3);

        //Click on cancel button on Housing Search Information tab
        HousingSearchInformationTab.clickOnCancelButton();

        //Delete first housing information record and verify that record count is reduced to 0.
        HousingSearchInformationTab.deleteHousingSearchRecord(0);
        recordCount = HealthInformationTab.getNoOfRecords();
        Assert.assertTrue(recordCount==0,"Housing search information can not be deleted");

        //Expand Changelog section
        CaseInformationTab.expandChangelog();
        WebDriverTasks.wait(3);

        //Verify that Action taken value is updated in Log table
        actionTaken = HousingSearchInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Delete".equals(actionTaken), "Action taken is not updated as expected in log table");
        WebDriverTasks.wait(3);

    }

    @Test
    public static void VerifyCriticalIncidentsTab() {
        String noteText = "This is automation note";
        HashMap<String,String> data = new HashMap<>();
        //Shift Details
        data.put("Area","1");
        data.put("Shift","Morning");
        data.put("Response Team","DHSPD,EMS");
        //Location Details
        data.put("Borough","Bronx");
        data.put("Location Type","Phone");
        data.put("Phone","8888888888");
        data.put("Is this the client's sleep location?","Yes");
        data.put("Additional Details","Automation testing additional details");
        //Engagement Details
        data.put("Link to an existing engagement","No");
        //Incident Details
        data.put("Incident Priority","Priority 1");
        data.put("Incident Date",Helpers.getCurrentDate("MM/dd/yyyy"));
        data.put("Incident Time","03:45 PM");
        data.put("Reporter Number","1234567891");
        data.put("Victims","Client");
        data.put("Victim Client","JEFFREY  MILLER (C50180 | MOC Client)");
        data.put("Perpetrators","Client");
        data.put("Perpetrator Client","JEFFREY  MILLER (C50180 | MOC Client)");
        data.put("Witnesses","Client");
        data.put("Witness Client","JEFFREY  MILLER (C50180 | MOC Client)");
        //Incident Type
        data.put("Incident Type","9.58 Removal");
        //Incident Description
        data.put("What Happened?","Automation testing what happened");
        data.put("Recurring Incident?","No");
        //Incident Outcome
        data.put("Immediate Action Taken","Issue resolved");
        //Additional Notes
        data.put("Additional Notes","Automation notes");

        //Navigate to Client List page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();
        WebDriverTasks.wait(3);

        //Filter only Caseload clients
        DailyOutreach.advancedFilterBy("ClientCategory", "Caseload", true);
        DailyOutreach.advancedFilterBy("Provider", "BronxWorks", false);
        WebDriverTasks.wait(3);

        //Click on first client id
        ClientList.clickonFirstClientID();

        //Get Provider name from Client Information tab
        data.put("Provider",HealthInformationTab.getProviderName());

        //Click on Critical Incidents tab
        CriticalIncidentsTab.clickOnCriticalIncidentsTab();

        //Delete all existing Critical Incident records
        CriticalIncidentsTab.deleteAllCriticalIncidentRecords();

        //Click on Add New button to add new critical incident record
        CriticalIncidentsTab.clickOnAddNewButton();

        //Submit data for Shift details section
        CriticalIncidentsTab.submitShiftDetails(data);
        //Submit data for Location details section
        CriticalIncidentsTab.submitLocationDetails(data);
        //Submit data for Engagement details section
        CriticalIncidentsTab.submitEngagementDetails(data);
        //Submit data for Incident details section
        CriticalIncidentsTab.submitIncidentDetails(data);
        //Submit data for Incident Type section
        CriticalIncidentsTab.submitIncidentTypeDetails(data);
        //Submit data for Incident Description section
        CriticalIncidentsTab.submitIncidentDescriptionDetails(data);
        //Submit data for Incident Outcome section
        CriticalIncidentsTab.submitIncidentOutcomeDetails(data);
        //Submit data for Additional notes
        CriticalIncidentsTab.setAdditionalNotes(data);

        //Expand Note section
        CaseInformationTab.clickOnNotesSection();
        //Add new note
        CaseInformationTab.addNewNote(noteText);
        WebDriverTasks.wait(3);

        //Click on Submit Pending Approvals button to save data
        CriticalIncidentsTab.clickSubmitPendingApproval();

        //Verify that success message is displayed after new record is added
        boolean isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");

        //Get No of Critical Incident records
        int recordCount = CriticalIncidentsTab.getNoOfRecords();
        Assert.assertTrue(recordCount==1,"Record count has not been increased");

        //Update extra values in data map
        data.put("Modified By",Base.returnLoggedInByUser());
        data.put("Status","Submitted-Pending Approval");

        //Verify expected data with the data displayed in table and gather all field name where mismatched is found
        String mismatchedFileds = CriticalIncidentsTab.verifyDataInTable(data);
        Assert.assertTrue(mismatchedFileds.isEmpty(),"Saved Critical Incident data is not reflected as expected for fields: " + mismatchedFileds);

        //Expand Changelog section
        CaseInformationTab.expandChangelog();
        WebDriverTasks.wait(3);

        //Verify that Action taken value is updated in Log table
        String actionTaken = HousingSearchInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Insert".equals(actionTaken), "Action taken is not updated as expected in log table");
        WebDriverTasks.wait(3);

        //Click on Action > View Details for the first record in Health information table
        CriticalIncidentsTab.viewCriticalInformationRecord(0);
        WebDriverTasks.wait(3);

        //Compare field values from Critical Incident form to expected data and gather all mismatched fields
        mismatchedFileds =  CriticalIncidentsTab.verifyCriticalIncidentData(data);
        Assert.assertTrue(mismatchedFileds.isEmpty(),"Saved Critical Incident data is not reflected as expected in Incident details page for fields: " + mismatchedFileds);

        //Click on Edit button
        CriticalIncidentsTab.clickEditButton();

        //Update values to verify edit functionality
        data.put("What Happened?","Updated testing comments");
        data.put("Incident Type","9.58 Removal,Arson");
        data.put("Phone","9999999999");

        //Submit Updated Incident Type details
        CriticalIncidentsTab.submitIncidentTypeDetails(data);
        //Submit Updated Location details
        CriticalIncidentsTab.submitLocationDetails(data);
        //Submit Updated Incident Description details
        CriticalIncidentsTab.submitIncidentDescriptionDetails(data);
        //Click on Submit Pending approvals
        CriticalIncidentsTab.clickSubmitPendingApproval();

        //Verify that success message is displayed after new record is added
        isSaved = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Changes have been saved successfully.");
        Assert.assertTrue(isSaved, "Success message is not displayed");

        //Verify expected data with the data displayed in table and gather all field name where mismatched is found
        mismatchedFileds = CriticalIncidentsTab.verifyDataInTable(data);
        Assert.assertTrue(mismatchedFileds.isEmpty(),"Saved Critical Incident data is not reflected as expected for fields: " + mismatchedFileds);

        //Expand Changelog section
        CaseInformationTab.expandChangelog();
        WebDriverTasks.wait(3);

        //Verify that Action taken value is updated in Log table
        actionTaken = HousingSearchInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Update".equals(actionTaken), "Action taken is not updated as expected in log table");
        WebDriverTasks.wait(3);

        //Click on Action > View Details for the first record in Critical Incidents table
        CriticalIncidentsTab.viewCriticalInformationRecord(0);
        WebDriverTasks.wait(3);

        //Compare field values from Critical Incident form to expected data and gather all mismatched fields
        mismatchedFileds =  CriticalIncidentsTab.verifyCriticalIncidentData(data);
        Assert.assertTrue(mismatchedFileds.isEmpty(),"Saved Critical Incident data is not reflected as expected in Incident details page for fields: " + mismatchedFileds);

        //Edit note and verify that it is reflected
        CaseInformationTab.editFirstNote("This is updated note");
        boolean isMatching = CaseInformationTab.isNoteAsExpected("This is updated note");
        Assert.assertTrue(isMatching, "Note text is not updated as expected");
        WebDriverTasks.wait(3);

        //Delete note and verify that success message is displayed on deletion of note
        CaseInformationTab.deleteNote(0);
        boolean noteDeleted = TravelAssistanceProgramTab.verifySuccessMessageIsDisplayed("Note has been deleted successfully.");
        Assert.assertTrue(noteDeleted,"Success message is not displayed after deleting a note");
        WebDriverTasks.wait(3);

        //Click on cancel button to return to Critical Incident table page
        CriticalIncidentsTab.clickCancelButton();

        //Delete first Critical Incident record
        CriticalIncidentsTab.deleteCriticalIncidentRecord(0);

        //Expand Changelog section
        CaseInformationTab.expandChangelog();
        WebDriverTasks.wait(3);

        //Verify that Action taken value is updated in Log table
        actionTaken = HousingSearchInformationTab.getActionTakenFromLog();
        Assert.assertTrue("Delete".equals(actionTaken), "Action taken is not updated as expected in log table");
        WebDriverTasks.wait(3);
    }

    @Test
    public static void VerifyMyCaseLoadDataGrid() throws IOException {
        String currentPlacement = "Permanent Placement";
        String legalName = "User3668";
        String provider = "Bowery Residents Committee, Inc.";

        //Navigate to My Caseload page
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnMyCaseLoadMenuItem();
        WebDriverTasks.wait(3);

        //Verify that data grid is displayed
        boolean gridDisplayed = MyCaseload.verifyCaseLoadDataGrid();
        Assert.assertTrue(gridDisplayed,"Could not find data grid for My Caseload");

        //Select multiple items per page values and verify that data grid is getting updated accordingly
        boolean isPaginated = MyCaseload.verifyRecordsPerPage();
        Assert.assertTrue(isPaginated,"Records count are not updated based on selection in items per page");

        //Search for Current placement values and verify that data grid shows only those values for 'Current Placement' column
        DailyOutreach.searchFor(currentPlacement);
        boolean verifySearch = MyCaseload.verifyDataInColumn("Current Placement",currentPlacement);
        Assert.assertTrue(verifySearch,"Searched data is not displayed in grid for column 'Current Placement'");

        //Search for Legal Name values and verify that data grid shows only those values for 'Legal Name' column
        DailyOutreach.searchFor(legalName);
        verifySearch = MyCaseload.verifyDataInColumn("Legal Name",legalName);
        Assert.assertTrue(verifySearch,"Searched data is not displayed in grid for column 'JCC Client'");

        //Search for Provider values and verify that data grid shows only those values for 'Provider' column
        DailyOutreach.searchFor(provider);
        verifySearch = MyCaseload.verifyDataInColumn("Provider",provider);
        Assert.assertTrue(verifySearch,"Searched data is not displayed in grid for column 'Provider'");

        //Clear search results (Un-do)
        DailyOutreach.searchFor("");

        //Apply advanced filter for Provider column and verify that data is filtered accordingly for Provider column
        DailyOutreach.advancedFilterBy("Provider", "BronxWorks", true);
        MyCaseload.verifyDataInColumn("Provider", "BronxWorks");

        //Remove advanced filters
        DailyOutreach.clearAdvancedFilter();

        //This code will perform validation on Columns in Data Grid and Exported excel for all records, it will also match the record count in exported excel is matching total records as per application
        int fileCountBefore = Helpers.countFilesInDownloadFolder();
        MyCaseload.exportAllRecordsToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take total record count from pagination and compare it with total records count in exported excel
        int allExpectedRecordsCount = ViewReports.getTotalRecordsCount();
        int allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("OutreachClientListReport");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of total record count");

        //This will change Records to be displayed per page to 5
        MyCaseload.selectItemsPerPage("5");

        //This code will perform validation on Columns in Data Grid and Exported excel for current view, it will also match the record count in exported excel is matching total records as per application
        fileCountBefore = Helpers.countFilesInDownloadFolder();
        MyCaseload.exportCurrentViewToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take count of records displayed in current data grid and compare it with records count in exported excel
        allExpectedRecordsCount = ViewReports.getVisibleRecordsCount();
        allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("OutreachClientListReport");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of record count in current view");

    }
}