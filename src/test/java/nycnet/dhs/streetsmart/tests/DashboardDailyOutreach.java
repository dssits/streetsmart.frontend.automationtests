package nycnet.dhs.streetsmart.tests;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.AddClient;
import nycnet.dhs.streetsmart.pages.AddEngagement;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.ClientInformation;
import nycnet.dhs.streetsmart.pages.ClientList;
import nycnet.dhs.streetsmart.pages.Common;
import nycnet.dhs.streetsmart.pages.dashboard.DailyOutreach;
import nycnet.dhs.streetsmart.pages.reports.CreateReport;
import nycnet.dhs.streetsmart.pages.reports.ViewReports;

public class DashboardDailyOutreach extends AutoRunTestController {
	
	//verify the total counts for the Homestat clients tile matches with sub counts for the tile. And counts matches with the middle panel and the grid
	@Test()
    public void VerifyHomeStatClientTileTotalCountMatchesSubCounts() {
       int fullNameGivenCount = DailyOutreach.getBreakdownCountFor("Full Name Given");
       int fullNameNotGivenCount = DailyOutreach.getBreakdownCountFor("Full Name Not Given");
       int assignedCaseManagerCount = DailyOutreach.getBreakdownCountFor("Assigned a Case Manager");
       int getTotalRecordCountOnTileForHomeStatClients =  DailyOutreach.getTotalRecordCountOnTile("HOME-STAT Clients");
       Assert.assertEquals(getTotalRecordCountOnTileForHomeStatClients, fullNameGivenCount + fullNameNotGivenCount +  assignedCaseManagerCount);
        DailyOutreach.clickOnDetailsButtonFor("HOME-STAT Clients");
        int homeStatClientGridTotalCount = DailyOutreach.getTotalRecordsCount();
        Assert.assertEquals(homeStatClientGridTotalCount, getTotalRecordCountOnTileForHomeStatClients);
        DailyOutreach.scrollToIconToToggleExpandMiddlePanel();
        DailyOutreach.clickOnIconToToggleExpandMiddlePanel();
        int ClientsCountMiddlePanelTotalCount = DailyOutreach.getTotalRecordCountExpandedMiddlePanel("Clients");
        int fullNameGivenCountMiddlePanelCount = DailyOutreach.getSubRecordCountExpandedMiddlePanel("Full Name Given");
        int fullNameNotGivenCountMiddlePanelCount = DailyOutreach.getSubRecordCountExpandedMiddlePanel("Full Name Not Given");
        int assignedCaseManagerMiddlePanelCount = DailyOutreach.getSubRecordCountExpandedMiddlePanel("Assigned a Case Manager");
        Assert.assertEquals(ClientsCountMiddlePanelTotalCount, fullNameGivenCountMiddlePanelCount + fullNameNotGivenCountMiddlePanelCount + assignedCaseManagerMiddlePanelCount);
        Assert.assertEquals(ClientsCountMiddlePanelTotalCount, homeStatClientGridTotalCount);
        
        Assert.assertEquals(ClientsCountMiddlePanelTotalCount, getTotalRecordCountOnTileForHomeStatClients);
        Assert.assertEquals(fullNameGivenCountMiddlePanelCount, fullNameGivenCount);
        Assert.assertEquals(fullNameNotGivenCountMiddlePanelCount, fullNameNotGivenCount);
        Assert.assertEquals(assignedCaseManagerMiddlePanelCount, assignedCaseManagerCount);
        
        //Verify Streetsmart Case No column can be enabled and disabled
        boolean isStreetSmartCaseNoColumnPresentOnGrid = DailyOutreach.isColumnPresentOnGrid("StreetSmart Case No.");
        Assert.assertTrue(isStreetSmartCaseNoColumnPresentOnGrid, "The column is not present on the Grid");
        DailyOutreach.clickOnColumnPicker();
        DailyOutreach.deselectColumnOnColumnPicker("StreetSmart Case No.");
        DailyOutreach.clickOnColumnPicker();
        isStreetSmartCaseNoColumnPresentOnGrid = DailyOutreach.isColumnPresentOnGrid("StreetSmart Case No.");
        Assert.assertFalse(isStreetSmartCaseNoColumnPresentOnGrid, "The column is present on the Grid");
        DailyOutreach.clickOnColumnPicker();
        DailyOutreach.selectColumnOnColumnPicker("StreetSmart Case No.");
        DailyOutreach.clickOnColumnPicker();
        isStreetSmartCaseNoColumnPresentOnGrid = DailyOutreach.isColumnPresentOnGrid("StreetSmart Case No.");
        Assert.assertTrue(isStreetSmartCaseNoColumnPresentOnGrid, "The column is not present on the Grid");
        
        //Search for client and open
        String streetSmartIDOnGrid = DailyOutreach.returnFirstRecordOnStreetSmartColumn();
        DailyOutreach.searchFor(streetSmartIDOnGrid);
        String streetSmartIDOnGridAfterSearch = DailyOutreach.returnFirstRecordOnStreetSmartColumn();
        Assert.assertEquals(streetSmartIDOnGridAfterSearch, streetSmartIDOnGrid);
        Common.clickOnLinkText(streetSmartIDOnGrid);
        String streetSmartIDOnClientInformation = ClientInformation.returnStreetsmartID();
        Assert.assertEquals(streetSmartIDOnClientInformation, streetSmartIDOnGrid);
    }
	
	//verify the Homestat clients tile advanced filtering works
		@Test()
	    public void VerifyHomeStatClientGridAdvancedFiltering() {
	        DailyOutreach.clickOnDetailsButtonFor("HOME-STAT Clients");
	        String streetSmartIDOnGrid = DailyOutreach.returnFirstRecordOnStreetSmartColumn();
	        DailyOutreach.clickOnAdvancedFilter();
	       // DailyOutreach.advancedFilterBy("StreetSmart ID", streetSmartIDOnGrid, true);
	        ClientList.clickOnChooseFiter();
	        DailyOutreach.chooseColumnFilterOnAdvancedFilter("StreetSmart ID");
	        DailyOutreach.enterFilterValueOnSelectedFilterOnAdvancedFilter("StreetSmart ID", streetSmartIDOnGrid);
	        ClientList.clickOnApplyFilter();
	        String streetSmartIDOnGridAfter = DailyOutreach.returnFirstRecordOnStreetSmartColumn();
	        Assert.assertEquals(streetSmartIDOnGridAfter, streetSmartIDOnGrid);
	        Common.clickOnLinkText(streetSmartIDOnGrid);
	        String streetSmartIDOnClientInformation = ClientInformation.returnStreetsmartID();
	        Assert.assertEquals(streetSmartIDOnClientInformation, streetSmartIDOnGrid);  
	    }

    //H20-751 - Verify Other Prospect Client Category AdvanceFilters In CurrentLocation And Prospects Grid
    @Test()
    public void VerifyEngagedUnshelteredProspectClientCategoryAdvanceFiltersInCurrentLocationAndProspectsGrid() {
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
        Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");
        DailyOutreach.openCurrentLocationProspectsAndClientsGrid();

        DailyOutreach.scrollToAdvancedFilter();
        DailyOutreach.clickOnAdvancedFilter();
        Common.clickOnChooseFiter();
        Common.clickOnRadioClientCategory();
        Common.clickOnClientCategoryFilterButton2();
        Common.clickOnRadioEngagedUnsheltered();

        Common.clickOnApplyFilter();

        Assert.assertEquals(DailyOutreach.returnFirstRecordOnClientCategoryColumn(), "Engaged Unsheltered Prospect");


    }

    //H20-751 - Verify Other Prospect Client Category AdvanceFilters In CurrentLocation And Prospects Grid
    @Test()
    public void VerifyOtherProspectClientCategoryAdvanceFiltersInCurrentLocationAndProspectsGrid() {
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
        Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");
        DailyOutreach.openCurrentLocationProspectsAndClientsGrid();

        DailyOutreach.scrollToAdvancedFilter();
        DailyOutreach.clickOnAdvancedFilter();
        Common.clickOnChooseFiter();
        Common.clickOnRadioClientCategory();
        Common.clickOnClientCategoryFilterButton2();
        Common.clickOnRadioOtherProspect();
        Common.clickOnApplyFilter();
        Assert.assertEquals(DailyOutreach.returnFirstRecordOnClientCategoryColumn(), "Other Prospect");

    }

    //H20-751 - Verify Caseload Client Category AdvanceFilters In CurrentLocation And Prospects Grid
    @Test()
    public void VerifyCaseloadClientCategoryAdvanceFiltersInCurrentLocationAndProspectsGrid() {
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
        Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");
        DailyOutreach.openCurrentLocationProspectsAndClientsGrid();

        DailyOutreach.scrollToAdvancedFilter();
        DailyOutreach.clickOnAdvancedFilter();
        Common.clickOnChooseFiter();
        Common.clickOnRadioClientCategory();
        Common.clickOnClientCategoryFilterButton2();
        Common.clickOnRadioCaseload();
        Common.clickOnApplyFilter();
        Assert.assertEquals(DailyOutreach.returnFirstRecordOnClientCategoryColumn(), "Caseload");

    }


    // - Click on Total Info Icon the Engaged Unsheltered and Other Prospects Tile and verify pop-up correctly opens.
    @Test()
    public void verifyInfoIconTextOnCurrentLocationOfClientsAndProspectsTile() {
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
        Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");
        DailyOutreach.clickOninfoIconUnshelteredCurrentLocationOfProspectsTileToOpenPopup();
        Assert.assertEquals(DailyOutreach.returnUnshelteredCurrentLocationOfProspectsPopupTitle(), "Unsheltered");
        DailyOutreach.clickOnXUnshelteredInfoPopupOnCurrentLocationOfProspectsTileToClosePopup();
        WebDriverTasks.specificWait(3000);
        DailyOutreach.clickOninfoIconOnStreetAndOtherSettingsCurrentLocationOfProspectsTileToOpenPopup();
        Assert.assertEquals(DailyOutreach.returninfoOnStreetAndOtherSettingsCurrentLocationOfProspectsPopupTitle(), "On Street & Other Settings");
        DailyOutreach.clickOnXOnStreetOtherSettingsInfoPopupOnCurrentLocationOfProspectsTileToClosePopup();
        WebDriverTasks.specificWait(3000);
        DailyOutreach.clickOninfoIconUndeterminedHousingStatusCurrentLocationOfProspectsTileToOpenPopup();
        Assert.assertEquals(DailyOutreach.returnInfoUndeterminedHousingStatusCurrentLocationOfProspectsTilePopupTitle(), "Undetermined Housing Status");
        DailyOutreach.clickOnXUndeterminedHousingStatusCurrentLocationOfProspectsTileToClosePopup();

    }

    //H20-730 - On the Engaged Unsheltered Prospect and Other Prospect grid, able to apply the Engaged Unsheltered Prospect
    //Client Category filters. Export to Excel and verify presence of Engaged Unsheltered Prospect
    @Test()
    public void verifyExportToExcelCurrentViewapplyEngagedUnshelteredProspectClientCategoryOnTheEngagedUnshelteredAndOtherProspectGrid() throws IOException {
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
        Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");
        WebDriverTasks.specificWait(3000);
        DailyOutreach.openEngagedUnshelteredProspectAndOtherProspectGrid();
        DailyOutreach.scrollToAdvancedFilter();
        DailyOutreach.clickOnAdvancedFilter();
        Common.clickOnChooseFiter();
        Common.clickOnRadioClientCategory();
        Common.clickOnClientCategoryFilterButton2();
        Common.clickOnRadioEngagedUnsheltered();
        Common.clickOnApplyFilter();
        int fileCountBefore = Helpers.countFilesInDownloadFolder();
        Common.scrollToExportToExcel();
        Common.clickOnExportToExcel();
        Common.clickOnExportToExcelCurrentView();
        Common.clickOnOKConfirmationButton();
        Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

        String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
        String clientCategoryFirstRecord = Helpers.returnExcelCellValue(mostRecentFile, "Engaged Unsheltered & Prospects", 1, 10);

        Assert.assertEquals(clientCategoryFirstRecord, "Engaged Unsheltered Prospect");

    }

    //H20-730 - On the Engaged Unsheltered Prospect and Other Prospect grid, able to apply the Other Prospect
    //Client Category filters. Export to Excel and verify presence of Other Prospect

    @Test()
    public void verifyExportToExcelCurrentViewOtherProspectClientCategoryOnTheEngagedUnshelteredAndOtherProspectGrid() throws IOException {
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
        Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");
        WebDriverTasks.specificWait(3000);
        DailyOutreach.openEngagedUnshelteredProspectAndOtherProspectGrid();
        WebDriverTasks.pageUpCommon();

        DailyOutreach.scrollToAdvancedFilter();
        DailyOutreach.clickOnAdvancedFilter();
        Common.clickOnChooseFiter();
        Common.clickOnRadioClientCategory();
        Common.clickOnClientCategoryFilterButton2();
        Common.clickOnRadioOtherProspect();
        Common.clickOnApplyFilter();
        int fileCountBefore = Helpers.countFilesInDownloadFolder();

        Common.scrollToExportToExcel();
        Common.clickOnExportToExcel();
        Common.clickOnExportToExcelCurrentView();
        Common.clickOnOKConfirmationButton();
        Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

        String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
        String clientCategoryFirstRecord = Helpers.returnExcelCellValue(mostRecentFile, "Engaged Unsheltered & Prospects", 1, 10);

        Assert.assertEquals(clientCategoryFirstRecord, "Other Prospect");

    }

    //H20-708 - Click on Total Info Icon the Engaged Unsheltered and Other Prospects Tile and verify pop-up correctly opens.
    @Test()
    public void totalInfoIconOnEngagedUnshelteredProspectAndOtherProspectsTile() {
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
        Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");
        DailyOutreach.clickOntotalInfoIconEngagedUnshelteredProspectAndOtherProspect();
        Assert.assertEquals(DailyOutreach.EngagedUnshelteredProspectAndOtherProspectPopupTile(), "Engaged Unsheltered Prospects and Other Prospects");
        Assert.assertEquals(DailyOutreach.returnEngagedUnshelteredProspectAndOtherProspectPopupText(), "The total number of Engaged Unsheltered Prospects and Other Prospects as of today. Counts will vary depending on time frame selected: daily, weekly, monthly.");

    }

    //H20-716 - Click on info icon Engaged Unsheltered on the Engaged Unsheltered  and Other Prospects Tile and verify pop-up correctly opens.
    @Test()
    public void InfoIconOnEngagedUnshelteredProspectOnEngagedUnshelteredProspectAndOtherProspectsTile() {
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
        Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");
        DailyOutreach.clickOnInfoIconEngagedUnshelteredProspect();
        Assert.assertEquals(DailyOutreach.returnEngagedUnshelteredProspectPopupTitle(), "Engaged Unsheltered Prospects");

        Assert.assertEquals(DailyOutreach.returnEngagedUnshelteredProspectPopupText(), "The total number of Engaged Unsheltered Prospects with any of the following as of today: first and last name as well as alias, first and last name, first name and alias, last name and alias. Counts will vary depending on timeframe selected: daily, weekly, monthly.");
        DailyOutreach.closeEngagedUnshelteredProspectPopupOnEngagedUnshelteredProspectAndOtherProspectTile();
        DailyOutreach.clickinfoiconOtherProspect();
        Assert.assertEquals(DailyOutreach.returnOtherProspectPopupTitle(), "Other Prospects");
        Assert.assertEquals(DailyOutreach.returnOtherProspectPopupText(), "The total number of Other Prospects with any of the following as of today: first and last name as well as alias, first and last name, first name and alias, last name and alias. Counts will vary depending on timeframe selected: daily, weekly, monthly.");
    }

    //H20-727 - Open Current location and prospects grid, disable Client Category using column picker,
    //verify client category is not on grid, enable client category using column picker, verify it appears on the grid
    @Test()
    public void verifyClientCategoryOnColumnPickerCurrentLocationAndProspectsGrid() {
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
        Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");
        DailyOutreach.openCurrentLocationProspectsAndClientsGrid();
        WebDriverTasks.pageDownCommon();
        WebDriverTasks.pageDownCommon();
        boolean presenceOfClientCategoryColumn = DailyOutreach.verifyIfclientCategoryColumnExists();
        Assert.assertTrue(presenceOfClientCategoryColumn, "The Client Category Column is not present");
        Common.clickOnColumnPicker();
        Common.clickOnclientCategoryOnColumnPicker();
        Common.clickOnColumnPicker();
        presenceOfClientCategoryColumn = DailyOutreach.verifyIfclientCategoryColumnExists();
        Assert.assertFalse(presenceOfClientCategoryColumn, "The Client Category Column is present");
        Common.clickOnColumnPicker();
        Common.clickOnclientCategoryOnColumnPicker();
        Common.clickOnColumnPicker();
        presenceOfClientCategoryColumn = DailyOutreach.verifyIfclientCategoryColumnExists();
        Assert.assertTrue(presenceOfClientCategoryColumn, "The Client Category Column is not present");
    }

    //H20-704 Rename the Prospective Clients tile on Daily Dashboard to Engaged Unsheltered Prospects and Other Prospect.
    @Test()
    public static void VerifyEngagedUnshelteredTileInDailyOutreachDashboard() {
        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Engaged Unsheltered Tile is displayed
        boolean isEngagedUnshelteredTileDisplayed = DailyOutreach.isEngagedUnsheltedProspectTileDisplayed();
        Assert.assertTrue(isEngagedUnshelteredTileDisplayed, "Engaged Unsheltered prospect and other prospect tile is not displayed on dashboard");

        //Verify total record count matches summation of Engaged Unsheltered and Other Prospect count
        boolean isSumAsExpected = DailyOutreach.verifyRecordCountForEngagedUnshelteredTile();
        Assert.assertTrue(isSumAsExpected, "Sum of Engaged Unsheltered and Other prospect records does not match total records count");

        DailyOutreach.clickOnSettingsButtonForEngagedUnshelteredTile();

        //Verify Setting options contains 'Daily' option
        boolean isDailyOptionDisplayed = DailyOutreach.verifySettingOptionOnEngagedUnshelteredTile("Daily");
        Assert.assertTrue(isDailyOptionDisplayed, "Cannot find settings option 'Daily'");

        //Verify Setting options contains 'Weekly' option
        boolean isWeeklyOptionDisplayed = DailyOutreach.verifySettingOptionOnEngagedUnshelteredTile("Weekly");
        Assert.assertTrue(isWeeklyOptionDisplayed, "Cannot find settings option 'Weekly'");

        //Verify Setting options contains 'Monthly' option
        boolean isMonthlyOptionDisplayed = DailyOutreach.verifySettingOptionOnEngagedUnshelteredTile("Monthly");
        Assert.assertTrue(isMonthlyOptionDisplayed, "Cannot find settings option 'Monthly'");

        DailyOutreach.clickOnDetailsButtonForEngagedUnshelteredTile();

        //Verify on clicking Details button Engaged Unsheltered data pane is displayed
        boolean isDataPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForEngagedUnsheltered();
        Assert.assertTrue(isDataPaneDisplayed, "Data pane is not displayed for Engaged Unsheltered Prospect and Other Prospect");

        //Verify total records count displayed on tile is same as total records in data grid
        boolean isRecordOnTileMatchGridDataCount = DailyOutreach.verifyTotalRecordsCountsMatchesWithGridRecordCount();
        Assert.assertTrue(isRecordOnTileMatchGridDataCount, "Data displayed on tile does not matched the records count in pagination");

        //Verify Export option is displayed for data grid
        boolean isDownloadToExcelDisplayed = DailyOutreach.verifyExportToExcelIsDisplayed();
        Assert.assertTrue(isDownloadToExcelDisplayed, "Export option for Excel is not displayed on data grid");

        DailyOutreach.clickOnExportToExcel();

        //Verify Export option contains 'All' sub-option
        boolean isDownloadAll = DailyOutreach.verifyExportToExcelOption_All();
        Assert.assertTrue(isDownloadAll, "All option for Excel export is not displayed on data grid");

        //Verify Export option contains 'Current View' sub-option
        boolean isDownloadCurrentView = DailyOutreach.verifyExportToExcelOption_CurrentView();
        Assert.assertTrue(isDownloadCurrentView, "Current view option for Excel export is not displayed on data grid");

    }

    //H20-714 As a User,when I open the Daily Outreach Dashboard,I want the "Engaged Unsheltered Prospects and Other Prospect tile" to display the new breakdown values and their counts respectively on the tile,as described below.
    @Test()
    public static void VerifyFiltersOnEngagedUnshelteredTile() {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Engaged Unsheltered Tile is displayed
        boolean isEngagedUnshelteredTileDisplayed = DailyOutreach.isEngagedUnsheltedProspectTileDisplayed();
        Assert.assertTrue(isEngagedUnshelteredTileDisplayed, "Engaged Unsheltered prospect and other prospect tile is not displayed on dashboard");

        //Verify two breakdowns 'Engaged Unsheltered Prospect' and 'Other Prospect' are displayed
        boolean isEngagedUnshelteredBreakdownDisplayed = DailyOutreach.verifyEngagedUnshelteredBreakdownIsDIsplayed();
        Assert.assertTrue(isEngagedUnshelteredBreakdownDisplayed, "'Engaged Unsheltered Prospect' breakdown is not displayed");
        boolean otherProspectBrekdownDisplayed = DailyOutreach.verifyOtherProspectBreakdownIsDIsplayed();
        Assert.assertTrue(otherProspectBrekdownDisplayed, "'Other Prospect' breakdown is not displayed");

        //Verify total record count matches summation of Engaged Unsheltered and Other Prospect count
        boolean isSumAsExpected = DailyOutreach.verifyRecordCountForEngagedUnshelteredTile();
        Assert.assertTrue(isSumAsExpected, "Sum of Engaged Unsheltered and Other prospect records does not match total records count");

        //Verify information is displayed on clicking 'i' icon
        boolean isInformationDisplayed = DailyOutreach.verifyDescriptionDisplayedForEngagedUnshelteredProspect();
        Assert.assertTrue(isInformationDisplayed, "Breakdown details for 'Engaged Unsheltered Prospects' is not displayed");
        isInformationDisplayed = DailyOutreach.verifyDescriptionDisplayedForOtherProspect();
        Assert.assertTrue(isInformationDisplayed, "Breakup details for 'Other Prospect' is not displayed");

        //Click on Setting button on 'Engaged Unsheltered' tile
        DailyOutreach.clickOnSettingsButtonForEngagedUnshelteredTile();

        //Daily filter by date 07/30/2020
        DailyOutreach.filterByDaily("07/30/2020");

        //Verify total records count displayed on tile is same as total records in data grid
        boolean isRecordOnTileMatchGridDataCount = DailyOutreach.verifyTotalRecordsCountsMatchesWithGridRecordCount();
        Assert.assertTrue(isRecordOnTileMatchGridDataCount, "Data displayed on tile does not matched the records count in pagination");

        //Weekly filter by date 07/30/2020
        DailyOutreach.filterByWeekly("07/30/2020");

        //Verify total records count displayed on tile is same as total records in data grid
        isRecordOnTileMatchGridDataCount = DailyOutreach.verifyTotalRecordsCountsMatchesWithGridRecordCount();
        Assert.assertTrue(isRecordOnTileMatchGridDataCount, "Data displayed on tile does not matched the records count in pagination");

        //Monthly filter by month July and 2020
        DailyOutreach.filterByMonthly("2020", "July");

        //Verify total records count displayed on tile is same as total records in data grid
        isRecordOnTileMatchGridDataCount = DailyOutreach.verifyTotalRecordsCountsMatchesWithGridRecordCount();
        Assert.assertTrue(isRecordOnTileMatchGridDataCount, "Data displayed on tile does not matched the records count in pagination");
    }

    //H20-722	As a User,when I click Engaged Unsheltered Prospects and Other Prospect KPI tile details link,in the grid,
    // I want to see a new field Client Category so that when I view the Client records along with their Client Category.
    @Test()
    public static void VerifyEngagegedUnshelteredDatGridFunctionality() throws IOException, InterruptedException {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Engaged Unsheltered Tile is displayed
        boolean isEngagedUnshelteredTileDisplayed = DailyOutreach.isEngagedUnsheltedProspectTileDisplayed();
        Assert.assertTrue(isEngagedUnshelteredTileDisplayed, "Engaged Unsheltered prospect and other prospect tile is not displayed on dashboard");

        //Click on Details button on Engaged Unsheltered tile
        DailyOutreach.clickOnDetailsButtonForEngagedUnshelteredTile();

        //Verify on clicking Details button Engaged Unsheltered data pane is displayed
        boolean isDataPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForEngagedUnsheltered();
        Assert.assertTrue(isDataPaneDisplayed, "Data pane is not displayed for Engaged Unsheltered Prospect and Other Prospect");

        //Verify total records count displayed on tile is same as total records in data grid
        boolean isRecordOnTileMatchGridDataCount = DailyOutreach.verifyTotalRecordsCountsMatchesWithGridRecordCount();
        Assert.assertTrue(isRecordOnTileMatchGridDataCount, "Data displayed on tile does not matched the records count in pagination");

        //This method will iterate through each data grid page and verify that Client category column contains only 'Engaged Unsheltered Prospects' or 'Other Prospect'
        boolean isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect,Other Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' or 'Other Prospect' data is displayed fpr Client Category column");

        //This method will iterate through each data grid page and verify that Current Placement column contains only 'On Street'
        isExpectedDataDisplayed = DailyOutreach.verifyDataInCurrentPlacementColumn();
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'On Street' data is displayed fpr Current Placement column");

        //Verify that client category column is after client type column and before current placement
        boolean isInPlace = DailyOutreach.verifyClientCategoryColumnPosition();
        Assert.assertTrue(isInPlace, "Client Category column is not in place");

        //This code will perform validation on Columns in Data Grid and Exported excel for all records, it will also match the record count in exported excel is matching total records as per application
        int fileCountBefore = Helpers.countFilesInDownloadFolder();
        ViewReports.exportAllRecordsToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take total record count from pagination and compare it with total records count in exported excel
        int allExpectedRecordsCount = ViewReports.getTotalRecordsCount();
        int allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Engaged Unsheltered & Prospects");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of total record count");

        //This will verify that 'Client Category' column is displayed in exported excel
        boolean isColumnDisplayed = DailyOutreach.isColumnDisplayedInExcel("Client Category", "Engaged Unsheltered & Prospects");
        Assert.assertTrue(isColumnDisplayed, "'Client Category' column is not displayed in exported excel");

        //This will change Records to be displayed per page to 5
        ViewReports.changeRecordsPerPageTo("5");

        //This code will perform validation on Columns in Data Grid and Exported excel for current view, it will also match the record count in exported excel is matching total records as per application
        fileCountBefore = Helpers.countFilesInDownloadFolder();
        ViewReports.exportCurrentViewToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take count of records displayed in current data grid and compare it with records count in exported excel
        allExpectedRecordsCount = ViewReports.getVisibleRecordsCount();
        allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Engaged Unsheltered & Prospects");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of record count in current view");

        //This will verify that 'Client Category' column is displayed in exported excel
        isColumnDisplayed = DailyOutreach.isColumnDisplayedInExcel("Client Category", "Engaged Unsheltered & Prospects");
        Assert.assertTrue(isColumnDisplayed, "'Client Category' column is not displayed in exported excel");

        //This will verify that Client Category is pre-selected in column picker
        boolean isPreselected = DailyOutreach.isClientCategorySelected();
        Assert.assertTrue(isPreselected, "'Client Category' is not pre-selected by default");

        //This will verify that de-selecting Client category column in column picker, column will be removed from grid
        boolean isDeselected = DailyOutreach.deselectClientCategoryColumn();
        Assert.assertTrue(isDeselected, "'Client Category' column is not removed from data grid");

        //This will verify that selecting Client category column in column picker,column will be displayed in grid
        boolean isSelected = DailyOutreach.selectClientCategoryColumn();
        Assert.assertTrue(isSelected, "'Client Category' column is not displayed");
    }

    //H20-728	As a User,when I advance filter on the Engaged Unsheltered and Other Prospect grid
    // I should see Client Category field within Choose filter and Engaged Unsheltered Prospect and Other Prospect as two filter options and be able to advance filter on them.
    @Test()
    public static void VerifyEngagedUnshelteredDataGridAdvancedFilterFunctionality() throws IOException {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Engaged Unsheltered Tile is displayed
        boolean isEngagedUnshelteredTileDisplayed = DailyOutreach.isEngagedUnsheltedProspectTileDisplayed();
        Assert.assertTrue(isEngagedUnshelteredTileDisplayed, "Engaged Unsheltered prospect and other prospect tile is not displayed on dashboard");

        //Click on Details button on 'Engaged Unsheltered' tile
        DailyOutreach.clickOnDetailsButtonForEngagedUnshelteredTile();

        //Verify on clicking Details button Engaged Unsheltered data pane is displayed
        boolean isDataPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForEngagedUnsheltered();
        Assert.assertTrue(isDataPaneDisplayed, "Data pane is not displayed for Engaged Unsheltered Prospect and Other Prospect");

        //Click on Advanced filter button and open Filter selection options list
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();

        //Verify position of 'Client Category' checkbox
        boolean isInPlace = DailyOutreach.verifyPositionOfClientCategoryCheckBox();
        Assert.assertTrue(isInPlace, "'Client Category' checkbox is either not after 'StreetSmart Client Type' checkbox or not before 'Current Placement' checkbox");

        //Apply Advanced filter for Client Category = 'Engaged Unsheltered Prospect'
        ClientList.clickOnRadioClientCategory();
        ClientList.clickOnClientCategorySubDropdown();
        ClientList.clickOnEngagedUnshelteredRadio();
        ClientList.clickOnApplyFilter();

        //This method will iterate through each data grid page and verify that Client category column contains only 'Engaged Unsheltered Prospects'
        boolean isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //Apply Advanced filter for Client Category = 'Other Prospect' and 'Engaged Unsheltered Prospect'
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        ClientList.clickOnRadioClientCategory();
        ClientList.clickOnClientCategorySubDropdown();
        ClientList.clickOnOtherProspectdRadio();
        ClientList.clickOnApplyFilter();

        //This method will iterate through each data grid page and verify that Client category column contains only 'Other Prospect'
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Other Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Other Prospect' data is displayed fpr Client Category column");

        //Apply Advanced filter for Client Category = 'Other Prospect' and 'Engaged Unsheltered Prospect'
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        ClientList.clickOnRadioClientCategory();
        ClientList.clickOnClientCategorySubDropdown();
        ClientList.clickOnOtherProspectdRadio();
        ClientList.clickOnEngagedUnshelteredRadio();
        ClientList.clickOnApplyFilter();

        //This method will iterate through each data grid page and verify that Client category column contains only 'Engaged Unsheltered Prospects' or 'Other Prospect'
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect,Other Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' or 'Other Prospect' data is displayed fpr Client Category column");

        //This code will perform validation on Columns in Data Grid and Exported excel for current view, it will also match the record count in exported excel is matching total records as per application
        int fileCountBefore = Helpers.countFilesInDownloadFolder();
        DailyOutreach.exportFilteredRecords();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take total record count from pagination and compare it with total records count in exported excel
        int allExpectedRecordsCount = ViewReports.getTotalRecordsCount();
        int allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Engaged Unsheltered & Prospects");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of total record count");

        //This will verify that 'Client Category' column is displayed in exported excel
        boolean isColumnDisplayed = DailyOutreach.isColumnDisplayedInExcel("Client Category", "Engaged Unsheltered & Prospects");
        Assert.assertTrue(isColumnDisplayed, "'Client Category' column is not displayed in exported excel");
    }

    //H20-729	As a user I want to search on "Engaged Unsheltered Prospects and Other Prospect" KPI tile details link using
    // two new client categories “Engaged Unsheltered Prospect"&"Other Prospect"
    @Test()
    public static void VerifyEngagedUnshelteredDatagridSearchFunctionality() {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Engaged Unsheltered Tile is displayed
        boolean isEngagedUnshelteredTileDisplayed = DailyOutreach.isEngagedUnsheltedProspectTileDisplayed();
        Assert.assertTrue(isEngagedUnshelteredTileDisplayed, "Engaged Unsheltered prospect and other prospect tile is not displayed on dashboard");

        //Click on Details button for 'Engaged Unsheltered' tile
        DailyOutreach.clickOnDetailsButtonForEngagedUnshelteredTile();

        //Verify on clicking Details button Engaged Unsheltered data pane is displayed
        boolean isDataPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForEngagedUnsheltered();
        Assert.assertTrue(isDataPaneDisplayed, "Data pane is not displayed for Engaged Unsheltered Prospect and Other Prospect");

        //This will perform search with value 'Engaged Unsheltered Prospect' and verify that Client Category column contains only 'Engaged Unshelctered Prospect' data
        DailyOutreach.searchFor("Engaged Unsheltered Prospect");
        boolean isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //This will perform search with value 'Engaged' and verify that Client Category column contains only 'Engaged Unshelctered Prospect' data
        DailyOutreach.searchFor("Engaged");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //This will perform search with value 'Unsheltered' and verify that Client Category column contains only 'Engaged Unshelctered Prospect' data
        DailyOutreach.searchFor("Unsheltered");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //This will perform search with value 'Unsheltered Prospect' and verify that Client Category column contains only 'Engaged Unshelctered Prospect' data
        DailyOutreach.searchFor("Unsheltered Prospect");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //This will perform search with value 'Engaged Unsheltered' and verify that Client Category column contains only 'Engaged Unshelctered Prospect' data
        DailyOutreach.searchFor("Engaged Unsheltered");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //This will perform search with value 'Other Prospect' and verify that Client Category column contains only 'Other' data
        DailyOutreach.searchFor("Other Prospect");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Other Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Other Prospect' data is displayed fpr Client Category column");

        //This will perform search with value 'Other' and verify that Client Category column contains only 'Other Prospect' data
        DailyOutreach.searchFor("Other");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Other Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Other Prospect' data is displayed fpr Client Category column");

        //This will perform search with value 'Prospect' and verify that Client Category column contains either 'Engaged Unsheltered Prospect' or 'Other Prospect' data
        DailyOutreach.searchFor("Prospect");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect,Other Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospect' or 'Other Prospect' data is displayed fpr Client Category column");
    }

    //H20-767	As a User,when I excel export from Current location of Clients and Prospects details grid,I want to be able to export the Client records of
    // Caseload,Engaged Unsheltered Prospect and Other Prospects from the grid.
    @Test()
    public static void VerifyCurrentLocationTileExportFunctionality() throws IOException, InterruptedException {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //This will check that 'Current location of client & Prospects' tile is displayed
        boolean currentLocationTileDisplayed = DailyOutreach.verifyCurrentLocationTileDisplayed();
        Assert.assertTrue(currentLocationTileDisplayed, "'Current Location of client & Prospects' tile is not displayed");

        //Click on Details button on Current location for clients tile
        DailyOutreach.clickOnDetailsButtonForCurrentLocationTile();

        //This will verify that on clicking Details button, data pane is displayed
        boolean isPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForCurrentLocation();
        Assert.assertTrue(isPaneDisplayed, "Data pane is not displayed for 'Current location of client & Prospects' section");

        //This code will perform validation on Columns in Data Grid and Exported excel for current view, it will also match the record count in exported excel is matching total records as per application
        int fileCountBefore = Helpers.countFilesInDownloadFolder();
        ViewReports.exportAllRecordsToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take total record count from pagination and compare it with total records count in exported excel
        int allExpectedRecordsCount = ViewReports.getTotalRecordsCount();
        int allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Cur Loc of Clients and Pros");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of total record count");

        //Get list of all columns from column picker
        DailyOutreach.clickOnColumnPicker();
        List<String> columnPickerColumnOptions = DailyOutreach.getAllColumnPickerColumns();
        List<String> visibleColumns = DailyOutreach.getCheckedColumnPickerColumns();
        DailyOutreach.clickOnColumnPicker();

        //This will verify that all columns are displayed in exported excel
        boolean allColumnsDisplayedInExcel = DailyOutreach.verifyColumnsDisplayedInDownloadedExcel("Cur Loc of Clients and Pros", columnPickerColumnOptions);
        Assert.assertTrue(allColumnsDisplayedInExcel, "Columns does not match between exported excel(for All records) and all column options");

        //Select 5 records per page
        ViewReports.changeRecordsPerPageTo("5");

        //This code will perform validation on Columns in Data Grid and Exported excel for current view, it will also match the record count in exported excel is matching total records as per application
        fileCountBefore = Helpers.countFilesInDownloadFolder();
        ViewReports.exportCurrentViewToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take count of records displayed in current data grid and compare it with records count in exported excel
        allExpectedRecordsCount = ViewReports.getVisibleRecordsCount();
        allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Cur Loc of Clients and Pros");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of record count in current view");

        //This will verify that all visible columns are displayed in exported excel
        boolean visibleColumnsDisplayedInExcel = DailyOutreach.verifyColumnsDisplayedInDownloadedExcel("Cur Loc of Clients and Pros", visibleColumns);
        Assert.assertTrue(visibleColumnsDisplayedInExcel, "Columns does not match between exported excel(for Current view) and all column options");
    }

    //H20-760	As a user I want to see "Client Category" added as column after "Client Type" & before "Current Placement" column & as a field value to
    // be selected from column picker on grid for Daily Dashboard -Current Location of Clients and Prospect KPI Tile .
    @Test()
    public static void VerifyClientCategoryColumnInCurrentLocationTile() {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Current location Tile is displayed
        boolean isTileDisplayed = DailyOutreach.verifyCurrentLocationTileDisplayed();
        Assert.assertTrue(isTileDisplayed, "Current location of client & prospects tile is not displayed on dashboard");

        //Click on Details button in Current location tile
        DailyOutreach.clickOnDetailsButtonForCurrentLocationTile();

        //Verify on clicking Details button,  Current location data pane is displayed
        boolean isDataPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForCurrentLocation();
        Assert.assertTrue(isDataPaneDisplayed, "Data pane is not displayed for Engaged Unsheltered Prospect and Other Prospect");

        //Verify that Client category column is displayed in data grid
        boolean isDisplayed = DailyOutreach.verifyClientCategoryColumnIsDisplayed();
        Assert.assertTrue(isDisplayed, "'Client Category' column is not displayed");

        //Verify that client category column is after client type column and before current placement
        boolean isInPlace = DailyOutreach.verifyClientCategoryColumnPosition();
        Assert.assertTrue(isInPlace, "Client Category column is not in place");

        //Get list of all columns from column picker
        DailyOutreach.clickOnColumnPicker();
        boolean isInPlaceInCOlumnPicker = DailyOutreach.verifyPositionOfClientCategoryColumnInColumnPicker();
        DailyOutreach.clickOnColumnPicker();
        Assert.assertTrue(isInPlaceInCOlumnPicker, "Client Category column is not in place in column picker");

        //This will verify that Client Category is pre-selected in column picker
        boolean isPreselected = DailyOutreach.isClientCategorySelected();
        Assert.assertTrue(isPreselected, "'Client Category' is not pre-selected by default");

        //This will verify that de-selecting Client category column in column picker, column will be removed from grid
        boolean isDeselected = DailyOutreach.deselectClientCategoryColumn();
        Assert.assertTrue(isDeselected, "'Client Category' column is not removed from data grid");

        //This will verify that selecting Client category column in column picker,column will be displayed in grid
        boolean isSelected = DailyOutreach.selectClientCategoryColumn();
        Assert.assertTrue(isSelected, "'Client Category' column is not displayed");
    }

    //H20-758	As a User,when I click on the Current Location of Clients and Prospects KPI Tile details link,I want the details grid to load
    // the Client records of Caseload,Engaged Unsheltered Prospects and Other Prospect Client records.
    @Test()
    public static void VerifyClientCategoryColumnDataInCurrentLocationTile() {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Current location Tile is displayed
        boolean isTileDIsplayed = DailyOutreach.verifyCurrentLocationTileDisplayed();
        Assert.assertTrue(isTileDIsplayed, "Current location tile is not displayed on dashboard");

        //Click on Details button for current location tile
        DailyOutreach.clickOnDetailsButtonForCurrentLocationTile();

        //Verify on clicking Details button Current location data pane is displayed
        boolean isDataPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForCurrentLocation();
        Assert.assertTrue(isDataPaneDisplayed, "Data pane is not displayed for  Current Location of Clients & Prospects");

        //THis will verify if client category column is displayed on data grid
        boolean isDisplayed = DailyOutreach.verifyClientCategoryColumnIsDisplayed();
        Assert.assertTrue(isDisplayed, "'Client Category' column is not displayed");

        //This will verify if Current placement column is displayed
        boolean verifyCurrentPlacementColumn = DailyOutreach.verifyCurrentPlacementColumnIsDisplayed();
        Assert.assertTrue(verifyCurrentPlacementColumn, "'Current Placement' column is not displayed");

        //This will verify that total count displayed on Current location for client & Prospects
        boolean countMatched = DailyOutreach.verifyTotalCountMatchesGridRecordCountForClientLocationTile();
        Assert.assertTrue(countMatched, "Count on tile does not matched with records count on data grid");

        //This will verify that Client category column does contain only 'Engaged Unsheltered Prospect','Other Prospect','Caseload'
        boolean verifyClientCategoryData = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect,Other Prospect,Caseload");
        Assert.assertTrue(verifyClientCategoryData, "Other than 'Engaged Unsheltered Prospect','Other Prospect','Caseload' data is displayed in Client category column");
    }

    //H20-748 As a user,when I click on Current location of Clients and Prospects KPI Tile details link,I want to see Client Category field in the details grid by default.
    @Test()
    public static void VerifyCurrentLocationDataGridFunctionality() throws IOException, InterruptedException {
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Currrent location Tile is displayed
        boolean isTileDisplayed = DailyOutreach.verifyCurrentLocationTileDisplayed();
        Assert.assertTrue(isTileDisplayed, "'Current location of client & Prospects' tile is not displayed on dashboard");

        //Click on Details button on Current location tile
        DailyOutreach.clickOnDetailsButtonForCurrentLocationTile();

        //Verify on clicking Details button 'Current location of client & Prospects' pane is displayed
        boolean isDataPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForCurrentLocation();
        Assert.assertTrue(isDataPaneDisplayed, "Data pane is not displayed for 'Current location of client & Prospects'");

        //Verify total records count displayed on tile is same as total records in data grid
        boolean isRecordOnTileMatchGridDataCount = DailyOutreach.verifyTotalCountMatchesGridRecordCountForClientLocationTile();
        Assert.assertTrue(isRecordOnTileMatchGridDataCount, "Data displayed on tile does not matched the records count in pagination");

        //This method will iterate through each data grid page and verify that Client category column contains only 'Engaged Unsheltered Prospects' or 'Other Prospect' or 'Caseload'
        boolean isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect,Other Prospect,Caseload");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' or 'Other Prospect' or 'Caseload' data is displayed fpr Client Category column");


        //Verify that client category column is after client type column and before current placement
        boolean isInPlace = DailyOutreach.verifyClientCategoryColumnPosition();
        Assert.assertTrue(isInPlace, "Client Category column is not in place");


        //This code will perform validation on Columns in Data Grid and Exported excel for all records, it will also match the record count in exported excel is matching total records as per application
        int fileCountBefore = Helpers.countFilesInDownloadFolder();
        ViewReports.exportAllRecordsToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take total record count from pagination and compare it with total records count in exported excel
        int allExpectedRecordsCount = ViewReports.getTotalRecordsCount();
        int allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Cur Loc of Clients and Pros");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of total record count");

        //This will verify that 'Client Category' column is displayed in exported excel
        boolean isColumnDisplayed = DailyOutreach.isColumnDisplayedInExcel("Client Category", "Cur Loc of Clients and Pros");
        Assert.assertTrue(isColumnDisplayed, "'Client Category' column is not displayed in exported excel");

        //This will change Records to be displayed per page to 5
        ViewReports.changeRecordsPerPageTo("5");

        //This code will perform validation on Columns in Data Grid and Exported excel for current view, it will also match the record count in exported excel is matching total records as per application
        fileCountBefore = Helpers.countFilesInDownloadFolder();
        ViewReports.exportCurrentViewToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take count of records displayed in current data grid and compare it with records count in exported excel
        allExpectedRecordsCount = ViewReports.getVisibleRecordsCount();
        allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Cur Loc of Clients and Pros");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of record count in current view");

        //This will verify that 'Client Category' column is displayed in exported excel
        isColumnDisplayed = DailyOutreach.isColumnDisplayedInExcel("Client Category", "Cur Loc of Clients and Pros");
        Assert.assertTrue(isColumnDisplayed, "'Client Category' column is not displayed in exported excel");

        //Apply Advanced filter for Client Category = 'Other Prospect' and 'Engaged Unsheltered Prospect'
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        ClientList.clickOnRadioClientCategory();
        ClientList.clickOnClientCategorySubDropdown();
        ClientList.clickOnOtherProspectdRadio();
        ClientList.clickOnEngagedUnshelteredRadio();
        ClientList.clickOnApplyFilter();

        //This method will iterate through each data grid page and verify that Client category column contains only 'Engaged Unsheltered Prospects' or 'Other Prospect'
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect,Other Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' or 'Other Prospect' data is displayed fpr Client Category column");

        //This code will perform validation on Columns in Data Grid and Exported excel for current view, it will also match the record count in exported excel is matching total records as per application
        fileCountBefore = Helpers.countFilesInDownloadFolder();
        DailyOutreach.exportFilteredRecords();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take total record count from pagination and compare it with total records count in exported excel
        allExpectedRecordsCount = ViewReports.getTotalRecordsCount();
        allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Cur Loc of Clients and Pros");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of total record count");

        //This will verify that 'Client Category' column is displayed in exported excel
        isColumnDisplayed = DailyOutreach.isColumnDisplayedInExcel("Client Category", "Cur Loc of Clients and Pros");
        Assert.assertTrue(isColumnDisplayed, "'Client Category' column is not displayed in exported excel");

        //This will change Records to be displayed per page to 25
        ViewReports.changeRecordsPerPageTo("25");
        WebDriverTasks.wait(5);

        //This will perform search with value 'Engaged Unsheltered Prospect' and verify that Client Category column contains only 'Engaged Unshelctered Prospect' data
        DailyOutreach.searchFor("Engaged Unsheltered Prospect");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //This will perform search with value 'Engaged' and verify that Client Category column contains only 'Engaged Unshelctered Prospect' data
        DailyOutreach.searchFor("Engaged");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //This will perform search with value 'Unsheltered' and verify that Client Category column contains only 'Engaged Unshelctered Prospect' data
        DailyOutreach.searchFor("Unsheltered");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //This will perform search with value 'Unsheltered Prospect' and verify that Client Category column contains only 'Engaged Unshelctered Prospect' data
        DailyOutreach.searchFor("Unsheltered Prospect");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //This will perform search with value 'Engaged Unsheltered' and verify that Client Category column contains only 'Engaged Unshelctered Prospect' data
        DailyOutreach.searchFor("Engaged Unsheltered");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospects' data is displayed fpr Client Category column");

        //This will perform search with value 'Other Prospect' and verify that Client Category column contains only 'Other' data
        DailyOutreach.searchFor("Other Prospect");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Other Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Other Prospect' data is displayed fpr Client Category column");

        //This will perform search with value 'Other' and verify that Client Category column contains only 'Other Prospect' data
        DailyOutreach.searchFor("Other");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Other Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Other Prospect' data is displayed fpr Client Category column");

        //This will perform search with value 'Prospect' and verify that Client Category column contains either 'Engaged Unsheltered Prospect' or 'Other Prospect' data
        DailyOutreach.searchFor("Prospect");
        isExpectedDataDisplayed = DailyOutreach.verifyDataInClientCategoryColumn("Engaged Unsheltered Prospect,Other Prospect");
        Assert.assertTrue(isExpectedDataDisplayed, "Other than 'Engaged Unsheltered Prospect' or 'Other Prospect' data is displayed fpr Client Category column");


        //This will verify that Client Category is pre-selected in column picker
        boolean isPreselected = DailyOutreach.isClientCategorySelected();
        Assert.assertTrue(isPreselected, "'Client Category' is not pre-selected by default");

        //This will verify that de-selecting Client category column in column picker, column will be removed from grid
        boolean isDeselected = DailyOutreach.deselectClientCategoryColumn();
        Assert.assertTrue(isDeselected, "'Client Category' column is not removed from data grid");

        //This will verify that selecting Client category column in column picker,column will be displayed in grid
        boolean isSelected = DailyOutreach.selectClientCategoryColumn();
        Assert.assertTrue(isSelected, "'Client Category' column is not displayed");
    }

    //H20-726	Rename the Current Home-Stat Client Location KPI tile on Daily Outreach Dashboard to Current Location of Clients and Prospects
    @Test()
    public static void VerifyCurrentLocationTileSettingDetailsFunctionality() throws InterruptedException, IOException {
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Current location Tile is displayed
        boolean isTileDisplayed = DailyOutreach.verifyCurrentLocationTileDisplayed();
        Assert.assertTrue(isTileDisplayed, "'Current location of client & Prospects' tile is not displayed on dashboard");

        //Click on Details button for Current location tile
        DailyOutreach.clickOnDetailsButtonForCurrentLocationTile();

        //Verify on clicking Details button 'Current location of client & Prospects' pane is displayed
        boolean isDataPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForCurrentLocation();
        Assert.assertTrue(isDataPaneDisplayed, "Data pane is not displayed for 'Current location of client & Prospects'");

        //This code will perform validation on Columns in Data Grid and Exported excel for all records, it will also match the record count in exported excel is matching total records as per application
        int fileCountBefore = Helpers.countFilesInDownloadFolder();
        ViewReports.exportAllRecordsToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take total record count from pagination and compare it with total records count in exported excel
        int allExpectedRecordsCount = ViewReports.getTotalRecordsCount();
        int allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Cur Loc of Clients and Pros");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of total record count");

        //This will change Records to be displayed per page to 5
        ViewReports.changeRecordsPerPageTo("5");

        //This code will perform validation on Columns in Data Grid and Exported excel for current view, it will also match the record count in exported excel is matching total records as per application
        fileCountBefore = Helpers.countFilesInDownloadFolder();
        ViewReports.exportCurrentViewToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take count of records displayed in current data grid and compare it with records count in exported excel
        allExpectedRecordsCount = ViewReports.getVisibleRecordsCount();
        allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Cur Loc of Clients and Pros");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of record count in current view");

        //Click on Setting button on Current location tile
        DailyOutreach.clickOnSettingsButtonForCurrentLocationTile();

        //Verify Setting options contains 'Daily' option
        boolean isDailyOptionDisplayed = DailyOutreach.verifySettingOptionOnCurrentLocationTile("Daily");
        Assert.assertTrue(isDailyOptionDisplayed, "Cannot find settings option 'Daily'");

        //Verify Setting options contains 'Weekly' option
        boolean isWeeklyOptionDisplayed = DailyOutreach.verifySettingOptionOnCurrentLocationTile("Weekly");
        Assert.assertTrue(isWeeklyOptionDisplayed, "Cannot find settings option 'Weekly'");

        //Verify Setting options contains 'Monthly' option
        boolean isMonthlyOptionDisplayed = DailyOutreach.verifySettingOptionOnCurrentLocationTile("Monthly");
        Assert.assertTrue(isMonthlyOptionDisplayed, "Cannot find settings option 'Monthly'");

        //Click on 'Daily' option and select date 07/03/2020
        DailyOutreach.filterByDaily("07/30/2020");

        //Verify total records count displayed on tile is same as total records in data grid
        boolean isRecordOnTileMatchGridDataCount = DailyOutreach.verifyTotalCountMatchesGridRecordCountForClientLocationTile();
        Assert.assertTrue(isRecordOnTileMatchGridDataCount, "Data displayed on tile does not matched the records count in pagination");

        //Click on 'Weekly' option and select date 07/03/2020
        DailyOutreach.filterByWeekly("07/30/2020");

        //Verify total records count displayed on tile is same as total records in data grid
        isRecordOnTileMatchGridDataCount = DailyOutreach.verifyTotalCountMatchesGridRecordCountForClientLocationTile();
        Assert.assertTrue(isRecordOnTileMatchGridDataCount, "Data displayed on tile does not matched the records count in pagination");

        //Click on 'Monthly' option and select year and month
        DailyOutreach.filterByMonthly("2020", "July");

        //Verify total records count displayed on tile is same as total records in data grid
        isRecordOnTileMatchGridDataCount = DailyOutreach.verifyTotalCountMatchesGridRecordCountForClientLocationTile();
        Assert.assertTrue(isRecordOnTileMatchGridDataCount, "Data displayed on tile does not matched the records count in pagination");

        //Click on Setting button on tile
        DailyOutreach.clickOnSettingsButtonForCurrentLocationTile();
    }

    //H20-957 As a User,I want to see the DOB Date Picker field to populate the years from 1900,across the application.
    @Test()
    public static void VerifyStartingYearForAllDOBFields() {
        //Close notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Click on Details button and verify that pane is displayed for 'HOME-STAT Clients'
        DailyOutreach.clickOnDetailsButtonFor("HOME-STAT Clients");
        boolean dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("HOME-STAT Clients");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'HOME-STAT Clients'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        boolean asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'HOME-STAT Clients'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();


        //Click on Details button and verify that pane is displayed for 'Engaged Unsheltered Prospects & Other Prospects'
        DailyOutreach.clickOnDetailsButtonFor("Engaged Unsheltered Prospects & Other Prospects");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("Engaged Unsheltered Prospects & Other Prospects");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'Engaged Unsheltered Prospects & Other Prospects'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'Engaged Unsheltered Prospects & Other Prospects'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();


        //Click on Details button and verify that pane is displayed for 'Engagements'
        DailyOutreach.clickOnDetailsButtonFor("Engagements");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("Engagements");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'Engagements'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'Engagements'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();


        //Click on Details button and verify that pane is displayed for 'Current Location of Clients & Prospects'
        DailyOutreach.clickOnDetailsButtonFor("Current Location of Clients & Prospects");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("Current Location of Clients & Prospects");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'Current Location of Clients & Prospects'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'Current Location of Clients & Prospects'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();


        //Click on Details button and verify that pane is displayed for 'HOME-STAT Clients Placed'
        DailyOutreach.clickOnDetailsButtonFor("HOME-STAT Clients Placed");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("HOME-STAT Clients Placed");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'HOME-STAT Clients Placed'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'HOME-STAT Clients Placed'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();


        //Click on Details button and verify that pane is displayed for 'Housing Packet/Subsidy in Process'
        DailyOutreach.clickOnDetailsButtonFor("Housing Packet/Subsidy in Process");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("Housing Packet/Subsidy in Process");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'Housing Packet/Subsidy in Process'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'Housing Packet/Subsidy in Process'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();


        //Click on Details button and verify that pane is displayed for 'Active Housing Packet/Subsidy'
        DailyOutreach.clickOnDetailsButtonFor("Active Housing Packet/Subsidy");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("Active Housing Packet/Subsidy");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'Active Housing Packet/Subsidy'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'Active Housing Packet/Subsidy'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();


        //Click on Details button and verify that pane is displayed for 'In Permanent Housing & Receiving Aftercare'
        DailyOutreach.clickOnDetailsButtonFor("In Permanent Housing & Receiving Aftercare");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("In Permanent Housing & Receiving Aftercare");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'In Permanent Housing & Receiving Aftercare'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'In Permanent Housing & Receiving Aftercare'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();

        //Click on Details button and verify that pane is displayed for 'New HOME-STAT Clients'
        DailyOutreach.clickOnDetailsButtonFor("New HOME-STAT Clients");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("New HOME-STAT Clients");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'New HOME-STAT Clients'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'New HOME-STAT Clients'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();


        //Click on Details button and verify that pane is displayed for 'HOME-STAT Clients Case End Reason'
        DailyOutreach.clickOnDetailsButtonFor("HOME-STAT Clients Case End Reason");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("Clients Enrollment Ended");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'HOME-STAT Clients Case End Reason'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'HOME-STAT Clients Case End Reason'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();

        //Click on Details button and verify that pane is displayed for 'Clients Vulnerability Status'
        DailyOutreach.clickOnDetailsButtonFor("Clients Vulnerability Status");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("Clients Vulnerability Status");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'Clients Vulnerability Status'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'Clients Vulnerability Status'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();

        //Click on Details button and verify that pane is displayed for 'Clients Chronicity Status'
        DailyOutreach.clickOnDetailsButtonFor("Clients Chronicity Status");
        dataGridDisplayed = DailyOutreach.verifyDataPaneIsDisplayedFor("Clients Chronicity Status");
        Assert.assertTrue(dataGridDisplayed, "Data grid is not displayed for 'Clients Chronicity Status'");
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'Clients Chronicity Status'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();

        //To navigate on client list menu
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnClientListMenuItem();
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'Clients List'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();

        //Click on first client id link to navigate to clients information page
        ClientList.clickonFirstClientID();
        //Edit client information
        ClientList.clickOnEditClient();
        //Verify that year for DOB field starts with 1900
        asExpected = ClientInformation.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for DOB field on client information page");

        //To navigate on client list menu
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnMyCaseLoadMenuItem();
        //Select Filter by Date Of Birth
        ClientList.clickOnAdvancedFilter();
        ClientList.clickOnClientCategoryFilterButton();
        ClientList.clearExistingSelection();
        DailyOutreach.clickOnRadioDateOfBirth();
        //To verify that DOB year starts with '1900'
        asExpected = DailyOutreach.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB Year does not starts with 1900 for 'My Caseload'");
        //This will close filter popup
        DailyOutreach.closeAdvancedFilterPopup();

        //To navigate on Add Client page
        Base.clickOnAddClientMenuItem();
        //To verify that DOB year starts with '1900'
        asExpected = AddClient.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB year does not starts with 1900 for DOB in 'Duplicate Search' pane on Add Client page");
        //Collapse Duplicate search pane
        AddClient.collapseDuplicateSearchPane();
        //Collapse Client search pane
        AddClient.expandClientSearchPane();
        //To verify that DOB year starts with '1900'
        asExpected = AddClient.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB year does not starts with 1900 for DOB in 'Client Search' pane on Add Client page");

        //To navigate on Add Engagement page
        Base.clickOnEngagementsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddEngagementsMenuItem();
        //To verify that DOB year starts with '1900'
        asExpected = AddEngagement.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB year does not starts with 1900 for DOB in 'Duplicate Search' pane on Add Engagement page");
        //Collapse Duplicate search pane
        AddEngagement.collapseDuplicateSearchPane();
        //Collapse Client search pane
        AddEngagement.expandClientSearchPane();
        //To verify that DOB year starts with '1900'
        asExpected = AddEngagement.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB year does not starts with 1900 for DOB in 'Client Search' pane on Add Engagement page");

        //To navigate on Create Report page
        Base.clickOnReports();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnCustomReport();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnCreateReport();
        //Expand Demographics Pane
        CreateReport.expandDemographicsPane();
        //Mark Date of birth checkbox
        CreateReport.markClientDOBCheckBox();
        //To verify that DOB year starts with '1900'
        asExpected = CreateReport.verifyDOBYearStartsWith("1900");
        Assert.assertTrue(asExpected, "DOB year does not starts with 1900 for DOB in 'Demographics' pane on Create reports page");
    }

    //H20-734 As a User,when I open the Daily Outreach Dashboard,I want the "Current Location of Clients and Prospects" to display the new breakdown values and their counts respectively on the tile,as described below.
    @Test()
    public static void VerifyBreakDownValuesOnCurrentLocationTile() {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //This will check that 'Current location of client & Prospects' tile is displayed
        boolean currentLocationTileDisplayed = DailyOutreach.verifyCurrentLocationTileDisplayed();
        Assert.assertTrue(currentLocationTileDisplayed, "'Current Location of client & Prospects' tile is not displayed");

        //This will verify breakup sequence for 'Current location of client & prospects' is as expected
        boolean isInSequence = DailyOutreach.verifyBreakupSequenceForCurrentLocationTile("Unsheltered,On Street & Other Settings,In Transitional Settings,In Permanent Housing & Receiving Aftercare,Undetermined Housing Status");
        Assert.assertTrue(isInSequence, "Breakup is not in sequence for 'Current location of client & prospects' tile");

        //Verify that all breakups contains information '(i)' icon
        boolean isInfoIconDisplayed = DailyOutreach.verifyAllBreakupComponentsAreWithInforButtonForCurrentLocationTile();
        Assert.assertTrue(isInfoIconDisplayed, "Any/All Breakup component/s displayed without Info icon(i)");

        //To get all clients counts for all the breakups
        int totalUnshelteredCount = DailyOutreach.getBreakdownCountFor("Unsheltered");
        int onstreetAndOtherSettingCount = DailyOutreach.getBreakdownCountFor("On Street & Other Settings");
        int underminingHousingCount = DailyOutreach.getBreakdownCountFor("Undetermined Housing Status");
        int intransitionalSettingsCount = DailyOutreach.getBreakdownCountFor("In Transitional Settings");
        int permanentHousingCount = DailyOutreach.getBreakdownCountFor("In Permanent Housing & Receiving Aftercare");

        //To verify Unsheltered Count
        Assert.assertTrue(totalUnshelteredCount == (onstreetAndOtherSettingCount + underminingHousingCount), String.format("Total unsheltered count displayed %d, expected unsheltered count %d", totalUnshelteredCount, (onstreetAndOtherSettingCount + underminingHousingCount)));

        //Click on Details button on Current location for clients tile
        DailyOutreach.clickOnDetailsButtonForCurrentLocationTile();

        //Advanced Filter 'Current Placement' data grid by 'On Street' and 'Other'
        DailyOutreach.advancedFilterBy("Current Placement", "On Street,Other", true);
        //Advanced Filter 'Client Category' data grid by 'Engaged Unsheltered Prospect'
        DailyOutreach.advancedFilterBy("ClientCategory", "Engaged Unsheltered Prospect", false);
        //Get Count displayed on data grid
        int gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'On Street and Other Settings' breakup count
        //TODO It's existing defect with application, uncomment below Assert when defect is resolved
        //Assert.assertTrue(onstreetAndOtherSettingCount==gridDataCount,String.format("On Street and Other Settings count displayed on tile : %d, actual record count in data grid : %d",onstreetAndOtherSettingCount,gridDataCount));

        //Advanced Filter 'Current Placement' data grid by 'Transitional Housing'
        DailyOutreach.advancedFilterBy("Current Placement", "Transitional Housing", true);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'In Transitional Settings' breakup count
        Assert.assertTrue(intransitionalSettingsCount == gridDataCount, String.format("In Transitional Settings count displayed on tile : %d, actual record count in data grid : %d", intransitionalSettingsCount, gridDataCount));

        //Advanced Filter 'Current Placement' data grid by 'Permanent Placement'
        DailyOutreach.advancedFilterBy("Current Placement", "Permanent Placement", true);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'In Permanent Housing & Receiving Aftercare ' breakup count
        Assert.assertTrue(permanentHousingCount == gridDataCount, String.format("In Permanent Housing & Receiving Aftercare count displayed on tile : %d, actual record count in data grid : %d", permanentHousingCount, gridDataCount));

        //Advanced Filter 'Current Placement' data grid by 'On Street' and 'Other'
        DailyOutreach.advancedFilterBy("Current Placement", "On Street,Other", true);
        //Advanced Filter 'Client Category' data grid by 'Other Prospect'
        DailyOutreach.advancedFilterBy("ClientCategory", "Other Prospect", false);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'Undetermined Housing Status' breakup count
        Assert.assertTrue(underminingHousingCount == gridDataCount, String.format("Undetermined Housing Status count displayed on tile : %d, actual record count in data grid : %d", underminingHousingCount, gridDataCount));

        //Click on Setting button on Current location tile
        DailyOutreach.clickOnSettingsButtonForCurrentLocationTile();
        //Click on 'Daily' option and select date 07/03/2020
        DailyOutreach.filterByDaily("07/30/2020");

        //To get all clients counts for all the breakups
        totalUnshelteredCount = DailyOutreach.getBreakdownCountFor("Unsheltered");
        onstreetAndOtherSettingCount = DailyOutreach.getBreakdownCountFor("On Street & Other Settings");
        underminingHousingCount = DailyOutreach.getBreakdownCountFor("Undetermined Housing Status");
        intransitionalSettingsCount = DailyOutreach.getBreakdownCountFor("In Transitional Settings");
        permanentHousingCount = DailyOutreach.getBreakdownCountFor("In Permanent Housing & Receiving Aftercare");

        //To verify Unsheltered Count
        Assert.assertTrue(totalUnshelteredCount == (onstreetAndOtherSettingCount + underminingHousingCount), String.format("Total unsheltered count displayed %d, expected unsheltered count %d", totalUnshelteredCount, (onstreetAndOtherSettingCount + underminingHousingCount)));

        //Click on Details button on Current location for clients tile
        DailyOutreach.clickOnDetailsButtonForCurrentLocationTile();

        //Advanced Filter 'Current Placement' data grid by 'On Street' and 'Other'
        DailyOutreach.advancedFilterBy("Current Placement", "On Street,Other", true);
        //Advanced Filter 'Client Category' data grid by 'Engaged Unsheltered Prospect'
        DailyOutreach.advancedFilterBy("ClientCategory", "Engaged Unsheltered Prospect", false);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'On Street and Other Settings' breakup count
        //TODO It's existing defect with application, uncomment below Assert when defect is resolved
        //Assert.assertTrue(onstreetAndOtherSettingCount==gridDataCount,String.format("On Street and Other Settings count displayed on tile : %d, actual record count in data grid : %d",onstreetAndOtherSettingCount,gridDataCount));


        //Advanced Filter 'Current Placement' data grid by 'Transitional Housing'
        DailyOutreach.advancedFilterBy("Current Placement", "Transitional Housing", true);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'In Transitional Settings' breakup count
        Assert.assertTrue(intransitionalSettingsCount == gridDataCount, String.format("In Transitional Settings count displayed on tile : %d, actual record count in data grid : %d", intransitionalSettingsCount, gridDataCount));

        //Advanced Filter 'Current Placement' data grid by 'Permanent Placement'
        DailyOutreach.advancedFilterBy("Current Placement", "Permanent Placement", true);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'In Permanent Housing & Receiving Aftercare ' breakup count
        Assert.assertTrue(permanentHousingCount == gridDataCount, String.format("In Permanent Housing & Receiving Aftercare count displayed on tile : %d, actual record count in data grid : %d", permanentHousingCount, gridDataCount));


        //Advanced Filter 'Current Placement' data grid by 'On Street' and 'Other'
        DailyOutreach.advancedFilterBy("Current Placement", "On Street,Other", true);
        //Advanced Filter 'Client Category' data grid by 'Other Prospect'
        DailyOutreach.advancedFilterBy("ClientCategory", "Other Prospect", false);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'Undetermined Housing Status' breakup count
        Assert.assertTrue(underminingHousingCount == gridDataCount, String.format("Undetermined Housing Status count displayed on tile : %d, actual record count in data grid : %d", underminingHousingCount, gridDataCount));


        //Click on Setting button on Current location tile
        DailyOutreach.clickOnSettingsButtonForCurrentLocationTile();
        //Click on 'Weekly' option and select date 07/03/2020
        DailyOutreach.filterByWeekly("07/30/2020");

        //To get all clients counts for all the breakups
        totalUnshelteredCount = DailyOutreach.getBreakdownCountFor("Unsheltered");
        onstreetAndOtherSettingCount = DailyOutreach.getBreakdownCountFor("On Street & Other Settings");
        underminingHousingCount = DailyOutreach.getBreakdownCountFor("Undetermined Housing Status");
        intransitionalSettingsCount = DailyOutreach.getBreakdownCountFor("In Transitional Settings");
        permanentHousingCount = DailyOutreach.getBreakdownCountFor("In Permanent Housing & Receiving Aftercare");

        //To verify Unsheltered Count
        Assert.assertTrue(totalUnshelteredCount == (onstreetAndOtherSettingCount + underminingHousingCount), String.format("Total unsheltered count displayed %d, expected unsheltered count %d", totalUnshelteredCount, (onstreetAndOtherSettingCount + underminingHousingCount)));

        //Click on Details button on Current location for clients tile
        DailyOutreach.clickOnDetailsButtonForCurrentLocationTile();

        //Advanced Filter 'Current Placement' data grid by 'On Street' and 'Other'
        DailyOutreach.advancedFilterBy("Current Placement", "On Street,Other", true);
        //Advanced Filter 'Client Category' data grid by 'Engaged Unsheltered Prospect'
        DailyOutreach.advancedFilterBy("ClientCategory", "Engaged Unsheltered Prospect", false);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'On Street and Other Settings' breakup count
        //TODO It's existing defect with application, uncomment below Assert when defect is resolved
        //Assert.assertTrue(onstreetAndOtherSettingCount==gridDataCount,String.format("On Street and Other Settings count displayed on tile : %d, actual record count in data grid : %d",onstreetAndOtherSettingCount,gridDataCount));


        //Advanced Filter 'Current Placement' data grid by 'Transitional Housing'
        DailyOutreach.advancedFilterBy("Current Placement", "Transitional Housing", true);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'In Transitional Settings' breakup count
        Assert.assertTrue(intransitionalSettingsCount == gridDataCount, String.format("In Transitional Settings count displayed on tile : %d, actual record count in data grid : %d", intransitionalSettingsCount, gridDataCount));


        //Advanced Filter 'Current Placement' data grid by 'Permanent Placement'
        DailyOutreach.advancedFilterBy("Current Placement", "Permanent Placement", true);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'In Permanent Housing & Receiving Aftercare ' breakup count
        Assert.assertTrue(permanentHousingCount == gridDataCount, String.format("In Permanent Housing & Receiving Aftercare count displayed on tile : %d, actual record count in data grid : %d", permanentHousingCount, gridDataCount));


        //Advanced Filter 'Current Placement' data grid by 'On Street' and 'Other'
        DailyOutreach.advancedFilterBy("Current Placement", "On Street,Other", true);
        //Advanced Filter 'Client Category' data grid by 'Other Prospect'
        DailyOutreach.advancedFilterBy("ClientCategory", "Other Prospect", false);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'Undetermined Housing Status' breakup count
        Assert.assertTrue(underminingHousingCount == gridDataCount, String.format("Undetermined Housing Status count displayed on tile : %d, actual record count in data grid : %d", underminingHousingCount, gridDataCount));


        //Click on Setting button on Current location tile
        DailyOutreach.clickOnSettingsButtonForCurrentLocationTile();
        //Click on 'Weekly' option and select date 07/03/2020
        DailyOutreach.filterByMonthly("2020", "Aug.");

        //To get all clients counts for all the breakups
        totalUnshelteredCount = DailyOutreach.getBreakdownCountFor("Unsheltered");
        onstreetAndOtherSettingCount = DailyOutreach.getBreakdownCountFor("On Street & Other Settings");
        underminingHousingCount = DailyOutreach.getBreakdownCountFor("Undetermined Housing Status");
        intransitionalSettingsCount = DailyOutreach.getBreakdownCountFor("In Transitional Settings");
        permanentHousingCount = DailyOutreach.getBreakdownCountFor("In Permanent Housing & Receiving Aftercare");

        //To verify Unsheltered Count
        Assert.assertTrue(totalUnshelteredCount == (onstreetAndOtherSettingCount + underminingHousingCount), String.format("Total unsheltered count displayed %d, expected unsheltered count %d", totalUnshelteredCount, (onstreetAndOtherSettingCount + underminingHousingCount)));

        //Click on Details button on Current location for clients tile
        DailyOutreach.clickOnDetailsButtonForCurrentLocationTile();

        //Advanced Filter 'Current Placement' data grid by 'On Street' and 'Other'
        DailyOutreach.advancedFilterBy("Current Placement", "On Street,Other", true);
        //Advanced Filter 'Client Category' data grid by 'Engaged Unsheltered Prospect'
        DailyOutreach.advancedFilterBy("ClientCategory", "Engaged Unsheltered Prospect", false);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'On Street and Other Settings' breakup count
        //TODO It's existing defect with application, uncomment below Assert when defect is resolved
        //Assert.assertTrue(onstreetAndOtherSettingCount==gridDataCount,String.format("On Street and Other Settings count displayed on tile : %d, actual record count in data grid : %d",onstreetAndOtherSettingCount,gridDataCount));


        //Advanced Filter 'Current Placement' data grid by 'Transitional Housing'
        DailyOutreach.advancedFilterBy("Current Placement", "Transitional Housing", true);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'In Transitional Settings' breakup count
        Assert.assertTrue(intransitionalSettingsCount == gridDataCount, String.format("In Transitional Settings count displayed on tile : %d, actual record count in data grid : %d", intransitionalSettingsCount, gridDataCount));


        //Advanced Filter 'Current Placement' data grid by 'Permanent Placement'
        DailyOutreach.advancedFilterBy("Current Placement", "Permanent Placement", true);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'In Permanent Housing & Receiving Aftercare ' breakup count
        Assert.assertTrue(permanentHousingCount == gridDataCount, String.format("In Permanent Housing & Receiving Aftercare count displayed on tile : %d, actual record count in data grid : %d", permanentHousingCount, gridDataCount));


        //Advanced Filter 'Current Placement' data grid by 'On Street' and 'Other'
        DailyOutreach.advancedFilterBy("Current Placement", "On Street,Other", true);
        //Advanced Filter 'Client Category' data grid by 'Other Prospect'
        DailyOutreach.advancedFilterBy("ClientCategory", "Other Prospect", false);
        //Get Count displayed on data grid
        gridDataCount = DailyOutreach.getTotalRecordsCount();
        //Verify that the count in data grid matches with count for 'Undetermined Housing Status' breakup count
        Assert.assertTrue(underminingHousingCount == gridDataCount, String.format("Undetermined Housing Status count displayed on tile : %d, actual record count in data grid : %d", underminingHousingCount, gridDataCount));
    }

    //H20-732 Current Location of Clients and Prospects KPI Tile total Count Info icon(i) description on the on Daily Outreach Dashboard to be updated as below.
    @Test()
    public static void VerifyCurrentLocationTileInfoDetails() {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //This will check that 'Current location of client & Prospects' tile is displayed
        boolean currentLocationTileDisplayed = DailyOutreach.verifyCurrentLocationTileDisplayed();
        Assert.assertTrue(currentLocationTileDisplayed, "'Current Location of client & Prospects' tile is not displayed");

        //Click on (i) button near total count on 'Current location of client & Prospects' tile amd verify that information popup is displayed
        DailyOutreach.clickOnCurrentLocationTileInfo();
        boolean isPopupDisplayed = DailyOutreach.verifyInfoPopupIsDisplayedForCurrentLocationTile();
        Assert.assertTrue(isPopupDisplayed, "On clicking (i) button on 'Current location of client & Prospects' tile information popup is not displayed");

        //Get expected popup text
        String expectedText = DailyOutreach.getExpectedCurrentLocationInfoText();

        //Get actual popup text
        String actualText = DailyOutreach.getInformationPopupText();

        //Verify that expected text matches with actual popup text
        boolean isMatching = DailyOutreach.verifyPopupTextMatches(expectedText, actualText);
        Assert.assertTrue(isMatching, "Actual 'Current location of client & Prospects' tile information text does not match with expected text");
    }

    //H20-931 As user I want to see the Email ID updated on Info Icon on all KPI Tiles within Daily Outreach Dashboard
    @Test()
    public static void VerifyEmailIDOnInformationPopup() {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        // Verify expected information icons buttons are displayed 'HOME-STAT Clients' tile
        boolean expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("HOME-STAT Clients", 4);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'HOME-STAT Clients'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for 'HOME-STAT Clients' tile
        boolean isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("HOME-STAT Clients", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'HOME-STAT Clients'");

        // Verify expected information icons buttons are displayed 'Engaged Unsheltered Prospects & Other Prospects' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("Engaged Unsheltered Prospects & Other Prospects", 3);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'Engaged Unsheltered Prospects & Other Prospects'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for 'Engaged Unsheltered Prospects & Other Prospects' tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("Engaged Unsheltered Prospects & Other Prospects", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'Engaged Unsheltered Prospects & Other Prospects'");

        // Verify expected information icons buttons are displayed 'Engagements' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("Engagements", 6);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'Engagements'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for 'Engagements' tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("Engagements", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'Engagements'");


        //TODO Information icon count on story is 4 which is 6 in QA environment
        //Verify expected information icons buttons are displayed 'Current Location of Clients & Prospects' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("Current Location of Clients & Prospects", 6);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'Current Location of Clients & Prospects'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for 'Current Location of Clients & Prospects' tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'Current Location of Clients & Prospects'");


        // Verify expected information icons buttons are displayed 'HOME-STAT Clients Placed' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("HOME-STAT Clients Placed", 4);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'HOME-STAT Clients Placed'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for 'HOME-STAT Clients Placed' tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("HOME-STAT Clients Placed", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'HOME-STAT Clients Placed'");


        // Verify expected information icons buttons are displayed 'Housing Packet/Subsidy in Process' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("Housing Packet/Subsidy in Process", 5);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'Housing Packet/Subsidy in Process'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for 'Housing Packet/Subsidy in Process' tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("Housing Packet/Subsidy in Process", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'Housing Packet/Subsidy in Process'");


        // Verify expected information icons buttons are displayed 'Active Housing Packet/Subsidy' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("Active Housing Packet/Subsidy", 5);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'Active Housing Packet/Subsidy'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for 'Active Housing Packet/Subsidy' tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("Active Housing Packet/Subsidy", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'Active Housing Packet/Subsidy'");


        // Verify expected information icons buttons are displayed 'In Permanent Housing & Receiving Aftercare' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("In Permanent Housing & Receiving Aftercare", 4);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'In Permanent Housing & Receiving Aftercare'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for ''In Permanent Housing & Receiving Aftercare tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("In Permanent Housing & Receiving Aftercare", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'In Permanent Housing & Receiving Aftercare'");


        // Verify expected information icons buttons are displayed 'New HOME-STAT Clients' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("New HOME-STAT Clients", 3);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'New HOME-STAT Clients'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for 'New HOME-STAT Clients' tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("New HOME-STAT Clients", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'New HOME-STAT Clients'");


        // Verify expected information icons buttons are displayed 'HOME-STAT Clients Case End Reason' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("HOME-STAT Clients Case End Reason", 3);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'HOME-STAT Clients Case End Reason'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for ''HOME-STAT Clients Case End Reason tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("HOME-STAT Clients Case End Reason", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'HOME-STAT Clients Case End Reason'");


        // Verify expected information icons buttons are displayed 'Clients Vulnerability Status' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("Clients Vulnerability Status", 4);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'Clients Vulnerability Status'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for 'Clients Vulnerability Status' tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("Clients Vulnerability Status", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'Clients Vulnerability Status'");


        // Verify expected information icons buttons are displayed 'Clients Chronicity Status' tile
        expectedNoOfInfoIconsDisplayed = DailyOutreach.verifyTileContainsInformationIcons("Clients Chronicity Status", 8);
        Assert.assertTrue(expectedNoOfInfoIconsDisplayed, "Expected #of information icons are not displayed for tile 'Clients Chronicity Status'");
        //Verify email 'mbamalui@dss.nyc.gov' is displayed on information popup for 'Clients Chronicity Status' tile
        isEmailDisplayed = DailyOutreach.verifyInformationPopupContains("Clients Chronicity Status", "mbamalui@dss.nyc.gov");
        Assert.assertTrue(isEmailDisplayed, "Expected email 'mbamalui@dss.nyc.gov' is not displayed on information popup for tile 'Clients Chronicity Status'");
    }

    //H20-949	As a User,I want to see Latest Contact date field as one of the default Columns in the Engaged Unsheltered Prospects and Other Prospects Details grid,so that I can view if relevant data is populated based on the Daily,weekly,Monthly filters selected.
    @Test()
    public static void VerifyLatestContactDateColumnInEngagedUnshelteredTile() throws ParseException, IOException {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Engaged Unsheltered Tile is displayed
        boolean isEngagedUnshelteredTileDisplayed = DailyOutreach.isEngagedUnsheltedProspectTileDisplayed();
        Assert.assertTrue(isEngagedUnshelteredTileDisplayed, "Engaged Unsheltered prospect and other prospect tile is not displayed on dashboard");

        //Click on Details button on Engaged Unsheltered tile
        DailyOutreach.clickOnDetailsButtonForEngagedUnshelteredTile();

        //Verify on clicking Details button Engaged Unsheltered data pane is displayed
        boolean isDataPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForEngagedUnsheltered();
        Assert.assertTrue(isDataPaneDisplayed, "Data pane is not displayed for Engaged Unsheltered Prospect and Other Prospect");

        //To verify that 'Latest Contact Date' column is displayed
        boolean isColumnDisplayed = DailyOutreach.verifyLatestContactDateColumnDisplayed();
        Assert.assertTrue(isColumnDisplayed, "'Latest Contact Date' column is not displayed");

        //To verify that 'Latest Contact Date' column is displayed after 'Client Category' column
        boolean isAtRightPlace = DailyOutreach.verifyLatestContactDateColumnPosition();
        Assert.assertTrue(isAtRightPlace, "'Latest Contact Date' column is not displayed after 'Client Category' column");

        //To verify that on clicking on 'Latest Contact Date' column data is sorted in ascending order
        DailyOutreach.clickOnLatestContactDateColumn();
        boolean isAscendingDate = DailyOutreach.verifyDateIsInAscendingOrder("Latest Contact Date");
        Assert.assertTrue(isAscendingDate, "'Latest Contact Date' data is not sorted in ascending order");

        //To verify that on clicking on 'Latest Contact Date' column data is sorted in descending order
        DailyOutreach.clickOnLatestContactDateColumn();
        boolean isDescendingDate = DailyOutreach.verifyDateIsInDescendingOrder("Latest Contact Date");
        Assert.assertTrue(isDescendingDate, "'Latest Contact Date' data is not sorted in descending order");

        //Verify search functionality for 'Latest Contact Date' column
        DailyOutreach.searchFor("08/01/2020");
        boolean isDataFiltered = DailyOutreach.verifyDataInLatestContactDateColumn("08/01/2020");
        Assert.assertTrue(isDataFiltered, "Data is not filtered as expected for 'Latest Contact Date' column");

        //Clear existing search
        DailyOutreach.searchFor("");

        //Verify advanced filter functionality for 'Latest Contact Date'
        DailyOutreach.advancedFilterBy("Lastest Contact Date", "08/01/2020", true);

        //Verify data is filtered as expected for 'Latest Contact Date' column
        isDataFiltered = DailyOutreach.verifyDataInLatestContactDateColumn("08/01/2020");
        Assert.assertTrue(isDataFiltered, "Data is not filtered as expected for 'Latest Contact Date' column");

        //Remove all advanced filters
        DailyOutreach.clearAdvancedFilter();

        //This code will perform validation on Columns in Data Grid and Exported excel for all records, it will also match the record count in exported excel is matching total records as per application
        int fileCountBefore = Helpers.countFilesInDownloadFolder();
        ViewReports.exportAllRecordsToExcel();
        Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
        WebDriverTasks.waitForAngularPendingRequests();
        WebDriverTasks.wait(2);
        int fileCountAfter = Helpers.countFilesInDownloadFolder();
        Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A file has not been downloaded saved to Downloads folder");

        //Take total record count from pagination and compare it with total records count in exported excel
        int allExpectedRecordsCount = ViewReports.getTotalRecordsCount();
        int allActualRecordsCount = DailyOutreach.getRecordCountInExportedExcel("Engaged Unsheltered & Prospects");
        Assert.assertTrue(allExpectedRecordsCount == allActualRecordsCount, "Exported record count are not matching that of total record count");

        //This will verify that 'Client Category' column is displayed in exported excel
        isColumnDisplayed = DailyOutreach.isColumnDisplayedInExcel("Latest Contact Date", "Engaged Unsheltered & Prospects");
        Assert.assertTrue(isColumnDisplayed, "'Latest Contact Date' column is not displayed in exported excel");

        //Verify that Latest contact date value matches with latest engagement date
        String latestContactDate = DailyOutreach.getLatestContactDateForFirstRecord();
        ClientList.clickonFirstClientID();
        String latestEngagementDate = ClientInformation.getLatestEngagementDate();
        Assert.assertTrue(latestContactDate.equals(latestEngagementDate), "Latest contact date does not match with latest engagement date");
    }

    //H20-990	As a User,I want to see the Latest Contact Date field in the Column Picker of the Engaged Unsheltered Prospects and Other Prospects details grid,so that I can select or unselect the field and view the data accordingly.
    @Test()
    public static void VerifyColumnPickerForLatestContactDateInEngagedUnshelteredTile() {
        //This will close What's New notification
        Base.clickOnWhatsNewNotificaitonCloseButton();

        //Navigation method below is commented as when user logs in Dashboard is displayed by default
        /* Base.clickOnDashBoard();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnDailyOutreach();*/

        //Verify Engaged Unsheltered Tile is displayed
        boolean isEngagedUnshelteredTileDisplayed = DailyOutreach.isEngagedUnsheltedProspectTileDisplayed();
        Assert.assertTrue(isEngagedUnshelteredTileDisplayed, "Engaged Unsheltered prospect and other prospect tile is not displayed on dashboard");

        //Click on Details button on Engaged Unsheltered tile
        DailyOutreach.clickOnDetailsButtonForEngagedUnshelteredTile();

        //Verify on clicking Details button Engaged Unsheltered data pane is displayed
        boolean isDataPaneDisplayed = DailyOutreach.verifyDataPaneIsDisplayedForEngagedUnsheltered();
        Assert.assertTrue(isDataPaneDisplayed, "Data pane is not displayed for Engaged Unsheltered Prospect and Other Prospect");

        //To verify that 'Latest Contact Date' column is displayed
        boolean isColumnDisplayed = DailyOutreach.verifyLatestContactDateColumnDisplayed();
        Assert.assertTrue(isColumnDisplayed, "'Latest Contact Date' column is not displayed");

        //Verify location of 'Latest Contact Date' column
        DailyOutreach.clickOnColumnPicker();
        boolean isInPlaceInColumnPicker = DailyOutreach.verifyPositionOfLatestContactDateColumnInColumnPicker();
        DailyOutreach.clickOnColumnPicker();
        Assert.assertTrue(isInPlaceInColumnPicker, "'Latest Contact Date' column is not displayed after 'Client Category' column");

        //This will verify that 'Latest Contact Date' is pre-selected in column picker
        boolean isPreselected = DailyOutreach.isLatestContactDateSelected();
        Assert.assertTrue(isPreselected, "'Latest Contact Date' is not pre-selected by default");

        //This will verify that de-selecting Latest Contact Date column in column picker, column will be removed from grid
        boolean isDeselected = DailyOutreach.deselectLatestContactDateColumn();
        Assert.assertTrue(isDeselected, "'Latest Contact Date' column is not removed from data grid");

        //This will verify that selecting Latest Contact Date column in column picker,column will be displayed in grid
        boolean isSelected = DailyOutreach.selectLatestContactDate();
        Assert.assertTrue(isSelected, "'Latest Contact Date' column is not displayed");
    }
    
	@Test()
    public void Tiles() throws InterruptedException {
        Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
        Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");
        for(int i = 2;i<=4;i++)
    	{   
        	DailyOutreach.Tile_verification(i);
			  
    	}
     
    }
}