package nycnet.dhs.streetsmart.tests;


import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.Common;
import nycnet.dhs.streetsmart.pages.ObservationsUnsuccessfulAttempts;
import nycnet.dhs.streetsmart.pages.RequestsApprovals;

public class ObservationsAndUnsuccessfulAttempts extends AutoRunTestController {

	//Add Observation and Unsuccessful attempt and verify it was added
	@Test()
	 	public void addObservationAndUnsuccessfulAttempts() {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickObservationsUnsuccessfulAttemptsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(),
				"dhs.nycnet/StreetSmart/ObservationsAndUnsuccessfulAttempts");
		Assert.assertEquals(ObservationsUnsuccessfulAttempts.returnObservationsUnsuccessfulAttemptsPageHeader(),
				"Observations & Unsuccessful Attempts");
		String dayofTheMonth = Helpers.returnCurrentDayOfTheMonth();
		//add observation
		ObservationsUnsuccessfulAttempts.clickOnAddNewButton();
		ObservationsUnsuccessfulAttempts.selectShift("Morning");
		ObservationsUnsuccessfulAttempts.clickOnObservationDatePicker();
		Common.clickOnLinkText(dayofTheMonth);
		ObservationsUnsuccessfulAttempts.clickOnObservationTimePicker();
		ObservationsUnsuccessfulAttempts.clickOnObservationTimeLabel();
		ObservationsUnsuccessfulAttempts.selectOutreachWorker("Aaron Phillips");
		ObservationsUnsuccessfulAttempts.inputTextInNumberOfIndividualsInput("5");
		ObservationsUnsuccessfulAttempts.selectArea("Brooklyn");
		ObservationsUnsuccessfulAttempts.selectborough("Brooklyn");
		ObservationsUnsuccessfulAttempts.selectSubWayLine("2");
		ObservationsUnsuccessfulAttempts.selectSubwayStation("Clark St");
		ObservationsUnsuccessfulAttempts.selectSubwayLocation("Corridors");
		ObservationsUnsuccessfulAttempts.inputTextInAdditionalDetailsInput("my text");
		
		//add unsuccessful attempt
		ObservationsUnsuccessfulAttempts.inputFirstName("Victor");
		ObservationsUnsuccessfulAttempts.inputLastName("Reyes");
		ObservationsUnsuccessfulAttempts.inputAlias("Vick");
		ObservationsUnsuccessfulAttempts.clickOnSaveButton();
		
		//add observation
				ObservationsUnsuccessfulAttempts.clickOnAddNewButton();
				ObservationsUnsuccessfulAttempts.selectShift("Morning");
				ObservationsUnsuccessfulAttempts.clickOnObservationDatePicker();
				Common.clickOnLinkText(dayofTheMonth);
				ObservationsUnsuccessfulAttempts.clickOnObservationTimePicker();
				ObservationsUnsuccessfulAttempts.clickOnObservationTimeLabel();
				ObservationsUnsuccessfulAttempts.selectOutreachWorker("Aaron Phillips");
				ObservationsUnsuccessfulAttempts.inputTextInNumberOfIndividualsInput("5");
				ObservationsUnsuccessfulAttempts.selectArea("Brooklyn");
				ObservationsUnsuccessfulAttempts.selectborough("Brooklyn");
				ObservationsUnsuccessfulAttempts.selectSubWayLine("2");
				ObservationsUnsuccessfulAttempts.selectSubwayStation("Clark St");
				ObservationsUnsuccessfulAttempts.selectSubwayLocation("Corridors");
				ObservationsUnsuccessfulAttempts.inputTextInAdditionalDetailsInput("my text");
				
				//add unsuccessful attempt
				ObservationsUnsuccessfulAttempts.inputFirstName("Victor");
				ObservationsUnsuccessfulAttempts.inputLastName("Reyes");
				ObservationsUnsuccessfulAttempts.inputAlias("Vick");
				ObservationsUnsuccessfulAttempts.clickOnSaveButton();
				
		//Verify Borough column can be enabled and disabled
        boolean isBoroughPresentOnGrid = ObservationsUnsuccessfulAttempts.isColumnPresentOnGrid("Borough");
        Assert.assertTrue(isBoroughPresentOnGrid, "The column is not present on the Grid");
        ObservationsUnsuccessfulAttempts.clickOnColumnPicker();
        ObservationsUnsuccessfulAttempts.deselectColumnOnColumnPicker("Borough");
        ObservationsUnsuccessfulAttempts.clickOnColumnPicker();
        isBoroughPresentOnGrid =  ObservationsUnsuccessfulAttempts.isColumnPresentOnGrid("Borough");
        Assert.assertFalse(isBoroughPresentOnGrid, "The column is present on the Grid");
        ObservationsUnsuccessfulAttempts.clickOnColumnPicker();
        ObservationsUnsuccessfulAttempts.selectColumnOnColumnPicker("Borough");
        ObservationsUnsuccessfulAttempts.clickOnColumnPicker();
        isBoroughPresentOnGrid = ObservationsUnsuccessfulAttempts.isColumnPresentOnGrid("Borough");
        Assert.assertTrue(isBoroughPresentOnGrid, "The column is not present on the Grid");
        
        //Search for observation and unsuccessful attempt
        String unsuccessfulAttemptID = ObservationsUnsuccessfulAttempts.returnFirstRecordOnSecondColumn();
        ObservationsUnsuccessfulAttempts.inputTextInSearchInputBox(unsuccessfulAttemptID);
        ObservationsUnsuccessfulAttempts.submitSearchInSearchInputBox();
        String unsuccessfulAttemptIDAfterSearch = RequestsApprovals.returnFirstRecordOnSecondColumn();
        Assert.assertEquals(unsuccessfulAttemptIDAfterSearch, unsuccessfulAttemptID);
        RequestsApprovals.verifyValueOFAllRecordsOnSecondColumn(unsuccessfulAttemptID);
        ObservationsUnsuccessfulAttempts.clearSearchInSearchInputBox();
        ObservationsUnsuccessfulAttempts.submitSearchInSearchInputBox();
        
        //Apply advanced filter
        ObservationsUnsuccessfulAttempts.clickOnAdvancedFilter();
        ObservationsUnsuccessfulAttempts.clickOnChooseFiter();
        ObservationsUnsuccessfulAttempts.chooseColumnFilterOnAdvancedFilter("ObservationAttemptNumber");
       
        ObservationsUnsuccessfulAttempts.enterFilterValueOnSelectedFilterOnAdvancedFilter("Unsuccessful Attempt ID/Observation ID", unsuccessfulAttemptID);
        ObservationsUnsuccessfulAttempts.clickOnApplyFilter();
       
        String unsuccessfulAttemptIDAfter =  ObservationsUnsuccessfulAttempts.returnFirstRecordOnSecondColumn();
        Assert.assertEquals(unsuccessfulAttemptIDAfter, unsuccessfulAttemptID);
        RequestsApprovals.verifyValueOFAllRecordsOnSecondColumn(unsuccessfulAttemptID);
        
        
        ObservationsUnsuccessfulAttempts.clickOnAdvancedFilter();
        ObservationsUnsuccessfulAttempts.clickOnResetFilterButton();
        ObservationsUnsuccessfulAttempts.clickOnCloseButton();
		
}
	
	
}




  


