package nycnet.dhs.streetsmart.tests;

import java.io.IOException;
import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;

import nycnet.dhs.streetsmart.core.Config;
import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.AddEngagement;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.Census;
import nycnet.dhs.streetsmart.pages.ClientAddNewClient;
import nycnet.dhs.streetsmart.pages.ClientInformation;
import nycnet.dhs.streetsmart.pages.ClientList;
import nycnet.dhs.streetsmart.pages.ClientSearch;
import nycnet.dhs.streetsmart.pages.Common;
import nycnet.dhs.streetsmart.pages.DailyDropIn;
import nycnet.dhs.streetsmart.pages.DashboardLanding;
import nycnet.dhs.streetsmart.pages.EncampmentsList;
import nycnet.dhs.streetsmart.pages.GeneralCanvassing;
import nycnet.dhs.streetsmart.pages.IntensiveCanvassing;
import nycnet.dhs.streetsmart.pages.JointOperations;

import nycnet.dhs.streetsmart.pages.GlobalSearch;

import nycnet.dhs.streetsmart.pages.MonthlyDashboard;
import nycnet.dhs.streetsmart.pages.MyCaseload;
import nycnet.dhs.streetsmart.pages.ObservationsUnsuccessfulAttempts;

import nycnet.dhs.streetsmart.pages.Panhandling;
import nycnet.dhs.streetsmart.pages.Placement;
import nycnet.dhs.streetsmart.pages.SignIn;
import nycnet.dhs.streetsmart.pages.StabilizationBed;
import nycnet.dhs.streetsmart.pages.PopulationTrends;
import nycnet.dhs.streetsmart.pages.ReleaseHistory;
import nycnet.dhs.streetsmart.pages.RequestsApprovals;
import nycnet.dhs.streetsmart.pages.ServiceRequests;
import nycnet.dhs.streetsmart.pages.SignIn;
import nycnet.dhs.streetsmart.pages.UserPermissions;
import nycnet.dhs.streetsmart.pages.Webinars;
import nycnet.dhs.streetsmart.pages.clientdetails.CaseInformationTab;
import nycnet.dhs.streetsmart.pages.clientdetails.EngagementsTab;
import nycnet.dhs.streetsmart.pages.dashboard.DailyOutreach;
import nycnet.dhs.streetsmart.pages.engagements.AllEngagements;
import nycnet.dhs.streetsmart.pages.engagements.ClientAndEngagementSearch;
import nycnet.dhs.streetsmart.pages.engagements.MyEngagements;
import nycnet.dhs.streetsmart.pages.reports.CreateReport;
import nycnet.dhs.streetsmart.pages.reports.RequestAReport;
import nycnet.dhs.streetsmart.pages.reports.ViewReports;

public class Tests extends AutoRunTestController {

	@Test()
	public void aboutStreetSmartTest() throws MalformedURLException {
		Base.clickOnAboutStreetMenuItem();
		WebDriverTasks.switchToNewWindow();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/AboutStreetSmart");
	}

	@Test()
	public void populationTrendsTest() throws MalformedURLException {
		// verify the Population Trends page loads as expected by verifying URL and
		// header
		Base.clickOnPopulationTrendsMenu();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/PopulationTrend");
		Assert.assertEquals(PopulationTrends.returnPopulationTrendsPageHeader(), "Population Trends");
	}

	@Test()
	public void DashBoardURLTest() throws MalformedURLException {

		// Verify the landing page defaulting to Dashboard screen
		// Base.clickOnDashBoard();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
		Assert.assertEquals(DashboardLanding.returndashboardLandingPageHeader(), "Dashboard");

		// Verify the URL & Header for Daily Outreach page
		Base.clickOnDailyOutreach();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
		Assert.assertEquals(DailyOutreach.returndailyoutreachPageHeader(), "Daily Outreach");

		// Verify the URL & Header for Daily Drop-In page
		Base.clickOnDailyDropInMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboardDropIn");
		Assert.assertEquals(DailyDropIn.returndaildropinPageHeader(), "Daily Drop-In");

		// Verify the URL & Header for Monthly DashBoard page
		Base.clickonMonthlyDashboardMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/Home/MonthlyDashboard");
		Assert.assertEquals(MonthlyDashboard.returnmonthlydashboardPageHeader(), "Monthly Dashboard");
	}

	@Test()
	public void ClientsURLTest() throws MalformedURLException {
		// Verify pages under Clients menu load successfully
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		// Verify Client List page loads as expected by verifying URL and header
		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		// Verify My Caseload page loads expected by verifying URL and header
		Base.clickOnMyCaseLoadMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyClientList");
		Assert.assertEquals(MyCaseload.returnMyCaseLoadPageHeader(), "My Caseload");

		// Verify Add Client page loads as expected by verifying URL and header
		Base.clickOnAddClientMenuItem();

		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
		Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
	}

	@Test()
	public void EngagementsURLTest() throws MalformedURLException {

		// Verify pages under Engagements menu load successfully
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		// Verify All Engagements pages loads as expected by verifying URL and header
		Base.clickOnAllEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
		Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");

		// Verify My Engagements page loads expected by verifying URL and header
		Base.clickOnMyEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyEngagements");
		Assert.assertEquals(MyEngagements.returnMyEngagementsBreadcrumb(), "My Engagements");

		// Verify Add Engagement page loads as expected by verifying URL and header
		Base.clickOnAddEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientAndEngagementSearch");
		Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");

		// Verify Observations & Unsuccessful Attempts page loads as expected by
		// verifying URL and header
		Base.clickObservationsUnsuccessfulAttemptsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(),
				"dhs.nycnet/StreetSmart/ObservationsAndUnsuccessfulAttempts");
		Assert.assertEquals(ObservationsUnsuccessfulAttempts.returnObservationsUnsuccessfulAttemptsPageHeader(),
				"Observations & Unsuccessful Attempts");

	}

	@Test()
	public void CanvassingURLTest() throws MalformedURLException {

		// Canvassing Module
		// Verify the URL & Header for General Canvassing
		Base.clickOnCanvassing();
		Base.waitForSideBarMenuToBexpanded();
		Base.ClickOnGeneralCanvassing();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/GeneralCanvassing");
		Assert.assertEquals(GeneralCanvassing.returnGeneralCanvassingPageHeader(), "General Canvassing");

		// Verify the URL & Header for Panhandling
		Base.clickOnPanhandling();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/Panhandling");
		Assert.assertEquals(Panhandling.returnPanhandlingPageHeader(), "Panhandling");

		// Verify the URL & Header for Joint Operations
		Base.clickOnJointOperations();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/JointOperations");
		Assert.assertEquals(JointOperations.returnJointOperationsPageHeader(), "Joint Operations");

		// Verify the URL & Header for Intensive Canvassing
		Base.clickOnIntensiveCanvassing();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/IntensiveCanvassing");
		Assert.assertEquals(IntensiveCanvassing.returnintensivecanvassingPageHeader(), "Intensive Canvassing");
	}

	// Verify the URL & Header for Outreach Requests
	@Test()
	public void OutreachRequestsURLTest() throws MalformedURLException {
		Base.clickOnOutreachRequests();
		Base.clickOnServiceRequests();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/Service311Request");
		Assert.assertEquals(ServiceRequests.returnservicerequestsPageHeader(), "311 Service Requests");
	}

	// Verify the URL & Header for Encampments
	@Test()
	public void EncampmentsURLTest() throws MalformedURLException, InterruptedException {
		Base.clickOnEncampments();
		Base.clickOnEncampmentsList();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/EncampmentList");
		Assert.assertEquals(EncampmentsList.returnencampmentsListsPageHeader(), "Encampments List");
	}

	@Test()
	public void ReportsURLTest() throws MalformedURLException {
		// Reports Module
		// Validate the URL & Header for Census
		Base.clickOnReports();
		Base.clickOnOutreachCannedReports();
		Base.clickOnCensus();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/CensusCannedReport");
		Assert.assertEquals(Census.returncensusPageHeader(), "Census");

		// Validate the URL & Header for Placement
		Base.clickOnPlacement();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/PlacementCannedReport");
		Assert.assertEquals(Placement.returnplacementPageHeader(), "Placement");

		// Validate the URL & Header for Stabilization Bed
		Base.clickOnStabilizationBed();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(),
				"dhs.nycnet/StreetSmart/StabilizationBedCannedReport");
		Assert.assertEquals(StabilizationBed.returnstabilizationbedPageHeader(), "Stabilization Bed");

		// Validate the URL & Header for Census under Drop-In Canned Reports
		// Validate the URL & Header for Create Report under Custom Report
		Base.clickOnCustomReport();
		Base.clickOnCreateReport();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/CreateCustomReport");
		Assert.assertEquals(CreateReport.returncreatereportPageHeader(), "Create Report");

		// Validate the URL & Header for View Report under Custom Report
		Base.clickOnViewReports();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ViewCustomReports");
		Assert.assertEquals(ViewReports.returnviewreportsPageHeader(), "View Reports");

		// Validate the URL & Header for
		Base.clickOnRequestAReport();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/RequestCustomForm");
		Assert.assertEquals(RequestAReport.returnrequestareportPageHeader(), "Request a Report");
	}

	@Test()
	public void globalSearchTest() throws MalformedURLException {
		Base.clickOnGlobalSearchMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/GlobalSearch");
		GlobalSearch.inputTextinGlobalSearchBox("query");
		Assert.assertEquals(GlobalSearch.returnGlobalSearchInputtedText(), "query");
	}

	@Test()
	public void adminTestTest() throws MalformedURLException, InterruptedException {
		Base.clickOnAdminMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnUserPermissionsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/Admin/UserPermissions");
		Assert.assertEquals(UserPermissions.returnUsersPageHeader(), "Users");
		Base.clickOnRequestsApprovalsMenuitem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/Admin/ApprovalRequests");
		Assert.assertEquals(RequestsApprovals.returnRequestsApprovalsBreadcrumb(), "Requests & Approvals");
	}

	@Test()
	public void resourcesTest() throws MalformedURLException {
		Base.clickOnResourcesMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnWebinarsMenuitem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/Webinars");
		Assert.assertEquals(Webinars.returnWebinarsPageHeader(), "Webinars");
		Base.clickOnReleaseHistoryMenuitem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ReleaseHistory");
		Assert.assertEquals(ReleaseHistory.returnReleaseHistoryPageHeader(), "Release History");
	}

	@Test()
	public void searchForandOpenClient() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		// Verify Client List page loads as expected by verifying URL and header
		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C160911");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnStreetSmartID("C160911");
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C160911");
	}

	@Test()
	public void seachAndOpenClientFromEngagement() throws MalformedURLException {

		// Verify pages under Engagements menu load successfully
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnAllEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
		Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");
		AllEngagements.clickOnDateFilterYearlyButton();
		AllEngagements.clickOnDateFilterYearlyMenu();
		AllEngagements.selectYearonYearlyFilter("2020");
		AllEngagements.inputTextinAllEngagementSearchBox("C160911");
		AllEngagements.submitSearchinGlobalSearchBox();
		AllEngagements.clickOnStreetSmartID();
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C160911");
	}

	@Test()
	public void verifyUnAssignedGridReceivedDurationIsAscendingOrder() throws MalformedURLException {

		Base.clickOnOutreachRequests();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnServiceRequests();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/Service311Request");
		Assert.assertEquals(ServiceRequests.receivedDurationColumnSortingDirection(), "Sort Ascending");

	}

	@Test()
	public void verifyAssignedGridAssignedDurationIsAscendingOrder() throws MalformedURLException {

		Base.clickOnOutreachRequests();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnServiceRequests();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/Service311Request");
		ServiceRequests.clickOnAssignedDurationDetails();
		Assert.assertEquals(ServiceRequests.assignedDurationColumnSortingDirection(), "Sort Ascending");

	}

	@Test()
	public void exportToExcelClientListSelected() throws IOException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		int fileCountBefore = Helpers.countFilesInDownloadFolder();

		ClientList.selectFirstRowinClientListGrid();

		ClientList.selectThirdRowinClientListGrid();

		ClientList.clickOnExportToExcel();
		ClientList.clickOnExportToExcelSelectedRecords();
		Assert.assertEquals(ClientList.verifyExportToExcelMessage(), "Excel will be available when completed.");

		WebDriverTasks.waitForAngularPendingRequests();

		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

		String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
		String JCCClientColumn = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 5);
		String JCCClientStartDate = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 6);
		String JCCClientEndDate = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 7);
		Assert.assertEquals(JCCClientColumn, "JCC Client");
		Assert.assertEquals(JCCClientStartDate, "JCC Client Start Date");
		Assert.assertEquals(JCCClientEndDate, "JCC Client End Date");

	}

	@Test()
	public void exportToExcelClientListFiltered() throws IOException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C160911");
		ClientList.submitSearchinGlobalSearchBox();

		int fileCountBefore = Helpers.countFilesInDownloadFolder();

		ClientList.clickOnExportToExcel();
		ClientList.clickOnExportToExcelFilteredRecords();
		Assert.assertEquals(ClientList.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();

		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

		String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
		String JCCClientColumn = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 5);
		String JCCClientStartDate = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 6);
		String JCCClientEndDate = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 7);
		Assert.assertEquals(JCCClientColumn, "JCC Client");
		Assert.assertEquals(JCCClientStartDate, "JCC Client Start Date");
		Assert.assertEquals(JCCClientEndDate, "JCC Client End Date");
	}

	@Test()
	public void exportToExcelClientListCurrentView() throws IOException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");
		int fileCountBefore = Helpers.countFilesInDownloadFolder();

		ClientList.clickOnExportToExcel();
		ClientList.clickOnExportToExcelCurrentView();
		Common.clickOnOKConfirmationButton();
		Assert.assertEquals(ClientList.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();

		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

		String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
		String JCCClientColumn = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 5);
		String JCCClientStartDate = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 6);
		String JCCClientEndDate = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 7);
		Assert.assertEquals(JCCClientColumn, "JCC Client");
		Assert.assertEquals(JCCClientStartDate, "JCC Client Start Date");
		Assert.assertEquals(JCCClientEndDate, "JCC Client End Date");

	}

	@Test()
	public void exportToClientListExcelAll() throws IOException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");
		int fileCountBefore = Helpers.countFilesInDownloadFolder();

		ClientList.clickOnExportToExcel();
		ClientList.clickOnExportToExcelAllRecords();
		Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");

		WebDriverTasks.waitForAngularPendingRequests();

		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

		String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
		String JCCClientColumn = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 7);
		String JCCClientStartDate = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 8);
		String JCCClientEndDate = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 0, 9);
		Assert.assertEquals(JCCClientColumn, "JCC Client");
		Assert.assertEquals(JCCClientStartDate, "JCC Client Start Date");
		Assert.assertEquals(JCCClientEndDate, "JCC Client End Date");

	}

	@Test()
	public void exportToExcelAllEngagementsSelected() throws MalformedURLException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnAllEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
		Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");
		AllEngagements.clickOnDateFilterYearlyButton();
		AllEngagements.clickOnDateFilterYearlyMenu();
		AllEngagements.selectYearonYearlyFilter("2020");

		Common.selectFirstRowinClientListGrid();

		Common.selectThirdRowinClientListGrid();
		int fileCountBefore = Helpers.countFilesInDownloadFolder();
		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelSelectedRecords();
		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
	}

	@Test()
	public void exportToExcelAllEngagementsFiltered() throws MalformedURLException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnAllEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
		Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");
		AllEngagements.clickOnDateFilterYearlyButton();
		AllEngagements.clickOnDateFilterYearlyMenu();
		AllEngagements.selectYearonYearlyFilter("2020");
		AllEngagements.inputTextinAllEngagementSearchBox("C160911");
		AllEngagements.submitSearchinGlobalSearchBox();
		int fileCountBefore = Helpers.countFilesInDownloadFolder();
		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelFilteredRecords();
		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
	}

	@Test()
	public void exportToExcelAllEngagementsCurrentView() throws MalformedURLException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnAllEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
		Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");
		AllEngagements.clickOnDateFilterYearlyButton();
		AllEngagements.clickOnDateFilterYearlyMenu();
		AllEngagements.selectYearonYearlyFilter("2020");
		int fileCountBefore = Helpers.countFilesInDownloadFolder();
		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelCurrentView();
		Common.clickOnOKConfirmationButton();
		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
	}

	@Test()
	public void exportToExcelAllEngagementsAll() throws MalformedURLException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnAllEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientEngagement");
		Assert.assertEquals(AllEngagements.returnAllEngagementsPageHeader(), "All Engagements");
		AllEngagements.clickOnDateFilterYearlyButton();
		AllEngagements.clickOnDateFilterYearlyMenu();
		AllEngagements.selectYearonYearlyFilter("2020");
		int fileCountBefore = Helpers.countFilesInDownloadFolder();
		ClientList.clickOnExportToExcel();
		ClientList.clickOnExportToExcelAllRecords();
		Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

	}

	@Test()
	public void exportToExcelSelectedMyEngagements() throws IOException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnMyEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyEngagements");
		Assert.assertEquals(MyEngagements.returnMyEngagementsBreadcrumb(), "My Engagements");
		Common.clickOnDateFilterYearlyButton();
		Common.clickOnDateFilterYearlyMenu();
		Common.selectYearonYearlyFilter("2019");

		int fileCountBefore = Helpers.countFilesInDownloadFolder();
		Common.selectFirstRowinClientListGrid();

		Common.selectThirdRowinClientListGrid();
		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelSelectedRecords();

		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();

		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

	}

	@Test()
	public void exportToExcelCurrentViewMyEngagements() throws MalformedURLException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnMyEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyEngagements");
		Assert.assertEquals(MyEngagements.returnMyEngagementsBreadcrumb(), "My Engagements");
		Common.clickOnDateFilterYearlyButton();
		Common.clickOnDateFilterYearlyMenu();
		Common.selectYearonYearlyFilter("2019");

		int fileCountBefore = Helpers.countFilesInDownloadFolder();

		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelCurrentView();
		Common.clickOnOKConfirmationButton();
		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();

		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

	}

	@Test()
	public void exportToExceMyEngagementsAll() throws MalformedURLException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnMyEngagementsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyEngagements");
		Assert.assertEquals(MyEngagements.returnMyEngagementsBreadcrumb(), "My Engagements");
		Common.clickOnDateFilterYearlyButton();
		Common.clickOnDateFilterYearlyMenu();
		Common.selectYearonYearlyFilter("2019");
		int fileCountBefore = Helpers.countFilesInDownloadFolder();
		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelAllRecords();
		Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();

		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

	}

	@Test()
	public void exportToExcelMyCaseloadSelected() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnMyCaseLoadMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyClientList");
		Assert.assertEquals(MyCaseload.returnMyCaseLoadPageHeader(), "My Caseload");

		Common.selectFirstRowinClientListGrid();

		Common.selectThirdRowinClientListGrid();

		int fileCountBefore = Helpers.countFilesInDownloadFolder();

		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelSelectedRecords();
		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
	}

	@Test()
	public void exportToExcelMyCaseloadFiltered() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnMyCaseLoadMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyClientList");
		Assert.assertEquals(MyCaseload.returnMyCaseLoadPageHeader(), "My Caseload");

		ClientList.inputTextinClientListSearchBox("C57273");
		ClientList.submitSearchinGlobalSearchBox();
		int fileCountBefore = Helpers.countFilesInDownloadFolder();
		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelFilteredRecords();
		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
	}

	@Test()
	public void exportToExcelMyCaseloadCurrentView() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnMyCaseLoadMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyClientList");
		Assert.assertEquals(MyCaseload.returnMyCaseLoadPageHeader(), "My Caseload");
		int fileCountBefore = Helpers.countFilesInDownloadFolder();
		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelCurrentView();
		Common.clickOnOKConfirmationButton();
		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
	}

	@Test()
	public void exportToMyCaseloadExcelAll() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnMyCaseLoadMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/MyClientList");
		Assert.assertEquals(MyCaseload.returnMyCaseLoadPageHeader(), "My Caseload");
		int fileCountBefore = Helpers.countFilesInDownloadFolder();
		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelAllRecords();
		Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
	}

	@Test()
	public void exportToExcelObservationsandUnsuccesfulAttemptsSelected() throws MalformedURLException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickObservationsUnsuccessfulAttemptsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(),
				"dhs.nycnet/StreetSmart/ObservationsAndUnsuccessfulAttempts");
		Assert.assertEquals(ObservationsUnsuccessfulAttempts.returnObservationsUnsuccessfulAttemptsPageHeader(),
				"Observations & Unsuccessful Attempts");
		Common.clickOnDateFilterYearlyButton();
		ObservationsUnsuccessfulAttempts.clickOnDateFilterYearlyMenu();
		Common.selectYearonYearlyFilter("2017");
		Common.selectFirstRowinClientListGrid();

		Common.selectThirdRowinClientListGrid();

		Common.selectFifthRowinClientListGrid();

		int fileCountBefore = Helpers.countFilesInDownloadFolder();

		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelSelectedRecords();
		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
	}

	@Test()
	public void exportToExcelObservationsandUnsuccesfulAttemptsCurrentView() throws MalformedURLException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickObservationsUnsuccessfulAttemptsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(),
				"dhs.nycnet/StreetSmart/ObservationsAndUnsuccessfulAttempts");
		Assert.assertEquals(ObservationsUnsuccessfulAttempts.returnObservationsUnsuccessfulAttemptsPageHeader(),
				"Observations & Unsuccessful Attempts");
		Common.clickOnDateFilterYearlyButton();
		ObservationsUnsuccessfulAttempts.clickOnDateFilterYearlyMenu();
		Common.selectYearonYearlyFilter("2017");

		int fileCountBefore = Helpers.countFilesInDownloadFolder();

		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelCurrentView();
		Common.clickOnOKConfirmationButton();
		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
	}

	@Test()
	public void exportToExcelObservationsandUnsuccesfulAttemptsFiltered() throws MalformedURLException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickObservationsUnsuccessfulAttemptsMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(),
				"dhs.nycnet/StreetSmart/ObservationsAndUnsuccessfulAttempts");
		Assert.assertEquals(ObservationsUnsuccessfulAttempts.returnObservationsUnsuccessfulAttemptsPageHeader(),
				"Observations & Unsuccessful Attempts");
		Common.clickOnDateFilterYearlyButton();
		ObservationsUnsuccessfulAttempts.clickOnDateFilterYearlyMenu();
		Common.selectYearonYearlyFilter("2017");
		ObservationsUnsuccessfulAttempts.inputTextInSearchInputBox("Manhattan");
		ObservationsUnsuccessfulAttempts.submitSearchInSearchInputBox();
		int fileCountBefore = Helpers.countFilesInDownloadFolder();

		Common.clickOnExportToExcel();
		Common.clickOnExportToExcelFilteredRecords();
		Assert.assertEquals(Common.verifyExportToExcelMessage(), "Excel will be available when completed.");
		WebDriverTasks.waitForAngularPendingRequests();
		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");
	}

	
	//Will disable for now until last step is fixed
	@Test(enabled=false)
	public void addNewClientClientSearchProspectClient() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnAddClientMenuItem();

		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
		Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");

		ClientSearch.expandClientSearchPanel();
		
		WebDriverTasks.scrollDownJavascriptExcecutor();
		ClientSearch.inputTextinClientSearchFirstNameField("Suyog");
		ClientSearch.clickOnClientSearchButton();
		ClientSearch.clickOnClientSearchAddNewClient();

		ClientSearch.clickOnProspectButton();

		Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
		
		//This step needs to be changed to verify Other Prospect is selected
		Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

	}
	
	//Add New Client Engaged Unsheltered Client from Duplicate Search section of Add New client
		@Test()
		public void addNewClientEngagedUnshelteredClientfromDuplicateSearch() throws MalformedURLException {
			Base.clickOnClientsMenu();
			Base.waitForSideBarMenuToBexpanded();
			Base.clickOnAddClientMenuItem();

			Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
			Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");

			ClientSearch.inputTextinFirstNameField("Nonexisting");
			ClientSearch.inputTextinLastNameField("Client");
			ClientSearch.clickOnDupliceSearchButton();
			ClientSearch.clickOnDuplicateSearchAddNewClient();
			
			String engagedUnshelteredProspectButtonText = Common.returnEngagedUnshelteredButtonText();
			Assert.assertEquals(engagedUnshelteredProspectButtonText, "Engaged Unsheltered Prospect");
			Common.clickOnEngagedUnshelteredButton();

			Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
			Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

		}
		
		//Add New Client Engaged Unsheltered Client from client search section of Add New client
				@Test()
				public void addNewClientEngagedUnshelteredClientfromClientSearch() throws MalformedURLException {
					Base.clickOnClientsMenu();
					Base.waitForSideBarMenuToBexpanded();
					Base.clickOnAddClientMenuItem();

					Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
					Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");
					ClientSearch.expandClientSearchPanel();
					WebDriverTasks.pageDownCommon();
					ClientSearch.inputTextinClientSearchFirstNameField("Nonexisting");
					ClientSearch.inputTextinClientSearchLastNameField("Client");
					ClientSearch.clickOnClientSearchButton();
					ClientSearch.clickOnClientSearchAddNewClient();
						String engagedUnshelteredProspectButtonText = Common.returnEngagedUnshelteredButtonText();
					Assert.assertEquals(engagedUnshelteredProspectButtonText, "Engaged Unsheltered Prospect");
					
					Common.clickOnEngagedUnshelteredButton();
					
				

					Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
					Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

				}



	//verify add new Engaged unsheltered client from duplicate search section of Add New Engagement
	@Test()
	public void addNewEngagementEngagedUnshelteredClient() throws MalformedURLException {
		Base.clickOnEngagementsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnAddEngagementsMenuItem();

		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientAndEngagementSearch");
		Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");

		ClientAndEngagementSearch.inputTextinFirstNameField("Nonexisting");
		ClientAndEngagementSearch.inputTextinLastNameField("Client");
		ClientAndEngagementSearch.clickOnDupliceSearchButton();
		ClientAndEngagementSearch.clickOnDuplicateSearchAddNewClient();
		String engagedUnshelteredProspectButtonText = Common.returnEngagedUnshelteredButtonText();
		Assert.assertEquals(engagedUnshelteredProspectButtonText, "Engaged Unsheltered Prospect");
		
		ClientAndEngagementSearch.clickOnEngagedUnshelteredButton();

		Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
		Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

	}

	//verify Add new Engaged Unsheltered client from Client and Engagement Search section of Add new engagement
	@Test()
	public void ClientandEngagementSearchEngagedUnshelteredClient() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnAddClientMenuItem();

		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
		Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");

		ClientAndEngagementSearch.expandClientSearchPanel();
		WebDriverTasks.scrollDownJavascriptExcecutor();
		ClientAndEngagementSearch.inputTextinClientSearchFirstNameField("Nonexisting");
		ClientAndEngagementSearch.inputTextinClientSearchLastNameField("Client");
		ClientAndEngagementSearch.clickOnClientSearchButton();
		ClientAndEngagementSearch.clickOnClientSearchAddNewClient();
		String engagedUnshelteredProspectButtonText = Common.returnEngagedUnshelteredButtonText();
		Assert.assertEquals(engagedUnshelteredProspectButtonText, "Engaged Unsheltered Prospect");
		ClientAndEngagementSearch.clickOnEngagedUnshelteredButton();

		Assert.assertEquals(ClientAddNewClient.returnClientListPageHeader(), "Client: Add New Client");
		Assert.assertEquals(ClientAddNewClient.verifyEngagedUnshelteredProspectisSelected(), "true");

	}
	
	
	@Test()
	public void verifyEngagedUnshelteredClientAppearsInDuplicateSearch() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnAddClientMenuItem();

		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
		Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");

		ClientSearch.inputTextinFirstNameField("Unsheltered");
		ClientSearch.inputTextinLastNameField("Automation");
		ClientSearch.clickOnDupliceSearchButton();
		ClientSearch.clickOnClientOnGrid("C160979");
		Assert.assertEquals(ClientSearch.returnClientCategory(), "Engaged Unsheltered Prospect");

	}

	@Test()
	public void verifyEngagedUnshelteredAppearsInClientClientSearch() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnAddClientMenuItem();

		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientSearch");
		Assert.assertEquals(ClientSearch.returnDuplicateSearchPageHeader(), "Duplicate Search");

		ClientSearch.expandClientSearchPanel();
		WebDriverTasks.pageDownCommon();
		ClientSearch.inputTextinClientSearchFirstNameField("Unsheltered");
		ClientSearch.inputTextinClientSearchLastNameField("Automation");
		ClientSearch.clickOnClientSearchButton();
		ClientSearch.clickOnClientOnGrid("C160979");
		WebDriverTasks.pageDownCommon();
		String StreetsmartID = ClientInformation.returnStreetsmartID();
		Assert.assertEquals(StreetsmartID, "C160979");
		String clientCategorySelectedOption = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
		Assert.assertEquals(clientCategorySelectedOption, "Engaged Unsheltered Prospect");
	}

	@Test()
	public void verifyEngagedUnshelteredClientfromClientList() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C160979");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnStreetSmartID("C160979");
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C160979");
		String clientCategorySelectedOption = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
		Assert.assertEquals(clientCategorySelectedOption, "Engaged Unsheltered Prospect");
	}

	@Test()
	public void verifyInactiveClientfromClientList() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C160999");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnStreetSmartID("C160999");
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C160999");
		String clientCategorySelectedOption = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
		Assert.assertEquals(clientCategorySelectedOption, "Inactive");
	}

	@Test()
	public void verifyPopupMessageWhenChangingToCaseLoad() throws MalformedURLException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C160982");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnStreetSmartID("C160982");
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C160982");
		WebDriverTasks.pageDownCommon();
		ClientInformation.clickOnEditButton();
		WebDriverTasks.pageDownCommon();
		// ClientInformation.selectClientCategoryOnEditPage("Inactive");
		ClientInformation.selectClientCategoryOnEditPage("Caseload");
		Assert.assertEquals(ClientInformation.returnCaseChangeMessagepopup(),
				"Please confirm if the client has agreed to be caseloaded in Streetsmart.");
	}

	@Test()
	public void activityStreamSubwayDiversionProgramStartDateTest() throws MalformedURLException {

		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C160911");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnStreetSmartID("C160911");
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C160911");

		EngagementsTab.clickOnEngagementsTab();
		EngagementsTab.clickOnActionButton();
		EngagementsTab.clickOnViewDetails();
		EngagementsTab.pageDownToEditButton();
		EngagementsTab.clickOnEditButton();
		EngagementsTab.pageUpToOpenCalendar();
		EngagementsTab.clickOnDatePickerCalendar();
		EngagementsTab.clickOnNextMonthOnCalendar();
		EngagementsTab.chooseNewDateOnCalendar();
		EngagementsTab.clickOnDoneButtonOnCalendar();
		EngagementsTab.pageDownToSaveButton();
		EngagementsTab.clickOnSaveButtonOnCalendar();

		Base.clickOnPopulationTrendsMenu();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/PopulationTrend");
		Assert.assertEquals(PopulationTrends.returnPopulationTrendsPageHeader(), "Population Trends");
		PopulationTrends.clickOnExpandActivityStreamButton();
		PopulationTrends.inputTexInSearchFieldOfActivityStream("System");
		PopulationTrends.submitSearchInActivityStream();
		Assert.assertEquals(PopulationTrends.returnUpdatedByFirstRowOfActivityStreamGrid(), "System");
		Assert.assertEquals(PopulationTrends.returnProviderColumnFirstRowOfActivityStreamGrid(), "-");
		Assert.assertEquals(PopulationTrends.returnItemColumnFirstRowOfActivityStreamGrid(), "Client");
		Assert.assertTrue(PopulationTrends.returnActionSummaryFirstRowOfActivityStreamGrid().endsWith("4/2/2020"),
				"The Subway Diversion Program Start Date is not 4/2/2020");

		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C160911");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnStreetSmartID("C160911");
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C160911");

		EngagementsTab.clickOnEngagementsTab();
		EngagementsTab.clickOnActionButton();
		EngagementsTab.clickOnViewDetails();
		EngagementsTab.pageDownToEditButton();
		EngagementsTab.clickOnEditButton();
		EngagementsTab.pageUpToOpenCalendar();
		EngagementsTab.clickOnDatePickerCalendar();
		EngagementsTab.clickOnNextMonthOnCalendar();
		EngagementsTab.chooseOldDateOnCalendar();
		EngagementsTab.clickOnDoneButtonOnCalendar();
		EngagementsTab.pageDownToSaveButton();
		EngagementsTab.clickOnSaveButtonOnCalendar();

	}

	//Need to disable until there is step to create a new case
	@Test(enabled = false)
	public void deleteCaseAndVerifySubwayDiversionProgramStart() throws MalformedURLException, InterruptedException {

		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C160976");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnStreetSmartID("C160976");
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C160976");

		CaseInformationTab.clickOncaseInformationTab();
		CaseInformationTab.clickOnActionButton();
		CaseInformationTab.clickOnDeleteButton();

		Thread.sleep(3000);
		CaseInformationTab.clickOnconfirmButton();
		Thread.sleep(3000);
		CaseInformationTab.clickOnmakeProspectOption();
		CaseInformationTab.clickOnNextButton();

		Thread.sleep(5000);
		EngagementsTab.clickOnEngagementsTab();
		EngagementsTab.clickOnActionButton();
		EngagementsTab.clickOnViewDetails();
		Assert.assertEquals(EngagementsTab.returnSubwayDiversionProgramStartDate(), "04/02/2020");

	}

	@Test()
	public void verifyClientCategoryWhenEditingEngagedUnshelteredAndInactive() throws MalformedURLException {

		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C160976");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnStreetSmartID("C160976");
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C160976");
		ClientInformation.clickOnEditButton();
		ClientInformation.returnEngagedInactiveCategoryMenuOption();
		ClientInformation.returnEngagedUnshelteredClientCategoryMenuOption();
	}

	@Test()
	public void logOutTest() throws MalformedURLException, InterruptedException {
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
		Assert.assertEquals(DashboardLanding.returndashboardLandingPageHeader(), "Dashboard");
		// Sign out of application and verify the sign-in page is presented. Please make
		// sure this is the last test that runs.
		Base.clickOnWhatsNewNotificaitonCloseButton();
		Thread.sleep(2000);
		Base.clickOnLogoutNavBar();
		Base.clickOnLogoutButton();
		// Consider removing the step to click on SignIn Button.
		SignIn.clickOnSignInButton();
		Assert.assertTrue(WebDriverTasks.returnEndingPageURL().contains("dhs.nycnet/Authentication/LogOn"),
				"Expected URL to contain dhs.nycnet/Authentication/LogOn");
		Assert.assertEquals(SignIn.returnSignInPageHeader(), "StreetSmart: Please Sign In");
	}

//Disabled until we have a dedicated test account. PRA_TestMOC3 is a shared test account.
	@Test()
	public void loginTest() throws MalformedURLException, InterruptedException {
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
		Assert.assertEquals(DashboardLanding.returndashboardLandingPageHeader(), "Dashboard");
		Base.clickOnWhatsNewNotificaitonCloseButton();
		Thread.sleep(2000);
		Base.clickOnLogoutNavBar();
		Base.clickOnLogoutButton();
		SignIn.inputUserName("dhs\\PRA_TestMOC3");
		SignIn.passwordInputBox("Welcome4");
		SignIn.clickOnSignInButton();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
		Assert.assertEquals(DashboardLanding.returndashboardLandingPageHeader(), "Dashboard");
	}

	// Disabled until we have a dedicated test account. PRA_TestMOC3 is a shared
	// test account.
	@Test(enabled = false)
	public void loginTestWrongPassword() throws MalformedURLException, InterruptedException {
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
		Assert.assertEquals(DashboardLanding.returndashboardLandingPageHeader(), "Dashboard");
		Base.clickOnWhatsNewNotificaitonCloseButton();
		Thread.sleep(2000);
		Base.clickOnLogoutNavBar();
		Base.clickOnLogoutButton();
		SignIn.inputUserName("dhs\\PRA_TestMOC3");
		SignIn.passwordInputBox("Welcome");
		SignIn.clickOnSignInButton();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/Error/AccessDenied");
		Assert.assertEquals(Base.returnaccessDeniedHeader(), "Access Denied");
	}

	@Test()
	public void loginTestWrongUserName() throws MalformedURLException, InterruptedException {
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/DailyDashboard");
		Assert.assertEquals(DashboardLanding.returndashboardLandingPageHeader(), "Dashboard");
		Base.clickOnWhatsNewNotificaitonCloseButton();
		Thread.sleep(2000);
		Base.clickOnLogoutNavBar();
		Base.clickOnLogoutButton();
		SignIn.inputUserName("dhs\\wrongusername");
		SignIn.clickOnSignInButton();
		Assert.assertEquals(SignIn.returnProvideUserNamePasswordHeader(), "Please provide Username and Password");
	}

	@Test()
	public void autoFlipThirtydays() throws MalformedURLException, InterruptedException {

		// Verify the Prospect client category
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		// Verify Client List page loads as expected by verifying URL and header
		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C160996");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnProspectClientC160996();
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C160996");

		// Add 3 new engagements

		// 1st engagement
		AddEngagement.clickOnEngagementTab();

		AddEngagement.clickOnAddNewEngagementButton();
		AddEngagement.selectEngagementProvider("BG");
		AddEngagement.selectEngagementArea("Bronx");
		AddEngagement.selectEngagementShift("Evening");
		AddEngagement.selectEngagementJointOutreach("No");
		AddEngagement.selectEngagementContactReason("311 Response");
		AddEngagement.selectEngagementContactTime();
		AddEngagement.selectEngagementContactDateField();
		AddEngagement.selectEngagementDatePickerPrevious();
		AddEngagement.selectEngagementFirstDate();

		AddEngagement.selectEngagementOutcome("958 Certified Removal");
		AddEngagement.selectEngagementBorough("Bronx");
		AddEngagement.selectLocationType("Park");
		AddEngagement.selectParkName("Amersfort");
		AddEngagement.inputEngagementNotes("1");
		AddEngagement.selectSaveEngagement();

		AddEngagement.clickOnEngagementTab();

		// 2nd Engagement
		AddEngagement.clickOnAddNewEngagementButton();
		AddEngagement.selectEngagementProvider("BG");
		AddEngagement.selectEngagementArea("Bronx");
		AddEngagement.selectEngagementShift("Afternoon");
		AddEngagement.selectEngagementJointOutreach("No");

		AddEngagement.selectEngagementContactReason("311 Response");
		AddEngagement.selectEngagementContactTime();
		AddEngagement.selectEngagementContactDateField();
		AddEngagement.selectEngagementSecondDate();

		AddEngagement.selectEngagementOutcome("958 Certified Removal");
		AddEngagement.selectEngagementBorough("Bronx");
		AddEngagement.selectLocationType("Park");
		AddEngagement.selectParkName("Astoria");
		AddEngagement.inputEngagementNotes("2");
		AddEngagement.selectSaveEngagement();

		AddEngagement.clickOnEngagementTab();

		// 3rd Engagement
		AddEngagement.clickOnAddNewEngagementButton();
		AddEngagement.selectEngagementProvider("BG");
		AddEngagement.selectEngagementArea("Bronx");
		AddEngagement.selectEngagementShift("Afternoon");
		AddEngagement.selectEngagementJointOutreach("No");

		AddEngagement.selectEngagementContactReason("311 Response");
		AddEngagement.selectEngagementContactTime();
		AddEngagement.selectEngagementContactDateField();
		AddEngagement.selectEngagementThirdDate();

		AddEngagement.selectEngagementOutcome("958 Certified Removal");
		AddEngagement.selectEngagementBorough("Bronx");
		AddEngagement.selectLocationType("Park");
		AddEngagement.selectParkName("Barretto");
		AddEngagement.inputEngagementNotes("3");
		AddEngagement.selectSaveEngagement();

		// verify the autoflip
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnClientListMenuItem();
		ClientList.inputTextinClientListSearchBox("C160996");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnProspectClientC160996();
		String clientCategorySelectedOption = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
		Assert.assertEquals(clientCategorySelectedOption, "Engaged Unsheltered Prospect");
	}

	@Test

	public void clientCategoryToCaseload() throws MalformedURLException {

		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		ClientList.inputTextinClientListSearchBox("C152443");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnStreetSmartID("C152443");
		Assert.assertEquals(ClientInformation.returnStreetsmartID(), "C152443");
		ClientInformation.selectCaseLoad("Caseload");
		
		

		CaseInformationTab.selectCaseStartDateField();
		CaseInformationTab.selectCaseStartDate();
		CaseInformationTab.selectCaseProvider("Breaking Ground");
		CaseInformationTab.selectCaseBorough("Bronx");
		CaseInformationTab.selectSaveCaseInformation();
		String clientCategorySelectedOption = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
		Assert.assertEquals(clientCategorySelectedOption, "Caseload");
	}

	@Test

	public void verifyJCCFieldsForDefaultView() throws MalformedURLException, InterruptedException {

		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		Assert.assertEquals(ClientList.clickOnJCCClient(), "JCC Client");
		Assert.assertEquals(ClientList.clickOnJCCClientStartDate(), "JCC Client Start Date");
		Assert.assertEquals(ClientList.clickOnJCCClientEndDate(), "JCC Client End Date");
	}

	@Test()

	public void verifyEngagedUnshelteredProspectCategory() throws MalformedURLException, InterruptedException {
		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C146864");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnProspectClientC146864();

		ClientInformation.clickOnEditClientInformationButton();
		ClientInformation.verifyEngagedUnshelteredClientCategory("Engaged Unsheltered Prospect");
		ClientInformation.clickOnSaveButtonforEngagedUnshelteredClient();
		String clientCategorySelectedOption = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
		Assert.assertEquals(clientCategorySelectedOption, "Engaged Unsheltered Prospect");

	}

	@Test()

	public void verifyInactiveClientCategory() throws MalformedURLException, InterruptedException {

		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");

		ClientList.inputTextinClientListSearchBox("C161003");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clickOnProspectClientC161003();

		ClientInformation.clickOnEditClientInformationButton();
		ClientInformation.verifyEngagedUnshelteredClientCategory("Inactive");
		ClientInformation.clickOnSaveButtonforInactiveClient();
		String clientCategorySelectedOption = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
		Assert.assertEquals(clientCategorySelectedOption, "Inactive");

	}

	@Test()

	public void verifySearchByEngagedUnshelteredandInactiveCategory()
			throws MalformedURLException, InterruptedException {

		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");
		ClientList.inputTextinClientListSearchBox("Engaged Unsheltered Prospect");
		ClientList.submitSearchinGlobalSearchBox();
		ClientList.clearTextinClientListSearchBox();
		ClientList.inputTextinClientListSearchBox("Inactive");
		ClientList.submitSearchinGlobalSearchBox();
	}

	@Test

	public void verifyEngagedUnshelteredandInactiveAdvanceFilters() throws MalformedURLException, InterruptedException {

		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();
		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");
		ClientList.clickOnAdvancedFilter();
		ClientList.clickOnChooseFiter();
		ClientList.clickOnRadioClientCategory();
		ClientList.clickOnClientCategoryFilterButton();
		ClientList.clickOnRadioEngagedUnsheltered();
		ClientList.clickOnRadioInactive();
		ClientList.clickOnApplyFilter();
	}

	@Test

	public void verifyExcelDownloadForEngagedUnshelteredandInactive()
			throws MalformedURLException, InterruptedException, IOException {

		Base.clickOnClientsMenu();
		Base.waitForSideBarMenuToBexpanded();

		Base.clickOnClientListMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/StreetSmart/ClientList");
		Assert.assertEquals(ClientList.returnClientListPageHeader(), "Client List");
		int fileCountBefore = Helpers.countFilesInDownloadFolder();

		ClientList.clickOnExportToExcel();
		ClientList.clickOnExportToExcelAllRecords();
		Assert.assertEquals(Common.verifyExportAllToExcelMessage(), "Excel will be available when completed.");

		WebDriverTasks.waitForAngularPendingRequests();

		int fileCountAfter = Helpers.countFilesInDownloadFolder();
		Assert.assertTrue(fileCountBefore + 1 == fileCountAfter, "A new file was not added to the directory");

		String mostRecentFile = Helpers.returnMostRecentFileinDownloadFolder();
		String EngagedUnshelteredProspectRecord = Helpers.returnExcelCellValue(mostRecentFile,
				"OutreachClientListReport", 7022, 5);
		String InactiveRecord = Helpers.returnExcelCellValue(mostRecentFile, "OutreachClientListReport", 16141, 5);

		Assert.assertEquals(EngagedUnshelteredProspectRecord, "Engaged Unsheltered Prospect");
		Assert.assertEquals(InactiveRecord, "Inactive");
	}

}