package nycnet.dhs.streetsmart.tests;

import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.EncampmentsList;
import nycnet.dhs.streetsmart.pages.MyCaseload;
import nycnet.dhs.streetsmart.pages.admin.UserPermissions;
import nycnet.dhs.streetsmart.pages.dashboard.DailyOutreach;
import nycnet.dhs.streetsmart.pages.dashboard.Monthly;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Month;
import java.util.HashMap;

public class Encampments extends AutoRunTestController{

    @Test
    public static void verifyEncampments(){
        //Set Encampment and Cleanup data
        String location = "Asser Levy Park";

        HashMap<String,String> encampmentData = new HashMap<>();
        encampmentData.put("Name","TestUser");
        encampmentData.put("Provider","BronxWorks");
        encampmentData.put("Area","Bronx");
        encampmentData.put("Property Type","Beach");
        encampmentData.put("Property Owner","NYC Government");
        encampmentData.put("Borough","Bronx");
        encampmentData.put("Beach Name","Bronx Beach");
        encampmentData.put("Syringes Present","No");
        encampmentData.put("Structures","Blankets");

        HashMap<String,String> cleanupsData = new HashMap<>();
        cleanupsData.put("Cleanup Date","01/06/2021");
        cleanupsData.put("Cleanup Time","12:30 PM");
        cleanupsData.put("Joint Outreach","No");
        cleanupsData.put("Client Exists","No");

        //Navigate t Encampment list page
        Base.clickOnEncampments();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnEncampmentsList();

        //Verify that datagrid is displayed
        boolean isDisplayed =  EncampmentsList.verifyDataGridIsDisplayed();
        Assert.assertTrue(isDisplayed,"Data grid is not displayed on Encampments List page");

        //Add new Encampment
        EncampmentsList.addNewEncampment(encampmentData);

        //Verify that new encampment we added is reflected with expected data
        boolean isAsExpected = EncampmentsList.verifyEncampmentData(encampmentData);
        Assert.assertTrue(isAsExpected,"Encampments detail are not reflcted as expected");

        //Add a new cleanup
        EncampmentsList.addNewCleanup(cleanupsData);

        //Navigate back to Encampment List page
        EncampmentsList.clickOnEncampmentsListLink();

        //Search for Location values and verify that data grid shows only those values for 'Location' column
        DailyOutreach.searchFor(location);
        boolean verifySearch = Monthly.verifyDataInColumn("Location",location);
        Assert.assertTrue(verifySearch,"Searched data is not displayed in grid for column 'Location'");

        //Clear the search before applying advanced filter
        DailyOutreach.searchFor("");

        //Apply advanced filter  for Name column and verify that data grid shows only those values for name column
        UserPermissions.advancedFilterBy("Name", "TestUser","Name", true);
        verifySearch = Monthly.verifyDataInColumn("Name","TestUser");
        Assert.assertTrue(verifySearch,"Filtered data is not displayed in grid for column 'Name'");
    }

}
