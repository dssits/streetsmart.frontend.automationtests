package nycnet.dhs.streetsmart.tests;

import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.*;

public class ActionSets {

	//Add a new enrolled client
    public static void createNewEnrolledClient() {
       
        Base.clickOnClientsMenu();
        Base.waitForSideBarMenuToBexpanded();
        Base.clickOnAddClientMenuItem();
        int random = Helpers.returnRandomNumber();
        String firstName = "User" + Helpers.returnRandomNumber();
        String lastName = "Automation" + random;
        System.out.println(firstName);
        ClientSearch.inputTextinFirstNameField(firstName);
        ClientSearch.inputTextinLastNameField("Automation" + random);
        ClientSearch.clickOnDupliceSearchButton();
        ClientSearch.clickOnDuplicateSearchAddNewClient();
        Common.clickOnEnrolledButton();
        ClientAddNewClient.inputFirstName(firstName);
        ClientAddNewClient.inputLastName(lastName);
        ClientAddNewClient.preferredFirstNameInput(firstName);
        ClientAddNewClient.preferredLastNameInput(lastName);
        ClientAddNewClient.aliasInput(firstName);
        ClientAddNewClient.selectProvider("BG");
        ClientAddNewClient.selectGender("Male");
        ClientAddNewClient.clickOnClientEnrollmentStartDate();
        Common.clickOnLinkText("1");
        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.selectCaseProvider("Breaking Ground");
        ClientAddNewClient.selectCaseStartDate();
        Common.clickOnLinkText("1");
        ClientAddNewClient.selectCaseBorough("Brooklyn");


        WebDriverTasks.pageDownCommon();
        ClientAddNewClient.selectConsentToCaseload("Agree");
        ClientAddNewClient.clickOnSaveButton2();
        
       
    }
    
}