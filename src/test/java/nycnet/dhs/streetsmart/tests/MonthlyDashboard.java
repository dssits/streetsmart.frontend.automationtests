package nycnet.dhs.streetsmart.tests;

import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.dashboard.Monthly;
import org.testng.annotations.Test;


public class MonthlyDashboard extends AutoRunTestController {

    @Test
    public static void VerifyMonthlyDashboard() {
        //Navigate to Monthly dashboard page
        //Base.clickOnDashBoard();
        //Base.waitForSideBarMenuToBexpanded();
        Base.clickonMonthlyDashboardMenuItem();

        //Select past month to ensure that there is data for each tile
        Monthly.selectYearAndMonth("August 2020");

        /*Verify below validations fore each tile
            1.Verify Counts match total count
            2.Verify counts match total count on view details page
            3.Verify total items on grid matches counts
            4.On the Grid - Disable and enable column using column picker
            5.Verify advanced filter can be applied
         */
        Monthly.verifyEachTile();
    }
}

