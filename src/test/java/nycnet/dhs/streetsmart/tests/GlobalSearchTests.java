package nycnet.dhs.streetsmart.tests;


import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import nycnet.dhs.streetsmart.core.Config;
import nycnet.dhs.streetsmart.core.Helpers;
import nycnet.dhs.streetsmart.core.WebDriverTasks;
import nycnet.dhs.streetsmart.pages.Base;
import nycnet.dhs.streetsmart.pages.ClientInformation;
import nycnet.dhs.streetsmart.pages.ClientList;
import nycnet.dhs.streetsmart.pages.Common;
import nycnet.dhs.streetsmart.pages.GlobalSearch;



public class GlobalSearchTests extends AutoRunTestController {

	//H20-859 - Use Global Search to search for client that is Engaged Unsheltered Prospect
	@Test()
	 	public void searchForEngagedUnshelteredProspectInGlobalSearch() {
		Base.clickOnGlobalSearchMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/GlobalSearch");
		String engagedUnshelteredClient = "C161270";
		GlobalSearch.inputTextinGlobalSearchBox(engagedUnshelteredClient);
		GlobalSearch.clickOnGlobalSearchSubmitButtonToSubmit();
		Common.clickOnPartialLinkText(engagedUnshelteredClient);
		ClientList.clearTextinClientListSearchBox();
		ClientList.inputTextinClientListSearchBox(engagedUnshelteredClient);
		ClientList.submitSearchinGlobalSearchBox();
		Common.clickOnLinkText(engagedUnshelteredClient);
		String streetSmartClientID = ClientInformation.returnStreetsmartID();
		String clientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
		Assert.assertEquals(streetSmartClientID, "C161270");
		Assert.assertEquals(clientCategory, "Engaged Unsheltered Prospect");
		
		Base.clickOnGlobalSearchMenuItem();
		Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/GlobalSearch");
		String engagedUnshelteredClientName = "DoNotEdit01 Automation";
		GlobalSearch.inputTextinGlobalSearchBox(engagedUnshelteredClientName);
		GlobalSearch.clickOnGlobalSearchSubmitButtonToSubmit();
		Common.clickOnPartialLinkText(engagedUnshelteredClientName);
		ClientList.clearTextinClientListSearchBox();
		ClientList.inputTextinClientListSearchBox(engagedUnshelteredClient);
		ClientList.submitSearchinGlobalSearchBox();
		Common.clickOnLinkText(engagedUnshelteredClient);
		streetSmartClientID = ClientInformation.returnStreetsmartID();
		clientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
		Assert.assertEquals(streetSmartClientID, "C161270");
		Assert.assertEquals(clientCategory, "Engaged Unsheltered Prospect");

}
	
	//H20-859 - Use Global Search to search for client that is Inactive
		@Test()
		 	public void searchForInactiveClientInGlobalSearch() {
			Base.clickOnGlobalSearchMenuItem();
			Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/GlobalSearch");
			String inactiveClient = "C161271";
			GlobalSearch.inputTextinGlobalSearchBox(inactiveClient);
			GlobalSearch.clickOnGlobalSearchSubmitButtonToSubmit();
			Common.clickOnPartialLinkText(inactiveClient);
			ClientList.clearTextinClientListSearchBox();
			ClientList.inputTextinClientListSearchBox(inactiveClient);
			ClientList.submitSearchinGlobalSearchBox();
			Common.clickOnLinkText(inactiveClient);
			String streetSmartClientID = ClientInformation.returnStreetsmartID();
			String clientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
			Assert.assertEquals(streetSmartClientID, "C161271");
			Assert.assertEquals(clientCategory, "Inactive");
			
			Base.clickOnGlobalSearchMenuItem();
			Assert.assertEquals(WebDriverTasks.returnEndingPageURL(), "dhs.nycnet/GlobalSearch");
			String engagedUnshelteredClientName = "DoNotEdit02 Automation";
			GlobalSearch.inputTextinGlobalSearchBox(engagedUnshelteredClientName);
			GlobalSearch.clickOnGlobalSearchSubmitButtonToSubmit();
			Common.clickOnPartialLinkText(engagedUnshelteredClientName);
			ClientList.clearTextinClientListSearchBox();
			ClientList.inputTextinClientListSearchBox(inactiveClient);
			ClientList.submitSearchinGlobalSearchBox();
			Common.clickOnLinkText(inactiveClient);
			streetSmartClientID = ClientInformation.returnStreetsmartID();
			clientCategory = ClientInformation.returnClientCategoryChosenOptionOnViewPage();
			Assert.assertEquals(streetSmartClientID, "C161271");
			Assert.assertEquals(clientCategory, "Inactive");

	}
	
}




  


